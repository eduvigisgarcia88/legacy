<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionSetting extends Model
{

    protected $table = 'permission_settings';


    public function permissions() {
        return $this->belongsTo('App\Permission', 'settings_id');
    }

    public function permissionModules() {
        return $this->hasOne('App\PermissionModule');
    }

}