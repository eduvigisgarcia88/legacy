-- phpMyAdmin SQL Dump
-- version 4.3.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 05, 2015 at 06:39 PM
-- Server version: 5.5.43-MariaDB
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `legacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `legacy_assigned_sales`
--

CREATE TABLE IF NOT EXISTS `legacy_assigned_sales` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `assistant_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_assigned_sales`
--

INSERT INTO `legacy_assigned_sales` (`id`, `sales_id`, `assistant_id`, `created_at`, `updated_at`) VALUES
(1, 43, 35, '2015-10-05 09:17:57', '2015-10-05 01:17:57');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_data_feeds`
--

CREATE TABLE IF NOT EXISTS `legacy_data_feeds` (
`id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `state_from_date` varchar(100) DEFAULT NULL,
  `state_to_date` varchar(100) DEFAULT NULL,
  `sales_code` varchar(100) DEFAULT NULL,
  `sales_name` varchar(100) DEFAULT NULL,
  `report_sales_code` varchar(100) DEFAULT NULL,
  `report_sales_name` varchar(100) DEFAULT NULL,
  `client_name` varchar(100) DEFAULT NULL,
  `client_sec_no` varchar(100) DEFAULT NULL,
  `contract_no` varchar(100) DEFAULT NULL,
  `contract_type` varchar(100) DEFAULT NULL,
  `contract_curr` varchar(100) DEFAULT NULL,
  `payment_curr` varchar(100) DEFAULT NULL,
  `trans_code` varchar(100) DEFAULT NULL,
  `trans_date` varchar(100) DEFAULT NULL,
  `gross_trans_amt` varchar(100) DEFAULT NULL,
  `gross_fee_amt` varchar(100) DEFAULT NULL,
  `net_fee_amt` varchar(100) DEFAULT NULL,
  `fee_desc` varchar(100) DEFAULT NULL,
  `fee_amt` varchar(100) DEFAULT NULL,
  `amt_conv_rate` varchar(100) DEFAULT NULL,
  `fee_amt_conv_rate` varchar(100) DEFAULT NULL,
  `batch_file_run_date` varchar(100) DEFAULT NULL,
  `adjust_amt` varchar(100) DEFAULT NULL,
  `issue_date` varchar(100) DEFAULT NULL,
  `incept_date` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_data_feeds`
--

INSERT INTO `legacy_data_feeds` (`id`, `data_id`, `provider_id`, `state_from_date`, `state_to_date`, `sales_code`, `sales_name`, `report_sales_code`, `report_sales_name`, `client_name`, `client_sec_no`, `contract_no`, `contract_type`, `contract_curr`, `payment_curr`, `trans_code`, `trans_date`, `gross_trans_amt`, `gross_fee_amt`, `net_fee_amt`, `fee_desc`, `fee_amt`, `amt_conv_rate`, `fee_amt_conv_rate`, `batch_file_run_date`, `adjust_amt`, `issue_date`, `incept_date`, `created_at`, `updated_at`) VALUES
(73, 11, 11, '20131201', '20131301', NULL, NULL, '88000113', 'Mike Lee', 'Ong Li', '               ', 'N2469943', 'NCA', 'SGD', 'SGD', 'B522', NULL, '500', '2.68', '2.5', 'INITIAL/UPFRONT FEE           ', '2.41', '1', '1', '20131231', '0', '20130923', '20130923', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(74, 11, 11, '20131201', '20131301', NULL, NULL, '88000113', 'Mike Lee', 'Ong Li', '               ', 'N2469943', 'NCA', 'SGD', 'SGD', 'B522', NULL, '500', '2.68', '2.5', 'INITIAL/UPFRONT FEE           ', '2.41', '1', '1', '20131231', '0', '20130923', '20130923', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(75, 11, 11, '20131201', '20131301', NULL, NULL, '88000113', 'Mike Lee', 'Jes Tee', '               ', 'N2474560', 'NOA', 'SGD', 'SGD', 'T642', NULL, '18000', '412.55', '385.56', 'INITIAL/UPFRONT FEE           ', '373.98', '1', '1', '20131231', '0', '20131209', '20131206', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(76, 11, 11, '20131201', '20131301', NULL, NULL, '88000113', 'Mike Lee', 'Jes Tee', '               ', 'N2474560', 'NOA', 'SGD', 'SGD', 'T642', NULL, '18000', '412.55', '385.56', 'INITIAL/UPFRONT FEE           ', '373.98', '1', '1', '20131231', '0', '20131209', '20131206', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(77, 11, 11, '20131201', '20131301', NULL, NULL, '88000111', 'John Stewart', 'Chng Ng', '               ', 'N2390719', 'NOA', 'SGD', 'SGD', 'B522', NULL, '550', '16.48', '15.4', 'INITIAL/UPFRONT FEE           ', '14.93', '1', '1', '20131231', '0', '20110708', '20110707', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(78, 11, 11, '20131201', '20131301', NULL, NULL, '88000111', 'John Stewart', 'Chng Ng', '               ', 'N2390728', 'NOA', 'SGD', 'SGD', 'B522', NULL, '500', '14.98', '14', 'INITIAL/UPFRONT FEE           ', '13.58', '1', '1', '20131231', '0', '20110718', '20110707', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(79, 11, 11, '20131201', '20131301', NULL, NULL, '88000111', 'John Stewart', 'Chng Go', '               ', 'N2424421', 'NOA', 'SGD', 'SGD', 'B522', NULL, '1000', '29.96', '28', 'INITIAL/UPFRONT FEE           ', '27.16', '1', '1', '20131231', '0', '20120503', '20120502', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(80, 11, 11, '20131201', '20131301', NULL, NULL, '88000124', 'Francis Chan', 'Zar Lim', '               ', 'N2392463', 'NOA', 'SGD', 'SGD', 'B522', NULL, '2500', '74.9', '70', 'INITIAL/UPFRONT FEE           ', '67.89', '1', '1', '20131231', '0', '20110721', '20110720', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(81, 11, 11, '20131201', '20131301', NULL, NULL, '88000124', 'Francis Chan', 'Chua Ser', '               ', 'N2398690', 'NOA', 'SGD', 'SGD', 'B522', NULL, '1000', '29.96', '28', 'INITIAL/UPFRONT FEE           ', '27.14', '1', '1', '20131231', '0', '20110831', '20110829', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(82, 11, 11, '20131201', '20131301', NULL, NULL, '88000121', 'Mike Lee', 'Jia Lim', '               ', 'N2393853', 'NOA', 'SGD', 'SGD', 'T679', NULL, '10000', '149.8', '140', 'INITIAL/UPFRONT FEE           ', '135.8', '1', '1', '20131231', '0', '20110802', '20110728', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(83, 11, 11, '20131201', '20131301', NULL, NULL, '88000121', 'Mike Lee', 'Shawn Han', '               ', 'N2426021', 'NOA', 'SGD', 'SGD', 'T679', NULL, '30000', '449.4', '420', 'INITIAL/UPFRONT FEE           ', '407.4', '1', '1', '20131231', '0', '20120521', '20120518', '2015-10-05 06:58:18', '2015-10-04 22:58:18'),
(84, 11, 11, '20131201', '20131301', NULL, NULL, '88000121', 'Mike Lee', 'Shawn Dy', '               ', 'N2427171', 'NOA', 'SGD', 'SGD', 'B522', NULL, '1500', '44.94', '42', 'INITIAL/UPFRONT FEE           ', '40.74', '1', '1', '20131231', '0', '20120604', '20120601', '2015-10-05 06:58:18', '2015-10-04 22:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_designations`
--

CREATE TABLE IF NOT EXISTS `legacy_designations` (
`id` int(11) NOT NULL,
  `rank` tinyint(1) NOT NULL,
  `designation` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_designations`
--

INSERT INTO `legacy_designations` (`id`, `rank`, `designation`) VALUES
(1, 1, 'Sales Agent'),
(2, 2, 'Assistant Manager'),
(3, 3, 'Manager'),
(4, 2, 'Assistant Director'),
(5, 3, 'Director'),
(6, 4, 'Partner');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_introducers`
--

CREATE TABLE IF NOT EXISTS `legacy_introducers` (
`id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `code` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `zipcode` varchar(1000) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_introducers`
--

INSERT INTO `legacy_introducers` (`id`, `policy_id`, `code`, `name`, `email`, `zipcode`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, '23', '23', '23', '23', 0, '2015-10-05 10:14:11', '2015-10-05 02:14:11'),
(2, 0, '23', '23', '23', '23', 0, '2015-10-05 10:14:13', '2015-10-05 02:14:13'),
(3, 0, '43', '43', '43', '43', 0, '2015-10-05 10:25:16', '2015-10-05 02:25:16');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_log_policies`
--

CREATE TABLE IF NOT EXISTS `legacy_log_policies` (
`id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `activity` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_log_products`
--

CREATE TABLE IF NOT EXISTS `legacy_log_products` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `activity` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_log_products`
--

INSERT INTO `legacy_log_products` (`id`, `product_id`, `edit_id`, `activity`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'Product Updated', '2015-09-23 05:39:39', '2015-09-22 21:39:39'),
(2, 5, 1, 'Description Changed', '2015-09-23 05:54:15', '2015-09-22 21:54:15'),
(3, 5, 1, 'Product Updated', '2015-09-23 05:54:15', '2015-09-22 21:54:15'),
(4, 5, 1, 'Code Changed', '2015-09-23 05:54:27', '2015-09-22 21:54:27'),
(5, 5, 1, 'Product Updated', '2015-09-23 05:54:27', '2015-09-22 21:54:27'),
(6, 5, 1, 'Name Changed', '2015-09-23 05:54:37', '2015-09-22 21:54:37'),
(7, 5, 1, 'Product Updated', '2015-09-23 05:54:37', '2015-09-22 21:54:37'),
(8, 5, 1, 'Provider Changed', '2015-09-23 05:54:45', '2015-09-22 21:54:45'),
(9, 5, 1, 'Product Updated', '2015-09-23 05:54:45', '2015-09-22 21:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_log_sales`
--

CREATE TABLE IF NOT EXISTS `legacy_log_sales` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `activity` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_log_sales`
--

INSERT INTO `legacy_log_sales` (`id`, `user_id`, `edit_id`, `activity`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 'Sales User Created', '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 9, 1, 'Sales User Created', '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 10, 1, 'Sales User Created', '2015-09-18 08:06:36', '2015-09-18 00:06:36'),
(4, 11, 1, 'Sales User Created', '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 12, 1, 'Sales User Created', '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 13, 1, 'Sales User Created', '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 14, 1, 'Sales User Created', '2015-09-18 08:13:59', '2015-09-18 00:13:59'),
(8, 15, 1, 'Sales User Created', '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 16, 1, 'Sales User Created', '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 17, 1, 'Sales User Created', '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 18, 1, 'Sales User Created', '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 19, 1, 'Sales User Created', '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 20, 1, 'Sales User Created', '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 21, 1, 'Sales User Created', '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 22, 1, 'Sales User Created', '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 23, 1, 'Sales User Created', '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 24, 1, 'Sales User Created', '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 25, 1, 'Sales User Created', '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 26, 1, 'Sales User Created', '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 27, 1, 'Sales User Created', '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 28, 1, 'Sales User Created', '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 29, 1, 'Sales User Created', '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 30, 1, 'Sales User Created', '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 31, 1, 'Sales User Created', '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(25, 32, 1, 'Sales User Created', '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 33, 1, 'Sales User Created', '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 34, 1, 'Sales User Created', '2015-09-18 08:46:53', '2015-09-18 00:46:53'),
(28, 36, 1, 'Sales User Created', '2015-09-23 02:34:03', '2015-09-22 18:34:03'),
(29, 37, 1, 'Sales User Created', '2015-09-23 02:40:51', '2015-09-22 18:40:51'),
(30, 38, 1, 'Sales User Created', '2015-09-23 02:43:09', '2015-09-22 18:43:09'),
(31, 39, 1, 'Sales User Created', '2015-10-02 07:00:50', '2015-10-01 23:00:50'),
(32, 40, 1, 'Sales User Created', '2015-10-02 07:01:49', '2015-10-01 23:01:49'),
(33, 41, 1, 'Sales User Created', '2015-10-02 07:02:43', '2015-10-01 23:02:43'),
(34, 42, 1, 'Sales User Created', '2015-10-02 07:06:21', '2015-10-01 23:06:21'),
(35, 43, 1, 'Sales User Created', '2015-10-02 07:08:02', '2015-10-01 23:08:02'),
(36, 44, 1, 'Sales User Created', '2015-10-02 07:11:33', '2015-10-01 23:11:33'),
(37, 45, 1, 'Sales User Created', '2015-10-02 07:12:20', '2015-10-01 23:12:20'),
(38, 46, 1, 'Sales User Created', '2015-10-02 07:13:00', '2015-10-01 23:13:00'),
(39, 44, 1, 'Code Changed', '2015-10-02 07:13:20', '2015-10-01 23:13:20'),
(40, 44, 1, 'System User Updated', '2015-10-02 07:13:21', '2015-10-01 23:13:21'),
(41, 46, 1, 'Code Changed', '2015-10-05 03:12:33', '2015-10-04 19:12:33'),
(42, 46, 1, 'System User Updated', '2015-10-05 03:12:34', '2015-10-04 19:12:34'),
(43, 45, 1, 'Code Changed', '2015-10-05 03:13:13', '2015-10-04 19:13:13'),
(44, 45, 1, 'System User Updated', '2015-10-05 03:13:14', '2015-10-04 19:13:14'),
(45, 44, 1, 'Code Changed', '2015-10-05 03:13:37', '2015-10-04 19:13:37'),
(46, 44, 1, 'System User Updated', '2015-10-05 03:13:37', '2015-10-04 19:13:37'),
(47, 43, 1, 'Code Changed', '2015-10-05 03:13:52', '2015-10-04 19:13:52'),
(48, 43, 1, 'System User Updated', '2015-10-05 03:13:52', '2015-10-04 19:13:52'),
(49, 42, 1, 'Code Changed', '2015-10-05 03:14:10', '2015-10-04 19:14:10'),
(50, 42, 1, 'System User Updated', '2015-10-05 03:14:10', '2015-10-04 19:14:10'),
(51, 46, 1, 'Bonding Updated', '2015-10-05 06:24:37', '2015-10-04 22:24:37'),
(52, 46, 1, 'Bonding Updated', '2015-10-05 06:37:40', '2015-10-04 22:37:40'),
(53, 20, 1, 'Sales Team Updated', '2015-10-05 07:26:46', '2015-10-04 23:26:46'),
(54, 20, 1, 'Sales Team Updated', '2015-10-05 07:27:10', '2015-10-04 23:27:10'),
(55, 20, 1, 'Sales Team Updated', '2015-10-05 07:27:25', '2015-10-04 23:27:25'),
(56, 37, 1, 'Sales Team Updated', '2015-10-05 07:28:07', '2015-10-04 23:28:07'),
(57, 37, 1, 'Sales Team Updated', '2015-10-05 07:28:25', '2015-10-04 23:28:25'),
(58, 45, 1, 'Sales Team Updated', '2015-10-05 07:28:56', '2015-10-04 23:28:56'),
(59, 45, 1, 'Sales Team Updated', '2015-10-05 07:29:44', '2015-10-04 23:29:44'),
(60, 45, 1, 'Sales Team Updated', '2015-10-05 07:30:00', '2015-10-04 23:30:00'),
(61, 45, 1, 'Sales Team Updated', '2015-10-05 07:30:14', '2015-10-04 23:30:14'),
(62, 20, 1, 'Sales Team Updated', '2015-10-05 07:32:15', '2015-10-04 23:32:15'),
(63, 46, 1, 'Sales Team Updated', '2015-10-05 07:34:01', '2015-10-04 23:34:01'),
(64, 46, 1, 'Sales Team Updated', '2015-10-05 07:38:01', '2015-10-04 23:38:01'),
(65, 46, 1, 'Bonding Updated', '2015-10-05 07:38:26', '2015-10-04 23:38:26'),
(66, 46, 1, 'Sales Team Updated', '2015-10-05 07:40:03', '2015-10-04 23:40:03'),
(67, 46, 1, 'Sales Team Updated', '2015-10-05 07:41:59', '2015-10-04 23:41:59'),
(68, 46, 1, 'Sales Team Updated', '2015-10-05 07:42:03', '2015-10-04 23:42:03'),
(69, 46, 1, 'Sales Team Updated', '2015-10-05 07:42:20', '2015-10-04 23:42:20'),
(70, 46, 1, 'Sales Team Updated', '2015-10-05 07:43:54', '2015-10-04 23:43:54'),
(71, 46, 1, 'Sales Team Updated', '2015-10-05 07:45:16', '2015-10-04 23:45:16'),
(72, 46, 1, 'Sales Team Updated', '2015-10-05 07:49:25', '2015-10-04 23:49:25'),
(73, 46, 1, 'Sales Team Updated', '2015-10-05 07:49:51', '2015-10-04 23:49:51'),
(74, 46, 1, 'Sales Team Updated', '2015-10-05 07:51:36', '2015-10-04 23:51:36'),
(75, 46, 1, 'Sales Team Updated', '2015-10-05 07:57:45', '2015-10-04 23:57:45'),
(76, 46, 1, 'Sales Team Updated', '2015-10-05 08:00:34', '2015-10-05 00:00:34'),
(77, 46, 1, 'Sales Team Updated', '2015-10-05 08:02:58', '2015-10-05 00:02:58'),
(78, 46, 1, 'Sales Team Updated', '2015-10-05 08:05:50', '2015-10-05 00:05:50'),
(79, 46, 1, 'Sales Team Updated', '2015-10-05 08:09:39', '2015-10-05 00:09:39'),
(80, 46, 1, 'Sales Team Updated', '2015-10-05 08:10:17', '2015-10-05 00:10:17'),
(81, 46, 1, 'Sales Team Updated', '2015-10-05 08:11:19', '2015-10-05 00:11:19'),
(82, 46, 1, 'Sales Team Updated', '2015-10-05 08:12:43', '2015-10-05 00:12:43'),
(83, 46, 1, 'Sales Team Updated', '2015-10-05 08:15:22', '2015-10-05 00:15:22'),
(84, 46, 1, 'Sales Team Updated', '2015-10-05 08:18:06', '2015-10-05 00:18:06'),
(85, 46, 1, 'Sales Team Updated', '2015-10-05 08:20:19', '2015-10-05 00:20:19'),
(86, 46, 1, 'Sales Team Updated', '2015-10-05 08:20:46', '2015-10-05 00:20:46'),
(87, 46, 1, 'Sales Team Updated', '2015-10-05 08:21:07', '2015-10-05 00:21:07'),
(88, 22, 1, 'Sales Team Updated', '2015-10-05 08:43:54', '2015-10-05 00:43:54'),
(89, 22, 1, 'Sales Team Updated', '2015-10-05 08:44:57', '2015-10-05 00:44:57'),
(90, 22, 1, 'Sales Team Updated', '2015-10-05 08:57:04', '2015-10-05 00:57:04'),
(91, 22, 1, 'Sales Team Updated', '2015-10-05 08:57:09', '2015-10-05 00:57:09'),
(92, 22, 1, 'Sales Team Updated', '2015-10-05 09:05:37', '2015-10-05 01:05:37'),
(93, 22, 1, 'Bonding Updated', '2015-10-05 09:15:49', '2015-10-05 01:15:49');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_log_users`
--

CREATE TABLE IF NOT EXISTS `legacy_log_users` (
`id` int(11) NOT NULL,
  `activity` varchar(1000) NOT NULL,
  `user_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_log_users`
--

INSERT INTO `legacy_log_users` (`id`, `activity`, `user_id`, `edit_id`, `created_at`, `updated_at`) VALUES
(1, 'Designation Changed', 7, 1, '2015-09-18 08:30:44', '2015-09-18 00:30:44'),
(2, 'System User Updated', 7, 1, '2015-09-18 08:30:45', '2015-10-02 07:40:57'),
(3, 'System User Disabled', 7, 1, '2015-09-21 06:20:51', '2015-10-02 07:41:01'),
(4, 'System User Created', 35, 1, '2015-09-21 06:57:48', '2015-09-20 22:57:48'),
(5, 'System User Enabled', 7, 1, '2015-09-23 05:42:40', '2015-09-22 21:42:40'),
(6, 'Agent Assigned', 35, 1, '2015-10-05 09:17:57', '2015-10-05 01:17:57');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_migrations`
--

CREATE TABLE IF NOT EXISTS `legacy_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_permissions`
--

CREATE TABLE IF NOT EXISTS `legacy_permissions` (
`id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_permissions`
--

INSERT INTO `legacy_permissions` (`id`, `module_id`, `usertype_id`, `view`, `edit`, `delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(2, 2, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-11 02:30:54'),
(3, 3, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-11 02:21:26'),
(4, 4, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-11 02:21:26'),
(5, 5, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-10 22:14:50'),
(6, 1, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 23:53:40'),
(7, 2, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-17 02:52:51'),
(8, 3, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:52'),
(9, 4, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:52'),
(10, 5, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-10 22:15:10'),
(11, 1, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(12, 2, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(13, 3, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(14, 4, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(15, 5, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(16, 1, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(17, 2, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(18, 3, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(19, 4, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(20, 5, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(21, 1, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(22, 2, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(23, 3, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:10'),
(24, 4, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(25, 5, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(26, 1, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(27, 2, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(28, 3, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(29, 4, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(30, 5, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(31, 1, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(32, 2, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(33, 3, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(34, 4, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(35, 5, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_permission_modules`
--

CREATE TABLE IF NOT EXISTS `legacy_permission_modules` (
`id` int(11) NOT NULL,
  `module` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_permission_modules`
--

INSERT INTO `legacy_permission_modules` (`id`, `module`) VALUES
(1, 'User Management'),
(2, 'Payroll Management'),
(3, 'Policy Management'),
(4, 'Provider and Product Management'),
(5, 'Summary and Reports');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_policies`
--

CREATE TABLE IF NOT EXISTS `legacy_policies` (
`id` int(11) NOT NULL,
  `state_from_date` datetime NOT NULL,
  `state_to_date` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `sales_code` varchar(1000) NOT NULL,
  `sales_name` varchar(1000) NOT NULL,
  `report_sales_code` varchar(1000) NOT NULL,
  `report_sales_name` varchar(1000) NOT NULL,
  `client_name` varchar(1000) NOT NULL,
  `client_sec_no` varchar(1000) NOT NULL,
  `contract_no` varchar(1000) NOT NULL,
  `contract_type` varchar(1000) NOT NULL,
  `contract_curr` varchar(1000) NOT NULL,
  `payment_curr` varchar(1000) NOT NULL,
  `trans_code` varchar(1000) NOT NULL,
  `trans_date` datetime NOT NULL,
  `gross_trans_amt` int(11) NOT NULL,
  `gross_fee_amt` int(11) NOT NULL,
  `net_fee_amt` int(11) NOT NULL,
  `fee_desc` varchar(1000) NOT NULL,
  `fee_amt` int(11) NOT NULL,
  `amt_conv_rate` int(11) NOT NULL,
  `fee_amt_conv_rate` int(11) NOT NULL,
  `batch_file_run_date` datetime NOT NULL,
  `adjust_amt` int(11) NOT NULL,
  `issue_date` datetime NOT NULL,
  `incept_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_policies`
--

INSERT INTO `legacy_policies` (`id`, `state_from_date`, `state_to_date`, `user_id`, `provider_id`, `sales_code`, `sales_name`, `report_sales_code`, `report_sales_name`, `client_name`, `client_sec_no`, `contract_no`, `contract_type`, `contract_curr`, `payment_curr`, `trans_code`, `trans_date`, `gross_trans_amt`, `gross_fee_amt`, `net_fee_amt`, `fee_desc`, `fee_amt`, `amt_conv_rate`, `fee_amt_conv_rate`, `batch_file_run_date`, `adjust_amt`, `issue_date`, `incept_date`, `status`, `created_at`, `updated_at`) VALUES
(1, '2013-12-01 00:00:00', '2014-01-01 00:00:00', NULL, 11, '880001', 'Jacob Lim', '88000113', 'Mike Lee', 'Ong Li', '               ', 'N2469943', 'NCA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 500, 3, 3, 'INITIAL/UPFRONT FEE           ', 2, 1, 1, '2013-12-31 00:00:00', 0, '2013-09-23 00:00:00', '2013-09-23 00:00:00', 1, '2015-10-05 07:04:28', '2015-10-05 07:47:14'),
(2, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 42, 11, '88000125', 'Jacob Lim', '88000113', 'Mike Lee', 'Ong Li', '               ', 'N2469943', 'NCA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 500, 3, 3, 'INITIAL/UPFRONT FEE           ', 2, 1, 1, '2013-12-31 00:00:00', 0, '2013-09-23 00:00:00', '2013-09-23 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(3, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 43, 11, '88000126', 'Andrea Chng', '88000113', 'Mike Lee', 'Jes Tee', '               ', 'N2474560', 'NOA', 'SGD', 'SGD', 'T642', '2013-12-09 00:00:00', 18000, 413, 386, 'INITIAL/UPFRONT FEE           ', 374, 1, 1, '2013-12-31 00:00:00', 0, '2013-12-09 00:00:00', '2013-12-06 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(4, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 43, 11, '88000126', 'Andrea Chng', '88000113', 'Mike Lee', 'Jes Tee', '               ', 'N2474560', 'NOA', 'SGD', 'SGD', 'T642', '2013-12-09 00:00:00', 18000, 413, 386, 'INITIAL/UPFRONT FEE           ', 374, 1, 1, '2013-12-31 00:00:00', 0, '2013-12-09 00:00:00', '2013-12-06 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(5, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 44, 11, '88000127', 'Roland Ng', '88000111', 'John Stewart', 'Chng Ng', '               ', 'N2390719', 'NOA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 550, 16, 15, 'INITIAL/UPFRONT FEE           ', 15, 1, 1, '2013-12-31 00:00:00', 0, '2011-07-08 00:00:00', '2011-07-07 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(6, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 44, 11, '88000127', 'Roland Ng', '88000111', 'John Stewart', 'Chng Ng', '               ', 'N2390728', 'NOA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 500, 15, 14, 'INITIAL/UPFRONT FEE           ', 14, 1, 1, '2013-12-31 00:00:00', 0, '2011-07-18 00:00:00', '2011-07-07 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(7, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 44, 11, '88000127', 'Roland Ng', '88000111', 'John Stewart', 'Chng Go', '               ', 'N2424421', 'NOA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 1000, 30, 28, 'INITIAL/UPFRONT FEE           ', 27, 1, 1, '2013-12-31 00:00:00', 0, '2012-05-03 00:00:00', '2012-05-02 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(8, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 45, 11, '88000128', 'Mark Sy', '88000124', 'Francis Chan', 'Zar Lim', '               ', 'N2392463', 'NOA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 2500, 75, 70, 'INITIAL/UPFRONT FEE           ', 68, 1, 1, '2013-12-31 00:00:00', 0, '2011-07-21 00:00:00', '2011-07-20 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(9, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 45, 11, '88000128', 'Mark Sy', '88000124', 'Francis Chan', 'Chua Ser', '               ', 'N2398690', 'NOA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 1000, 30, 28, 'INITIAL/UPFRONT FEE           ', 27, 1, 1, '2013-12-31 00:00:00', 0, '2011-08-31 00:00:00', '2011-08-29 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(10, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 46, 11, '88000133', 'Dennis Chua', '88000121', 'Mike Lee', 'Jia Lim', '               ', 'N2393853', 'NOA', 'SGD', 'SGD', 'T679', '2013-12-19 00:00:00', 10000, 150, 140, 'INITIAL/UPFRONT FEE           ', 136, 1, 1, '2013-12-31 00:00:00', 0, '2011-08-02 00:00:00', '2011-07-28 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(11, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 46, 11, '88000133', 'Dennis Chua', '88000121', 'Mike Lee', 'Shawn Han', '               ', 'N2426021', 'NOA', 'SGD', 'SGD', 'T679', '2013-12-13 00:00:00', 30000, 449, 420, 'INITIAL/UPFRONT FEE           ', 407, 1, 1, '2013-12-31 00:00:00', 0, '2012-05-21 00:00:00', '2012-05-18 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29'),
(12, '2013-12-01 00:00:00', '2014-01-01 00:00:00', 46, 11, '88000133', 'Dennis Chua', '88000121', 'Mike Lee', 'Shawn Dy', '               ', 'N2427171', 'NOA', 'SGD', 'SGD', 'B522', '2013-12-15 00:00:00', 1500, 45, 42, 'INITIAL/UPFRONT FEE           ', 41, 1, 1, '2013-12-31 00:00:00', 0, '2012-06-04 00:00:00', '2012-06-01 00:00:00', 1, '2015-10-05 07:04:29', '2015-10-04 23:04:29');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_pre_payrolls`
--

CREATE TABLE IF NOT EXISTS `legacy_pre_payrolls` (
`id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `type` enum('Incentives','Deductives') NOT NULL,
  `remarks` varchar(1000) NOT NULL,
  `cost` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_production_cases`
--

CREATE TABLE IF NOT EXISTS `legacy_production_cases` (
`id` int(11) NOT NULL,
  `policy_id` int(11) NOT NULL,
  `policy_no` int(11) NOT NULL,
  `policy_type` int(11) NOT NULL,
  `comp_code` int(11) NOT NULL,
  `policy_term` int(11) NOT NULL,
  `contract_curr` int(11) NOT NULL,
  `sum_insured` int(11) NOT NULL,
  `incept_date` datetime NOT NULL,
  `install_form_date` datetime NOT NULL,
  `billing_freq` int(11) NOT NULL,
  `policy_exp_date` datetime NOT NULL,
  `gross_prem_paid` int(11) NOT NULL,
  `net_prem_paid` int(11) NOT NULL,
  `gross_prem_inc_gst` int(11) NOT NULL,
  `net_prem_inc_gst` int(11) NOT NULL,
  `prem_wo_com` int(11) NOT NULL,
  `prem_conv_rate` int(11) NOT NULL,
  `payment_curr` int(11) NOT NULL,
  `com_or` int(11) NOT NULL,
  `com_run_date` datetime NOT NULL,
  `com_conv_rate` int(11) NOT NULL,
  `trans_code` int(11) NOT NULL,
  `prem_term` int(11) NOT NULL,
  `date_issue` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_production_cases`
--

INSERT INTO `legacy_production_cases` (`id`, `policy_id`, `policy_no`, `policy_type`, `comp_code`, `policy_term`, `contract_curr`, `sum_insured`, `incept_date`, `install_form_date`, `billing_freq`, `policy_exp_date`, `gross_prem_paid`, `net_prem_paid`, `gross_prem_inc_gst`, `net_prem_inc_gst`, `prem_wo_com`, `prem_conv_rate`, `payment_curr`, `com_or`, `com_run_date`, `com_conv_rate`, `trans_code`, `prem_term`, `date_issue`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 12, 12, 12, 12, 12, 12, '2015-12-01 00:00:00', '2015-12-01 00:00:00', 12, '2015-12-01 00:00:00', 12, 12, 12, 12, 12, 12, 12, 12, '2015-12-01 00:00:00', 12, 12, 12, '2015-12-01 00:00:00', 1, '2015-10-02 10:48:39', '2015-10-02 02:48:39'),
(2, 12, 12, 12, 12, 12, 12, 12, '2015-12-01 00:00:00', '2015-12-01 00:00:00', 12, '2015-12-01 00:00:00', 12, 12, 12, 12, 12, 12, 12, 12, '2015-12-01 00:00:00', 12, 12, 12, '2015-12-01 00:00:00', 1, '2015-10-02 10:50:02', '2015-10-02 02:50:02'),
(3, 13, 13, 13, 13, 13, 13, 13, '2015-01-01 00:00:00', '2015-01-01 00:00:00', 13, '2015-01-01 00:00:00', 13, 13, 13, 13, 13, 13, 13, 13, '2015-01-01 00:00:00', 13, 13, 13, '2015-01-01 00:00:00', 1, '2015-10-05 03:14:28', '2015-10-04 19:14:28'),
(4, 15, 15, 15, 15, 15, 15, 15, '2015-01-01 00:00:00', '2015-01-01 00:00:00', 15, '2015-05-01 00:00:00', 15, 15, 15, 15, 15, 15, 15, 15, '2015-01-01 00:00:00', 15, 15, 15, '2015-01-01 00:00:00', 1, '2015-10-05 03:20:29', '2015-10-04 19:20:29'),
(5, 16, 16, 16, 16, 16, 16, 16, '2015-01-01 00:00:00', '2015-01-01 00:00:00', 16, '2015-01-01 00:00:00', 16, 16, 16, 16, 16, 16, 16, 16, '2015-01-01 00:00:00', 16, 16, 16, '2015-01-01 00:00:00', 1, '2015-10-05 03:22:32', '2015-10-04 19:22:32'),
(6, 18, 18, 18, 18, 18, 18, 18, '2015-01-01 00:00:00', '2015-01-01 00:00:00', 18, '2015-01-01 00:00:00', 18, 18, 18, 18, 18, 18, 18, 18, '2015-01-01 00:00:00', 18, 18, 18, '2015-01-01 00:00:00', 1, '2015-10-05 03:26:49', '2015-10-04 19:26:49'),
(7, 79, 79, 79, 79, 79, 79, 79, '2015-07-01 00:00:00', '2015-07-01 00:00:00', 79, '2015-07-01 00:00:00', 79, 79, 79, 79, 79, 79, 79, 79, '2015-07-01 00:00:00', 79, 79, 79, '2015-07-01 00:00:00', 1, '2015-10-05 03:31:28', '2015-10-04 19:31:28'),
(8, 16, 16, 16, 16, 16, 16, 16, '2015-01-01 00:00:00', '2015-01-01 00:00:00', 16, '2015-01-01 00:00:00', 16, 16, 16, 16, 16, 16, 16, 16, '2015-01-01 00:00:00', 16, 16, 16, '2015-01-01 00:00:00', 1, '2015-10-05 05:43:16', '2015-10-04 21:43:16');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_production_incept`
--

CREATE TABLE IF NOT EXISTS `legacy_production_incept` (
`id` int(11) NOT NULL,
  `case_id` varchar(1000) NOT NULL,
  `info` varchar(1000) NOT NULL,
  `code` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `case_policy_id` int(11) NOT NULL,
  `visor` enum('advisor','supervisor') NOT NULL,
  `case` enum('submission','inception') NOT NULL,
  `edit_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_production_incept`
--

INSERT INTO `legacy_production_incept` (`id`, `case_id`, `info`, `code`, `name`, `case_policy_id`, `visor`, `case`, `edit_id`, `status`, `created_at`, `updated_at`) VALUES
(10, 'CASE-0010', '33', '33', '33', 10, 'advisor', 'inception', 1, 1, '2015-09-28 08:43:37', '2015-10-04 21:43:44'),
(11, 'CASE-0011', '11', '11', '11', 14, 'advisor', 'inception', 1, 1, '2015-09-28 10:09:32', '2015-09-30 01:54:43'),
(12, 'CASE-0012', '213', '213', '213', 15, 'advisor', 'submission', 1, 2, '2015-09-29 03:11:40', '2015-09-30 00:30:43'),
(13, 'CASE-0013', '132', '213', '213', 16, 'advisor', 'submission', 1, 2, '2015-09-29 03:51:30', '2015-09-29 23:01:51'),
(14, 'CASE-0014', '69', '69', '69', 17, 'advisor', 'submission', 1, 1, '2015-09-29 03:51:42', '2015-09-30 01:54:35'),
(15, 'CASE-0015', '89', '89', '89', 18, 'advisor', 'submission', 1, 2, '2015-09-29 03:52:49', '2015-09-29 01:31:14'),
(16, 'CASE-0016', '78', '78', '78', 19, 'advisor', 'submission', 1, 2, '2015-09-29 03:53:19', '2015-09-29 01:25:06'),
(17, 'CASE-0017', '56', '56', '56', 20, 'supervisor', 'submission', 1, 2, '2015-09-29 08:21:20', '2015-09-29 09:19:54'),
(18, 'CASE-0018', '89', '89', '89', 21, 'supervisor', 'inception', 1, 1, '2015-09-29 08:36:28', '2015-10-04 21:43:57'),
(19, 'CASE-0019', '32', '89', '89', 22, 'supervisor', 'submission', 1, 1, '2015-09-29 09:32:05', '2015-09-30 01:51:31'),
(20, 'CASE-0020', '23', '23', '23', 23, 'advisor', 'inception', 1, 1, '2015-09-30 03:31:23', '2015-09-30 01:51:46'),
(21, 'CASE-0021', '32', '32', '32', 24, 'supervisor', 'inception', 1, 1, '2015-09-30 05:30:59', '2015-09-30 02:45:45'),
(22, 'CASE-0022', '54', '54', '54', 25, 'supervisor', 'inception', 1, 1, '2015-09-30 06:50:01', '2015-09-30 02:45:45'),
(23, 'CASE-0023', '53', '53', '53', 26, 'supervisor', 'submission', 1, 2, '2015-09-30 06:50:24', '2015-09-29 23:01:58'),
(24, 'CASE-0024', '51', '51', '51', 27, 'advisor', 'inception', 1, 2, '2015-09-30 06:50:58', '2015-09-29 23:02:03'),
(25, 'CASE-0025', '79', '7', '979', 28, 'advisor', 'inception', 1, 1, '2015-09-30 06:51:37', '2015-10-01 02:08:57'),
(26, 'CASE-0026', '21', '12', '12', 29, 'supervisor', 'inception', 1, 2, '2015-09-30 06:56:39', '2015-09-29 23:02:08'),
(27, 'CASE-0027', '59', '59', '59', 30, 'supervisor', 'inception', 1, 1, '2015-09-30 09:42:28', '2015-09-30 02:45:45'),
(28, 'CASE-0028', '48', '48', '48', 31, 'supervisor', 'submission', 1, 1, '2015-09-30 09:43:06', '2015-09-30 01:43:06'),
(29, 'CASE-0029', '15', '15', '15', 32, 'supervisor', 'submission', 1, 1, '2015-09-30 09:43:42', '2015-09-30 01:43:42'),
(30, 'CASE-0030', '12', '12', '12', 461, 'advisor', 'inception', 1, 1, '2015-10-02 10:38:44', '2015-10-04 21:43:44'),
(31, 'CASE-0031', '12', '12', '12', 462, 'advisor', 'inception', 1, 1, '2015-10-02 10:40:15', '2015-10-04 21:43:44'),
(32, 'CASE-0032', '12', '12', '12', 1, 'advisor', 'submission', 1, 1, '2015-10-02 10:48:40', '2015-10-02 02:48:40'),
(33, 'CASE-0033', '12', '12', '12', 2, 'advisor', 'inception', 1, 1, '2015-10-02 10:50:02', '2015-10-04 21:43:44'),
(34, 'CASE-0034', '13', '13', '13', 3, 'advisor', 'submission', 1, 1, '2015-10-05 03:14:28', '2015-10-04 19:14:28'),
(35, 'CASE-0035', '15', '15', '15', 4, 'advisor', 'inception', 1, 1, '2015-10-05 03:20:29', '2015-10-04 21:43:44'),
(36, 'CASE-0036', '16', '16', '16', 5, 'advisor', 'submission', 1, 1, '2015-10-05 03:22:32', '2015-10-04 19:22:32'),
(37, 'CASE-0037', '18', '18', '18', 6, 'advisor', 'submission', 1, 1, '2015-10-05 03:26:49', '2015-10-04 19:26:49'),
(38, 'CASE-0038', '79', '79', '79', 7, 'advisor', 'submission', 1, 1, '2015-10-05 03:31:29', '2015-10-04 19:31:29'),
(39, 'CASE-0039', '16', '16', '16', 8, 'advisor', 'submission', 1, 1, '2015-10-05 05:43:16', '2015-10-04 21:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_products`
--

CREATE TABLE IF NOT EXISTS `legacy_products` (
`id` int(11) NOT NULL,
  `code` varchar(1000) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `rate` varchar(1000) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_products`
--

INSERT INTO `legacy_products` (`id`, `code`, `provider_id`, `name`, `description`, `rate`, `status`, `created_at`, `updated_at`) VALUES
(1, 'save', 2, 'gdfg', 'gfdgd', '645', 2, '2015-09-18 10:48:49', '2015-09-20 21:45:57'),
(2, 'fdg654', 1, 'rtter', 'fhfgh', '54645', 2, '2015-09-18 10:51:01', '2015-09-20 21:53:31'),
(3, 'fs34', 7, 'lean', 'rwefs', '43543', 2, '2015-09-18 10:56:02', '2015-09-20 21:53:35'),
(4, 'hfghf', 4, 'hfgh', 'hfgh', '6546', 2, '2015-09-18 10:56:37', '2015-09-20 21:53:31'),
(5, '0001', 12, 'first', 'first', '450', 1, '2015-09-21 02:42:50', '2015-09-21 22:59:56'),
(6, '0002', 11, 'second', 'second product', '67', 1, '2015-09-21 05:54:17', '2015-09-21 19:18:24'),
(7, '00031dsf', 12, 'thirds', 'thirds', '23', 1, '2015-09-21 05:54:40', '2015-09-22 21:31:46'),
(8, '1234', 9, '1fd', '1234', '12', 1, '2015-09-22 03:09:10', '2015-09-21 19:09:10'),
(9, 'gfd', 2, 'gfd', 'gdf', '423', 1, '2015-09-22 06:54:13', '2015-09-21 22:54:13'),
(10, 'fsdf', 2, 'fdsf', 'fd', '43', 1, '2015-09-22 06:57:21', '2015-09-21 22:57:21'),
(11, 'tert', 4, 'ter', 'tert', '65', 2, '2015-09-22 06:58:28', '2015-09-22 00:17:34'),
(12, 'ytr', 2, 'ytr', 'yrt', '76', 1, '2015-09-22 07:01:35', '2015-09-21 23:01:35'),
(13, 'ytr', 2, 'ytr', 'yrt', '76', 1, '2015-09-22 07:30:51', '2015-09-21 23:30:51'),
(14, 'parod-0019', 2, 'Prouck', '125678', '123', 1, '2015-09-22 07:32:21', '2015-09-21 23:32:21'),
(15, 'asdasd', 2, 'asdsad', 'dasads', '12', 2, '2015-09-22 07:35:31', '2015-09-22 00:17:34'),
(16, 'sadasd', 3, 'asdsad', 'sadsad', '123', 1, '2015-09-22 07:38:21', '2015-09-21 23:38:21'),
(17, '543', 4, 'rt', 'ete', '45', 2, '2015-09-22 07:42:54', '2015-09-22 00:17:34'),
(18, 'dassad', 2, 'asdasd', 'sadsad', '23', 2, '2015-09-22 07:50:32', '2015-09-22 00:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_product_rates`
--

CREATE TABLE IF NOT EXISTS `legacy_product_rates` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `prev_rate` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_product_rates`
--

INSERT INTO `legacy_product_rates` (`id`, `product_id`, `prev_rate`, `rate`, `created_at`, `updated_at`) VALUES
(1, 8, 12, 12, '2015-09-22 03:09:10', '2015-09-21 19:09:10'),
(2, 5, 15, 15, '2015-09-22 03:17:48', '2015-09-21 19:17:48'),
(3, 6, 25, 25, '2015-09-22 03:18:05', '2015-09-21 19:18:05'),
(4, 6, 67, 67, '2015-09-22 03:18:24', '2015-09-21 19:18:24'),
(5, 5, 20, 20, '2015-09-22 03:21:47', '2015-09-21 19:21:47'),
(6, 5, 15, 45, '2015-09-22 03:24:44', '2015-09-21 19:24:44'),
(7, 5, 45, 450, '2015-09-22 03:25:42', '2015-09-21 19:25:42'),
(8, 7, 100, 100, '2015-09-22 00:00:00', '2015-09-22 03:28:46'),
(9, 7, 100, 23, '2015-09-22 03:28:54', '2015-09-21 19:28:54'),
(10, 6, 67, 67, '2015-09-22 00:00:00', '2015-09-22 03:29:24'),
(11, 7, 450, 2, '2015-09-22 05:47:42', '2015-09-21 21:47:42'),
(12, 5, 450, 100, '2015-09-22 05:47:59', '2015-09-21 21:47:59'),
(13, 9, 423, 423, '2015-09-22 06:54:13', '2015-09-21 22:54:13'),
(14, 10, 43, 43, '2015-09-22 06:57:21', '2015-09-21 22:57:21'),
(15, 11, 65, 65, '2015-09-22 06:58:28', '2015-09-21 22:58:28'),
(16, 12, 76, 76, '2015-09-22 07:01:35', '2015-09-21 23:01:35'),
(17, 13, 76, 76, '2015-09-22 07:30:51', '2015-09-21 23:30:51'),
(18, 14, 123, 123, '2015-09-22 07:32:21', '2015-09-21 23:32:21'),
(19, 15, 12, 12, '2015-09-22 07:35:31', '2015-09-21 23:35:31'),
(20, 16, 123, 123, '2015-09-22 07:38:21', '2015-09-21 23:38:21'),
(21, 17, 45, 45, '2015-09-22 07:42:54', '2015-09-21 23:42:54'),
(22, 18, 23, 23, '2015-09-22 07:50:32', '2015-09-21 23:50:32'),
(23, 5, 100, 1, '2015-09-22 08:03:58', '2015-09-22 00:03:58'),
(24, 10, 43, 0, '2015-09-22 10:20:45', '2015-09-22 02:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_providers`
--

CREATE TABLE IF NOT EXISTS `legacy_providers` (
`id` int(11) NOT NULL,
  `code` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_providers`
--

INSERT INTO `legacy_providers` (`id`, `code`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'gdfg', 'gfdg', 'gdfgf', 2, '2015-09-18 06:23:07', '2015-09-17 23:26:48'),
(2, 'gdfg7657', 'gdfg765765', 'gdfg765765', 0, '2015-09-18 06:24:17', '2015-10-01 23:34:05'),
(3, 'gdfg6546jkbjk', 'gfdggdfgdf', 'gdfgf6456', 2, '2015-09-18 06:38:12', '2015-09-17 23:26:03'),
(4, 'gdfgll', 'gdfg', 'gdfg', 2, '2015-09-18 07:19:42', '2015-09-17 23:23:36'),
(5, 'gdfgll;;', 'gdfg', 'gdfg', 2, '2015-09-18 07:19:51', '2015-09-17 23:21:32'),
(6, 'gdfgad', 'gdfg', 'gdfg', 2, '2015-09-18 07:20:09', '2015-09-17 23:21:32'),
(7, 'dsad', 'dasd', 'dsadas', 2, '2015-09-18 07:26:55', '2015-09-20 21:52:24'),
(8, 'fsdf', 'fdsf', 'fsdf', 2, '2015-09-18 08:31:51', '2015-09-20 21:52:24'),
(9, 'gdfgd', 'gfgdf', 'gdfgdf', 2, '2015-09-18 10:06:46', '2015-09-20 21:52:24'),
(10, '0001', 'Provider 1', 'first provider', 0, '2015-09-21 02:42:27', '2015-10-04 21:43:44'),
(11, '0002', 'Provider 2', 'second provider', 1, '2015-09-21 05:52:56', '2015-10-01 23:33:50'),
(12, '0003', 'Provider 3', 'third provider', 1, '2015-09-21 05:53:21', '2015-10-01 23:34:03'),
(13, 'Fourth', 'Provider 4', 'fourth', 1, '2015-09-22 06:54:48', '2015-09-22 02:30:41');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales`
--

CREATE TABLE IF NOT EXISTS `legacy_sales` (
`id` int(11) NOT NULL,
  `sales_id` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `designation_id` int(11) NOT NULL,
  `bonding_rate` int(3) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_sales`
--

INSERT INTO `legacy_sales` (`id`, `sales_id`, `designation_id`, `bonding_rate`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'SALES-0001', 6, 10, 8, '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 'SALES-0002', 6, 10, 9, '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 'SALES-0003', 5, 10, 10, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(4, 'SALES-0004', 5, 10, 11, '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 'SALES-0005', 3, 10, 12, '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 'SALES-0006', 3, 10, 13, '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 'SALES-0007', 4, 10, 14, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(8, 'SALES-0008', 4, 10, 15, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 'SALES-0009', 4, 10, 16, '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 'SALES-0010', 4, 10, 17, '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 'SALES-0011', 2, 10, 18, '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 'SALES-0012', 2, 10, 19, '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 'SALES-0013', 2, 10, 20, '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 'SALES-0014', 2, 10, 21, '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 'SALES-0015', 1, 10, 22, '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 'SALES-0016', 1, 10, 23, '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 'SALES-0017', 1, 10, 24, '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 'SALES-0018', 1, 10, 25, '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 'SALES-0019', 1, 10, 26, '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 'SALES-0020', 1, 10, 27, '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 'SALES-0021', 1, 10, 28, '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 'SALES-0022', 1, 10, 29, '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 'SALES-0023', 1, 10, 30, '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 'SALES-0024', 1, 10, 31, '2015-09-18 08:43:13', '2015-09-18 00:43:13'),
(25, 'SALES-0025', 1, 10, 32, '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 'SALES-0026', 1, 10, 33, '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 'SALES-0027', 1, 10, 34, '2015-09-18 08:46:52', '2015-09-18 00:46:52'),
(28, 'SALES-0028', 2, 10, 36, '2015-09-23 02:34:03', '2015-09-22 18:34:03'),
(29, 'SALES-0029', 1, 10, 37, '2015-09-23 02:40:51', '2015-09-22 18:40:51'),
(30, 'SALES-0030', 2, 10, 38, '2015-09-23 02:43:09', '2015-09-22 18:43:09'),
(31, 'SALES-0031', 5, 10, 39, '2015-10-02 07:00:50', '2015-10-01 23:00:50'),
(32, 'SALES-0032', 6, 10, 40, '2015-10-02 07:01:49', '2015-10-01 23:01:49'),
(33, 'SALES-0033', 3, 10, 41, '2015-10-02 07:02:43', '2015-10-01 23:02:43'),
(34, 'SALES-0034', 1, 10, 42, '2015-10-02 07:06:21', '2015-10-01 23:06:21'),
(35, 'SALES-0035', 1, 10, 43, '2015-10-02 07:08:02', '2015-10-01 23:08:02'),
(36, 'SALES-0036', 2, 10, 44, '2015-10-02 07:11:33', '2015-10-01 23:11:33'),
(37, 'SALES-0037', 1, 10, 45, '2015-10-02 07:12:20', '2015-10-01 23:12:20'),
(38, 'SALES-0038', 1, 10, 46, '2015-10-02 07:12:59', '2015-10-01 23:12:59');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_bondings`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_bondings` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `bonding_rate` int(11) NOT NULL,
  `prev_bonding_rate` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `effective_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_bondings`
--

INSERT INTO `legacy_sales_bondings` (`id`, `sales_id`, `bonding_rate`, `prev_bonding_rate`, `edit_id`, `effective_date`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 10, 1, '2015-09-18 08:05:06', '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 2, 10, 10, 1, '2015-09-18 08:05:44', '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 3, 10, 10, 1, '2015-09-18 08:06:35', '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(4, 4, 10, 10, 1, '2015-09-18 08:07:39', '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 5, 10, 10, 1, '2015-09-18 08:08:24', '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 6, 10, 10, 1, '2015-09-18 08:09:01', '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 7, 10, 10, 1, '2015-09-18 08:13:58', '2015-09-18 08:13:59', '2015-09-18 00:13:59'),
(8, 8, 10, 10, 1, '2015-09-18 08:15:37', '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 9, 10, 10, 1, '2015-09-18 08:16:39', '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 10, 10, 10, 1, '2015-09-18 08:17:23', '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 11, 10, 10, 1, '2015-09-18 08:18:31', '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 12, 10, 10, 1, '2015-09-18 08:21:04', '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 13, 10, 10, 1, '2015-09-18 08:21:52', '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 14, 10, 10, 1, '2015-09-18 08:22:20', '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 15, 10, 10, 1, '2015-09-18 08:28:02', '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 16, 10, 10, 1, '2015-09-18 08:28:48', '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 17, 10, 10, 1, '2015-09-18 08:29:41', '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 18, 10, 10, 1, '2015-09-18 08:30:26', '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 19, 10, 10, 1, '2015-09-18 08:31:23', '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 20, 10, 10, 1, '2015-09-18 08:33:47', '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 21, 10, 10, 1, '2015-09-18 08:36:30', '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 22, 10, 10, 1, '2015-09-18 08:38:43', '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 23, 10, 10, 1, '2015-09-18 08:39:32', '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 24, 10, 10, 1, '2015-09-18 08:43:13', '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(25, 25, 10, 10, 1, '2015-09-18 08:44:25', '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 26, 10, 10, 1, '2015-09-18 08:45:13', '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 27, 10, 10, 1, '2015-09-18 08:46:52', '2015-09-18 08:46:53', '2015-09-18 00:46:53'),
(28, 28, 10, 10, 1, '2015-09-23 02:34:03', '2015-09-23 02:34:03', '2015-09-22 18:34:03'),
(29, 29, 10, 10, 1, '2015-09-23 02:40:51', '2015-09-23 02:40:51', '2015-09-22 18:40:51'),
(30, 30, 10, 10, 1, '2015-09-23 02:43:09', '2015-09-23 02:43:09', '2015-09-22 18:43:09'),
(31, 31, 10, 10, 1, '2015-10-02 07:00:50', '2015-10-02 07:00:50', '2015-10-01 23:00:50'),
(32, 32, 10, 10, 1, '2015-10-02 07:01:49', '2015-10-02 07:01:49', '2015-10-01 23:01:49'),
(33, 33, 10, 10, 1, '2015-10-02 07:02:43', '2015-10-02 07:02:43', '2015-10-01 23:02:43'),
(34, 34, 10, 10, 1, '2015-10-02 07:06:21', '2015-10-02 07:06:21', '2015-10-01 23:06:21'),
(35, 35, 10, 10, 1, '2015-10-02 07:08:02', '2015-10-02 07:08:02', '2015-10-01 23:08:02'),
(36, 36, 10, 10, 1, '2015-10-02 07:11:33', '2015-10-02 07:11:33', '2015-10-01 23:11:33'),
(37, 37, 10, 10, 1, '2015-10-02 07:12:20', '2015-10-02 07:12:20', '2015-10-01 23:12:20'),
(38, 38, 10, 10, 1, '2015-10-02 07:12:59', '2015-10-02 07:13:00', '2015-10-01 23:13:00'),
(39, 38, 10, 10, 1, '2015-09-30 06:24:37', '2015-10-05 06:24:37', '2015-10-04 22:24:37'),
(40, 38, 20, 10, 1, '2015-10-07 06:37:40', '2015-10-05 06:37:40', '2015-10-04 22:37:40'),
(41, 38, 20, 20, 1, '2015-10-09 07:38:26', '2015-10-05 07:38:26', '2015-10-04 23:38:26'),
(42, 15, 20, 10, 1, '2015-10-08 09:15:49', '2015-10-05 09:15:49', '2015-10-05 01:15:49');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_designations`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_designations` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `prev_designation_id` int(11) DEFAULT NULL,
  `edit_id` int(11) NOT NULL,
  `effective_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_designations`
--

INSERT INTO `legacy_sales_designations` (`id`, `sales_id`, `designation_id`, `prev_designation_id`, `edit_id`, `effective_date`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 6, 1, '2015-09-18 08:05:06', '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 2, 6, 6, 1, '2015-09-18 08:05:44', '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 3, 5, 5, 1, '2015-09-18 08:06:35', '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(4, 4, 5, 5, 1, '2015-09-18 08:07:39', '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 5, 3, 3, 1, '2015-09-18 08:08:24', '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 6, 3, 3, 1, '2015-09-18 08:09:01', '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 7, 4, 4, 1, '2015-09-18 08:13:58', '2015-09-18 08:13:59', '2015-09-18 00:13:59'),
(8, 8, 4, 4, 1, '2015-09-18 08:15:37', '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 9, 4, 4, 1, '2015-09-18 08:16:39', '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 10, 4, 4, 1, '2015-09-18 08:17:23', '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 11, 2, 2, 1, '2015-09-18 08:18:31', '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 12, 2, 2, 1, '2015-09-18 08:21:04', '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 13, 2, 2, 1, '2015-09-18 08:21:52', '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 14, 2, 2, 1, '2015-09-18 08:22:20', '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 15, 1, 1, 1, '2015-09-18 08:28:02', '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 16, 1, 1, 1, '2015-09-18 08:28:48', '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 17, 1, 1, 1, '2015-09-18 08:29:41', '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 18, 1, 1, 1, '2015-09-18 08:30:26', '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 19, 1, 1, 1, '2015-09-18 08:31:23', '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 20, 1, 1, 1, '2015-09-18 08:33:47', '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 21, 1, 1, 1, '2015-09-18 08:36:30', '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 22, 1, 1, 1, '2015-09-18 08:38:43', '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 23, 1, 1, 1, '2015-09-18 08:39:32', '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 24, 1, 1, 1, '2015-09-18 08:43:13', '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(25, 25, 1, 1, 1, '2015-09-18 08:44:25', '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 26, 1, 1, 1, '2015-09-18 08:45:13', '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 27, 1, 1, 1, '2015-09-18 08:46:52', '2015-09-18 08:46:53', '2015-09-18 00:46:53'),
(28, 28, 2, 2, 1, '2015-09-23 02:34:03', '2015-09-23 02:34:03', '2015-09-22 18:34:03'),
(29, 29, 1, 1, 1, '2015-09-23 02:40:51', '2015-09-23 02:40:51', '2015-09-22 18:40:51'),
(30, 30, 2, 2, 1, '2015-09-23 02:43:09', '2015-09-23 02:43:09', '2015-09-22 18:43:09'),
(31, 31, 5, 5, 1, '2015-10-02 07:00:50', '2015-10-02 07:00:50', '2015-10-01 23:00:50'),
(32, 32, 6, 6, 1, '2015-10-02 07:01:49', '2015-10-02 07:01:49', '2015-10-01 23:01:49'),
(33, 33, 3, 3, 1, '2015-10-02 07:02:43', '2015-10-02 07:02:43', '2015-10-01 23:02:43'),
(34, 34, 1, 1, 1, '2015-10-02 07:06:21', '2015-10-02 07:06:21', '2015-10-01 23:06:21'),
(35, 35, 1, 1, 1, '2015-10-02 07:08:02', '2015-10-02 07:08:02', '2015-10-01 23:08:02'),
(36, 36, 2, 2, 1, '2015-10-02 07:11:33', '2015-10-02 07:11:33', '2015-10-01 23:11:33'),
(37, 37, 1, 1, 1, '2015-10-02 07:12:20', '2015-10-02 07:12:20', '2015-10-01 23:12:20'),
(38, 38, 1, 1, 1, '2015-10-02 07:12:59', '2015-10-02 07:13:00', '2015-10-01 23:13:00');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_no_teams`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_no_teams` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_no_teams`
--

INSERT INTO `legacy_sales_no_teams` (`id`, `sales_id`, `created_at`, `updated_at`) VALUES
(1, 4, '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(2, 6, '2015-09-18 08:09:01', '2015-09-18 08:10:48'),
(3, 10, '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(4, 14, '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(5, 27, '2015-09-18 08:46:52', '2015-09-18 00:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_partners`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_partners` (
`id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_partners`
--

INSERT INTO `legacy_sales_partners` (`id`, `partner_id`, `sales_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(2, 1, 7, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(3, 1, 11, '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(4, 1, 31, '2015-10-02 07:00:50', '2015-10-01 23:00:50'),
(5, 2, 33, '2015-10-02 07:02:43', '2015-10-01 23:02:43'),
(6, 32, 36, '2015-10-02 07:11:33', '2015-10-01 23:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_supervisors`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_supervisors` (
`id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_supervisors`
--

INSERT INTO `legacy_sales_supervisors` (`id`, `supervisor_id`, `sales_id`, `team_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(2, 1, 5, 1, '2015-09-18 00:00:00', '2015-09-18 08:11:45'),
(3, 1, 7, 1, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(4, 3, 8, 1, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(5, 4, 9, NULL, '2015-09-18 08:16:39', '2015-09-21 09:59:17'),
(6, 1, 11, 1, '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(7, 5, 12, 1, '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(8, 6, 13, NULL, '2015-09-18 08:21:52', '2015-09-21 09:59:40'),
(9, 5, 15, 1, '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(10, 3, 16, 1, '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(11, 4, 17, NULL, '2015-09-18 08:29:41', '2015-09-21 10:00:05'),
(12, 6, 18, NULL, '2015-09-18 08:30:26', '2015-09-21 09:59:43'),
(13, 7, 19, 1, '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(14, 10, 20, NULL, '2015-09-18 08:33:47', '2015-09-21 10:00:30'),
(15, 8, 21, 1, '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(16, 9, 22, 1, '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(17, 11, 23, 1, '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(18, 14, 24, NULL, '2015-09-18 08:43:14', '2015-09-21 10:00:33'),
(19, 12, 25, 1, '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(20, 13, 26, 1, '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(21, 5, 28, 1, '2015-09-23 02:34:03', '2015-09-22 18:34:03'),
(22, 17, 29, NULL, '2015-09-23 02:40:51', '2015-09-22 18:40:51'),
(23, 6, 30, NULL, '2015-09-23 02:43:09', '2015-09-22 18:43:09'),
(24, 1, 31, NULL, '2015-10-02 07:00:50', '2015-10-01 23:00:50'),
(25, 2, 33, NULL, '2015-10-02 07:02:43', '2015-10-01 23:02:43'),
(26, 31, 34, NULL, '2015-10-02 07:06:21', '2015-10-01 23:06:21'),
(27, 31, 35, NULL, '2015-10-02 07:08:02', '2015-10-01 23:08:02'),
(28, 32, 36, NULL, '2015-10-02 07:11:33', '2015-10-01 23:11:33'),
(29, 33, 37, NULL, '2015-10-02 07:12:20', '2015-10-01 23:12:20'),
(30, 31, 38, NULL, '2015-10-02 07:13:00', '2015-10-01 23:13:00');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_teams`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_teams` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `prev_supervisor_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_teams`
--

INSERT INTO `legacy_sales_teams` (`id`, `sales_id`, `prev_supervisor_id`, `supervisor_id`, `edit_id`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, 1, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(2, 5, 1, 1, 1, '2015-09-18 00:00:00', '2015-09-18 08:11:56'),
(3, 7, 1, 1, 1, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(4, 8, 3, 3, 1, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(5, 9, 4, 4, 1, '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(6, 11, 1, 1, 1, '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(7, 12, 5, 5, 1, '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(8, 13, 6, 6, 1, '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(9, 15, 5, 5, 1, '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(10, 16, 3, 3, 1, '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(11, 17, 4, 4, 1, '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(12, 18, 6, 6, 1, '2015-09-18 08:30:26', '2015-10-05 09:09:56'),
(13, 19, 7, 7, 1, '2015-09-18 08:31:23', '2015-10-05 09:10:03'),
(66, 20, 5, 17, 1, '2015-10-05 08:43:54', '2015-10-05 09:10:07'),
(67, 21, 5, 16, 1, '2015-10-05 08:44:57', '2015-10-05 09:10:10'),
(68, 22, 5, 15, 1, '2015-10-05 08:57:04', '2015-10-05 09:10:13'),
(69, 23, 5, 18, 1, '2015-10-05 08:57:08', '2015-10-05 09:10:16'),
(70, 24, 5, 16, 1, '2015-10-05 09:05:37', '2015-10-05 09:10:23');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_teams`
--

CREATE TABLE IF NOT EXISTS `legacy_teams` (
`id` int(11) NOT NULL,
  `team_code` varchar(1000) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_teams`
--

INSERT INTO `legacy_teams` (`id`, `team_code`, `sales_id`, `created_at`, `updated_at`) VALUES
(1, 'TEAM-0001', 1, '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 'TEAM-0002', 2, '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 'TEAM-0003', 32, '2015-10-02 07:01:49', '2015-10-01 23:01:49');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_upload_feeds`
--

CREATE TABLE IF NOT EXISTS `legacy_upload_feeds` (
`id` int(11) NOT NULL,
  `file_name` varchar(1000) NOT NULL,
  `orig_name` varchar(1000) NOT NULL,
  `feed_type` varchar(1000) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_upload_feeds`
--

INSERT INTO `legacy_upload_feeds` (`id`, `file_name`, `orig_name`, `feed_type`, `provider_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '1.csv', '1.xlsx', 'Provider Data Feed', 10, 1, '2015-10-02 09:26:57', '2015-10-02 01:37:37'),
(3, '3.csv', '3.xlsx', 'Provider Data Feed', 11, 1, '2015-10-05 06:21:52', '2015-10-04 22:23:29'),
(4, '4.csv', '4.xlsx', 'Provider Data Feed', 11, 1, '2015-10-05 06:25:40', '2015-10-04 22:27:12'),
(7, '7.csv', '7.csv', 'Provider Data Feed', 11, 1, '2015-10-05 06:29:48', '2015-10-04 22:31:46'),
(9, '9.csv', '9.xlsx', 'Provider Data Feed', 11, 1, '2015-10-05 06:35:31', '2015-10-04 22:37:33'),
(10, '10.csv', '10.xlsx', 'Provider Data Feed', 13, 1, '2015-10-05 06:38:08', '2015-10-04 22:39:41'),
(11, '11.csv', '11.csv', 'Provider Data Feed', 11, 1, '2015-10-05 06:46:00', '2015-10-04 22:58:18'),
(12, '12.csv', '12.csv', 'Provider Data Feed', 11, 1, '2015-10-05 06:59:08', '2015-10-04 23:04:12'),
(13, '13.csv', '13.csv', 'Provider Data Feed', 11, 0, '2015-10-05 07:26:07', '2015-10-04 23:26:07');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_users`
--

CREATE TABLE IF NOT EXISTS `legacy_users` (
`id` int(11) unsigned NOT NULL,
  `system_id` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `photo` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `name` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `email` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `mobile` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `username` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `password` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_users`
--

INSERT INTO `legacy_users` (`id`, `system_id`, `code`, `photo`, `name`, `email`, `mobile`, `username`, `password`, `usertype_id`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'USER-0001', 'CEO-0001', '1.jpg', 'CEO', 'ceo@legacy.com', '123456789', 'ceo@legacy.com', '$2y$10$MW18a47sC5Yoqp6cVme.4.I0XKvKYpNAa2dM87K3Sa1lvw2W7ZwL.', 1, '0YBPkhRaKh8X7DHy9Md9jmfw7wwRIXR7AMjOg9vNl7BomNxjeMdEQhf1Ka65', 1, '2015-09-18 00:00:00', '2015-10-02 01:28:15'),
(2, 'USER-0002', 'IT-0001', '2.jpg', 'IT Users', 'itusers@legacy.com', '123456789', 'itusers@legacy.com', '$2y$10$EKLdiyYypLhQpJqFVbCZT.2Iyxwjj/c6Jc3qD7eboSuKUPiCHlidO', 2, NULL, 1, '2015-09-18 03:04:36', '2015-09-17 19:04:37'),
(3, 'USER-0003', 'ADMIN-0001', '3.jpg', 'Admin', 'admin@legacy.com', '123456789', 'admin@legacy.com', '$2y$10$G4Z7/HcFWx1LeqVar0QC3eVW3G5Sfq7I9oFmIbcFDMdBmZlPUm9O.', 3, NULL, 1, '2015-09-18 03:05:43', '2015-09-17 19:05:43'),
(4, 'USER-0004', 'ADMIN-0002', '4.jpg', 'Admin Assistant', 'admin_assistant@legacy.com', '123456789', 'admin_assistant@legacy.com', '$2y$10$.CJMtHlbwcvdiL3IT9Ot/ulbIo7De6Z6pZaNXbRJiVEkZXg8pSy86', 4, NULL, 1, '2015-09-18 03:06:54', '2015-09-17 19:06:55'),
(5, 'USER-0005', 'ACC-0001', '5.jpg', 'Accounting', 'accounting@legacy.com', '123456789', 'accounting@legacy.com', '$2y$10$wHiFDt8sAopMt5sEx.1IGOPTpgay4FQRILCu/xX23Eb/OKbk6MVT2', 5, NULL, 1, '2015-09-18 03:07:49', '2015-09-17 19:07:56'),
(6, 'USER-0006', 'ACC-0002', '6.jpg', 'Accounting Assistant', 'acc_assistant@legacy.com', '123456789', 'acc_assistant@legacy.com', '$2y$10$50jEEFTCKlv66IvNEpycq.kIXI8I0bdgNY.GRt4OjvXJYMskXZpCy', 6, NULL, 1, '2015-09-18 03:10:07', '2015-09-17 19:10:09'),
(7, 'USER-0007', 'SALES-0001', '7.jpg', 'Sales Assistant', 'sales_assistant@legacy.com', '123456789', 'sales_assistant@legacy.com', '$2y$10$.r4lWIhPbHnqwGXFOkyFsOIvPZsHDAUJ8mSK9j2U4iqrf.ftZQ.OW', 7, NULL, 1, '2015-09-18 03:12:25', '2015-09-22 21:42:40'),
(8, NULL, 'AGENT-001', '8.jpg', 'Partner', 'partner@legacy.com', '123456789', 'partner@legacy.com', '$2y$10$9mM50198HSAtVCD65rusVe1ScKhnVpPUGUkAfbPjgQWINgan4U8Fa', 8, NULL, 1, '2015-09-18 08:05:05', '2015-09-18 00:05:06'),
(9, NULL, 'AGENT-002', '9.jpg', 'Partner', 'partner@legacy.co', '123456789', 'partner@legacy.co', '$2y$10$JjyPjL2nToyhIhT8PAx2FOhFDX9YN0JYd7Pwwo6WkzN.hbDno8YJ2', 8, NULL, 1, '2015-09-18 08:05:43', '2015-09-18 00:05:44'),
(10, NULL, 'AGENT-003', '10.jpg', 'Director', 'director@legacy.com', '123456789', 'director@legacy.com', '$2y$10$FktQHCwB/TR8NEh4TFe3QuP/vsWjpuvNjyUvO9vRnO45UAKM2LDIG', 8, NULL, 1, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(11, NULL, 'AGENT-004', '11.jpg', 'No Director', 'no_director@legacy.com', '123456789', 'no_director@legacy.com', '$2y$10$55Jlmjr/bYmmbYixJO4uzuX2722Th/ylTZ4GUwynMy5.ep1szB7Ki', 8, NULL, 1, '2015-09-18 08:07:38', '2015-09-18 00:07:39'),
(12, NULL, 'AGENT-005', '12.jpg', 'Manager', 'manager@legacy.com', '123456789', 'manager@legacy.com', '$2y$10$xTXw.PZd3w7DGsnliYnql.UJpIhJMem0QGBBKxmzfT0BqlGwttLqG', 8, NULL, 1, '2015-09-18 08:08:23', '2015-09-18 00:08:24'),
(13, NULL, 'AGENT-006', '13.jpg', 'No Manager', 'no_manager@legacy.com', '123456789', 'no_manager@legacy.com', '$2y$10$omOlEKq2O2catgziIh1rXum/wo63NKzZodfIJ46jPUrwzcQFBc1qq', 8, NULL, 1, '2015-09-18 08:09:00', '2015-09-18 00:09:01'),
(14, NULL, 'AGENT-007', '14.jpg', 'Assistant Director', 'assistant_director@legacy.com', '123456789', 'assistant_director@legacy.com', '$2y$10$L6pW/ApUqwTu1ngNGa7vke4gvjTnZqFe7UTV0Izow5XbR/6yau1r2', 8, NULL, 1, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(15, NULL, 'AGENT-008', '15.jpg', 'Dir Assistant Director', 'dir_assistant_director@legacy.com', '123456789', 'dir_assistant_director@legacy.com', '$2y$10$ltphYj3aOX12DYj0meQbK.yA9a7z2ppZzoku6H5LVYj4W5xWYnUoi', 8, NULL, 1, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(16, NULL, 'AGENT-009', '16.jpg', 'NoDir Assistant Director', 'nodir_assistant_director@legacy.com', '123456789', 'nodir_assistant_director@legacy.com', '$2y$10$6ng5nCBxa6BqG9LbWJI/6OP7MBlCi.es8NT6TleG4nmiVENX2j1d.', 8, NULL, 1, '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(17, NULL, 'AGENT-010', '17.jpg', 'No Assistant Director', 'no_assistant_director@legacy.com', '123456789', 'no_assistant_director@legacy.com', '$2y$10$iCpjBUPIMkJFq2x0NHCXN.IXLcuxOSb0BK1..7y8239TIOur1BUT2', 8, NULL, 1, '2015-09-18 08:17:22', '2015-09-18 00:17:23'),
(18, NULL, 'AGENT-011', '18.jpg', 'Assistant Manager', 'assistant_manager@legacy.com', '123456789', 'assistant_manager@legacy.com', '$2y$10$2mK5W24Jg/yieshsmPq0r.pK8Yzz1Q8YSKwxBNSlOzuin7.1dt.Ni', 8, NULL, 1, '2015-09-18 08:18:30', '2015-09-18 00:18:31'),
(19, NULL, 'AGENT-012', '19.jpg', 'Man Assistant Manager', 'man_assistant_manager@legacy.com', '123456789', 'man_assistant_manager@legacy.com', '$2y$10$mbw73qKsmPW2FmqxmDsSVeGWZu1mrFCgwKhbxnd1vUXydjaXrGXNi', 8, NULL, 1, '2015-09-18 08:21:03', '2015-09-18 00:21:04'),
(20, NULL, 'AGENT-013', '20.jpg', 'NoMan Assistant Manager', 'noman_assistant_manager@legacy.com', '123456789', 'noman_assistant_manager@legacy.com', '$2y$10$NUK/l06IM/KkOcE.gTtLduROgBQmUzgdOGSXXP.x4f3WyvKgIC2fS', 8, NULL, 1, '2015-09-18 08:21:51', '2015-09-18 00:21:52'),
(21, NULL, 'AGENT-014', '21.jpg', 'No Assistant Manager', 'no_assistant_manager@legacy.com', '123456789', 'no_assistant_manager@legacy.com', '$2y$10$vafhz3eRcXkyontKtSCxQ.QMr0uEB/j5g2JDLmuIjm3fA.GLIIJ36', 8, NULL, 1, '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(22, NULL, 'AGENT-015', '22.jpg', 'Man Sales Agent', 'man_sales_agent@legacy.com', '123456789', 'man_sales_agent@legacy.com', '$2y$10$r4827Oy.79PU3rJXPxeRvOrQp6/eRsnhkE4.ksNsr2O8Kid.YHyBG', 8, NULL, 1, '2015-09-18 08:28:01', '2015-09-18 00:28:02'),
(23, NULL, 'AGENT-016', '23.jpg', 'Dir Sales Agent', 'dir_sales_agent@legacy.com', '123456789', 'dir_sales_agent@legacy.com', '$2y$10$/S6cZ1dkM6MOzVQgOasEbOvqrhCLPdvLuoY6/6ijTMe2MzxW0bu8.', 8, NULL, 1, '2015-09-18 08:28:47', '2015-09-18 00:28:48'),
(24, NULL, 'AGENT-017', '24.jpg', 'NoDir Sales Agent', 'nodir_sales_agent@legacy.com', '123456789', 'nodir_sales_agent@legacy.com', '$2y$10$bdEa/o78KPVqBpk2GIXroOrwQfxdqo4F2vnPn/HQekXTRGX4RZLK.', 8, NULL, 1, '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(25, NULL, 'AGENT-018', '25.jpg', 'NoMan Sales Agent', 'noman_sales_agent@legacy.com', '123456789', 'noman_sales_agent@legacy.com', '$2y$10$pTlIEFmYA0MQ2to8ToretesezBPnUImLuS8PWpngdaE1i0g2ekpay', 8, NULL, 1, '2015-09-18 08:30:25', '2015-09-18 00:30:26'),
(26, NULL, 'AGENT-019', '26.jpg', 'ADir Sales Agent', 'adir_sales_agent@legacy.com', '123456789', 'adir_sales_agent@legacy.com', '$2y$10$/HzMkV94taISoh8YO7D/q.gjOzRFe5ws8TFLgHmNrxgI6rHSH797K', 8, NULL, 1, '2015-09-18 08:31:22', '2015-09-18 00:31:23'),
(27, NULL, 'AGENT-020', '27.jpg', 'NoADir Sales Agent', 'noadir_sales_agent@legacy.com', '123456789', 'noadir_sales_agent@legacy.com', '$2y$10$YeoCbfyC2Rr68GsuPkKoxeQQXBrjKgLLLoXN0/DYftkFYdro/ysQe', 8, NULL, 1, '2015-09-18 08:33:46', '2015-09-18 00:33:47'),
(28, NULL, 'AGENT-021', '28.jpg', 'DirADir Sales Agent', 'diradir_sales_agent@legacy.com', '123456789', 'diradir_sales_agent@legacy.com', '$2y$10$r.Gjier4FHnVhAkH77n3be.au48X6E8dOxOclCc6oSXt8mT5yuaBC', 8, NULL, 1, '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(29, NULL, 'AGENT-022', '29.jpg', 'NoDirADir Sales Agent', 'nodiradir_sales_agent@legacy.com', '123456789', 'nodiradir_sales_agent@legacy.com', '$2y$10$YzmFRIxQJmwZxlocd4D1B.UCwUG8leHAmQRAIyQ55mZlpdHa6n3cm', 8, NULL, 1, '2015-09-18 08:38:42', '2015-09-18 00:38:43'),
(30, NULL, 'AGENT-023', '30.jpg', 'AMan Sales Agent', 'aman_sales_agent@legacy.com', '123456789', 'aman_sales_agent@legacy.com', '$2y$10$bhIZaooJ8J2lcAQlfSQS/ODzqpbXFA1NqN5lWTgAaP4.Sx7xnw/na', 8, NULL, 1, '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(31, NULL, 'AGENT-024', '31.jpg', 'NoAMan Sales Agent', 'noaman_sales_agent@legacy.com', '123456789', 'noaman_sales_agent@legacy.com', '$2y$10$NUfkIWD2qmtbBbocLijMAe8iII9/M..qhRLbAGNvWQyN186NVFn0y', 8, NULL, 1, '2015-09-18 08:43:13', '2015-09-18 00:43:13'),
(32, NULL, 'AGENT-025', '32.jpg', 'ManAMan Sales Agent', 'manaman_sales_agent@legacy.com', '123456789', 'manaman_sales_agent@legacy.com', '$2y$10$uzbxeCvwXqyBNEdGTS75JOE3DPa5MaEdQHydWsK5csXxSNIap5x8y', 8, NULL, 1, '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(33, NULL, 'AGENT-026', '33.jpg', 'NoManAMan Sales Agent', 'nomanaman_sales_agent@legacy.com', '123456789', 'nomanaman_sales_agent@legacy.com', '$2y$10$acP69bKWErDN8skHe5ubKuJgmL3eXVR2RXHeXCWq2fJE6Mi9kZyPy', 8, NULL, 1, '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(34, NULL, 'AGENT-027', '34.jpg', 'No Sales Agent', 'no_sales_agent@legacy.com', '123456789', 'no_sales_agent@legacy.com', '$2y$10$WYHkCmCXrMrIgFeQ4DiU8eFTEbzVcPvvNJNyM3/bKX871AyUeayMm', 8, NULL, 1, '2015-09-18 08:46:52', '2015-09-18 00:46:52'),
(35, 'USER-0035', 'SalesAssistant', '35.jpg', 'SalesAssistant', 'salesassistant@a.com', '123123213', 'salesassistant@a.com', '$2y$10$CFOuWK6gz9Az7ex6FEjCW.ni6fTWYBwFGGPdXrSkeFihF2LpCbZ36', 7, NULL, 1, '2015-09-21 06:57:48', '2015-09-20 22:57:48'),
(36, NULL, 'AGENT-022', '36.jpg', 'Sales Agent', 'sales.agent@gmail.com', '123456789', 'sales.agent@gmail.com', '$2y$10$JvrOiPhzJoTcqFAY8XXFCONQ8npVx89kOmXAHXtDPVDnk0MrfHMj.', 8, NULL, 1, '2015-09-23 02:34:03', '2015-09-22 18:34:03'),
(37, NULL, 'AGENT-023', '37.jpg', 'Sales Team', 'sales_team@gmail.com', '12345789', 'sales_team@gmail.com', '$2y$10$Nr3BdDAr9BN/euxS7GYLYOxwXZazd7VoO5f2ZNDNhGFg5FjfftkKa', 8, NULL, 1, '2015-09-23 02:40:50', '2015-09-22 18:40:50'),
(38, NULL, 'AGENT-024', '38.jpg', 'Agent TwoFour', 'agent_24@legacy.com', '1234567890', 'agent_24@legacy.com', '$2y$10$NeqdXC.WEGTadhlHCfKZ8uEseQhD.5Sd9taaGJnfapCI7naKDBndG', 8, NULL, 1, '2015-09-23 02:43:08', '2015-09-22 18:43:09'),
(39, NULL, '10001', '39.jpg', 'Lee', 'lee@legacy.com', '87654321', 'lee@legacy.com', '$2y$10$G6fF8q8eSBFRLdoidutr3u2weYnSG1s2MxTkae3glfi/VYhAAmX3O', 8, NULL, 1, '2015-10-02 07:00:49', '2015-10-01 23:00:50'),
(40, NULL, '10002', '40.jpg', 'Stewart', 'stewart@legacy.com', '87654321', 'stewart@legacy.com', '$2y$10$yt6z3sbvGsCNxI6lPYQusuyPCVLRrP49jWC9eNCJ1N87PSbJrJP3O', 8, NULL, 1, '2015-10-02 07:01:48', '2015-10-01 23:01:49'),
(41, NULL, '10003', '41.jpg', 'Chan', 'chan@legacy.com', '876543210', 'chan@legacy.com', '$2y$10$ywuhDFrMs6YNV7rWdYr94uGAyCWofKCTcptm2Gwe6/YvEua.PM0aK', 8, NULL, 1, '2015-10-02 07:02:42', '2015-10-01 23:02:43'),
(42, NULL, '88000125', '42.jpg', 'Lim', 'lim@legacy.com', '123456780', 'lim@legacy.com', '$2y$10$y720exQ09OtsbGaP.x6nQu.TDKmVt1eHHfQp8FpQlpI2sxeoP1MYW', 8, NULL, 1, '2015-10-02 07:06:21', '2015-10-04 19:14:10'),
(43, NULL, '88000126', '43.jpg', 'Chng', 'chng@legacy.com', '123456478', 'chng@legacy.com', '$2y$10$OmfRucU3Ab60mm9aSkoVWO2jrCF.ZunOAl0D1/S/FCid./EGTXcd6', 8, NULL, 1, '2015-10-02 07:08:02', '2015-10-04 19:13:52'),
(44, NULL, '88000127', '44.jpg', 'Ng', 'ng@legacy.com', '12346841', 'ng@legacy.com', '$2y$10$ucsIx7C3XORpVzzhQN75G.z4aGMtSEDytoHMuQZqD9y5Q46oki06m', 8, NULL, 1, '2015-10-02 07:11:33', '2015-10-04 19:13:37'),
(45, NULL, '88000128', '45.jpg', 'Sy', 'sy@legacy.com', '153465468', 'sy@legacy.com', '$2y$10$cTjw1jo1qm5DrUdUQUbWBeuxpOev2ku8s5PeUr.1WoPI.nNM7EzUa', 8, NULL, 1, '2015-10-02 07:12:20', '2015-10-04 19:13:13'),
(46, NULL, '88000133', '46.jpg', 'Chua', 'chua@legacy.com', '1564188', 'chua@legacy.com', '$2y$10$ESyyupe9c1ONsJsf3jjH4.AdqxviVp13Sq/yl.ZHVC37Cag3Tqh9y', 8, NULL, 1, '2015-10-02 07:12:59', '2015-10-04 19:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_usertypes`
--

CREATE TABLE IF NOT EXISTS `legacy_usertypes` (
`id` int(11) unsigned NOT NULL,
  `type_name` varchar(1000) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_usertypes`
--

INSERT INTO `legacy_usertypes` (`id`, `type_name`) VALUES
(1, 'CEO'),
(2, 'IT Users'),
(3, 'Admin Accounts'),
(4, 'Admin Assistant'),
(5, 'Accounting'),
(6, 'Accounting Assistant'),
(7, 'Sales Assistant'),
(8, 'Agent');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `legacy_assigned_sales`
--
ALTER TABLE `legacy_assigned_sales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_data_feeds`
--
ALTER TABLE `legacy_data_feeds`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_designations`
--
ALTER TABLE `legacy_designations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_introducers`
--
ALTER TABLE `legacy_introducers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_log_policies`
--
ALTER TABLE `legacy_log_policies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_log_products`
--
ALTER TABLE `legacy_log_products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_log_sales`
--
ALTER TABLE `legacy_log_sales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_log_users`
--
ALTER TABLE `legacy_log_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_permissions`
--
ALTER TABLE `legacy_permissions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_permission_modules`
--
ALTER TABLE `legacy_permission_modules`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_policies`
--
ALTER TABLE `legacy_policies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_pre_payrolls`
--
ALTER TABLE `legacy_pre_payrolls`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_production_cases`
--
ALTER TABLE `legacy_production_cases`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_production_incept`
--
ALTER TABLE `legacy_production_incept`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_products`
--
ALTER TABLE `legacy_products`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_product_rates`
--
ALTER TABLE `legacy_product_rates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_providers`
--
ALTER TABLE `legacy_providers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales`
--
ALTER TABLE `legacy_sales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_bondings`
--
ALTER TABLE `legacy_sales_bondings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_designations`
--
ALTER TABLE `legacy_sales_designations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_no_teams`
--
ALTER TABLE `legacy_sales_no_teams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_partners`
--
ALTER TABLE `legacy_sales_partners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_supervisors`
--
ALTER TABLE `legacy_sales_supervisors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_teams`
--
ALTER TABLE `legacy_sales_teams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_teams`
--
ALTER TABLE `legacy_teams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_upload_feeds`
--
ALTER TABLE `legacy_upload_feeds`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_users`
--
ALTER TABLE `legacy_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_usertypes`
--
ALTER TABLE `legacy_usertypes`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `legacy_assigned_sales`
--
ALTER TABLE `legacy_assigned_sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `legacy_data_feeds`
--
ALTER TABLE `legacy_data_feeds`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `legacy_designations`
--
ALTER TABLE `legacy_designations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `legacy_introducers`
--
ALTER TABLE `legacy_introducers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `legacy_log_policies`
--
ALTER TABLE `legacy_log_policies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `legacy_log_products`
--
ALTER TABLE `legacy_log_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `legacy_log_sales`
--
ALTER TABLE `legacy_log_sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `legacy_log_users`
--
ALTER TABLE `legacy_log_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `legacy_permissions`
--
ALTER TABLE `legacy_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `legacy_permission_modules`
--
ALTER TABLE `legacy_permission_modules`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `legacy_policies`
--
ALTER TABLE `legacy_policies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `legacy_pre_payrolls`
--
ALTER TABLE `legacy_pre_payrolls`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `legacy_production_cases`
--
ALTER TABLE `legacy_production_cases`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `legacy_production_incept`
--
ALTER TABLE `legacy_production_incept`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `legacy_products`
--
ALTER TABLE `legacy_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `legacy_product_rates`
--
ALTER TABLE `legacy_product_rates`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `legacy_providers`
--
ALTER TABLE `legacy_providers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `legacy_sales`
--
ALTER TABLE `legacy_sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `legacy_sales_bondings`
--
ALTER TABLE `legacy_sales_bondings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `legacy_sales_designations`
--
ALTER TABLE `legacy_sales_designations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `legacy_sales_no_teams`
--
ALTER TABLE `legacy_sales_no_teams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `legacy_sales_partners`
--
ALTER TABLE `legacy_sales_partners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `legacy_sales_supervisors`
--
ALTER TABLE `legacy_sales_supervisors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `legacy_sales_teams`
--
ALTER TABLE `legacy_sales_teams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `legacy_teams`
--
ALTER TABLE `legacy_teams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `legacy_upload_feeds`
--
ALTER TABLE `legacy_upload_feeds`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `legacy_users`
--
ALTER TABLE `legacy_users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `legacy_usertypes`
--
ALTER TABLE `legacy_usertypes`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
