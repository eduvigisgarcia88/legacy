-- phpMyAdmin SQL Dump
-- version 4.3.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 18, 2015 at 04:47 PM
-- Server version: 5.5.43-MariaDB
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `legacy`
--

-- --------------------------------------------------------

--
-- Table structure for table `legacy_assigned_sales`
--

CREATE TABLE IF NOT EXISTS `legacy_assigned_sales` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `assistant_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_designations`
--

CREATE TABLE IF NOT EXISTS `legacy_designations` (
`id` int(11) NOT NULL,
  `rank` tinyint(1) NOT NULL,
  `designation` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_designations`
--

INSERT INTO `legacy_designations` (`id`, `rank`, `designation`) VALUES
(1, 1, 'Sales Agent'),
(2, 2, 'Assistant Manager'),
(3, 3, 'Manager'),
(4, 2, 'Assistant Director'),
(5, 3, 'Director'),
(6, 4, 'Partner');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_log_sales`
--

CREATE TABLE IF NOT EXISTS `legacy_log_sales` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `activity` varchar(1000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_log_sales`
--

INSERT INTO `legacy_log_sales` (`id`, `user_id`, `edit_id`, `activity`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 'Sales User Created', '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 9, 1, 'Sales User Created', '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 10, 1, 'Sales User Created', '2015-09-18 08:06:36', '2015-09-18 00:06:36'),
(4, 11, 1, 'Sales User Created', '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 12, 1, 'Sales User Created', '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 13, 1, 'Sales User Created', '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 14, 1, 'Sales User Created', '2015-09-18 08:13:59', '2015-09-18 00:13:59'),
(8, 15, 1, 'Sales User Created', '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 16, 1, 'Sales User Created', '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 17, 1, 'Sales User Created', '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 18, 1, 'Sales User Created', '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 19, 1, 'Sales User Created', '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 20, 1, 'Sales User Created', '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 21, 1, 'Sales User Created', '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 22, 1, 'Sales User Created', '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 23, 1, 'Sales User Created', '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 24, 1, 'Sales User Created', '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 25, 1, 'Sales User Created', '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 26, 1, 'Sales User Created', '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 27, 1, 'Sales User Created', '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 28, 1, 'Sales User Created', '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 29, 1, 'Sales User Created', '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 30, 1, 'Sales User Created', '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 31, 1, 'Sales User Created', '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(25, 32, 1, 'Sales User Created', '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 33, 1, 'Sales User Created', '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 34, 1, 'Sales User Created', '2015-09-18 08:46:53', '2015-09-18 00:46:53');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_log_users`
--

CREATE TABLE IF NOT EXISTS `legacy_log_users` (
`id` int(11) NOT NULL,
  `activity` varchar(1000) NOT NULL,
  `user_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_log_users`
--

INSERT INTO `legacy_log_users` (`id`, `activity`, `user_id`, `edit_id`, `created_at`, `updated_at`) VALUES
(1, 'Designation Changed', 7, 1, '2015-09-18 08:30:44', '2015-09-18 00:30:44'),
(2, 'System User Updated', 7, 1, '2015-09-18 08:30:45', '2015-09-18 00:30:45');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_migrations`
--

CREATE TABLE IF NOT EXISTS `legacy_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_permissions`
--

CREATE TABLE IF NOT EXISTS `legacy_permissions` (
`id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_permissions`
--

INSERT INTO `legacy_permissions` (`id`, `module_id`, `usertype_id`, `view`, `edit`, `delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(2, 2, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-11 02:30:54'),
(3, 3, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-11 02:21:26'),
(4, 4, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-11 02:21:26'),
(5, 5, 1, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-10 22:14:50'),
(6, 1, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 23:53:40'),
(7, 2, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-17 02:52:51'),
(8, 3, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:52'),
(9, 4, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:52'),
(10, 5, 2, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-10 22:15:10'),
(11, 1, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(12, 2, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(13, 3, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(14, 4, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(15, 5, 3, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:42:38'),
(16, 1, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(17, 2, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(18, 3, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(19, 4, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(20, 5, 4, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(21, 1, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(22, 2, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(23, 3, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:10'),
(24, 4, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(25, 5, 5, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-13 18:54:32'),
(26, 1, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(27, 2, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(28, 3, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(29, 4, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(30, 5, 6, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-18 00:23:21'),
(31, 1, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(32, 2, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(33, 3, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(34, 4, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26'),
(35, 5, 7, 1, 1, 1, '2015-09-10 00:00:00', '2015-09-16 00:32:26');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_permission_modules`
--

CREATE TABLE IF NOT EXISTS `legacy_permission_modules` (
`id` int(11) NOT NULL,
  `module` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_permission_modules`
--

INSERT INTO `legacy_permission_modules` (`id`, `module`) VALUES
(1, 'User Management'),
(2, 'Payroll Management'),
(3, 'Policy Management'),
(4, 'Provider and Product Management'),
(5, 'Summary and Reports');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_products`
--

CREATE TABLE IF NOT EXISTS `legacy_products` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `rate` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_providers`
--

CREATE TABLE IF NOT EXISTS `legacy_providers` (
`id` int(11) NOT NULL,
  `code` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales`
--

CREATE TABLE IF NOT EXISTS `legacy_sales` (
`id` int(11) NOT NULL,
  `sales_id` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `designation_id` int(11) NOT NULL,
  `bonding_rate` int(3) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_sales`
--

INSERT INTO `legacy_sales` (`id`, `sales_id`, `designation_id`, `bonding_rate`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'SALES-0001', 6, 10, 8, '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 'SALES-0002', 6, 10, 9, '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 'SALES-0003', 5, 10, 10, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(4, 'SALES-0004', 5, 10, 11, '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 'SALES-0005', 3, 10, 12, '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 'SALES-0006', 3, 10, 13, '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 'SALES-0007', 4, 10, 14, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(8, 'SALES-0008', 4, 10, 15, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 'SALES-0009', 4, 10, 16, '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 'SALES-0010', 4, 10, 17, '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 'SALES-0011', 2, 10, 18, '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 'SALES-0012', 2, 10, 19, '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 'SALES-0013', 2, 10, 20, '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 'SALES-0014', 2, 10, 21, '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 'SALES-0015', 1, 10, 22, '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 'SALES-0016', 1, 10, 23, '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 'SALES-0017', 1, 10, 24, '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 'SALES-0018', 1, 10, 25, '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 'SALES-0019', 1, 10, 26, '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 'SALES-0020', 1, 10, 27, '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 'SALES-0021', 1, 10, 28, '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 'SALES-0022', 1, 10, 29, '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 'SALES-0023', 1, 10, 30, '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 'SALES-0024', 1, 10, 31, '2015-09-18 08:43:13', '2015-09-18 00:43:13'),
(25, 'SALES-0025', 1, 10, 32, '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 'SALES-0026', 1, 10, 33, '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 'SALES-0027', 1, 10, 34, '2015-09-18 08:46:52', '2015-09-18 00:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_bondings`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_bondings` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `bonding_rate` int(11) NOT NULL,
  `prev_bonding_rate` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `effective_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_bondings`
--

INSERT INTO `legacy_sales_bondings` (`id`, `sales_id`, `bonding_rate`, `prev_bonding_rate`, `edit_id`, `effective_date`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 10, 1, '2015-09-18 08:05:06', '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 2, 10, 10, 1, '2015-09-18 08:05:44', '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 3, 10, 10, 1, '2015-09-18 08:06:35', '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(4, 4, 10, 10, 1, '2015-09-18 08:07:39', '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 5, 10, 10, 1, '2015-09-18 08:08:24', '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 6, 10, 10, 1, '2015-09-18 08:09:01', '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 7, 10, 10, 1, '2015-09-18 08:13:58', '2015-09-18 08:13:59', '2015-09-18 00:13:59'),
(8, 8, 10, 10, 1, '2015-09-18 08:15:37', '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 9, 10, 10, 1, '2015-09-18 08:16:39', '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 10, 10, 10, 1, '2015-09-18 08:17:23', '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 11, 10, 10, 1, '2015-09-18 08:18:31', '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 12, 10, 10, 1, '2015-09-18 08:21:04', '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 13, 10, 10, 1, '2015-09-18 08:21:52', '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 14, 10, 10, 1, '2015-09-18 08:22:20', '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 15, 10, 10, 1, '2015-09-18 08:28:02', '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 16, 10, 10, 1, '2015-09-18 08:28:48', '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 17, 10, 10, 1, '2015-09-18 08:29:41', '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 18, 10, 10, 1, '2015-09-18 08:30:26', '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 19, 10, 10, 1, '2015-09-18 08:31:23', '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 20, 10, 10, 1, '2015-09-18 08:33:47', '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 21, 10, 10, 1, '2015-09-18 08:36:30', '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 22, 10, 10, 1, '2015-09-18 08:38:43', '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 23, 10, 10, 1, '2015-09-18 08:39:32', '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 24, 10, 10, 1, '2015-09-18 08:43:13', '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(25, 25, 10, 10, 1, '2015-09-18 08:44:25', '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 26, 10, 10, 1, '2015-09-18 08:45:13', '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 27, 10, 10, 1, '2015-09-18 08:46:52', '2015-09-18 08:46:53', '2015-09-18 00:46:53');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_designations`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_designations` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `prev_designation_id` int(11) DEFAULT NULL,
  `edit_id` int(11) NOT NULL,
  `effective_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_designations`
--

INSERT INTO `legacy_sales_designations` (`id`, `sales_id`, `designation_id`, `prev_designation_id`, `edit_id`, `effective_date`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 6, 1, '2015-09-18 08:05:06', '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 2, 6, 6, 1, '2015-09-18 08:05:44', '2015-09-18 08:05:44', '2015-09-18 00:05:44'),
(3, 3, 5, 5, 1, '2015-09-18 08:06:35', '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(4, 4, 5, 5, 1, '2015-09-18 08:07:39', '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(5, 5, 3, 3, 1, '2015-09-18 08:08:24', '2015-09-18 08:08:24', '2015-09-18 00:08:24'),
(6, 6, 3, 3, 1, '2015-09-18 08:09:01', '2015-09-18 08:09:01', '2015-09-18 00:09:01'),
(7, 7, 4, 4, 1, '2015-09-18 08:13:58', '2015-09-18 08:13:59', '2015-09-18 00:13:59'),
(8, 8, 4, 4, 1, '2015-09-18 08:15:37', '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(9, 9, 4, 4, 1, '2015-09-18 08:16:39', '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(10, 10, 4, 4, 1, '2015-09-18 08:17:23', '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(11, 11, 2, 2, 1, '2015-09-18 08:18:31', '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(12, 12, 2, 2, 1, '2015-09-18 08:21:04', '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(13, 13, 2, 2, 1, '2015-09-18 08:21:52', '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(14, 14, 2, 2, 1, '2015-09-18 08:22:20', '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(15, 15, 1, 1, 1, '2015-09-18 08:28:02', '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(16, 16, 1, 1, 1, '2015-09-18 08:28:48', '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(17, 17, 1, 1, 1, '2015-09-18 08:29:41', '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(18, 18, 1, 1, 1, '2015-09-18 08:30:26', '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(19, 19, 1, 1, 1, '2015-09-18 08:31:23', '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(20, 20, 1, 1, 1, '2015-09-18 08:33:47', '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(21, 21, 1, 1, 1, '2015-09-18 08:36:30', '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(22, 22, 1, 1, 1, '2015-09-18 08:38:43', '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(23, 23, 1, 1, 1, '2015-09-18 08:39:32', '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(24, 24, 1, 1, 1, '2015-09-18 08:43:13', '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(25, 25, 1, 1, 1, '2015-09-18 08:44:25', '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(26, 26, 1, 1, 1, '2015-09-18 08:45:13', '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(27, 27, 1, 1, 1, '2015-09-18 08:46:52', '2015-09-18 08:46:53', '2015-09-18 00:46:53');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_no_teams`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_no_teams` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_no_teams`
--

INSERT INTO `legacy_sales_no_teams` (`id`, `sales_id`, `created_at`, `updated_at`) VALUES
(1, 4, '2015-09-18 08:07:39', '2015-09-18 00:07:39'),
(2, 6, '2015-09-18 08:09:01', '2015-09-18 08:10:48'),
(3, 10, '2015-09-18 08:17:23', '2015-09-18 00:17:23'),
(4, 14, '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(5, 27, '2015-09-18 08:46:52', '2015-09-18 00:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_partners`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_partners` (
`id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_partners`
--

INSERT INTO `legacy_sales_partners` (`id`, `partner_id`, `sales_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(2, 1, 7, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(3, 1, 11, '2015-09-18 08:18:31', '2015-09-18 00:18:31');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_supervisors`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_supervisors` (
`id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_supervisors`
--

INSERT INTO `legacy_sales_supervisors` (`id`, `supervisor_id`, `sales_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(2, 1, 5, '2015-09-18 00:00:00', '2015-09-18 08:11:45'),
(3, 1, 7, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(4, 3, 8, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(5, 4, 9, '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(6, 1, 11, '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(7, 5, 12, '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(8, 6, 13, '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(9, 5, 15, '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(10, 3, 16, '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(11, 4, 17, '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(12, 6, 18, '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(13, 7, 19, '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(14, 10, 20, '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(15, 8, 21, '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(16, 9, 22, '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(17, 11, 23, '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(18, 14, 24, '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(19, 12, 25, '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(20, 13, 26, '2015-09-18 08:45:13', '2015-09-18 00:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_sales_teams`
--

CREATE TABLE IF NOT EXISTS `legacy_sales_teams` (
`id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `prev_supervisor_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `edit_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_sales_teams`
--

INSERT INTO `legacy_sales_teams` (`id`, `sales_id`, `prev_supervisor_id`, `supervisor_id`, `edit_id`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, 1, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(2, 5, 1, 1, 1, '2015-09-18 00:00:00', '2015-09-18 08:11:56'),
(3, 7, 1, 1, 1, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(4, 8, 3, 3, 1, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(5, 9, 4, 4, 1, '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(6, 11, 1, 1, 1, '2015-09-18 08:18:31', '2015-09-18 00:18:31'),
(7, 12, 5, 5, 1, '2015-09-18 08:21:04', '2015-09-18 00:21:04'),
(8, 13, 6, 6, 1, '2015-09-18 08:21:52', '2015-09-18 00:21:52'),
(9, 15, 5, 5, 1, '2015-09-18 08:28:02', '2015-09-18 00:28:02'),
(10, 16, 3, 3, 1, '2015-09-18 08:28:48', '2015-09-18 00:28:48'),
(11, 17, 4, 4, 1, '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(12, 18, 6, 6, 1, '2015-09-18 08:30:26', '2015-09-18 00:30:26'),
(13, 19, 7, 7, 1, '2015-09-18 08:31:23', '2015-09-18 00:31:23'),
(14, 20, 10, 10, 1, '2015-09-18 08:33:47', '2015-09-18 00:33:47'),
(15, 21, 8, 8, 1, '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(16, 22, 9, 9, 1, '2015-09-18 08:38:43', '2015-09-18 00:38:43'),
(17, 23, 11, 11, 1, '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(18, 24, 14, 14, 1, '2015-09-18 08:43:14', '2015-09-18 00:43:14'),
(19, 25, 12, 12, 1, '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(20, 26, 13, 13, 1, '2015-09-18 08:45:13', '2015-09-18 00:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_teams`
--

CREATE TABLE IF NOT EXISTS `legacy_teams` (
`id` int(11) NOT NULL,
  `team_code` varchar(1000) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `legacy_teams`
--

INSERT INTO `legacy_teams` (`id`, `team_code`, `sales_id`, `created_at`, `updated_at`) VALUES
(1, 'TEAM-0001', 1, '2015-09-18 08:05:06', '2015-09-18 00:05:06'),
(2, 'TEAM-0002', 2, '2015-09-18 08:05:44', '2015-09-18 00:05:44');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_upload_feeds`
--

CREATE TABLE IF NOT EXISTS `legacy_upload_feeds` (
`id` int(11) NOT NULL,
  `file_name` varchar(1000) NOT NULL,
  `orig_name` varchar(1000) NOT NULL,
  `feed_type` varchar(1000) NOT NULL,
  `provider` varchar(1000) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `legacy_users`
--

CREATE TABLE IF NOT EXISTS `legacy_users` (
`id` int(11) unsigned NOT NULL,
  `system_id` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `photo` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `name` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `email` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `mobile` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `username` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `password` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_users`
--

INSERT INTO `legacy_users` (`id`, `system_id`, `code`, `photo`, `name`, `email`, `mobile`, `username`, `password`, `usertype_id`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'USER-0001', 'CEO-0001', '1.jpg', 'CEO', 'ceo@legacy.com', '123456789', 'ceo@legacy.com', '$2y$10$MW18a47sC5Yoqp6cVme.4.I0XKvKYpNAa2dM87K3Sa1lvw2W7ZwL.', 1, NULL, 1, '2015-09-18 00:00:00', '2015-09-17 19:05:58'),
(2, 'USER-0002', 'IT-0001', '2.jpg', 'IT Users', 'itusers@legacy.com', '123456789', 'itusers@legacy.com', '$2y$10$EKLdiyYypLhQpJqFVbCZT.2Iyxwjj/c6Jc3qD7eboSuKUPiCHlidO', 2, NULL, 1, '2015-09-18 03:04:36', '2015-09-17 19:04:37'),
(3, 'USER-0003', 'ADMIN-0001', '3.jpg', 'Admin', 'admin@legacy.com', '123456789', 'admin@legacy.com', '$2y$10$G4Z7/HcFWx1LeqVar0QC3eVW3G5Sfq7I9oFmIbcFDMdBmZlPUm9O.', 3, NULL, 1, '2015-09-18 03:05:43', '2015-09-17 19:05:43'),
(4, 'USER-0004', 'ADMIN-0002', '4.jpg', 'Admin Assistant', 'admin_assistant@legacy.com', '123456789', 'admin_assistant@legacy.com', '$2y$10$.CJMtHlbwcvdiL3IT9Ot/ulbIo7De6Z6pZaNXbRJiVEkZXg8pSy86', 4, NULL, 1, '2015-09-18 03:06:54', '2015-09-17 19:06:55'),
(5, 'USER-0005', 'ACC-0001', '5.jpg', 'Accounting', 'accounting@legacy.com', '123456789', 'accounting@legacy.com', '$2y$10$wHiFDt8sAopMt5sEx.1IGOPTpgay4FQRILCu/xX23Eb/OKbk6MVT2', 5, NULL, 1, '2015-09-18 03:07:49', '2015-09-17 19:07:56'),
(6, 'USER-0006', 'ACC-0002', '6.jpg', 'Accounting Assistant', 'acc_assistant@legacy.com', '123456789', 'acc_assistant@legacy.com', '$2y$10$50jEEFTCKlv66IvNEpycq.kIXI8I0bdgNY.GRt4OjvXJYMskXZpCy', 6, NULL, 1, '2015-09-18 03:10:07', '2015-09-17 19:10:09'),
(7, 'USER-0007', 'SALES-0001', '7.jpg', 'Sales Assistant', 'sales_assistant@legacy.com', '123456789', 'sales_assistant@legacy.com', '$2y$10$.r4lWIhPbHnqwGXFOkyFsOIvPZsHDAUJ8mSK9j2U4iqrf.ftZQ.OW', 7, NULL, 1, '2015-09-18 03:12:25', '2015-09-18 08:03:59'),
(8, NULL, 'AGENT-001', '8.jpg', 'Partner', 'partner@legacy.com', '123456789', 'partner@legacy.com', '$2y$10$9mM50198HSAtVCD65rusVe1ScKhnVpPUGUkAfbPjgQWINgan4U8Fa', 8, NULL, 1, '2015-09-18 08:05:05', '2015-09-18 00:05:06'),
(9, NULL, 'AGENT-002', '9.jpg', 'Partner', 'partner@legacy.co', '123456789', 'partner@legacy.co', '$2y$10$JjyPjL2nToyhIhT8PAx2FOhFDX9YN0JYd7Pwwo6WkzN.hbDno8YJ2', 8, NULL, 1, '2015-09-18 08:05:43', '2015-09-18 00:05:44'),
(10, NULL, 'AGENT-003', '10.jpg', 'Director', 'director@legacy.com', '123456789', 'director@legacy.com', '$2y$10$FktQHCwB/TR8NEh4TFe3QuP/vsWjpuvNjyUvO9vRnO45UAKM2LDIG', 8, NULL, 1, '2015-09-18 08:06:35', '2015-09-18 00:06:35'),
(11, NULL, 'AGENT-004', '11.jpg', 'No Director', 'no_director@legacy.com', '123456789', 'no_director@legacy.com', '$2y$10$55Jlmjr/bYmmbYixJO4uzuX2722Th/ylTZ4GUwynMy5.ep1szB7Ki', 8, NULL, 1, '2015-09-18 08:07:38', '2015-09-18 00:07:39'),
(12, NULL, 'AGENT-005', '12.jpg', 'Manager', 'manager@legacy.com', '123456789', 'manager@legacy.com', '$2y$10$xTXw.PZd3w7DGsnliYnql.UJpIhJMem0QGBBKxmzfT0BqlGwttLqG', 8, NULL, 1, '2015-09-18 08:08:23', '2015-09-18 00:08:24'),
(13, NULL, 'AGENT-006', '13.jpg', 'No Manager', 'no_manager@legacy.com', '123456789', 'no_manager@legacy.com', '$2y$10$omOlEKq2O2catgziIh1rXum/wo63NKzZodfIJ46jPUrwzcQFBc1qq', 8, NULL, 1, '2015-09-18 08:09:00', '2015-09-18 00:09:01'),
(14, NULL, 'AGENT-007', '14.jpg', 'Assistant Director', 'assistant_director@legacy.com', '123456789', 'assistant_director@legacy.com', '$2y$10$L6pW/ApUqwTu1ngNGa7vke4gvjTnZqFe7UTV0Izow5XbR/6yau1r2', 8, NULL, 1, '2015-09-18 08:13:58', '2015-09-18 00:13:58'),
(15, NULL, 'AGENT-008', '15.jpg', 'Dir Assistant Director', 'dir_assistant_director@legacy.com', '123456789', 'dir_assistant_director@legacy.com', '$2y$10$ltphYj3aOX12DYj0meQbK.yA9a7z2ppZzoku6H5LVYj4W5xWYnUoi', 8, NULL, 1, '2015-09-18 08:15:37', '2015-09-18 00:15:37'),
(16, NULL, 'AGENT-009', '16.jpg', 'NoDir Assistant Director', 'nodir_assistant_director@legacy.com', '123456789', 'nodir_assistant_director@legacy.com', '$2y$10$6ng5nCBxa6BqG9LbWJI/6OP7MBlCi.es8NT6TleG4nmiVENX2j1d.', 8, NULL, 1, '2015-09-18 08:16:39', '2015-09-18 00:16:39'),
(17, NULL, 'AGENT-010', '17.jpg', 'No Assistant Director', 'no_assistant_director@legacy.com', '123456789', 'no_assistant_director@legacy.com', '$2y$10$iCpjBUPIMkJFq2x0NHCXN.IXLcuxOSb0BK1..7y8239TIOur1BUT2', 8, NULL, 1, '2015-09-18 08:17:22', '2015-09-18 00:17:23'),
(18, NULL, 'AGENT-011', '18.jpg', 'Assistant Manager', 'assistant_manager@legacy.com', '123456789', 'assistant_manager@legacy.com', '$2y$10$2mK5W24Jg/yieshsmPq0r.pK8Yzz1Q8YSKwxBNSlOzuin7.1dt.Ni', 8, NULL, 1, '2015-09-18 08:18:30', '2015-09-18 00:18:31'),
(19, NULL, 'AGENT-012', '19.jpg', 'Man Assistant Manager', 'man_assistant_manager@legacy.com', '123456789', 'man_assistant_manager@legacy.com', '$2y$10$mbw73qKsmPW2FmqxmDsSVeGWZu1mrFCgwKhbxnd1vUXydjaXrGXNi', 8, NULL, 1, '2015-09-18 08:21:03', '2015-09-18 00:21:04'),
(20, NULL, 'AGENT-013', '20.jpg', 'NoMan Assistant Manager', 'noman_assistant_manager@legacy.com', '123456789', 'noman_assistant_manager@legacy.com', '$2y$10$NUK/l06IM/KkOcE.gTtLduROgBQmUzgdOGSXXP.x4f3WyvKgIC2fS', 8, NULL, 1, '2015-09-18 08:21:51', '2015-09-18 00:21:52'),
(21, NULL, 'AGENT-014', '21.jpg', 'No Assistant Manager', 'no_assistant_manager@legacy.com', '123456789', 'no_assistant_manager@legacy.com', '$2y$10$vafhz3eRcXkyontKtSCxQ.QMr0uEB/j5g2JDLmuIjm3fA.GLIIJ36', 8, NULL, 1, '2015-09-18 08:22:20', '2015-09-18 00:22:20'),
(22, NULL, 'AGENT-015', '22.jpg', 'Man Sales Agent', 'man_sales_agent@legacy.com', '123456789', 'man_sales_agent@legacy.com', '$2y$10$r4827Oy.79PU3rJXPxeRvOrQp6/eRsnhkE4.ksNsr2O8Kid.YHyBG', 8, NULL, 1, '2015-09-18 08:28:01', '2015-09-18 00:28:02'),
(23, NULL, 'AGENT-016', '23.jpg', 'Dir Sales Agent', 'dir_sales_agent@legacy.com', '123456789', 'dir_sales_agent@legacy.com', '$2y$10$/S6cZ1dkM6MOzVQgOasEbOvqrhCLPdvLuoY6/6ijTMe2MzxW0bu8.', 8, NULL, 1, '2015-09-18 08:28:47', '2015-09-18 00:28:48'),
(24, NULL, 'AGENT-017', '24.jpg', 'NoDir Sales Agent', 'nodir_sales_agent@legacy.com', '123456789', 'nodir_sales_agent@legacy.com', '$2y$10$bdEa/o78KPVqBpk2GIXroOrwQfxdqo4F2vnPn/HQekXTRGX4RZLK.', 8, NULL, 1, '2015-09-18 08:29:41', '2015-09-18 00:29:41'),
(25, NULL, 'AGENT-018', '25.jpg', 'NoMan Sales Agent', 'noman_sales_agent@legacy.com', '123456789', 'noman_sales_agent@legacy.com', '$2y$10$pTlIEFmYA0MQ2to8ToretesezBPnUImLuS8PWpngdaE1i0g2ekpay', 8, NULL, 1, '2015-09-18 08:30:25', '2015-09-18 00:30:26'),
(26, NULL, 'AGENT-019', '26.jpg', 'ADir Sales Agent', 'adir_sales_agent@legacy.com', '123456789', 'adir_sales_agent@legacy.com', '$2y$10$/HzMkV94taISoh8YO7D/q.gjOzRFe5ws8TFLgHmNrxgI6rHSH797K', 8, NULL, 1, '2015-09-18 08:31:22', '2015-09-18 00:31:23'),
(27, NULL, 'AGENT-020', '27.jpg', 'NoADir Sales Agent', 'noadir_sales_agent@legacy.com', '123456789', 'noadir_sales_agent@legacy.com', '$2y$10$YeoCbfyC2Rr68GsuPkKoxeQQXBrjKgLLLoXN0/DYftkFYdro/ysQe', 8, NULL, 1, '2015-09-18 08:33:46', '2015-09-18 00:33:47'),
(28, NULL, 'AGENT-021', '28.jpg', 'DirADir Sales Agent', 'diradir_sales_agent@legacy.com', '123456789', 'diradir_sales_agent@legacy.com', '$2y$10$r.Gjier4FHnVhAkH77n3be.au48X6E8dOxOclCc6oSXt8mT5yuaBC', 8, NULL, 1, '2015-09-18 08:36:30', '2015-09-18 00:36:30'),
(29, NULL, 'AGENT-022', '29.jpg', 'NoDirADir Sales Agent', 'nodiradir_sales_agent@legacy.com', '123456789', 'nodiradir_sales_agent@legacy.com', '$2y$10$YzmFRIxQJmwZxlocd4D1B.UCwUG8leHAmQRAIyQ55mZlpdHa6n3cm', 8, NULL, 1, '2015-09-18 08:38:42', '2015-09-18 00:38:43'),
(30, NULL, 'AGENT-023', '30.jpg', 'AMan Sales Agent', 'aman_sales_agent@legacy.com', '123456789', 'aman_sales_agent@legacy.com', '$2y$10$bhIZaooJ8J2lcAQlfSQS/ODzqpbXFA1NqN5lWTgAaP4.Sx7xnw/na', 8, NULL, 1, '2015-09-18 08:39:32', '2015-09-18 00:39:32'),
(31, NULL, 'AGENT-024', '31.jpg', 'NoAMan Sales Agent', 'noaman_sales_agent@legacy.com', '123456789', 'noaman_sales_agent@legacy.com', '$2y$10$NUfkIWD2qmtbBbocLijMAe8iII9/M..qhRLbAGNvWQyN186NVFn0y', 8, NULL, 1, '2015-09-18 08:43:13', '2015-09-18 00:43:13'),
(32, NULL, 'AGENT-025', '32.jpg', 'ManAMan Sales Agent', 'manaman_sales_agent@legacy.com', '123456789', 'manaman_sales_agent@legacy.com', '$2y$10$uzbxeCvwXqyBNEdGTS75JOE3DPa5MaEdQHydWsK5csXxSNIap5x8y', 8, NULL, 1, '2015-09-18 08:44:25', '2015-09-18 00:44:25'),
(33, NULL, 'AGENT-026', '33.jpg', 'NoManAMan Sales Agent', 'nomanaman_sales_agent@legacy.com', '123456789', 'nomanaman_sales_agent@legacy.com', '$2y$10$acP69bKWErDN8skHe5ubKuJgmL3eXVR2RXHeXCWq2fJE6Mi9kZyPy', 8, NULL, 1, '2015-09-18 08:45:13', '2015-09-18 00:45:13'),
(34, NULL, 'AGENT-027', '34.jpg', 'No Sales Agent', 'no_sales_agent@legacy.com', '123456789', 'no_sales_agent@legacy.com', '$2y$10$WYHkCmCXrMrIgFeQ4DiU8eFTEbzVcPvvNJNyM3/bKX871AyUeayMm', 8, NULL, 1, '2015-09-18 08:46:52', '2015-09-18 00:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `legacy_usertypes`
--

CREATE TABLE IF NOT EXISTS `legacy_usertypes` (
`id` int(11) unsigned NOT NULL,
  `type_name` varchar(1000) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legacy_usertypes`
--

INSERT INTO `legacy_usertypes` (`id`, `type_name`) VALUES
(1, 'CEO'),
(2, 'IT Users'),
(3, 'Admin Accounts'),
(4, 'Admin Assistant'),
(5, 'Accounting'),
(6, 'Accounting Assistant'),
(7, 'Sales Assistant'),
(8, 'Agent');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `legacy_assigned_sales`
--
ALTER TABLE `legacy_assigned_sales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_designations`
--
ALTER TABLE `legacy_designations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_log_sales`
--
ALTER TABLE `legacy_log_sales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_log_users`
--
ALTER TABLE `legacy_log_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_permissions`
--
ALTER TABLE `legacy_permissions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_permission_modules`
--
ALTER TABLE `legacy_permission_modules`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_providers`
--
ALTER TABLE `legacy_providers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales`
--
ALTER TABLE `legacy_sales`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_bondings`
--
ALTER TABLE `legacy_sales_bondings`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_designations`
--
ALTER TABLE `legacy_sales_designations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_no_teams`
--
ALTER TABLE `legacy_sales_no_teams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_partners`
--
ALTER TABLE `legacy_sales_partners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_supervisors`
--
ALTER TABLE `legacy_sales_supervisors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_sales_teams`
--
ALTER TABLE `legacy_sales_teams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_teams`
--
ALTER TABLE `legacy_teams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_upload_feeds`
--
ALTER TABLE `legacy_upload_feeds`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_users`
--
ALTER TABLE `legacy_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legacy_usertypes`
--
ALTER TABLE `legacy_usertypes`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `legacy_assigned_sales`
--
ALTER TABLE `legacy_assigned_sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `legacy_designations`
--
ALTER TABLE `legacy_designations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `legacy_log_sales`
--
ALTER TABLE `legacy_log_sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `legacy_log_users`
--
ALTER TABLE `legacy_log_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `legacy_permissions`
--
ALTER TABLE `legacy_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `legacy_permission_modules`
--
ALTER TABLE `legacy_permission_modules`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `legacy_providers`
--
ALTER TABLE `legacy_providers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `legacy_sales`
--
ALTER TABLE `legacy_sales`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `legacy_sales_bondings`
--
ALTER TABLE `legacy_sales_bondings`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `legacy_sales_designations`
--
ALTER TABLE `legacy_sales_designations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `legacy_sales_no_teams`
--
ALTER TABLE `legacy_sales_no_teams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `legacy_sales_partners`
--
ALTER TABLE `legacy_sales_partners`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `legacy_sales_supervisors`
--
ALTER TABLE `legacy_sales_supervisors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `legacy_sales_teams`
--
ALTER TABLE `legacy_sales_teams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `legacy_teams`
--
ALTER TABLE `legacy_teams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `legacy_upload_feeds`
--
ALTER TABLE `legacy_upload_feeds`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `legacy_users`
--
ALTER TABLE `legacy_users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `legacy_usertypes`
--
ALTER TABLE `legacy_usertypes`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
