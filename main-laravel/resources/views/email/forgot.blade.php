<p>Hi {{$name}},</p>
<p>Your LFA Account password has been successfully reset</p>
<p>Please log in to <a href="https://sys.legacyfa-asia.com">https://sys.legacyfa-asia.com</a> using the following credentials:</p>
<br>
<p>User: {{$email}}</p>
<p>Password: s*******Z!</p>
<br>
<br>
<p>Please note that password is your NRIC with the first letter in lowercase, capital last letter and ends with an exclamatory point (!)</p>
<br>
<p>Kindly reset the password once you have logged in, using the link at the upper right corner of the home screen.</p>
<br>
<p>For any issues encountered, you can e-mail <a href="mailto:techsupport@legacyfa-asia.com">techsupport@legacyfa-asia.com</a> Thank you and welcome!</p>