<p>Hi {{$name}},</p>
<br>
<p>Your account has been successfully created!</p>
<p>You may login the FA System / Case Submission Portal via <a href="https://sys.legacyfa-asia.com">https://sys.legacyfa-asia.com</a></p>
<br>
<p>User: {{$email}}</p>
<p>Password: s*******Z!</p>
<br>
<br>
<p>Please note that password is your NRIC with the first letter in lowercase, capital last letter and ends with an exclamatory point (!)</p>
<br>
<p>Kindly reset the password once you have logged in, using the link at the upper right corner of the home screen.</p>
<br>
<p>For any issues encountered, you can e-mail <a href="mailto:techsupport@legacyfa-asia.com">techsupport@legacyfa-asia.com</a> Thank you and welcome!</p>