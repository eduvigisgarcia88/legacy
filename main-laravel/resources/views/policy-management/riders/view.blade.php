 @extends('layouts.master')

 @section('content')
    <!-- Page Content -->
 <div id="page-content-wrapper" class="response">
    <div class="container-fluid">
    <!-- Title -->
    <div class="row main-title-container container-space">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  title-margin">
            <h3 class="main-title">POLICY MANAGEMENT</h3>
            <h5 class="bread-crumb">POLICY MANAGEMENT > VIEW <h5>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  title-margin">
              <a href="policy-management-policy.html" type="button" class="btn btn-success borderzero btn-tool pull-right"><i class="fa fa-chevron-left"></i> BACK</a>     
              <a type="button" href="policy-management-edit.html" class="btn btn-info borderzero btn-tool pull-right"><i class="fa fa-pencil"></i> Edit</a>          
        </div>
        </div>
              <div id="basicinfo" class="row default-container container-space">
                      <div class="col-lg-12">
                          <p><strong>BASIC INFORMATION</strong></p>
                      </div>
                      <div class="col-lg-1">
                          <img src="{{ url('images/avatar.png') }}" style="color: #79828f; margin:0px auto;" class="img-circle" alt="Smiley face" height="100" width="100">
                      </div>
                      <div class="col-lg-11">
                      <form class="form-horizontal">
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">AGENT ID</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="12345" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">DESIGNATION</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Sales Agent" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">PHOTO</label>
                              <div class="input-group col-lg-6 col-md-6 col-sm-8 col-xs-8" style="padding-right: 14px; padding-left: 15px;">
                                <input type="text" class="form-control borderzero" placeholder="default.jpg" disabled>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">AGENT CODE</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="sales2015" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">USER NAME</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="SLS-12345" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">AGENT NAME</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Steve Jobs" disabled>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">EMAIL</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="stevejobs@gmail.com" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">MOBILE</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="+65 123 4567" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">STATUS</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="ACTIVE" disabled>
                              </div>
                          </div>
                      </div>
                      </form>
          </div>
          </div>
  <div id="basicinfo" class="row default-container container-space">
  <div class="col-lg-6 col-xs-6">
    <p><strong>POLICY INFORMATION</strong></p>
  </div>
  <div class="col-lg-6 col-xs-6" style="padding-left:0;">
      <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#team" aria-expanded="false" aria-controls="collapseTwo"></i>
  </div>
  <div id="team" class="panel-collapse collapse in col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
  <div class="container-fluid">
  <form class="form-horizontal" >     
  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY ID</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Filter Policy ID" disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY NO</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Filter Policy NO" disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY TYPE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMPONENT CODE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY TERM</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>CONTRACT CURRENCY</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>SUM INSURED</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>INCEPT DATE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>INSTALLMENT FORM DATE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>BILLING FREQUENCY</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY EXPIRY DATE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>GROSS PREMIUM PAID</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>NET PREMIUM PAID</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>GROSS PREMIUM INC GST</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>NET PREMIUM INC GST</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PREMIUM W/O COM</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PREMIUM CONV RATE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PAYMENT CURRENCY</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMMISION OR</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMMISION RUN DATE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMMISION CONV RATE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TRANSACTION CODE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PREMIUM TERM</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                  </div>
              </div>
              <div class="form-group">
                  <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>DATE OF ISSUE</small></label>
                  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                    <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="basicinfo" class="row default-container container-space">
    <div class="col-lg-6 col-xs-6">
      <p><strong>INTRODUCER SUMMARY</strong></p>
    </div>
  <div class="col-lg-6 col-xs-6" style="padding-left:0;">
      <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#policy" aria-expanded="false" aria-controls="collapseTwo"></i>
  </div>
    <div id="policy" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
      <div class="container-fluid">
          <div class="table-responsive">     
                <table class="table">
                  <thead id="policyfirstthead">
                    <tr>
                      <th>INTRODUCER ID</th>
                      <th>INTRODUCER NAME</th>
                      <th>REMARKS</th>
                      <th>DATE OF ENTRY</th>
                      <th>TOOLS</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
                <button type="button" class="btn btn-default borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalmove"> Update Introducers</button>
          </div>
      </div>
    </div>
</div>
<div id="basicinfo" class="row default-container container-space">
    <div class="col-lg-6 col-xs-6">
      <p><strong>ADMIN PRE-PAYROLL ENTRIES</strong></p>
    </div>
  <div class="col-lg-6 col-xs-6" style="padding-left:0;">
      <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#payroll" aria-expanded="false" aria-controls="collapseTwo"></i>
  </div>
    <div id="payroll" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
      <div class="container-fluid">
        <form class="form-horizontal" >     
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL INCENTIVES</small></label>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                  <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Incentives"disabled>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL DEDUCTIONS</small></label>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                  <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Deductions"disabled>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
            <div class="form-group">
                <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL</small></label>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                  <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total"disabled>
                </div>
            </div>
       </div>
       <button type="button" class="btn btn-default borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalmove"> Update Admin Entries</button>
</form>
</div>
</div>
</div>
<div id="basicinfo" class="row default-container container-space">
    <div class="col-lg-6 col-xs-6">
      <p><strong>MOVEMENT HISTORY</strong></p>
    </div>
  <div class="col-lg-6 col-xs-6" style="padding-left:0;">
      <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#tax" aria-expanded="false" aria-controls="collapseTwo"></i>
  </div>
    <div id="tax" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
      <table class="table table-striped">
        <thead class="tbheader">
          <tr>
            <th><i class="fa fa-calendar"></i> DATE</th>
            <th><i class="fa fa-sort"></i> PREVIOUS AGENT</th>
            <th><i class="fa fa-sort"></i> NEW AGENT</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>January 2015</td>
            <td>Bond</td>
            <td>James</td>
          </tr>
          <tr>
            <td>February 2015</td>
            <td>Bond</td>
            <td>James</td>
          </tr>
          <tr>
            <td>March 2015</td>
            <td>James</td>
            <td>Bond</td>
          </tr>
        </tbody>
      </table>
      <button type="button" class="btn btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i> Move Policy</button>
    </div>
</div>
<div id="basicinfo" class="row default-container container-space">
    <div class="col-lg-6 col-xs-6">
      <p><strong>PAYROLL PREVIEW</strong></p>
    </div>
  <div class="col-lg-6 col-xs-6" style="padding-left:0;">
      <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#movement" aria-expanded="false" aria-controls="collapseTwo"></i>
  </div>
    <div id="movement" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
      <div class="container-fluid">
        <form class="form-horizontal" >     
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL INCENTIVES</small></label>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                  <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Incentives"disabled>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL DEDUCTIONS</small></label>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                  <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Deductions"disabled>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
            <div class="form-group">
                <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL</small></label>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                  <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total"disabled>
                </div>
            </div>
       </div>
</form>
</div>
</div>
</div>
</div>
</div>
          
<!-- /#page-content-wrapper -->
@include('policy-management.riders.form')
@stop