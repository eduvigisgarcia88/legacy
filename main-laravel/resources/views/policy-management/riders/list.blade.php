@extends('layouts.master')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';
  function refresh() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');

    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();

    $.post("{{ url('policy/riders/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

      var rows = '';
      $.each(response.rows.data, function(index, row) {
        var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

        rows += '<tr data-id="' + row.id + '">' +
                  '<td><a class="btn btn-xs btn-table btn-expand" data-policy="' + row.policy_no + '"><i class="fa fa-plus"></i></a> ' + row.policy_no + '</td>' +
                  '<td>' + row.name + '</td>' +
                '</tr>';
      });

      $('#rows').html(rows);
      $('#row-pages').html(response.pages);
      $('.loading-pane').addClass('hide');
      //$('.fetch-data').html(list);
        
    });
  }

  function search() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();

    $.post("{{ url('policy/riders/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

      var rows = '';
      $.each(response.rows.data, function(index, row) {
        var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

        rows += '<tr data-id="' + row.id + '">' +
                  '<td><a class="btn btn-xs btn-table btn-expand" data-policy="' + row.policy_no + '"><i class="fa fa-plus"></i></a> ' + row.policy_no + '</td>' +
                  '<td>' + row.name + '</td>' +
                '</tr>';
      });

      $('#rows').html(rows);
      $('#row-pages').html(response.pages);
      $('.loading-pane').addClass('hide');
      //$('.fetch-data').html(list);
        
    });
  }

  $(document).ready(function() {

    $loading = false;
    $selectedID = 0;

    $("#page-content-wrapper").on("click", ".btn-expand", function() {
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
        if ($loading){
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $policy = $(this).data('policy');

        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('policy/get-policies') }}", { id: $id, policy: $policy, _token: $_token }, function(response) {

          $tr.after('<tr><td colspan="6" style="padding:10px"><div class="row-' + $selectedID + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
        });

      });

  });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        //$('#ajax-data_feeds').load($(this).attr('href'));
        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();

        $.post("{{ url('policy/riders/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

          var rows = '';
          $.each(response.rows.data, function(index, row) {
            var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

            rows += '<tr data-id="' + row.id + '">' +
                      '<td><a class="btn btn-xs btn-table btn-expand" data-policy="' + row.policy_no + '"><i class="fa fa-plus"></i></a> ' + row.policy_no + '</td>' +
                      '<td>' + row.name + '</td>' +
                    '</tr>';
          });

          $('#rows').html(rows);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          //$('.fetch-data').html(list);
            
        });
      }
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();

        $.post("{{ url('policy/riders/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {
          console.log(response);
          var rows = '';
          $.each(response.rows.data, function(index, row) {
            console.log(response);
            var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

            rows += '<tr data-id="' + row.id + '">' +
                      '<td><a class="btn btn-xs btn-table btn-expand" data-policy="' + row.policy_no + '"><i class="fa fa-plus"></i></a> ' + row.policy_no + '</td>' +
                      '<td>' + row.name + '</td>' +
                    '</tr>';
          });

          $('#rows').html(rows);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          //$('.fetch-data').html(list);
            
        });

    });


</script>

@stop


@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">RIDERS</h3>
        <h5 class="bread-crumb">PRODUCTION & POLICY MANAGEMENT</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
   <!--        <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-minus"></i> Remove</button>
          <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i> Disable</button>    
          <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalimport"><i class="fa fa-upload"></i> IMPORT</button> 
     --></div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="i">
                        <input type="text" class="form-control borderzero" id="row-search" onkeyup="search()" placeholder="Search for...">
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
          <div class="container-fluid content-table col-xs-12" style="border-top:1px #e6e6e6 solid;">
            <div class="table-responsive block-content tbblock">
                        <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div> 
              <table class="table table-striped table-pagination">
                <thead class="tbheader">
                  <tr>
                    <th class="th-sort" data-sort="policy_no"><i></i> POLICY NO</th>
                    <th class="th-sort" data-sort="name"><i></i> AGENT NAME</th>
                  </tr>
                </thead>
                <tbody id="rows">
                  @foreach($rows as $row)
                  <tr data-id="{{ $row->id }}" >
                    <td><a class="btn btn-xs btn-table btn-expand" data-policy="{{ $row->policy_no }}"><i class="fa fa-plus"></i></a> {{ $row->policy_no }}</td>
                    <td>{{ $row->name }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>    
        <div id="row-pages" class="col-lg-6 text-center borderzero leftalign">{!! $pages !!}</div>
              <div class="col-lg-6 content-table" style="text-align: -webkit-right;">
              <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
              </select>
            </div>
              <div class="text-center">
                <input type="hidden" id="row-page" value="1">
                <input type="hidden" id="row-sort" value="">
                <input type="hidden" id="row-order" value="">
              </div>
    </div>
    <!-- /#page-content-wrapper -->
@include('policy-management.form')
@stop