<!-- Modal -->
<div id="myModalview" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-eye"></i> Profile</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">POLICY ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="POL-12345" disabled>
  </div>
  <div class="form-group">
    <label for="email">AGENT ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="steve12345" disabled>
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">USER NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="Sales Agent" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12345678" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">DESIGNATION</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="steve@email.com" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">EMAIL</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12/06/1985" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">CONTACT NO.</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="+65 123 4567" disabled>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Edit</button>
</div>
</div>
</div>
</div>


<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-plus"></i> Add New</h4>
</div>
<div class="modal-body">
<form role="form">
    <div class="form-group">
    <label for="email">POLICY ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div> 
  <div class="form-group">
    <label for="email">AGENT ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">USER NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">DESIGNATION</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">EMAIL</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">CONTACT NO.</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">TYPE</label>
    <select id="select" class="form-control borderzero" id="sel1">
        <option>CEO</option>
        <option>IT Users</option>
        <option>Admin Accounts</option>
        <option>Accounting</option>
    </select>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>

<div id="myModaldelete" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-minus"></i> Remove</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to remove the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>

<div id="myModaldeactivate" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-adjust"></i> Disable</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to disable the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalimport" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-upload"></i> IMPORT</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to import the policy?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>

<!-- Modal -->
<div id="myModalmove" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">MOVE</h4>
      </div>
      <div class="modal-body">
<form role="form">
    <div class="form-group">
    <label for="pwd">RANK</label>
    <select id="select" class="form-control borderzero">
            <option>Sales Agent</option>
            <option>Assistant Manager</option>
            <option>Manager</option>
            <option>Assistant Director</option>
            <option>Director</option>
            <option>Partner</option>
          </select>
    
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">DATE</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="">
  </div>
</form>
</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="myModalorphan" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-book"></i> ORPHAN</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to orphan the policy?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>