@extends('layouts.master')

@section('scripts')
  <script>
    $(document).ready(function() {
      //datatable
      myTable = $('#my-table').dataTable({
            "aoColumnDefs": [{ 
              'bSortable': false, 
              'aTargets': [4],
            }],
            "order": [[ 4, "desc" ]],
            "dom": '<"top"i>rt<"bottom"flp><"clear">'
      });
      //show table
      $('.content-table').removeClass('hide');

      //search/filter
     /* 
      $('#search-designation').change(function(){
          myTable.fnFilter($(this).val(), 0);
      });

      /*
      $('#search-rank').change(function(){
          myTable.fnFilter($(this).val(), 3);
      });*/

      $('#search').keyup(function(){
          myTable.fnFilter($(this).val());
      });

        $('#page-content-wrapper').on('click', '.btn-export', function() {
          $('#modal-export').modal('show');
        });

      //   $('.btn-export-all').on('click', function() {
      //     console.log('btn-export-all');
          
      //     $_token = '{{ csrf_token() }}';

      //     $.post("{{ url('product/logs/export') }}", { _token: $_token }, function(response) {
      //       console.log('response: ' + response);
            // var blob=new Blob([response]);
            // var link=document.createElement('a');
            // link.href=window.URL.createObjectURL(blob);
            // link.download="myfile.csv";
            // link.click();
      //     }, 'json');
      //   });
      });

  </script>
@stop

@section('content')
    <!-- Page Content -->
    <div id="page-content-wrapper" class="response">
    <!-- Title -->
    <div class="container-fluid main-title-container container-space">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <h3 class="main-title">POLICY LOG</h3>
          <h5 class="bread-crumb">PRODUCTION & POLICY MANAGEMENT</h5>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
              <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalarchive"><i class="fa fa-archive"></i> Archive</button>     
              <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalexport"><i class="fa fa-plus"></i> Export</button>
        </div>
        </div>
      <!-- <div class="container-fluid default-container container-space"> -->
       <!--  <div class="col-lg-12"> -->
             <!--  <p><strong>FILTER OPTIONS</strong></p> -->
       <!--  </div> -->
         <!--  <form class="form-horizontal" >     
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> -->
            <!--   <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH OPTION</strong></p></label>
              <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <select class="select form-control borderzero" id="sel1" >
                      <option>All Providers</option>
                      <option>A</option>
                      <option>B</option>
                      <option>C</option>
                      <option>D</option>
                    </select>
                  </div>
              </div> -->
         <!--  </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> -->
             <!--  <div class="form-group">
                  <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                  <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <div class="input-group">
                    <input type="text" class="form-control borderzero select" placeholder="Search for..." id="search">
                    <span class="input-group-btn">
                      <button class="btn btn-default borderzero" type="button"><i class="fa fa-eraser"></i> <span class="hidden-xs">CLEAR</span></button>
                    </span>
                  </div>
                </div>
              </div> -->
            <!-- </div>
          </form> -->
        <!-- </div> -->
        <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
        <div class="table-responsive block-content tbblock">
          <table class="table" id="tblData">
              <thead class="tbheader">
                <tr>
                  <th><i class="fa fa-sort"></i> POLICY CONTRACT NO</th>
                  <th><i class="fa fa-sort"></i> ACTIVITY</th>
                  <th><i class="fa fa-sort"></i> EDITED BY</th>
                  <th><i class="fa fa-sort"></i> DATE</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($rows as $row)
                  <tr>
                    <td>{{ $row->policyInfo->contract_no }}</td>
                    <td>{{ $row->activity }}</td>
                    <td>{{ $row->policyLog->name}}</td>
                    <td>{{ $row->created_at }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>
<!-- /#page-content-wrapper -->
@include('policy-management.policy-history.form')

@stop