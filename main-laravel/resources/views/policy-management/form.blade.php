<!-- Modal -->
<div id="myModalview" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-eye"></i> Profile</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">POLICY ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="POL-12345" disabled>
  </div>
  <div class="form-group">
    <label for="email">AGENT ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="steve12345" disabled>
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">USER NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="Sales Agent" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12345678" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">DESIGNATION</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="steve@email.com" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">EMAIL</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12/06/1985" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">CONTACT NO.</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="+65 123 4567" disabled>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Edit</button>
</div>
</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-plus"></i> Add New</h4>
</div>
<div class="modal-body">
<form role="form">
    <div class="form-group">
    <label for="email">POLICY ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div> 
  <div class="form-group">
    <label for="email">AGENT ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">USER NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">DESIGNATION</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">EMAIL</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">CONTACT NO.</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">TYPE</label>
    <select id="select" class="form-control borderzero" id="sel1">
        <option>CEO</option>
        <option>IT Users</option>
        <option>Admin Accounts</option>
        <option>Accounting</option>
    </select>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
<div id="myModaldelete" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-minus"></i> REMOVE</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to remove the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldeactivate" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-adjust"></i> DISABLE</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to disable the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalimport" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content borderzero">
<div class="modal-header tbheader">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-upload"></i> IMPORT</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to import the policy?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>

<div id="myModalmove" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-exchange"></i> MOVE</h4>
</div>
<div class="row modal-body">
    <div class="container-fluid main-title"><h5><strong>MOVE POLICY TO ANOTHER AGENT</strong></h5></div>
    <div class="container-fluid default-container">
    <div class="main-title col-md-6">
      <form class="form-horizontal">
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>POLICY ID</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
               <h5>12345</h5>
            </div>
      </div>
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>POLICY NO</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <h5>1</h5>
            </div>
      </div>     
    </div>
  </form>
    <div class="main-title col-md-6">
      <form class="form-horizontal">
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>POLICY TYPE</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <h5>POLICY A</h5>
            </div>
      </div>
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PROVIDER</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <h5>PROVIDER A</h5>
            </div>
      </div>     
    </div>
  </form>
  </div>
    <div class=" main-title col-md-6">
      <h5><strong>BEFORE MOVE</strong></h5>
      <div class="container-fluid">
      <form class="form-horizontal">
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>AGENT ID</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero" placeholder="12345">
                </div>
            </div>
      </div>
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>AGENT CODE</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero" placeholder="CODE1234">
                </div>
            </div>
      </div>     
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>USERNAME</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero select" placeholder="stevejobs"disabled>
                </div>
            </div>
      </div>
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>REAL NAME</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero select" placeholder="Bill Gates"disabled>
                </div>
            </div>
      </div>
    </div>
  </form>
</div>
    <div class="container-fluid main-title col-md-6">
      <h5><strong>AFTER MOVE</strong></h5>
      <div class="container-fluid">
      <form class="form-horizontal">
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>AGENT ID</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero" placeholder="67890">
                </div>
            </div>
      </div>
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>AGENT CODE</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero" placeholder="CODE4321">
                </div>
            </div>
      </div>     
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>USERNAME</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero select" placeholder="billgates"disabled>
                </div>
            </div>
      </div>
      <div class="form-group">
        <label for="" class="col-lg-5 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>REAL NAME</strong></p></label>
            <div class="col-lg-7 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                    <input type="text" class="form-control borderzero select" placeholder="Steve Jobs"disabled>
                </div>
            </div>
      </div>
    </div>
  </form>
</div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h5 class="bread-crumb">DATE CHANGED <h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <button type="button" class="btn btn-danger borderzero btn-sm btn-tool pull-right" data-toggle="modal" data-target="myModaldelete"><i class="fa fa-undo"></i> UNDO CHANGES</button>
          <button type="button" class="btn btn-success borderzero btn-sm btn-tool pull-right" data-toggle="modal" data-target="myModalimport"><i class="fa fa-save"></i> SAVE</button> 
    </div>
    </div>
</div>
</div>
</div>
<div id="myModalorphan" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-book"></i> ORPHAN</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to orphan the policy?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>