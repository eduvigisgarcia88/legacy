@extends('layouts.master')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';
  function refresh() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');

    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();
    $status = $("#row-filter_status").val();

        $.post("{{ url('policy/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          
        }, 'json');
  }

  function search() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();
    $status = $("#row-filter_status").val();

    $("#row-export").val($status);

        $.post("{{ url('policy/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
            
        }, 'json');
  }

  function search_policy() {
    
    $('.loading-pane').removeClass('hide');
    $id = $('#search-id').val();
    $date = $('#row-filter_status').val();

    $.post("{{ url('policy/get-policies') }}", { id: $id, date: $date, _token: $_token }, function(response) {

      $('.policies-table').parent().html(response);

      $(function () {
        $(".filter_status").datetimepicker({
            locale: "en",
            format: "MMM YYYY",
            ignoreReadonly: true,
            useCurrent: false
        });
      });

      $(".filter_status").on("dp.change", function(e) {
        search_policy();
      });

      $('.loading-pane').addClass('hide');
        
    }, 'json');

  }


  $(document).ready(function() {

    $loading = false;
    $selectedID = 0;

    $("#page-content-wrapper").on("click", ".btn-expand", function() {
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
        if ($loading){
          return;
        }
        $tr = $(this).parent().parent();
        $upload = $(this).data('upload');
        $id = $(this).parent().parent().data('id');
        $policy = $(this).data('policy');

        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('policy/get-policies') }}", { id: $id, upload: $upload, policy: $policy, _token: $_token }, function(response) {

          $tr.after('<tr><td colspan="1" style="padding:10px"><div class="row-' + $selectedID + '" style="display:none;">' + response + '</div></td></tr>');

          $(function () {
            $(".filter_status").datetimepicker({
                locale: "en",
                format: "MMM YYYY",
                ignoreReadonly: true,
                useCurrent: false
            });
          });

          $(".filter_status").on("dp.change", function(e) {
            search_policy();
          });

          $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
        });

      });

  });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();
    		$status = $("#row-filter_status").val();

        $.post("{{ url('policy/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
            
        }, 'json');
      }
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();
    		$status = $("#row-filter_status").val();

        $.post("{{ url('policy/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
            
        }, 'json');

    });

    $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
    });


</script>

@stop

@section('content')

  <!-- Page Content -->
  <div id="page-content-wrapper" class="response">
    <!-- Title -->
    <style type="text/css">
      .td-hover:hover {
        cursor: pointer;
        background-color: #eeeeee;
      }
    </style>
    <div class="container-fluid main-title-container container-space">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">POLICIES</h3>
        <h5 class="bread-crumb">PRODUCTION & POLICY MANAGEMENT</h5>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        @if (Auth::user()->usertype_id != 8)
          <form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('policy/export') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $export }}" name="export" id="row-export">
            <button type="submit" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-export"><i class="fa fa-print"></i> Export</button>
          </form>
        @endif
      </div>
    </div>
      <div class="container-fluid default-container container-space">
        <div class="col-lg-12">
          <p><strong>FILTER OPTIONS</strong></p>
        </div>
        @if ($supervisor == "no" || $supervisor == "system")
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PAYROLL MONTH</strong></p></label>
              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <select class=" form-control borderzero" onchange="search()" id="row-filter_status" >
                    <option value="">All</option>
                  @foreach ($batches as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                  <div class="input-group">
                  <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                  </span>
                </div>
                </div>
              </div>
            </div>
        @else
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                  <div class="input-group">
                  <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                  </span>
                </div>
                </div>
              </div>
            </div>
        @endif
      </div>
    <div class="container-fluid content-table col-xs-12" style="border-top:1px #e6e6e6 solid;">
      <div class="table-responsive block-content tbblock">
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
        </div> 
        <div class="table-responsive">
          <table class="table table-striped table-pagination">
            <thead class="tbheader">
              @if ($supervisor == 'no')
              <tr>
                <th class="tbheader th-sort" nowrap data-sort="policy_no"><i></i> POLICY NUMBER</th>
                <th class="tbheader th-sort" nowrap data-sort="policy_holder"><i></i> POLICY OWNER NAME</th>
                <th class="tbheader th-sort" nowrap data-sort="product_name"><i></i> PRODUCT NAME</th>
                <th class="tbheader th-sort" nowrap data-sort="cat_name"><i></i> POLICY<br>TYPE</th>
                <th class="tbheader th-sort" nowrap data-sort="net_prem_paid"><i></i> PREMIUM</th>
                <th class="tbheader th-sort" nowrap data-sort="billing_freq"><i></i> PAYMENT<br>FREQUENCY</th>
                <th class="tbheader th-sort" nowrap data-sort="spec_variable_income"><i></i> APE</th>
                <th class="tbheader th-sort" nowrap data-sort="gross_revenue"><i></i> GROSS REV AMT</th>
                <th class="tbheader th-sort" nowrap data-sort="agent_banding"><i></i> AGENT SHARE</th>
              </tr>
              @elseif ($supervisor == 'system')
              <tr>
                <th class="tbheader th-sort" nowrap data-sort="agent_name"><i class="fa fa-sort-down"></i> AGENT NAME</th>
                <th class="tbheader th-sort" nowrap data-sort="policy_no"><i></i> POLICY NUMBER</th>
                <th class="tbheader th-sort" nowrap data-sort="policy_holder"><i></i> POLICY OWNER NAME</th>
                <th class="tbheader th-sort" nowrap data-sort="provider_name"><i></i> PROVIDER</th>
                <th class="tbheader th-sort" nowrap data-sort="compo_code"><i></i> COMPONENT CODE</th>
                <th class="tbheader th-sort" nowrap data-sort="product_name"><i></i> PRODUCT NAME</th>
                <th class="tbheader th-sort" nowrap data-sort="cat_name"><i></i> POLICY<br>TYPE</th>
                <th class="tbheader th-sort" nowrap data-sort="net_prem_paid"><i></i> PREMIUM</th>
                <th class="tbheader th-sort" nowrap data-sort="billing_freq"><i></i> PAYMENT<br>FREQUENCY</th>
                <th class="tbheader th-sort" nowrap data-sort="spec_variable_income"><i></i> APE</th>
                <th class="tbheader th-sort" nowrap data-sort="gross_revenue"><i></i> GROSS REV AMT</th>
                <th class="tbheader th-sort" nowrap data-sort="agent_banding"><i></i> AGENT SHARE</th>
              </tr>
              @else
              <tr>
                <th class="th-sort" nowrap data-sort="name"><i></i> AGENT NAME</th>
              </tr>
              @endif
            </thead>
            <tbody id="rows">
              <?php echo($paginate) ?>
            </tbody>
          </table>
        </div>
      </div>
      <div id="row-pages" class="col-lg-6 text-center borderzero leftalign">{!! $pages !!}</div>
      <div class="col-lg-6 content-table" style="text-align: -webkit-right;">
      @if ($supervisor == 'no' || $supervisor == 'system')
        <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
      @else
        <select class="form-control borderzero rightalign hide" onchange="search()" id="row-per" style="width:70px; border: 0px;">
      @endif
          <option value="10">10</option>
          <option value="20" selected>20</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
      <div class="text-center">
        <input type="hidden" id="row-page" value="1">
        <input type="hidden" id="row-sort" value="">
        <input type="hidden" id="row-order" value="">
      </div>
    </div>
  </div>
  <!-- /#page-content-wrapper -->

@include('policy-management.form')
@stop