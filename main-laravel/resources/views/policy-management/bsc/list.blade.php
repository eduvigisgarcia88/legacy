@extends('layouts.master')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';


  $(function () {
    $('#year-datepicker').datetimepicker({
        locale: 'en',
        defaultDate: '{{ $today_year }}',
        format: 'YYYY',
        ignoreReadonly: true,
        useCurrent: false
    });
  });

  $(function () {
    $('#year-datepicker2').datetimepicker({
        locale: 'en',
        defaultDate: '{{ $today_year_index }}',
        format: 'YYYY',
        ignoreReadonly: true,
        useCurrent: false
    });
  });

  // submit form
    $("#modal-batch").find("form").submit(function(e) {
   
      var form = $("#modal-batch").find("form");
      var loading = $("#load-batch");
      var progress = $("#batch-progress");

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        // console.log(response);
        if(response.error) {

          $(".sup-errors").html("");
          $(".form-control").removeClass("required");
          $.each(response.error, function(index, value) {
            var errors = value.split("|");
            $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
            $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
          });

          loading.addClass("hide");

        } else if(response.nopolicy) {
          status('No Policy', 'No Policies Found.', 'alert-danger');
          loading.addClass("hide");
        } else {

          var ajaxQueue = $({});
          var ctr = -1;

          $.ajaxQueue = function(ajaxOpts) {
            // Hold the original complete function
            var oldComplete = ajaxOpts.complete;

            // Queue our ajax request
            ajaxQueue.queue(function(next) {
              // Create a complete callback to invoke the next event in the queue
              ajaxOpts.complete = function() {
                // Invoke the original complete if it was there
                if (oldComplete) {
                  oldComplete.apply(this, arguments);
                }
                var percent = (((ctr+2) / response.loop) * 100).toFixed(2);

                  progress.attr('aria-valuenow', percent);
                  progress.attr('style', 'width: ' + percent + '%');
                  progress.html(percent + '%');
                
                  ctr = ctr + 1;
                  
                  if ((ctr+1) == response.loop) {
                    progress.html('Updating PFR Links...');
                  }

                  if ((ctr+1) > response.loop) {
                    loading.addClass("hide");
                    status('Success', 'PFR Links successfully updated.', 'alert-success');
                    $("#modal-batch").modal("hide");
                    refresh();
                  }
                // Run the next query in the queue
                next();
              };
              // Run the query
              $.ajax(ajaxOpts);
            });

          };

          for (var i=1; i<=(response.loop + 1); i++) {

            if (i > response.loop) {
              progress.html('Creating Batch...');
              $.ajaxQueue({
                url: response.url_total,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(response_last) {

                }
              });
            } else {
              $.ajaxQueue({
                url: response.url,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(response_last) {

                }

              });
            }
          }
        }
      }

      });

    });




  function refresh() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');

    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();
    $status = $("#row-filter_status").val();
    $year = $("#year-datepicker2").val();

        $.post("{{ url('policy/bsc/refresh') }}", { page: $page, per: $per, year: $year, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          
        }, 'json');
  }

  function search() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();
    $status = $("#row-filter_status").val();

    $("#row-export").val($status);

    $year = $("#year-datepicker2").val();

        $.post("{{ url('policy/bsc/refresh') }}", { page: $page, per: $per, year: $year, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          
        }, 'json');
  }

  function after_save() {
    
    $('.loading-pane').removeClass('hide');
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();
    $status = $("#row-filter_status").val();

    $("#row-export").val($status);

    $year = $("#year-datepicker2").val();

        $.post("{{ url('policy/bsc/refresh') }}", { page: $page, per: $per, year: $year, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          
        }, 'json');
  }


  $(document).ready(function() {

      $("#page-content-wrapper").on("click", ".btn-pfr", function() {

        // reset all form fields
        $("#modal-batch").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $("#row-id").val("");

        $("#row-sales_id").select2("val", "");
        $("#row-policy_id").html("");
        

        $("#modal-batch").find('input').removeAttr('readonly', 'readonly');

        var progress = $("#batch-progress");
        progress.attr('aria-valuenow', 0);
        progress.attr('style', 'width: 0%');
        progress.html("");
        //$(".modal-header").removeAttr("class").addClass("modal-header addnew-bg");
        $(".modal-title").html("<i class='fa fa-link'></i> Update PFR Links");
        $("#modal-batch").modal('show');
      
      });


      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

        // reset all form fields
        $("#modal-bsc").find('form').trigger("reset");
        $("#row-id").val("");


        $.post("{{ url('policy/bsc/view') }}", { id: id, _token: $_token }, function(response) {
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {

            // output form data
            $.each(response, function(index, value) {
              var field = $("#row-" + index);

              if(field.length > 0) {
                  field.val(value);
              }
            });

            $("#modal-bsc").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }
  
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
        }, 'json');

      });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {

      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();
        $status = $("#row-filter_status").val();

        $year = $("#year-datepicker2").val();

        $.post("{{ url('policy/bsc/refresh') }}", { page: $page, per: $per, year: $year, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          
        }, 'json');
      }
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();
        $status = $("#row-filter_status").val();

        $year = $("#year-datepicker2").val();

        $.post("{{ url('policy/bsc/refresh') }}", { page: $page, per: $per, year: $year, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          
        }, 'json');

    });

  });


    $('#row-search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            search();
        }
    });


    $('.btn-filter').click(function(event){
      search();
    });
</script>

@stop


@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">BSC REPORT</h3>
        <h5 class="bread-crumb">SUMMARY AND REPORTS</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('policy/bsc/export') }}">
            {{ csrf_field() }}
              <button type="button" class="btn btn-sm btn-primary pull-right borderzero btn-tool btn-pfr"><i class="fa fa-link"></i> PFR Links</button>
              <div class="input-group pull-right">

                <select class="form-control borderzero" name="quarter" style="line-height: 20px; width: 100px; height: 30px; font-size: 12px; border: 2px solid #5cb85c; padding: 2px;">
                  {{-- <option value="">All</option> --}}
                  <option value="1|3">1st Qtr</option>
                  <option value="4|6">2nd Qtr</option>
                  <option value="7|9">3rd Qtr</option>
                  <option value="10|12">4th Qtr</option>
                </select><input type="text" name="year" id="year-datepicker" style="padding: 4px; border: 1px solid #cccccc !important; max-width: 70px;" readonly="readonly">
                <select class="form-control borderzero hide" name="batch_id" style="width: 100px; height: 30px; font-size: 12px; border: 2px solid #5cb85c; padding: 2px;">
                  @if(!$export)
                  <option value="" selected>All</option>
                  @else
                  <option value="">All</option>
                  @endif
                  @foreach($batches as $row)
                    @if($export == $row->id)
                    <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                    @else
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endif
                  @endforeach
                </select>
                <span>
                  <button type="submit" class="btn btn-sm btn-success pull-right borderzero btn-tool btn-export" style=" margin: 0; border-radius: 0px 4px 4px 0;"><i class="fa fa-print"></i> Export</button>
                </span>
            </div>
          </form>
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>QUARTER</strong></p></label>
              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <select class="borderzero" id="row-filter_status" style="width: 50%; float: left; padding: 5px; line-height: 26px;">
                        <option value="">All</option>
                        <option value="1|3">1st Qtr</option>
                        <option value="4|6">2nd Qtr</option>
                        <option value="7|9">3rd Qtr</option>
                        <option value="10|12">4th Qtr</option>
                    </select>
                    <input type="text" name="year" id="year-datepicker2" style="padding: 4px; border: 1px solid #cccccc !important; width: 35%; float: left;" readonly="readonly">
                    <a class="btn-filter" style="width: 15%; float: left; padding: 4px; background-color: #337ab7; border: 1px solid #337ab7; color: #ffffff; text-align: center; cursor: pointer;"><i class="fa fa-arrow-right"></i></a>
                  </div>
            </div>
          </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hide">
          <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>BATCH NAME</strong></p></label>
              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <select class=" form-control borderzero" onchange="search()" id="row-filter_status" >
                        <option value="">All</option>
                      @foreach ($batches as $row)
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                      @endforeach
                    </select>
                  </div>
            </div>
          </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                        <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                        </span>
                      </div>
                      </div>
                    </div>
                  </div>
      </div>
          <div class="container-fluid content-table col-xs-12" style="border-top:1px #e6e6e6 solid;">
            <div class="table-responsive block-content tbblock">
                <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
                </div> 
            <div class="table-responsive">
              <table class="table table-striped table-pagination">
                <thead class="tbheader">
                  <tr>
                    <th class="th-sort" nowrap data-sort="agent_code"><i></i> AGENT ID</th>
                    <th class="th-sort" nowrap data-sort="agent_name"><i></i> AGENT NAME</th>
                    <th class="th-sort" nowrap data-sort="salesforce_id"><i></i> REPORTING TO</th>
                    <th class="th-sort" nowrap data-sort="supervisor_name"><i></i> REPORTING NAME</th>
                    <th class="th-sort" nowrap data-sort="policy_holder"><i></i> POLICY OWNER</th>
                    <th class="th-sort" nowrap data-sort="provider_name"><i></i> COMPANY NAME</th>
                    <th class="th-sort" nowrap data-sort="compo_code"><i></i> PRODUCT NAME</th>
                    <th class="th-sort" nowrap data-sort="payroll_computations.created_at"><i></i> DATE ISSUE</th>
                    <th class="th-sort" nowrap data-sort="incept_date"><i></i> DATE INCEPTED</th>
                    <th nowrap data-sort="date_effective"><i></i> DATE EFFECTIVE</th>
                    <th nowrap data-sort="date_transferred"><i></i> DATE TRANSFERRED</th>
                    <th nowrap data-sort="date_backdated"><i></i> DATE BACKDATED</th>
                    <th class="th-sort" nowrap data-sort="premium_term"><i></i> POLICY TERM</th>
                    <th class="th-sort" nowrap data-sort="billing_freq"><i></i> FREQUENCY</th>
                    <th class="th-sort" nowrap data-sort="net_prem_paid"><i></i> PREMIUM</th>
                    <th class="th-sort" nowrap data-sort="variable_income"><i></i> VARIABLE INCOME</th>
                    <th class="th-sort" nowrap data-sort="spec_variable_income"><i></i> SPECIFIED VARIABLE INCOME</th>
                    <th class="th-sort" nowrap data-sort="bsc_or_svi"><i></i> OVERRIDES SVI</th>
                    <th class="th-sort" nowrap data-sort="s_or_tier2"><i></i> S OR TIER2</th>
                    <th class="th-sort" nowrap data-sort="s_or_tier3"><i></i> S OR TIER3</th>
                    @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2)
                    <th class="rightalign"><i></i> TOOLS</th>
                    @endif
                  </tr>
                </thead>
                <tbody id="rows">
                  @foreach($rows as $row)
                  <tr data-id="{{ $row->id }}">
                    <td>{{ $row->agent_code }}</td>
                    <td>{{ strtoupper($row->agent_name) }}</td>
                    <td>{{ strtoupper($row->salesforce_id) }}</td>
                    <td>{{ strtoupper($row->supervisor_name) }}</td>
                    <td>{{ strtoupper($row->policy_holder) }}</td>
                    <td>{{ strtoupper($row->provider_name) }}</td>
                    <td>{{ strtoupper($row->compo_code) }}</td>
                    <td>{{ ($row->upload_date ? date_format(date_create(substr($row->upload_date, 0,10)),'d/m/Y') : '') }}</td>
                    <td>{{ ($row->incept_date ? date_format(date_create(substr($row->incept_date, 0,10)),'d/m/Y') : '') }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="rightalign">{{ $row->premium_term }}</td>
                    <td class="rightalign">{{ $row->billing_freq }}</td>
                    <td class="rightalign">{{ number_format($row->net_prem_paid, 2) }}</td>
                    <td class="rightalign">{{ number_format($row->variable_income, 2) }}</td>
                    <td class="rightalign">{{ number_format($row->spec_variable_income, 2) }}</td>
                    <td class="rightalign">{{ number_format($row->bsc_or_svi, 2) }}</td>
                    <td class="rightalign">{{ number_format($row->s_or_tier2, 2) }}</td>
                    <td class="rightalign">{{ number_format($row->s_or_tier3, 2) }}</td>
                    @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2)
                    <td class="rightalign">
                      <button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                    </td>
                    @endif
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
    </div>
      <div id="row-pages" class="col-lg-6 text-center borderzero leftalign">{!! $pages !!}</div>
      <div class="col-lg-6 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
      </select>
      </div>
      <div class="text-center">
        <input type="hidden" id="row-page" value="1">
        <input type="hidden" id="row-sort" value="">
        <input type="hidden" id="row-order" value="">
      </div>
    </div>

@include('policy-management.bsc.form')

@stop