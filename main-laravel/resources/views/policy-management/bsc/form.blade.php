<!-- modal add user -->
<div class="modal fade" id="modal-batch" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-batch" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'policy/bsc/check', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header" style="background-color: #337ab7; border-color: #337ab7;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Create Batch</h4>
              </div>
              <div class="modal-body">
               <div id="batch-notice"></div>
                 <div class="form-group">
                    <label for="batch-id" class="col-sm-2">Batch Name</label>
                  <div class="col-sm-10 error-id">
                    <select name="id" id="batch-id" class="form-control input-md borderzero multi_select" style="width: 100%;" placeholder="Agent">
                        <option value="">All</option>
                      @foreach ($batches as $row)
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                      @endforeach
                    </select> 
                    <sup class=" sup-errors"></sup>
                  </div>
                </div>
            </div>
              <div class="modal-footer borderzero">
            <div class="progress">
              <div id="batch-progress" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0%" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                
              </div>
            </div>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

<div class="modal fade" id="modal-bsc" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog  modal-lg">
            <div class="modal-content borderzero">
                <div id="load-bsc" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'policy/bsc/save-bsc', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header" style="background-color: #337ab7; border-color: #337ab7; color: #ffffff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-pencil"></i> Edit BSC</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="row">
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="form-agent_name" class="col-sm-3">Agent Name</label>
                  <div class="col-sm-9 error-agent_name">
                    <input type="text" name="agent_name" class="form-control borderzero" id="row-agent_name" placeholder="" readonly="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-agent_code" class="col-sm-3">Agent ID</label>
                  <div class="col-sm-9 error-agent_code">
                    <input type="text" name="agent_code" class="form-control borderzero" id="row-agent_code" placeholder="" readonly="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-salesforce_id" class="col-sm-3">Reporting To</label>
                  <div class="col-sm-9 error-salesforce_id">
                    <input type="text" name="salesforce_id" class="form-control borderzero" id="row-salesforce_id" placeholder="" readonly="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-supervisor_name" class="col-sm-3">Reporting Name</label>
                  <div class="col-sm-9 error-supervisor_name">
                    <input type="text" name="supervisor_name" class="form-control borderzero" id="row-supervisor_name" placeholder="" readonly="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-policy_holder" class="col-sm-3">Policy Owner</label>
                  <div class="col-sm-9 error-policy_holder">
                    <input type="text" name="policy_holder" class="form-control borderzero" id="row-policy_holder" placeholder="" readonly="">
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="form-provider_name" class="col-sm-3">Company Name</label>
                  <div class="col-sm-9 error-provider_name">
                    <input type="text" name="provider_name" class="form-control borderzero" id="row-provider_name" placeholder="" readonly="">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-compo_code" class="col-sm-3">Product Name</label>
                  <div class="col-sm-9 error-compo_code">
                    <input type="text" name="compo_code" class="form-control borderzero" id="row-compo_code" placeholder="" readonly="">
                  </div>
                </div>
                 <div class="form-group date">
                  <label for="form-upload_date" class="col-sm-3">Date Issue</label>
                  <div class="col-sm-9 error-upload_date">
                    <input type="text" name="upload_date" class="form-control borderzero" id="row-upload_date" placeholder="" readonly="">
                  </div>
                </div>
                 <div class="form-group date">
                  <label for="form-incept_date" class="col-sm-3">Date Incepted</label>
                  <div class="col-sm-9 error-incept_date">
                    <input type="text" name="incept_date" class="form-control borderzero" id="row-incept_date" placeholder="" readonly="">
                  </div>
                </div>

                 <div class="form-group">
                  <label for="form-premium" class="col-sm-3">Premium</label>
                  <div class="col-sm-9 error-premium">
                    <input type="text" name="premium" class="form-control borderzero" id="row-premium" placeholder="" readonly="">
                  </div>
                </div>
                </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6"">
                 <div class="form-group">
                  <label for="form-variable" class="col-sm-4">Variable</label>
                  <div class="col-sm-8 error-variable">
                    <input type="text" name="variable" class="form-control borderzero" id="row-variable" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-variable_income" class="col-sm-4">Variable Income</label>
                  <div class="col-sm-8 error-variable_income">
                    <input type="text" name="variable_income" class="form-control borderzero" id="row-variable_income" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-spec_variable_income" class="col-sm-4"> Specified Variable Income</label>
                  <div class="col-sm-8 error-spec_variable_income">
                    <input type="text" name="spec_variable_income" class="form-control borderzero" id="row-spec_variable_income" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-svi" class="col-sm-4"> Overrides SVI</label>
                  <div class="col-sm-8 error-svi">
                    <input type="text" name="svi" class="form-control borderzero" id="row-svi" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-bsc_or_svi" class="col-sm-4"> BSC OR SVI</label>
                  <div class="col-sm-8 error-bsc_or_svi">
                    <input type="text" name="bsc_or_svi" class="form-control borderzero" id="row-bsc_or_svi" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group hide">
                  <label for="form-total_override" class="col-sm-4"> Total Override</label>
                  <div class="col-sm-8 error-total_override">
                    <input type="text" name="total_override" class="form-control borderzero" id="row-total_override" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                  <label for="form-variable_or" class="col-sm-4"> Variable OR</label>
                  <div class="col-sm-8 error-variable_or">
                    <input type="text" name="variable_or" class="form-control borderzero" id="row-variable_or" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-variable_or_tier2" class="col-sm-4"> Variable OR Tier2</label>
                  <div class="col-sm-8 error-variable_or_tier2">
                    <input type="text" name="variable_or_tier2" class="form-control borderzero" id="row-variable_or_tier2" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-variable_or_tier3" class="col-sm-4"> Variable OR Tier3</label>
                  <div class="col-sm-8 error-variable_or_tier3">
                    <input type="text" name="variable_or_tier3" class="form-control borderzero" id="row-variable_or_tier3" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-s_or" class="col-sm-4"> S OR</label>
                  <div class="col-sm-8 error-s_or">
                    <input type="text" name="s_or" class="form-control borderzero" id="row-s_or" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-s_or_tier2" class="col-sm-4"> S OR Tier2</label>
                  <div class="col-sm-8 error-s_or_tier2">
                    <input type="text" name="s_or_tier2" class="form-control borderzero" id="row-s_or_tier2" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="form-s_or_tier3" class="col-sm-4"> S OR Tier3</label>
                  <div class="col-sm-8 error-s_or_tier3">
                    <input type="text" name="s_or_tier3" class="form-control borderzero" id="row-s_or_tier3" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                </div>
                </div>
            </div>
              <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>