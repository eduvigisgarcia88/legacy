 @extends('layouts.master')

 @section('content')
    <!-- Page Content -->
 <div id="page-content-wrapper" class="response">
    <div class="container-fluid">
    <!-- Title -->
    <div class="row main-title-container container-space">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
            <h3 class="main-title">POLICY MANAGEMENT</h3>
            <h5 class="bread-crumb">POLICY MANAGEMENT > VIEW <h5>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
              <a href="policy-management-policy.html" type="button" class="btn btn-success borderzero btn-tool pull-right"><i class="fa fa-chevron-left"></i> BACK</a>     
              <a type="button" href="policy-management-edit.html" class="btn btn-info borderzero btn-tool pull-right"><i class="fa fa-pencil"></i> Edit</a>          
        </div>
        </div>
              <div id="basicinfo" class="row default-container container-space">
                      <div class="col-lg-12">
                          <p><strong>BASIC INFORMATION</strong></p>
                      </div>
                      <div class="col-lg-1">
                          <img src="{{ url('images/avatar.png') }}" style="color: #79828f; margin:0px auto;" class="img-circle" alt="Smiley face" height="100" width="100">
                      </div>
                      <div class="col-lg-11">
                      <form class="form-horizontal">
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">AGENT ID</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="12345" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">DESIGNATION</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Sales Agent" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">PHOTO</label>
                              <div class="input-group col-lg-6 col-md-6 col-sm-8 col-xs-8" style="padding-right: 14px; padding-left: 15px;">
                                <input type="text" class="form-control borderzero" placeholder="default.jpg" disabled>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">AGENT CODE</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="sales2015" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">USER NAME</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="SLS-12345" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">AGENT NAME</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Steve Jobs" disabled>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">EMAIL</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="stevejobs@gmail.com" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">MOBILE</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="+65 123 4567" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left">STATUS</label>
                              <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                <input type="" class="form-control borderzero" id="inputEmail3" placeholder="ACTIVE" disabled>
                              </div>
                          </div>
                      </div>
                      </form>
          </div>
          </div>
          <div class="row content-table">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  <h3 class="main-title">POLICY SUMMARY</h3>
</div>
</div>
<div class="content-table">
  <p><strong>
  POLICY INFORMATION<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#4" aria-expanded="false" aria-controls="collapseTwo"></i></p>
  </p></strong>
<div id="4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
      <div class="container-fluid">
                <form class="form-horizontal" >     
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY ID</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Filter Policy ID" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY NO</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Filter Policy NO" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY TYPE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMPONENT CODE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY TERM</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>CONTRACT CURRENCY</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>SUM INSURED</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>INCEPT DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>INSTALLMENT FORM DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>BILLING FREQUENCY</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>POLICY EXPIRY DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>GROSS PREMIUM PAID</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>NET PREMIUM PAID</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>GROSS PREMIUM INC GST</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>NET PREMIUM INC GST</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PREMIUM W/O COM</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PREMIUM CONV RATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PAYMENT CURRENCY</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMMISION OR</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMMISION RUN DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>COMMISION CONV RATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TRANSACTION CODE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>PREMIUM TERM</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>DATE OF ISSUE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Feed Info"disabled>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="content-table">
                    <p><strong>
                    INTRODUCER ADJUSTMENT<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#5" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                    </p></strong>
                    <div id="5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="container-fluid">
                                <div class="table-responsive">     
                                      <table class="table">
                                        <thead id="policyfirstthead">
                                          <tr>
                                            <th>INTRODUCER ID</th>
                                            <th>INTRODUCER NAME</th>
                                            <th>REMARKS</th>
                                            <th>DATE OF ENTRY</th>
                                            <th>TOOLS</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-table">
                    <p><strong>
                    ADMIN PRE-PAYROLL ENTRIES<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#6" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                    </p></strong>
                    <div id="6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="container-fluid">
                                <div class="table-responsive">     
                                      <table class="table">
                                        <thead id="policyfirstthead">
                                          <tr>
                                            <th>INTRODUCER ID</th>
                                            <th>INTRODUCER NAME</th>
                                            <th>REMARKS</th>
                                            <th>DATE OF ENTRY</th>
                                            <th>TOOLS</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-table">
                    <p><strong>
                    PAYROLL PREVIEW<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#7" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                    </p></strong>
                    <div id="7" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="container-fluid">
                                <form class="form-horizontal" >     
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL INCENTIVES</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Incentives"disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL DEDUCTIONS</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Deductions"disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total"disabled>
                                        </div>
                                    </div>
                               </div>
                        </form>
                    </div>
                    <div class="rightalign col-lg-12" style="padding: 0px; margin-bottom: 10px;">
                        <hr>
                        <a href="policy-management-policy.html" type="button" class="btn btn-success borderzero btn-tool pull-right"><i class="fa fa-chevron-left"></i> BACK</a>     
                        <a type="button" href="policy-management-edit.html" class="btn btn-info borderzero btn-tool pull-right"><i class="fa fa-pencil"></i> Edit</a>
                    </div>    
              </div>
        </div>                                                
    </div>
</div>
<!-- /#page-content-wrapper -->

@stop