@extends('layouts.master')

@section('scripts')
<script>

$_token = '{{ csrf_token() }}';
    // refresh the list
    function refresh() {
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);

      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";

        $.each(response.rows.data, function(index, row) {
           console.log(response);
            body += '<tr data-id="' + row.id + '">'+
                '<td>' + row.name + '</td>' + 
                '<td>' + row.contract_no + '</td>' + 
                '<td>' + moment(row.incept_date).format('MM/DD/YYYY') + '</td>' +
                '<td class="rightalign">'+
                  '<a href="{{ url("policy") . "/" }}'+ row.id + '/view" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                  '&nbsp;<a href="{{ url("policy/orphan-policies/edit") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>'+
                  '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>'+
                  '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>'+ 
                  '&nbsp;<input id="checkbox" type="checkbox" value="">'+
                '</td>' +
            '</tr>';

        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $('#row-pages').html(response.pages);

        loading.addClass("hide");

      }, 'json');

    }

    function search() {
      var loading = $(".loading-pane");
      var table = $("#rows");

      $("#row-page").val(1);

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";

        $.each(response.rows.data, function(index, row) {
           console.log(response);
            body += '<tr data-id="' + row.id + '">'+
                '<td>' + row.name + '</td>' + 
                '<td>' + row.contract_no + '</td>' + 
                '<td>' + moment(row.incept_date).format('MM/DD/YYYY') + '</td>' +
                '<td class="rightalign">'+
                  '<a href="{{ url("policy") . "/" }}'+ row.id + '/view" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                  '&nbsp;<a href="{{ url("policy/orphan-policies/edit") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>'+
                  '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>'+
                  '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>'+ 
                  '&nbsp;<input id="checkbox" type="checkbox" value="">'+
                '</td>' +
                '</tr>';
        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $('#row-pages').html(response.pages);

        loading.addClass("hide");

      }, 'json');

    }

     $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();
        //$search = $("#row-search").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ $refresh_route }}", { page: $page,_token: $_token, per: $per }, function(response) {
         
          // clear
          table.html("");
          var body = "";

          $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '">' +
                  '<td>' + row.name + '</td>' + 
                  '<td>' + row.contract_no + '</td>' + 
                  '<td>' + moment(row.incept_date).format('MM/DD/YYYY') + '</td>' +
                  '<td class="rightalign">'+
                    '<a href="{{ url("policy") . "/" }}'+ row.id + '/view" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                    '&nbsp;<a href="{{ url("policy/orphan-policies/edit") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>'+
                    '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>'+
                    '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>'+ 
                    '&nbsp;<input id="checkbox" type="checkbox" value="">'+
                  '</td>' +
                '</tr>';
          });

          table.html(body);
          $('#row-pages').html(response.pages);
          $(".tablesorter").trigger('update');

          loading.addClass("hide");

        }, 'json');
      }
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {
      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        table.html("");
        var body = "";

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '">' +
                  '<td>' + row.name + '</td>' + 
                  '<td>' + row.contract_no + '</td>' + 
                  '<td>' + moment(row.incept_date).format('MM/DD/YYYY') + '</td>' +
                  '<td class="rightalign">'+
                    '<a href="{{ url("policy") . "/" }}'+ row.id + '/view" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                    '&nbsp;<a href="{{ url("policy/orphan-policies/edit") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>'+
                    '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>'+
                    '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>'+ 
                    '&nbsp;<input id="checkbox" type="checkbox" value="">'+
                  '</td>' +
                '</tr>';

        });
        
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');

    });

</script>
@stop

@section('content')

<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">ORPHAN POLICY SERVICE</h3>
        <h5 class="bread-crumb">PRODUCTION & POLICY MANAGEMENT</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-minus"></i> Remove</button>
          <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i> Disable</button>    
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
  <form class="form-horizontal" >     
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
                <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH OPTION</strong></p></label>
                        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                              <select class=" form-control borderzero" id="sel1" >
                                <option>All Providers</option>
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                                <option>D</option>
                              </select>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                          <input type="text" class="form-control borderzero " placeholder="Search for..." onkeyup="search()" id="row-search">
                        </div>
                    </div>
                </div>
              </form>
            </div>
    <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
        <div class="table-responsive block-content tbblock col-xs-12">
         <div class="loading-pane hide">
            <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
         </div>
          <div class="table-responsive">
            <table class="table table-pagination" id="my-table">
                <thead class="tbheader">
                  <tr>
                    <th class="th-sort" data-sort="name"><i></i> PROVIDER</th>
                    <th class="th-sort" data-sort="contract_no"><i></i> POLICY NUMBER</th>
                    <th class="th-sort" data-sort="incept_date"><i></i> INCEPT DATE</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody id="rows">
                  @foreach($rows as $row)
                  <tr data-id="{{ $row->id }}">
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->contract_no }}</td>
                    <td>{{ date_format(date_create(substr($row->incept_date, 0,10)),'m/d/Y') }}</td>
                    <td class="rightalign">
                      <a href="{{ url('policy/' . $row->id . '/view') }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                      <a href="{{ url('policy/orphan-policies/edit') }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                      <button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>
                      <button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button> 
                      <input id="checkbox" type="checkbox" value="">
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
        <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
        <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
      </div>
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>   
      </div>

    <!-- /#page-content-wrapper -->
@include('policy-management.orphan-policies.form')

@stop