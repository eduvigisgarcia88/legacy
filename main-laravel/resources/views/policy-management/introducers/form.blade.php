<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
           <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
           </div>
          {!! Form::open(array('url' => 'policy/introducers/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
           <div class="modal-header tbheader">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class='fa fa-adjust'></i>Modal title</h4>
           </div>
           <div class="modal-body">
           <div id="form-notice"></div>
            @if (Auth::user()->usertype_id != 8)
             <div class="form-group">
                <label for="row-user_sales_id" class="col-sm-3">Agent</label>
              <div class="col-sm-9 error-user_sales_id">
                <select name="user_sales_id" id="row-user_sales_id" class="form-control input-md borderzero multi_select" style="width: 100%;">
                  <option value="" class="hide"></option>
                  @foreach($sales as $row)
                    <option value="{{ $row->id }}">[{{ $row->code }}] {{ $row->name }}</option>
                  @endforeach
                </select> 
                <sup class=" sup-errors"></sup>
              </div>
            </div>
            @else
            <input type="hidden" name="user_sales_id" class="form-control borderzero" id="row-user_sales_id" maxlength="30" value="NONE">
            @endif
            <div class="form-group">
              <label for="row-code" class="col-sm-3">Introducer Code</label>
            <div class="col-sm-9 error-code">
              <input type="text" name="code" class="form-control borderzero" id="row-code" maxlength="30">
              <sup class=" sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-name" class="col-sm-3">Introducer Name</label>
            <div class="col-sm-9 error-name ">
              <input type="text" name="name" class="form-control borderzero" id="row-name" maxlength="30">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-email" class="col-sm-3">Email</label>
            <div class="col-sm-9 error-email">
              <input type="email" name="email" class="form-control borderzero" id="row-email" maxlength="50">
              <sup class=" sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
                <label for="row-zipcode" class="col-sm-3">ZIP Code</label>
            <div class="col-sm-9 error-zipcode">
                <input type="text" name="zipcode" class="form-control borderzero" id="row-zipcode" maxlength="30">
                <sup class=" sup-errors"></sup>
            </div>
            </div>
            <div class="assign-form">
            <hr>
            <h5>ASSIGN POLICY<small> (NOT REQUIRED)</small></h5>
            <br>
            <div class="form-group">
              <label for="row-provider_id2" class="col-sm-3">Provider</label>
            <div class="col-sm-9">
              <select name="provider_id2" id="row-provider_id2" class="form-control input-md borderzero" style="width: 100%;">
                  <option value="" class="hide">Select</option>
                  @foreach($provider_user as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                  @endforeach
              </select> 
            </div>
            </div>
            <div class="form-group">
              <label for="row-policy_id2" class="col-sm-3">Policy</label>
            <div class="col-sm-9">
              <select name="policy_id2" id="row-policy_id2" class="form-control input-md borderzero multi_select" style="width: 100%;">

              </select> 
            </div>
            </div>
           
            <div class="form-group">
                <label for="row-remarks2" class="col-sm-3">Remarks</label>
            <div class="col-sm-9">
                <textarea name="remarks2" class="form-control borderzero" id="row-remarks2"></textarea>
            </div>
            </div>
            <div class="form-group">
                <label for="row-amount2" class="col-sm-3">Percentage</label>
            <div class="col-sm-9">
                <div class="input-group">
                  <input type="text" name="amount2" class="form-control borderzero" id="row-amount2" maxlength="30" placeholder="">
                  <span class="input-group-addon">
                    <span>%</span>
                  </span>
                </div>
            </div>
            </div>
            </div>
            </div>
            <div class="modal-footer borderzero">
            <input type="hidden" name="id" id="row-id" value="">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

<div class="modal fade" id="modal-assign" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
           <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
           </div>
          {!! Form::open(array('url' => 'policy/introducers/assign', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
           <div class="modal-header tbheader">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="assign-title">Assign Policy</h4>
           </div>
           <div class="modal-body">
           <div id="assign-notice"></div>
            <div class="form-group">
              <label for="row-provider_id" class="col-sm-3">Provider</label>
            <div class="col-sm-9">
              <select name="provider_id" id="row-provider_id" class="form-control input-md borderzero" style="width: 100%;">

              </select> 
            </div>
            </div>
            <div class="form-group">
              <label for="row-policy_id" class="col-sm-3">Policy</label>
            <div class="col-sm-9">
              <select name="policy_id" id="row-policy_id" class="form-control input-md borderzero multi_select" style="width: 100%;">

              </select> 
            </div>
            </div>
           
            <div class="form-group">
                <label for="row-remarks" class="col-sm-3">Remarks</label>
            <div class="col-sm-9">
                <textarea name="remarks" class="form-control borderzero" id="row-remarks"></textarea>
            </div>
            </div>
            <div class="form-group">
                <label for="row-amount" class="col-sm-3">Percentage</label>
            <div class="col-sm-9">
                <div class="input-group">
                  <input type="text" name="amount" class="form-control borderzero" id="row-amount" maxlength="30" placeholder="">
                  <span class="input-group-addon">
                    <span>%</span>
                  </span>
                </div>
            </div>
            </div>
            </div>
            <div class="modal-footer borderzero">
            <input type="hidden" name="introducer_id" id="row-introducer_id" value="">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>

  <!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

  <div id="myModaldelete" class="modal fade" role="dialog">
      <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><i class="fa fa-trash"></i> Delete</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to remove?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default borderzero btn-yes" data-dismiss="modal">Yes</button>
            <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>
