@extends('layouts.master')

@section('scripts')

<script type="text/javascript">

$selectedID = 0;
$loading = false;

function refresh() {
 $_token = '{{ csrf_token() }}';
      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");

      $.post("{{ url('policy/introducers/refresh') }}", { _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        
        // //clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows, function(index, row) {
            table.append(
              '<tr data-id="' + row.id + '"' + ((row.status == 1) ? '' : 'class="tr-disabled"') + '>' +
                '<td class="hide">' + row.status + '</td>' +
                '<td>' + '<a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a>&nbsp;' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.get_user.name + '</td>' +
                '<td>' + row.email + '</td>' + 
                '<td>' + row.zipcode + '</td>' + 
                '<td class="rightalign">' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                  // '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                  '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">' +
                '</td>' +
              '</tr>'
            );
        })

        $(".tablesorter").trigger('update');

        // populate cache
        // $('#my-table').find('tbody tr').each(function() {
        //   $('#my-table').dataTable().fnAddData(this);
        // });

      $selectedID = 0;
      
      loading.addClass("hide");
      }, 'json');

}

    // function initTable() {
    //   //datatable
    //   myTable = $('#my-table').dataTable({
    //           "aoColumnDefs": [{ 
    //             'bSortable': false, 
    //             'aTargets': [1, 5],
    //           }],
    //           "dom": '<"top"f>rt<"bottom"lip><"clear">'
    //     });

    //     // show table
    //     $('.content-table').removeClass('hide');

    //     $('#kwd_search2').change(function(){
    //         myTable.fnFilter($(this).val(), 0);
    //     });

    //     $('#kwd_search').keyup(function(){
    //         myTable.fnFilter($(this).val());
    //     });
    // }


      //search/filter
        // $('#search-status').change(function(){
        //     myTable.fnFilter($(this).val(), 0);
        // });

        // $('#search-rank').change(function(){
        //     myTable.fnFilter($(this).val(), 3);
        // });

        // $('#search').keyup(function(){
        //     myTable.fnFilter($(this).val());
        // });
  
  $(document).ready(function() {

    // initTable();

    //refresh();
    
    $(".multi_select").select2();
    //add user
    $(".btn-add").click(function() {

      // reset all form fields
      $(".assign-form").removeClass("hide");
     
      $("#row-user_sales_id").select2("val", "");
      @if (Auth::user()->usertype_id != 8)
      $("#row-provider_id2").html("");
      @endif
      
      $("#row-policy_id2").html("");
      $("#row-policy_id2").select2("val", "");

      $("#modal-form").find('form').trigger("reset");
      $("#row-id").val("");

      $("#modal-form").find('.hide-view').removeClass('hide');
      $("#modal-form").find('.show-view').addClass('hide');
      $("#modal-form").find('.preview').addClass('hide');
      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
      $("#modal-form").modal('show');

    });

    $("#row-provider_id").on("change", function() {
      $_token = "{{ csrf_token() }}";
      var user = $('#row-user_sales_id').val();
      var provider = $('#row-provider_id').val();
      var policy = $("#row-policy_id");

      var id = $("#row-introducer_id").val();
      $.post("{{ url('policy/introducers/get-policies') }}", { id: id, user: user, provider: provider, _token: $_token }, function(response) {
        console.log(response);
        policy.append(response);
       
      }, 'json');
    });
     $("#row-provider_id2").on("change", function() {
      $_token = "{{ csrf_token() }}";
      var user = $('#row-user_sales_id').val();
      var provider = $('#row-provider_id2').val();
      var policy2 = $("#row-policy_id2");
      var id = $("#row-introducer_id").val();
      $.post("{{ url('policy/introducers/get-policies') }}", { user: user, provider: provider, _token: $_token }, function(response) {
        console.log(response);
        policy2.append(response);
       
      }, 'json');
    });

     $("#row-user_sales_id").on("change", function() {

         var user = $("#row-user_sales_id").val();
    
        $_token = "{{ csrf_token() }}";
        var provider2 = $("#row-provider_id2");
        provider2.html("");
       
        $.post("{{ url('policy/introducers/get-providers') }}", { user: user, _token: $_token }, function(response) {

          provider2.append(response);

        }, 'json');

    });

    //add user
    $("#page-content-wrapper").on("click", ".btn-assign", function() {

      // var policy = $('#row-policy_id');
      // $_token = "{{ csrf_token() }}";
      var id = $(this).data('id');
      // reset all form fields
      $("#modal-assign").find('form').trigger("reset");
      $("#row-introducer_id").val(id);

      $("#row-user_sales_id").select2("val", "");
      $("#row-policy_id").html("");
      $("#row-policy_id").select2("val", "");

      $("#modal-assign").find('.hide-view').removeClass('hide');
      $("#modal-assign").find('.show-view').addClass('hide');
      $("#modal-assign").find('.preview').addClass('hide');
      $("#modal-assign").find('input').removeAttr('readonly', 'readonly');

      $_token = "{{ csrf_token() }}";
      var provider = $("#row-provider_id");
      provider.html("");

      $.post("{{ url('policy/introducers/get-providers') }}", { id: id, _token: $_token }, function(response) {

        provider.append(response);

      }, 'json');

      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
      $("#modal-assign").modal('show');

    });

    $("#page-content-wrapper").on("click", ".btn-assign-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";


        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          // reset all form fields
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-assign").find('form').trigger("reset");
          $("#row-id").val("");


              $.post("{{ url('policy/introducers/assign-view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title

                  
                      $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View Assigned Policy</strong>');
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);


                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                          }
                      });

                      $("#modal-assign").find('.hide-view').addClass('hide');
                      $("#modal-assign").find('.preview').removeClass('hide');
                      $("#modal-assign").find('.change').addClass('hide');
                      $("#modal-assign").find('.show-view').removeClass('hide');
                      $("#modal-assign").find('input').attr('readonly', 'readonly');
                      // show form
                      $("#modal-assign").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
      });

    $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";


        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          // reset all form fields
          $("#row-user_sales_id").prop('disabled', true);
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-form").find('form').trigger("reset");
          $("#row-id").val("");

              $.post("{{ url('policy/introducers/view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                  console.log(response);
                  
                      $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View System User</strong>');
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);

                          if (index == "user_id") {
                            $("#row-user_sales_id").select2("val", value);
                          }
                          // field exists, therefore populate
                          if(field.length > 0) {

                              field.val(value);
                          }
                      });

                      $("#modal-form").find('.hide-view').addClass('hide');
                      $("#modal-form").find('.preview').removeClass('hide');
                      $("#modal-form").find('.change').addClass('hide');
                      $("#modal-form").find('.show-view').removeClass('hide');
                      $("#modal-form").find('input').attr('readonly', 'readonly');
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
      });

                //edit provider
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        $(".assign-form").addClass("hide");
        // reset all form fields
        $("#row-user_sales_id").prop('disabled', false);
        $("#modal-form").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";


        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          $.post("{{ url('policy/introducers/view') }}", { id: id, _token: $_token }, function(response) {
            if(!response.error) {
            // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Provider</strong>');

              // output form data
              $.each(response, function(index, value) {
                var field = $("#row-" + index);

                if(index == "user_type") {
                  $("#row-usertype_id-static").val(value.type_name);
                }

                if (index == "user_id") {
                  $("#row-user_sales_id").select2("val", value);
                }

                // field exists, therefore populate
                if(field.length > 0) {
                    field.val(value);
                }
              });

              $("#modal-form").find('.hide-view').removeClass('hide');
              $("#modal-form").find('.show-view').addClass('hide');
              $("#modal-form").find('.preview').removeClass('hide');
              $("#modal-form").find('.change').addClass('hide');
              $("#modal-form").find('input').removeAttr('readonly', 'readonly');
              // show form
              $("#modal-form").modal('show');
            } else {
              status(false, response.error, 'alert-danger');
            }

            btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
          }, 'json');
        
      });

      $("#page-content-wrapper").on("click", ".btn-expand", function() {
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
        if ($loading){
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');

        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
            
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('policy/introducers/assign-view') }}", { id: $id, _token: $_token }, function(response) {

          
          $tr.after('<tr><td colspan="6" style="padding:10px"><div class="row-' + $selectedID + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
        });

      });

      // disable user
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Account Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('policy/introducers/disable') }}", id);
      });

      // enable user
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Account Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('policy/introducers/enable') }}", id);
      });

      // show modal for multiple disable
      $("#page-content-wrapper").on("click", ".btn-disable-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Disable");
        $(".modal-multiple-body").html("Are you sure you want to disable the selected introducer/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected introducer/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
        console.log('multiple');

        
        if (ids.length == 0) {
          status("Error", "Select introducers first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('policy/production/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('policy/production/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });

      // Delete File 
      $('#myModaldelete').on('click', '.btn-yes', function() {

        $.post("{{ url('policy/introducers/delete') }}", { id: $selectedIDDelete, _token: $_token }, function(response) {

          toastr[response.type](response.body);
          refresh();

        });

      });

      // Set Delete ID
      $("#page-content-wrapper").on('click', '.btn-delete', function() {
        $selectedIDDelete = $(this).parent().parent().data('id');
      });

  });

</script>

@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">INTRODUCERS</h3>
        <h5 class="bread-crumb">PRODUCTION & POLICY MANAGEMENT</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <button type="button" class="btn btn-danger borderzero btn-sm btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
          <!-- <button type="button" class="btn btn-info borderzero btn-tool btn-sm pull-right btn-disable-select"><i class="fa fa-adjust"></i> Disable</button>      -->
          <button type="button" class="btn btn-warning borderzero btn-tool btn-sm pull-right btn-add"><i class="fa fa-plus"></i> Add</button>
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="i">
                        <input type="text" class="form-control borderzero" id="row-search" onkeyup="search()" placeholder="Search for...">
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
      <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
      <div class="table-responsive block-content tbblock">
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
          </div>
          <div class="table-responsive">
            <table class="table" id="my-table">
                <thead class="tbheader">
                    <tr>
                      <th nowrap><i class="fa fa-sort"></i> INTRODUCER CODE</th>
                      <th nowrap><i class="fa fa-sort"></i> INTRODUCER NAME</th>
                      <th nowrap><i class="fa fa-sort"></i> AGENT NAME</th>
                      <th nowrap><i class="fa fa-sort"></i> EMAIL</th>
                      <th nowrap><i class="fa fa-sort"></i> ZIPCODE</th>
                      <th class="rightalign">TOOLS</th>
                    </tr>
                  </thead>
                  <tbody id="rows">
                  @foreach($rows as $row)
                    <tr data-user="{{ $row->user_id }}" data-id="{{ $row->id }}" {{ ($row->status == 1) ? '' : 'class=tr-disabled' }}>
                      <td class="hide">{{ $row->status }}</td>
                      <td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> {{ $row->code }}</td>
                      <td>{{ $row->name }}</td>
                      <td>{{ $row->getUser->name }}</td>
                      <td>{{ $row->email }}</td>
                      <td>{{ $row->zipcode }}</td>
                      <td class="rightalign">
                        <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-eye"></i></button>
                        <button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>
                        <!-- <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}"><i class="fa fa-{{ ($row->status == 1) ? 'adjust' : 'check-circle' }}"></i></button> -->
                        <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

@include('policy-management.introducers.form')

@stop