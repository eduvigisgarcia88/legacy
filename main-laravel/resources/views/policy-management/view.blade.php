 @extends('layouts.master')

 @section('scripts')
  <script>

  </script>
@stop

 @section('content')
    <!-- Page Content -->
 <div id="page-content-wrapper" class="response">
    <div class="container-fluid">
    <!-- Title -->
    <div class="row main-title-container container-space">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
            <h3 class="main-title">POLICY MANAGEMENT</h3>
            <h5 class="bread-crumb">POLICY MANAGEMENT > VIEW </h5>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
              <a href="{{ url('policy') }}" type="button" class="btn btn-success borderzero btn-tool pull-right"><i class="fa fa-chevron-left"></i> BACK</a>     
             <!--  <a type="button" href="policy-management-edit.html" class="btn btn-info borderzero btn-tool pull-right"><i class="fa fa-pencil"></i> Edit</a>    -->       
        </div>
        </div>
              <div id="basicinfo" class="row default-container container-space">
              @if ($policy->user_id)
        <div class="col-lg-12">
            <p><strong>BASIC INFORMATION</strong></p>
        </div>
            <div class="content-table text-center col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <img src="{{ url('uploads/',$row->photo) }}" class="img-responsive img-circle" alt="Generic placeholder thumbnail" id="img-photo">
            </div>
            <div class="content-table row col-lg-5 col-md-5 col-sm-5 col-xs-12">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">AGENT ID</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-sales_id">{{ $row->salesInfo->sales_id }}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">AGENT CODE</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-code">{{ $row->code }}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">AGENT NAME</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">   
                      <span id="label-name">{{ $row->name }}</span>
                    </div>
                </div>
              </div>
            </div>
            <div class="content-table row col-lg-5 col-md-5 col-sm-5 col-xs-12">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">STATUS</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      @if($row->status == 1) 
                        <span id="label-static-status">ACTIVE</span>
                      @elseif($row->status == 0)
                        <span id="label-static-status">DISABLED</span>
                      @else
                        <span id="label-static-status">REMOVED</span>
                      @endif
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">DESIGNATION</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-designation">{{ $row->salesInfo->designations->designation }}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">EMAIL</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-email">{{ $row->email}}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">CONTACT</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-mobile">{{ $row->mobile }}</span>
                    </div>
                </div>
              </div>
            </div>
              @else
                POLICY IS ORPHAN
              @endif
          </div>
          <div class="row content-table">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  <h3 class="main-title">POLICY SUMMARY</h3>
</div>
</div>
<div class="content-table">
  <p><strong>
  POLICY INFORMATION<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#4" aria-expanded="false" aria-controls="collapseTwo"></i></p>
  </p></strong>
<div id="4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
      <div class="container-fluid">
                <form class="form-horizontal" >     
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>STATEMENT FROM DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->state_from_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->state_from_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>STATEMENT TO DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->state_to_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->state_to_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>TRANSACTION DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->transaction_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->transaction_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>AGENT CODE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->agent_code == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->agent_code }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>CONTRACT NUMBER</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->contract_no == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->contract_no }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>POLICY TYPE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->policy_type == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->policy_type }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>COMPONENT CODE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->compo_code == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->compo_code }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>POLICY TERM</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->policy_term == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->policy_term }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>COMMISSION ADJ AMOUNT</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->comm_adj_amount == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->comm_adj_amount }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>COMMISSION RUN DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->comm_run_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->comm_run_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>COMMISSION CONV RATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->comm_conv_rate == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->comm_conv_rate }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>CONTRACT CURRENCY</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->contract_currency == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->contract_currency }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>SUM INSURED</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->sum_insured == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->sum_insured }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>INCEPT DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->incept_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->incept_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>INSTALLMENT FROM DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->inst_from_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->inst_from_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>BILLING FREQUENCY</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->policy_exp_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->policy_exp_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>GROSS PREMIUM PAID</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->gross_prem_paid == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->gross_prem_paid }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>GROSS FEE AMOUNT</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->net_prem_paid == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->net_prem_paid }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>NET PREMIUM PAID</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->gross_fee_amt == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->gross_fee_amt }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>PREMIUM TERM</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->premium_term == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->premium_term }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>ISSUE DATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->issue_date == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->issue_date }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>ADJUSTMENT AMOUNT</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->adj_amt == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->adj_amt }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>NET FEE AMOUNT</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->net_fee_amt == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->net_fee_amt }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>GROSS PREMIUM GST AMOUNT</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->gross_prem_gst_amt == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->gross_prem_gst_amt }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>NET PREMIUM GST AMOUNT</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->net_prem_gst_amt == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->net_prem_gst_amt }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>PREMIUM W/O COMMISSION</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->prem_wo_commission == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->prem_wo_commission }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>PREMIUM CONV RATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->prem_conv_rate == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->prem_conv_rate }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>FEE CONV RATE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->fee_conv_rate == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->fee_conv_rate }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>PAYMENT CURRENCY</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->payment_currency == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->payment_currency }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>COMMISSION OR</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->comm_or == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->comm_or }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>FUND CODE</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->fund_code == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->fund_code }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>TRAILER FEE AMOUNT</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->trailer_fee_amt == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->trailer_fee_amt }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4" style="text-align:left"><small>POLICY HOLDER NRIC NUMBER</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                            @if ($policy->policy_nric == null)
                                            <span>N/A</span>
                                            @else
                                            <span>{{ $policy->policy_nric }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="content-table">
                    <p><strong>
                    INTRODUCER ADJUSTMENT<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#5" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                    </p></strong>
                    <div id="5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="container-fluid">
                                <div class="table-responsive">     
                                      <table class="table">
                                        <thead id="policyfirstthead">
                                          <tr>
                                            <th>INTRODUCER ID</th>
                                            <th>INTRODUCER NAME</th>
                                            <th>REMARKS</th>
                                            <th>DATE OF ENTRY</th>
                                            <th>TOOLS</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-table">
                    <p><strong>
                    ADMIN PRE-PAYROLL ENTRIES<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#6" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                    </p></strong>
                    <div id="6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="container-fluid">
                                <div class="table-responsive">     
                                      <table class="table">
                                        <thead id="policyfirstthead">
                                          <tr>
                                            <th>INTRODUCER ID</th>
                                            <th>INTRODUCER NAME</th>
                                            <th>REMARKS</th>
                                            <th>DATE OF ENTRY</th>
                                            <th>TOOLS</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-table">
                    <p><strong>
                    PAYROLL PREVIEW<i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#7" aria-expanded="false" aria-controls="collapseTwo"></i></p>
                    </p></strong>
                    <div id="7" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="container-fluid">
                                <form class="form-horizontal" >     
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL INCENTIVES</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Incentives"disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL DEDUCTIONS</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total Deductions"disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                                    <div class="form-group">
                                        <label for="" class="col-lg-6 col-md-6 col-sm-4 col-xs-4 control-label" style="text-align:left"><small>TOTAL</small></label>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                                          <input type="" class="form-control borderzero" id="inputEmail3" placeholder="Total"disabled>
                                        </div>
                                    </div>
                               </div>
                        </form>
                    </div>
                    <div class="rightalign col-lg-12" style="padding: 0px; margin-bottom: 10px;">
                        <hr>
                        <a href="{{ url('policy') }}" type="button" class="btn btn-success borderzero btn-tool pull-right"><i class="fa fa-chevron-left"></i> BACK</a>     
                   <!--      <a type="button" href="policy-management-edit.html" class="btn btn-info borderzero btn-tool pull-right"><i class="fa fa-pencil"></i> Edit</a>
                     --></div>    
              </div>
        </div>                                                
    </div>
</div>
</div>
<!-- /#page-content-wrapper -->

@stop