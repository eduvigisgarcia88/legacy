@extends('layouts.master')

@section('scripts')
<script type="text/javascript">
  $_token = '{{ csrf_token() }}';
  var token = $("input[name='_token']").val();

  $rows_life = '<tr>' +
                '<th class="th-sort" nowrap data-sort="agent_name"><i></i> AGENT NAME</th>' + 
                '<th class="th-sort" nowrap data-sort="agent_code"><i></i> AGENT CODE</th>' +
                '<th class="th-sort" nowrap data-sort="provider_name"><i></i> PROVIDER</th>' +
                '<th class="th-sort" nowrap data-sort="firstname"><i></i> POLICYOWNER NAME</th>' +
                '<th class="th-sort" nowrap data-sort="annual_income_range"><i></i> ANNUAL INCOME</th>' +
                '<th class="th-sort" nowrap data-sort="life_firstname"><i></i> LIFE ASSURED NAME</th>' +
                '<th class="th-sort" nowrap data-sort="policy_term"><i></i> POLICY TERM</th>' +
                '<th class="th-sort" nowrap data-sort="mode_of_payment"><i></i> MODE OF PAYMENT</th>' +
                '<th class="th-sort" nowrap data-sort="designation"><i></i> DESIGNATION</th>' +
                '<th class="th-sort" nowrap data-sort="status"><i></i> SUBMISSION STATUS</th>' +
                @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || 
                  Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || 
                  Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
                '<th class="th-sort" nowrap data-sort="screened_status"><i></i> SCREENED STATUS</th>' +
                @endif
                '<th class="rightalign">TOOLS</th>' +
              '</tr>';

  $rows_gi = '<tr>' +
                '<th class="th-sort" nowrap data-sort="agent_name"><i></i> AGENT NAME</th>' + 
                '<th class="th-sort" nowrap data-sort="agent_code"><i></i> AGENT CODE</th>' +
                '<th class="th-sort" nowrap data-sort="provider_name"><i></i> PROVIDER</th>' +
                '<th class="th-sort" nowrap data-sort="company_name"><i></i> POLICYOWNER NAME</th>' +
                '<th class="th-sort" nowrap data-sort="mode_of_payment"><i></i> MODE OF PAYMENT</th>' +
                '<th class="th-sort" nowrap data-sort="designation"><i></i> DESIGNATION</th>' +
                '<th class="th-sort" nowrap data-sort="status"><i></i> SUBMISSION STATUS</th>' +
                @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || 
                  Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || 
                  Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
                '<th class="th-sort" nowrap data-sort="screened_status"><i></i> SCREENED STATUS</th>' +
                @endif
                '<th class="rightalign">TOOLS</th>' +
              '</tr>';
             
  function changeClass() {

    var loading = $(".loading-pane");
    var table = $("#rows");
    var headers = '';
    $status = $("#row-filter_status").val();

    if ($status == 'life') {
      headers = $rows_life;
    } else if ($status == 'gi') {
      headers = $rows_gi;
    }

    loading.removeClass("hide");
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');
    $(".th-sort").find('i').removeAttr('class');

    $per = $("#row-per").val();
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();

    $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, headers: headers, per: $per, _token: $_token }, function(response) {
    
      table.html(response.paginate);
      $("#rows_header").html(response.paginate_headers);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");

    }, 'json');
  }

  function refresh() {

    var loading = $(".loading-pane");
    var table = $("#rows");
    var headers = $("#rows_header").html();

    loading.removeClass("hide");
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');
    $(".th-sort").find('i').removeAttr('class');

    $per = $("#row-per").val();
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $status = $("#row-filter_status").val();

    $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, headers: headers, per: $per, _token: $_token }, function(response) {
      
      table.html(response.paginate);
      $("#rows_header").html(response.paginate_headers);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");

    }, 'json');
  }

  function search() {

    var loading = $(".loading-pane");
    var table = $("#rows");
    var headers = $("#rows_header").html();

    loading.removeClass("hide");
    $("#row-page").val(1);

    $per = $("#row-per").val();
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $status = $("#row-filter_status").val();

    $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, headers: headers, per: $per, _token: $_token }, function(response) {
      
      table.html(response.paginate);
      $("#rows_header").html(response.paginate_headers);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      
    }, 'json');
  }


    function singaporeDollarCurrencyConverter() {
      $('#row-currency_usd_rate').val($('#row-sd_rate').val() * $('#row-currency_sd_rate').val());
    }

    function usDollarCurrencyConverter() {
      $('#row-currency_sd_rate').val($('#row-usd_rate').val() * $('#row-currency_usd_rate').val());
    }
    function singaporeDollarTotalConverter() {
      $('#row-total_usd_rate').val($('#row-sd_rate').val() * $('#row-total_sd_rate').val());
    }
    function usDollarTotalConverter() {
      $('#row-total_sd_rate').val($('#row-usd_rate').val() * $('#row-total_usd_rate').val());
    }

    $(function(){

      $('.Delete').click(function() {
        $('.Option:last').remove();
      });

      $('.Add').click(function() {
        $('.Option:last').after($('.Option:last').clone());
      });

    });

      // add riders
    $(".btn-add-plan").click(function() {
        var table = $("#plan-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();

        newrow.appendTo(table);
        table.find("select[name='visa_country_id[]']:last").focus();

        var last_tr = table.find("tr:last");
        last_tr.find('select').val("");
        last_tr.find('input').val("");
    });

    // delete riders
    $(document).on('click', '.btn-del-plan', function() {
        var table = $("#plan-table tbody");
        var row = $(this).parent().parent();
        var rowcount = table.find("tr").length;

        if(rowcount > 1) {
            row.remove();
            rowcount--;
        } else {
            row.find(":input").val("");
        }
    });

       // add telephone
    $(".btn-add-telephone").click(function() {
        var table = $("#telephone-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();

        newrow.appendTo(table);

        table.find("select[name='contact[]']:last").focus();
        var last_tr = table.find("tr:last");
        last_tr.find('input').val("");
    });

    // delete telephone
    $(document).on('click', '.btn-del-telephone', function() {
        var table = $("#telephone-table tbody");
        var row = $(this).parent().parent();
        var rowcount = table.find("tr").length;

        if(rowcount > 1) {
            row.remove();
            rowcount--;
        } else {
            row.find(":input").val("");
        }
    });

  $(document).ready(function() {
            $("#row-owner_age").attr("disabled","disabled");
            $("#row-life_age").attr("disabled","disabled"); 
     $('#row-backdate').change(function() {
        if($(this).is(":checked")) {
            $("#row-backdate_value").attr("value",1);  // checked
            $("#row-owner_age").removeAttr("disabled","disabled");
            $("#row-life_age").removeAttr("disabled","disabled");
        }else{
            $("#row-backdate_value").attr("value",0);
            $("#row-owner_age").attr("disabled","disabled");
            $("#row-life_age").attr("disabled","disabled"); 
        }


      });
      $('#row-consent_mail').change(function() {
        if($(this).is(":checked")) {
           $('#row-consent_mail_value').val(1);

        }else{
          $('#row-consent_mail_value').val(0);
        }

      });
      $('#row-consent_sms').change(function() {
        if($(this).is(":checked")) {
           $('#row-consent_sms_value').val(1);

        }else{
          $('#row-consent_sms_value').val(0);
        }

      });
      $('#row-consent_telephone').change(function() {
         if($(this).is(":checked")) {
           $('#row-consent_telephone_value').val(1);

        }else{
          $('#row-consent_telephone_value').val(0);
        }

      });
    $("#row-currency").on("change", function() {
      $_token = "{{ csrf_token() }}";
      var col = $(this).val();

      $.post("{{ url('policy/production/case-submission-advisor/get-currency') }}", { col: col, _token: $_token }, function(response) {
        $("#row-currency_rate").val(response);
        var rate = $("#row-currency_rate").val();
        var value = $("#row-currency_value").val();
        var myResult = rate * value;
        $("#row-conv_total").val(myResult);

      }, 'json');
    });

    $("#row-currency_value").on("input", function() {
      $_token = "{{ csrf_token() }}";
      var col = $(this).val();

      $.post("{{ url('policy/production/case-submission-advisor/get-currency') }}", { col: col, _token: $_token }, function(response) {
        $("#row-currency_rate").val(response);
        var rate = $("#row-currency_rate").val();
        var value = $("#row-currency_value").val();
        var myResult = rate * value;
        $("#row-conv_total").val(myResult);

      }, 'json');
    });

    $( "#row-currency" ).change(function() {
      var currency = document.getElementById('row-currency').value; 
      document.getElementById("total_currency").innerHTML =  currency;
      document.getElementById("total_currency2").innerHTML =  currency;
      document.getElementById("total_currency3").innerHTML =  currency;
      
    });

      $(".btn-refresh").click(function() {
        refresh();
      });

      // delete pending case
      $("#page-content-wrapper").on("click", ".btn-delete", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Deletion', 'Are you sure you want to delete case <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission/delete') }}", id);
      });

      // delete pending case
      $("#page-content-wrapper").on("click", ".btn-delete-gi", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Deletion', 'Are you sure you want to delete case <strong>' + name + '</strong>?', "{{ url('policy/production/gi-case-submission/delete') }}", id);
      });

      // delete pending case
      $("#page-content-wrapper").on("click", ".btn-negative-gi", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        
        dialog('GI Submission Screened Status', 'Set GI Submission Screen Status to <strong>Negative</strong>?', "{{ url('policy/production/negative-gi') }}", id);
      });

      // delete pending case
      $("#page-content-wrapper").on("click", ".btn-undetermined-gi", function() {
        var id = $(this).parent().parent().data('id');

        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        
        dialog('GI Submission Screened Status', 'Set GI Submission Screen Status to <strong>Undetermined</strong>?', "{{ url('policy/production/undetermined-gi') }}", id);
      });

      $(".btn-undetermined-life").click(function(e) {
        var loading = $("#load-form");
        loading.removeClass('hide');

        e.preventDefault();
        var form = $("#modal-production").find('form');

        $.ajax({
          type: "post",
          url: "{{ url('policy/production/undetermined-life') }}",
          data: form.serialize(),
          dataType: "json",
          success: function(response) {

            if (response.unauthorized) {
              window.location.href = response.unauthorized;
            } else if(response.error) {

              status(response.title, response.error, 'alert-danger');
            } else {
              $("#modal-production").modal('hide');
              status(response.title, response.body, 'alert-success');

              // refresh the list
              refresh();
            }

            loading.addClass('hide');
          }
        });
      });

      $(".btn-negative-life").click(function(e) {
        var loading = $("#load-form");
        loading.removeClass('hide');

        e.preventDefault();
        var form = $("#modal-production").find('form');
        
        $.ajax({
          type: "post",
          url: "{{ url('policy/production/negative-life') }}",
          data: form.serialize(),
          dataType: "json",
          success: function(response) {

            if (response.unauthorized) {
              window.location.href = response.unauthorized;
            } else if(response.error) {

              status(response.title, response.error, 'alert-danger');
            } else {
              $("#modal-production").modal('hide');
              status(response.title, response.body, 'alert-success');

              // refresh the list
              refresh();
            }

            loading.addClass('hide');
          }
        });
      });

      $(".btn-undetermined-gi").click(function(e) {
        var loading = $("#load-gi");
        loading.removeClass('hide');

        e.preventDefault();
        var form = $("#modal-gi").find('form');

        $.ajax({
          type: "post",
          url: "{{ url('policy/production/undetermined-gi') }}",
          data: form.serialize(),
          dataType: "json",
          success: function(response) {

            if (response.unauthorized) {
              window.location.href = response.unauthorized;
            } else if(response.error) {

              status(response.title, response.error, 'alert-danger');
            } else {
              $("#modal-gi").modal('hide');
              status(response.title, response.body, 'alert-success');

              // refresh the list
              refresh();
            }

            loading.addClass('hide');
          }
        });
      });

      $(".btn-negative-gi").click(function(e) {
        var loading = $("#load-gi");
        loading.removeClass('hide');

        e.preventDefault();
        var form = $("#modal-gi").find('form');
        
        $.ajax({
          type: "post",
          url: "{{ url('policy/production/negative-gi') }}",
          data: form.serialize(),
          dataType: "json",
          success: function(response) {

            if (response.unauthorized) {
              window.location.href = response.unauthorized;
            } else if(response.error) {

              status(response.title, response.error, 'alert-danger');
            } else {
              $("#modal-gi").modal('hide');
              status(response.title, response.body, 'alert-success');

              // refresh the list
              refresh();
            }

            loading.addClass('hide');
          }
        });
      });

      //add user
      $(".btn-add").click(function() {

        var table_telephone = $("#telephone-table tbody");
        var row_telephone = table_telephone.find("tr:first");

        table_telephone.html("");
        var newrow_telephone = row_telephone.clone();
        newrow_telephone.appendTo(table_telephone);

        var table_plan = $("#plan-table tbody");
        var row_plan = table_plan.find("tr:first");

        table_plan.html("");
        var newrow_plan = row_plan.clone();
        newrow_plan.appendTo(table_plan);

        // reset all form fields
        $("#row-reject_reason").addClass("hide");
        $("#modal-production").find('input').val("");
        $("#modal-production").find('form').trigger("reset");
        $("input[name='_token']").val(token);
        $(".modal-body button").show();
        $("#row-currency_rate").hide();
        $("#row-id").val("");

        $("#row-consent_mail").removeAttr("checked","checked");
        $("#row-consent_sms").removeAttr("checked","checked");
        $("#row-consent_telephone").removeAttr("checked","checked");
        $("#modal-production").find('.hide-view').removeClass('hide');
        $("#modal-production").find('.show-view').addClass('hide');
        $("#modal-production").find('.preview').addClass('hide');
        $("#modal-production").find('input').removeAttr('readonly', 'readonly');
        $("#modal-production").find('select').removeAttr('disabled');

        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
        $("#row-conv_total").attr('readOnly', 'readonly');
        $("#modal-production").modal('show');
      });


      $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
        $("#row-reject_reason").removeClass("hide");
        $("#label-reason").removeClass("hide");
        var table_telephone = $("#telephone-table tbody");
        var row_telephone = table_telephone.find("tr:first");

        table_telephone.html("");
        var newrow_telephone = row_telephone.clone();
        newrow_telephone.appendTo(table_telephone);

        var table_plan = $("#plan-table tbody");
        var row_plan = table_plan.find("tr:first");

        table_plan.html("");
        var newrow_plan = row_plan.clone();
        newrow_plan.appendTo(table_plan);
        $("#submission-form").addClass("hide");
        $("#screened-form").addClass("hide");
        // reset all form fields
        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        $("#modal-production").find('form').trigger("reset");
        $("#modal-production").find('input').val("");
        $("input[name='_token']").val(token);
        $("#row-user_sales_id").prop('disabled', true);
        $("#row-annual_income_range").prop('disabled', true);
        $("#row-owner_gender").prop('disabled', true);
        $("#row-provider_list").prop('disabled', true);
        $("#row-provider_list_rider").prop('disabled', true);
        $("#row-rider_product_id").prop('disabled', true);
        $("#row-life_gender").prop('disabled', true);
        $("#row-residency_others").prop('disabled', true);
        $("#row-residency_status").prop('disabled', true);
        $("#row-nationality_id").prop('disabled', true);
        $(".form-mode_others").addClass("hide");
        $(".btn-add-plan").prop('disabled', true);
        $(".btn-del-plan").prop('disabled', true);
        $(".btn-add-telephone").prop('disabled', true);
        $(".btn-del-telephone").prop('disabled', true);
        $("#row-consent_marketing_val_two").prop('disabled', true);
        $("#row-consent_marketing_val_one").prop('disabled', true);
        $("#row-consent_servicing_val_two").prop('disabled', true);
        $("#row-consent_servicing_val_one").prop('disabled', true);
        $("#row-currency_id").prop('disabled', true);
        $("#row-currency2_id").prop('disabled', true);
        $("servicing").prop('disabled', true);
        $("#row-prem_currency").prop('disabled', true);
        $("#row-policy_term").prop('disabled', true);
        $("#row-payment_frequency").prop('disabled', true);
        $("#row-mode_of_payment").prop('disabled', true);
        $("#row-product_id").prop('disabled', true);
        $("#row-source").prop('disabled', true);
        $("#row-main_product_id").prop('disabled', true);
        $("#row-currency_rate").hide();
        $("#row-currency_rate2").hide();
        $(".modal-body button").show();
        $("#row-id").val("");
        $("#row-main_product_id").html("");
        $("#row-ape").attr('readOnly', 'readonly');

            $.post("{{ url('policy/production/view-all') }}", { id: id, _token: $_token }, function(response) {
              if(!response.error) {

                // set form title
                $(".modal-title").html('<i class="fa fa-flag"></i> <strong>Submission Status</strong>');

                // output form data
                $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                    if (index == "case") {
                      $(".modal-title").html('<i class="fa fa-flag"></i> <strong>Submission Status (' + value + ')</strong>');
                    }

                    if (index == 'user_id') {
                      field = $("#row-user_sales_id");
                    }

                    if (index == 'get_provider') {

                      if (index.length > 0) {

                      $.each(value, function(gpindex, gpvalue) {
                        
                        if(gpindex == 'get_products' && gpvalue.length > 0) {
                          var rows = '';
                          $.each(gpvalue, function(gpaindex, gpavalue) {
                            if (gpavalue.get_case_riders.length > 0) {
                              rows += '<option value=' + gpavalue.id + '>' + gpavalue.name + '</option>';
                            }
                          });
                          $("#row-main_product_id").html(rows);
                        }

                      });
                    
                      $("#row-main_product_id").val(response.row.main_product_id);
                      }

                    }

                    if (index == "get_contact") {
                      
                      if (index.length > 0) {
                        var table = $("#telephone-table tbody");
                        var row = table.find("tr:first");

                        table.html("");

                        $.each(value, function(cindex, cvalue) {
                          var newrow = row.clone();

                          $.each(cvalue, function(ptindex, ptvalue) {

                            var rows = "input[name='" + ptindex + "[]'], input[name='contact_" + ptindex + "[]']";

                            var field = newrow.find(rows);

                            if(field.length > 0) {
                              field.val(ptvalue);
                            }
                          });

                          newrow.appendTo(table);
                        });

                      // delete empty first row
                      row.remove();
                      }
                    }
                   
                    if (index == "get_sum") {
                      if (index.length > 0) {
                        var table = $("#plan-table tbody");
                        var row = table.find("tr:first");
                        // var ctr = 1;
                        table.html("");

                        $.each(value, function(pindex, pvalue) {
                          var newrow = row.clone();

                          var rider = '<option class="hide">Select Plan</option>';
                          // var vlength = 0;
                          $.each(pvalue, function(ptindex, ptvalue) {
                            var rows = "select[name='" + ptindex + "_rider[]'], input[name='riders_" + ptindex + "[]'], input[name='rider_" + ptindex + "[]'], select[name='rider_" + ptindex + "[]']";
                            
                            var field = newrow.find(rows);

                            if(field.length > 0) {

                              if (ptindex == "sub_id") {
                                field.parent().parent().find("td:nth-child(1)").removeAttr("class").attr("class", "col-xs-4 error-provider_list_rider_" + ptvalue);
                                field.parent().parent().find("td:nth-child(2)").removeAttr("class").attr("class", "col-xs-4 error-rider_product_id_" +  ptvalue);
                                field.parent().parent().find("td:nth-child(3)").removeAttr("class").attr("class", "col-xs-4 error-rider_sum_assured_" +  ptvalue);
                              }

                              if (ptindex == "product_id") {
                                if (response.row.get_main_riders.length > 0) {
                                  $.each(response.row.get_main_riders, function(pttindex, pttvalue) {
                                    rider += '<option value="' + pttvalue.id + '">' + pttvalue.name + '</option>';
                                  });
                                }
                                field.html(rider);
                              }
                              field.val(ptvalue);
                            }
                          });

                          newrow.appendTo(table);
                        });
                      // delete empty first row
                      row.remove();
                      }
                    }

                   if(index == "consent_marketing") {
                    if(value == "yes") {
                      $('#row-consent_marketing_val_one').attr('checked','checked');
                      $("#row-consent_marketing").val("yes");
                    } else {
                      $('#row-consent_marketing_val_two').attr('checked','checked');
                      $("#row-consent_marketing").val("no");
                    }
                  }

                  if(index == "consent_servicing") {
                    if(value == "yes") {
                      $('#row-consent_servicing_val_one').attr('checked','checked');
                      $("#row-consent_servicing").val("yes");
                    } else {
                      $('#row-consent_servicing_val_two').attr('checked','checked');
                      $("#row-consent_servicing").val("no");
                    }
                  }

                  if (index == "mode_others") {
                    if (value) {
                      $(".form-" + index).removeClass("hide");
                      $("#row-mod_others").val(value);
                    }
                  }

                  if (index == "residency_others") {
                    if (value) {
                      $(".form-" + index).removeClass("hide");
                      $("#row-residency_others").val(value);
                    }
                  }

                  // field exists, therefore populate
                  if(field.length > 0) {
                    if(field.parent().hasClass("date")) {
                      if (value) {
                        field.val(moment(value).format('MM/DD/YYYY'));
                      }
                    } else {
                      if (index == "mode_others") {
                      } else {
                        field.val(value);
                      }
                      if (index == "residency_others") {
                      } else {
                        field.val(value);
                      }
                    }
                  }

                  @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
                        Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
                        Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
                  if (index == "case") {
                    if (value != "pending") {
                      $("#screened-form").removeClass("hide");
                    } else {
                      $("#submission-form").removeClass("hide");
                    }
                  }
                  @else
                  $("#submission-form").removeClass("hide");
                  @endif
                });

            $("#modal-production").find('.hide-view').addClass('hide');
            $("#modal-production").find('.preview').removeClass('hide');
            $("#modal-production").find('.change').addClass('hide');
            $("#modal-production").find('.show-view').removeClass('hide');
            $("#modal-production").find('input').attr('readonly', 'readonly');

            // show form
            $("#modal-production").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-flag");
        }, 'json');

      });

        $(function () {
        $('.owner_dob').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

      $(function () {
        $('.life_dob').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

      $(function () {
        $('.policy_exp_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

      $(function () {
        $('.com_run_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

      $(function () {
        $('.date_issue').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected provider/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });
      
      $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
        console.log('multiple');

        
        if (ids.length == 0) {
          status("Error", "Select providers first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('provider/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('policy/production/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });

      // approved case
      $("#modal-production").find("form").on("click", ".btn-approved", function() {
        var id = $("#row-id").val();

        $("#load-production").removeClass("hide");
        $.post("{{ url('policy/production/case-submission-advisor/verified') }}", { id: id, _token: $_token }, function(response) {
             
              $("#modal-production").modal('hide');
              refresh();
              $("#load-production").addClass("hide");
              status(response.title, response.body, 'alert-success');
        }, 'json');

      });

      //verified case
       $("#modal-production").find("form").on("click", ".btn-verified", function() {
        var id = $("#row-id").val();
        $("#verified_reason").val("");

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-verified").modal("show");

      });
      // reject case
      $("#modal-production").find("form").on("click", ".btn-reject", function() {

        var id = $("#row-id").val();

        $("#load-production").removeClass("hide");
        $.post("{{ url('policy/production/case-submission-advisor/reject') }}", { id: id, _token: $_token }, function(response) {
             
              $("#modal-production").modal('hide');
              refresh();
              $("#load-production").addClass("hide");
              status(response.title, response.body, 'alert-success');
        }, 'json');

      });

      $("#page-content-wrapper").on("click", ".btn-view-gi", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
        $("#gi-reject_reason").removeClass("hide");
        $("#label-reason").removeClass("hide");
        var table_telephone = $("#telephone-table tbody");
        var gi_telephone = table_telephone.find("tr:first");

        table_telephone.html();
        // var newgi_telephone = gi_telephone.clone();
        // newgi_telephone.appendTo(table_telephone);

        var table_plan = $("#plan-table tbody");
        var gi_plan = table_plan.find("tr:first");

        table_plan.html("");
        var newgi_plan = gi_plan.clone();
        newgi_plan.appendTo(table_plan);

        // reset all form fields
        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        $("#modal-gi").find('form').trigger("reset");
        $("#modal-gi").find('input').val("");
        $("input[name='_token']").val(token);
        $("#gi-user_sales_id").prop('disabled', true);
        $("#gi-annual_income_range").prop('disabled', true);
        $("#gi-owner_gender").prop('disabled', true);
        $("#gi-provider_list").prop('disabled', true);
        $("#gi-provider_list_rider").prop('disabled', true);
        $("#gi-rider_product_id").prop('disabled', true);
        $("#gi-life_gender").prop('disabled', true);
        $("#gi-residency_others").prop('disabled', true);
        $("#gi-residency_status").prop('disabled', true);
        $("#gi-type_of_insurance_others").prop('disabled', true);
        $("#gi-type_of_insurance").prop('disabled', true);
        $("#gi-nationality_id").prop('disabled', true);
        $(".gi-mode_others").addClass("hide");
        $(".btn-add-plan").prop('disabled', true);
        $(".btn-del-plan").prop('disabled', true);
        $(".btn-add-telephone").prop('disabled', true);
        $(".btn-del-telephone").prop('disabled', true);
        $("#gi-consent_marketing_val_two").prop('disabled', true);
        $("#gi-consent_marketing_val_one").prop('disabled', true);
        $("#gi-consent_servicing_val_two").prop('disabled', true);
        $("#gi-consent_servicing_val_one").prop('disabled', true);
        $("#gi-currency").prop('disabled', true);
        $("#gi-currency2").prop('disabled', true);
        $("#gi-type_of_insurance").prop('disabled', true);
        $("servicing").prop('disabled', true);
        $("#gi-prem_currency").prop('disabled', true);
        $("#gi-policy_term").prop('disabled', true);
        $("#gi-payment_frequency").prop('disabled', true);
        $("#gi-mode_of_payment").prop('disabled', true);
        $("#gi-product_id").prop('disabled', true);
        $("#gi-source").prop('disabled', true);
        $("#gi-main_product_id").prop('disabled', true);
        $("#gi-currency_rate").hide();
        $("#gi-currency_rate2").hide();
        $(".modal-body button").show();
        $("#gi-id").val("");
        $("#gi-main_product_id").html("");
        $("#gi-ape").attr('readOnly', 'readonly');
        $("#gi-nationality_id").select2('val', 124);
        $("#gi-user_sales_id").select2('val', '');

        $(".upload_file").addClass("hide");
        $(".view_file").addClass("hide");
        $(".btn-cancel-upload").addClass('hide');

        $(".upload_file_2").addClass("hide");
        $(".view_file_2").addClass("hide");
        $(".btn-cancel-upload_2").addClass('hide');

        $(".upload_file_3").addClass("hide");
        $(".view_file_3").addClass("hide");
        $(".btn-cancel-upload_3").addClass('hide');

        $(".upload_file_4").addClass("hide");
        $(".view_file_4").addClass("hide");
        $(".btn-cancel-upload_4").addClass('hide');

        $(".upload_file_5").addClass("hide");
        $(".view_file_5").addClass("hide");
        $(".btn-cancel-upload_5").addClass('hide');
        $("#gi_form-mode_others").addClass("hide");
        $("#gi_form-type_of_insurance_others").addClass("hide");
        $("#submission-form-gi").addClass("hide");
        $("#submission-form-gi").addClass("hide");

        $.post("{{ url('policy/production/gi-case-submission/view-all') }}", { id: id, _token: $_token }, function(response) {
            if(!response.error) {

              // set form title
              $(".modal-title").html('<i class="fa fa-flag"></i> <strong>Submission Status</strong>');

              // output form data
              $.each(response.row, function(index, value) {
                var field = $("#gi-" + index);

                if (index == "case") {
                  $(".modal-title").html('<i class="fa fa-flag"></i> <strong>Submission Status (' + value + ')</strong>');
                }

                  if (index == 'user_id') {
                    field = $("#gi-user_sales_id");
                    $("#gi-user_sales_id").select2('val', value);
                  }

                  if (index == "mode_others") {
                    if (value) {
                      $("#gi_form-" + index).removeClass("hide");
                      $("#gi-mod_others").val(value);
                    }
                  }

                  if (index == "type_of_insurance_others") {
                    if (value) {
                      $("#gi_form-" + index).removeClass("hide");
                      $("#gi-type_of_insurance_others").val(value);
                    }
                  }

                  // field exists, therefore populate
                  if(field.length > 0) {
                    if(field.parent().hasClass("date")) {
                      if (value) {
                        field.val(moment(value).format('DD/MM/YYYY'));
                      }
                    } else {
                      if (index == "mode_others") {
                      } else {
                        field.val(value);
                      }
                      if (index == "type_of_insurance_others") {
                      } else {
                        field.val(value);
                      }
                    }
                  }

                @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
                      Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
                      Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
                if (index == "case") {
                  if (value != "pending") {
                    $("#screened-form-gi").removeClass("hide");
                  } else {
                    $("#submission-form-gi").removeClass("hide");
                  }
                }
                @else
                $("#submission-form-gi").removeClass("hide");
                @endif
              });

            $("#modal-gi").find('.hide-view').addClass('hide');
            $("#modal-gi").find('.preview').removeClass('hide');
            $("#modal-gi").find('.change').addClass('hide');
            $("#modal-gi").find('.show-view').removeClass('hide');
            $("#modal-gi").find('input').attr('readonly', 'readonly');

            // show form
            $("#modal-gi").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-flag");
        }, 'json');

      });

      // approved case
      $("#modal-gi").find("form").on("click", ".btn-approved-gi", function() {
        var id = $("#gi-id").val();

        $("#load-gi").removeClass("hide");
        $.post("{{ url('policy/production/gi-case-submission/verified') }}", { id: id, _token: $_token }, function(response) {
             
              $("#modal-gi").modal('hide');
              refresh();
              $("#load-gi").addClass("hide");
              status(response.title, response.body, 'alert-success');
        }, 'json');

      });

      // reject case
      $("#modal-gi").find("form").on("click", ".btn-reject-gi", function() {

        var id = $("#gi-id").val();

        $("#load-gi").removeClass("hide");
        $.post("{{ url('policy/production/gi-case-submission/reject') }}", { id: id, _token: $_token }, function(response) {
             
              $("#modal-gi").modal('hide');
              refresh();
              $("#load-gi").addClass("hide");
              status(response.title, response.body, 'alert-success');
        }, 'json');

      });

      // disable case
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/disable') }}", id);
      });

      // enable case
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/enable') }}", id);
      });

      // show modal for multiple inception
      $("#page-content-wrapper").on("click", ".btn-incept-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Inception");
        $(".modal-multiple-body").html("Are you sure you want to incept the selected case/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });
        $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
        console.log('multiple');

        
        if (ids.length == 0) {
          status("Error", "Select providers first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == 0) {
            $.post("{{ url('policy/production/case-submission-advisor/incept-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });

    });



    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {
      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

      $("#row-page").val($(this).html());
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      var headers = $("#rows_header").html();

      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, headers: headers, per: $per, _token: $_token }, function(response) {
        
        table.html(response.paginate);
        $("#rows_header").html(response.paginate_headers);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");
        
      }, 'json');
    });


    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");
        var headers = $("#rows_header").html();

        loading.removeClass("hide");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, headers: headers, per: $per, _token: $_token }, function(response) {
          
          table.html(response.paginate);
          $("#rows_header").html(response.paginate_headers);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
          
        }, 'json');
      }
   });
 
    $('#row-search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          search();
        }
    });

  </script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
    <h3 class="main-title">PENDING CASES</h3>
    <h5 class="bread-crumb">PRODUCTION & POLICY MANAGEMENT</h5>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
    <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-refresh" onclick="refresh();"><i class="fa fa-refresh"></i> Refresh</button> 
  </div>
  </div>
  <div class="container-fluid default-container container-space">
  <div class="col-lg-12">
        <p><strong>FILTER OPTIONS</strong></p>
  </div>
                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group hide-view">
                        <label for="" class="col-sm-2 col-xs-5 control-label-left font-color">CASES</label>
                      <div class="col-sm-10 col-xs-7">
                        <select class="form-control borderzero" onchange="changeClass()"  id="row-filter_status">
                          <option value="life" selected>Life Submissions</option>
                          <option value="gi">GI Submissions</option>
                        </select>
                      </div>
                    </div>
                  </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                        <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                        </span>
                      </div>
                      </div>
                    </div>
                  </div>
              </div>
      <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
          <div class="table-responsive block-content tbblock col-xs-12">
            <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
          <div class="table-responsive">
            <table class="table table-striped table-pagination" id="tblData">
              <thead id="rows_header" class="tbheader">
                  <tr>
                    <th class="th-sort" nowrap data-sort="agent_name"><i></i> AGENT NAME</th>
                    <th class="th-sort" nowrap data-sort="agent_code"><i></i> AGENT CODE</th>
                    <th class="th-sort" nowrap data-sort="provider_name"><i></i> PROVIDER</th>
                    <th class="th-sort" nowrap data-sort="firstname"><i></i> POLICYOWNER NAME</th>
                    <th class="th-sort" nowrap data-sort="annual_income_range"><i></i> ANNUAL INCOME</th>
                    <th class="th-sort" nowrap data-sort="life_firstname"><i></i> LIFE ASSURED NAME</th>
                    <th class="th-sort" nowrap data-sort="policy_term"><i></i> POLICY TERM</th>
                    <th class="th-sort" nowrap data-sort="mode_of_payment"><i></i> MODE OF PAYMENT</th>
                    <th class="th-sort" nowrap data-sort="designation"><i></i> DESIGNATION</th>
                    <th class="th-sort" nowrap data-sort="status"><i></i> SUBMISSION STATUS</th>
                    @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || 
                      Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || 
                      Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
                    <th class="th-sort" nowrap data-sort="screened_status"><i></i> SCREENED STATUS</th>
                    @endif
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody id="rows">
                  <?php echo($paginate); ?>
                </tbody>
              </table>
            </div>
          </div>           
      </div>
      <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
        <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
        <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
      </div>
    <div class="text-center">
      <input type="hidden" id="row-page" value="1">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-order" value="">
    </div>
    <div class="col-xs-12" style="color: red; font-weight: bold;">
      <small>*for <u>REJECTED</u> Cases, please see Admin</small>
    </div>
    </div>
    <!-- /#page-content-wrapper -->

@include('policy-management.production.case-pending.form')

@stop