<!-- modal add product -->
<div class="modal fade" id="modal-production" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog modal-lg">
<div class="modal-content borderzero">
      <div id="load-form" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
    {!! Form::open(array('url' => 'policy/production/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_production')) !!}
    <div class="modal-header modal-warning">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-body modal-viewing">
      @if(Auth::user()->usertype_id == 8)
          <input type="hidden" name="user_sales_id" value="{{ Auth::user()->id }}"> 
      @else
      <div class="form-group">
          <label for="row-user_sales_id" class="col-sm-2">AGENT</label>
        <div class="col-sm-10 error-user_sales_id">
          <select name="user_sales_id" id="row-user_sales_id" class="form-control input-md borderzero multi_select" style="width: 100%;" placeholder="Agent">
            <option value="" class="hide"></option>
            @foreach($sales as $row)
              <option value="{{ $row->id }}">[{{ $row->code }}] {{ $row->name }}</option>
            @endforeach
          </select> 
          <sup class=" sup-errors"></sup>
        </div>
      </div>
      @endif
    <div class="row">
    <hr class="hr">
    </div>
    <div class="hidden-md hidden-sm hidden-xs ">
        <div class="error-owner_id">
          <h4>POLICY OWNER</h4>
        </div>
    </div>
    <div class="form-group">
        <label for="row-policy_owner" class="col-sm-2">POLICY OWNER NAME</label>
    <div class="col-sm-10">
      <div class="row">
        <div class="col-lg-6 error-lastname ">
            <input type="text" name="lastname" class="form-control borderzero" id="row-lastname" maxlength="255" placeholder="Family Name">
            <sup class="sup-errors"></sup>
        </div>
        <div class="col-lg-6 error-firstname">
            <input type="text" name="firstname" class="form-control borderzero" id="row-firstname" maxlength="255" placeholder="Given Name">
            <sup class=" sup-errors"></sup>
        </div>
      </div>
    </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
    <div class="form-group">
      <label for="row-code" class="col-sm-4"><small>IDENTITY CARD/PASSPORT NO.</small></label>
      <div class="col-sm-8 error-owner_id ">
          <div class="">
              <input type="text" name="owner_id" class="form-control borderzero" id="row-owner_id" maxlength="255" placeholder="">
              <sup class="sup-errors"></sup>
          </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
    <label for="row-code" class="col-sm-4">OCCUPATION</label>
      <div class="col-sm-8">
          <div>
              <input type="text" name="owner_occupation" class="form-control borderzero" id="row-owner_occupation" maxlength="255" placeholder="">
          </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <label for="row-code" class="col-sm-4">GENDER</label>
    <div class="col-sm-8">
        <div>
            <select name="owner_gender" id="row-owner_gender" class="form-control borderzero">
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="row-code" class="col-sm-4">DATE OF BIRTH</label>
      <div class="col-sm-8">
          <div>
              <div class="input-group date owner_dob">
                <input class="form-control" name="owner_dob" id="row-owner_dob" name="owner_dob" type="text" >
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
<div class="row form-backdate">
    <!-- <label class="col-sm-12"><input type="checkbox" class="hide-view" value="yes" name="backdate" id="row-backdate"> BACKDATE -->
        <!-- <input type="hidden" name="backdate_value" id="row-backdate_value"></label> -->

  <div class="col-sm-6">
    <div class="form-group">
    <label class="col-sm-4 hide-view"><input type="checkbox" value="yes" name="backdate" id="row-backdate"> BACKDATE
        <input type="hidden" name="backdate_value" id="row-backdate_value"></label>
    <label class="col-sm-4 show-view"> BACKDATE</label>
      <div class="col-sm-8">
          <div>
              <div class="input-group date owner_age">
                <input class="form-control" name="owner_age" id="row-owner_age" name="owner_age" type="text" >
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
          </div>
      </div>
    </div>
  </div>
        <div class="col-sm-6">
      <div class="form-group">
        <label for="row-code" class="col-sm-4">AGE NEXT BIRTHDAY</label>
         <div class="col-sm-8 error-owner_bday">
            <div>
                <input type="text" name="owner_bday" class="form-control borderzero" id="row-owner_bday" maxlength="255" placeholder="">
            </div>        
        <sup class="sup-errors"></sup>  
        </div>
      </div>
    </div>
    <div class="col-sm-6">
    <div class="form-group">
     <label for="row-nationality_id" class="col-sm-4">NATIONALITY</label>
      <div class="col-sm-8 error-nationality_id ">
        <select name="nationality_id" id="row-nationality_id" class="form-control borderzero multi_select">
          <option class="hide" value="">Select Nationality</option>
          @foreach($nations as $row)
          <option value="{{ $row->id }}" {{ ($row->id == 124 ? 'selected' : '') }}>{{ $row->nationality }}</option>
          @endforeach
        </select>
        <sup class="sup-errors"></sup>
      </div>
    </div>
  </div>

    <div class="col-sm-6">
    <div class="form-group">
        <label for="row-annual_income_range" class="col-sm-4">ANNUAL INCOME RANGE</label>
    <div class="col-sm-8 error-annual_income_range">
        <select name="annual_income_range" id="row-annual_income_range" class="form-control borderzero">
          <option value="">Select Income Range</option>
          <option value="$0 - $29,999">$0 - $29,999</option>
          <option value="$30,000 - $49,999">$30,000 - $49,999</option>
          <option value="$50,000 - $99,000">$50,000 - $99,000</option>
          <option value="$100,000 - $149,999">$100,000 - $149,999</option>
          <option value="$150,000 - $299,999">$150,000 - $299,999</option>
          <option value="$300,000 & above">$300,000 & above</option>
        </select>
        <sup class=" sup-errors"></sup>
    </div>
    </div>
    </div>
</div>
  <div class="row">
    <div class="col-sm-6">
    <div class="form-group">
     <label for="row-residency_status" class="col-sm-4">RESIDENCY STATUS</label>
      <div class="col-sm-8 error-residency_status ">
          <select name="residency_status" id="row-residency_status" class="form-control borderzero">
            <option value="">Select Status</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Singaporean / PR">Singapore PR</option>
            <option value="Employment Pass">Employment Pass</option>
            <option value="S-Pass">S-Pass</option>
            <option value="Work Permit">Work Permit</option>
            <option value="Dependent’s Pass">Dependent’s Pass</option>
            <option value="Student Pass">Student Pass</option>
            <option value="Others">Others</option>
          </select>
          <sup class="sup-errors"></sup>
      </div>
    </div>
    </div>




    <div class="col-sm-6">
      <div class="form-residency_others hide">
      <div class="form-group">
       <!-- <label for="row-residency_others" class="col-sm-4"></label> -->
        <div class="col-sm-12 error-residency_others ">
            <input type="text" name="residency_others" class="form-control borderzero" id="row-residency_others" placeholder="Others" maxlength="30">
            <sup class="sup-errors"></sup>
        </div>
      </div>
    </div>
    </div>

    </div>

    <div class="row">
    <hr class="hr">
    </div>
    <div class="hidden-md hidden-sm hidden-xs ">
        <div class="error-owner_id">
          <h4>LIFE ASSURED</h4>
        </div>
    </div>
    <div class="form-same hide-view">
        <label for="row-same" class="hidden-xs"></label>
        <label>
          <input type="checkbox" class="hide-view " name="same" id="row-same" value="yes"> POLICY OWNER IS SAME AS LIFE ASSURED
        </label>
    </div>
    <div class="form-life_assured">
    <div class="form-group">
        <label for="row-life_assured" class="col-sm-2">LIFE ASSURED NAME</label>
    <div class="col-sm-10">
      <div class="row">
        <div class="col-lg-6">
            <input type="text" name="life_lastname" class="form-control borderzero" id="row-life_lastname" maxlength="255" placeholder="Family Name">
        </div>
        <div class="col-lg-6">
            <input type="text" name="life_firstname" class="form-control borderzero" id="row-life_firstname" maxlength="255" placeholder="Given Name">
        </div>
      </div>
    </div>
    </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
    <div class="form-group">
      <label for="row-code" class="col-sm-4"><small>IDENTITY CARD/PASSPORT NO.</small></label>
      <div class="col-sm-8 error-owner_id ">
          <div class="">
              <input type="text" name="life_id" class="form-control borderzero" id="row-life_id" maxlength="255" placeholder="">
              <sup class="sup-errors"></sup>
          </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
    <label for="row-code" class="col-sm-4">OCCUPATION</label>
      <div class="col-sm-8">
          <div>
              <input type="text" name="life_occupation" class="form-control borderzero" id="row-life_occupation" maxlength="255" placeholder="">
          </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <label for="row-code" class="col-sm-4">GENDER</label>
    <div class="col-sm-8">
        <div>
            <select name="life_gender" id="row-life_gender" class="form-control borderzero">
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label for="row-code" class="col-sm-4">DATE OF BIRTH</label>
      <div class="col-sm-8">
          <div>
              <div class="input-group date life_dob">
                <input class="form-control" name="life_dob" id="row-life_dob" name="life_dob" type="text" >
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
<div class="row form-life_backdate">
    <!-- <label class="col-sm-12"><input type="checkbox" class="hide-view" value="yes" name="life_backdate" id="row-life_backdate"> BACKDATE -->
        <!-- <input type="hidden" name="backdate_value" id="row-backdate_value"></label> -->

  <div class="col-sm-6">
    <div class="form-group">
    <label class="col-sm-4 hide-view"><input type="checkbox" value="yes" name="life_backdate" id="row-life_backdate"> BACKDATE
        <input type="hidden" name="backdate_value" id="row-backdate_value"></label>
    <label class="col-sm-4 show-view"> BACKDATE</label>
      <div class="col-sm-8">
          <div>
              <div class="input-group date life_age">
                <input class="form-control" name="life_age" id="row-life_age" name="life_age" type="text" >
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
          </div>
      </div>
    </div>
  </div>

    <div class="col-sm-6">
      <div class="form-group">
        <label for="row-code" class="col-sm-4">AGE NEXT BIRTHDAY</label>
        <div class="col-sm-8">
          <div>
            <input type="text" name="life_bday" class="form-control borderzero" id="row-life_bday" maxlength="255" placeholder="">
          </div>          
        </div>
      </div>
    </div>

  </div>

      <div class="row">
      <hr class="hr">
      </div>
      <div class="row">
      <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-12">
        <div class="form-group">
            <label for="row-address" class="col-sm-4">ADDRESS</label>
        <div class="col-sm-8 error-address ">
            <input type="text" name="address" class="form-control borderzero" id="row-address" maxlength="255">
            <sup class="sup-errors"></sup>
        </div>
      </div>
      </div>
      <div class="col-lg-12">
        <div class="form-group">
         <label for="row-email" class="col-sm-4">EMAIL</label>
          <div class="col-sm-8 error-production_email ">
              <input type="email" name="production_email" class="form-control borderzero" id="row-email" maxlength="255">
              <sup class="sup-errors"></sup>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="col-lg-6">
      <div class="row">

        <div class="col-lg-12">
          <div class="form-group">
            <label for="row-postal_code" class="col-sm-4">POSTAL CODE</label>
            <div class="col-sm-8 error-postal_code ">
                <input type="text" name="postal_code" class="form-control borderzero" id="row-postal_code" maxlength="255">
                <sup class="sup-errors"></sup>
            </div>
        </div>
        </div>
        <div class="col-lg-12">
          <div class="row">
          <label for="row-code" class="col-sm-4"><button type="button" value="" class="hide-view btn-add-telephone"> <i class="fa fa-plus Add"> </i></button> TELEPHONE</label>
            <div class="table-responsive">
            <table id="telephone-table" class="table tablesorter">
                <tbody id="telephone-table-body"><tr>
                        <td class="col-xs-12">
                            <div class="col-xs-12 input-group">
                                <input type="text" style="border-right: 1px solid #cccccc !important;" name="contact[]" class="form-control borderzero  requiredcol-lg-12" id="row-contact" maxlength="255">
                            </div>
                        </td>
                        <td class="hide-sta text-right">
                            <input type="hidden" name="contact_id[]" value="">
                            <button type="button" value="" class="hide-view btn-del-telephone" title="Remove"> <i class="fa fa-close"> </i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
      </div>
      </div>
    </div>
  </div>
<div id="clone" class="">
    <div class="row">
      <div class="col-lg-4">
        <label for="row-code">PRODUCT LIST</label>
        <div class="form-group">
        <div class="col-sm-12">
          <div class="input-group col-xs-12 error-provider_list">
          <select name="provider_list" id="row-provider_list" class="form-control borderzero">
            <option value="">Select Provider</option>
            @foreach ($providers as $row)
            <option value="{{ $row->id }}">{{ $row->name }}</option>
            @endforeach
          </select>
          <sup class=" sup-errors"></sup>

          </div>
      </div>
      </div>
      </div>
      <div class="col-lg-4">
        <label for="row-code">TYPE OF PLAN</label>
        <div class="form-group">
        <div class="col-xs-12 ">
          <div class="input-group col-sm-12 error-main_product_id">
          <select name="main_product_id" id="row-main_product_id" class="form-control borderzero"><option value="" class="hide">Select Product List</option></select>
          <sup class="sup-errors"></sup>
          <input type="hidden" name="conversion_rate" id="row-conversion_rate" value="">
          <input type="hidden" name="conversion_year" id="row-conversion_year" value="">
          <input type="hidden" name="conversion_gst" id="row-conversion_gst" value="">
          </div>
      </div>
      </div>
      </div>
      <div class="col-lg-4">
        <label for="row-code">SUM ASSURED</label>
        <div class="form-group">
      <div class="col-sm-12 error-main_sum_assured">
          <input type="text" name="main_sum_assured" class="form-control borderzero" id="row-main_sum_assured" maxlength="255">
          <sup class=" sup-errors"></sup>
      </div>
      </div>
      </div>
    </div>
  </div>
  <div class="form-group">
          <div class="row col-lg-12">
            <label for="row-code" class="col-sm-3 col-lg-2"><button type="button" value="" class="clone hide-view btn-add-plan"> <i class="fa fa-plus Add"> </i></button> RIDERS</label>
            <label for="row-code" class="col-sm-9 col-lg-10 hidden-xs"></label>
          </div>
            <div class="col-lg-12">
            <div class="table-responsive">
            <table id="plan-table" class="table tablesorter">
                <tbody id="plan-table-body">
                    <tr>
                      <td id="rider_product_id_error" class="col-xs-6 error-rider_product_id_1">
                        <div class="input-group col-xs-12">
                          <select name="rider_product_id[]" id="row-rider_product_id" class="form-control borderzero rider_product_id">
                            <option value="" class="hide">Select Plan</option>
                          </select>
                        </div>
                      <sup class="sup-errors"></sup>
                      </td>
                      <td id="rider_sum_assured_error" class="col-xs-6 error-rider_sum_assured_1">
                        <div class="input-group col-xs-9">
                          <input type="text" name="rider_sum_assured[]" class="form-control borderzero" id="row-rider_sum_assured" maxlength="255" placeholder="Sum Assured">
                        </div>
                      <sup class="sup-errors"></sup>
                      </td>
                      <td class="hide-sta leftalign">
                        <input type="hidden" name="riders_id[]" value="">
                        <input type="hidden" name="riders_sub_id[]" value="1">
                        <button type="button" value="" class="hide-view btn-del-plan" title="Remove"> <i class="fa fa-close"> </i></button>
                      </td>
                    </tr>
                </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="row">
        <hr class="hr">
        </div>
        <div class="row">
        <div class="col-sm-6">
        <div class="form-group">
            <label for="row-policy_owner" class="col-sm-4">CURRENCY PREMIUM AMOUNT</label>
        <div class="col-sm-8">
          <div class="row" style="padding: 0 15px !important;">
            <div class=" error-currency_value">
                <div class="input-group">
              <input type="number" name="currency_value"  step="0.01" class="form-control borderzero" id="row-currency_value" maxlength="255" placeholder="" oninput="">
                <span class="input-group-addon">
                <input type="text" id="row-currency_rate">
                  <select id="row-currency_id" name="currency_id" class="select" onchange="">
                    <option class="hide" value="">Select:</option>
                    @foreach($currencies as $row)
                      <option value="{{ $row->id }}"><span id="currencyName" value="{{$row->code}}">{{$row->code}}</span></option>
                    @endforeach
                  </select>
                </span>
              </div>
              <sup class=" sup-errors"></sup>
            </div>
            <div class="hide">
              <div id="inputpad" class="input-group">
                <input type="text" id="row-conv_total" name="conv_total" class="form-control">
                <span class="input-group-addon">
                  <span class="input-group" id="total_currency">SGD</span>
                </span>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
        <div class="col-sm-6">
        <div class="form-group">
            <label for="row-policy_owner" class="col-sm-4">TOTAL PREMIUM AMOUNT</label>
        <div class="col-sm-8">
            <div class="">
                <div class="input-group">
              <input type="number"  step="0.01"  name="currency_value2" step="0.01" class="form-control borderzero" id="row-currency_value2" maxlength="255" readonly>
                <span class="input-group-addon">
                <input type="text" id="row-currency_rate2">
                  <select id="row-currency2_id" name="currency2_id" class="select" onchange="">
                    <option class="hide" value="">Select:</option>
                    @foreach($currencies as $row)
                       <option value="{{ $row->id }}"><span id="currencyName" value="{{$row->code}}">{{$row->code}}</span></option>
                    @endforeach
                  </select>
                </span>
              </div>
            </div>
            <div class="hide ">
              <div id="inputpad" class="input-group">
                  <input type="text" id="row-life_total_amount" name="life_total_amount" class="form-control" value="" readonly="readonly">
                  <span class="input-group-addon">
                    <span class="input-group" id="total_currency3">SGD</span>
                  </span>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
<!--         <div class="row">
        <hr class="hr">
        </div> -->
        <div class="row">
      <div class="col-lg-6">
            <div class="form-group">
              <label for="row-postal_code" class="col-sm-4">POLICY TERM</label>
              <div class="col-sm-8 error-policy_term ">
                  <input type="text" name="policy_term" class="form-control borderzero" id="row-policy_term" maxlength="255">
                  <sup class="sup-errors"></sup>
              </div>
            </div>
      <div class="form-group">
       <label for="row-email" class="col-sm-4">PAYMENT FREQUENCY</label>
        <div class="col-sm-8 error-payment_frequency ">
            <select name="payment_frequency" id="row-payment_frequency" class="form-control borderzero">
                <option value="">Select Frequency</option>
                <option value="Single">Single</option>
                <option value="Yearly">Yearly</option>
                <option value="Half-Yearly">Half-Yearly</option>
                <option value="Quarterly">Quarterly</option>
                <option value="Monthly">Monthly</option>
            </select>
            <sup class="sup-errors"></sup>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="row-address" class="col-sm-4">APE</label>
        <div class="col-sm-8 error-ape">
            <input type="number" name="ape" step="0.01"  class="form-control borderzero" id="row-ape" maxlength="255" readonly>
            <sup class=" sup-errors"></sup>
        </div>
      </div>
            <div class="form-group">
              <label for="row-postal_code" class="col-sm-4">MODE OF PAYMENT</label>
              <div class="col-sm-8 error-mode_of_payment">
                  <select name="mode_of_payment" id="row-mode_of_payment" class="form-control borderzero">
                <option value="">Select Payment</option>
                <option value="Cheque">Cheque</option>
                <option value="Credit Card">Credit Card</option>
                <option value="Direct Transfer">Direct Transfer</option>
                <option value="CPF">CPF</option>
                <option value="SRS">SRS</option>
                <option value="GIRO Payment">GIRO Payment</option>
                <option value="Others">Others</option>
            </select>
            <sup class=" sup-errors"></sup>
            </div>
          </div>

        <div id="form-mode_others" class="hide col-lg-12">
          <label for="row-mode_others" class="col-sm-4">&nbsp;</label>
          <div class="col-sm-8 error-mode_others">
            <input type="text" class="form-control borderzero" name="mode_others" id="row-mod_others">
            <sup class="sup-errors"></sup>
          </div>
        </div>
    </div>
  </div>
<div class="row">
<div class="col-sm-6">
  <div class="form-group">
            <label for="row-source" class="col-sm-4">SOURCE</label>
        <div class="col-sm-8 error-source ">
            <select name="source" id="row-source" class="form-control borderzero">
                <option value="">Select Source</option>
                <!-- <option value="Aviva Lead">Aviva Lead</option> -->
                <option value="Call Centre">Call Centre</option>
                <option value="Canvassing">Canvassing</option>
                <option value="Door Knocking">Door Knocking</option>
                <option value="Existing Client">Existing Client</option>
                <option value="Fighter">Fighter</option>
                <!-- <option value="MyCare Planning">MyCare Planning</option> -->
                <option value="Others">Others</option>
                <!-- <option value="Pod Duty">Pod Duty</option> -->
                <option value="Referrals">Referrals</option>
                <option value="Road Show">Road Show</option>
                <option value="Warm">Warm</option>
            </select>
            <sup class="sup-errors"></sup>
        </div>
        </div>
      </div>
      <div class="col-sm-6">
      <div class="form-group">
        <label for="row-source" class="col-sm-4">PDPA CONSENT</label>
        <div class="col-sm-8">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <label>Marketing:
            <br>
            <input type="radio" name="marketing" id="row-consent_marketing_val_one" value="" checked="checked">Yes
            <br>
            <input type="radio" name="marketing" id="row-consent_marketing_val_two" value="">No
            </label>
            <input type="hidden" name="consent_marketing" id="row-consent_marketing" value="yes">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 hide">
            <label>Servicing:
            <br>
            <input type="radio" name="servicing" id="row-consent_servicing_val_one" value="" checked="checked">Yes
            <br>
            <input type="radio" name="servicing" id="row-consent_servicing_val_two" value="">No
            </label>
            <input type="hidden" name="consent_servicing" id="row-consent_servicing" value="yes">
        </div>
      </div>
    </div>
  </div>
</div>






      
    </div>
    <div class="clearfix"></div>
    <div class="modal-footer borderzero">      
    <input type="hidden" name="id" id="row-id">
        @if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
          <button type="button" class="btn btn-success btn-verified borderzero hide">Verified</button>
          <div id="submission-form" class="hide">
            <button type="button" class="btn btn-success btn-approved borderzero">Verify</button>
            <button type="button" class="btn btn-danger btn-reject borderzero">Reject</button>
            <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
          </div>
          <div id="screened-form" class="hide">
            <button type="button" class="btn btn-warning btn-undetermined-life borderzero">Undetermined</button>
            <button type="button" class="btn btn-danger btn-negative-life borderzero">Negative</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
          </div>
        @endif
          <button type="submit" class="btn btn-default btn-submit hide-view borderzero">Submit</button>
    </div>
  </div>
    {!! Form::close() !!}
  </div>
</div>

<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Case Reject Modal -->
  <div id="modal-reject" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div class="modal-header modal-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <div id="load-form" class="loading-pane hide">
            <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
          </div>
          <h4 class="modal-reject-title">Case Reject</h4>
        </div>
        <div id="reject_notice"></div>
        
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-case_id" name="id">
          <button type="submit" class="btn btn-default borderzero" data-dismiss="modal" id="modal-reject-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

<!-- Case Approval Modal -->
  <div id="modal-approved" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div class="modal-header modal-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <div id="load-form" class="loading-pane hide">
            <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
          </div>
          <h4 class="modal-approved-title">Case Verification</h4>
        </div>
        <div id="reject_notice"></div>
       
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-case_id" name="id">
          <button type="submit" class="btn btn-default borderzero" data-dismiss="modal" id="modal-approved-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Case Verification Modal -->
  <div id="modal-verified" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div class="modal-header modal-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <div id="load-form" class="loading-pane hide">
            <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
          </div>
          <h4 class="modal-approved-title">Case Verification</h4>
        </div>
        <div id="reject_notice"></div>
     
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-case_id" name="id">
          <button type="submit" class="btn btn-default borderzero" data-dismiss="modal" id="modal-verified-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Team Modal -->
  <div class="modal fade" id="modal-assessment" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
          {!! Form::open(array('url' => 'sales/assessment', 'role' => 'form', 'id' => 'modal-save_assessment', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="assessment-title"><i class="fa fa-cog"></i> Supervisor</h4>
           </div>
           <div class="modal-body">
            <div id="assessment-notice"></div>
            <div class="form-group">
              <label for="row-supervisor_id" class="col-sm-3 control-label">Supervisor</label>
            <div class="col-sm-9">
              <select name="supervisor_id" id="row-supervisor_id" class="form-control input-md borderzero" style="width: 100%;">
              </select>
            </div>
            </div>
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="assessment-id" value="">
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view borderzero btn-submit"><i class="fa fa-save"></i> Save changes</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>

    <!-- modal add product -->
<div class="modal fade" id="modal-gi" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog modal-lg">
<div class="modal-content borderzero">
      <div id="load-gi" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
    {!! Form::open(array('url' => 'policy/production/gi-case-submission/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_production')) !!}
    <div class="modal-header modal-warning">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-viewing modal-body">
      @if(Auth::user()->usertype_id == 8)
      <input type="hidden" name="user_sales_id" value="{{ Auth::user()->id }}"> 
      @else
      <div class="form-group">
          <label for="gi-user_sales_id" class="col-sm-2">*AGENT</label>
        <div class="col-sm-10 error-user_sales_id">
          <select name="user_sales_id" id="gi-user_sales_id" class="form-control input-md borderzero multi_select" style="width: 100%;" placeholder="Agent">
            <option value="" class="hide"></option>
            @foreach($sales as $row)
              <option value="{{ $row->id }}">[{{ $row->code }}] {{ $row->name }}</option>
            @endforeach
          </select> 
          <sup class=" sup-errors"></sup>
        </div>
      </div>
      @endif

      <div class="form-group">
        <label for="gi-company_name" class="col-sm-2">*CLIENT/COMPANY NAME</label>
        <div class="col-sm-10 error-company_name">
          <input type="text" name="company_name" class="form-control borderzero" id="gi-company_name" maxlength="100">
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
       <label for="gi-nationality_id" class="col-sm-2">*NATIONALITY</label>
        <div class="col-sm-10 error-nationality_id ">
          <select name="nationality_id" id="gi-nationality_id" class="form-control borderzero multi_select">
            <option class="hide" value="">Select Nationality</option>
            @foreach($nations as $row)
            <option value="{{ $row->id }}" {{ ($row->id == 124 ? 'selected' : '') }}>{{ $row->nationality }}</option>
            @endforeach
          </select>
          <sup class="sup-errors"></sup>
        </div>
      </div>


      <div class="row">
        <div class="col-xs-12">
        <div class="form-group">
         <label for="gi-residency_status" class="col-sm-2">*RESIDENCY STATUS</label>
          <div class="col-sm-10 error-residency_status ">
              <select name="residency_status" id="gi-residency_status" class="form-control borderzero">
                <option value="">Select Status</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Singapore PR">Singapore PR</option>
                <option value="Employment Pass">Employment Pass</option>
                <option value="S-Pass">S-Pass</option>
                <option value="Work Permit">Work Permit</option>
                <option value="Dependent’s Pass">Dependent’s Pass</option>
                <option value="Student Pass">Student Pass</option>
                <option value="Others">Others</option>
              </select>
              <sup class="sup-errors"></sup>
          </div>
        </div>
        </div>
        <div class="col-sm-offset-2 col-sm-10">
          <div class="form-residency_others hide">
          <div class="form-group">
            <div class="col-sm-12 error-residency_others ">
                <input type="text" name="residency_others" class="form-control borderzero" id="gi-residency_others" placeholder="Others" maxlength="30">
                <sup class="sup-errors"></sup>
            </div>
          </div>`
        </div>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-code" class="col-sm-2">*DATE OF BIRTH</label>
        <div class="col-sm-10 error-owner_dob">
            <div>
              <div class="input-group date owner_dob">
                <input class="form-control" name="owner_dob" id="gi-owner_dob" name="owner_dob" type="text" placeholder="dd/mm/yyyy">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
              <sup class="sup-errors"></sup>
            </div>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-nric" class="col-sm-2">*NRIC / UEN NO.</label>
        <div class="col-sm-10 error-nric">
          <input type="text" name="nric" class="form-control borderzero" id="gi-nric" maxlength="100">
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-contact_no" class="col-sm-2">*CONTACT NO</label>
        <div class="col-sm-10 error-contact_no">
          <input type="text" name="contact_no" class="form-control borderzero" id="gi-contact_no" maxlength="100">
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-provider_list" class="col-sm-2">*INSURANCE COMPANY</label>
        <div class="col-sm-10 error-provider_list">
          <select name="provider_list" id="gi-provider_list" class="form-control borderzero">
            <option value="">Select Provider</option>
            @foreach ($providers as $row)
            <option value="{{ $row->id }}">{{ $row->name }}</option>
            @endforeach
          </select>
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-submission_date" class="col-sm-2">*SUBMISSION DATE</label>
        <div class="col-sm-10 error-submission_date">
          <div>
            <div class="input-group date submission_date">
              <input class="form-control" name="submission_date" id="gi-submission_date" name="submission_date" type="text" placeholder="dd/mm/yyyy" value="{{ Carbon::now()->format('d/m/Y') }}">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
            <sup class="sup-errors"></sup>
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-type_of_insurance" class="col-sm-2">*TYPE OF INSURANCE</label>
        <input type="hidden" name="conversion_rate" id="gi-conversion_rate" value="1">
        <input type="hidden" name="conversion_year" id="gi-conversion_year" value="others">
        <input type="hidden" name="conversion_gst" id="gi-conversion_gst" value="0">
        <div class="col-sm-10 error-type_of_insurance">
            <select name="type_of_insurance" id="gi-type_of_insurance" class="form-control borderzero">
              <option value="">Type of Insurance</option>
              <option value="Travel">Travel</option>
              <option value="Home">Home</option>
              <option value="PA">PA</option>
              <option value="Motor">Motor</option>
              <option value="Medical">Medical</option>
              <option value="Commercial Lines">Commercial Lines</option>
              <option value="Domestic Helper">Domestic Helper</option>
              <option value="Employee Benefits">Employee Benefits</option>
              <option value="Others">Others</option>
            </select>
            <sup class="sup-errors"></sup>
        </div>
      </div>

      <div id="gi_form-type_of_insurance_others" class="form-group hide">
        <label for="gi-type_of_insurance_others" class="col-sm-2">&nbsp;</label>
        <div class="col-sm-10 error-type_of_insurance_others">
          <input type="text" class="form-control borderzero" name="type_of_insurance_others" id="gi-type_of_insurance_others">
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-currency_value2" class="col-sm-2">*TOTAL PREMIUM AMOUNT</label>
        <div class="col-sm-10 error-currency_value2">
          <div class="input-group">
            <input type="number"  step="0.01"  name="currency_value2" step="0.01" class="form-control borderzero" id="gi-currency_value2" maxlength="255" readonly>
              <span class="input-group-addon">
              <input type="text" id="gi-currency_rate2">
                <select id="gi-currency_id" name="currency_id" class="select" onchange="">
                  <option class="hide" value="">Select:</option>
                  @foreach($currencies as $gi)
                    <option value="{{ $gi->id }}"><span id="currencyName" value="{{$gi->code}}">{{$gi->code}}</span></option>
                  @endforeach
                </select>
              </span>
          </div>
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-ape" class="col-sm-2">*APE</label>
        <div class="col-sm-10 error-ape">
          <input type="number"  step="0.01"  name="ape" step="0.01" class="form-control borderzero" id="gi-ape" maxlength="255" readonly>
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
       <label for="gi-payment_frequency" class="col-sm-2">*PAYMENT FREQUENCY</label>
        <div class="col-sm-10 error-payment_frequency ">
          <select name="payment_frequency" id="gi-payment_frequency" class="form-control borderzero">
            <option value="">Select Frequency</option>
            <option value="Single">Single</option>
            <option value="Yearly">Yearly</option>
            <option value="Half-Yearly">Half-Yearly</option>
            <option value="Quarterly">Quarterly</option>
            <option value="Monthly">Monthly</option>
          </select>
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-mode_of_payment" class="col-sm-2">*MODE OF PAYMENT</label>
        <div class="col-sm-10 error-mode_of_payment">
          <select name="mode_of_payment" id="gi-mode_of_payment" class="form-control borderzero">
              <option value="">Select Payment</option>
              <option value="Cheque">Cheque</option>
              <option value="Credit Card">Credit Card</option>
              <option value="Direct Transfer">Direct Transfer</option>
              <option value="GIRO Payment">GIRO Payment</option>
              <option value="Others">Others</option>
          </select>
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div id="gi_form-mode_others" class="form-group">
        <label for="gi-mode_others" class="col-sm-2">*OTHERS</label>
        <div class="col-sm-10 error-mode_others">
          <input type="text" class="form-control borderzero" name="mode_others" id="gi-mod_others">
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="gi-source" class="col-sm-2">*SOURCE</label>
        <div class="col-sm-10 error-source ">
          <select name="source" id="gi-source" class="form-control borderzero">
            <option value="">Select Source</option>
            <option value="Call Centre">Call Centre</option>
            <option value="Canvassing">Canvassing</option>
            <option value="Door Knocking">Door Knocking</option>
            <option value="Existing Client">Existing Client</option>
            <option value="Fighter">Fighter</option>
            <option value="Others">Others</option>
            <option value="Referrals">Referrals</option>
            <option value="Road Show">Road Show</option>
            <option value="Warm">Warm</option>
          </select>
          <sup class="sup-errors"></sup>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
    <div class="modal-footer borderzero">      
      <input type="hidden" name="id" id="gi-id">

      @if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
        <button type="button" class="btn btn-success btn-verified-gi borderzero hide">Verified</button>
        <div id="submission-form-gi" class="hide">
        <button type="button" class="btn btn-warning btn-approved-gi borderzero">Verify</button>
        <button type="button" class="btn btn-danger btn-reject-gi borderzero">Reject</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
        </div>
        <div id="screened-form-gi" class="hide">
          <button type="button" class="btn btn-warning btn-undetermined-gi borderzero">Undetermined</button>
          <button type="button" class="btn btn-danger btn-negative-gi borderzero">Negative</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
        </div>
      @endif
      <button type="submit" class="btn btn-default btn-submit hide-view borderzero">Submit</button>
    </div>
  </div>
    {!! Form::close() !!}
  </div>
</div>


