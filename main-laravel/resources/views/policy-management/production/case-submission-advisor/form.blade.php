<!-- modal add product -->
<div class="modal fade" id="modal-production" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog modal-lg">
<div class="modal-content borderzero">
      <div id="load-form" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
    {!! Form::open(array('url' => 'policy/production/case-submission-advisor/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_production')) !!}
    <div class="modal-header modal-warning">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-body">
    <div id="production-notice"></div>  
    <div class="form-group">
              <label for="row-user_sales_id" class="col-sm-3">AGENT</label>
            <div class="col-sm-9">
              <select name="user_sales_id" id="row-user_sales_id" class="form-control input-md borderzero multi_select" style="width: 100%;" placeholder="Agent">
                <option value="" class="hide"></option>
                @foreach($sales as $row)
                  <option value="{{ $row->id }}">[{{ $row->code }}] {{ $row->name }}</option>
                @endforeach
              </select> 
            </div>
          </div>
    <div class="form-group">
            <label for="row-policy_owner" class="col-sm-3">POLICY OWNER</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <input type="text" name="firstname" class="form-control borderzero" id="row-firstname" maxlength="30" placeholder="Given Name">
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <input type="text" name="lastname" class="form-control borderzero" id="row-lastname" maxlength="30" placeholder="Family Name">
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-life_assured" class="col-sm-3">LIFE ASSURED</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <input type="text" name="life_firstname" class="form-control borderzero" id="row-life_firstname" maxlength="30" placeholder="Given Name">
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <input type="text" name="life_lastname" class="form-control borderzero" id="row-life_lastname" maxlength="30" placeholder="Family Name">
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-annual_income_range" class="col-sm-3">ANNUAL INCOME RANGE</label>
        <div class="col-sm-9">
            <select name="annual_income_range" id="row-annual_income_range" class="form-control borderzero">
              <option>Select Income Range</option>
              <option value="Below $30,000">Below $30,000</option>
              <option value="$30,001 - $50,000">$30,001 - $50,000</option>
              <option value="$50,001 - $75,000">$50,001 - $75,000</option>
              <option value="$75,001 - 100,000">$75,001 - 100,000</option>
              <option value="$100,001 - 150,000">$100,001 - 150,000</option>
              <option value="$150,001 - 250,000<">$150,001 - 250,000</option>
              <option value="$250,001 - Above">$250,001 - Above</option>
            </select>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <div class="col-lg-9 col-lg-offset-3">
            <div class="col-lg-6" style="padding-left: 0;">
                <label for="row-code">POLICY OWNER</label>
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <label for="row-code" >LIFE ASSURED</label>
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">IDENTITY CARD/PASSPORT NO.</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <input type="text" name="owner_id" class="form-control borderzero" id="row-owner_id" maxlength="30" placeholder="">
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <input type="text" name="life_id" class="form-control borderzero" id="row-life_id" maxlength="30" placeholder="">
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">OCCUPATION</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <input type="text" name="owner_occupation" class="form-control borderzero" id="row-owner_occupation" maxlength="30" placeholder="">
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <input type="text" name="life_occupation" class="form-control borderzero" id="row-life_occupation" maxlength="30" placeholder="">
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">GENDER</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <select name="owner_gender" id="row-owner_gender" class="form-control borderzero">
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <select name="life_gender" id="row-life_gender" class="form-control borderzero">
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">DATE OF BIRTH</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <div class="input-group date owner_dob">
                  <input class="form-control" name="owner_dob" id="row-owner_dob" name="owner_dob" type="text">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <div class="input-group date life_dob">
                  <input class="form-control" name="life_dob" id="row-life_dob" name="life_dob" type="text">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">AGE NEXT BIRTHDAY</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <input type="text" name="owner_age" class="form-control borderzero" id="row-owner_age" maxlength="30" placeholder="">
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <input type="text" name="life_age" class="form-control borderzero" id="row-life_age" maxlength="30" placeholder="">
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-contact" class="col-sm-3">TELEPHONE NO.</label>
        <div class="col-sm-9">
            <input type="text" name="contact" class="form-control borderzero" id="row-contact" maxlength="30">
        </div>
    </div>
    <div class="form-group">
            <label for="row-address" class="col-sm-3">ADDRESS</label>
        <div class="col-sm-9">
            <input type="text" name="address" class="form-control borderzero" id="row-address" maxlength="30">
        </div>
    </div>
    <div class="form-group">
            <label for="row-postal_code" class="col-sm-3">POSTAL CODE</label>
        <div class="col-sm-9">
            <input type="text" name="postal_code" class="form-control borderzero" id="row-postal_code" maxlength="30">
        </div>
    </div>
    <div class="form-group">
            <label for="row-email" class="col-sm-3">EMAIL</label>
        <div class="col-sm-9">
            <input type="text" name="email" class="form-control borderzero" id="row-email" maxlength="30">
        </div>
    </div>
    <div class="form-group">
            <label for="row-type_of_plan" class="col-sm-3">TYPE OF PLAN</label>
        <div class="col-sm-9">
            <select  name="type_of_plan" id="row-type_of_plan" class="form-control borderzero">
                <option>Select Plan</option>
                <option value="BUPA">BUPA</option>
                <option value="Global Savings Account">Global Savings Account</option>
                <option value="Ideal CI Protector">Ideal CI Protector</option>
                <option value="Ideal Income">Ideal Income</option>
                <option value="LifeTimeFlexi">LifeTimeFlexi</option>
                <option value="My FamilyCover Plan 1">My FamilyCover Plan 1</option>
                <option value="My FamilyCover Plan 2">My FamilyCover Plan 2</option>
                <option value="My FamilyCover Plan 3">My FamilyCover Plan 3</option>
                <option value="My FamilyCover Plan 4">My FamilyCover Plan 4</option>
                <option value="MyCare">MyCare</option>
                <option value="MyCarePlus">MyCarePlus</option>
                <option value="MyEduPlan">MyEduPlan</option>
                <option value="MyFlexiSaver">MyFlexiSaver</option>
                <option value="MyIncomPlus">MyIncomPlus</option>
                <option value="MyLifeChoice">MyLifeChoice</option>
                <option value="MyProtector - Decreasing">MyProtector - Decreasing</option>
                <option value="MyProtector - Level Plus (5yr policy term)">MyProtector - Level Plus (5yr policy term)</option>
                <option value="MyProtector - Level Plus (10yr policy term)">MyProtector - Level Plus (10yr policy term)</option>
                <option value="MyProtector - Level Plus (11yr policy term)">MyProtector - Level Plus (11yr policy term)</option>
                <option value="MyProtector - Level Term">MyProtector - Level Term</option>
                <option value="MyProtector - MoneyBack">MyProtector - MoneyBack</option>
                <option value="MyRegularPay">MyRegularPay</option>
                <option value="MyRetirement">MyRetirement</option>
                <option value="MyRetirement 8 Years">MyRetirement 8 Years</option>
                <option value="MyShield">MyShield</option>
                <option value="MyShieldPlus">MyShieldPlus</option>
                <option value="MyWealthPlan 10yr Ltd Pay">MyWealthPlan 10yr Ltd Pay</option>
                <option value="MyWealthPlan 5yr Ltd Pay">MyWealthPlan 5yr Ltd Pay</option>
                <option value="Navigator">Navigator</option>
            </select>
        </div>
    </div>
    <div class="form-group">
            <label for="row-sum_assured" class="col-sm-3">SUM ASSURED</label>
        <div class="col-sm-9">
            <input type="text" name="sum_assured" class="form-control borderzero" id="row-sum_assured" maxlength="30">
        </div>
    </div>
    <hr>
    <div class="form-group">
        <div class="col-lg-9 col-lg-offset-3">
            <div class="col-lg-6" style="padding-left: 0;">
                <label for="row-code">RIDERS</label>
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-amount" class="col-sm-3">CURRENCY PREMIUM ACCOUNT</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <input type="text" name="amount" class="form-control borderzero" id="row-amount" maxlength="30" placeholder="">
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <select name="currency" id="row-currency" class="form-control borderzero">
                    <option>Select</option>
                    <option value="SGD">SGD</option>
                    <option value="USD">USD</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">TOTAL PREMIUM ACCOUNT</label>
        <div class="col-sm-9">
            <div class="col-lg-6" style="padding-left: 0;">
                <input type="text" name="prem_account" class="form-control borderzero" id="row-prem_account" maxlength="30" placeholder="">
            </div>
            <div class="col-lg-6" style="padding-right: 0;">
                <select name="prem_currency" id="row-prem_currency" class="form-control borderzero">
                    <option>Select</option>
                    <option value="SGD">SGD</option>
                    <option value="USD">USD</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">POLICY TERM</label>
        <div class="col-sm-9">
            <select name="policy_term" id="row-policy_term" class="form-control borderzero">
                <option>Select Source</option>
                <option value="Aviva Lead">Aviva Lead</option>
                <option value="Call Centre">Call Centre</option>
                <option value="Canvassing">Canvassing</option>
                <option value="Door Knocking">Door Knocking</option>
                <option value="Existing Client">Existing Client</option>
                <option value="Fighter">Fighter</option>
                <option value="MyCare Planning">MyCare Planning</option>
                <option value="Others">Others</option>
                <option value="Pod Duty">Pod Duty</option>
                <option value="Referrals">Referrals</option>
                <option value="Road Show">Road Show</option>
                <option value="Warm">Warm</option>
            </select>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">PAYMENT FREQUENCY</label>
        <div class="col-sm-9">
            <select name="payment_frequency" id="row-payment_frequency" class="form-control borderzero">
                <option>Select</option>
                <option value="Single">Single</option>
                <option value="Double">Double</option>
            </select>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">MODE OF PAYMENT</label>
        <div class="col-sm-9">
            <select name="mode_of_payment" id="row-mode_of_payment" class="form-control borderzero">
                <option value="Cash">Cash</option>
                <option value="Credit">Credit</option>
            </select>
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">APE</label>
        <div class="col-sm-9">
            <input type="text" name="ape" class="form-control borderzero" id="row-ape" maxlength="30">
        </div>
    </div>
    <div class="form-group">
            <label for="row-code" class="col-sm-3">CKA</label>
        <div class="col-sm-9">
            <div class="col-sm-1" style="padding: 0;">
                <label>
                    <input type="radio" name="cka" id="cka" value="N/A" checked>
                    N/A
                </label>
            </div>
            <div class="col-sm-1" style="padding: 0;">
                <label>
                    <input type="radio" name="cka" id="cka" value="Pass" >
                    Pass
                </label>
            </div>
            <div class="col-sm-4" style="padding: 0;">
                <label>
                    <input type="radio" name="cka" id="cka" value="Fail and follow agents advice" >
                    Fail and follow agents advice
                </label>
            </div>
            <div class="col-sm-5" style="padding: 0;">
                <label>
                    <input type="radio" name="cka" id="cka" value="Fail but does not follow agents advice" >
                    Fail but does not follow agents advice
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
            <label for="row-source" class="col-sm-3">SOURCE</label>
        <div class="col-sm-9">
            <select name="source" id="row-source" class="form-control borderzero">
                <option>Select Source</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </div>
    </div>
    <div class="clearfix"></div>
    <div class="modal-footer borderzero">
      <input type="hidden" name="id" id="row-id">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
    </div>
    {!! Form::close() !!}
  </div>
</div>
</div>
    </div> 

<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
