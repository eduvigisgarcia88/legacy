<style>
  

.checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}

</style>
<!-- modal add product -->
<div class="modal fade" id="modal-production" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog modal-lg">
<div class="modal-content borderzero">
      <div id="load-form" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
    {!! Form::open(array('url' => 'policy/production/gi-case-submission/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_production')) !!}
    <div class="modal-header modal-warning">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-viewing modal-body">
      @if(Auth::user()->usertype_id == 8)
      <input type="hidden" name="user_sales_id" value="{{ Auth::user()->id }}"> 
      @else
      <div class="form-group">
          <label for="row-user_sales_id" class="col-sm-2">*AGENT</label>
        <div class="col-sm-10 error-user_sales_id">
          <select name="user_sales_id" id="row-user_sales_id" class="form-control input-md borderzero multi_select" style="width: 100%;" placeholder="Agent">
            <option value="" class="hide"></option>
            @foreach($sales as $row)
              <option value="{{ $row->id }}">[{{ $row->code }}] {{ $row->name }}</option>
            @endforeach
          </select> 
          <sup class=" sup-errors"></sup>
        </div>
      </div>
      @endif

      <div class="form-group">
        <label for="row-company_name" class="col-sm-2">*CLIENT/COMPANY NAME</label>
        <div class="col-sm-10 error-company_name">
          <input type="text" name="company_name" class="form-control borderzero" id="row-company_name" maxlength="100">
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
       <label for="row-nationality_id" class="col-sm-2">*NATIONALITY</label>
        <div class="col-sm-10 error-nationality_id ">
          <select name="nationality_id" id="row-nationality_id" class="form-control borderzero multi_select">
            <option value="" selected>Select Nationality</option>
            @foreach($nations as $row)
            <option value="{{ $row->id }}">{{ $row->nationality }}</option>
            @endforeach
          </select>
          <sup class="sup-errors"></sup>
        </div>
      </div>


      <div class="row">
        <div class="col-xs-12">
        <div class="form-group">
         <label for="row-residency_status" class="col-sm-2">*RESIDENCY STATUS</label>
          <div class="col-sm-10 error-residency_status ">
              <select name="residency_status" id="row-residency_status" class="form-control borderzero">
                <option value="">Select Status</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Singapore PR">Singapore PR</option>
                <option value="Employment Pass">Employment Pass</option>
                <option value="S-Pass">S-Pass</option>
                <option value="Work Permit">Work Permit</option>
                <option value="Dependent’s Pass">Dependent’s Pass</option>
                <option value="Student Pass">Student Pass</option>
                <option value="Others">Others</option>
              </select>
              <sup class="sup-errors"></sup>
          </div>
        </div>
        </div>
        <div class="col-sm-offset-2 col-sm-10">
          <div class="form-residency_others hide">
          <div class="form-group">
            <div class="col-sm-12 error-residency_others ">
                <input type="text" name="residency_others" class="form-control borderzero" id="row-residency_others" placeholder="Others" maxlength="30">
                <sup class="sup-errors"></sup>
            </div>
          </div>`
        </div>
        </div>
      </div>

      <div class="form-group">
        <label for="row-code" class="col-sm-2">*DATE OF BIRTH</label>
        <div class="col-sm-10 error-owner_dob">
            <div>
              <div class="input-group date owner_dob">
                <input class="form-control" name="owner_dob" id="row-owner_dob" name="owner_dob" type="text" placeholder="dd/mm/yyyy">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
              <sup class="sup-errors"></sup>
            </div>
        </div>
      </div>

      <div class="form-group">
        <label for="row-nric" class="col-sm-2">*NRIC / UEN NO.</label>
        <div class="col-sm-10 error-nric">
          <input type="text" name="nric" class="form-control borderzero" id="row-nric" maxlength="100">
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="row-contact_no" class="col-sm-2">*CONTACT NO</label>
        <div class="col-sm-10 error-contact_no">
          <input type="text" name="contact_no" class="form-control borderzero" id="row-contact_no" maxlength="100">
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="row-provider_list" class="col-sm-2">*INSURANCE COMPANY</label>
        <div class="col-sm-10 error-provider_list">
          <select name="provider_list" id="row-provider_list" class="form-control borderzero">
            <option value="">Select Provider</option>
            @foreach ($providers as $row)
            <option value="{{ $row->id }}">{{ $row->name }}</option>
            @endforeach
          </select>
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="row-submission_date" class="col-sm-2">*SUBMISSION DATE</label>
        <div class="col-sm-10 error-submission_date">
          <div>
            <div class="input-group date submission_date">
              <input class="form-control" name="submission_date" id="row-submission_date" name="submission_date" type="text" placeholder="dd/mm/yyyy" value="{{ Carbon::now()->format('d/m/Y') }}">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
            <sup class="sup-errors"></sup>
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="row-type_of_insurance" class="col-sm-2">*TYPE OF INSURANCE</label>
        <input type="hidden" name="conversion_rate" id="row-conversion_rate" value="1">
        <input type="hidden" name="conversion_year" id="row-conversion_year" value="others">
        <input type="hidden" name="conversion_gst" id="row-conversion_gst" value="0">
        <div class="col-sm-10 error-type_of_insurance">
            <select name="type_of_insurance" id="row-type_of_insurance" class="form-control borderzero">
              <option value="">Type of Insurance</option>
              <option value="Travel">Travel</option>
              <option value="Home">Home</option>
              <option value="PA">PA</option>
              <option value="Motor">Motor</option>
              <option value="Medical">Medical</option>
              <option value="Commercial Lines">Commercial Lines</option>
              <option value="Domestic Helper">Domestic Helper</option>
              <option value="Employee Benefits">Employee Benefits</option>
              <option value="Others">Others</option>
            </select>
            <sup class="sup-errors"></sup>
        </div>
      </div>

      <div id="form-type_of_insurance_others" class="form-group hide">
        <label for="row-type_of_insurance_others" class="col-sm-2">&nbsp;</label>
        <div class="col-sm-10 error-type_of_insurance_others">
          <input type="text" class="form-control borderzero" name="type_of_insurance_others" id="row-type_of_insurance_others">
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="row-currency_value2" class="col-sm-2">*TOTAL PREMIUM AMOUNT</label>
        <div class="col-sm-10 error-currency_value2">
          <div class="input-group">
            <input type="number"  step="0.01"  name="currency_value2" step="0.01" class="form-control borderzero" id="row-currency_value2" maxlength="255" readonly>
              <span class="input-group-addon">
              <input type="text" id="row-currency_rate2">
                <select id="row-currency_id" name="currency_id" class="select" onchange="">
                  <option class="hide" value="">Select:</option>
                  @foreach($currencies as $row)
                    <option value="{{ $row->id }}"><span id="currencyName" value="{{$row->code}}">{{$row->code}}</span></option>
                  @endforeach
                </select>
              </span>
          </div>
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="row-ape" class="col-sm-2">*APE</label>
        <div class="col-sm-10 error-ape">
          <input type="number"  step="0.01"  name="ape" step="0.01" class="form-control borderzero" id="row-ape" maxlength="255" readonly>
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
       <label for="row-payment_frequency" class="col-sm-2">*PAYMENT FREQUENCY</label>
        <div class="col-sm-10 error-payment_frequency ">
          <select name="payment_frequency" id="row-payment_frequency" class="form-control borderzero">
            <option value="">Select Frequency</option>
            <option value="Single">Single</option>
            <option value="Yearly">Yearly</option>
            <option value="Half-Yearly">Half-Yearly</option>
            <option value="Quarterly">Quarterly</option>
            <option value="Monthly">Monthly</option>
          </select>
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="row-mode_of_payment" class="col-sm-2">*MODE OF PAYMENT</label>
        <div class="col-sm-10 error-mode_of_payment">
          <select name="mode_of_payment" id="row-mode_of_payment" class="form-control borderzero">
              <option value="">Select Payment</option>
              <option value="Cheque">Cheque</option>
              <option value="Credit Card">Credit Card</option>
              <option value="Direct Transfer">Direct Transfer</option>
              <option value="GIRO Payment">GIRO Payment</option>
              <option value="Others">Others</option>
          </select>
          <sup class=" sup-errors"></sup>
        </div>
      </div>

      <div id="form-mode_others" class="form-group">
        <label for="row-mode_others" class="col-sm-2">*OTHERS</label>
        <div class="col-sm-10 error-mode_others">
          <input type="text" class="form-control borderzero" name="mode_others" id="row-mod_others">
          <sup class="sup-errors"></sup>
        </div>
      </div>

      <div class="form-group">
        <label for="row-source" class="col-sm-2">*SOURCE</label>
        <div class="col-sm-10 error-source ">
          <select name="source" id="row-source" class="form-control borderzero">
            <option value="">Select Source</option>
            <option value="Call Centre">Call Centre</option>
            <option value="Canvassing">Canvassing</option>
            <option value="Door Knocking">Door Knocking</option>
            <option value="Existing Client">Existing Client</option>
            <option value="Fighter">Fighter</option>
            <option value="Others">Others</option>
            <option value="Referrals">Referrals</option>
            <option value="Road Show">Road Show</option>
            <option value="Warm">Warm</option>
          </select>
          <sup class="sup-errors"></sup>
        </div>
      </div>

        <div class="form-group" style="margin-top:17px;">
        <label for="row-selected_client" class="col-sm-2">SELECTED<br>CLIENT</label>
        <div class="col-sm-8">
            <div class="checkbox" style="bottom:7px; right:15px;">
                <label style="font-size: 2em">
                    <input id="row-selected_client" type="checkbox" value="1" name="selected_client">
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                </label>
            </div>
        </div>
        </div>

      <div class="form-file">
        <div class="form-group upload_file_2 hide">
          <label for="upload" class="col-sm-2">NRIC</label>
          <div class="col-sm-10 error-upload_2 hide-view">
            <input type="hidden" name="upload_remove_2" id="row-upload_remove_2">
            <input type="file" class="form-control borderzero file file_case" id="row-upload_2_change" name="upload_2" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <sup class="sup-errors"></sup>
            <button type="button" class="btn btn-default btn-cancel-upload_2 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
          </div>
            <input type="hidden" class="form-control borderzero" name="upload_name_2" id="row-upload_name_2" placeholder="APP FORM Filename">
          <div class="col-sm-10 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_2 hide">
          <label for="upload" class="col-sm-2">NRIC</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="upload_2_title" readonly="readonly" id="row-upload_2_title">
            <a target="_blank" href="" id="row-download_2" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Preview</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_2 borderzero"><i class="fa fa-trash"></i> Remove</button>
            <div class="input-group hide">
              <input type="text" class="form-control" id="row-link_2">
              <span class="input-group-btn">
                <button class="btn btn-default btn-copy linkster" type="button" data-clipboard-target="#row-link_2" title="Copied!"><i class="fa fa-link"></i> Copy Link</button>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group upload_file_3 hide">
          <label for="upload" class="col-sm-2">APP FORM</label>
          <div class="col-sm-10 error-upload_3 hide-view">
            <input type="hidden" name="upload_remove_3" id="row-upload_remove_3">
            <input type="file" class="form-control borderzero file file_case" id="row-upload_3_change" name="upload_3" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <sup class="sup-errors"></sup>
            <button type="button" class="btn btn-default btn-cancel-upload_3 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
          </div>
            <input type="hidden" class="form-control borderzero" name="upload_name_3" id="row-upload_name_3" placeholder="APP FORM Filename">
          <div class="col-sm-10 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_3 hide">
          <label for="upload" class="col-sm-2">APP FORM</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="upload_3_title" readonly="readonly" id="row-upload_3_title">
            <a target="_blank" href="" id="row-download_3" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Preview</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_3 borderzero"><i class="fa fa-trash"></i> Remove</button>
            <div class="input-group hide">
              <input type="text" class="form-control" id="row-link_3">
              <span class="input-group-btn">
                <button class="btn btn-default btn-copy linkster" type="button" data-clipboard-target="#row-link_3" title="Copied!"><i class="fa fa-link"></i> Copy Link</button>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group upload_file_4 hide">
          <label for="upload" class="col-sm-2">SUPPORTING DOC</label>
          <div class="col-sm-10 error-upload_4 hide-view">
            <input type="hidden" name="upload_remove_4" id="row-upload_remove_4">
            <input type="file" class="form-control borderzero file file_case" id="row-upload_4_change" name="upload_4" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <button type="button" class="btn btn-default btn-cancel-upload_4 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
            <sup class="sup-errors"></sup>
          </div>
            <input type="hidden" class="form-control borderzero" name="upload_name_4" id="row-upload_name_4" placeholder="SUPPORTING DOC Filename">
          <div class="col-sm-10 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_4 hide">
          <label for="upload" class="col-sm-2">SUPPORTING DOC</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" readonly="readonly" id="row-upload_4_title">
            <a target="_blank" href="" id="row-download_4" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Preview</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_4 borderzero"><i class="fa fa-trash"></i> Remove</button>
            <div class="input-group hide">
              <input type="text" class="form-control" id="row-link_4">
              <span class="input-group-btn">
                <button class="btn btn-default btn-copy linkster" type="button" data-clipboard-target="#row-link_4" title="Copied!"><i class="fa fa-link"></i> Copy Link</button>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group upload_file_5 hide">
          <label for="upload" class="col-sm-2">OTHERS</label>
          <div class="col-sm-10 error-upload_5 hide-view">
            <input type="hidden" name="upload_remove_5" id="row-upload_remove_5">
            <input type="file" class="form-control borderzero file file_case" id="row-upload_5_change" name="upload_5" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <button type="button" class="btn btn-default btn-cancel-upload_5 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
            <sup class="sup-errors"></sup>
          </div>
            <input type="hidden" class="form-control borderzero" name="upload_name_5" id="row-upload_name_5" placeholder="OTHERS Filename">
          <div class="col-sm-10 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_5 hide">
          <label for="upload" class="col-sm-2">OTHERS</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" readonly="readonly" id="row-upload_5_title">
            <a target="_blank" href="" id="row-download_5" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Preview</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_5 borderzero"><i class="fa fa-trash"></i> Remove</button>
            <div class="input-group hide">
              <input type="text" class="form-control" id="row-link_5">
              <span class="input-group-btn">
                <button class="btn btn-default btn-copy linkster" type="button" data-clipboard-target="#row-link_5" title="Copied!"><i class="fa fa-link"></i> Copy Link</button>
              </span>
            </div>
          </div>
        </div>
      </div>

    </div>
    <div class="clearfix"></div>
    <div class="modal-footer borderzero">
      <input type="hidden" name="id" id="row-id">
          <button type="button" class="btn btn-default borderzero btn-close" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-default btn-submit hide-view borderzero">Submit</button>
    </div>
  </div>
    {!! Form::close() !!}
  </div>
</div>
</div>
</div>

<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>



  <!-- Team Modal -->
  <div class="modal fade" id="modal-assessment" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-assessment" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/gi-case-submission/assessment', 'role' => 'form', 'id' => 'modal-save_assessment', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="assessment-title"><i class="fa fa-cog"></i> Assessment</h4>
           </div>
           <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="assessment-id" value="">
                <input type="hidden" name="assessment_outcome" id="assessment-outcome" value="">
                <button type="button" class="btn btn-warning hide-view borderzero btn-false_positive"> False Positive</button>
                <button type="button" class="btn btn-success hide-view borderzero btn-positive_hit"> Positive Hit</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>

  <!-- Team Modal -->
  <div class="modal fade" id="modal-review" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-review" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/gi-case-submission/review', 'role' => 'form', 'id' => 'modal-save_review', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="review-title"><i class="fa fa-cog"></i> Review</h4>
           </div>
           <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="review-id" value="">
                <input type="hidden" name="review_outcome" id="review-outcome" value="">
                <button type="button" class="btn btn-warning hide-view borderzero btn-false_positive"> False Positive</button>
                <button type="button" class="btn btn-success hide-view borderzero btn-positive_hit"> Positive Hit</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
          {!! Form::close() !!}
        </div> 
    </div>
    </div>



  <!-- Team Modal -->
  <div class="modal fade" id="modal-submitted" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-submitted" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/gi-case-submission/submitted', 'role' => 'form', 'id' => 'modal-save_submitted', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="submitted-title"><i class="fa fa-cog"></i> Submit to HO</h4>
           </div>
           <div class="modal-body">
            <div id="submitted-notice"></div>
              Submit To HO?
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
              <input type="hidden" name="id" id="submitted-id" value="">
              <input type="hidden" name="submitted_outcome" id="submitted-outcome" value="">
              <button type="button" class="btn btn-success hide-view borderzero btn-yes_submit"> Yes</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"> No</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>


  <!-- Team Modal -->
  <div class="modal fade" id="modal-duplicate" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-duplicate" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/gi-case-submission/duplicate', 'role' => 'form', 'id' => 'modal-save_duplicate', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-info">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="duplicate-title"><i class="fa fa-cog"></i> Duplicate Case</h4>
           </div>
           <div class="modal-body">
            <div id="duplicate-notice"></div>
              Duplicate case?
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
              <input type="hidden" name="id" id="duplicate-id" value="">
              <button type="submit" class="btn btn-success hide-view borderzero"> Yes</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"> No</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>