<!-- modal add product -->
<div class="modal fade" id="modal-production" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog modal-lg">
<div class="modal-content borderzero">
      <div id="load-form" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
    {!! Form::open(array('url' => 'policy/production/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_production')) !!}
    <div class="modal-header modal-warning">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
          @if(Auth::user()->usertype_id == 8)
              <input type="hidden" name="user_sales_id" value="{{ Auth::user()->id }}"> 
          @else
          <div class="form-group">
              <label for="row-user_sales_id" class="col-sm-2">AGENT</label>
            <div class="col-sm-10 error-user_sales_id">
              <select name="user_sales_id" id="row-user_sales_id" class="form-control input-md borderzero multi_select" style="width: 100%;" placeholder="Agent">
                <option value="" class="hide"></option>
                @foreach($sales as $row)
                  <option value="{{ $row->id }}">[{{ $row->code }}] {{ $row->name }}</option>
                @endforeach
              </select> 
              <sup class=" sup-errors"></sup>
            </div>
          </div>
          @endif
        </div>
        <div class="col-lg-12">
        <div class="form-group">
            <label for="row-policy_owner" class="col-sm-2">POLICY OWNER</label>
        <div class="col-sm-10">
          <div class="row">
            <div class="col-lg-6 error-firstname">
                <input type="text" name="firstname" class="form-control borderzero" id="row-firstname" maxlength="30" placeholder="Given Name">
                <sup class=" sup-errors"></sup>
            </div>
            <div class="col-lg-6 error-lastname ">
                <input type="text" name="lastname" class="form-control borderzero" id="row-lastname" maxlength="30" placeholder="Family Name">
                <sup class="sup-errors"></sup>
            </div>
          </div>
        </div>
        </div>
        </div>


        <div class="col-lg-12 form-same hide-view">
            <label for="row-same" class="col-sm-2 hidden-xs"></label>
            <label>
              <input type="checkbox" class="hide-view" name="same" id="row-same" value="yes"> POLICY OWNER IS SAME AS LIFE ASSURED
            </label>
        </div>
        <div class="col-lg-12 form-life_assured">
        <div class="form-group">
            <label for="row-life_assured" class="col-sm-2">LIFE ASSURED</label>
        <div class="col-sm-10">
          <div class="row">
            <div class="col-lg-6">
                <input type="text" name="life_firstname" class="form-control borderzero" id="row-life_firstname" maxlength="30" placeholder="Given Name">
            </div>
            <div class="col-lg-6">
                <input type="text" name="life_lastname" class="form-control borderzero" id="row-life_lastname" maxlength="30" placeholder="Family Name">
            </div>
          </div>
        </div>
        </div>
        </div>
        <div class="col-lg-12">
        <div class="form-group">
            <label for="row-annual_income_range" class="col-sm-2">ANNUAL INCOME RANGE</label>
        <div class="col-sm-10 error-annual_income_range">
            <select name="annual_income_range" id="row-annual_income_range" class="form-control borderzero">
              <option value="">Select Income Range</option>
              <option value="$0 - $29,999">$0 - $29,999</option>
              <option value="$30,000 - $49,999">$30,000 - $49,999</option>
              <option value="$50,000 - $99,000">$50,000 - $99,000</option>
              <option value="$100,000 - $149,999">$100,000 - $149,999</option>
              <option value="$150,000 - $299,999">$150,000 - $299,999</option>
              <option value="$300,000 & above">$300,000 & above</option>
            </select>
            <sup class=" sup-errors"></sup>
        </div>
        </div>
        </div>
        <hr class="hr">
        <div class="col-lg-6">
          <div class="row">
        <div class="hidden-md hidden-sm hidden-xs col-lg-12">
            <label for="row-code" class="col-lg-4"><small></small></label>
            <div class="col-lg-8 error-owner_id">
              <h4>POLICY OWNER</h4>
            </div>
        </div>
        <div class="visible-md visible-sm visible-xs col-lg-12">
              <h4>POLICY OWNER</h4>
        </div>
        <div class="col-lg-12">
          <div class="form-group">
            <label for="row-code" class="col-sm-4"><small>IDENTITY CARD/PASSPORT NO.</small></label>
            <div class="col-sm-8 error-owner_id ">
                <div class="">
                    <input type="text" name="owner_id" class="form-control borderzero" id="row-owner_id" maxlength="30" placeholder="">
                    <sup class="sup-errors"></sup>
                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="form-group">
          <label for="row-code" class="col-sm-4">OCCUPATION</label>
            <div class="col-sm-8">
                <div>
                    <input type="text" name="owner_occupation" class="form-control borderzero" id="row-owner_occupation" maxlength="30" placeholder="">
                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
        <div class="form-group">
            <label for="row-code" class="col-sm-4">GENDER</label>
          <div class="col-sm-8">
              <div>
                  <select name="owner_gender" id="row-owner_gender" class="form-control borderzero">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="form-group">
            <label for="row-code" class="col-sm-4">DATE OF BIRTH</label>
            <div class="col-sm-8">
                <div>
                    <div class="input-group date owner_dob">
                      <input class="form-control" name="owner_dob" id="row-owner_dob" name="owner_dob" type="text" placeholder="mm/dd/yyyy">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                </div>
            </div>
          </div>
        </div>

      <div class="col-lg-12">
        <div class="form-group">
         <label for="row-nationality_id" class="col-sm-4">NATIONALITY</label>
          <div class="col-sm-8 error-nationality_id ">
            <select name="nationality_id" id="row-nationality_id" class="form-control borderzero multi_select">
              <option class="hide" value="">Select Nationality</option>
              @foreach($nations as $row)
              <option value="{{ $row->id }}" {{ ($row->id == 124 ? 'selected' : '') }}>{{ $row->nationality }}</option>
              @endforeach
            </select>
            <sup class="sup-errors"></sup>
          </div>
        </div>
      </div>


      <div class="col-lg-12">
      <div class="form-group">
       <label for="row-residency_status" class="col-sm-4">RESIDENCY STATUS</label>
        <div class="col-sm-8 error-residency_status ">
            <select name="residency_status" id="row-residency_status" class="form-control borderzero">
              <option value="">Select Status</option>
              <option value="Singaporean">Singaporean</option>
              <option value="Singaporean / PR">Singaporean / PR</option>
              <option value="Employment Pass">Employment Pass</option>
              <option value="S-Pass">S-Pass</option>
              <option value="Work Permit">Work Permit</option>
              <option value="Dependent’s Pass">Dependent’s Pass</option>
              <option value="Student Pass">Student Pass</option>
              <option value="Others">Others</option>
            </select>
            <sup class="sup-errors"></sup>
        </div>
      </div>
    </div>

      <div class="form-residency_others col-lg-12 hide">
      <div class="form-group">
       <label for="row-residency_others" class="col-sm-4"></label>
        <div class="col-sm-8 error-residency_others ">
            <input type="text" name="residency_others" class="form-control borderzero" id="row-residency_others" maxlength="30">
            <sup class="sup-errors"></sup>
        </div>
      </div>
    </div>


        <div class="col-lg-12">
            <label><input type="checkbox" class="hide-view" name="back_date" id="row-backdate"> BACKDATE
            <input type="hidden" name="backdate_value" id="row-backdate_value"></label>
        </div>
        <div class="col-lg-12">
          <div class="form-group">
            <label for="row-code" class="col-sm-4">AGE NEXT BIRTHDAY</label>
             <div class="col-sm-8">
                <div>
                    <input type="text" name="owner_age" class="form-control borderzero" id="row-owner_age" maxlength="30" placeholder="">
                </div>          
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-12">
          <h4 for="row-code" >LIFE ASSURED</h4>
        </div>
      <div class="col-lg-12">
        <div class="form-group">
              <label for="row-code" class="col-sm-4 visible-xs visible-sm visible-md">IDENTITY CARD/PASSPORT NO.</label>
          <div class="col-sm-8 error-life_id ">
                <div>
                    <input type="text" name="life_id" class="form-control borderzero" id="row-life_id" maxlength="30" placeholder="">
                    <sup class="sup-errors"></sup>
                </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="form-group">
              <label for="row-code" class="col-sm-4 visible-xs visible-sm visible-md">OCCUPATION</label>
          <div class="col-sm-8">
              <div>
                  <input type="text" name="life_occupation" class="form-control borderzero" id="row-life_occupation" maxlength="30" placeholder="">
              </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="form-group">
              <label for="row-code" class="col-sm-4 visible-xs visible-sm visible-md">GENDER</label>
          <div class="col-sm-8">
              <div>
                  <select name="life_gender" id="row-life_gender" class="form-control borderzero">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
              </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="form-group">
              <label for="row-code" class="col-sm-4 visible-xs visible-sm visible-md">DATE OF BIRTH</label>
          <div class="col-sm-8">
              <div>
                  <div class="input-group date life_dob">
                    <input class="form-control" name="life_dob" id="row-life_dob" name="life_dob" type="text" placeholder="mm/dd/yyyy">
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
              </div>
          </div>
        </div>
      </div>

      <div class="form-residency_others col-lg-12 hide hidden-sm hidden-xs hidden-md" style="height: 54px;">
        <label>&nbsp;</label>
      </div>

      <div class="col-lg-12 hidden-sm hidden-xs hidden-md" style="height: 137px;">
        <label>&nbsp;</label>
      </div>

      <div class="col-lg-12 error-owner_id hidden-sm hidden-xs hidden-md">
        <sup style="color: #ffffff;" class="sup-errors"></sup>
      </div>

      <div class="col-lg-12 error-nationality_id hidden-sm hidden-xs hidden-md">
        <sup style="color: #ffffff;" class="sup-errors"></sup>
      </div>

      <div class="col-lg-12 error-residency_status   hidden-sm hidden-xs hidden-md">
        <sup style="color: #ffffff;" class="sup-errors"></sup>
      </div>


      <div class="col-lg-12">
        <div class="form-group">
          <label for="row-code" class="col-sm-4 visible-xs visible-sm visible-md">AGE NEXT BIRTHDAY</label>
            <div class="col-sm-8">
              <div>
                  <input type="text" name="life_age" class="form-control borderzero" id="row-life_age" maxlength="30" placeholder="">
              </div>
            </div>          
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="row">
      <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-12">
        <div class="form-group">
            <label for="row-address" class="col-sm-4">ADDRESS</label>
        <div class="col-sm-8 error-address ">
            <input type="text" name="address" class="form-control borderzero" id="row-address" maxlength="100">
            <sup class="sup-errors"></sup>
        </div>
      </div>
      </div>
      <div class="col-lg-12">
        <div class="form-group">
         <label for="row-email" class="col-sm-4">EMAIL</label>
          <div class="col-sm-8 error-production_email ">
              <input type="email" name="production_email" class="form-control borderzero" id="row-email" maxlength="100">
              <sup class="sup-errors"></sup>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="col-lg-6">
      <div class="row">

        <div class="col-lg-12">
          <div class="form-group">
            <label for="row-postal_code" class="col-sm-4">POSTAL CODE</label>
            <div class="col-sm-8 error-postal_code ">
                <input type="text" name="postal_code" class="form-control borderzero" id="row-postal_code" maxlength="30">
                <sup class="sup-errors"></sup>
            </div>
        </div>
        </div>
        <div class="col-lg-12">
          <div class="row">
          <label for="row-code" class="col-sm-4"><button type="button" value="" class="hide-view btn-add-telephone "> <i class="fa fa-plus Add"> </i></button> TELEPHONE</label>
            <div class="table-responsive">
            <table id="telephone-table" class="table">
                <tbody id="telephone-table-body">
                    <tr>
                        <td class="col-xs-12">
                            <div class="col-xs-12 input-group">
                                <input type="text" name="contact[]" class="form-control borderzero  requiredcol-lg-12" id="row-contact" maxlength="30">
                            </div>
                        </td>
                        <td class="hide-sta text-right">
                            <input type="hidden" name="contact_id[]" value="">
                            <button type="button" value="" class="hide-view btn-del-telephone" title="Remove"> <i class="fa fa-close"> </i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
      </div>
      </div>
    </div>
    <div id="clone" class="col-lg-12">
      <div class="row">
        <div class="col-lg-4">
          <label for="row-code">PRODUCT LIST</label>
          <div class="form-group">
          <div class="col-sm-12">
            <div class="input-group col-xs-12 error-provider_list">
            <select  name="provider_list" id="row-provider_list" class="form-control borderzero">
                <option value="">Select Provider</option>
                @foreach($providers as $row)
                 <option value="{{ $row->id }}">{{$row->name}}</option>
                @endforeach
            </select>
            <sup class=" sup-errors"></sup>

            </div>
        </div>
        </div>
        </div>
        <div class="col-lg-4">
          <label for="row-code">TYPE OF PLAN</label>
          <div class="form-group">
          <div class="col-xs-12 ">
            <div class="input-group col-sm-12 error-main_product_id">
            <select  name="main_product_id" id="row-main_product_id" class="form-control borderzero"> 
              <option class="hide" value="">Select Plan</option>
            </select>
            <sup class="sup-errors"></sup>
            <input type="hidden" name="conversion_rate" id="row-conversion_rate">
            <input type="hidden" name="conversion_year" id="row-conversion_year">
            </div>
        </div>
        </div>
        </div>
        <div class="col-lg-4">
          <label for="row-code">SUM ASSURED</label>
          <div class="form-group">
        <div class="col-sm-12 error-main_sum_assured">
            <input type="text" name="main_sum_assured" class="form-control borderzero" id="row-main_sum_assured" maxlength="30">
            <sup class=" sup-errors"></sup>
        </div>
        </div>
        </div>
      </div>
    </div>
    <div id="clone" class="col-lg-12">
      <div id="setClone" class="row">
      </div>
    </div>
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-12">
        <div class="form-group">
          <div class="row col-lg-12">
            <label for="row-code" class="col-sm-3 col-lg-2"><button type="button" value="" class="clone hide-view btn-add-plan"> <i class="fa fa-plus Add"> </i></button> RIDERS</label>
            <label for="row-code" class="col-sm-9 col-lg-10 hidden-xs"></label>
          </div>
            <div class="col-lg-12">
            <div class="table-responsive">
            <table id="plan-table" class="table">
                <tbody id="plan-table-body">
                    <tr>
                      <!-- <td id="provider_list_rider_error" class="col-xs-4 error-provider_list_rider_1">
                        <div class="input-group col-xs-12">
                          <select  name="provider_list_rider[]" id="row-provider_list_rider" class="form-control borderzero provider_list_rider">
                            <option value="">Select Provider</option>
                            @foreach($providers as $row)
                             <option value="{{ $row->id }}">{{$row->name}}</option>
                            @endforeach
                          </select>
                        <sup class="sup-errors"></sup>
                        </div>
                      </td> -->
                      <td id="rider_product_id_error" class="col-xs-6 error-rider_product_id_1">
                        <div class="input-group col-xs-12">
                          <select  name="rider_product_id[]" id="row-rider_product_id" class="form-control borderzero rider_product_id">
                            <option value="">Select Plan</option>
                          </select>
                        </div>
                      <sup class="sup-errors"></sup>
                      </td>
                      <td  id="rider_sum_assured_error" class="col-xs-6 error-rider_sum_assured_1">
                        <div class="input-group col-xs-9">
                          <input type="text" name="rider_sum_assured[]" class="form-control borderzero" id="row-rider_sum_assured" maxlength="30" placeholder="Sum Assured">
                        </div>
                      <sup class="sup-errors"></sup>
                      </td>
                      <td class="hide-sta leftalign">
                        <input type="hidden" name="riders_id[]" value="">
                        <input type="hidden" name="riders_sub_id[]" value="1">
                        <button type="button" value="" class="hide-view btn-del-plan" title="Remove"> <i class="fa fa-close"> </i></button>
                      </td>
                    </tr>
                </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    <hr class="hr">
    <div class="col-lg-6">
        <div class="form-group">
            <label for="row-policy_owner" class="col-sm-2">CURRENCY PREMIUM AMOUNT</label>
        <div class="col-sm-10">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error-currency_value">
                <div class="input-group">
              <input type="number" name="currency_value"  step="0.01" class="form-control borderzero" id="row-currency_value" maxlength="30" placeholder="" oninput="">
                <span class="input-group-addon">
                <input type="text" id="row-currency_rate">
                  <select id="row-currency" name="currency" class="select" onchange="">
                    <option class="hide" value="">Select:</option>
                    @foreach($currencies as $row)
                      <option value="{{ $row->rate." ".$row->code}}"><span id="currencyName" value="{{$row->code}}">{{$row->code}}</span></option>
                    @endforeach
                  </select>
                </span>
              </div>
              <sup class=" sup-errors"></sup>
            </div>
            <div class="hide col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div id="inputpad" class="input-group">
                <input type="text" id="row-conv_total" name="conv_total" class="form-control">
                <span class="input-group-addon">
                  <span class="input-group" id="total_currency">SGD</span>
                </span>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
        <div class="col-lg-6">
        <div class="form-group">
            <label for="row-policy_owner" class="col-sm-2">TOTAL PREMIUM AMOUNT</label>
        <div class="col-sm-10">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
              <input type="number"  step="0.01"  name="currency_value2" class="form-control borderzero" id="row-currency_value2" maxlength="30" readonly>
                <span class="input-group-addon">
                <input type="text" id="row-currency_rate2">
                  <select id="row-currency2" name="currency2" class="select" onchange="">
                    <option class="hide" value="">Select:</option>
                    @foreach($currencies as $row)
                       <option value="{{ $row->rate." ".$row->code}}"><span id="currencyName" value="{{$row->code}}">{{$row->code}}</span></option>
                    @endforeach
                  </select>
                </span>
              </div>
            </div>
            <div class="hide col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div id="inputpad" class="input-group">
                  <input type="text" id="row-life_total_amount" name="life_total_amount" class="form-control" value="" readonly="readonly">
                  <span class="input-group-addon">
                    <span class="input-group" id="total_currency3">SGD</span>
                  </span>
                </div>
            </div>
          </div>
        </div>
        </div>
        </div>
        <hr>
        <div class="col-lg-12">
      <div class="row">
      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <label for="row-postal_code" class="col-sm-4">POLICY TERM</label>
              <div class="col-sm-8 error-policy_term ">
                  <input type="text" name="policy_term" class="form-control borderzero" id="row-policy_term" maxlength="30">
                  <sup class="sup-errors"></sup>
              </div>
            </div>
          </div>
      <div class="col-lg-12">
      <div class="form-group">
       <label for="row-email" class="col-sm-4">PAYMENT FREQUENCY</label>
        <div class="col-sm-8 error-payment_frequency ">
            <select name="payment_frequency" id="row-payment_frequency" class="form-control borderzero">
                <option value="">Select Frequency</option>
                <option value="Single">Single</option>
                <option value="Yearly">Yearly</option>
                <option value="Half-Yearly">Half-Yearly</option>
                <option value="Quarterly">Quarterly</option>
                <option value="Monthly">Monthly</option>
                <option value="Monthly/Single">Monthly/Single</option>
                <option value="Yearly/Single">Yearly/Single</option>
            </select>
            <sup class="sup-errors"></sup>
        </div>
      </div>
    </div>
    </div>
    </div>
    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-12">
        <div class="form-group">
            <label for="row-address" class="col-sm-4">APE</label>
        <div class="col-sm-8">
            <input type="number" name="ape" class="form-control borderzero" id="row-ape" maxlength="30" readonly>
        </div>
      </div>
      </div>

      </div>
    </div>
    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
              <label for="row-postal_code" class="col-sm-4">MODE OF PAYMENT</label>
              <div class="col-sm-8 error-mode_of_payment">
                  <select name="mode_of_payment" id="row-mode_of_payment" class="form-control borderzero">
                <option value="">Select Payment</option>
                <option value="Cheque">Cheque</option>
                <option value="Credit Card">Credit Card</option>
                <option value="Direct Transfer">Direct Transfer</option>
                <option value="CPF">CPF</option>
                <option value="SRS">SRS</option>
                <option value="GIRO Payment">GIRO Payment</option>
                <option value="Others">Others</option>
            </select>
            <sup class=" sup-errors"></sup>
            </div>
          </div>
        </div>

        <div id="form-mode_others" class="hide col-lg-12">
          <label for="row-mode_others" class="col-sm-4">&nbsp;</label>
          <div class="col-sm-8 error-mode_others">
            <input type="text" class="form-control borderzero" name="mode_others" id="row-mod_others">
            <sup class="sup-errors"></sup>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
    <div class="col-lg-12">
        <div class="form-group">
            <label for="row-source" class="col-sm-4 col-md-2 col-lg-2">SOURCE</label>
        <div class="col-sm-8 col-md-10 col-lg-10 error-source ">
            <select name="source" id="row-source" class="form-control borderzero">
                <option value="">Select Source</option>
                <!-- <option value="Aviva Lead">Aviva Lead</option> -->
                <option value="Call Centre">Call Centre</option>
                <option value="Canvassing">Canvassing</option>
                <option value="Door Knocking">Door Knocking</option>
                <option value="Existing Client">Existing Client</option>
                <option value="Fighter">Fighter</option>
                <!-- <option value="MyCare Planning">MyCare Planning</option> -->
                <option value="Others">Others</option>
                <!-- <option value="Pod Duty">Pod Duty</option> -->
                <option value="Referrals">Referrals</option>
                <option value="Road Show">Road Show</option>
                <option value="Warm">Warm</option>
            </select>
            <sup class="sup-errors"></sup>
        </div>
        </div>
        </div>
        <div class="col-lg-12">
        <div class="form-group">
            <label for="row-source" class="col-sm-2">PDPA CONSENT</label>
        <div class="col-sm-10">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <label>Marketing:
            <br>
            <input type="radio" name="marketing" id="row-consent_marketing_val_one" value="yes">Yes
            <br>
            <input type="radio" name="marketing" id="row-consent_marketing_val_two" value="no">No
            </label>
            <input type="hidden" name="consent_marketing" id="row-consent_marketing" value="yes">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <label>Servicing:
            <br>
            <input type="radio" name="servicing" id="row-consent_servicing_val_one" value="yes">Yes
            <br>
            <input type="radio" name="servicing" id="row-consent_servicing_val_two" value="no">No
            </label>
            <input type="hidden" name="consent_servicing" id="row-consent_servicing" value="yes">
        </div>
        </div>
        </div>
        <div class="form-file">
        <div class="form-group upload_file hide">
          <label for="upload" class="col-sm-3">PFR</label>
          <div class="col-sm-9 error-upload hide-view">
            <input type="hidden" name="upload_remove" id="row-upload_remove">
            <input type="file" class="form-control borderzero file file_case" name="upload" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <sup class="sup-errors"></sup>
            <button type="button" class="btn btn-default btn-cancel-upload borderzero hide"><i class="fa fa-times"></i> Cancel</button>
          </div>
          <div class="col-sm-9 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file hide">
          <label for="upload" class="col-sm-3">PFR</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="upload_title" readonly="readonly" id="row-upload_title">
            <a href="" id="row-download" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Download</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload borderzero"><i class="fa fa-trash"></i> Remove</button>
          </div>
        </div>

        <div class="form-group upload_file_2 hide">
          <label for="upload" class="col-sm-3">BI</label>
          <div class="col-sm-9 error-upload_2 hide-view">
            <input type="hidden" name="upload_remove_2" id="row-upload_remove_2">
            <input type="file" class="form-control borderzero file file_case" name="upload_2" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <button type="button" class="btn btn-default btn-cancel-upload_2 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
            <sup class="sup-errors"></sup>
          </div>
          <div class="col-sm-9 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_2 hide">
          <label for="upload" class="col-sm-3">BI</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" readonly="readonly" id="row-upload_2_title">
            <a href="" id="row-download_2" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Download</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_2 borderzero"><i class="fa fa-trash"></i> Remove</button>
          </div>
        </div>

        <div class="form-group upload_file_3 hide">
          <label for="upload" class="col-sm-3">APP FORM</label>
          <div class="col-sm-9 error-upload_3 hide-view">
            <input type="hidden" name="upload_remove_3" id="row-upload_remove_3">
            <input type="file" class="form-control borderzero file file_case" name="upload_3" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <sup class="sup-errors"></sup>
            <button type="button" class="btn btn-default btn-cancel-upload_3 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
          </div>
          <div class="col-sm-9 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_3 hide">
          <label for="upload" class="col-sm-3">APP FORM</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="upload_3_title" readonly="readonly" id="row-upload_3_title">
            <a href="" id="row-download_3" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Download</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_3 borderzero"><i class="fa fa-trash"></i> Remove</button>
          </div>
        </div>


        <div class="form-group upload_file_4 hide">
          <label for="upload" class="col-sm-3">SUPPORTING DOC</label>
          <div class="col-sm-9 error-upload_4 hide-view">
            <input type="hidden" name="upload_remove_4" id="row-upload_remove_4">
            <input type="file" class="form-control borderzero file file_case" name="upload_4" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <button type="button" class="btn btn-default btn-cancel-upload_4 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
            <sup class="sup-errors"></sup>
          </div>
          <div class="col-sm-9 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_4 hide">
          <label for="upload" class="col-sm-3">SUPPORTING DOC</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" readonly="readonly" id="row-upload_4_title">
            <a href="" id="row-download_4" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Download</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_4 borderzero"><i class="fa fa-trash"></i> Remove</button>
          </div>
        </div>

        <div class="form-group upload_file_5 hide">
          <label for="upload" class="col-sm-3">OTHERS</label>
          <div class="col-sm-9 error-upload_5 hide-view">
            <input type="hidden" name="upload_remove_5" id="row-upload_remove_5">
            <input type="file" class="form-control borderzero file file_case" name="upload_5" multiple data-show-upload="false" placeholder="" data-show-preview="false">
            <button type="button" class="btn btn-default btn-cancel-upload_5 borderzero hide"><i class="fa fa-times"></i> Cancel</button>
            <sup class="sup-errors"></sup>
          </div>
          <div class="col-sm-9 show-view">
            <label>N/A</label>
          </div>
        </div>
        <div class="form-group view_file_5 hide">
          <label for="upload" class="col-sm-3">OTHERS</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" readonly="readonly" id="row-upload_5_title">
            <a href="" id="row-download_5" class="btn btn-default borderzero btn-close"><i class="fa fa-file"></i> Download</a>
            <button type="button" class="btn btn-default hide-view btn-remove-upload_5 borderzero"><i class="fa fa-trash"></i> Remove</button>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
    </div>
    </div>
    <div class="clearfix"></div>
    <div class="modal-footer borderzero">
      <input type="hidden" name="id" id="row-id">
          <button type="button" class="btn btn-default borderzero btn-close" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-default btn-submit hide-view borderzero">Submit</button>
    </div>
  </div>
    {!! Form::close() !!}
  </div>
</div>
</div>
</div>

<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>



  <!-- Team Modal -->
  <div class="modal fade" id="modal-assessment" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-assessment" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/assessment', 'role' => 'form', 'id' => 'modal-save_assessment', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="assessment-title"><i class="fa fa-cog"></i> Assessment</h4>
           </div>
           <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="assessment-id" value="">
                <input type="hidden" name="assessment_outcome" id="assessment-outcome" value="">
                <button type="button" class="btn btn-warning hide-view borderzero btn-false_positive"> False Positive</button>
                <button type="button" class="btn btn-success hide-view borderzero btn-positive_hit"> Positive Hit</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>

  <!-- Team Modal -->
  <div class="modal fade" id="modal-review" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-review" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/review', 'role' => 'form', 'id' => 'modal-save_review', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="review-title"><i class="fa fa-cog"></i> Review</h4>
           </div>
           <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="review-id" value="">
                <input type="hidden" name="review_outcome" id="review-outcome" value="">
                <button type="button" class="btn btn-warning hide-view borderzero btn-false_positive"> False Positive</button>
                <button type="button" class="btn btn-success hide-view borderzero btn-positive_hit"> Positive Hit</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
          {!! Form::close() !!}
        </div> 
    </div>
    </div>



  <!-- Team Modal -->
  <div class="modal fade" id="modal-submitted" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-submitted" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/submitted', 'role' => 'form', 'id' => 'modal-save_submitted', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="submitted-title"><i class="fa fa-cog"></i> Submit to HO</h4>
           </div>
           <div class="modal-body">
            <div id="submitted-notice"></div>
              Submit To HO?
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
              <input type="hidden" name="id" id="submitted-id" value="">
              <input type="hidden" name="submitted_outcome" id="submitted-outcome" value="">
              <button type="button" class="btn btn-success hide-view borderzero btn-yes_submit"> Yes</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"> No</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>


  <!-- Team Modal -->
  <div class="modal fade" id="modal-duplicate" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
      <div id="load-duplicate" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
          {!! Form::open(array('url' => 'policy/production/duplicate', 'role' => 'form', 'id' => 'modal-save_duplicate', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-info">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="duplicate-title"><i class="fa fa-cog"></i> Duplicate Case</h4>
           </div>
           <div class="modal-body">
            <div id="duplicate-notice"></div>
              Duplicate case?
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
              <input type="hidden" name="id" id="duplicate-id" value="">
              <button type="submit" class="btn btn-success hide-view borderzero"> Yes</button>
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"> No</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>