@extends('layouts.master')

@section('scripts')
<script type="text/javascript">
$_token = '{{ csrf_token() }}';

  function refresh() {

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();

      $.post("{{ url('policy/production/case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id +'">' +
                '<td>' + row.firstname + ' ' + row.lastname +'</td>' +
                '<td>' + row.annual_income_range + '</td>' +
                '<td>' + row.life_firstname + ' ' + row.life_lastname +'</td>' + 
                // '<td>' + row.code + '</td>' +
                '<td>' + row.policy_term + '</td>' + 
                '<td>' + row.mode_of_payment + '</td>' + 
                '<td>' + row.designation + '</td>' + 
                '<td class="rightalign">' +
                  '<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-decept"><i class="fa fa-check"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                  '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">' +
                '</td>' +
              '</tr>';
        });

        table.html(body);
        $('#row-pages').html(response.pages);

      loading.addClass("hide");
      }, 'json');
  }

  function search() {

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");
      $("#row-page").val(1);

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();

      $.post("{{ url('policy/production/case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id +'">' +
                '<td>' + row.firstname + ' ' + row.lastname +'</td>' +
                '<td>' + row.annual_income_range + '</td>' +
                '<td>' + row.life_firstname + ' ' + row.life_lastname +'</td>' + 
                // '<td>' + row.code + '</td>' +
                '<td>' + row.policy_term + '</td>' + 
                '<td>' + row.mode_of_payment + '</td>' + 
                '<td>' + row.designation + '</td>' + 
                '<td class="rightalign">' +
                  '<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-decept"><i class="fa fa-check"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                  '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">' +
                '</td>' +
              '</tr>';
        });

        table.html(body);
        $('#row-pages').html(response.pages);

      loading.addClass("hide");
      }, 'json');
  }

  $(document).ready(function() {

      $(".btn-refresh").click(function() {
        refresh();
      });

      //add user
      $(".btn-add").click(function() {

        // reset all form fields
        $("#modal-production").find('form').trigger("reset");
        $("#row-id").val("");

        $("#modal-production").find('.hide-view').removeClass('hide');
        $("#modal-production").find('.show-view').addClass('hide');
        $("#modal-production").find('.preview').addClass('hide');
        $("#modal-production").find('input').removeAttr('readonly', 'readonly');

        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
        $("#modal-production").modal('show');

      });


      $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          // reset all form fields
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-production").find('form').trigger("reset");
          $("#row-id").val("");

            $.post("{{ url('policy/production/view') }}", { id: id, _token: $_token }, function(response) {
                if(!response.error) {
                // set form title
                console.log(response);
                
                    $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View Production</strong>');
                    //$(".pic").html('<img src="uploads/theheck.jpg">');

                    // output form data
                    $.each(response, function(index, value) {
                        var field = $("#row-" + index);

                        if (index == 'user_id') {
                          field = $("#row-user_sales_id");
                        }

                        // field exists, therefore populate
                        if(field.length > 0) {
                          if(field.parent().hasClass("date")) {
                            field.val(moment(value).format('MM/DD/YYYY'));
                          } else {
                            field.val(value);
                          }
                        }
                    });

                    $("#modal-production").find('.hide-view').addClass('hide');
                    $("#modal-production").find('.preview').removeClass('hide');
                    $("#modal-production").find('.change').addClass('hide');
                    $("#modal-production").find('.show-view').removeClass('hide');
                    $("#modal-production").find('input').attr('readonly', 'readonly');
                    // show form
                    $("#modal-production").modal('show');
                } else {
                    status(false, response.error, 'alert-danger');
                }

                btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
            }, 'json');
      });

      //edit cases
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        // reset all form fields
        $("#modal-production").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

              $.post("{{ url('policy/production/view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                  console.log(response);
                  
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Production Case</strong>');
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {
                        var field = $("#row-" + index);

                        if (index == 'user_id') {
                          field = $("#row-user_sales_id");
                        }

                        // field exists, therefore populate
                        if(field.length > 0) {
                          if(field.parent().hasClass("date")) {
                            field.val(moment(value).format('MM/DD/YYYY'));
                          } else {
                            field.val(value);
                          }
                        }
                      });

                      $("#modal-production").find('.hide-view').removeClass('hide');
                      $("#modal-production").find('.show-view').addClass('hide');
                      $("#modal-production").find('input').removeAttr('readonly');
                      // show form
                      $("#modal-production").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

       $(function () {
        $('.state_from_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.state_to_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.transaction_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.incept_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.inst_from_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.policy_exp_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.comm_run_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.issue_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected provider/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
        console.log('multiple');

        
        if (ids.length == 0) {
          status("Error", "Select providers first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('provider/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('policy/production/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });

      // decept case
      $("#page-content-wrapper").on("click", ".btn-decept", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Inception', 'Are you sure you want to decept <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/decept') }}", id);
      });

      // disable case
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/disable') }}", id);
      });

      // enable case
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/enable') }}", id);
      });

      // show modal for multiple inception
      $("#page-content-wrapper").on("click", ".btn-decept-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Inception");
        $(".modal-multiple-body").html("Are you sure you want to re-submit the selected case/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });
        $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
        console.log('multiple');

        
        if (ids.length == 0) {
        } else {

          if ($("#row-multiple-hidden").val() == 0) {
            $.post("{{ url('policy/production/case-submission-advisor/decept-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });


    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {
      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

      $("#row-page").val($(this).html());
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");

      $.post("{{ url('policy/production/case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id +'">' +
                '<td>' + row.firstname + ' ' + row.lastname +'</td>' +
                '<td>' + row.annual_income_range + '</td>' +
                '<td>' + row.life_firstname + ' ' + row.life_lastname +'</td>' + 
                // '<td>' + row.code + '</td>' +
                '<td>' + row.policy_term + '</td>' + 
                '<td>' + row.mode_of_payment + '</td>' + 
                '<td>' + row.designation + '</td>' + 
                '<td class="rightalign">' +
                  '<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-decept"><i class="fa fa-check"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                  '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">' +
                '</td>' +
              '</tr>';
        });

        table.html(body);
        $('#row-pages').html(response.pages);

        loading.addClass("hide");
      }, 'json');
    });


    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ url('policy/production/case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
          // clear
          table.html("");
          
          var body = "";
          // clear datatable cache
          $('#my-table').dataTable().fnClearTable();

          $.each(response.rows.data, function(index, row) {
              body += '<tr data-id="' + row.id +'">' +
                  '<td>' + row.firstname + ' ' + row.lastname +'</td>' +
                  '<td>' + row.annual_income_range + '</td>' +
                  '<td>' + row.life_firstname + ' ' + row.life_lastname +'</td>' + 
                  // '<td>' + row.code + '</td>' +
                  '<td>' + row.policy_term + '</td>' + 
                  '<td>' + row.mode_of_payment + '</td>' + 
                  '<td>' + row.designation + '</td>' + 
                  '<td class="rightalign">' +
                    '<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-decept"><i class="fa fa-check"></i></button>' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                    '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">' +
                  '</td>' +
                '</tr>';
          });

          table.html(body);
          $('#row-pages').html(response.pages);

          loading.addClass("hide");
        }, 'json');

      }
    });

  </script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">POLICY MANAGEMENT</h3>
        <h5 class="bread-crumb">PRODUCTION > CASE SUBMISSION </h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
          <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-decept-select"><i class="fa fa-check"></i> Re-submit</button>     
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
<form class="form-horizontal" >     
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <input type="text" class="form-control borderzero " placeholder="Search for..." onkeyup="search()" id="row-search">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
      <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
          <div class="table-responsive block-content tbblock col-xs-12">
            <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
            <table class="table table-striped table-pagination" id="tblData">
              <thead class="tbheader">
                  <tr>
                    <!-- <th nowrap><i class="fa fa-sort"></i> AGENT NAME</th> -->
                    <th class="th-sort" data-sort="firstname"><i></i> NAME</th>
                    <th class="th-sort" data-sort="annual_income_range"><i></i> ANNUAL INCOME</th>
                    <th class="th-sort" data-sort="life_firstname"><i></i> NAME (LIFE)</th>
                    <!-- <th class="th-sort" data-sort="code"><i></i> TYPE OF PLAN</th> -->
                    <th class="th-sort" data-sort="policy_term"><i></i> POLICY TERM</th>
                    <th class="th-sort" data-sort="mode_of_payment"><i></i> MODE OF PAYMENT</th>
                    <th class="th-sort" data-sort="designation"><i></i> DESIGNATION</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody id="rows">
                  @foreach($rows as $row)
                  <tr data-id="{{ $row->id }}">
                    <!-- <td>{{ $row->name }}</td> -->
                    <td>{{ $row->firstname }} {{ $row->lastname }}</td>
                    <td>{{ $row->annual_income_range }}</td>
                    <td>{{ $row->life_firstname }} {{ $row->life_lastname }}</td>
                    <!-- <td>{{ $row->code }}</td> -->
                    <td>{{ $row->policy_term }}</td>
                    <td>{{ $row->mode_of_payment }}</td>
                    <td>{{ $row->designation }}</td>
                    <td class="rightalign">
                        <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-eye"></i></button>
                        <button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>
                        <button type="button" class="btn btn-xs btn-table btn-decept"><i class="fa fa-check"></i></button> 
                        <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}"><i class="fa fa-adjust"></i></button>
                        <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>           
      </div>
           <div id="row-pages" class="col-lg-6 text-center borderzero leftalign">{!! $pages !!}</div>
              <div class="col-lg-6 content-table" style="text-align: -webkit-right;">
              <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="50">100</option>
              </select>
            </div>
          <div class="text-center">
            <input type="hidden" id="row-page" value="1">
            <input type="hidden" id="row-sort" value="">
            <input type="hidden" id="row-order" value="">
          </div>
    </div>
    <!-- /#page-content-wrapper -->

@include('policy-management.production.case-inception.form')
@stop