@extends('layouts.master')

@section('scripts')
<script type="text/javascript">
$_token = '{{ csrf_token() }}';
var token = $("input[name='_token']").val();
$plan_tbody = $("#plan-table-body").html();
$tel_tbody = $("#telephone-table-body").html();

$(function () {
  $('.submission_date').datetimepicker({
      locale: 'en',
      format: 'DD/MM/YYYY'
  });
});


  // function formatNumber(num) {
  //   return parseFloat(num).toFixed(2).replace(/(.)(?=(\d{3})+$)/g,'$1,');
  // }

function formatNumber(number) {

  var decimalSeparator = ".";
  var thousandSeparator = ",";

  // make sure we have a string
  var result = String(parseFloat(number).toFixed(2));

  // split the number in the integer and decimals, if any
  var parts = result.split(decimalSeparator);

  // if we don't have decimals, add .00
  if (!parts[1]) {
    parts[1] = "00";
  }

  // reverse the string (1719 becomes 9171)
  result = parts[0].split("").reverse().join("");

  // add thousand separator each 3 characters, except at the end of the string
  result = result.replace(/(\d{3}(?!$))/g, "$1" + thousandSeparator);

  // reverse back the integer and replace the original integer
  parts[0] = result.split("").reverse().join("");

  // recombine integer with decimals
  return parts.join(decimalSeparator);
}

  function refresh() {

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      $.post("{{ url('policy/production/gi-case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        table.html(response.rows);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }

  function search() {

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");
      $("#row-page").val(1);

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      
      $.post("{{ url('policy/production/gi-case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        table.html(response.rows);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }


    $(function(){

      $('.Delete').click(function() {
        $('.Option:last').remove();
      });

      $('.Add').click(function() {
        $('.Option:last').after($('.Option:last').clone());
      });

    });


    $(".btn-add-gross").click(function() {
        var table = $("#gross-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();
        var length = parseInt(table.find("input[name='gross_year_id[]']:last").val());

        newrow.appendTo(table);
        table.find("input[name='gross_value[]']:last").focus();
        table.find("tr:last #gross-label").html("YEAR " + (length+1));

        var last_tr = table.find("tr:last");
        last_tr.find('select').val("");
        last_tr.find('input').val("");
        last_tr.find("#gross-error").removeAttr('class').attr('class', 'col-xs-6 error-gross_value_' + (length+1));
        last_tr.find("input[name='gross_year_id[]']:last").val(length+1);
    });


      // add riders
    $(".btn-add-plan").click(function() {
        var table = $("#plan-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();
        var length = parseInt(table.find("input[name='riders_sub_id[]']:last").val());

        newrow.appendTo(table);

        var last_tr = table.find("tr:last");
        last_tr.find('select').val("");
        last_tr.find('input').val("");
        last_tr.find("#rider_product_id_error").removeAttr('class').attr('class', 'col-xs-6 error-rider_product_id_' + (length+1));
        last_tr.find("#rider_sum_assured_error").removeAttr('class').attr('class', 'col-xs-6 error-rider_sum_assured_' + (length+1));
        last_tr.find("input[name='riders_sub_id[]']:last").val(length+1);
    });

    // delete riders
    $(document).on('click', '.btn-del-plan', function() {
        var table = $("#plan-table tbody");
        var row = $(this).parent().parent();
        var rowcount = table.find("tr").length;

        if(rowcount > 1) {
            row.remove();
            rowcount--;
        } else {
            $("#plan-table tbody").html($plan_tbody);
        }
    });

       // add telephone
    $(".btn-add-telephone").click(function() {
        var table = $("#telephone-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();

        newrow.appendTo(table);

        table.find("select[name='contact[]']:last").focus();
        var last_tr = table.find("tr:last");
        last_tr.find('input').val("");
    });


    // delete telephone
    $(document).on('click', '.btn-del-telephone', function() {
        var table = $("#telephone-table tbody");
        var row = $(this).parent().parent();
        var rowcount = table.find("tr").length;

        if(rowcount > 1) {
            row.remove();
            rowcount--;
        } else {
            row.find(":input").val("");
        }
    });


  $(document).ready(function() {
    new Clipboard('.btn-copy');

    $('.linkster').tooltipster({
      theme: 'tooltipster-noir',
      contentAsHTML: true,
      trigger: 'click',
      interactive: true
    });

    $(".multi_select").select2();

    $('#row-upload_change').on('change', function(event) {
      if ($(this).val()) {
        var title = $(".error-upload .file-caption-name").attr('title');
        var title_all = title.split('.');
        var title_length = title_all.length - 1;
        title_last = title_all[title_length];
        title = title.replace('.' + title_last, '');
        $("#row-upload_name").val(title);
        $("#row-upload_name").focus();
      } else {
        $("#row-upload_name").val('');
      }
    });

    $('#row-upload_2_change').on('change', function(event) {
      if ($(this).val()) {
        var title = $(".error-upload_2 .file-caption-name").attr('title');
        var title_all = title.split('.');
        var title_length = title_all.length - 1;
        title_last = title_all[title_length];
        title = title.replace('.' + title_last, '');
        $("#row-upload_name_2").val(title);
        $("#row-upload_name_2").focus();
      } else {
        $("#row-upload_name_2").val('');
      }
    });

    $('#row-upload_3_change').on('change', function(event) {
      if ($(this).val()) {
        var title = $(".error-upload_3 .file-caption-name").attr('title');
        var title_all = title.split('.');
        var title_length = title_all.length - 1;
        title_last = title_all[title_length];
        title = title.replace('.' + title_last, '');
        $("#row-upload_name_3").val(title);
        $("#row-upload_name_3").focus();
      } else {
        $("#row-upload_name_3").val('');
      }
    });
    
    $('#row-upload_4_change').on('change', function(event) {
      if ($(this).val()) {
        var title = $(".error-upload_4 .file-caption-name").attr('title');
        var title_all = title.split('.');
        var title_length = title_all.length - 1;
        title_last = title_all[title_length];
        title = title.replace('.' + title_last, '');
        $("#row-upload_name_4").val(title);
        $("#row-upload_name_4").focus();
      } else {
        $("#row-upload_name_4").val('');
      }
    });

    $('#row-upload_5_change').on('change', function(event) {
      if ($(this).val()) {
        var title = $(".error-upload_5 .file-caption-name").attr('title');
        var title_all = title.split('.');
        var title_length = title_all.length - 1;
        title_last = title_all[title_length];
        title = title.replace('.' + title_last, '');
        $("#row-upload_name_5").val(title);
        $("#row-upload_name_5").focus();
      } else {
        $("#row-upload_name_5").val('');
      }
    });

    $('#row-upload_change').on('fileclear', function(event) {
      $("#row-upload_name").val('');
    });

    $('#row-upload_2_change').on('fileclear', function(event) {
      $("#row-upload_name_2").val('');
    });

    $('#row-upload_3_change').on('fileclear', function(event) {
      $("#row-upload_name_3").val('');
    });

    $('#row-upload_4_change').on('fileclear', function(event) {
      $("#row-upload_name_4").val('');
    });

    $('#row-upload_5_change').on('fileclear', function(event) {
      $("#row-upload_name_5").val('');
    });

    $('#row-backdate').change(function() {
      if($(this).is(":checked")) {
        $("#row-backdate_value").attr("value",1);
        $("#row-owner_age").removeAttr("disabled","disabled");
      } else {
        $("#row-backdate_value").attr("value",0);
        $("#row-owner_age").attr("disabled","disabled");
      }
    });

    $('#row-life_backdate').change(function() {
      if($(this).is(":checked")) {
        $("#row-life_backdate_value").attr("value",1);
        $("#row-life_age").removeAttr("disabled","disabled");
      } else {
        $("#row-life_backdate_value").attr("value",0);
        $("#row-life_age").attr("disabled","disabled");
      }
    });

    $('#row-same').change(function() {
      if($(this).is(":checked")) {
        $("#row-life_firstname").val($("#row-firstname").val());
        $("#row-life_lastname").val($("#row-lastname").val());
        $("#row-life_id").val($("#row-owner_id").val());
        $("#row-life_occupation").val($("#row-owner_occupation").val());
        $("#row-life_gender").val($("#row-owner_gender").val());
        $("#row-life_dob").val($("#row-owner_dob").val());
        $("#row-life_bday").val($("#row-owner_bday").val());
        $("#row-life_backdate").prop('disabled', true);

        if ($("#row-backdate").is(":checked")) {
          $(".form-life_backdate").find("input[type='checkbox']").prop('checked', true);
          $("#row-life_age").val($("#row-owner_age").val());
        } else {
          $(".form-life_backdate").find("input[type='checkbox']").prop('checked', false);
          $("#row-life_age").val("");
        }

        $("#row-life_age").attr('readOnly', 'readonly');
        $("#row-life_firstname").attr('readOnly', 'readonly');
        $("#row-life_lastname").attr('readOnly', 'readonly');
        $("#row-life_id").attr('readOnly', 'readonly');
        $("#row-life_occupation").attr('readOnly', 'readonly');
        $("#row-life_gender").attr('disabled', 'disabled');
        $("#row-life_dob").attr('readOnly', 'readonly');
        $("#row-life_bday").attr('readOnly', 'readonly');
      } else {
        $("#row-life_firstname").val("");
        $("#row-life_lastname").val("");
        $("#row-life_id").val("");
        $("#row-life_occupation").val("");
        $("#row-life_gender").val("");
        $("#row-life_dob").val("");
        $("#row-life_backdate").prop('disabled', false);
        $("#row-life_bday").val("");
        $("#row-life_age").val("");


        $(".form-life_backdate").find("input[type='checkbox']").prop('checked', false);

        $("#row-life_age").removeAttr('readOnly', 'readonly');
        $("#row-life_firstname").removeAttr('readOnly', 'readonly');
        $("#row-life_lastname").removeAttr('readOnly', 'readonly');
        $("#row-life_id").removeAttr('readOnly', 'readonly');
        $("#row-life_occupation").removeAttr('readOnly', 'readonly');
        $("#row-life_gender").removeAttr('disabled', 'disabled');
        $("#row-life_dob").removeAttr('readOnly', 'readonly');
        $("#row-life_bday").removeAttr('readOnly', 'readonly');
      }
    });

    $("#row-firstname").on("input", function() {
      if($("#row-same").is(":checked")) {
        $("#row-life_firstname").val($("#row-firstname").val());
      }
    });

    $("#row-lastname").on("input", function() {
      if($("#row-same").is(":checked")) {
        $("#row-life_lastname").val($("#row-lastname").val());
      }
    });

    $("#row-owner_occupation").on("input", function() {
      if($("#row-same").is(":checked")) {
        $("#row-life_occupation").val($("#row-owner_occupation").val());
      }
    });

    $("#row-owner_id").on("input", function() {
      if($("#row-same").is(":checked")) {
        $("#row-life_id").val($("#row-owner_id").val());
      }
    });

    $("#row-owner_gender").on("change", function() {
      if($("#row-same").is(":checked")) {
        $("#row-life_gender").val($("#row-owner_gender").val());
      }
    });

    $("#row-owner_dob").on("input", function() {
      if($("#row-same").is(":checked")) {
        $("#row-life_dob").val($("#row-owner_dob").val());
      }
    });

    $("#row-owner_dob").on("focusout", function() {
      var backage = moment('{{ $now }}').diff(moment($("#row-owner_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;

      if ($("#row-backdate").is(":checked")) {
        var backage = moment(moment($("#row-owner_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-owner_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
      }

      if ($("#row-backdate").is(":checked")) {
        if ($("#row-owner_age").val()) {
          $("#row-owner_bday").val(backage);
        }
      } else {
        $("#row-owner_bday").val(backage);
      }

      if($(".form-same").find('input').is(":checked")) {
        $("#row-life_dob").val($("#row-owner_dob").val());
        if ($("#row-backdate").is(":checked")) {
          if ($("#row-owner_age").val()) {
            $("#row-life_bday").val(backage);
          }
        } else {
          $("#row-life_bday").val(backage);
        }
      }
    });

    $("#row-owner_age").on("focusout", function() {
      if ($("#row-backdate").is(":checked")) {

        if ($(".form-same").find('input').is(":checked")) {
          $("#row-life_age").val($(this).val());
        }

        var backage = moment(moment($("#row-owner_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-owner_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;

        if ($(this).val() && $("#row-owner_dob").val()) {
          $("#row-owner_bday").val(backage);

          if ($(".form-same").find('input').is(":checked")) {
            $("#row-life_dob").val($("#row-owner_dob").val());
            $("#row-life_bday").val(backage);
          }
        }
      }
    });

    $("#row-backdate").change(function() {
      if($(this).is(":checked")) {
        $("#row-owner_age").val("");
        if($(".form-same").find('input').is(":checked")) {
          $(".form-life_backdate").find("input[type='checkbox']").prop('checked', true);
          $("#row-life_age").val("");
        }
      } else {
        if($(".form-same").find('input').is(":checked")) {
          $(".form-life_backdate").find("input[type='checkbox']").prop('checked', false);
        }
        $("#row-owner_age").val("");
        if($(".form-same").find('input').is(":checked")) {
          $("#row-life_age").val("");
        }

        var backage = moment('{{ $now }}').diff(moment($("#row-owner_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
        var backage_life = moment('{{ $now }}').diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
        
        if ($("#row-owner_dob").val()) {
          $("#row-owner_bday").val(backage);
        }

        if($(".form-same").find('input').is(":checked")) {
          $("#row-life_dob").val($("#row-owner_dob").val());
          if ($("#row-backdate").is(":checked")) {
            if ($("#row-owner_dob").val()) {
              $("#row-life_bday").val(backage);
            }
          }
        } else {
          if ($("#row-life_dob").val()) {
            if ($("#row-backdate").is(":checked")) {
              $("#row-life_age").val(backage_life);
            }
          }
        }
      }
    });

    $("#row-owner_dob").on("input", function() {
      var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;

      if((date_regex.test($(this).val()))) {
        var backage = moment('{{ $now }}').diff(moment($("#row-owner_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;

        if ($("#row-backdate").is(":checked")) {
          var backage = moment(moment($("#row-owner_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-owner_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
        }

        if ($("#row-backdate").is(":checked")) {
          if ($("#row-owner_age").val()) {
            $("#row-owner_bday").val(backage);
          }
        } else {
          $("#row-owner_bday").val(backage);
        }

        if($(".form-same").find('input').is(":checked")) {
          $("#row-life_dob").val($("#row-owner_dob").val());
          $("#row-life_age").val($("#row-owner_age").val());
          if ($("#row-backdate").is(":checked")) {
            if ($("#row-owner_age").val()) {
              $("#row-life_bday").val(backage);
            }
          } else {
            $("#row-life_bday").val(backage);
          }
        }
      } else {
        $("#row-owner_bday").val("");
        if($(".form-same").find('input').is(":checked")) {
          $("#row-life_dob").val($("#row-owner_dob").val());
          if ($("#row-backdate").is(":checked")) {
            $("#row-life_bday").val("");
          }
        }
      }
    });

    $("#row-owner_age").on("input", function() {
      var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;

      if ($(".form-same").find('input').is(":checked")) {
        $("#row-life_age").val($(this).val());
      }
        
      if((date_regex.test($(this).val()))) {
        if ($("#row-backdate").is(":checked")) {
          var backage = moment(moment($("#row-owner_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-owner_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;

          if ($(this).val() && $("#row-owner_dob").val()) {
            $("#row-owner_bday").val(backage);

            if ($(".form-same").find('input').is(":checked")) {
              $("#row-life_dob").val($("#row-owner_dob").val());
              $("#row-life_bday").val(backage);
            }
          }
        }
      } else {
        $("#row-owner_bday").val("");
        if($(".form-same").find('input').is(":checked")) {
          $("#row-life_dob").val($("#row-owner_dob").val());
          if ($("#row-backdate").is(":checked")) {
            $("#row-life_bday").val("");
          }
        }
      }
    });


    $("#row-life_backdate").change(function() {

      if($(this).is(":checked")) {
        $("#row-life_age").val("");
      } else {
        var backage_life = moment('{{ $now }}').diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
        if ($("#row-life_dob").val()) {
          $("#row-life_bday").val(backage_life);
        }
      }

    });

    $("#row-life_dob").on("focusout", function() {

      var backage = moment('{{ $now }}').diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;

      if ($("#row-life_backdate").is(":checked")) {
        var backage = moment(moment($("#row-life_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
      }

      if ($(this).val()) {
        if($(".form-same").find('input').is(":checked")) {
          // none
        } else {
          $("#row-life_bday").val(backage);
        }
      }

    });


    $("#row-life_dob").on("input", function() {
      var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;

      if((date_regex.test($(this).val()))) {
          var backage = moment('{{ $now }}').diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;

          if ($("#row-life_backdate").is(":checked")) {
            var backage = moment(moment($("#row-life_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
          }

        if($(".form-same").find('input').is(":checked")) {
          // none
        } else {
          $("#row-life_bday").val(backage);
        }
      } else {
        $("#row-life_bday").val("");
      }
    });

    $("#row-life_age").on("focusout", function() {
      if ($("#row-life_backdate").is(":checked")) {
        var backage = moment(moment($("#row-life_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
    
        if ($(this).val() && $("#row-life_dob").val()) {
          $("#row-life_bday").val(backage);
        }
      }
    });


    $("#row-life_age").on("input", function() {
      var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;

      if((date_regex.test($(this).val()))) {
        if ($("#row-life_backdate").is(":checked")) {
          var backage = moment(moment($("#row-life_age").val(), 'DD/MM/YYYY').format()).diff(moment($("#row-life_dob").val(), 'DD/MM/YYYY').format(), 'years') + 1;
      
          if ($(this).val() && $("#row-life_dob").val()) {
            $("#row-life_bday").val(backage);
          }
        }
      } else {
        $("#row-life_bday").val("");
      }
    });


    $('#row-consent_marketing_val_one').change(function() {
      if($(this).is(":checked")) {
        $("#row-consent_marketing").val("yes");
      }
    });

    $('#row-consent_marketing_val_two').change(function() {
      if($(this).is(":checked")) {
        $("#row-consent_marketing").val("no");
      }
    });

    $('#row-consent_servicing_val_one').change(function() {
      if($(this).is(":checked")) {
        $("#row-consent_servicing").val("yes");
      }
    });

    $('#row-consent_servicing_val_two').change(function() {
      if($(this).is(":checked")) {
        $("#row-consent_servicing").val("no");
      }
    });

    $('#row-consent_sms').change(function() {
      if($(this).is(":checked")) {
        $('#row-consent_sms_value').val(1);
      } else {
        $('#row-consent_sms_value').val(0);
      }
    });

    $('#row-consent_telephone').change(function() {
      if($(this).is(":checked")) {
        $('#row-consent_telephone_value').val(1);
      } else {
        $('#row-consent_telephone_value').val(0);
      }
    });


    // // CURRENCY CONVERSTION
    // $("#row-currency_value").on("input", function() {
    //   var currency = $('#row-currency').val();
    //   var currencyValue = $('#row-currency_value').val();
    //   var res = currency.split(" ");
    //   var myResult = res[0] * currencyValue;
    //   $("#row-conv_total").val(myResult);
    //   $("#row-currency_value2").val(myResult);
    //   var currency2 = $('#row-currency2').val();
    //   var res2 = currency2.split(" ");
    //   var myResult2 = res2[0] * myResult;
    //   $("#row-life_total_amount").val(myResult2);

    //   var productRate = $('#row-main_product_id').val();
    //   var sumAssured = $('#row-currency_value').val();
    //   var conversion_year =  $("#row-conversion_year").val();
    //   var conversion_rate =  $("#row-conversion_rate").val();
    //   var gst =  sumAssured * $("#row-conversion_gst").val();
    //   // console.log(gst + ' == ' + sumAssured + ' == ' + $("#row-conversion_gst").val());
    //   var policy_term = $("#row-policy_term").val();

    //   var frequency = $('#row-payment_frequency').val();
    //   var payment_frequency  = 0;
    //   if (frequency == "Single") {
    //     var payment_frequency  = 1;
    //   } else if (frequency == "Yearly") {
    //     var payment_frequency  = 1;
    //   } else if (frequency == "Half-Yearly") {
    //     var payment_frequency  = 2;
    //   } else if (frequency == "Quarterly") {
    //     var payment_frequency  = 3;
    //   } else if (frequency == "Monthly") {
    //     var payment_frequency  = 12;
    //   } else if (frequency == "Monthly/Single") {
    //     var payment_frequency  = 12;
    //   } else if (frequency == "Yearly/Single") {
    //     var payment_frequency  = 1;
    //   }

    //   // sumAssured = sumAssured * payment_frequency;

    //   if (conversion_year) {
    //     $(".error-ape .sup-errors").html("");
    //     if (conversion_year != 'none') {
    //       if (conversion_year == "5and8") {
    //         if (policy_term == "5") {
    //           $("#row-currency_value2").val(parseFloat((sumAssured * .5) - gst).toFixed(2));
    //           $("#row-ape").val(parseFloat(((sumAssured * .5) - gst) * payment_frequency).toFixed(2));
    //         } else if (policy_term == "8") {
    //           $("#row-currency_value2").val(parseFloat((sumAssured * .8) - gst).toFixed(2));
    //           $("#row-ape").val(parseFloat(((sumAssured * .8) - gst) * payment_frequency).toFixed(2));
    //         } else {
    //           $("#row-currency_value2").val(parseFloat(sumAssured - gst).toFixed(2));
    //           $("#row-ape").val(parseFloat((sumAssured - gst) * payment_frequency).toFixed(2));
    //         }
    //       } else if (conversion_year == "5") {
    //         if (policy_term == "5") {
    //           $("#row-currency_value2").val(parseFloat((sumAssured * .5) - gst).toFixed(2));
    //           $("#row-ape").val(parseFloat(((sumAssured * .5) - gst) * payment_frequency).toFixed(2));
    //         } else {
    //           $("#row-currency_value2").val(parseFloat(sumAssured - gst).toFixed(2));
    //           $("#row-ape").val(parseFloat((sumAssured - gst) * payment_frequency).toFixed(2));
    //         }
    //       } else if (conversion_year == "8") {
    //         if (policy_term == "8") {
    //           $("#row-currency_value2").val(parseFloat((sumAssured * .8) - gst).toFixed(2));
    //           $("#row-ape").val(parseFloat(((sumAssured * .8) - gst) * payment_frequency).toFixed(2));
    //         } else {
    //           $("#row-currency_value2").val(parseFloat(sumAssured - gst).toFixed(2));
    //           $("#row-ape").val(parseFloat((sumAssured - gst) * payment_frequency).toFixed(2));
    //         }
    //       }
    //     } else {
    //       $("#row-currency_value2").val(parseFloat(sumAssured - gst).toFixed(2));
    //       $("#row-ape").val(parseFloat((sumAssured - gst) * payment_frequency).toFixed(2));
    //     }
    //   } else {
    //     $(".error-ape .sup-errors").html("<span style='color: red;'>Select type of plan first.</span>");
    //   }
     
    // });

    // PLAN CONVERSION
    $("#row-payment_frequency").on("change", function() {
      var productRate = $('#row-main_product_id').val();
      var sumAssured = $('#row-currency_value2').val();

      var frequency = $('#row-payment_frequency').val();
      var payment_frequency = null;
      if (frequency == "Single") {
        var payment_frequency  = .1;
      } else if (frequency == "Yearly") {
        var payment_frequency  = 1;
      } else if (frequency == "Half-Yearly") {
        var payment_frequency  = 2;
      } else if (frequency == "Quarterly") {
        var payment_frequency  = 3;
      } else if (frequency == "Monthly") {
        var payment_frequency  = 12;
      } else if (frequency == "Monthly/Single") {
        var payment_frequency  = 12;
      } else if (frequency == "Yearly/Single") {
        var payment_frequency  = 1;
      }

      if (sumAssured.length < 1) {
        sumAssured = false;
      }

      if (!sumAssured) {
        $("#row-ape").val("");
        $(".error-ape .sup-errors").html("<span style='color: red;'>Insert amount for Total Premium Amount.</span>");
      } else if (!payment_frequency) {
        $("#row-ape").val("");
        $(".error-ape .sup-errors").html("<span style='color: red;'>Select Payment Frequency first.</span>");
      } else {
        $(".error-ape .sup-errors").html("");
        $("#row-ape").val(parseFloat(sumAssured * payment_frequency).toFixed(2));
      }
     });

    $("#row-currency_value2").on("keyup", function() {
      var productRate = $('#row-main_product_id').val();
      var sumAssured = $('#row-currency_value2').val();

      var frequency = $('#row-payment_frequency').val();
      var payment_frequency = null;
      if (frequency == "Single") {
        var payment_frequency  = .1;
      } else if (frequency == "Yearly") {
        var payment_frequency  = 1;
      } else if (frequency == "Half-Yearly") {
        var payment_frequency  = 2;
      } else if (frequency == "Quarterly") {
        var payment_frequency  = 3;
      } else if (frequency == "Monthly") {
        var payment_frequency  = 12;
      } else if (frequency == "Monthly/Single") {
        var payment_frequency  = 12;
      } else if (frequency == "Yearly/Single") {
        var payment_frequency  = 1;
      }

      if (sumAssured.length < 1) {
        sumAssured = false;
      }

      if (!sumAssured) {
        $("#row-ape").val("");
        $(".error-ape .sup-errors").html("<span style='color: red;'>Insert amount for Total Premium Amount.</span>");
      } else if (!payment_frequency) {
        $("#row-ape").val("");
        $(".error-ape .sup-errors").html("<span style='color: red;'>Select Payment Frequency first.</span>");
      } else {
        $(".error-ape .sup-errors").html("");
        $("#row-ape").val(parseFloat(sumAssured * payment_frequency).toFixed(2));
      }
     });

   // add telephone
    $("#row-mode_of_payment").change(function() {
      $("#row-mode_others").val("");
      if ($(this).val() == "Others") {
        $("#form-mode_others").removeClass("hide");
      } else {
        $("#form-mode_others").addClass("hide");
      }
    });

   // add telephone
    $("#row-residency_status").change(function() {
      $("#row-residency_others").val("");
      if ($(this).val() == "Others") {
        $(".form-residency_others").removeClass("hide");
      } else {  
        $(".form-residency_others").addClass("hide");
      }
    });

   // add telephone
    $("#row-type_of_insurance").change(function() {
      $("#row-type_of_insurance_others").val("");
      if ($(this).val() == "Others") {
        $("#form-type_of_insurance_others").removeClass("hide");
      } else {
        $("#form-type_of_insurance_others").addClass("hide");
      }
    });


    $(".btn-remove-upload").click(function() {
      $(".upload_file").removeClass('hide');
      $(".view_file").addClass('hide');
      $(".btn-cancel-upload").removeClass('hide');
      $("#row-upload_remove").val("yes");
    });

    $(".btn-cancel-upload").click(function() {
      $(".upload_file").addClass('hide');
      $(".view_file").removeClass('hide');
      $(".btn-cancel-upload").addClass('hide');
      $("#row-upload_remove").val("");
    });


    $(".btn-remove-upload_2").click(function() {
      $(".upload_file_2").removeClass('hide');
      $(".view_file_2").addClass('hide');
      $(".btn-cancel-upload_2").removeClass('hide');
      $("#row-upload_remove_2").val("yes");
    });

    $(".btn-cancel-upload_2").click(function() {
      $(".upload_file_2").addClass('hide');
      $(".view_file_2").removeClass('hide');
      $(".btn-cancel-upload_2").addClass('hide');
      $("#row-upload_remove_2").val("");
    });


    $(".btn-remove-upload_3").click(function() {
      $(".upload_file_3").removeClass('hide');
      $(".view_file_3").addClass('hide');
      $(".btn-cancel-upload_3").removeClass('hide');
      $("#row-upload_remove_3").val("yes");
    });

    $(".btn-cancel-upload_3").click(function() {
      $(".upload_file_3").addClass('hide');
      $(".view_file_3").removeClass('hide');
      $(".btn-cancel-upload_3").addClass('hide');
      $("#row-upload_remove_3").val("");
    });


    $(".btn-remove-upload_4").click(function() {
      $(".upload_file_4").removeClass('hide');
      $(".view_file_4").addClass('hide');
      $(".btn-cancel-upload_4").removeClass('hide');
      $("#row-upload_remove_4").val("yes");
    });

    $(".btn-cancel-upload_4").click(function() {
      $(".upload_file_4").addClass('hide');
      $(".view_file_4").removeClass('hide');
      $(".btn-cancel-upload_4").addClass('hide');
      $("#row-upload_remove_4").val("");
    });


    $(".btn-remove-upload_5").click(function() {
      $(".upload_file_5").removeClass('hide');
      $(".view_file_5").addClass('hide');
      $(".btn-cancel-upload_5").removeClass('hide');
      $("#row-upload_remove_5").val("yes");
    });

    $(".btn-cancel-upload_5").click(function() {
      $(".upload_file_5").addClass('hide');
      $(".view_file_5").removeClass('hide');
      $(".btn-cancel-upload_5").addClass('hide');
      $("#row-upload_remove_5").val("");
    });


    $( "#row-currency2" ).change(function() {
      var currency2 = $('#row-currency2').val();
      var currencyValue2 = $('#row-currency_value2').val();
      var res2 = currency2.split(" ");
      var myResult2 = res2[0] * currencyValue2;
      var myResultCode = res2[1];
      $("#row-life_total_amount").val(myResult2);
      $("#total_currency3").text(myResultCode);
    });

    $(".btn-refresh").click(function() {
      refresh();
    });

      //add user
    $(".btn-add").click(function() {

      var table_telephone = $("#telephone-table tbody");
      var row_telephone = table_telephone.find("tr:first");

      table_telephone.html($tel_tbody);
      $("#plan-table tbody").html($plan_tbody);
      // var newrow_telephone = row_telephone.clone();
      // newrow_telephone.appendTo(table_telephone);
      $("#row-reject_reason").addClass("hide");
      $("#label-reason").addClass("hide");

      $('.file_case').fileinput('reset');

      $(".upload_file").removeClass("hide");
      $(".view_file").addClass("hide");
      $(".btn-cancel-upload").addClass('hide');

      $(".upload_file_2").removeClass("hide");
      $(".view_file_2").addClass("hide");
      $(".btn-cancel-upload_2").addClass('hide');

      $(".upload_file_3").removeClass("hide");
      $(".view_file_3").addClass("hide");
      $(".btn-cancel-upload_3").addClass('hide');

      $(".upload_file_4").removeClass("hide");
      $(".view_file_4").addClass("hide");
      $(".btn-cancel-upload_4").addClass('hide');

      $(".upload_file_5").removeClass("hide");
      $(".view_file_5").addClass("hide");
      $(".btn-cancel-upload_5").addClass('hide');
      $("#row-nationality_id").select2('val', 124);
      $("#row-user_sales_id").select2('val', '');
      $("#row-ape").attr('readOnly', 'readonly');
      // reset all form fields
      $("#modal-production").find('input').val("");
      $("#modal-production").find('form').trigger("reset");
      $("input[name='_token']").val(token);
      // $(".modal-body button").show();
      $("#row-currency_rate").hide();
      $("#row-currency_rate2").hide();
      $("#form-mode_others").addClass("hide");
      $("#form-type_of_insurance_others").addClass("hide");
      $("#row-id").val("");
      $(".btn-add-plan").prop('disabled', false);
      $(".btn-del-plan").prop('disabled', false);
      $(".btn-add-telephone").prop('disabled', false);
      $(".btn-del-telephone").prop('disabled', false);
      $("#row-firstname").prop('disabled', false);
      $("#row-lastname").prop('disabled', false);
      $(".form-residency_others").addClass('hide');
      $("#row-life_firstname").prop('disabled', false);
      $("#row-life_lastname").prop('disabled', false);
      $("#row-policy_term").prop('disabled', false);
      $("#row-conv_total").prop('disabled', false);
      $("#row-life_total_amount").prop('disabled', false);
      $("#row-currency_value2").prop('disabled', false);
      $("#row-consent_marketing_val_two").prop('disabled', false);
      $("#row-consent_marketing_val_one").prop('disabled', false);
      $("#row-consent_servicing_val_two").prop('disabled', false);
      $("#row-consent_servicing_val_one").prop('disabled', false);
      $("#row-life_backdate").prop('disabled', false);

      $("#row-owner_age").prop('disabled', true);
      $("#row-life_age").prop('disabled', true);
      $("#row-selected_client").prop('disabled', false);

      $("#row-consent_marketing_val_one").attr("checked","checked");
      $("#row-consent_servicing_val_one").attr("checked","checked");

      $("#row-consent_mail").removeAttr("checked","checked");
      $("#row-consent_sms").removeAttr("checked","checked");
      $("#row-consent_telephone").removeAttr("checked","checked");
      $("#modal-production").find('.hide-view').removeClass('hide');
      $("#modal-production").find('.show-view').addClass('hide');
      $("#modal-production").find('.preview').addClass('hide');
      $("#modal-production").find('input').removeAttr('readonly', 'readonly');
      $("#modal-production").find('select').removeAttr('disabled');
      $('#row-currency_id').val("{{ $sgd_value->id }}");

      $("#row-currency_value2").attr("readonly", "readonly");
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
      $("#row-conv_total").attr('readOnly', 'readonly');
      $("#row-life_total_amount").attr('readOnly', 'readonly');
      // $("#row-ape").attr('readOnly', 'readonly');
      $("#modal-production").find('input').removeAttr('readonly', 'readonly');

      $(".rider_product_id").html("<option value='' class='hide'>Select Plan</option");
      $("#row-main_product_id").html("<option value='' class='hide'>Type of Insurance</option");

      $("#row-consent_marketing").val("yes");
      $("#row-consent_servicing").val("yes");
      $("#row-backdate").val("yes");
      $("#row-same").val("yes");
      $("#modal-production").modal('show');
    });


    $(".close").click(function(){
    var isValid = true;
        $('.required').each(function() {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $("#modal-production .alert").addClass("hide");
                $(this).attr("placeholder", "");
                $(this).css({
                    "border-color": "",
                    "background-color": "",

                });
            }
        });
    });

    $(".btn-close").click(function(){
    var isValid = true;
        $('.required').each(function() {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $("#modal-production .alert").addClass("hide");
                $(this).attr("placeholder", "");
                $(this).css({
                    "border-color": "",
                    "background-color": "",

                });
            }
        });
    });


    $("#modal-production").on("change", ".provider_list_rider", function() {

      var id = $(this).val();
      var lists = $(this).parent().parent().parent().find('td:nth-child(2)').find('select');
      $_token = "{{ csrf_token() }}";
      lists.html("");

      $.post("{{ url('policy/production/gi-case-submission/get-rider-plan') }}", { id: id, _token: $_token }, function(response) {
        if (response.lists.length > 0) {

          $(".provider_category").removeClass('hide');
          lists.append(
            '<option value="" class="hide">Select:</option>'
          );

          $.each(response.lists, function(index, row) {
            lists.append(
              '<option value="' + row.id + '">' + row.code + '-'  + row.description + '</option>'
            );
          });

        } else {
          $(".provider_category").addClass('hide');
        }
      }, 'json');
    });


      $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
        $("#row-reject_reason").removeClass("hide");
        $("#label-reason").removeClass("hide");
        var table_telephone = $("#telephone-table tbody");
        var row_telephone = table_telephone.find("tr:first");

        table_telephone.html($tel_tbody);
        // var newrow_telephone = row_telephone.clone();
        // newrow_telephone.appendTo(table_telephone);

        var table_plan = $("#plan-table tbody");
        var row_plan = table_plan.find("tr:first");

        table_plan.html("");
        var newrow_plan = row_plan.clone();
        newrow_plan.appendTo(table_plan);

        // reset all form fields
        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        $("#modal-production").find('form').trigger("reset");
        $("#modal-production").find('input').val("");
        $("input[name='_token']").val(token);
        $("#row-user_sales_id").prop('disabled', true);
        $("#row-annual_income_range").prop('disabled', true);
        $("#row-owner_gender").prop('disabled', true);
        $("#row-provider_list").prop('disabled', true);
        $("#row-provider_list_rider").prop('disabled', true);
        $("#row-rider_product_id").prop('disabled', true);
        $("#row-life_gender").prop('disabled', true);
        $("#row-residency_others").prop('disabled', true);
        $("#row-residency_status").prop('disabled', true);
        $("#row-type_of_insurance_others").prop('disabled', true);
        $("#row-type_of_insurance").prop('disabled', true);
        $("#row-nationality_id").prop('disabled', true);
        $(".form-mode_others").addClass("hide");
        $(".btn-add-plan").prop('disabled', true);
        $(".btn-del-plan").prop('disabled', true);
        $(".btn-add-telephone").prop('disabled', true);
        $(".btn-del-telephone").prop('disabled', true);
        $("#row-consent_marketing_val_two").prop('disabled', true);
        $("#row-consent_marketing_val_one").prop('disabled', true);
        $("#row-consent_servicing_val_two").prop('disabled', true);
        $("#row-consent_servicing_val_one").prop('disabled', true);
        $("#row-currency").prop('disabled', true);
        $("#row-currency2").prop('disabled', true);
        $("#row-type_of_insurance").prop('disabled', true);
        $("servicing").prop('disabled', true);
        $("#row-prem_currency").prop('disabled', true);
        $("#row-policy_term").prop('disabled', true);
        $("#row-payment_frequency").prop('disabled', true);
        $("#row-mode_of_payment").prop('disabled', true);
        $("#row-product_id").prop('disabled', true);
        $("#row-source").prop('disabled', true);
        $("#row-main_product_id").prop('disabled', true);
        $("#row-selected_client").prop('disabled', true);
        $("#row-currency_rate").hide();
        $("#row-currency_rate2").hide();
        $(".modal-body button").show();
        $("#row-id").val("");
        $("#row-main_product_id").html("");
        $("#row-ape").attr('readOnly', 'readonly');
        $("#row-nationality_id").select2('val', 124);
        $("#row-user_sales_id").select2('val', '');

        $(".upload_file").addClass("hide");
        $(".view_file").addClass("hide");
        $(".btn-cancel-upload").addClass('hide');

        $(".upload_file_2").addClass("hide");
        $(".view_file_2").addClass("hide");
        $(".btn-cancel-upload_2").addClass('hide');

        $(".upload_file_3").addClass("hide");
        $(".view_file_3").addClass("hide");
        $(".btn-cancel-upload_3").addClass('hide');

        $(".upload_file_4").addClass("hide");
        $(".view_file_4").addClass("hide");
        $(".btn-cancel-upload_4").addClass('hide');

        $(".upload_file_5").addClass("hide");
        $(".view_file_5").addClass("hide");
        $(".btn-cancel-upload_5").addClass('hide');
        $("#form-mode_others").addClass("hide");

            $.post("{{ url('policy/production/gi-case-submission/view-all') }}", { id: id, _token: $_token }, function(response) {
              if(!response.error) {

                // set form title
                $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View Case</strong>');

                // output form data
                $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                  if(index == 'selected_client')
                  {
                    if(value == 1)
                    {
                      $('#row-' + index).prop('checked', true);
                    }
                  }


                    if (index == 'user_id') {
                      field = $("#row-user_sales_id");
                      $("#row-user_sales_id").select2('val', value);
                    }

                    if (index == "mode_others") {
                      if (value) {
                        $("#form-" + index).removeClass("hide");
                        $("#row-mod_others").val(value);
                      }
                    }

                    if (index == "type_of_insurance_others") {
                      if (value) {
                        $("#form-" + index).removeClass("hide");
                        $("#row-type_of_insurance_others").val(value);
                      }
                    }

                    if (index == "upload_2") {
                      if (value) {
                        $(".upload_file_2").addClass("hide");
                        $(".view_file_2").removeClass("hide");
                        $("#row-download_2").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        $("#row-link_2").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        
                      } else {
                        $(".upload_file_2").removeClass("hide");
                        $(".view_file_2").addClass("hide");
                      }
                    }

                    if (index == "upload_3") {
                      if (value) {
                        $(".upload_file_3").addClass("hide");
                        $(".view_file_3").removeClass("hide");
                        $("#row-download_3").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        $("#row-link_3").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        
                      } else {
                        $(".upload_file_3").removeClass("hide");
                        $(".view_file_3").addClass("hide");
                      }
                    }

                    if (index == "upload_4") {
                      if (value) {
                        $(".upload_file_4").addClass("hide");
                        $(".view_file_4").removeClass("hide");
                        $("#row-download_4").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        $("#row-link_4").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        
                      } else {
                        $(".upload_file_4").removeClass("hide");
                        $(".view_file_4").addClass("hide");
                      }
                    }

                    if (index == "upload_5") {
                      if (value) {
                        $(".upload_file_5").addClass("hide");
                        $(".view_file_5").removeClass("hide");
                        $("#row-download_5").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        $("#row-link_5").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value);
                        
                      } else {
                        $(".upload_file_5").removeClass("hide");
                        $(".view_file_5").addClass("hide");
                      }
                    }

                    // field exists, therefore populate
                    if(field.length > 0) {
                      if(field.parent().hasClass("date")) {
                        if (value) {
                          field.val(moment(value).format('DD/MM/YYYY'));
                        }
                      } else {
                        if (index == "mode_others") {
                        } else {
                          field.val(value);
                        }
                        if (index == "type_of_insurance_others") {
                        } else {
                          field.val(value);
                        }
                      }
                    }

                });

            $("#modal-production").find('.hide-view').addClass('hide');
            $("#modal-production").find('.preview').removeClass('hide');
            $("#modal-production").find('.change').addClass('hide');
            $("#modal-production").find('.show-view').removeClass('hide');
            $("#modal-production").find('input').attr('readonly', 'readonly');

            // show form
            $("#modal-production").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-eye");
        }, 'json');

      });

      //edit cases
      $("#page-content-wrapper").on("click", ".btn-duplicate", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        var table_telephone = $("#telephone-table tbody");
        // var row_telephone = table_telephone.find("tr:first");
        $("#row-reject_reason").removeClass("hide");
        $("#label-reason").removeClass("hide");
        table_telephone.html($tel_tbody);
        // var newrow_telephone = row_telephone.clone();
        // newrow_telephone.appendTo(table_telephone);

        var table_plan = $("#plan-table tbody");
        var row_plan = table_plan.find("tr:first");

        table_plan.html("");
        var newrow_plan = row_plan.clone();
        newrow_plan.appendTo(table_plan);

        var token = $("input[name='_token']").val();
        // reset all form fields
        $("#modal-production").find('form').trigger("reset");
        $("#modal-production").find('input').val("");
        $("input[name='_token']").val(token);
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-currency_rate").hide();
        $("#row-currency_rate2").hide();
        $(".form-mode_others").addClass("hide");
        $("#row-residency_others").prop('disabled', false);
        $("#row-residency_status").prop('disabled', false);
        $("#row-nationality_id").prop('disabled', false);
        //$("#row-conv_total").prop('disabled', true);
        $(".btn-add-plan").prop('disabled', false);
        $(".btn-del-plan").prop('disabled', false);
        $(".btn-add-telephone").prop('disabled', false);
        $(".btn-del-telephone").prop('disabled', false);
        $("#row-user_sales_id").prop('disabled', false);
        $("#row-annual_income_range").prop('disabled', false);
        $("#row-owner_gender").prop('disabled', false);
        $("#row-life_gender").prop('disabled', false);
        $("#row-currency").prop('disabled', false);
        $("#row-prem_currency").prop('disabled', false);
        $("#row-provider_list").prop('disabled', false);
        $("#row-provider_list_rider").prop('disabled', false);
        $("#row-rider_product_id").prop('disabled', false);
        $("#row-consent_marketing_val_two").prop('disabled', false);
        $("#row-consent_marketing_val_one").prop('disabled', false);
        $("#row-consent_servicing_val_two").prop('disabled', false);
        $("#row-consent_servicing_val_one").prop('disabled', false);
        $("#row-currency").prop('disabled', false);
        $("#row-currency2").prop('disabled', false);
        $("#row-policy_term").prop('disabled', false);
        $("#row-payment_frequency").prop('disabled', false);
        $("#row-mode_of_payment").prop('disabled', false);
        $("#row-product_id").prop('disabled', false);
        $("#row-main_product_id").prop('disabled', false);
        $("#row-source").prop('disabled', false);
        // $(".modal-body button").show();
        $("#row-id").val("");
        $("#row-main_product_id").html("");

        $('.file_case').fileinput('reset');
        $(".upload_file").addClass("hide");
        $(".view_file").addClass("hide");
        $(".btn-cancel-upload").addClass('hide');
        
        $(".upload_file_2").addClass("hide");
        $(".view_file_2").addClass("hide");
        $(".btn-cancel-upload_2").addClass('hide');
        
        $(".upload_file_3").addClass("hide");
        $(".view_file_3").addClass("hide");
        $(".btn-cancel-upload_3").addClass('hide');
        
        $(".upload_file_4").addClass("hide");
        $(".view_file_4").addClass("hide");
        $(".btn-cancel-upload_4").addClass('hide');
        
        $(".upload_file_5").addClass("hide");
        $(".view_file_5").addClass("hide");
        $(".btn-cancel-upload_5").addClass('hide');

        $("#row-life_age").prop('disabled', true);
        $("#row-owner_age").prop('disabled', true);
        $("#row-ape").attr('readOnly', 'readonly');

        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-files-o");

              $.post("{{ url('policy/production/gi-case-submission/view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                    // console.log(response);
                  // set form title
                      $(".modal-title").html('<i class="fa fa-files-o"></i> <strong>Duplicate Production Case</strong>');

                      // output form data
                      $.each(response.row, function(index, value) {
                        var field = $("#row-" + index);


                        if (index == 'user_id') {
                          field = $("#row-user_sales_id");
                          $("#row-user_sales_id").select2('val', value);
                        }

                        if (index == 'nationality_id') {
                          $("#row-nationality_id").select2('val', value);
                        }
                        
                        if (index == "upload_3") {
                          if (value) {
                            $(".upload_file_3").addClass("hide");
                            $(".view_file_3").removeClass("hide");
                            $("#row-download_3").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));
                          } else {
                            $(".upload_file_3").removeClass("hide");
                            $(".view_file_3").addClass("hide");
                          }
                        }

                        if (index == "upload_4") {
                          if (value) {
                            $(".upload_file_4").addClass("hide");
                            $(".view_file_4").removeClass("hide");
                            $("#row-download_4").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));
                          } else {
                            $(".upload_file_4").removeClass("hide");
                            $(".view_file_4").addClass("hide");
                          }
                        }

                        if (index == "upload_5") {
                          if (value) {
                            $(".upload_file_5").addClass("hide");
                            $(".view_file_5").removeClass("hide");
                            $("#row-download_5").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));
                          } else {
                            $(".upload_file_5").removeClass("hide");
                            $(".view_file_5").addClass("hide");
                          }
                        }

                        // field exists, therefore populate
                        if(field.length > 0) {
                          if(field.parent().hasClass("date")) {
                            if (value) {
                              field.val(moment(value).format('DD/MM/YYYY'));
                            }
                          } else {
                            if (index == "mode_others") {
                            } else {
                              field.val(value.split(' ').join('%20'));
                            }
                            if (index == "type_of_insurance_others") {
                            } else {
                              field.val(value);
                            }
                          }
                        }

                      });

                      $("#modal-production").find('.hide-view').removeClass('hide');
                      $("#modal-production").find('.show-view').addClass('hide');
                      $("#modal-production").find('input').removeAttr('readonly');
                      $(".form-file").find('input').attr('readonly', 'readonly');
                      // show form
                      $("#row-ape").attr('readonly', 'readonly');

                      $(".upload_file_3").removeClass("hide");
                      $(".view_file_3").addClass("hide");
                      $(".btn-cancel-upload_3").addClass('hide');

                      $(".upload_file_4").removeClass("hide");
                      $(".view_file_4").addClass("hide");
                      $(".btn-cancel-upload_4").addClass('hide');

                      $(".upload_file_5").removeClass("hide");
                      $(".view_file_5").addClass("hide");
                      $(".btn-cancel-upload_5").addClass('hide');

                      $("#row-upload_name_3").removeAttr('readonly');
                      $("#row-upload_name_4").removeAttr('readonly');
                      $("#row-upload_name_5").removeAttr('readonly');
                      $("#row-id").val('');
                      $("#row-provider_list").val('');
                      $("#row-type_of_insurance").val('');
                      $("#row-currency_value2").val('');
                      $("#row-ape").val('');
                      $("#row-payment_frequency").val('');
                      $("#row-mode_of_payment").val('');
                      $("#row-source").val('');
                      $("#row-currency2").val('1 SGD');
                      $("#form-mode_others").addClass('hide');
                      $("#modal-production").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-files-o");
              }, 'json');
      });

      //edit cases
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        var table_telephone = $("#telephone-table tbody");
        // var row_telephone = table_telephone.find("tr:first");
        $("#row-reject_reason").removeClass("hide");
        $("#label-reason").removeClass("hide");
        table_telephone.html($tel_tbody);
        // var newrow_telephone = row_telephone.clone();
        // newrow_telephone.appendTo(table_telephone);

        var table_plan = $("#plan-table tbody");
        var row_plan = table_plan.find("tr:first");

        table_plan.html("");
        var newrow_plan = row_plan.clone();
        newrow_plan.appendTo(table_plan);
        $("#row-life_backdate").prop('disabled', false);

        var token = $("input[name='_token']").val();
        // reset all form fields
        $("#modal-production").find('form').trigger("reset");
        $("#modal-production").find('input').val("");
        $("input[name='_token']").val(token);
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-currency_rate").hide();
        $("#row-currency_rate2").hide();
        $(".form-mode_others").addClass("hide");
        $("#row-residency_others").prop('disabled', false);
        $("#row-residency_status").prop('disabled', false);
        $("#row-type_of_insurance_others").prop('disabled', false);
        $("#row-type_of_insurance").prop('disabled', false);
        $("#row-nationality_id").prop('disabled', false);
        //$("#row-conv_total").prop('disabled', true);
        $(".btn-add-plan").prop('disabled', false);
        $(".btn-del-plan").prop('disabled', false);
        $(".btn-add-telephone").prop('disabled', false);
        $(".btn-del-telephone").prop('disabled', false);
        $("#row-user_sales_id").prop('disabled', false);
        $("#row-annual_income_range").prop('disabled', false);
        $("#row-owner_gender").prop('disabled', false);
        $("#row-life_gender").prop('disabled', false);
        $("#row-currency").prop('disabled', false);
        $("#row-type_of_insurance").prop('disabled', false);
        $("#row-prem_currency").prop('disabled', false);
        $("#row-provider_list").prop('disabled', false);
        $("#row-provider_list_rider").prop('disabled', false);
        $("#row-rider_product_id").prop('disabled', false);
        $("#row-consent_marketing_val_two").prop('disabled', false);
        $("#row-consent_marketing_val_one").prop('disabled', false);
        $("#row-consent_servicing_val_two").prop('disabled', false);
        $("#row-consent_servicing_val_one").prop('disabled', false);
        $("#row-currency").prop('disabled', false);
        $("#row-currency2").prop('disabled', false);
        $("#row-policy_term").prop('disabled', false);
        $("#row-payment_frequency").prop('disabled', false);
        $("#row-mode_of_payment").prop('disabled', false);
        $("#row-product_id").prop('disabled', false);
        $("#row-main_product_id").prop('disabled', false);
        $("#row-source").prop('disabled', false);
        $("#row-selected_client").prop('disabled', false);
        // $(".modal-body button").show();
        $("#row-ape").attr('readOnly', 'readonly');
        $("#row-id").val("");
        $("#row-main_product_id").html("");

        $('.file_case').fileinput('reset');
        $(".upload_file").addClass("hide");
        $(".view_file").addClass("hide");
        $(".btn-cancel-upload").addClass('hide');
        
        $(".upload_file_2").addClass("hide");
        $(".view_file_2").addClass("hide");
        $(".btn-cancel-upload_2").addClass('hide');
        
        $(".upload_file_3").addClass("hide");
        $(".view_file_3").addClass("hide");
        $(".btn-cancel-upload_3").addClass('hide');
        
        $(".upload_file_4").addClass("hide");
        $(".view_file_4").addClass("hide");
        $(".btn-cancel-upload_4").addClass('hide');
        
        $(".upload_file_5").addClass("hide");
        $(".view_file_5").addClass("hide");
        $(".btn-cancel-upload_5").addClass('hide');

        $("#row-life_age").prop('disabled', true);
        $("#row-owner_age").prop('disabled', true);
        
        $("#form-mode_others").addClass("hide");
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

              $.post("{{ url('policy/production/gi-case-submission/view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                    // console.log(response);
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Production Case</strong>');

                      // output form data
                      $.each(response.row, function(index, value) {
                        var field = $("#row-" + index);

                        if(index == 'selected_client')
                        {
                          if(value == 1)
                          {
                            $('#row-' + index).prop('checked', true).val(1);
                          }
                        }

                        if (index == 'user_id') {
                          field = $("#row-user_sales_id");
                          $("#row-user_sales_id").select2('val', value);
                        }

                        if (index == 'nationality_id') {
                          $("#row-nationality_id").select2('val', value);
                        }

                        if (index == "mode_others") {
                          if (value) {
                            $("#form-" + index).removeClass("hide");
                            $("#row-mod_others").val(value);
                          }
                        }

                        if (index == "type_of_insurance_others") {
                          if (value) {
                            $("#form-" + index).removeClass("hide");
                            $("#row-type_of_insurance_others").val(value);
                          }
                        }

                        if (index == "upload_2") {
                          if (value) {
                            $(".upload_file_2").addClass("hide");
                            $(".view_file_2").removeClass("hide");
                            $("#row-download_2").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));
                            $("#row-link_2").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));

                            $('#row-link_2').tooltipster({
                              theme: 'tooltipster-noir',
                              contentAsHTML: true,
                              trigger: 'click',
                              interactive: true
                            });
                          } else {
                            $(".upload_file_2").removeClass("hide");
                            $(".view_file_2").addClass("hide");
                          }
                        }

                        if (index == "upload_3") {
                          if (value) {
                            $(".upload_file_3").addClass("hide");
                            $(".view_file_3").removeClass("hide");
                            $("#row-download_3").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));
                            $("#row-link_3").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));

                            $('#row-link_3').tooltipster({
                              theme: 'tooltipster-noir',
                              contentAsHTML: true,
                              trigger: 'click',
                              interactive: true
                            });
                          } else {
                            $(".upload_file_3").removeClass("hide");
                            $(".view_file_3").addClass("hide");
                          }
                        }

                        if (index == "upload_4") {
                          if (value) {
                            $(".upload_file_4").addClass("hide");
                            $(".view_file_4").removeClass("hide");
                            $("#row-download_4").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));
                            $("#row-link_4").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));

                            $('#row-link_4').tooltipster({
                              theme: 'tooltipster-noir',
                              contentAsHTML: true,
                              trigger: 'click',
                              interactive: true
                            });
                          } else {
                            $(".upload_file_4").removeClass("hide");
                            $(".view_file_4").addClass("hide");
                          }
                        }

                        if (index == "upload_5") {
                          if (value) {
                            $(".upload_file_5").addClass("hide");
                            $(".view_file_5").removeClass("hide");
                            $("#row-download_5").attr('href', '{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));
                            $("#row-link_5").val('{{ url("policy/production/gi-case-submission/download") }}' + '/' + value.split(' ').join('%20'));

                            $('#row-link_5').tooltipster({
                              theme: 'tooltipster-noir',
                              contentAsHTML: true,
                              trigger: 'click',
                              interactive: true
                            });
                          } else {
                            $(".upload_file_5").removeClass("hide");
                            $(".view_file_5").addClass("hide");
                          }
                        }

                        // field exists, therefore populate
                        if(field.length > 0) {
                          if(field.parent().hasClass("date")) {
                            if (value) {
                              field.val(moment(value).format('DD/MM/YYYY'));
                            }
                          } else {
                            if (index == "mode_others") {
                            } else {
                              field.val(value);
                            }
                            if (index == "type_of_insurance_others") {
                            } else {
                              field.val(value);
                            }
                          }
                        }

                      });

                      $("#modal-production").find('.hide-view').removeClass('hide');
                      $("#modal-production").find('.show-view').addClass('hide');
                      $("#modal-production").find('input').removeAttr('readonly');
                      $(".form-file").find('input').attr('readonly', 'readonly');

                      // show form
                      // $("#row-ape").attr('readonly', 'readonly');

                      $("#row-upload_name_2").removeAttr('readonly');
                      $("#row-upload_name_3").removeAttr('readonly');
                      $("#row-upload_name_4").removeAttr('readonly');
                      $("#row-upload_name_5").removeAttr('readonly');
                      $("#modal-production").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
      });

        $(function () {
        $('.owner_dob').datetimepicker({
            locale: 'en',
            format: 'DD/MM/YYYY'
        });
      });

        $(function () {
        $('.filter_status').datetimepicker({
            locale: 'en',
            format: 'MMM YYYY',
            ignoreReadonly: true,
            useCurrent: false
        });
      });

      $(".filter_status").on("dp.change", function(e) {
        search();
      });

      $(function () {
        $('.life_dob').datetimepicker({
            locale: 'en',
            format: 'DD/MM/YYYY'
        });
      });

        $(function () {
        $('.owner_age').datetimepicker({
            locale: 'en',
            format: 'DD/MM/YYYY'
        });
      });

        $(function () {
        $('.life_age').datetimepicker({
            locale: 'en',
            format: 'DD/MM/YYYY'
        });
      });


      $(function () {
        $('.policy_exp_date').datetimepicker({
            locale: 'en',
            format: 'DD/MM/YYYY'
        });
      });

      $(function () {
        $('.com_run_date').datetimepicker({
            locale: 'en',
            format: 'DD/MM/YYYY'
        });
      });

      $(function () {
        $('.date_issue').datetimepicker({
            locale: 'en',
            format: 'DD/MM/YYYY'
        });
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected provider/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      $("#modal-multiple-button").click(function() {

        var ids = [];

        $_token = "{{ csrf_token() }}";

        $("#tblData input:checked").each(function(){
            ids.push($(this).val());
        });

        if (ids.length == 0) {
          status("Error", "Select cases first.", 'alert-danger');
        } else {

        $.post("{{ url('policy/production/gi-case-submission/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
          refresh();

          if(response) {
            status("Success", "Select cases has been removed.", 'alert-success');
          }

        }, 'json');
          
        }

      });

      // incept case
      $("#page-content-wrapper").on("click", ".btn-incept", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Inception', 'Are you sure you want to incept <strong>' + name + '</strong>?', "{{ url('policy/production/gi-case-submission/incept') }}", id);
      });

      // delete pending case
      $("#page-content-wrapper").on("click", ".btn-delete", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Delete', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('policy/production/gi-case-submission/delete') }}", id);
      });

      // disable case
      // $("#page-content-wrapper").on("click", ".btn-disable", function() {
      //   var id = $(this).parent().parent().data('id');
      //   var name = $(this).parent().parent().find('td:nth-child(1)').html();

      //   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
      //   dialog('Case Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/disable') }}", id);
      // });

      // enable case
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('policy/production/gi-case-submission/enable') }}", id);
      });

      // enable case
      $("#page-content-wrapper").on("click", ".btn-assessment", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();
        $("#assessment-id").val(id);
        $("#modal-assessment").modal("show");
      });

      // enable case
      $("#page-content-wrapper").on("click", ".btn-review", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();
        $("#review-id").val(id);
        $("#modal-review").modal("show");
      });

      // enable case
      $("#page-content-wrapper").on("click", ".btn-submitted_ho", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();
        $("#submitted-id").val(id);
        $("#modal-submitted").modal("show");
      });

      // enable case
      // $("#page-content-wrapper").on("click", ".btn-duplicate", function() {
      //   var id = $(this).parent().parent().data('id');
      //   var name = $(this).parent().parent().find('td:nth-child(1)').html();
      //   $("#duplicate-id").val(id);
      //   $("#modal-duplicate").modal("show");
      // });


      // show modal for multiple inception
      $("#page-content-wrapper").on("click", ".btn-incept-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Inception");
        $(".modal-multiple-body").html("Are you sure you want to incept the selected case/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });

    });



    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {
      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      console.log('asc');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      console.log('desc');
      }

      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");

      $.post("{{ url('policy/production/gi-case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        table.html(response.rows);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
      
    });


    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ url('policy/production/gi-case-submission/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
          
          table.html(response.rows);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');

      }
   });


    $('#row-search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            search();
        }
    });

    $('#row-selected_client').change(function(){
      var selected_client = $(this);
      var checked = selected_client.prop('checked');
      if(checked == true)
      {
        selected_client.val(1);
      }
      else
      {
        selected_client.val(0);
      }

      console.log(selected_client.val());
      
    });

  </script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
      <h3 class="main-title">GI SUBMISSION</h3>
      <h5 class="bread-crumb">PRODUCTION & POLICY MANAGEMENT</h5>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
        @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || Auth::user()->usertype_id == 8)
          @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4)
          <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
          @endif
          <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-add"><i class="fa fa-plus"></i> Add</button> 
        @endif
        @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
          <form style="float: right;" role="form" method="post" action="{{ url('policy/production/gi-case-submission/export') }}">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-export"><i class="fa fa-print"></i> Export</button>
          </form>
          <form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('policy/production/gi-case-submission/export') }}">
            {{ csrf_field() }}
            <input type="hidden" name="selected_client_on" value="1">
            <button type="submit" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-export"><i class="fa fa-print"></i> Export Selected Client</button>
          </form>
        @endif
  </div>
  </div>
  <div class="container-fluid default-container container-space">
  <div class="col-lg-12">
        <p><strong>FILTER OPTIONS</strong></p>
  </div>
    <form class="form-horizontal" >     

  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
        <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>MONTH YEAR</strong></p></label>
        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
          <div>
              <div class="input-group date filter_status">
                <input class="form-control" readonly="readonly" style="background:white;" name="filter_status" id="row-filter_status" name="filter_status" type="text">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
          </div>
      </div>
    </div>
  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                          <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                          <span class="input-group-btn">
                            <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
    </form>
  </div>
  <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
    <div class="block-content tbblock col-xs-12">
      <div class="loading-pane hide">
        <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
      </div>
      <div class="table-responsive">
        @if(Auth::user()->usertype_id == 8)
          @if($supervisor)
          <table class="table table-striped table-pagination table-case table-case-supervisor" id="tblData">
            <thead class="tbheader">
              <tr>
                <th nowrap data-sort="name"><i></i> <input type="hidden" value="name"><small>Agent Name</small></th>
                <th class="th-sort" nowrap data-sort="company_name"><i></i> <input type="hidden" value="company_name"><small>PolicyOwner<br>Name</small></th>
                <th class="th-sort" nowrap data-sort="type_of_insurance"><i></i> <input type="hidden" value="type_of_insurance"><small>Main Plan</small></th>
                <th class="th-sort" nowrap data-sort="main_sum_assured"><i></i> <input type="hidden" value="main_sum_assured"><small>Sum<br>Assured</small></th>
                <th class="th-sort" nowrap data-sort="currency_value2"><i></i> <input type="hidden" value="currency_value2"><small>Premium<br>Amount</small></th>
                <th class="th-sort" nowrap data-sort="payment_frequency"><i></i> <input type="hidden" value="payment_frequency"><small>Payment<br>Frequency</small></th>
                <th class="th-sort" nowrap data-sort="ape"><i></i> <input type="hidden" value="ape"><small>APE</small></th>
                <th class="th-sort" nowrap data-sort="case"><i></i> <input type="hidden" value="case"><small>Status</small></th>
                <th class="th-sort" nowrap data-sort="selected_client"><i></i> <input type="hidden" value="selected_client"><small>Selected<br>Client</small></th>
                <th class="rightalign"> <small>TOOLS</small></th>
              </tr>
            </thead>
            <tbody id="rows">
              <?php echo($rows); ?>
            </tbody>
          </table>
          @else
          <table class="table table-striped table-pagination table-case" id="tblData">
            <thead class="tbheader">
              <tr>
                <th class="th-sort" nowrap data-sort="company_name"><i></i> <small>PolicyOwner<br>Name</small></th>
                <!-- <th class="th-sort" nowrap data-sort="owner_id"><i></i> <small>PolicyOwner<br>ID</small></th> -->
                <th class="th-sort" nowrap data-sort="nationality"><i></i> <small>PolicyOwner<br>Nationality</small></th>
                <th class="th-sort" nowrap data-sort="residency_status"><i></i> <small>PolicyOwner<br>Residency Status</small></th>
                <th class="th-sort" nowrap data-sort="owner_dob"><i></i> <small>PolicyOwner<br>Date of Birth</small></th>
                <th class="th-sort" nowrap data-sort="type_of_insurance"><i></i> <small>Main Plan</small></th>
                <th class="th-sort" nowrap data-sort="main_sum_assured"><i></i> <small>Sum<br>Assured</small></th>
                <th class="th-sort" nowrap data-sort="currency_value2"><i></i> <small>Premium<br>Amount</small></th>
                <th class="th-sort" nowrap data-sort="policy_term"><i></i> <small>Policy<br>Term</small></th>
                <th class="th-sort" nowrap data-sort="case"><i></i> <small>Status</small></th>
                <th class="th-sort" nowrap data-sort="selected_client"><i></i> <small>Selected<br>Client</small></th>
                <!-- <th class="th-sort" nowrap data-sort="consent_marketing"><i></i> <small>Marketing</small></th>
                <th class="th-sort" nowrap data-sort="consent_servicing"><i></i> <small>Servicing</small></th> -->
                <th class="rightalign"> <small>TOOLS</small></th>
              </tr>
            </thead>
            <tbody id="rows">
              @foreach($rows as $row)
              <tr class="{{ ($row->status == 1) ? '' : 'tr-disabled' }}" data-id="{{ $row->id}}">
                <td>{{ $row->company_name }}</td>
                <!-- <td>{{ $row->owner_id }}</td> -->
                <td>{{ $row->nationality }}</td>
                @if ($row->residency_status == "Others")
                <td>{{ $row->residency_others }}</td>
                @else
                <td>{{ $row->residency_status }}</td>
                @endif
                <td>{{ ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') }}</td>
                @if ($row->type_of_insurance == "Others")
                <td>{{ $row->type_of_insurance_others }}</td>
                @else
                <td>{{ $row->type_of_insurance }}</td>
                @endif
                <td class="rightalign">{{ $row->main_sum_assured, 2 }}</td>
                <td class="rightalign">{{ number_format($row->currency_value2, 2) }}</td>
                <td>{{ $row->policy_term }}</td>
                <td>{{ $row->case }}</td>
                <!-- <td>{{ $row->consent_marketing }}</td>
                <td>{{ $row->consent_servicing }}</td> -->
                <td>{{$row->selected_client == 1 ? 'YES' : 'NO'}}</td>
                <td class="rightalign">
                  <button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>
                  <!-- <button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button> -->
                  <!-- <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}"><i class="fa fa-adjust"></i></button> -->
                  <!-- <input id="checkbox" type="checkbox" value="{{ $row->id }}"> -->
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @endif
        @elseif(Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4)
        <!-- Admin Users -->
          <table class="table table-striped table-pagination table-case" id="tblData">
            <thead class="tbheader">
              <tr>
                <th class="th-sort" nowrap data-sort="name"><i></i> <small>Agent<br>Name</small></th>
                <th class="th-sort" nowrap data-sort="company_name"><i></i> <small>PolicyOwner<br>Name</small></th>
                <!-- <th class="th-sort" nowrap data-sort="owner_id"><i></i> <small>PolicyOwner<br>ID</small></th> -->
                <th class="th-sort" nowrap data-sort="nationality"><i></i> <small>PolicyOwner<br>Nationality</small></th>
                <th class="th-sort" nowrap data-sort="residency_status"><i></i> <small>PolicyOwner<br>Residency Status</small></th>
                <th class="th-sort" nowrap data-sort="owner_dob"><i></i> <small>PolicyOwner<br>Date of Birth</small></th>
                <th class="th-sort" nowrap data-sort="type_of_insurance"><i></i> <small>Main Plan</small></th>
                <th class="th-sort" nowrap data-sort="currency_id"><i></i> <small>Currency<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="main_sum_assured"><i></i> <small>Sum<br>Assured</small></th>
                <th class="th-sort" nowrap data-sort="currency_value2"><i></i> <small>Premium<br>Amount</small></th>
                <th class="th-sort" nowrap data-sort="policy_term"><i></i> <small>Policy<br>Term</small></th>
                <th class="th-sort" nowrap data-sort="mode_of_payment"><i></i> <small>ModeOf<br>Payment</small></th>
                <th class="th-sort" nowrap data-sort="annual_currency_value2"><i></i> <small>APE</small></th>
                <th class="th-sort" nowrap data-sort="agent_code"><i></i> <small>Agent<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="created_at"><i></i> <small>Date<br>Submitted</small></th>
                <th class="th-sort" nowrap data-sort="source"><i></i> <small>Source</small></th>
                <th class="th-sort" nowrap data-sort="case"><i></i> <small>Submission<br>Status</small></th>
                <th class="th-sort" nowrap data-sort="screen_date"><i></i> <small>Screened<br>Date</small></th>
                <th class="th-sort" nowrap data-sort="screened_by"><i></i> <small>Screened<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_ho"><i></i> <small>Submitted<br>to HO</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_date"><i></i> <small>Submitted<br>to HO Date</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_by"><i></i> <small>Submitted<br>to HO by</small></th>
                <th class="th-sort" nowrap data-sort="screened_status"><i></i> <small>Screened<br>Status</small></th>
                <th class="rightalign"> <small>TOOLS</small></th>
              </tr>
            </thead>
            <tbody id="rows">
              @foreach($rows as $row)
              <tr class="{{ ($row->status == 1) ? '' : 'tr-disabled' }}" data-id="{{ $row->id}}">
                <td>{{ strtoupper($row->name) }}</td>
                <td>{{ $row->company_name }}</td>
                <!-- <td>{{ $row->owner_id }}</td> -->
                <td>{{ $row->nationality }}</td>
                @if ($row->residency_status == "Others")
                <td>{{ $row->residency_others }}</td>
                @else
                <td>{{ $row->residency_status }}</td>
                @endif
                <td>{{ ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') }}</td>
                @if ($row->type_of_insurance == "Others")
                <td>{{ $row->type_of_insurance_others }}</td>
                @else
                <td>{{ $row->type_of_insurance }}</td>
                @endif
                <td>{{ $row->getSettingsRate->code }}</td>
                <td class="rightalign">{{ $row->main_sum_assured, 2 }}</td>
                <td class="rightalign">{{ number_format($row->currency_value2, 2) }}</td>
                <td>{{ $row->policy_term }}</td>
                <td>{{ $row->mode_of_payment }}</td>
                <td class="rightalign">{{ number_format($row->ape, 2) }}</td>
                <td>{{ $row->agent_code }}</td>
                <td>{{ date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') }}</td>
                <td>{{ $row->source }}</td>
                <td>{{ $row->case }}</td>
                @if($row->screen_date)
                <td>{{ date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->screened_by)
                <td>{{ $row->screened_by }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->submitted_ho)
                <td class="hide">{{ $row->submitted_ho }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_date)
                <td class="hide">{{ date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_by)
                <td class="hide">{{ $row->submitted_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->screened_status)
                <td class="">{{ $row->screened_status }}</td>
                @else
                <td class="">N/A</td>
                @endif
                <td class="rightalign">
                  @if($row->assess_outcome == "False Positive" && $row->submitted_ho == "No")
                  <button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>
                  @endif
                  <button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                  <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        @elseif(Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10)
          <table class="table table-striped table-pagination table-case" id="tblData">
            <thead class="tbheader">
              <tr>
                <th class="th-sort" nowrap data-sort="name"><i></i> <small>Agent<br>Name</small></th>
                <th class="th-sort" nowrap data-sort="company_name"><i></i> <small>PolicyOwner<br>Name</small></th>
                <!-- <th class="th-sort" nowrap data-sort="owner_id"><i></i> <small>PolicyOwner<br>ID</small></th> -->
                <th class="th-sort" nowrap data-sort="nationality"><i></i> <small>PolicyOwner<br>Nationality</small></th>
                <th class="th-sort" nowrap data-sort="residency_status"><i></i> <small>PolicyOwner<br>Residency Status</small></th>
                <th class="th-sort" nowrap data-sort="owner_dob"><i></i> <small>PolicyOwner<br>Date of Birth</small></th>
                <th class="th-sort" nowrap data-sort="type_of_insurance"><i></i> <small>Main Plan</small></th>
                <th class="th-sort" nowrap data-sort="currency_id"><i></i> <small>Currency<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="main_sum_assured"><i></i> <small>Sum<br>Assured</small></th>
                <th class="th-sort" nowrap data-sort="currency_value2"><i></i> <small>Premium<br>Amount</small></th>
                <th class="th-sort" nowrap data-sort="policy_term"><i></i> <small>Policy<br>Term</small></th>
                <th class="th-sort" nowrap data-sort="mode_of_payment"><i></i> <small>ModeOf<br>Payment</small></th>
                <th class="th-sort" nowrap data-sort="annual_currency_value2"><i></i> <small>APE</small></th>
                <th class="th-sort" nowrap data-sort="agent_code"><i></i> <small>Agent<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="created_at"><i></i> <small>Date<br>Submitted</small></th>
                <th class="th-sort" nowrap data-sort="source"><i></i> <small>Source</small></th>
                <th class="th-sort" nowrap data-sort="case"><i></i> <small>Submission<br>Status</small></th>
                <th class="th-sort" nowrap data-sort="screen_date"><i></i> <small>Screened<br>Date</small></th>
                <th class="th-sort" nowrap data-sort="screened_by"><i></i> <small>Screened<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_outcome"><i></i> <small>Assessed<br>Outcome</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_date"><i></i> <small>Assessed<br>Date</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_by"><i></i> <small>Assessed<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="review_outcome"><i></i> <small>Reviewed<br>Outcome</small></th>
                <th class="th-sort hide" nowrap data-sort="review_date"><i></i> <small>Reviewed<br>Date</small></th>
                <th class="th-sort hide" nowrap data-sort="review_by"><i></i> <small>Reviewed<br>By</small></th>
                <th class="th-sort " nowrap data-sort="screened_status"><i></i> <small>Screened<br>Status</small></th>
                <th class="rightalign"> <small>TOOLS</small></th>
              </tr>
            </thead>
            <tbody id="rows">
              @foreach($rows as $row)
              <tr class="{{ ($row->status == 1) ? '' : 'tr-disabled' }}" data-id="{{ $row->id}}">
                <td>{{ strtoupper($row->name) }}</td>
                <td>{{ $row->company_name }}</td>
                <!-- <td>{{ $row->owner_id }}</td> -->
                <td>{{ $row->nationality }}</td>
                @if ($row->residency_status == "Others")
                <td>{{ $row->residency_others }}</td>
                @else
                <td>{{ $row->residency_status }}</td>
                @endif
                <td>{{ ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') }}</td>
                @if ($row->type_of_insurance == "Others")
                <td>{{ $row->type_of_insurance_others }}</td>
                @else
                <td>{{ $row->type_of_insurance }}</td>
                @endif
                <td>{{ $row->getSettingsRate->code }}</td>
                <td class="rightalign">{{ $row->main_sum_assured, 2 }}</td>
                <td class="rightalign">{{ number_format($row->currency_value2, 2) }}</td>
                <td>{{ $row->policy_term }}</td>
                <td>{{ $row->mode_of_payment }}</td>
                <td class="rightalign">{{ number_format($row->ape, 2) }}</td>
                <td>{{ $row->agent_code }}</td>
                <td>{{ date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') }}</td>
                <td>{{ $row->source }}</td>
                <td>{{ $row->case }}</td>
                @if($row->screen_date)
                <td>{{ date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->screened_by)
                <td>{{ $row->screened_by }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->assess_outcome)
                <td class="hide">{{ $row->assess_outcome }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->assess_date)
                <td class="hide">{{ date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->assess_by)
                <td class="hide">{{ $row->assess_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->review_outcome)
                <td class="hide">{{ $row->review_outcome }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->review_date)
                <td class="hide">{{ date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->review_by)
                <td class="hide">{{ $row->review_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->screened_status)
                <td class="">{{ $row->screened_status }}</td>
                @else
                <td class="">N/A</td>
                @endif
                <td class="rightalign">
                  @if (Auth::user()->usertype_id == 9)
                    @if(!$row->review_outcome && $row->assess_outcome)
                    <button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>
                    @endif
                  @endif
                  @if(!$row->assess_outcome && $row->case == "rejected")
                  <button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>
                  @endif
                  <button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

        @elseif(Auth::user()->usertype_id == 1)
        <!-- CEO -->
          <table class="table table-striped table-pagination table-case" id="tblData">
            <thead class="tbheader">
              <tr>
                <th class="th-sort" nowrap data-sort="name"><i></i> <small>Agent<br>Name</small></th>
                <th class="th-sort" nowrap data-sort="company_name"><i></i> <small>PolicyOwner<br>Name</small></th>
                <!-- <th class="th-sort" nowrap data-sort="owner_id"><i></i> <small>PolicyOwner<br>ID</small></th> -->
                <th class="th-sort" nowrap data-sort="nationality"><i></i> <small>PolicyOwner<br>Nationality</small></th>
                <th class="th-sort" nowrap data-sort="residency_status"><i></i> <small>PolicyOwner<br>Residency Status</small></th>
                <th class="th-sort" nowrap data-sort="owner_dob"><i></i> <small>PolicyOwner<br>Date of Birth</small></th>
                <th class="th-sort" nowrap data-sort="type_of_insurance"><i></i> <small>Main Plan</small></th>
                <th class="th-sort" nowrap data-sort="currency_id"><i></i> <small>Currency<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="main_sum_assured"><i></i> <small>Sum<br>Assured</small></th>
                <th class="th-sort" nowrap data-sort="currency_value2"><i></i> <small>Premium<br>Amount</small></th>
                <th class="th-sort" nowrap data-sort="policy_term"><i></i> <small>Policy<br>Term</small></th>
                <th class="th-sort" nowrap data-sort="mode_of_payment"><i></i> <small>ModeOf<br>Payment</small></th>
                <th class="th-sort" nowrap data-sort="annual_currency_value2"><i></i> <small>APE</small></th>
                <th class="th-sort" nowrap data-sort="agent_code"><i></i> <small>Agent<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="created_at"><i></i> <small>Date<br>Submitted</small></th>
                <th class="th-sort" nowrap data-sort="source"><i></i> <small>Source</small></th>
                <th class="th-sort" nowrap data-sort="case"><i></i> <small>Submission<br>Status</small></th>
                <th class="th-sort" nowrap data-sort="screen_date"><i></i> <small>Screened<br>Date</small></th>
                <th class="th-sort" nowrap data-sort="screened_by"><i></i> <small>Screened<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_outcome"><i></i> <small>Assessed<br>Outcome</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_date"><i></i> <small>Assessed<br>Date</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_by"><i></i> <small>Assessed<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="review_outcome"><i></i> <small>Reviewed<br>Outcome</small></th>
                <th class="th-sort hide" nowrap data-sort="review_date"><i></i> <small>Reviewed<br>Date</small></th>
                <th class="th-sort hide" nowrap data-sort="review_by"><i></i> <small>Reviewed<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_ho"><i></i> <small>Submitted<br>to HO</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_date"><i></i> <small>Submitted<br>to HO Date</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_by"><i></i> <small>Submitted<br>to HO by</small></th>
                <th class="th-sort" nowrap data-sort="screened_status"><i></i> <small>Screened<br>Status</small></th>
                <th class="th-sort" nowrap data-sort="selected_client"><i></i> <small>Selected<br>Client</small></th>
                <th class="rightalign"> <small>TOOLS</small></th>
              </tr>
            </thead>
            <tbody id="rows">
              @foreach($rows as $row)
              <tr class="{{ ($row->status == 1) ? '' : 'tr-disabled' }}" data-id="{{ $row->id}}">
                <td>{{ strtoupper($row->name) }}</td>
                <td>{{ $row->company_name }}</td>
                <!-- <td>{{ $row->owner_id }}</td> -->
                <td>{{ $row->nationality }}</td>
                @if ($row->residency_status == "Others")
                <td>{{ $row->residency_others }}</td>
                @else
                <td>{{ $row->residency_status }}</td>
                @endif
                <td>{{ ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') }}</td>
                @if ($row->type_of_insurance == "Others")
                <td>{{ $row->type_of_insurance_others }}</td>
                @else
                <td>{{ $row->type_of_insurance }}</td>
                @endif
                <td>{{ $row->getSettingsRate->code }}</td>
                <td class="rightalign">{{ $row->main_sum_assured, 2 }}</td>
                <td class="rightalign">{{ number_format($row->currency_value2, 2) }}</td>
                <td>{{ $row->policy_term }}</td>
                <td>{{ $row->mode_of_payment }}</td>
                <td class="rightalign">{{ number_format($row->ape, 2) }}</td>
                <td>{{ $row->agent_code }}</td>
                <td>{{ date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') }}</td>
                <td>{{ $row->source }}</td>
                <td>{{ $row->case }}</td>
                @if($row->screen_date)
                <td>{{ date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->screened_by)
                <td>{{ $row->screened_by }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->assess_outcome)
                <td class="hide">{{ $row->assess_outcome }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->assess_date)
                <td class="hide">{{ date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->assess_by)
                <td class="hide">{{ $row->assess_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->review_outcome)
                <td class="hide">{{ $row->review_outcome }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->review_date)
                <td class="hide">{{ date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->review_by)
                <td class="hide">{{ $row->review_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_ho)
                <td class="hide">{{ $row->submitted_ho }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_date)
                <td class="hide">{{ date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_by)
                <td class="hide">{{ $row->submitted_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->screened_status)
                <td class="">{{ $row->screened_status }}</td>
                @else
                <td class="">N/A</td>
                @endif
                @if($row->selected_client == 1)
                <td class="">YES</td>
                @else
                <td class="">NO</td>
                @endif
                <td class="rightalign">
                  @if(!$row->review_outcome && $row->assess_outcome)
                  <button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>
                  @endif
                  @if($row->assess_outcome == "False Positive" && $row->submitted_ho == "No")
                  <button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>
                  @endif
                  @if(!$row->assess_outcome && $row->case == "rejected")
                  <button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>
                  @endif
                  <button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                  <button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>
                  <!-- <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}"><i class="fa fa-{{ ($row->status == 1) ? 'adjust' : 'check-circle' }}"></i></button> -->
                  <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        @else
          <table class="table table-striped table-pagination table-case" id="tblData">
            <thead class="tbheader">
              <tr>
                <th class="th-sort" nowrap data-sort="name"><i></i> <small>Agent<br>Name</small></th>
                <th class="th-sort" nowrap data-sort="company_name"><i></i> <small>PolicyOwner<br>Name</small></th>
                <!-- <th class="th-sort" nowrap data-sort="owner_id"><i></i> <small>PolicyOwner<br>ID</small></th> -->
                <th class="th-sort" nowrap data-sort="nationality"><i></i> <small>PolicyOwner<br>Nationality</small></th>
                <th class="th-sort" nowrap data-sort="residency_status"><i></i> <small>PolicyOwner<br>Residency Status</small></th>
                <th class="th-sort" nowrap data-sort="owner_dob"><i></i> <small>PolicyOwner<br>Date of Birth</small></th>
                <th class="th-sort" nowrap data-sort="type_of_insurance"><i></i> <small>Main Plan</small></th>
                <th class="th-sort" nowrap data-sort="currency_id"><i></i> <small>Currency<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="main_sum_assured"><i></i> <small>Sum<br>Assured</small></th>
                <th class="th-sort" nowrap data-sort="currency_value2"><i></i> <small>Premium<br>Amount</small></th>
                <th class="th-sort" nowrap data-sort="policy_term"><i></i> <small>Policy<br>Term</small></th>
                <th class="th-sort" nowrap data-sort="mode_of_payment"><i></i> <small>ModeOf<br>Payment</small></th>
                <th class="th-sort" nowrap data-sort="annual_currency_value2"><i></i> <small>APE</small></th>
                <th class="th-sort" nowrap data-sort="agent_code"><i></i> <small>Agent<br>Code</small></th>
                <th class="th-sort" nowrap data-sort="created_at"><i></i> <small>Date<br>Submitted</small></th>
                <th class="th-sort" nowrap data-sort="source"><i></i> <small>Source</small></th>
                <th class="th-sort" nowrap data-sort="case"><i></i> <small>Submission<br>Status</small></th>
                <th class="th-sort" nowrap data-sort="screen_date"><i></i> <small>Screened<br>Date</small></th>
                <th class="th-sort" nowrap data-sort="screened_by"><i></i> <small>Screened<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_outcome"><i></i> <small>Assessed<br>Outcome</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_date"><i></i> <small>Assessed<br>Date</small></th>
                <th class="th-sort hide" nowrap data-sort="assess_by"><i></i> <small>Assessed<br>By</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_ho"><i></i> <small>Submitted<br>to HO</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_date"><i></i> <small>Submitted<br>to HO Date</small></th>
                <th class="th-sort hide" nowrap data-sort="submitted_by"><i></i> <small>Submitted<br>to HO by</small></th>
                @if (Auth::user()->usertype_id == 2)
                <th class="th-sort" nowrap data-sort="screened_status"><i></i> <small>Screened<br>Status</small></th>
                @endif
                <th class="rightalign"> <small>TOOLS</small></th>
              </tr>
            </thead>
            <tbody id="rows">
              @foreach($rows as $row)
              <tr class="{{ ($row->status == 1) ? '' : 'tr-disabled' }}" data-id="{{ $row->id}}">
                <td>{{ strtoupper($row->name) }}</td>
                <td>{{ $row->company_name }}</td>
                <!-- <td>{{ $row->owner_id }}</td> -->
                <td>{{ $row->nationality }}</td>
                @if ($row->residency_status == "Others")
                <td>{{ $row->residency_others }}</td>
                @else
                <td>{{ $row->residency_status }}</td>
                @endif
                <td>{{ ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') }}</td>
                @if ($row->type_of_insurance == "Others")
                <td>{{ $row->type_of_insurance_others }}</td>
                @else
                <td>{{ $row->type_of_insurance }}</td>
                @endif
                <td>{{ $row->getSettingsRate->code }}</td>
                <td class="rightalign">{{ $row->main_sum_assured, 2 }}</td>
                <td class="rightalign">{{ number_format($row->currency_value2, 2) }}</td>
                <td>{{ $row->policy_term }}</td>
                <td>{{ $row->mode_of_payment }}</td>
                <td class="rightalign">{{ number_format($row->ape, 2) }}</td>
                <td>{{ $row->agent_code }}</td>
                <td>{{ date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') }}</td>
                <td>{{ $row->source }}</td>
                <td>{{ $row->case }}</td>
                @if($row->screen_date)
                <td>{{ date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->screened_by)
                <td>{{ $row->screened_by }}</td>
                @else
                <td>N/A</td>
                @endif
                @if($row->assess_outcome)
                <td class="hide">{{ $row->assess_outcome }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->assess_date)
                <td class="hide">{{ date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->assess_by)
                <td class="hide">{{ $row->assess_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_ho)
                <td class="hide">{{ $row->submitted_ho }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_date)
                <td class="hide">{{ date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if($row->submitted_by)
                <td class="hide">{{ $row->submitted_by }}</td>
                @else
                <td class="hide">N/A</td>
                @endif
                @if (Auth::user()->usertype_id == 2)
                @if($row->screened_status)
                <td class="">{{ $row->screened_status }}</td>
                @else
                <td class="">N/A</td>
                @endif
                @endif
                <td class="rightalign">
                  <button type="button" class="btn btn-xs btn-view btn-table btn-view" title=""View><i class="fa fa-eye"></i></button>
                  @if (Auth::user()->usertype_id == 2)
                  <button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                  <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        @endif
        </div>
      </div>           
    </div>
    @if (Auth::user()->usertype_id != 8)
      <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
        <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
          <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="50">100</option>
          </select>
        </div>
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>
        <div class="col-xs-12" style="color: red; font-weight: bold;">
          <small>*for <u>REJECTED</u> Cases, please see Admin</small>
        </div>
      </div>
    @else
      @if ($supervisor)
      <div class="hide">
        <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
          <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
            <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="50">100</option>
            </select>
          </div>
          <div class="text-center">
            <input type="hidden" id="row-page" value="1">
            <input type="hidden" id="row-sort" value="">
            <input type="hidden" id="row-order" value="">
          </div>
          <div class="col-xs-12" style="color: red; font-weight: bold;">
            <small>*for <u>REJECTED</u> Cases, please see Admin</small>
          </div>
      </div>
      @else
      <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
        <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
          <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="50">100</option>
          </select>
        </div>
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>
        <div class="col-xs-12" style="color: red; font-weight: bold;">
          <small>*for <u>REJECTED</u> Cases, please see Admin</small>
        </div>
      @endif
    @endif

    </div>
    <!-- /#page-content-wrapper -->

@include('policy-management.production.gi-case-submission.form')

@stop