@extends('layouts.master')

@section('scripts')
  <script>

  function refresh() {
 $_token = '{{ csrf_token() }}';
      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");

      $.post("{{ url('policy/production/case-inception-advisor/refresh') }}", { _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        $('#my-table').dataTable().fnClearTable();

        $.each(response.rows, function(index, row) {
          if (row.status != 2) {
            table.append(
              '<tr data-id="' + row.id +'">' +
                '<td>' + row.user_id + '</td>' +
                '<td>' + row.firstname + row.lastname +'</td>' +
                '<td>' + row.annual_income_range + '</td>' +
                '<td>' + row.life_firstname + row.life_lastname +'</td>' + 
                '<td>' + row.type_of_plan + '</td>' +
                '<td>' + row.policy_term + '</td>' + 
                '<td>' + row.mode_of_payment + '</td>' + 
                '<td>' + row.cka + '</td>' + 
                '<td class="rightalign">' +
                  '<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-decept"><i class="fa fa-check"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                  '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">' +
                '</td>' +
              '</tr>'
            );
          }
        });

        $(".tablesorter").trigger('update');

        // populate cache
        $('#my-table').find('tbody tr').each(function() {
          $('#my-table').dataTable().fnAddData(this);
        });

      loading.addClass("hide");
      }, 'json');

}

  $(document).ready(function() {
      //add user
      $(".btn-add").click(function() {

        // reset all form fields
        $("#modal-production").find('form').trigger("reset");
        $("#row-id").val("");

        $("#modal-production").find('.hide-view').removeClass('hide');
        $("#modal-production").find('.show-view').addClass('hide');
        $("#modal-production").find('.preview').addClass('hide');
        $("#modal-production").find('input').removeAttr('readonly', 'readonly');

        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
        $("#modal-production").modal('show');

      });


      $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          // reset all form fields
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-production").find('form').trigger("reset");
          $("#row-id").val("");


              $.post("{{ url('policy/production/case-inception-advisor/view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                  console.log(response);
                  
                      $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View Production</strong>');
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);

                          if (index = "get_case_policy") {
                            if (response.get_case_policy != null) {
                              $.each(response.get_case_policy, function(index2, value2) {
                                var field2 = $("#row-" + index2);
                                if(field2.length > 0) {
                                    field2.val(value2);
                                }
                              });
                            }
                          }
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                          }
                      });

                      $("#modal-production").find('.hide-view').addClass('hide');
                      $("#modal-production").find('.preview').removeClass('hide');
                      $("#modal-production").find('.change').addClass('hide');
                      $("#modal-production").find('.show-view').removeClass('hide');
                      $("#modal-production").find('input').attr('readonly', 'readonly');
                      // show form
                      $("#modal-production").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
      });

      //edit cases
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        // reset all form fields
        $("#modal-production").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";


        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

              $.post("{{ url('policy/production/case-inception-advisor/view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                  console.log(response);
                  
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Production Case</strong>');
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);

                          if (index = "get_case_policy") {
                            if (response.get_case_policy != null) {
                              $.each(response.get_case_policy, function(index2, value2) {
                                var field2 = $("#row-" + index2);
                                if(field2.length > 0) {
                                    field2.val(value2);
                                }
                              });
                            }
                          }
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                          }
                      });

                      $("#modal-production").find('.hide-view').removeClass('hide');
                      $("#modal-production").find('.show-view').addClass('hide');
                      $("#modal-production").find('input').removeAttr('readonly');
                      // show form
                      $("#modal-production").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

       $(function () {
        $('.state_from_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.state_to_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.transaction_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.incept_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.inst_from_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.policy_exp_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.comm_run_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

       $(function () {
        $('.issue_date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected provider/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
        console.log('multiple');

        
        if (ids.length == 0) {
          status("Error", "Select providers first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('provider/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('policy/production/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });

      // decept case
      $("#page-content-wrapper").on("click", ".btn-decept", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Inception', 'Are you sure you want to decept <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/decept') }}", id);
      });

      // disable case
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/disable') }}", id);
      });

      // enable case
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Case Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('policy/production/case-submission-advisor/enable') }}", id);
      });

      // show modal for multiple inception
      $("#page-content-wrapper").on("click", ".btn-decept-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Inception");
        $(".modal-multiple-body").html("Are you sure you want to re-submit the selected case/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });
        $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
        console.log('multiple');

        
        if (ids.length == 0) {
        } else {

          if ($("#row-multiple-hidden").val() == 0) {
            $.post("{{ url('policy/production/case-submission-advisor/decept-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });


    });

  </script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">POLICY MANAGEMENT</h3>
        <h5 class="bread-crumb">PRODUCTION > CASE SUBMISSION </h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
          <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-decept-select"><i class="fa fa-check"></i> Re-submit</button>     
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
<form class="form-horizontal" >     
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH OPTION</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class=" form-control borderzero" id="sel1" >
                              <option>All Providers</option>
                              <option>A</option>
                              <option>B</option>
                              <option>C</option>
                              <option>D</option>
                            </select>
                          </div>
                      </div>
                  </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                        <input type="text" class="form-control borderzero" placeholder="Search for..." id="search">
                        <span class="input-group-btn">
                          <button class="btn btn-default borderzero" type="button"><i class="fa fa-eraser"></i> <span class="hidden-xs">CLEAR</span></button>
                        </span>
                      </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
      <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
          <div class="table-responsive block-content tbblock">
            <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
            <table class="table table-striped" id="tblData">
              <thead class="tbheader">
                  <tr>
                    <th nowrap><i class="fa fa-sort"></i> AGENT NAME</th>
                    <th nowrap><i class="fa fa-sort"></i> NAME</th>
                    <th nowrap><i class="fa fa-sort"></i> ANNUAL INCOME</th>
                    <th nowrap><i class="fa fa-sort"></i> NAME (LIFE)</th>
                    <th nowrap><i class="fa fa-sort"></i> TYPE OF PLAN</th>
                    <th nowrap><i class="fa fa-sort"></i> POLICY TERM</th>
                    <th nowrap><i class="fa fa-sort"></i> MODE OF PAYMENT</th>
                    <th nowrap><i class="fa fa-sort"></i> CKA</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody id="rows">
                   @foreach($rows as $row)
                  @if ($row->status != 2)
                  <tr data-id="{{ $row->id }}">
                    <td>{{ $row->getUserSubmission->name }}</td>
                    <td>{{ $row->firstname }} {{ $row->lastname}}</td>
                    <td>{{ $row->annual_income_range }}</td>
                    <td>{{ $row->life_firstname}} {{ $row->life_firstname}}</td>
                    <td>{{ $row->type_of_plan }}</td>
                    <td>{{ $row->policy_term }}</td>
                    <td>{{ $row->mode_of_payment }}</td>
                    <td>{{ $row->cka }}</td>
                    <td class="rightalign">
                        <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-eye"></i></button>
                        <button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>
                        <button type="button" class="btn btn-xs btn-table btn-incept"><i class="fa fa-check"></i></button> 
                        <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}"><i class="fa fa-adjust"></i></button>
                        <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                      </td>
                  </tr>
                  @endif
                  @endforeach
                </tbody>
              </table>
          </div>           
      </div>
    </div>
    <!-- /#page-content-wrapper -->

@include('policy-management.production.case-inception-advisor.form')
@stop