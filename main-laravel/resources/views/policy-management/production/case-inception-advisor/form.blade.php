<!-- modal add product -->
<div class="modal fade" id="modal-production" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog modal-lg">
<div class="modal-content borderzero">
      <div id="load-form" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
    {!! Form::open(array('url' => 'policy/production/case-inception-advisor/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_production')) !!}
    <div class="modal-header modal-warning">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-body">
    <div id="production-notice"></div>  
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
    <label for="row-state_from_date" class="col-sm-3 col-xs-5"><small>STATEMENT FROM DATE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date state_from_date">
          <input class="form-control" id="row-state_from_date" name="state_from_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="row-state_to_date" class="col-sm-3 col-xs-5"><small>STATEMENT TO DATE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date state_to_date">
          <input class="form-control" id="row-state_to_date" name="state_to_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="row-transaction_date" class="col-sm-3 col-xs-5"><small>TRANSACTION CODE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date transaction_date">
          <input class="form-control" id="row-transaction_date" name="transaction_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>AGENT CODE</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="agent_code" class="form-control borderzero" id="row-agent_code" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>CONTRACT NO</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="contract_no" class="form-control borderzero" id="row-contract_no" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>POLICY TYPE</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="policy_type" class="form-control borderzero" id="row-policy_type" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>COMPONENT CODE</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="compo_code" class="form-control borderzero" id="row-compo_code" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>POLICY TERM</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="policy_term" class="form-control borderzero" id="row-policy_term" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>CONTRACT CURRENCY</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="contract_currency" class="form-control borderzero" id="row-contract_currency" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>SUM INSURED</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="sum_insured" class="form-control borderzero" id="row-sum_insured" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="row-incept_date" class="col-sm-3 col-xs-5"><small>INCEPT DATE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date incept_date">
          <input class="form-control" id="row-incept_date" name="incept_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="row-inst_from_date" class="col-sm-3 col-xs-5"><small>INSTALLMENT FROM DATE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date inst_from_date">
          <input class="form-control" id="row-inst_from_date" name="inst_from_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>BILLING FREQUENCY</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="billing_freq" class="form-control borderzero" id="row-billing_freq" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="row-policy_exp_date" class="col-sm-3 col-xs-5"><small>POLICY EXPIRY DATE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date policy_exp_date">
          <input class="form-control" id="row-policy_exp_date" name="policy_exp_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>GROSS PREMIUM PAID</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="gross_prem_paid" class="form-control borderzero" id="row-gross_prem_paid" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>GROSS FEE AMOUNT</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="gross_fee_amt" class="form-control borderzero" id="row-gross_fee_amt" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>NET PREMIUM PAID</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="net_prem_paid" class="form-control borderzero" id="row-net_prem_paid" placeholder="">
    </div>
    </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>NET FEE AMOUNT</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="net_fee_amt" class="form-control borderzero" id="row-net_fee_amt" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>GROSS PREMIUM GST AMOUNT</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="gross_prem_gst_amt" class="form-control borderzero" id="row-gross_prem_gst_amt" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>NET PREMIUM GST AMOUNT</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="net_prem_gst_amt" class="form-control borderzero" id="row-net_prem_gst_amt" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>PREMIUM W/O COM</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="prem_wo_commission" class="form-control borderzero" id="row-prem_wo_commission" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>PREMIUM CONV RATE</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="prem_conv_rate" class="form-control borderzero" id="row-prem_conv_rate" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>FEE CONV RATE</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="fee_conv_rate" class="form-control borderzero" id="row-fee_conv_rate" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>PAYMENT CURRECY</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="payment_currency" class="form-control borderzero" id="row-payment_currency" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>COMMISSION OR</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="comm_or" class="form-control borderzero" id="row-comm_or" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>COMMISSION ADJ AMT</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="comm_adj_amt" class="form-control borderzero" id="row-comm_adj_amt" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="row-comm_run_date" class="col-sm-3 col-xs-5"><small>COMMISSION RUN DATE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date comm_run_date">
          <input class="form-control" id="row-comm_run_date" name="comm_run_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>COMMISSION CONV RATE</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="comm_conv_rate" class="form-control borderzero" id="row-comm_conv_rate" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>PREMIUM TERM</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="premium_term" class="form-control borderzero" id="row-premium_term" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="row-issue_date" class="col-sm-3 col-xs-5"><small>ISSUE DATE</small></label>
    <div class="col-sm-9 col-xs-7">
        <div class="input-group date issue_date">
          <input class="form-control" id="row-issue_date" name="issue_date" type="text">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
        </div>
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>ADJUSTMENT AMOUNT</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="adj_amt" class="form-control borderzero" id="row-adj_amt" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>FUND CODE</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="fund_code" class="form-control borderzero" id="row-fund_code" placeholder="">
    </div>
    </div>
    <div class="form-group">
    <label for="email" class="col-sm-3 col-xs-5"><small>TRAILER FEE AMOUNT</small></label>
    <div class="col-sm-9 col-xs-7">
    <input type="text" name="trailer_fee_amt" class="form-control borderzero" id="row-trailer_fee_amt" placeholder="">
    </div>
    </div>
    </div>    
    </div>   
    <div class="clearfix"></div>
    <div class="modal-footer borderzero">
      <input type="hidden" name="id" id="row-id">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
    </div>
    {!! Form::close() !!}
  </div>
</div>
</div>
<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
