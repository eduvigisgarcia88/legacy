@extends('layouts.master')

@section('scripts')
<script type="text/javascript">
// submit form
    $("#form-settings").find("form").submit(function(e) {
   
      var form = $("#form-settings").find("form");
      var loading = $("#load-settings");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#settings-notice');
        } else {
          //$("#modal-designate").modal('hide');
          $("#settings-notice").html("");
          status(response.title, response.body, 'alert-success');
          // refresh the list
          refreshPayrollRates();
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

   $("#form-settings-rate").find("form").submit(function(e) {
   
      var form = $("#form-settings-rate").find("form");
      var loading = $("#load-settings-rate");
      // console.log("yey");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');

      // console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#settings-notice-rate');
        } else {
          $("#settings-notice-rate").html("");
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refreshRate();
        }

        loading.addClass('hide');

      }
      });
    });
   $("#form-settings-tiers").find("form").submit(function(e) {
   
      var form = $("#form-settings-tiers").find("form");
      var loading = $("#load-settings-tiers");

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');

      // console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#settings-notice');
        } else {
          $("#settings-notice").html("");
          status(response.title, response.body, 'alert-success');

          // refresh the list
         refreshTiers();
        }

        loading.addClass('hide');

      }
      });
    });

    $("#form-settings-cpd").find("form").submit(function(e) {
   
      var form = $("#form-settings-cpd").find("form");
      var loading = $("#load-settings-cpd");

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');

      // console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#settings-notice');
        } else {
          $("#settings-notice").html("");
          status(response.title, response.body, 'alert-success');

          // refresh the list
         refreshTiers();
        }

        loading.addClass('hide');

      }
      });
    });

    $("#form-settings-gi").find("form").submit(function(e) {
   
      var form = $("#form-settings-gi").find("form");
      var loading = $("#load-settings-gi");

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');

      // console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#settings-notice');
        } else {
          $("#settings-notice").html("");
          status(response.title, response.body, 'alert-success');

          // refresh the list
         refreshTiers();
        }

        loading.addClass('hide');

      }
      });
    });

$(document).ready(function() {
    
    $("#select_all").click(function() {
      console.log($(this).attr("checked") ? 1 : 0);
      if($(this).is(':checked')==true) { 
        console.log('checked');
       $("#form-settings-rate").find("input[type='checkbox']").prop('checked', true);
      } else {
        console.log('unchecked');
       $("#form-settings-rate").find("input[type='checkbox']").prop('checked', false);
      }
      // if ($(this).)
      // if ($(this).val() == true) {
      // } else {
      // }
    });

});

function refresh() {
      $_token = '{{ csrf_token() }}';
      var loading = $(".loading-pane");
      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { _token: $_token }, function(response) {
       
        // output form data
          $.each(response.rows, function(index, value) {
              var field = $("#row-" + index);
              if(field.length > 0) {
                  field.val(value);
                  // console.log(field+"="+value);
              }
         });       
        loading.addClass("hide");

      }, 'json');
    }

function refreshRate() {
  $_token = '{{ csrf_token() }}';
  var loading = $("#load-settings-rate");
  loading.removeClass("hide");

  $.post("{{ $refresh_route_rate }}", { _token: $_token }, function(response) {
   
    // output form data
      $.each(response.rows, function(index, value) {
          var field = $("#row-" + index);
          if(field.length > 0) {
              field.val(value);
              
          }

     });       
    loading.addClass("hide");

  }, 'json');
}

function refreshTiers() {
  $_token = '{{ csrf_token() }}';
  var loading = $("#load-settings-rate");
  loading.removeClass("hide");

  $.post("{{ $refresh_route_tier }}", { _token: $_token }, function(response) {
   
    // output form data
      $.each(response.rows, function(index, value) {
          var field = $("#row-" + index);
          if(field.length > 0) {
              field.val(value).toFixed(2);
              
          }

     });       
    loading.addClass("hide");

  }, 'json');
}

function refreshPayrollRates() {
  $_token = '{{ csrf_token() }}';
  var loading = $("#load-settings-rate");
  loading.removeClass("hide");

  $.post("{{ url('settings/refresh/payroll-rates') }} ", { _token: $_token }, function(response) {
   
    // output form data
      $.each(response.rows, function(index, value) {

          var field = $("#row-" + value.id);
          if(field.length > 0) {
              field.val((value.rate).toFixed(2));
              // console.log(value.rate);
          }

     });       
    loading.addClass("hide");

  }, 'json');


  $.post("{{ url('settings/refresh/designation-rates') }} ", { _token: $_token }, function(response) {
      
    // output form data
      $.each(response.rows, function(index, value) {
          var field = $("#row-" + value.designation_name);

          if(field.length > 0) {
              field.val((value.rate).toFixed(2));
              //console.log(value.designation_name+"="+(value.rate).toFixed(2));
              
          }

     });       
    //loading.addClass("hide");

  }, 'json');

  $.post("{{ url('settings/refresh/cpd') }} ", { _token: $_token }, function(response) {
      
    // output form data
      $.each(response.rows, function(index, value) {
          var field = $("#row-" + value.designation_name);

          if(field.length > 0) {
              field.val((value.rate).toFixed(2));
              //console.log(value.designation_name+"="+(value.rate).toFixed(2));
              
          }

     });       
    //loading.addClass("hide");

  }, 'json');
}     
</script>
@stop

@section('content')


<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
    <h3 class="main-title">SETTINGS</h3>
    <h5 class="bread-crumb"></h5>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
  </div>
</div>

<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#rate" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>DEFAULT RATE SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#rate" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="rate" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">    
    <div class="modal-body">
      <p><strong>SALES USERS SETTINGS</strong></p>
      <div id="form-settings">
        <div id="load-settings" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
         {!! Form::open(array('url' => 'settings/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_settings')) !!}
          <div id="settings-notice"></div>
          @foreach($designation_rates as $row)
          <div class="form-group">
              <label for="row-partner" class="col-sm-3 col-xs-5 control-label font-color">{{$row->designation_full}} ({{$row->designation_name}})</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="designation_value[{{$row->id}}]" id="row-{{$row->designation_name}}" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($row->rate, 2, '.', '') }}">
            <input type="hidden" class="form-control" name="designation_id[{{$row->id}}]" id="row-{{$row->designation_name}}" maxlength="2" aria-describedby="basic-addon2" value="{{$row->id}}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          @endforeach
          <!-- <div class="form-group">
              <label for="row-FSD" class="col-sm-3 col-xs-5 control-label font-color">Financial Services Director(FSD)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="FSD" id="row-FSD" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->FSD, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-AFSD" class="col-sm-3 col-xs-5 control-label font-color">Assistant Financial Services Director(AFSD)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="AFSD" id="row-AFSD" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->AFSD, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-FSM" class="col-sm-3 col-xs-5 control-label font-color">Financial Services Manager(FSM)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="FSM" id="row-FSM" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->FSM, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-FSC" class="col-sm-3 col-xs-5 control-label font-color">Financial Services Consultant(FSC)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="FSC" id="row-FSC" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->FSC, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-EFSC" class="col-sm-3 col-xs-5 control-label font-color">Executive Financial Services Consultant(EFSC)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="EFSC" id="row-EFSC" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->EFSC, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-PFSC" class="col-sm-3 col-xs-5 control-label font-color">Premier Financial Services Consultant(PFSC)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="PFSC" id="row-PFSC" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->PFSC, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-SFSC" class="col-sm-3 col-xs-5 control-label font-color">Senior Financial Services Consultant(SFSC)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="SFSC" id="row-SFSC"placeholder="10" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->SFSC, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-FSC_Elite" class="col-sm-3 col-xs-5 control-label font-color">Financial Services Consultant-Elite(FSC-Elite)</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="FSC_Elite" id="row-FSC_Elite"placeholder="10" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($default->FSC_Elite, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div> -->
           <hr>
          <p><strong>PAYROLL SETTINGS</strong></p>

           @foreach($payroll_rates as $row)
           <div class="form-group">
                <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">{{ $row->name }}</label>
            <div class="input-group col-sm-9 col-xs-7">
               <input type="text" class="form-control" name="payroll_rate_value[{{$row->id}}]" id="row-{{$row->id}}" maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($row->rate, 2, '.', '') }}">
               <input type="hidden" class="form-control" name="payroll_rate_id[{{$row->id}}]" id="row-{{$row->id}}" maxlength="2" aria-describedby="basic-addon2" value="{{$row->id}}">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
           </div>
           @endforeach
          <hr>
          <p><strong>INTRODUCERS SETTINGS</strong></p>
          <div class="form-group">
              <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">Introducer</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" aria-describedby="basic-addon2">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <hr>
          <p><strong>PRODUCT SETTINGS</strong></p>
          <div class="form-group">
              <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">Product</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" aria-describedby="basic-addon2">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <hr>
          <p><strong>GST SETTINGS</strong></p>
          <div class="form-group">
              <label for="row-GST" class="col-sm-3 col-xs-5 control-label font-color">Rate</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" id="row-GST" class="form-control" name="GST" aria-describedby="basic-addon2" value="{{ number_format($default->GST, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <button type="submit" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-save"></i> SAVE</button>   
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#tier" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>BANDING RANK SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#tier" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="tier" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">    
    <div class="modal-body">
      <div id="form-settings-tiers">
        <div id="load-settings-tiers" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
        {!! Form::open(array('url' => 'settings/tierSave', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_settings')) !!}
          <div id="settings-notice"></div>
          @foreach($tiers as $row)

          <div class="form-group">
              <label for="row-partner" class="col-sm-3 col-xs-5 control-label font-color">{{$row->name}}</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="tier_value[{{$row->id}}]"  maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($row->rate, 2, '.', '') }}">
            <input type="hidden" class="form-control" name="tier_id[{{$row->id}}]" maxlength="2" aria-describedby="basic-addon2" value="{{$row->id}}">
            <input type="hidden" class="form-control" name="tier_code[{{$row->id}}]" maxlength="2" aria-describedby="basic-addon2" value="{{$row->id}}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          @endforeach
         
          <button type="submit" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-save"></i> SAVE</button>   
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#cpd" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>CPD HOURS SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#cpd" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="cpd" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">    
    <div class="modal-body">
      <div id="form-settings-cpd">
        <div id="load-settings-cpd" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
        {!! Form::open(array('url' => 'settings/cpdSave', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_settings')) !!}
          <div id="settings-notice"></div>
          @foreach($cpd as $row)

          <div class="form-group">
              <label for="row-partner" class="col-sm-3 col-xs-5 control-label font-color">{{$row->name}}</label>
          <div class="input-group col-sm-9 col-xs-7">
            <input type="text" class="form-control" name="cpd_value[{{$row->id}}]"  maxlength="2" aria-describedby="basic-addon2" value="{{ number_format($row->no_of_hours, 2, '.', '') }}">
            <input type="hidden" class="form-control" name="cpd_id[{{$row->id}}]" maxlength="2" aria-describedby="basic-addon2" value="{{$row->id}}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          @endforeach
         
          <button type="submit" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-save"></i> SAVE</button>   
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordiongi" href="#gi" aria-expanded="false" aria-controls="collapseGI">
  <p><strong>GI BANDING RANK SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordiongi" href="#gi" aria-expanded="false" aria-controls="collapseGI"></i>
</div>
<div id="gi" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">    
    <div class="modal-body">
      <div id="form-settings-gi">
        <div id="load-settings-gi" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
        {!! Form::open(array('url' => 'settings/giSave', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_settings')) !!}
          <div id="settings-notice"></div>
          @foreach($gis as $row)

          <div class="form-group">
              <label for="row-partner" class="col-sm-3 col-xs-5 control-label font-color"> LEGACY FA Rank {{$row->rank}}</label>
          <div class="input-group col-sm-3 col-xs-7">
            <input type="text" class="form-control" name="legacy_fa[{{$row->id}}]" aria-describedby="basic-addon2" value="{{ number_format($row->legacy_fa, 2, '.', '') }}">
            <input type="hidden" class="form-control" name="gi_id[{{$row->id}}]" maxlength="2" aria-describedby="basic-addon2" value="{{$row->id}}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-partner" class="col-sm-3 col-xs-5 control-label font-color"> MGR Rank {{$row->rank}}</label>
          <div class="input-group col-sm-3 col-xs-7">
            <input type="text" class="form-control" name="mgr[{{$row->id}}]" aria-describedby="basic-addon2" value="{{ number_format($row->mgr, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>

          <div class="form-group">
              <label for="row-partner" class="col-sm-3 col-xs-5 control-label font-color"> FSD Rank {{$row->rank}}</label>
          <div class="input-group col-sm-3 col-xs-7">
            <input type="text" class="form-control" name="fsd[{{$row->id}}]" aria-describedby="basic-addon2" value="{{ number_format($row->fsd, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <div class="form-group">
              <label for="row-partner" class="col-sm-3 col-xs-5 control-label font-color"> Agent Rank {{$row->rank}}</label>
          <div class="input-group col-sm-3 col-xs-7">
            <input type="text" class="form-control" name="agents[{{$row->id}}]" aria-describedby="basic-addon2" value="{{ number_format($row->agents, 2, '.', '') }}">
            <span class="input-group-addon" id="basic-addon2">%</span>
          </div>
          </div>
          <hr>
          @endforeach
         
          <button type="submit" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-save"></i> SAVE</button>   
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- <div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#rate" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>TIER SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#tier" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="tier" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">    
<div class="modal-body">
  <div class="form-group">
      <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">Management Share 1</label>
  <div class="input-group col-sm-9 col-xs-7">
    <input type="text" class="form-control" placeholder="10" aria-describedby="basic-addon2">
    <span class="input-group-addon" id="basic-addon2">%</span>
  </div>
  </div>
  <div class="form-group">
      <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">Management Share 2</label>
  <div class="input-group col-sm-9 col-xs-7">
    <input type="text" class="form-control" placeholder="10" aria-describedby="basic-addon2">
    <span class="input-group-addon" id="basic-addon2">%</span>
  </div>
  </div>
  <div class="form-group">
      <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">Management Share 3</label>
  <div class="input-group col-sm-9 col-xs-7">
    <input type="text" class="form-control" placeholder="10" aria-describedby="basic-addon2">
    <span class="input-group-addon" id="basic-addon2">%</span>
  </div>
  </div>
</div>
<button type="" href="" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-save"></i> SAVE</button>   
</div>
</div> -->


<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#currency" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>CURRENCY SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#currency" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="currency" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
<div class="modal-body">
  <div id="form-settings-rate" class="col-xs-12">
    <div id="load-settings-rate" class="loading-pane hide">
      <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
    </div>
    <div id="settings-notice-rate"></div>
      {!! Form::open(array('url' => 'settings/currencysave', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_currency_settings')) !!}
        <div class="col-lg-12 text-right">

          <label><input type="checkbox" id="select_all" {{ $select_all }}> Select all</label>
        </div>
        <div class="row">
     <?php
         $i = 0;
         foreach($rate as $row){
           if($i == 3){
               $i = 0;
              ?>
                  </div><div class="row">
                <?php
            } 
                ?>
            <div class="col-sm-4">
               <div class="form-group">
            <label for="row-AED" class="col-sm-3 col-xs-5 control-label font-color text-right"><input type="checkbox" value="2" name="status[{{$row->id}}]" {{ ($row->status == 2 ? 'checked="checked"' : '') }}> {{$row->code}}</label>
        <div class="input-group col-sm-9 col-xs-7">
          <input type="text" class="form-control" name="rate_value[{{$row->id}}]" maxlength="5" aria-describedby="basic-addon2" value="{{$row->rate}}">
          <input type="hidden" class="form-control" name="rate_id[{{$row->id}}]" maxlength="5" aria-describedby="basic-addon2" value="{{$row->id}}">
          <input type="hidden" class="form-control" name="rate_code[{{$row->id}}]" maxlength="5" aria-describedby="basic-addon2" value="{{$row->code}}">
        </div>
        </div>
            </div>
          <?php
            $i++;
         }
        ?>
        </div>
        <button type="submit" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-save"></i> SAVE</button>   
      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- <div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#email" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>EMAIL SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#email" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="email" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
  <div class="content-table table-responsive container-fluid" >     
  <table class="table table-striped">
  <thead class="tbheader">
    <tr>
      <th><i class="fa fa-calendar"></i> DATE ASSIGNED</th>
      <th><i class="fa fa-sort"></i> TEAM CODE</th>
      <th><i class="fa fa-sort"></i> USER ID</th>
      <th><i class="fa fa-sort"></i> USER TYPE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>October 11, 2015</td>
      <td>Team001</td>
      <td>User001</td>
      <td>Assistant Director</td>
    </tr>
    <tr>
      <td>September 3, 2015</td>
      <td>Team001</td>
      <td>User007</td>
      <td>Manager</td>
    </tr>
    <tr>
      <td>September 11, 2015</td>
      <td>Team002</td>
      <td>User003</td>
      <td>Sales Assistant</td>
    </tr>
  </tbody>
</table>
<button type="" href="" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-group"></i> Supervisor</button>   
</div>
</div>
</div>

<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#report" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>REPORT SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#report" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="report" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
  <div class="content-table table-responsive container-fluid" >     
  <table class="table table-striped">
  <thead class="tbheader">
    <tr>
      <th><i class="fa fa-calendar"></i> DATE ASSIGNED</th>
      <th><i class="fa fa-sort"></i> TEAM CODE</th>
      <th><i class="fa fa-sort"></i> USER ID</th>
      <th><i class="fa fa-sort"></i> USER TYPE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>October 11, 2015</td>
      <td>Team001</td>
      <td>User001</td>
      <td>Assistant Director</td>
    </tr>
    <tr>
      <td>September 3, 2015</td>
      <td>Team001</td>
      <td>User007</td>
      <td>Manager</td>
    </tr>
    <tr>
      <td>September 11, 2015</td>
      <td>Team002</td>
      <td>User003</td>
      <td>Sales Assistant</td>
    </tr>
  </tbody>
</table>
<button type="" href="" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-group"></i> Supervisor</button>   
</div>
</div>
</div>

<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-6 col-xs-6" data-toggle="collapse" data-parent="#accordion3" href="#server" aria-expanded="false" aria-controls="collapseTwo">
  <p><strong>SERVER SETTINGS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#server" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="server" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
  <div class="content-table table-responsive container-fluid" >     
  <table class="table table-striped">
  <thead class="tbheader">
    <tr>
      <th><i class="fa fa-calendar"></i> DATE ASSIGNED</th>
      <th><i class="fa fa-sort"></i> TEAM CODE</th>
      <th><i class="fa fa-sort"></i> USER ID</th>
      <th><i class="fa fa-sort"></i> USER TYPE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>October 11, 2015</td>
      <td>Team001</td>
      <td>User001</td>
      <td>Assistant Director</td>
    </tr>
    <tr>
      <td>September 3, 2015</td>
      <td>Team001</td>
      <td>User007</td>
      <td>Manager</td>
    </tr>
    <tr>
      <td>September 11, 2015</td>
      <td>Team002</td>
      <td>User003</td>
      <td>Sales Assistant</td>
    </tr>
  </tbody>
</table>
<button type="" href="" class="btn btn-success borderzero btn-sm btn-tool pull-right "><i class="fa fa-group"></i> Supervisor</button>   
</div>
</div>
</div> -->

</div>
@stop