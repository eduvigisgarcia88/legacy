@extends('layouts.error')

@section('content')
    <!-- Page Content -->
    <div id="page-content-wrapper" class="response">
	    <div class="container-fluid">
	    	<h2>Page not found</h2>
	    </div>
    </div>
    
@stop