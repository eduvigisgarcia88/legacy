@extends('layouts.master')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';
  function refresh() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');

    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();
    $status = $("#row-filter_status").val();

        $.post("{{ url('reports/ongoing-fees/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
          
        }, 'json');
  }

  function search() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();
    $status = $("#row-filter_status").val();

    $("#row-export").val($status);

        $.post("{{ url('reports/ongoing-fees/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
            
        }, 'json');
  }

  $(document).ready(function() {

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      console.log('asd');
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();
        $status = $("#row-filter_status").val();

        $.post("{{ url('reports/ongoing-fees/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
            
        }, 'json');
      }
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();
        $status = $("#row-filter_status").val();

        $.post("{{ url('reports/ongoing-fees/refresh') }}", { page: $page, per: $per, sort: $sort, order: $order, search: $search, status: $status, _token: $_token }, function(response) {
          var rows = '';

          $('#rows').html(response.paginate);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
            
        }, 'json');

    });

  });

$('#row-search').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        search();
    }
});

</script>

@stop


@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">{{ $breadcrumb }}</h3>
        <h5 class="bread-crumb">SUMMARY AND REPORTS</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('reports/ongoing-fees/export') }}">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $export }}" name="export" id="row-export">
            <input type="hidden" value="{{ $breadcrumb }}" name="fees" id="row-fees">
            <button type="submit" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-export"><i class="fa fa-print"></i> Export</button>
          </form>
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>BATCH NAME</strong></p></label>
              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <select class=" form-control borderzero" onchange="search()" id="row-filter_status" >
                        <option value="">All</option>
                      @foreach ($batches as $row)
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                      @endforeach
                    </select>
                  </div>
            </div>
          </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                        <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                        </span>
                      </div>
                      </div>
                    </div>
                  </div>
      </div>
          <div class="container-fluid content-table col-xs-12" style="border-top:1px #e6e6e6 solid;">
            <div class="table-responsive block-content tbblock">
                <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div> 
            <div class="table-responsive">
              <table class="table table-striped table-pagination">
                <thead class="tbheader">
                  <tr>
                    <th class="th-sort" nowrap data-sort="salesforceid"><i></i> SALESFORCE ID</th>
                    <th class="th-sort" nowrap data-sort="agent_name"><i></i> AGENT NAME</th>
                    <th class="th-sort" nowrap data-sort="policy_owner"><i></i> POLICY OWNER</th>
                    <th class="th-sort" nowrap data-sort="premium"><i></i> PREMIUM</th>
                    <th class="th-sort" nowrap data-sort="revenue"><i></i> REVENUE</th>
                    <th class="th-sort" nowrap data-sort="month_id"><i></i> MONTH</th>
                    <th class="th-sort" nowrap data-sort="inception_date"><i></i> INCEPTION DATE</th>
                  </tr>
                </thead>
                <tbody id="rows">
                  @foreach($rows as $row)
                  <tr data-id="{{ $row->id }}">
                    <td>{{ strtoupper($row->salesforceid) }}</td>
                    <td>{{ strtoupper($row->agent_name) }}</td>
                    <td>{{ strtoupper($row->policy_owner) }}</td>
                    <td class="rightalign">{{ number_format($row->premium, 2) }}</td>
                    <td class="rightalign">{{ number_format($row->revenue, 2) }}</td>
                    <td>{{ ($row->month ? $row->month : '') }}</td>
                    <td>{{ ($row->inception_date ? date_format(date_create(substr($row->inception_date, 0,10)),'m/d/Y') : '') }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>
    </div>
      <div id="row-pages" class="col-lg-6 text-center borderzero leftalign">{!! $pages !!}</div>
      <div class="col-lg-6 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
      </select>
      </div>
      <div class="text-center">
        <input type="hidden" id="row-page" value="1">
        <input type="hidden" id="row-sort" value="">
        <input type="hidden" id="row-order" value="">
      </div>
    </div>

@stop