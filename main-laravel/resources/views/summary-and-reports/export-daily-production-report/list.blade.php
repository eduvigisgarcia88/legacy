@extends('layouts.master')

@section('content')


@include('summary-and-reports.export-daily-production-report.form')

<!-- Page Content -->
        <div id="page-content-wrapper" class="response">
        <!-- Title -->
        <div class="container-fluid main-title-container container-space">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
                <h3 class="main-title">SUMMARY AND REPORTS</h3>
                <h5 class="bread-crumb">SUMMARY AND REPORTS > VIEW FIRM REVENUE SUMMARY <h5>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
                  <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i> Disable</button>     
                  <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-minus"></i> Remove</button>
                  <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaledit"><i class="fa fa-pencil"></i> Edit</button>
                  <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add</button>
                         
            </div>
            </div>
            <div id="basicinfo" class="container-fluid default-container container-space">
            <div class="col-lg-12">
                  <p><strong>BASIC INFORMATION</strong></p>
            </div>
                  <form class="form-horizontal" >     
                      <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                  <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>AGENT NAME</strong></p></div>
                                      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                          <p class="p">Steve Jobs</p>
                                      </div>
                            </div>
                      </div>
                      <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                  <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>DESIGNATION</strong></p></div>
                                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                              <p class="p">Sales Agent</p>
                                        </div>
                                  </div>
                            </div>
                      <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" >
                            <div class="form-group">
                                  <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>AGENT ID</strong></p></div>
                                          <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                              <p class="p">12345</p>
                                          </div>
                                  </div>
                            </div>
                  </form>
              </div>
              <div class="container-fluid default-container container-space">
              <div class="col-lg-12">
                    <p><strong>FILTER OPTIONS</strong></p>
              </div>
                  <form class="form-horizontal" >     
                      <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                          <div class="form-group">
                                <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PROVIDER</strong></p></label>
                                        <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                              <select class="select form-control borderzero" id="sel1">
                                                <option>All Providers</option>
                                                <option>A</option>
                                                <option>B</option>
                                                <option>C</option>
                                                <option>D</option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD FROM</strong></p></label>
                                            <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                              <select class="select form-control borderzero" id="sel1" >
                                                <option>Jan 2013</option>
                                                <option>Feb 2013</option>
                                                <option>Mar 2013</option>
                                                <option>Apr 2013</option>
                                                <option>May 2013</option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                                        <div class="form-group">
                                            <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD TO</strong></p></label>
                                            <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                              <select class="select form-control borderzero" id="sel1" >
                                                <option>Mar 2015</option>
                                                <option>Apr 2015</option>
                                                <option>May 2015</option>
                                                <option>Jun 2015</option>
                                                <option>Jul 2015</option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    </div>
<div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
    <div class="table-responsive block-content tbblock">
        <table class="table" id="tblData">
            <thead class="tbheader">
                <tr>
                  <th>PHOTO</th>
                  <th><i class="fa fa-sort"></i> AGENT ID</th>
                  <th><i class="fa fa-sort"></i> AGENT CODE</th>
                  <th><i class="fa fa-sort"></i> USER NAME</th>
                  <th><i class="fa fa-sort"></i> AGENT REAL NAME</th>
                  <th><i class="fa fa-sort"></i> DESIGNATION</th>
                  <th><i class="fa fa-sort"></i> EMAIL</th>
                  <th><i class="fa fa-sort"></i> MOBILE</th>
                  <th class="rightalign">TOOLS</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign" ><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
                <tr>
                  <td><img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30"></td>
                  <td>12345</td>
                  <td>SLS-12345</td>
                  <td>Sales2015</td>
                  <td>Steve Jobs</td>
                  <td>Sales Agent</td>
                  <td>stevejobs@gmail.com</td>
                  <td>+65 1234 567</td>
                  <td class="rightalign"><button type="button" class="btn btn-xs btn-table"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table"><i class="fa fa-close"></i></button>
                      <input id="checkbox" type="checkbox" value="">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
      </div>
   </div>
</div>
<!-- /#page-content-wrapper -->

@stop
