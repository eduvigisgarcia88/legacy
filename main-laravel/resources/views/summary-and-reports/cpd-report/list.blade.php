@extends('layouts.master')

@section('scripts')
<script type="text/javascript">
  $_token = '{{ csrf_token() }}';

  function refresh() {
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');

    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    var body = '';

    $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {
      $.each(response.rows.data, function(index, row) {
        body += '<tr data-id="'+row.id+'">'+
                '<td>'+row.name+'</td>'+
                '<td>'+moment(row.start).format('MM/DD/YYYY')+" - "+moment(row.end).format('MM/DD/YYYY')+'</td>'+
                '<td>'+row.left_note+'</td>'+
                '<td>'+row.right_note+'</td>'+
                '<td class="rightalign">'+
                    '<a type="button" class="btn-table btn btn-xs btn-default btn-edit"><i class="fa fa-pencil"></i></a>'+
                    '&nbsp<a href="' + '{{ url('reports') }}/' + row.id + '/get-cpd-info" target ="blank" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                    '&nbsp;<input id="checkbox" type="checkbox" value="'+row.id+'">'+
                  '</td>';
      });
      $('#rows').html(body);
      $('#row-pages').html(response.pages);
      $('.loading-pane').addClass('hide');
    }, 'json');
  }

  function search() {
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();

    var body = '';

    $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {
      $.each(response.rows.data, function(index, row) {
        body += '<tr data-id="'+row.id+'">'+
                '<td>'+row.name+'</td>'+
                '<td>'+moment(row.start).format('MM/DD/YYYY')+" - "+moment(row.end).format('MM/DD/YYYY')+'</td>'+
                '<td>'+row.left_note+'</td>'+
                '<td>'+row.right_note+'</td>'+
                '<td class="rightalign">'+
                    '<a type="button" class="btn-table btn btn-xs btn-default btn-edit"><i class="fa fa-pencil"></i></a>'+
                    '&nbsp<a href="' + '{{ url('reports') }}/' + row.id + '/get-cpd-info" target ="blank" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                    '&nbsp;<input id="checkbox" type="checkbox" value="'+row.id+'">'+
                  '</td>';
      });
      $('#rows').html(body);
      $('#row-pages').html(response.pages);
      $('.loading-pane').addClass('hide');
    }, 'json');
  }

  $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

    if ($(this).find('i').hasClass('fa-sort-up')) {
      $('.table-pagination th i').removeAttr('class');
      $order = $("#row-order").val('asc');
      $sort = $("#row-sort").val($(this).data('sort'));
      $(this).find('i').addClass('fa fa-sort-down');
    } else {
      $('.table-pagination th i').removeAttr('class');
      $order = $("#row-order").val('desc');
      $sort = $("#row-sort").val($(this).data('sort'));
      $(this).find('i').addClass('fa fa-sort-up');
    }

    $('.loading-pane').removeClass('hide');
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();

    var body = '';

    $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {
      $.each(response.rows.data, function(index, row) {
        body += '<tr data-id="'+row.id+'">'+
                '<td>'+row.name+'</td>'+
                '<td>'+moment(row.start).format('MM/DD/YYYY')+" - "+moment(row.end).format('MM/DD/YYYY')+'</td>'+
                '<td>'+row.left_note+'</td>'+
                '<td>'+row.right_note+'</td>'+
                '<td class="rightalign">'+
                    '<a type="button" class="btn-table btn btn-xs btn-default btn-edit"><i class="fa fa-pencil"></i></a>'+
                    '&nbsp<a href="' + '{{ url('reports') }}/' + row.id + '/get-cpd-info" target ="blank" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                    '&nbsp;<input id="checkbox" type="checkbox" value="'+row.id+'">'+
                  '</td>';
      });
      $('#rows').html(body);
      $('#row-pages').html(response.pages);
      $('.loading-pane').addClass('hide');
    }, 'json');
  });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {

      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();
        var body = '';

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {
          $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="'+row.id+'">'+
                    '<td>'+row.name+'</td>'+
                    '<td>'+moment(row.start).format('MM/DD/YYYY')+" - "+moment(row.end).format('MM/DD/YYYY')+'</td>'+
                    '<td>'+row.left_note+'</td>'+
                    '<td>'+row.right_note+'</td>'+
                    '<td class="rightalign">'+
                        '<a type="button" class="btn-table btn btn-xs btn-default btn-edit"><i class="fa fa-pencil"></i></a>'+
                        '&nbsp<a href="' + '{{ url('reports') }}/' + row.id + '/get-cpd-info" target ="blank" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                        '&nbsp;<input id="checkbox" type="checkbox" value="'+row.id+'">'+
                      '</td>';
          });
          $('#rows').html(body);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
        }, 'json');
      }
    });


 $("#page-content-wrapper").on("click", ".btn-generate", function() {
        $("#modal-form").find('form').trigger("reset");
        $("#row-id").val("");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
     
        $(".show-add").removeClass("hide");
        $(".show-edit").addClass("hide");
        $(".per-user").addClass("hide");
        $(".per-team").addClass("hide");

        $(".type-pane").addClass("hide");
        $("#modal-form").find('input').removeAttr('readonly', 'readonly');
        $(".modal-title").html("<i class='fa fa-plus'></i> Generate Report");
        $("#modal-form").modal('show');
        
      });

  $(function () {
        $('.date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });
$("#page-content-wrapper").on("click", ".btn-edit", function() {

        // $("#modal-form").find('form').trigger("reset");
       
        // var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        $(".show-add").addClass("hide");
        $(".show-edit").removeClass("hide");
        $(".per-user").addClass("hide");
        $(".per-team").addClass("hide");
        $(".type-pane").addClass("hide");

        $tr = $(this).parent().parent();
        $name = $tr.data('name');
        //console.log($name);
        //$id = $tr.data('id');
        $id = $tr.data('id');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          // reset all form fields
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-form").find('form').trigger("reset");
          $("#row-id").val("");


              $.post("{{ url('reports/cpd/view') }}", { id: $id, _token: $_token }, function(response) {
                  //console.log(response);
                  if(!response.error) {
                  // set form title
                      
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {

                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                          }
                          if(index == "start")
                          {
                              field.val(moment(value).format('MM/DD/YYYY'));
                          }
                          if(index == "end")
                          {
                              field.val(moment(value).format('MM/DD/YYYY'));
                          }
                            
                          if (index == "type") {
                            if (value == "Per User") {
                              $(".per-team").addClass("hide");
                              $(".per-user").removeClass("hide");
                            } else if (value == "Per Team") {
                              $(".per-user").addClass("hide");
                              $(".per-team").removeClass("hide");
                            }
                          }
                      });
                   
                      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
                      //$("#row-id").val("");
                      $(".sales_user_id").val("");
                      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
                      //$(".modal-header").removeAttr("class").addClass("modal-header addnew-bg");
                      $(".modal-title").html("<i class='fa fa-plus'></i> Edit CPD Report");
                      // show form

                      $("#row-type").attr("readonly", "readonly");
                      $("#row-agent_name").attr("readonly", "readonly");
                      $("#row-group_code").attr("readonly", "readonly");
                      $("#modal-form").modal('show');
                      //$(".btn-submit").html('<i class="fa fa-file-archive-o"></i> Regenerate');

                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
       
        
      });
    $("#page-content-wrapper").on("click", ".btn-remove-select", function() {

        $("#modal-multiple").find("input").val("");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected cpd report/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");

    });
    $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });

        
        if (ids.length == 0) {
          status("Error", "Select cpd report/s first.", 'alert-danger');
        } else {

            $.post("{{ url('reports/cpd/remove') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
        }

      });
   $("#row-report_type").on("change", function() {
     var id = $("#row-report_type").val();
        $_token = "{{ csrf_token() }}";
        var per = $("#row-per_name");
        per.html("");
        if ($("#row-report_type").val() == "1") {
          $(".type-pane").addClass("hide")
        }
        if ($("#row-report_type").val() == "2") {
          $("#type-pane-label").text("Group Code");
          $(".type-pane").removeClass("hide")
          $.post("{{ url('reports/get/group') }}", { _token: $_token }, function(response) {
            per.append(response);
          }, 'json');
        }
        if ($("#row-report_type").val() == "3") {
          $("#type-pane-label").text("User Name");
          $(".type-pane").removeClass("hide")
          $.post("{{ url('reports/get/user') }}", { _token: $_token }, function(response) {
            per.append(response);
          }, 'json');
        }

  });

    $('#row-search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            search();
        }
    });

</script>
@stop

@section('content')

<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">CPD REPORT</h3>
        <h5 class="bread-crumb">SUMMARY AND REPORTS</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right" onclick = "refresh()"><i class="fa fa-refresh"></i> REFRESH</button>
          <button type="button" class="btn btn-sm btn-danger btn-remove-select borderzero btn-tool pull-right"><i class="fa fa-minus"></i> REMOVE</button>
          <button type="button" class="btn btn-sm btn-generate btn-warning borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-file-archive-o"></i> GENERATE REPORT</button>        
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
           <!--  <p><strong>FILTER OPTIONS</strong></p> -->
      </div> 
   <!--    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH OPTION</strong></p></label>
                        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                              <select class=" form-control borderzero" id="sel1" >
                                <option>All Providers</option>
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                                <option>D</option>
                              </select>
                            </div>
                        </div>
              </div> -->
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
              <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                </span>
              </div>
              </div>
            </div>
          </div>
            </div>
    <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
        <div class="block-content tbblock col-xs-12">
           <div class="loading-pane hide">
                <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
           </div>
           <div class="table-responsive content-table" style="padding-top: 0 !important;">
            <table class="table table-striped table-pagination" id="my-table">
                <thead class="tbheader">
                  <tr>
                    <th class="th-sort" data-sort="name"><i></i> NAME</th>
                    <th class="th-sort" data-sort="start"><i></i> DATE RANGED</th>
                    <th class="th-sort" data-sort="left_note"><i></i> TOP LEFT NOTE</th>
                    <th class="th-sort" data-sort="right_note"><i></i> TOP RIGHT NOTE</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody id ="rows">
                @foreach($rows as $row)
                  <tr data-id="{{ $row->id}}">
                    <td>{{$row->name}}</td>
                    <td>{{date_format(date_create(substr($row->start, 0,10)),'m/d/Y')}} - {{date_format(date_create(substr($row->end, 0,10)),'m/d/Y')}}</td>
                    <td>{{$row->left_note}}</td>
                    <td>{{$row->right_note}}</td>
                    <td class="rightalign">
               
                      <a type="button" class="btn-table btn btn-xs btn-default btn-edit"><i class="fa fa-pencil"></i></a>
                      <a href="{{ url('reports/'.$row->id.'/get-cpd-info') }}" target ="blank" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                     <!--  <a type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-file-excel-o"></i></a>
                      <a type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-file-pdf-o"></i></a> -->
                      <input id="checkbox" type="checkbox" value="{{$row->id}}">
                    </td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
          </div>
        </div>
      </div>
     <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
      <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="50">100</option>
      </select>
      </div>
      <div class="text-center">
        <input type="hidden" id="row-page" value="1">
        <input type="hidden" id="row-sort" value="">
        <input type="hidden" id="row-order" value="">
      </div>
    </div>
    <!-- /#page-content-wrapper -->
@include('summary-and-reports.cpd-report.form')

@stop