<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
       <div class="modal-content borderzero">
           <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
           </div>
          {!! Form::open(array('url' => 'reports/cpd/generate', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
           <div class="modal-header tbheader">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
           </div>
           <div class="modal-body">
           <div id="form-notice"></div>

           <div class="form-group hide">
              <label for="row-user_id" class="col-sm-3 col-xs-5 control-label font-color">Agent</label>
                <div class="col-sm-9 col-xs-7">
                  <select class="form-control" id="row-user_id" name="user_id">
                    <option value="">All</option>
                    @foreach($agents as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endforeach
                </select>
                </div>
           </div>
           <div class="form-group">
              <label for="row-report_name" class="col-sm-3 col-xs-5 control-label font-color">Report Name</label>
                <div class="col-sm-9 col-xs-7 error-report_name">
                  <input type="text" name="report_name" class="form-control borderzero" id="row-name" maxlength="30" placeholder="Enter your report name...">
                    <sup class="sup-errors"></sup>
                </div>
           </div>
           <div class="form-group show-edit">
              <label for="row-type" class="col-sm-3 col-xs-5 control-label font-color">Type</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="type" class="form-control borderzero" id="row-type" maxlength="30" placeholder="Type">
                </div>
           </div>

           <div class="form-group show-edit per-team">
              <label for="row-group_code" class="col-sm-3 col-xs-5 control-label font-color">Group Code</label>
              <div class="col-sm-9 col-xs-7">
                <input type="text" name="group_code" class="form-control borderzero" id="row-group_code" maxlength="30" placeholder="Agent Name">
              </div>
           </div>

           <div class="form-group show-edit per-user">
              <label for="row-agent_name" class="col-sm-3 col-xs-5 control-label font-color">Agent Name</label>
              <div class="col-sm-9 col-xs-7">
                <input type="text" name="agent_name" class="form-control borderzero" id="row-agent_name" maxlength="30" placeholder="Agent Name">
              </div>
           </div>

           <div class="form-group show-add">
              <label for="row-report_name" class="col-sm-3 col-xs-5 control-label font-color">Type</label>
                <div class="col-sm-9 col-xs-7 error-report_type">
                  <select class="form-control" id="row-report_type" name="report_type">
                    <option value="" class="hide">Select:</option>
                    <option value="1">All</option>
                    <option value="2">PER TEAM</option>
                    <option value="3">PER USER</option>
                  </select>
                  <sup class="sup-errors"></sup>
                </div>
           </div>
           <div class="form-group type-pane hide">
              <label for="row-report_name" class="col-sm-3 col-xs-5 control-label font-color" id="type-pane-label"></label>
                <div class="col-sm-9 col-xs-7 error-per_name">
                  <select class="form-control" id="row-per_name" name="per_name">
                  </select>
                  <sup class="sup-errors"></sup>
                </div>
           </div>
           <div class="form-group">
                <label for="row-date_from" class="col-sm-3 control-label">From</label>
                <div class="col-sm-9 error-date_from">
                    <div class="input-group date" id="date-from">
                      <input type="text" name="date_from" class="form-control borderzero" id="row-start" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                    </div>
                    <sup class="sup-errors"></sup>
                </div>
            </div>
             <div class="form-group">
                <label for="row-date_to" class="col-sm-3 control-label">To</label>
                <div class="col-sm-9 error-date_to">
                    <div class="input-group date" id="date-to">
                      <input type="text" name="date_to" class="form-control borderzero" id="row-end" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                    </div>
                    <sup class="sup-errors"></sup>
                </div>
            </div>
            <div class="form-group">
                    <label for="row-left_note" class="col-sm-3 col-xs-5 control-label font-color">Top Left Note</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea type="text" name="left_note" class="form-control borderzero" id="row-left_note" rows="3" placeholder="Enter your top left note..."></textarea>
                </div>
           </div>
           <div class="form-group">
                    <label for="row-right_note" class="col-sm-3 col-xs-5 control-label font-color" placeholder="Enter your top right note...">Top Right Note</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea type="text" name="right_note" class="form-control borderzero" id="row-right_note" rows="3"></textarea>
                </div>
           </div>
            <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-file-archive-o"></i> Generate</button>
            </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
  
