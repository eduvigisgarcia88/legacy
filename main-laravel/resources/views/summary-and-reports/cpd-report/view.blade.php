@extends('layouts.report')

@section('scripts')

@stop

@section('content')

<div id="page-content-wrapper" class="response">
  <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
    <center>
      <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 60px; padding-bottom: 10px;">
    </center>
    <h5 class="pull-left" style="color:#79828f;">
      <?php
        $textleft = $top_left_note;
        $new_top_left_note = wordwrap($textleft, 20, "<br />\n");
        echo $new_top_left_note;
      ?>
    </h5>    
    <h5 class="pull-right" style="color:#79828f;">
      <?php
        $textright = $top_right_note;
        $new_top_right_note = wordwrap($textright, 20, "<br />\n");
        echo $new_top_right_note;
      ?>&nbsp;
    </h5>    
    <div class="table-responsive tbblock col-xs-12" style="height: 450px; padding: 0; border: 1px solid #cccccc;">
      <table class="table table-striped">
        <thead class="tbheader">
          <tr>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="provider"><small>AGENT NAME<br>&nbsp;</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="contract_no"><small>REPORTING TO<br>&nbsp;</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>AGENT CODE<br>&nbsp;</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>NEW OR<br>EXISTING</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>DATE JOINED<br>&nbsp;</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>CPD PERIOD<br>&nbsp;</small></th>
<!--             <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>CPD TRACKING<br>PERIOD</small></th> -->
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>CPD COURSE<br>&nbsp;</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>CPD HOURS REQ<br>(STRUCTURED)</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>CPD HOURS REQ<br>(ETHICS)</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>CPD HOURS REQ<br>(RULES & REGULATIONS)</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(STRUCTURED)</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(ETHICS)</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(RULES & REGULATIONS)</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(S,E & R)</small></th>
            <th style="vertical-align: middle; font-weight: bold; white-space: nowrap;" class="th-sort" data-sort="issue_date"> <small>SHORTFALL /<br>EXCESS HOURS</small></th>
          </tr>
        </thead>
        <tbody id ="rows">
          @foreach($rows as $row)
          <tr data-id="{{ $row->id}}">
            <td>{{$row->name}}</td>
            <td>{{$row->supervisor ?:$row->name}}</td>
            <td>{{$row->code}}</td>
            <td>{{$row->cpd_status}}</td>
            <td>{{date_format(date_create(substr($row->date_joined, 0,10)),'m/d/Y')}}</td>
            <td>{{$row->cpd_period}}</td>
<!--             <td>{{$row->cpd_tracking_period}}</td> -->
            <td>{{$row->course}}</td>
            <td>{{number_format($row->cpd_hours_req_skills, 2, '.', '')}}</td>
            <td>{{number_format($row->cpd_hours_req_knowledge, 2, '.', '')}}</td>
            <td>{{number_format($row->cpd_hours_req_rules_and_regulations, 2, '.', '')}}</td>
            <td>{{number_format($row->total_cpd_hours_skills, 2, '.', '')}}</td>
            <td>{{number_format($row->total_cpd_hours_knowledge, 2, '.', '')}}</td>
            <td>{{number_format($row->total_cpd_hours_rules_and_regulations, 2, '.', '')}}</td>
            <td>{{number_format($row->total_cpd_hours_final, 2, '.', '')}}</td>
            <td>{{$row->shortfall?:0}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="col-xs-12" style="font-size: 12px; padding: 0; padding-top: 10px;">
      <i>(*This document and the information in it are divrovided in confidence,For the sole purpose of LEGACY, And may not be disclosed to any third party or used for any other purpose without the express written permission of LEGACY.)</i>
    </div>
    <div class="row content-table pull-right" style="margin-right:2px;margin-top:3px;">
      <a type="button" href="{{ url('reports/'.$get_info->id.'/exportpdf') }}" class="borderzero btn btn-sm btn-danger hide">PDF</a>
      <a type=button" href="{{ url('reports/'.$get_info->id.'/exportexcel') }}" class="borderzero btn btn-sm btn-success">XLS</a>
    </div>
  </div>
</div>              
     
@stop