<style>
table, th, td {
    /*border: 1px solid white;*/
    border-collapse: collapse;
    padding: 5px 0 9px 5px;
    font-size: 12px;
    font-family: 'Helvetica';
    font-weight: normal;
}
th,{
    font-size: 12px;
    color: white;
    
}
.th-sort
{
    font-size: 11.6px !important;
    font-family: Arial !important;
    padding:7px 0 7px 8px;
    text-align: left;
}
body
{
    margin: 0 -30px 0 -30px;
    font-family: 'Helvetica';
}
.th-sort {
   font-family: 'Helvetica' !important; 
}
.reporting-to{
    width: 200px;
}
</style>
      
    <center>
      <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 60px; padding-bottom: 10px;">
    </center>

        <div class="container-fluid content-table">
        <h5>
            <table style="width:100%;color:#79828f;padding: 5px 0 5px 8px !important;">
                <tr>
                    <td style="text-align: left;vertical-align: top;padding: 5px 0 5px 8px !important;"><?php 
                    $textleft = $top_left_note;
                    $new_top_left_note = wordwrap($textleft, 20, "<br />\n");
                    echo $new_top_left_note;
                    ?></td><td style="text-align: right;padding-right:16px;vertical-align: top;">
                    <?php 
                    $textright = $top_right_note;
                    $new_top_right_note = wordwrap($textright, 20, "<br />\n");
                    echo $new_top_right_note;
                    ?>
                </td>
                </tr>
            </table>
        </h5>    

        <div class="table-responsive block-content tbblock col-xs-12">
            <table class="table" id="my-table">
                <thead class="tbheader" style="background-color: #26629A !important;border-bottom:2px solid #555B63;font-family: Helvetica;">
                  <tr>
                    
                    <th class="th-sort" data-sort="contract_no" width="110px"><small>AGENT NAME<br>&nbsp;</small></th>
                    <th class="th-sort" data-sort="contract_no"> <small>REPORTING TO<br>&nbsp;</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>AGENT CODE<br>&nbsp;</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>NEW OR<br>EXISTING</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>DATE JOINED<br>&nbsp;</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>CPD PERIOD<br>&nbsp;</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>CPD COURSE<br>&nbsp;</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>CPD TRACKING<br>PERIOD</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>CPD HOURS REQ<br>(STRUCTURED)</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>CPD HOURS REQ<br>(ETHICS)</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>CPD HOURS REQ<br>(RULES & <br>REGULATIONS)</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(STRUCTURED)</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(ETHICS)</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(RULES & REGULATION)</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>TOTAL CPD HOURS<br>(S,E & R)</small></th>
                    <th class="th-sort" data-sort="issue_date"> <small>SHORTFALL/<br>EXCESS HOURS</small></th>
                  </tr>
                </thead>
                <tbody id ="rows">
                @foreach($get_info as $row)
                  <tr data-id="{{ $row->id}}">
                    <td>{{$row->name}}</td>
                    <td class="reporting-to">{{$row->supervisor ?:$row->name}}</td>
                    <td>{{$row->code}}</td>
                    <td>{{$row->cpd_status}}</td>
                    <td>{{date_format(date_create(substr($row->date_joined, 0,10)),'m/d/Y')}}</td>
                    <td>{{$row->cpd_period}}</td>
                    <td>{{$row->course}}</td>
                    <td>{{$row->cpd_tracking_period}}</td>
                    <td>{{$row->cpd_hours_req_skills}}</td>
                    <td>{{$row->cpd_hours_req_knowledge}}</td>
                    <td>{{$row->cpd_hours_req_rules_and_regulations}}</td>
                    <td>{{$row->total_cpd_hours_knowledge}}</td>
                    <td>{{$row->total_cpd_hours_knowledge}}</td>
                    <td></td>
                   <td>{{$row->total_cpd_hours_final}}</td>
                    <td>{{$row->shortfall?:0}}</td>
                  </tr>
                 @endforeach
            
                </tbody>
              </table>
               <p style="font-size:xx-small;"><i>(*This document and the information in it are provided in confidence,For the sole purpose of LEGACY, And may not be disclosed to any third party or used for any other purpose without the express written permission of LEGACY.)</i></p> 
          </div>
      </div>
           
