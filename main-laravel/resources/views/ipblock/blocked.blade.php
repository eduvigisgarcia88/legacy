<!DOCTYPE html>
<html lang="en">
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>This domain is blocked.</title>
   <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
 </head>
 <body>
   <div class="container">
     <div class="panel panel-default" style="margin: 20px 0; border-color: #ff0000 ">
       <div class="panel-body">
         <h3 style="margin: 0 0 10px 0;">
           <i class="fa fa-minus-circle" aria-hidden="true" style="color: #ff0000 "></i>
           <b>This domain is blocked.</b>
         </h3>
         <p>Site blocked. {{url()}} is not allowed on this network.</p>
       </div>
     </div>
   </div>
 </body>
</html>