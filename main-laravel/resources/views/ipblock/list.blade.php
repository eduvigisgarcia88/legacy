@extends('layouts.master')

@section('scripts')

<script>
$(document).ready(function(){

  $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });

  $(document).on('click', '.btn-add', function(){
    $('#modal-ip_save').trigger('reset');
    $('#indicator').val(1);
    $('#row-ip_address').prop('readonly',false);
    $('#row-description').prop('readonly',false);
    $('#row-expiration_date').prop('readonly',false);
    $('#row-id').prop('readonly',false);
    $('#ip_modal_footer').removeClass('hide');
    $('#modal-ip').modal('toggle');
  });

  $(document).on('click', '.btn-delete', function(){
    var id = $(this).parent().parent().data('id');
    var title = 'Delete';
    var body = 'Are you sure do you want to delete this IP?';
    var action = '{{ url('manage/ips/delete') }}';
    dialog(title, body, action, id);
  });

  $(document).on('click', '.btn-toggle', function(){
    var id = $(this).parent().parent().data('id');
    var title_give = $(this).children('i.fa-unlock').length;
    if(title_give > 0)
    {
      var title = 'Whitelist IP';
      var body = 'Are you sure do you want to whitelist this IP?';
    }
    else
    {
      var title = 'Blacklist IP';
      var body = 'Are you sure do you want to blacklist this IP?';      
    }
    var action = '{{ url('manage/ips/toggle') }}';
    dialog(title, body, action, id);
  });

  $(document).on('click', '.btn-edit', function(){
    var id = $(this).parent().parent().data('id');
    $('#indicator').val(2);
    $.ajax({
      url: '{{ url('manage/ips/get') }}',
      data: { id: id },
      type: 'POST',
      success: function(response){
        if(response.code == 1)
        {
          $('#row-ip_address').prop('readonly',false).val(response.ip_address);
          $('#row-description').prop('readonly',false).val(response.description);
          $('#row-expiration_date').prop('readonly',false).val(response.expiration_hours);
          $('#row-id').prop('readonly',false).val(response.id);
          $('#ip_modal_footer').removeClass('hide');
          $('#modal-ip').modal('toggle');
        }
        else
        {
          status('Failed', 'Failed', 'alert-danger');
        }
      }
    });
  });

  $(document).on('click', '.btn-read', function(){
    var id = $(this).parent().parent().data('id');
    $('#indicator').val(2);
    $.ajax({
      url: '{{ url('manage/ips/get') }}',
      data: { id: id },
      type: 'POST',
      success: function(response){
        if(response.code == 1)
        {
          $('#row-ip_address').prop('readonly',true).val(response.ip_address);
          $('#row-description').prop('readonly',true).val(response.description);
          $('#row-expiration_date').prop('readonly',true).val(response.expiration_hours);
          $('#row-id').prop('readonly',true).val(response.id);
          $('#ip_modal_footer').addClass('hide');
          $('#modal-ip').modal('toggle');
        }
        else
        {
          status('Failed', 'Failed', 'alert-danger');
        }
      }
    });
  });

  $('#modal-ip_save').submit(function(e){
    e.preventDefault();
      $.ajax({
      url: '{{ url('manage/ips/save') }}',
      data: new FormData(this),
      type: 'POST',
      contentType: false,
      processData: false,
      beforeSend: function(){
        $('.sup-errors').text('');
      },
      success: function(response){
        if(response.code == 1)
        {
          refresh();
          status('Success', response.message, 'alert-success');
          $('#modal-ip').modal('toggle');
        }
        else
        {
          $(response.errors).each(function(k, v){
            var message = v.split('|');
            $('#'+message[0]).text(message[1]).css('color','#cc0000');
          });
        }
      }
    });
  });

});
  
</script>
<script>
  
  function refresh() {
  $('#table_load_div').removeClass('hide');
  var loading = $("#table_load_div");
  var table = $("#rows");
  $page = $("#row-page").val();
  $order = $("#row-order").val();
  $sort = $("#row-sort").val();
  $per = $("#row-per").val();
  $('#refresh_button').find('i').addClass('fa-spin').parent().prop('disabled',true);
  console.log($page + '-' + $per);
  $.post('{{ url('manage/ips/refresh') }}',{ page: $page, sort: $sort, order: $order, per: $per }, function(response){
    table.html("");
    var body = "";
    $.each(response.rows.data, function(index, row) {
    body += '<tr data-id="' + row.id + '">'+
            '<td>'+ row.ip_address +'</td>'+
            '<td>'+ row.description +'</td>'+
            '<td>'+ (row.expiration_hours == null ? 'N/A' : row.expiration_hours) +'</td>'+
            '<td>'+ (row.status == 1 ? 'Whitelisted' : 'Blacklisted') +'</td>'+
            '<td class="rightalign">'+

                '<button type="button" class="btn btn-xs btn-table btn-toggle borderzero" data-toggle="tooltip" title="'+(row.status == 1 ? 'Whitelist' : 'Blacklist')+'" ><i class="'+(row.status == 1 ? 'fa fa-lock' : 'fa fa-unlock')+'"></i></button> '+

                '<button type="button" class="btn btn-xs btn-table btn-read borderzero" data-toggle="tooltip" title="Read" ><i class="fa fa-eye"></i></button> '+

                '<button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button> '+

                '<button type="button" class="btn btn-xs btn-table btn-delete borderzero" data-toggle="tooltip" title="Delete" ><i class="fa fa-trash"></i></button> '+

            '</td></tr>';
    });
    $('#modal-ip_save').trigger('reset');
    table.html(body);
    $(".th-sort").find('i').removeAttr('class');
    $('#row-pages').html(response.pages);
    loading.addClass("hide");
    $('#refresh_button').find('i').removeClass('fa-spin').parent().prop('disabled',false);
  }, 'json');
}
 $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();
        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ url('manage/ips/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per }, function(response) {
          table.html("");
          var body = "";

          $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '">'+
            '<td>'+ row.ip_address +'</td>'+
            '<td>'+ row.description +'</td>'+
            '<td>'+ (row.expiration_hours == null ? 'N/A' : row.expiration_hours) +'</td>'+
            '<td>'+ (row.status == 1 ? 'Whitelisted' : 'Blacklisted') +'</td>'+
            '<td class="rightalign">'+

                '<button type="button" class="btn btn-xs btn-table btn-toggle borderzero" data-toggle="tooltip" title="'+(row.status == 1 ? 'Whitelist' : 'Blacklist')+'" ><i class="'+(row.status == 1 ? 'fa fa-lock' : 'fa fa-unlock')+'"></i></button> '+

                '<button type="button" class="btn btn-xs btn-table btn-read borderzero" data-toggle="tooltip" title="Read" ><i class="fa fa-eye"></i></button> '+

                '<button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button> '+

                '<button type="button" class="btn btn-xs btn-table btn-delete borderzero" data-toggle="tooltip" title="Delete" ><i class="fa fa-trash"></i></button> '+

            '</td></tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
      }
    });

</script>

@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
    <h3 class="main-title">IP LIST</h3>
    <h5 class="bread-crumb">USER MANAGEMENT</h5>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
    <button onclick="refresh()" type="button" class="btn btn-sm btn-success borderzero btn-tool btn-refresh pull-right" id="refresh_button"><i class="fa fa-refresh"></i> Refresh</button>
    <button type="button" class="btn btn-sm btn-warning borderzero btn-tool btn-add pull-right" id="user-add"><i class="fa fa-plus"></i> Add</button>   
  </div>
</div>

<div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
  <div class="block-content tbblock col-xs-12">
    <div class="loading-pane hide">
      <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
    </div>
    <div class="table-responsive">
    <div id="table_load_div" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
      </div>
      <table class="table table-striped table-pagination" id="my-table">
        <thead class="tbheader">
          <tr>
            <th class="th-sort"><i></i> IP</th>
            <th class="th-sort"><i></i> DESCRIPTION</th>
            <th class="th-sort"><i></i> EXPIRATION (HOURS)</th>
            <th class="th-sort"><i></i> STATUS</th>
            <th class="rightalign">TOOLS</th>
          </tr>
          </thead>
          <tbody id="rows">
          @foreach($rows as $row)
            <tr data-id="{{$row->id}}">
              <td>{{$row->ip_address}}</td>
              <td>{{$row->description}}</td>
              <td>{{$row->expiration_hours == null ? 'N/A' : $row->expiration_hours}}</td>
              <td>{{$row->status == 1 ? 'Whitelisted' : 'Blacklisted'}}</td>
              <td class="rightalign">
                <button type="button" class="btn btn-xs btn-table btn-toggle borderzero" data-toggle="tooltip" title="{{$row->status == 1 ? 'Blacklist' : 'Whitelist'}}" ><i class="{{$row->status == 1 ? 'fa fa-lock' : 'fa fa-unlock'}}"></i></button>
                <button type="button" class="btn btn-xs btn-table btn-read borderzero" data-toggle="tooltip" title="Read" ><i class="fa fa-eye"></i></button>
                <button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>
                <button type="button" class="btn btn-xs btn-table btn-delete borderzero" data-toggle="tooltip" title="Delete" ><i class="fa fa-trash"></i></button>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign"> {!!$pages!!} </div>
  <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
  <select class="form-control borderzero rightalign" id="row-per" onchange="refresh();" style="width:70px; border: 0px;">
    <option value="10">10</option>
    <option value="25">25</option>
    <option value="50">50</option>
    <option value="100">100</option>
  </select>
  </div>
  <div class="text-center">
    <input type="hidden" id="row-page" value="">
    <input type="hidden" id="row-sort" value="">
    <input type="hidden" id="row-order" value="">
  </div> 
  </div>

  <!-- modals here -->

  <div class="modal fade" id="modal-ip" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-assign" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'manage/ips/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-ip_save')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="assign-title"><i class="fa fa-plus"></i> Add IP</h4>
              </div>
              <div class="modal-body">
              <div id="assign-notice"></div>

                <div class="form-group">
                  <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">IP Address (IPV4)</label>
                  <div class="col-sm-9 col-xs-7 error-code">
                    <input type="text" name="ip_address" class="form-control borderzero" id="row-ip_address" maxlength="30" placeholder="Enter IP...">
                    <sup id="ip-address" class="sup-errors"></sup>
                  </div>
                </div>

                <div class="form-group">
                  <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">Expiration (Hours)</label>
                  <div class="col-sm-9 col-xs-7 error-code">
                    <input type="text" name="expiration_date" class="form-control borderzero" id="row-expiration_date" maxlength="30" placeholder="Enter expiration in hours...">
                    <sup id="expiration-hours" class="sup-errors"></sup>
                  </div>
                </div>

                <div class="form-group">
                  <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">Description</label>
                  <div class="col-sm-9 col-xs-7 error-code">
                    <input type="text" name="description" class="form-control borderzero" id="row-description" maxlength="30" placeholder="Enter description e.g Hacker's IP ...">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                
              </div>
              <div class="clearfix"></div>
              <div id="ip_modal_footer" class="modal-footer borderzero">
                <input type="hidden" id="indicator" name="indicator" value="">
                <input type="hidden" id="row-id" name="row_id" value="">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

@stop