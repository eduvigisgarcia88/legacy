<!-- modal add user -->
<div class="modal fade" id="modal-batch" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-batch" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'payroll/batch/check', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Create Batch</h4>
              </div>
              <div class="modal-body">
               <div id="batch-notice"></div>
              <div class="form-group">
                <label for="batch-name" class="col-sm-3 col-xs-5 control-label">Batch Name</label>
              <div class="col-sm-9 col-xs-7 error-name ">
                  <div class="input-group batch-name">
                    <input type="text" name="name" class="form-control borderzero" id="batch-name" maxlength="28" placeholder="">
                    <span class="input-group-addon">
                      <span><i class="fa fa-calendar"></i></span>
                    </span>
                  </div>
                  <sup class="sup-errors"></sup>

              </div>
              </div>
                <!-- <div class="form-group">
                    <label for="batch-name" class="col-sm-3 col-xs-5 control-label font-color">Batch Name</label>
                <div class="col-sm-9 col-xs-7 error-name ">
                  <input type="text" name="name" class="form-control borderzero" id="batch-name" maxlength="30" placeholder="">
                  <sup class="sup-errors"></sup>

                </div>
                </div> -->
              <div class="form-group">
                <label for="batch-start" class="col-sm-3 col-xs-5 control-label">Batch Start</label>
              <div class="col-sm-9 col-xs-7 error-start ">
                  <div class="input-group batch-start">
                    <input type="text" name="start" class="form-control borderzero" id="batch-start" maxlength="28" placeholder="">
                    <span class="input-group-addon">
                      <span><i class="fa fa-calendar"></i></span>
                    </span>
                  </div>
                  <sup class="sup-errors"></sup>

              </div>
              </div>
              <div class="form-group">
                <label for="batch-end" class="col-sm-3 col-xs-5 control-label">Batch End</label>
              <div class="col-sm-9 col-xs-7 error-end ">
                  <div class="input-group batch-end">
                    <input type="text" name="end" class="form-control borderzero" id="batch-end" maxlength="30" placeholder="">
                    <span class="input-group-addon">
                      <span><i class="fa fa-calendar"></i></span>
                    </span>
                  </div>
                  <sup class="sup-errors"></sup>
              </div>

              </div>
            </div>
              <div class="modal-footer borderzero">
            <input type="hidden" name="id" id="batch-id">
            <div class="progress">
              <div id="batch-progress" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0%" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                
              </div>
            </div>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

<!-- modal add user -->
<div class="modal fade" id="modal-reprocess" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-reprocess" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'payroll/batch/check', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Release Batch</h4>
              </div>
              <div class="modal-body">
               <div id="reprocess-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Reprocess?</label>
                </div>
              </div>
              <div class="modal-footer borderzero">
              <div class="progress">
                <div id="reprocess-progress" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0%" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                  
                </div>
              </div>
                <input type="hidden" name="id" id="reprocess-id">
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-check"></i> Yes</button>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

<!-- modal add user -->
<div class="modal fade" id="modal-release" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-release" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'payroll/generate-payroll/release-check', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Release Batch</h4>
              </div>
              <div class="modal-body">
               <div id="release-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Release?</label>
                </div>
              </div>
              <div class="modal-footer borderzero">
              <div class="progress">
                <div id="release-progress" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0%" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                  
                </div>
              </div>
            <input type="hidden" name="id" id="release-id">
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-check"></i> Yes</button>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>