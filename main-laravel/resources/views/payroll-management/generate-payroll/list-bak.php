@extends('layouts.master')

@section('scripts')

  <script type="text/javascript">

    // submit form
    $("#modal-release").find("form").submit(function(e) {
   
      var form = $("#modal-release").find("form");
      var loading = $("#load-release");
      var progress = $("#release-progress");

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {

          $(".sup-errors").html("");
          $(".form-control").removeClass("required");
          $.each(response.error, function(index, value) {
            var errors = value.split("|");
            $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
            $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
          });

          loading.addClass("hide");
          // status('Please correct the following:', errors, 'alert-danger', '#release-notice');
        } else if(response.nopolicy) {
          status('No Policy', 'No Policies Found.', 'alert-danger');
          loading.addClass("hide");
        } else {

          var ajaxQueue = $({});
          var ctr = -1;

          $.ajaxQueue = function(ajaxOpts) {
            // Hold the original complete function
            var oldComplete = ajaxOpts.complete;

            // Queue our ajax request
            ajaxQueue.queue(function(next) {
              // Create a complete callback to invoke the next event in the queue
              ajaxOpts.complete = function() {
                // Invoke the original complete if it was there
                if (oldComplete) {
                  oldComplete.apply(this, arguments);
                }
                var percent = (((ctr+2) / response.loop) * 100).toFixed(2);

                  progress.attr('aria-valuenow', percent);
                  progress.attr('style', 'width: ' + percent + '%');
                  progress.html(percent + '%');
                
                  ctr = ctr + 1;
                  console.log(ctr + '=' + response.loop);
                  if ((ctr+1) == response.loop) {
                    progress.html('Releasing Batch...');
                  }

                  if ((ctr+1) > response.loop) {
                    loading.addClass("hide");
                    status('Success', 'Batch released.', 'alert-success');
                    $("#modal-release").modal("hide");
                    refresh();
                  }
                // Run the next query in the queue
                next();
              };
              // Run the query
              $.ajax(ajaxOpts);
            });

          };

          var id_all = response.ids;

          // var idp[0] = new Array(2);
          var ids = '';
          for (i = 0; i < id_all.length; i++) {
            ids += '|' + id_all[i];
          }

          for (var i=1; i<=(response.loop + 1); i++) {
            if (i > response.loop) {
              progress.html('Creating Batch...');
              $.ajaxQueue({
                url: response.url_total,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, ids: ids,
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }
              });
            } else {
              $.ajaxQueue({
                url: response.url,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, ids: ids,
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }

              });
            }
          }
        }
      }

      });
    });


  // submit form
    $("#modal-batch").find("form").submit(function(e) {
   
      var form = $("#modal-batch").find("form");
      var loading = $("#load-batch");
      var progress = $("#batch-progress");

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        // console.log(response);
        if(response.error) {

          $(".sup-errors").html("");
          $(".form-control").removeClass("required");
          $.each(response.error, function(index, value) {
            var errors = value.split("|");
            $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
            $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
          });

          loading.addClass("hide");

        } else if(response.nopolicy) {
          status('No Policy', 'No Policies Found.', 'alert-danger');
          loading.addClass("hide");
        } else {

          var ajaxQueue = $({});
          var ctr = -1;

          $.ajaxQueue = function(ajaxOpts) {
            // Hold the original complete function
            var oldComplete = ajaxOpts.complete;

            // Queue our ajax request
            ajaxQueue.queue(function(next) {
              // Create a complete callback to invoke the next event in the queue
              ajaxOpts.complete = function() {
                // Invoke the original complete if it was there
                if (oldComplete) {
                  oldComplete.apply(this, arguments);
                }
                var percent = (((ctr+2) / response.loop) * 100).toFixed(2);

                  progress.attr('aria-valuenow', percent);
                  progress.attr('style', 'width: ' + percent + '%');
                  progress.html(percent + '%');
                
                  ctr = ctr + 1;
                  
                  if ((ctr+1) == response.loop) {
                    progress.html('Creating Batch...');
                  }

                  if ((ctr+1) > response.loop) {
                    loading.addClass("hide");
                    status('Success', 'Batch successfully created.', 'alert-success');
                    $("#modal-batch").modal("hide");
                    refresh();
                  }
                // Run the next query in the queue
                next();
              };
              // Run the query
              $.ajax(ajaxOpts);
            });

          };

          for (var i=1; i<=(response.loop + 1); i++) {
            if (i > response.loop) {
              progress.html('Creating Batch...');
              $.ajaxQueue({
                url: response.url_total,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }
              });
            } else {
              $.ajaxQueue({
                url: response.url,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }

              });
            }
          }
        }
      }

      });

    });


  // submit form
    $("#modal-reprocess").find("form").submit(function(e) {
   
      var form = $("#modal-reprocess").find("form");
      var loading = $("#load-reprocess");
      var progress = $("#reprocess-progress");

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          $(".sup-errors").html("");
          $(".form-control").removeClass("required");
          $.each(response.error, function(index, value) {
            var errors = value.split("|");
            $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
            $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
          });

          loading.addClass("hide");
        } else if (response.jsonerror) {
          status('Error', response.jsonerror, 'alert-danger');
          loading.addClass("hide");
          $("#modal-reprocess").modal("hide");
        } else if(response.nopolicy) {
          status('No Policy', 'No Policies Found.', 'alert-danger');
          loading.addClass("hide");
        } else {

          var ajaxQueue = $({});
          var ctr = -1;

          $.ajaxQueue = function(ajaxOpts) {
            // Hold the original complete function
            var oldComplete = ajaxOpts.complete;

            // Queue our ajax request
            ajaxQueue.queue(function(next) {
              // Create a complete callback to invoke the next event in the queue
              ajaxOpts.complete = function() {
                // Invoke the original complete if it was there
                if (oldComplete) {
                  oldComplete.apply(this, arguments);
                }
                var percent = (((ctr+2) / response.loop) * 100).toFixed(2);

                  progress.attr('aria-valuenow', percent);
                  progress.attr('style', 'width: ' + percent + '%');
                  progress.html(percent + '%');
                
                  ctr = ctr + 1;
                  
                  if ((ctr+1) == response.loop) {
                    progress.html('Creating Batch...');
                  }

                  if ((ctr+1) > response.loop) {
                    loading.addClass("hide");
                    status('Success', 'Batch successfully created.', 'alert-success');
                    $("#modal-reprocess").modal("hide");
                  }
                // Run the next query in the queue
                next();
              };
              // Run the query
              $.ajax(ajaxOpts);
            });

          };

          for (var i=1; i<=(response.loop + 1); i++) {
            if (i > response.loop) {
              progress.html('Creating Batch...');
              $.ajaxQueue({
                url: response.url_total,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }
              });
            } else {
              $.ajaxQueue({
                url: response.url,
                data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                          firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                          _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }

              });
            }
          }
        }
      }

      });
    });

  $selectedID = 0;
  $loading = false;
  $_token = '{{ csrf_token() }}';
  function refresh() {

    $_token = '{{ csrf_token() }}';
    var loading = $(".loading-pane");
    var table = $("#rows");

    loading.removeClass("hide");

    $.post("{{ url('payroll/generate-payroll/refresh') }}", { _token: $_token }, function(response) {

      // clear
      table.html("");
      
      var body = "";

      $.each(response.rows, function(index, row) {

        body += '<tr data-id="' + row.id + '"><td>';
              if (row.status == 0 || row.status == 6) {
                body += '<a type="button" class="btn-table btn btn-xs btn-expand btn-default btn-edit"><i class="fa fa-plus"></i></a>&nbsp;';
              } else if (row.status == 1) {
                body += '<a type="button" class="btn-table btn btn-xs btn-expand btn-default btn-view"><i class="fa fa-plus"></i></a>&nbsp;';
              }
        body += row.batch_id + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + moment(row.start).format('MM-DD-YYYY') + '</td>' +
                '<td>' + moment(row.end).format('MM-DD-YYYY') + '</td><td>';
                if (row.status == 0) {
                  body += 'Unreleased';                   
                } else if (row.status == 9) {
                  body += 'Processing';                   
                } else if (row.status == 7) {
                  body += 'Processing';                   
                } else if (row.status == 5) {
                  body += 'Processing';
                } else if (row.status == 8) {
                  body += 'Interrupted';
                } else if (row.status == 6) {
                  body += 'Interrupted';
                } else if (row.status == 4) {
                  body += 'Interrupted';
                } else if (row.status == 1) {
                  body += 'Released';
                }
                body += '</td><td class="rightalign">';
                if (row.status == 0) {
                  body += '<a type="button" class="btn btn-xs btn-success btn-release borderzero" data-id="' + row.id + '">Release</a>&nbsp;';               
                } else if (row.status == 9) {
                  body += '<span class="label label-warning borderzero">Processing</span>&nbsp;';                 
                } else if (row.status == 7) {
                  body += '<span class="label label-warning borderzero">Processing</span>&nbsp;';      
                } else if (row.status == 5) {
                  body += '<span class="label label-warning borderzero">Processing</span>&nbsp;';
                } else if (row.status == 8) {
                  body += '<a type="button" class="btn btn-xs btn-primary btn-reprocess borderzero" data-id="' + row.id + '">Reprocess</a>&nbsp;';
                } else if (row.status == 6) {
                  body += '<a type="button" class="btn btn-info btn-xs btn-expand btn-default btn-edit-recompute">Recompute</a>&nbsp;';
                } else if (row.status == 4) {
                  body += '<a type="button" class="btn btn-xs btn-danger btn-release borderzero" data-id="' + row.id + '">Release</a>&nbsp;';
                } else if (row.status == 1) {
                  body += '<span class="label label-default borderzero">Released</span>&nbsp;';
                }
                body += '<input id="checkbox" type="checkbox" value="' + row.id + '"></td></tr>';
      });

      table.html(body);

      $selectedID = 0;

      loading.addClass("hide");

    }, 'json');
    
  }

  $(function () {
        $('.batch-name').datetimepicker({format: 'MMM YYYY'});
        $('.batch-start').datetimepicker({format: 'L'});
        $('.batch-end').datetimepicker({
          format: 'L',
            useCurrent: false //Important! See issue .1075
        });
        $(".batch-start").on("dp.change", function (e) {
            $('.batch-end').data("DateTimePicker").minDate(e.date);
        });
        $(".batch-end").on("dp.change", function (e) {
            $('.batch-start').data("DateTimePicker").maxDate(e.date);
        });
    });

  $(document).ready(function() {

    // $(function () {
    //     $('.batch-date').datetimepicker({
    //         locale: 'en',
    //         format: 'MM/DD/YYYY'
    //     });
    // });

    $("#page-content-wrapper").on("click", ".btn-add", function() {

      var progress = $("#batch-progress");
      progress.attr('aria-valuenow', 0);
      progress.attr('style', 'width: 0%');
      progress.html("");

      $("#modal-batch").find('form').trigger('reset');
      $("#modal-batch").modal('show');

    });

    $("#page-content-wrapper").on("click", ".btn-reprocess", function() {

      var progress = $("#reprocess-progress");
      progress.attr('aria-valuenow', 0);
      progress.attr('style', 'width: 0%');
      progress.html("");
      
      $("#modal-reprocess").find('form').trigger('reset');
      $("#reprocess-id").val($(this).data('id'));
      $("#modal-reprocess").modal('show');

    });

    $("#page-content-wrapper").on("click", ".btn-release", function() {
      $("#release-notice").html("");
      var progress = $("#release-progress");
      progress.attr('aria-valuenow', '0');
      progress.attr('style', 'width: 0%');
      progress.html("");
      $("#modal-release").find('form').trigger('reset');
      $("#release-id").val($(this).data('id'));
      $("#modal-release").modal('show');
    });

    $("#page-content-wrapper").on("click", ".btn-edit-recompute", function() {
      $(this).parent().parent().find('td:nth-child(1)').find('a').click();
      $(this).html("Recompute");
    });

      $("#page-content-wrapper").on("click", ".btn-edit", function() {

        $(".btn-expand").html('<i class="fa fa-minus"></i>');
        $(".btn-expand").html('<i class="fa fa-plus"></i>');

        if ($loading){
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
            
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('payroll/generate-payroll/get-batch') }}", { id: $id, _token: $_token }, function(response) {
        
          if (!response.error) {

            var url = "{{ url('payroll/generate-payroll/recompute-check') }}";
            var token = '<input name="_token" type="hidden" value="' + '{{ csrf_token() }}' + '"><input type="hidden" name="update_id" value="' + $selectedID + '">';
            var form = '<div id="form-batch"><form method="POST" action="' + url + '" accept-charset="UTF-8" role="form" class="form-horizontal" id="form-update_batch" enctype="multipart/form-data">' + token;
            var table = form + '<div class="content-table"><table class="table">' +
                          '<tbody>' +
                            '<tr>' +
                              '<th colspan="6" class="tbheader" scope="row">RATE SETTINGS</th>' +
                            '</tr>' +
                            '<tr class="hide">' +
                              '<th scope="row">GROSS REVENUE PERCENTAGE</th>' +
                              '<td class="error-gross_revenue">' +
                              '<input type="text" class="form-control borderzero input-sm" name="gross_revenue" id="row-gross_revenue" value="' + response.gross_revenue +  '">' +
                              '<sup class="sup-errors"></sup>' +
                              '</td>' +
                            '</tr>' +
                            '<tr>' +
                              '<th scope="row">FIRM REVENUE PERCENTAGE</th>' +
                              '<td class="error-firm_revenue">' +
                              '<input type="text" class="form-control borderzero input-sm" name="firm_revenue" id="row-firm_revenue" value="' + response.firm_revenue +  '">' +
                              '<sup class="sup-errors"></sup>' +
                              '</td>' +
                            '</tr>' +
                            '<tr>' +
                              '<th scope="row">BUYOUT PERCENTAGE</th>' +
                              '<td class="error-buyout_per">' +
                              '<input type="text" class="form-control borderzero input-sm" name="buyout_per" id="row-buyout_per" value="' + response.buyout_per +  '">' +
                              '<sup class="sup-errors"></sup>' +
                              '</td>' +
                            '</tr>' +
                        '<tbody>';
           
            table += '</tbody>' +
                      '</table>'+
                      '<table class="table">' +
                          '<tbody>' +
                            '<tr>' +
                              '<th colspan="6" class="tbheader" scope="row">FOOTER NOTES</th>' +
                            '</tr>' +
                            '<tr>' +
                              '<td colspan="6"><input type="text" class="form-control borderzero input-sm" name="notes" id="row-notes" value="' + (response.notes != null ? response.notes : '') +  '"></td>' +
                            '</tr>' +
                            '<tr>' +
                              '<td colspan="6">' +
                                '<div class="progress">' +
                                  '<div id="recompute-progress" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0%" aria-valuemin="0" aria-valuemax="100" style="width:0%">' +                  
                                  '</div>' +
                                '</div>' +
                              '</td>' +
                            '</tr>' +
                        '<tbody>' +
                      '</table>' +
                      '<hr><div id="batch-notice"></div>' +
                      '<input type="hidden" value="' + $id + '" name="id">' +
                      '<button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-close"><i class="fa fa-close"></i> Close</button>' +

                      '&nbsp;<button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-update" data-id="' + $id + '"><i class="fa fa-calculator"></i> Recompute</button></form></div>';

            $tr.after('<tr><td colspan="6" style="padding:10"><div class="row-' + $selectedID + '" style="display:none;">' + table + '</div></td></tr>');

            $('.row-' + $selectedID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loading = false;
          } else {
            status(false, response.error, 'alert-danger');
          }

        });

      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {

        var ids = [];
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $("input:checked").each(function(){
            ids.push($(this).val());
        });

        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        
        dialog('Remove Selected', 'Are you sure you want to remove selected batch?', "{{ url('payroll/generate-payroll/remove-select') }}", ids);
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-rollback", function() {

        var ids = [];
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $("input:checked").each(function(){
            ids.push($(this).val());
        });

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Rollback Selected', 'Are you sure you want to rollback selected batch?', "{{ url('payroll/generate-payroll/rollback-select') }}", ids);
      });

      $("#page-content-wrapper").on("click", ".btn-view", function() {

      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');

        if ($loading){
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
            
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('payroll/generate-payroll/get-batch') }}", { id: $id, _token: $_token }, function(response) {
        
          if (!response.error) {

             var table = '<div class="content-table"><table class="table">' +
                          '<tbody>' +
                            '<tr>' +
                              '<th colspan="6" class="tbheader" scope="row">RATE SETTINGS</th>' +
                            '</tr>' +
                            '<tr class="hide">' +
                              '<th scope="row">GROSS REVENUE PERCENTAGE</th>' +
                              '<td class="error-gross_revenue">' +
                              '<input type="text" class="form-control borderzero input-sm" name="gross_revenue" id="row-gross_revenue" disabled value="' + response.gross_revenue +  '">' +
                              '<sup class="sup-errors"></sup>' +
                              '</td>' +
                            '</tr>' +
                            '<tr>' +
                              '<th scope="row">FIRM REVENUE PERCENTAGE</th>' +
                              '<td class="error-firm_revenue">' +
                              '<input type="text" class="form-control borderzero input-sm" name="firm_revenue" id="row-firm_revenue" disabled value="' + response.firm_revenue +  '">' +
                              '<sup class="sup-errors"></sup>' +
                              '</td>' +
                            '</tr>' +
                            '<tr>' +
                              '<th scope="row">BUYOUT PERCENTAGE</th>' +
                              '<td class="error-buyout_per">' +
                              '<input type="text" class="form-control borderzero input-sm" name="buyout_per" id="row-buyout_per" disabled value="' + response.buyout_per +  '">' +
                              '<sup class="sup-errors"></sup>' +
                              '</td>' +
                            '</tr>' +
                        '<tbody>';
           
            table += '</tbody>' +
                      '</table>'+
                      '<table class="table">' +
                          '<tbody>' +
                            '<tr>' +
                              '<th colspan="6" class="tbheader" scope="row">FOOTER NOTES</th>' +
                            '</tr>' +
                            '<tr>' +
                              '<td colspan="6"><input type="text" disabled class="form-control borderzero input-sm" name="notes" id="row-notes" value="' + (response.notes != null ? response.notes : '') +  '"></td>' +
                            '</tr>' +
                        '<tbody>' +
                      '</table>' +

                      '</div>' +
                      '<hr>' +


                      '<div id="batch-notice"></div>' +

                      '<button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-close"><i class="fa fa-close"></i> Close</button>';

            $tr.after('<tr><td colspan="6" style="padding:10"><div class="row-' + $selectedID + '" style="display:none;">' + table + '</div></td></tr>');

            $('.row-' + $selectedID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loading = false;
          } else {
            status(false, response.error, 'alert-danger');
          }

        });

      });

      // Close Map Listings
       $("#page-content-wrapper").on('click', '.btn-close', function() {
        $row = $(this).closest('tr');
        $id = $row.prev().data('id');

        $row.find('.content-table').slideUp(function() {
          $row.remove();
        });

        if ($id == $selectedID) {
          $selectedID = 0;
        }
        
       });

      // submit form
      $("#page-content-wrapper").on("click", ".btn-update", function(e) {

        var form = $("#form-batch").find("form");
        var loading = $(".loading-pane");
        var progress = $("#recompute-progress");

        //$(this).html('<i class="fa fa-spinner fa-spin"></i> LOADING');
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        // push form data
        $.ajax({
        type: "post",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "json",
        success: function(response) {
          if(response.error) {
            $(".sup-errors").html("");
            $(".form-control").removeClass("required");
            $.each(response.error, function(index, value) {
              var errors = value.split("|");
              $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
              $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
            });

            loading.addClass("hide");
          } else if (response.jsonerror) {
            status('Error', response.jsonerror, 'alert-danger');
            loading.addClass("hide");
          } else if(response.nopolicy) {
            status('No Policy', 'No Policies Found.', 'alert-danger');
            loading.addClass("hide");
          } else {

            var ajaxQueue = $({});
            var ctr = -1;

            $.ajaxQueue = function(ajaxOpts) {
              // Hold the original complete function
              var oldComplete = ajaxOpts.complete;

              // Queue our ajax request
              ajaxQueue.queue(function(next) {
                // Create a complete callback to invoke the next event in the queue
                ajaxOpts.complete = function() {
                  // Invoke the original complete if it was there
                  if (oldComplete) {
                    oldComplete.apply(this, arguments);
                  }
                  var percent = (((ctr+2) / response.loop) * 100).toFixed(2);

                    progress.attr('aria-valuenow', percent);
                    progress.attr('style', 'width: ' + percent + '%');
                    progress.html(percent + '%');
                  
                    ctr = ctr + 1;
                    
                    if ((ctr+1) == response.loop) {
                      progress.html('Recomputing Batch...');
                    }

                    if ((ctr+1) > response.loop) {
                      loading.addClass("hide");
                      progress.addClass("hide");
                      status('Success', 'Batch successfully recomputed.', 'alert-success');
                      refresh();
                    }
                  // Run the next query in the queue
                  next();
                };
                // Run the query
                $.ajax(ajaxOpts);
              });

            };

            for (var i=1; i<=(response.loop + 1); i++) {
              if (i > response.loop) {
                progress.html('Creating Batch...');
                $.ajaxQueue({
                  url: response.url_total,
                  data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                            firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                            _token: '{{ csrf_token() }}' },
                  type: "POST",
                  success: function(data) {

                  }
                });
              } else {
                $.ajaxQueue({
                  url: response.url,
                  data: { batch_id: response.batch_id, start: response.start, end: response.end, gross_revenue: response.gross_revenue, 
                            firm_revenue: response.firm_revenue, buyout_per: response.buyout_per, name: response.name, loop: response.loop, count: response.count, loop: i,
                            _token: '{{ csrf_token() }}' },
                  type: "POST",
                  success: function(data) {

                  }

                });
              }
            }
          }
        }

      });
    });

  });

  // reset form notice
  $("#modal-progress").on('show.bs.modal', function(e) {
    $("#reprocess-notice").html("");
    var progress = $("#reprocess-progress");
    progress.attr('aria-valuenow', '0');
    progress.attr('style', 'width: 0%');
    progress.html("");
  });

  // reset form notice
  $("#modal-batch").on('show.bs.modal', function(e) {
    $("#batch-notice").html("");
    var progress = $("#batch-progress");
    progress.attr('aria-valuenow', '0');
    progress.attr('style', 'width: 0%');
    progress.html("");
  });

  </script>

@stop

@section('content')

<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">PAYROLL MANAGEMENT</h3>
        <h5 class="bread-crumb">PAYROLL MANAGEMENTY > GENERATE PAYROLL </h5>
    </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
              <button type="button" class="btn btn-success borderzero btn-sm btn-tool pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>  
              <button type="button" class="btn btn-danger borderzero btn-sm btn-tool pull-right btn-remove-select" data-toggle="modal"><i class="fa fa-minus"></i> Remove</button>  
              <button type="button" class="btn btn-warning borderzero  btn-sm btn-tool btn-add pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> CREATE BATCH</button>
              <button type="button" class="btn btn-info borderzero btn-sm btn-tool pull-right btn-rollback"><i class="fa fa-reply"></i> ROLLBACK</button>
        </div>
  </div>
<div id="basicinfo" class="container-fluid default-container container-space">
<div class="col-lg-12">
<p><strong>FILTER OPTIONS</strong></p>
</div>
  <form class="form-horizontal">
    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
              <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PROVIDER</strong></p></label>
                      <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class=" form-control borderzero" id="sel1">
                              <option>All Providers</option>
                              <option>A</option>
                              <option>B</option>
                              <option>C</option>
                              <option>D</option>
                            </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD FROM</strong></p></label>
                          <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class=" form-control borderzero" id="sel1" >
                              <option>Jan 2013</option>
                              <option>Feb 2013</option>
                              <option>Mar 2013</option>
                              <option>Apr 2013</option>
                              <option>May 2013</option>
                            </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD TO</strong></p></label>
                          <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class=" form-control borderzero" id="sel1" >
                              <option>Mar 2015</option>
                              <option>Apr 2015</option>
                              <option>May 2015</option>
                              <option>Jun 2015</option>
                              <option>Jul 2015</option>
                            </select>
                          </div>
                      </div>
                  </div>
                </form>
              </div>

          <div class="container-fluid content-table col-xs-12" style="border-top:1px #e6e6e6 solid;">
            <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
            </div>
            <div class="table-responsive"> <!--  block-content tbblock -->    
                <table class="table table-striped" id="tblData">
                  <thead class="tbheader">
                  <tr>
                    <th nowrap><i class="fa fa-sort"></i> BATCH ID</th>
                    <th nowrap><i class="fa fa-sort"></i> BATCH NAME</th>
                    <th nowrap><i class="fa fa-sort"></i> BATCH START</th>
                    <th nowrap><i class="fa fa-sort"></i> BATCH END</th>
                    <th nowrap><i class="fa fa-sort"></i> RELEASE STATUS</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody id="rows">
                  @foreach($rows as $row)
                  <tr data-id="{{ $row->id }}">
                    <td>
                    @if ($row->status == 0 || $row->status == 6)
                    <a type="button" class="btn-table btn btn-xs btn-expand btn-default btn-edit"><i class="fa fa-plus"></i></a> 
                    @elseif ($row->status == 1)
                    <a type="button" class="btn-table btn btn-xs btn-expand btn-default btn-view"><i class="fa fa-plus"></i></a>
                    @endif
                    {{ $row->batch_id }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ date_format(new DateTime($row->start), 'm-d-Y') }}</td>
                    <td>{{ date_format(new DateTime($row->end), 'm-d-Y') }}</td>
                    <td>
                    @if ($row->status == 0)
                      Unreleased
                    @elseif ($row->status == 9) 
                      Processing
                    @elseif ($row->status == 7)
                      Processing
                    @elseif ($row->status == 5)
                      Processing
                    @elseif ($row->status == 8)
                      Interrupted
                    @elseif ($row->status == 6)
                      Interrupted
                    @elseif ($row->status == 4)
                      Interrupted
                    @elseif ($row->status == 1)
                      Released
                    @endif
                    </td>
                    <td class="rightalign">
                    @if ($row->status == 0)
                      <a type="button" class="btn btn-xs btn-success btn-release borderzero" data-id="{{ $row->id }}">Release</a>
                    @elseif ($row->status == 9) 
                      <span class="label label-warning borderzero">Processing</span>
                    @elseif ($row->status == 7)
                      <span class="label label-warning borderzero">Processing</span>
                    @elseif ($row->status == 5)
                      <span class="label label-warning borderzero">Processing</span>
                    @elseif ($row->status == 8)
                      <a type="button" class="btn btn-xs btn-primary btn-reprocess borderzero" data-id="{{ $row->id }}">Reprocess</a>
                    @elseif ($row->status == 6)
                    <a type="button" class="btn btn-info btn-xs btn-expand btn-default btn-edit-recompute">Recompute</a> 
                    @elseif ($row->status == 4)
                      <a type="button" class="btn btn-xs btn-danger btn-release borderzero" data-id="{{ $row->id }}">Release</a>
                    @elseif ($row->status == 1)
                      <span class="label label-default borderzero">Released</span>
                    @endif
                      <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                </table>
            </div>
          </div>
        </div>
    <!-- /#page-content-wrapper -->

@include('payroll-management.generate-payroll.form')

@stop
