
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LEGACYGROUP</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.2.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/navbar.css" rel="stylesheet">
    <link href="css/content2.css" rel="stylesheet">
    <link href="css/accordion.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-plus"></i> CREATE BATCH</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">BATCH INFORMATION:</label>
    <br>
    <label for="email" class="content-table">Batch ID:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="BT1234" disabled>
    <label for="email" class="content-table">Batch Code:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="12345" disabled>
    <label for="email" class="content-table">Batch Name:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="">
    <label for="email" class="content-table">Batch Start:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="">
    <label for="email" class="content-table">Batch End:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="">
  </div>  
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save</button>
</div>
</div>
</div>
</div>
<div id="myModaldelete" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-trash"></i> Delete</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to delete the entries?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalrelease" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"> RELEASE</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to release the entries?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldeactivate" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">

<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-adjust"></i> Disable</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to disable the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaledit" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-pencil"></i> Edit</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">BATCH INFORMATION:</label>
    <br>
    <label for="email" class="content-table">Batch ID:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="BT1234" disabled>
    <label for="email" class="content-table">Batch Code:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="12345" disabled>
    <label for="email" class="content-table">Batch Name:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="BATCH ONE">
    <label for="email" class="content-table">Batch Start:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="11/11/11">
    <label for="email" class="content-table">Batch End:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="11/11/12">
  </div>  
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
<div id="myModalview" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-eye"></i> View</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">BATCH INFORMATION:</label>
    <br>
    <label for="email" class="content-table">Batch ID:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="BT1234" disabled>
    <label for="email" class="content-table">Batch Code:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="12345" disabled>
    <label for="email" class="content-table">Batch Name:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="BATCH ONE" disabled>
    <label for="email" class="content-table">Batch Start:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="11/11/11" disabled>
    <label for="email" class="content-table">Batch End:</label>
    <input type="text" class="form-control borderzero" id="pwd" placeholder="11/11/12" disabled>
  </div>  
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                    <a href="#menu-toggle3" id="menu-toggle3" ><i class="fa fa-close pull-right" style="color: #79828f; padding-top: 24px; padding-right: 18px;"></i></a>
                <li class="sidebar-brand">
                    <a href="#" style="color: #79828f;">
                    <i class="fa fa-cubes" style="color: #79828f;"></i><span id="hide"> LEGACYGROUP</span>
                    </a>
                </li>
                <div id="side">
                <li>
                    <a href="dashboard.html"><i class="fa fa-cube"></i><span id="hide"> Dashboard</span></a>
                </li>
                <div class="panel-group" id="accordion">
                  
      <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion"
                href="#accordionOne">
          <h5 class="panel-title">
            <a>
                <i class="fa fa-users"></i><span id="hide"> User Management</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h5>
        </div>
        <li id="accordionOne" class="panel-collapse collapse">
    <ul id="menu" class="inner menu-pad">
      <div class="panel-group" id="accordion1">
        <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"
                href="#a" style="background-color: transparent; padding-left: 0px;">
          <h5 class="panel-title">
            <a>
              <span id="hide"> System Users</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h5>
        </div>
        <li id="a" class="panel-collapse collapse">
           <ul id="menu" class="inner menu-padd">
                    <li><a href="system-users-all.html">All</a></li>
                    <li><a href="system-users-ceo.html">CEO</a></li>
                    <li><a href="system-users-it-users.html">IT Users</a></li>
                    <li><a href="system-users-admin-accounts.html">Admin Accounts</a></li>
                    <li><a href="system-users-accounting.html">Accounting</a></li>
                    <li><a href="system-users-sales-assistant.html">Sales Assistant</a></li>
                    <li><a href="system-users-permissions.html">Permissions</a></li>
                    <li><a href="system-users-log.html">System User Log</a></li>                
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"
                href="#b" style="background-color: transparent; padding-left: 0px;">
          <h5 class="panel-title">
            <a>
              <span id="hide"> Sales Users</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h5>
        </div>
        <li id="b" class="panel-collapse collapse">
           <ul id="menu" class="inner menu-padd">
                    <li><a href="agent-management-all.html">All</a></li>
                    <li><a href="agent-management-sales-agent.html">Sales Agent</a></li>
                    <li><a href="agent-management-assistant-manager.html">Assistant Manager</a></li>
                    <li><a href="agent-management-manager.html">Manager</a></li>
                    <li><a href="agent-management-assistant-director.html">Assistant Director</a></li>
                    <li><a href="agent-management-director.html">Director</a></li>
                    <li><a href="agent-management-partner.html">Partner</a></li>
                    <div class="panel panel-default">
                      <div class="panel-heading"  data-toggle="collapse" data-parent="#"
                              href="#e" style="background-color: transparent; padding-left: 0px;">
                        <h5 class="panel-title">
                          <a>
                            <span id="hide"> Team Management</span>
                            <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
                          </a>
                        </h5>
                      </div>
                      <li id="e" class="panel-collapse collapse">
                         <ul id="menu" class="inner menu-padding">
                                  <li><a href="agent-management-all2.html">All</a></li>
                                  <li><a href="agent-management-incomplete.html">Incomplete</a></li>
                                  <li><a href="agent-management-duplicate.html">Duplicate</a></li>
                          </ul>
                      </li>
                    </div>
                    <li><a href="agent-management-log.html">Sales User Log</a></li>
            </ul>
        </li>
      </div>
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion"
               href="#accordionFour">
          <h4 class="panel-title">
            <a style="color:white;">
              <i class="fa fa-credit-card"></i><span id="hide"> Payroll Management</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        <li id="accordionFour" class="panel-collapse collapse in">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="policy-management-import-feeds.html">Upload Data Feeds</a></li>
                    <li><a href="payroll-management-generate-payroll.html" style="color:white;">Generate Payroll</a></li>
                    <li><a href="payroll-management-pre-payroll-entries.html">Admin Pre-payroll Entries</a></li>
                    <li><a href="payroll-management-computation2.html">Payroll Computation</a></li>
                    <li><a href="payroll-management-total-invoice-advisor.html">Invoice Summary (Advisor)</a></li>
                    <li><a href="payroll-management-total-invoice-supervisor.html">Invoice Summary (Supervisor)</a></li>
                    
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" 
               href="#accordionThree">
          <h4 class="panel-title">
            <a>
              <i class="fa fa-book"></i><span id="hide"> Policy Management</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        
        <li id="accordionThree" class="panel-collapse collapse">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="policy-management-policy.html">Policies</a></li>
                    <li><a href="policy-management-riders.html">Riders</a></li>
                    <li><a href="policy-management-introducers.html">Introducers</a></li>
                    <li><a href="policy-management-log.html">Policy History</a></li>
                    <li><a href="policy-management-orphan-policy.html">Orphan Policies</a></li>
                    <li><a href="policy-management-duplicate.html">Duplicates</a></li>
                    
                    <div class="panel panel-default">
                      <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"
                              href="#c" style="background-color: transparent; padding-left: 0px;">
                        <h5 class="panel-title">
                          <a>
                            <span id="hide"> Production</span>
                            <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
                          </a>
                        </h5>
                      </div>
                      <li id="c" class="panel-collapse collapse">
                         <ul id="menu" class="inner menu-padd">
                                  <li><a href="production-cases.html">Case Submission (Advisor)</a></li>
                                  <li><a href="production-cases-supervisor.html">Case Submission (Supervisor)</a></li>
                                  <li><a href="production-incept.html">Case Inceptions (Advisor)</a></li>
                                  <li><a href="production-incept-supervisor.html">Case Inceptions (Supervisor)</a></li>
                          </ul>
                      </li>
                    </div>
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion"
               href="#accordionFive">
          <h4 class="panel-title">
            <a>
              <i class="fa fa-clipboard"></i><span id="hide"> Provider & Products Mgt</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        <li id="accordionFive" class="panel-collapse collapse">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="provider&products-provider-list.html">Provider List</a></li>
                    <li><a href="provider&products-product-list.html">Product List</a></li>
                    <li><a href="provider&products-product-log.html">Product Log</a></li>
            </ul>
        </li>
      </div>
     <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion"
               href="#accordionNine">
          <h4 class="panel-title">
            <a>
              <i class="fa fa-credit-card"></i><span id="hide"> Summary and Reports</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        <li id="accordionNine" class="panel-collapse collapse">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="reports-view-firm.html">View Firm Revenue Summary</a></li>
                    <li><a href="reports-view-daily.html">View Daily Production Report</a></li>
                    <li><a href="reports-export.html">Export Daily Production Report</a></li>   
          </ul>
        </li>
      </div>
    </div>
  </div>
</ul>
</div>
        <!-- /#sidebar-wrapper -->
        <!-- Static navbar -->
        <nav class="navbar navbar-default navbar-static-top" style="background-color: #ffffff">
        <div class="container-fluid">
        <div class="pull-left">
        <a ></a>
        <a href="#menu-toggle" id="menu-toggle" type="button" class="btn btn-default"><i class="fa fa-bars" style="color: #79828f;"></i></a>
        </div>
        <div style="text-align: right; padding-left: 20px">
        <button id="bell"type="button" class="btn btn-default"><i class="fa fa-bell" style="color: #79828f;"></i></button>
        <button id="profile"class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">username <img src="images/avatar.png" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30">
        <i class="fa fa-angle-down" style="color: #79828f;"></i></button>
        <ul id="userdrop" class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Inbox <i class="fa fa-envelope pull-right"></i></i></a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Profile <i class="fa fa-user pull-right"></i></a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Settings <i class="fa fa-cog pull-right"></i></a></li>
          <li role="presentation" class="divider"></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Log out <i class="fa fa-sign-out pull-right"></i></a></li>
        </ul>
        </div><!--/.nav-collapse -->
        </div>
        </nav>
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<div class="container-fluid">
<!-- Title -->
<div class="row main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3 class="main-title">PAYROLL MANAGEMENT</h3>
        <h5 class="bread-crumb">PAYROLL MANAGEMENTY > GENERATE PAYROLL <h5>
    </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <button type="button" class="btn btn-danger borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-minus"></i> Remove</button>  
              <button type="button" class="btn btn-warning borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> CREATE BATCH</button>
        </div>
  </div>
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-12">
<p><strong>FILTER OPTIONS</strong></p>
</div>
  <form class="form-horizontal" >     
    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
              <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PROVIDER</strong></p></label>
                      <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class="select form-control borderzero" id="sel1">
                              <option>All Providers</option>
                              <option>A</option>
                              <option>B</option>
                              <option>C</option>
                              <option>D</option>
                            </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD FROM</strong></p></label>
                          <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class="select form-control borderzero" id="sel1" >
                              <option>Jan 2013</option>
                              <option>Feb 2013</option>
                              <option>Mar 2013</option>
                              <option>Apr 2013</option>
                              <option>May 2013</option>
                            </select>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD TO</strong></p></label>
                          <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class="select form-control borderzero" id="sel1" >
                              <option>Mar 2015</option>
                              <option>Apr 2015</option>
                              <option>May 2015</option>
                              <option>Jun 2015</option>
                              <option>Jul 2015</option>
                            </select>
                          </div>
                      </div>
                  </div>
                </form>
              </div>
          <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
            <div id="third" class="row table-responsive">      
              <table class="table">
                <thead>
                  <tr>
                    <th><i class="fa fa-sort"></i> BATCH ID</th>
                    <th><i class="fa fa-sort"></i> BATCH CODE</th>
                    <th><i class="fa fa-sort"></i> BATCH NAME</th>
                    <th><i class="fa fa-sort"></i> BATCH START</th>
                    <th><i class="fa fa-sort"></i> BATCH END</th>
                    <th><i class="fa fa-sort"></i> RELEASE STATUS</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-collapse-group="" data-toggle="collapse" data-target="#breakdown" class="accordion-toggle"><i class="fa fa-plus-square-o cursor"></i> BT-1234</td>
                    <td data-collapse-group="" data-toggle="collapse" data-target="#breakdown" class="accordion-toggle">12345</td>
                    <td data-collapse-group="" data-toggle="collapse" data-target="#breakdown" class="accordion-toggle">BATCH ONE</td>
                    <td data-collapse-group="" data-toggle="collapse" data-target="#breakdown" class="accordion-toggle">11/11/11</td>
                    <td data-collapse-group="" data-toggle="collapse" data-target="#breakdown" class="accordion-toggle">11/11/12</td>
                    <td data-collapse-group="" data-toggle="collapse" data-target="#breakdown" class="accordion-toggle">Pending</td>
                    <td  class="rightalign">
                        <a type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#myModalrelease"> Release</a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalview"><i class="fa fa-eye"></i></a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaledit"><i class="fa fa-pencil"></i></a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-trash"></i></a>
                        <input id="checkbox" type="checkbox" value="">
                    </td>
                  </tr>
                  <td colspan="8" style="padding:0;">
  <div class="content-table panel panel-default accordian-body collapse" id="breakdown" style="margin-bottom: 0;">
    <div class="container-fluid panel panel-default" style="border:1px;">
      <!-- Default panel contents -->
      <div class="panel-heading">RATE SETTINGS</div>
      <!-- Table -->
      <table class="table">
        <tbody>
          <tr>
            <th scope="row">FIRM PERCENTAGE</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="15% Default"></td>
          </tr>
          <tr>
            <th scope="row">AGENT BANDING</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="60% Default"></td>
          </tr>
          <tr>
            <th scope="row">SUPEVISOR BANDING PERCENTAGE</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="40% Default"></td>
          </tr>
          <tr>
            <th scope="row">BUYOUT PROMOTION PERCENTAGE</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="15% Default"></td>
          </tr>
          <tr>
            <th scope="row">INTRODUCER COMMISION PERCENTAGE</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="10% Default"></td>
          </tr>
          <tr>
            <th scope="row">SALES AGENT</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="10% Default"></td>
          </tr>
          <tr>
            <th scope="row">ASSISTANT MANAGER</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="10% Default"></td>
          </tr>
          <tr>
            <th scope="row">MANAGER</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="10% Default"></td>
          </tr>
          <tr>
            <th scope="row">ASSISTANT DIRECTOR</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="10% Default"></td>
          </tr>
          <tr>
            <th scope="row">DIRECTOR</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="10% Default"></td>
          </tr>
          <tr>
            <th scope="row">PARTNER</th>
            <td><input type="text" class="form-control borderzero input-sm" id="pwd" placeholder="10% Default"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="container-fluid panel panel-default" style="border:1px;">
      <!-- Default panel contents -->
      <div class="panel-heading">BATCH SUMMARY</div>
      <!-- Table -->
    <table class="table">
        <thead>
            <tr>
              <th class="col-lg-3 col-xs-3">CUT OFF DATE (11/11/11-11/11/12)</th>
              <th class="col-lg-3 col-xs-3">ADVISOR </th>
              <th class="col-lg-3 col-xs-3">INTRODUCER </th>
              <th class="col-lg-3 col-xs-3">GROSS COMMSSION</th>
            </tr>
        </thead>
        <tbody> 
            <tr>
              <td><strong> Jan 2015</strong></td>
              <td><strong> 300</strong></td>
              <td><strong> 300</strong></td>
              <td><strong>400</strong></td>
            </tr>
          <tr>
            <td></span><strong> Feb 2015</strong> </td>
            <td><strong>200</strong></td>
            <td><strong>200</strong></td>
            <td><strong>300</strong></td>
          </tr>
          <tr>
            <td></span><strong> Mar 2015</strong> </td>
            <td><strong>100</strong></td>
            <td><strong>150</strong></td>
            <td><strong>200</strong></td>
          </tr>
          <tr>
            <td></span><strong>TOTAL</strong> </td>
            <td><strong>600</strong></td>
            <td><strong>650</strong></td>
            <td><strong>900</strong></td>
          </tr>
          </tbody>
          </table>
    </div>
    <div class="container-fluid panel panel-default" style="border:1px;">
      <!-- Default panel contents -->
      <div class="panel-heading">ADJUSMENTS AND CORRECTIONS</div>
      <!-- Table -->
      <table class="table">
                <thead>
                  <tr>
                    <th><i class="fa fa-sort"></i> APP ID</th>
                    <th><i class="fa fa-sort"></i> APP CODE</th>
                    <th><i class="fa fa-sort"></i> APPNAME</th>
                    <th><i class="fa fa-sort"></i> TYPE</th>
                    <th><i class="fa fa-sort"></i> REMARKS</th>
                    <th><i class="fa fa-sort"></i> AGENT ID</th>
                    <th><i class="fa fa-sort"></i> AGENT NAME</th>
                    <th><i class="fa fa-sort"></i> AGENT CODE</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>PRE-1234</td>
                    <td>12345</td>
                    <td>Renewal</td>
                    <td>Incentives</td>
                    <td>Remarks</td>
                    <td>Sales Agent</td>
                    <td>Steve Jobs</td>
                    <td>12345</td>
                  </tr>
                  <td>PRE-1234</td>
                    <td>12345</td>
                    <td>Renewal</td>
                    <td>Deductions</td>
                    <td>Remarks</td>
                    <td>Sales Agent</td>
                    <td>Bill Gates</td>
                    <td>12345</td>
                  </tr>
                  <td>PRE-1234</td>
                    <td>12345</td>
                    <td>Renewal</td>
                    <td>Incentives</td>
                    <td>Remarks</td>
                    <td>Sales Agent</td>
                    <td>Steve Bill</td>
                    <td>12345</td>
                  </tr>
                </tbody>
              </table>

            </div>
            <div class="container-fluid panel panel-default" style="border:1px;">
            <!-- Default panel contents -->
            <div class="panel-heading">FOOTER NOTES</div>
            <input type="" class="form-control borderzero"  placeholder="SET FOOT NOTES HERE">
              <br>
              <button type="button" class="btn btn-danger borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-minus"></i> Update</button>  
            <button type="button" class="btn btn-warning borderzero btn-tool pull-right" data-collapse-group="" data-toggle="collapse" data-target="#breakdown" class="accordion-toggle"><i class="fa fa-plus"></i> Close</button>
            </div>
            </div>
                  <tr>
                    <td><i class="fa fa-plus-square-o"></i> BT-1234</td>
                    <td>12345</td>
                    <td>BATCH ONE</td>
                    <td>11/11/11</td>
                    <td>11/11/12</td>
                    <td>Released</td>
                    <td  class="rightalign">
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalrelease" disabled> Release</a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalview"><i class="fa fa-eye"></i></a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaledit"><i class="fa fa-pencil"></i></a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-trash"></i></a>
                        <input id="checkbox" type="checkbox" value="">
                    </td>
                  </tr>
                  <tr>
                    <td><i class="fa fa-plus-square-o"></i> BT-1234</td>
                    <td>12345</td>
                    <td>BATCH ONE</td>
                    <td>11/11/11</td>
                    <td>11/11/12</td>
                    <td>Released</td>
                    <td  class="rightalign">
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalrelease" disabled> Release</a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalview"><i class="fa fa-eye"></i></a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaledit"><i class="fa fa-pencil"></i></a>
                        <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-trash"></i></a>
                        <input id="checkbox" type="checkbox" value="">
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    <script>
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('.collapse').collapse("hide");
        $("#menu-toggle3").click(function(e) {
        e.preventDefault();
        $("#accordionFour").collapse('hide');
        });
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").addClass("toggled");
        });
        $("#menu-toggle3").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        });
    $("ul .sidebar-nav").click(function(){
    $("#wrapper").addClass("toggled");
    });
    $("#sidebar-wrapper i").click(function(){
    $("#wrapper").addClass("toggled");
    });
    $("#side").click(function(){
    $("#wrapper").addClass("toggled");
    });
    }
    </script>
    <script>
    $("").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
</body>

</html>
