<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-plus"></i> Add New</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">Username:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="email">Name:</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">Confirm Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">Type:</label>
    <select id="select" class="form-control borderzero" id="sel1">
        <option>CEO</option>
        <option>IT Users</option>
        <option>Admin Accounts</option>
        <option>Accounting</option>
    </select>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
<div id="myModaldelete" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-minus"></i> Remove</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to remove the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldeactivate" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-adjust"></i> Disable</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to disable the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaledit" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-pencil"></i> Edit</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">Username:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="stevejobs">
  </div>
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="stevejobs@email.com">
  </div>
  <div class="form-group">
    <label for="email">Name:</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="********">
  </div>
  <div class="form-group">
    <label for="pwd">Confirm Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="********">
  </div>
  <div class="form-group">
    <label for="pwd">Type:</label>
    <select id="select" class="form-control borderzero" id="sel1">
        <option>CEO</option>
        <option>IT Users</option>
        <option>Admin Accounts</option>
        <option>Accounting</option>
    </select>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
<div id="myModalprint" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> Print Invoice</h4>
</div>
<div class="modal-body">
<p>Do you want to print the tables?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalsave" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> Save PDF</h4>
</div>
<div class="modal-body">
<p>Do you want to save the tables?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalintroducer" class="modal fade modal-wide" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> INTRODUCER SUMMARY</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
              <table class="table" style="margin:0; padding:0;">
              <thead>
              <tr>
                <th>INTRODUCER ID <i class="fa fa-sort"></i></th>
                <th>INTRODUCER NAME <i class="fa fa-sort"></i></th>
                <th>LEVEL <i class="fa fa-sort"></i></th>
                <th>REMARKS <i class="fa fa-sort"></i></th>
                <th>POLICY CODE <i class="fa fa-sort"></i></th>
                <th>DATE <i class="fa fa-sort"></i></th>
                <th>AMOUNT <i class="fa fa-sort"></i></th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>12345</td>
                <td>Introducer A</td>
                <td>Level 1</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Introducer B</td>
                <td>Level 1</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Introducer C</td>
                <td>Level 1</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td><strong>TOTAL AMOUNT</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>300</strong></td>
              </tr>
              </tbody>
              </table> 
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<div id="myModaladmin" class="modal fade modal-wide" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> ADMIN PRE-PAYROLL SUMMARY</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
              <table class="table" style="margin:0; padding:0;">
              <thead>
              <tr>
                <th>PRE PAYROLL ID <i class="fa fa-sort"></i></th>
                <th>ENTRY TYPE <i class="fa fa-sort"></i></th>
                <th>REMARKS <i class="fa fa-sort"></i></th>
                <th>POLICY CODE <i class="fa fa-sort"></i></th>
                <th>DATE <i class="fa fa-sort"></i></th>
                <th>AMOUNT <i class="fa fa-sort"></i></th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>12345</td>
                <td>Incentives A</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Incentives A</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Deduction A</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td><strong>TOTAL AMOUNT</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>300</strong></td>
                </tr>
              </tbody>
              </table> 
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<div id="myModalpolicy" class="modal fade modal-wide" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> POLICY SUMMARY</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
              <table class="table" style="margin:0; padding:0;">
              <thead>
              <tr>
                <th>POLICY ID <i class="fa fa-sort"></i></th>
                <th>POLICY NO <i class="fa fa-sort"></i></th>
                <th>POLICY TYPE <i class="fa fa-sort"></i></th>
                <th>TRANSACTION CODE <i class="fa fa-sort"></i></th>
                <th>POLICY CODE <i class="fa fa-sort"></i></th>
                <th>DATE <i class="fa fa-sort"></i></th>
                <th>AMOUNT <i class="fa fa-sort"></i></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                  <td>12345</td>
                  <td>12345</td>
                  <td>Plan A</td>
                  <td>12345</td>
                  <td>12345</td>
                  <td>12/06/1985</td>
                  <td>100</td>
              </tr>
              <tr>
                  <td>12345</td>
                  <td>12345</td>
                  <td>Plan B</td>
                  <td>12345</td>
                  <td>12345</td>
                  <td>12/06/1985</td>
                  <td>100</td>
              </tr>
              <tr>
                  <td>12345</td>
                  <td>12345</td>
                  <td>Plan C</td>
                  <td>12345</td>
                  <td>12345</td>
                  <td>12/06/1985</td>
                  <td>100</td>
              </tr>
              <tr>
                  <td><strong>TOTAL AMOUNT</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>300</strong></td>
              </tr>
            </tbody>
            </table> 
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>