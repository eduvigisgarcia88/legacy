<style>
table, th, td {
    /*border: 1px solid white;*/
    border-collapse: collapse;
    padding: 5px 0 13px 8px;
    font-size: 12px;
    font-family: 'Helvetica';
}
th,{
    font-size: 12px;
    color: white;
    
}
.th-sort
{
    font-size: 11.6px !important;
    font-family: Arial !important;
    padding:7px 0 7px 8px;
    text-align: left;
}
body
{
    margin: 0 -30px 0 -30px;
    font-family: 'Helvetica';
}
.th-sort {
   font-family: 'Helvetica' !important; 
}
</style>
      
        <h2 id="logo" style="text-align: center; margin-top:0px; margin-bottom:5px; color: #79828f;"><span style="font-family:Helvetica;">{!! HTML::image('images/legacygroup.png') !!} LEGACYGROUP</span></h2><br>
        <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
        <h5>
            <table style="width:100%;color:#79828f;padding: 5px 0 5px 8px !important;">
                <tr>
                    <td style="text-align: left;vertical-align: top;padding: 5px 0 5px 8px !important;"><?php 
                    $textleft = $top_left_note;
                    $new_top_left_note = wordwrap($textleft, 20, "<br />\n");
                    echo $new_top_left_note;
                    ?></td><td style="text-align: right;padding-right:16px;vertical-align: top;">
                    <?php 
                    $textright = $top_right_note;
                    $new_top_right_note = wordwrap($textright, 20, "<br />\n");
                    echo $new_top_right_note;
                    ?>
                </td>
                </tr>
            </table>
        </h5>    

<?php

	echo($rows);

?>