@extends('layouts.master')

@section('scripts')

<script type="text/javascript">

    // submit form
    // $("form").submit(function(e) {

    $_token = '{{ csrf_token() }}';

    @if (Auth::user()->usertype_id == 1)
      function refresh() {
        var loading = $(".loading-pane");

        loading.removeClass('hide');
        var table = $("#rows-main");

        $.post("{{ url('payroll/payroll-summary-advisor/refresh') }}", { _token: $_token }, function(response) {
          table.html(response.table);
          loading.addClass('hide');
        }, 'json');
      }

      $("#page-content-wrapper").on("click", ".btn-show", function(e) {
        var id = $(this).data('id');
        var loading = $(".loading-pane");

        loading.removeClass('hide');
        $.post("{{ url('payroll/payroll-summary-advisor/show') }}", { id: id, _token: $_token }, function(response) {
          if(response.error) {
            status(false, response.error, 'alert-danger');
            loading.addClass('hide');
          } else {
            refresh();
            status(response.title, response.body, 'alert-success');
          }
        }, 'json');
      });

      $("#page-content-wrapper").on("click", ".btn-hide", function(e) {
        var id = $(this).data('id');
        var loading = $(".loading-pane");

        loading.removeClass('hide');
        $.post("{{ url('payroll/payroll-summary-advisor/hide') }}", { id: id, _token: $_token }, function(response) {
          if(response.error) {
            status(false, response.error, 'alert-danger');
          } else {
            refresh();
            status(response.title, response.body, 'alert-success');
          }
          loading.addClass('hide');
        }, 'json');
      });
    @endif


    $("#page-content-wrapper").on("click", ".payroll-form button[type='submit']", function(e) {
      var form = $(this).parent();
      var loading = $(".loading-pane");
      var button = $(this);
      var button_class = $(this).find('i').attr('class');

      button.find('i').removeAttr('class').addClass('fa fa-spinner fa-spin');

      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');

      var fields = form.serializeArray();

      // tell controller this is a mobile device
      // if(isMobile.any) {
      //     fields.push({ name: 'mobile', value: 1 });
      // }

      // push form data
      $.ajax({
          type: "post",
          url: form.attr('action'),
          data: fields,
          dataType: "json",
          async: false,
          success: function(response) {

              if(response.error) {

                status(false, response.error, 'alert-danger');
              } else {
                  // show report window

                  var w = window.open();
                  var html = response.report;

                  try {
                    w.document.write(html);
                  }
                  catch (e) {
                    alert("Pop-up Blocker is enabled! Please add this site to your exception list.");
                    button.find('i').removeAttr('class').addClass(button_class);
                    loading.addClass('hide');
                  }
              }

              button.find('i').removeAttr('class').addClass(button_class);

              loading.addClass('hide');
          }
      });
  });

  $selectedID = 0;
  $loading = false;
  $selectedAdvisorID = '0';
  $loadingAdvisor = false;
  $selectedItemAdvisorID = '0';
  $loadingItemAdvisor = false;
  $selectedItemSubAdvisorID = '0';
  $loadingItemSubAdvisor = false;
  $selectedItemSubGroupAdvisorID = '0';
  $loadingItemSubGroupAdvisor = false;
  $selectedItemSubGroupUnitAdvisorID = '0';
  $loadingItemSubGroupUnitAdvisor = false;
  $selectedItemSubGroupUnitAdvisorExpandID = '0';
  $loadingItemSubGroupUnitAdvisorExpand = false;
  $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
  $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

  $(document).ready(function() {

      $("#page-content-wrapper").on("click", ".btn-batch-expand", function() {
      $(".btn-batch-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-batch-expand").html('<i class="fa fa-plus"></i>');

        $selectedAdvisorID = '0';
        $selectedItemAdvisorID = '0';
        $selectedItemSubAdvisorID = '0';
        $loadingItemSubAdvisor = false;
        $loadingItemAdvisor = false;
        $loadingAdvisor = false;
        $selectedItemSubGroupAdvisorID = '0';
        $loadingItemSubGroupAdvisor = false;
        $selectedItemSubGroupUnitAdvisorID = '0';
        $loadingItemSubGroupUnitAdvisor = false;
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        if ($loading){
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
            
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('payroll/payroll-summary-advisor/get-month') }}", { id: $id, _token: $_token }, function(response) {

          var table = '<div id="period-table"><table class="table table-striped">' +
                        '<thead class="tbheader">' +
                          '<tr>' +
                            @if (Auth::user()->usertype_id == 1)
                            '<th> PERIOD</th>' +
                            '<th> ADVISOR </th>' +
                            '<th> INTRODUCER </th>' +
                            '<th> NET SHARE </th>' +
                            '<th> FIRM REVENUE </th>' +
                            '<th> GROSS REVENUE </th>' +
                            @else
                            '<th> PERIOD</th>' +
                            '<th> ADVISOR </th>' +
                            '<th> INTRODUCER </th>' +
                            '<th class="hide"> GROSS REVENUE </th>' +
                            @endif
                            '<th class="rightalign"> TOOLS</th>' +
                          '</tr>' +
                      '</thead>' +
                      '<tbody>';

                      console.log(response.rows);
          $.each(response.rows, function(index, row) {

            table += '<tr data-id="' + response.batch_id + '">' + 
                        // '<td>' + moment(row.date).format('MMM YYYY') + '</td>' +
                        '<td>' + row.name + '</td>' +
                        '<td data-id="' + row.end + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="advisor" data-batch_id="' + row.date + '-' + row.batch_id + '" data-id=' + row.date + '><i class="fa fa-plus"></i></a> ' + row.advisor + '</td>' +
                        '<td data-id="' + row.end + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="introducer" data-batch_id="' + row.date + '-' + row.batch_id + '" data-id=' + row.date + '><i class="fa fa-plus"></i></a> ' + row.introducer + '</td>' +
                        @if (Auth::user()->usertype_id == 1)
                        '<td>' + row.gross + '</td>' +
                        '<td>' + row.firm_revenue + '</td>' +
                        '<td>' + row.total_revenue + '</td>' +
                        @endif
                        '<td class="rightalign">' + 
                        // '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-expand") }}"> {{ csrf_field() }}' + '<input type="hidden" name="id" value="' + response.batch_id  + '"><input type="hidden" name="start" value="' + row.date  + '"><input type="hidden" name="end" value="' + row.end  + '">' + 
                        // @if($tier == 0)
                        // '<button type="submit" data-toggle="tooltip" title="Save PDF" class="btn btn-table btn-xs btn-pdf-batch-expand">' + 
                        // '<i class="fa fa-print">' + 
                        // @else
                        // '<button type="submit" data-toggle="tooltip" title="Save Team Advisor PDF" class="btn btn-table btn-xs btn-pdf-batch-expand">' + 
                        // '<i class="fa fa-users">' + 
                        // @endif
                        // '</i></button>' +
                        // '</form>' +
                        @if ($tier == 3 || $tier == 2)
                        '<form class="payroll-form hide" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-expand") }}"> {{ csrf_field() }}' + '<input type="hidden" name="id" value="' + response.batch_id  + '"><input type="hidden" name="personal" value="personal"><input type="hidden" name="start" value="' + row.date  + '"><input type="hidden" name="end" value="' + row.end  + '"><button data-toggle="tooltip" title="View Team Report" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand"><i class="fa fa-print"></i></button>' +
                        '</form>' +
                          @if ($tier == 3)
                          '<form role="form" class="payroll-form" style="float: right; padding-left: 5px;" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}">' +
                            '{{ csrf_field() }}' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="id" value="' +  response.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.date).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                              '<button data-toggle="tooltip" title="View Personal Detailed Breakdown" type="submit" data-batch="' +  response.batch_id + '" class="btn btn-table btn-xs">' +
                              '<i class="fa fa-user"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form role="form" style="float: right; padding-left: 5px;" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") }}">' +
                            '{{ csrf_field() }}' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="id" value="' +  response.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.date).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                              '<button data-toggle="tooltip" title="Summary Statement" formtarget="_blank" type="submit" data-batch="' +  response.batch_id + '" class="btn btn-table btn-xs">' +
                              '<i class="fa fa-file-pdf-o"></i>' +
                            '</button>' +
                          '</form>' +
                          @endif
                        @endif
                        @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2)
                        '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-payroll-breakdown-override") }}"> {{ csrf_field() }}' +
                          '<input type="hidden" name="id" value="' + response.batch_id  + '"><input type="hidden" name="start" value="' + row.date  + '"><input type="hidden" name="end" value="' + row.end  + '"><button data-toggle="tooltip" title="Payroll Breakdown Overrides" formtarget="_blank" type="submit" class="btn btn-table btn-xs"><i class="glyphicon glyphicon-print"></i></button>' +
                        '</form>' +
                        '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-payroll-breakdown-revenue") }}"> {{ csrf_field() }}' +
                          '<input type="hidden" name="id" value="' + response.batch_id  + '"><input type="hidden" name="start" value="' + row.date  + '"><input type="hidden" name="end" value="' + row.end  + '"><button data-toggle="tooltip" title="Payroll Breakdown Revenue" formtarget="_blank" type="submit" class="btn btn-table btn-xs"><i class="glyphicon glyphicon-print"></i></button>' +
                        '</form>' +
                        @endif
                        '</td>' +
                      '</tr>';
          });

          table += '</tbody>' +
                    '</table><br></div>';

          $tr.after('<tr><td colspan="5" style="padding:10px"><div class="row-' + $selectedID + '" style="display:none;">' + table + 

                @if (Auth::user()->usertype_id != 8)
                '<form role="form" class="payroll-form" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}"> {{ csrf_field() }}' + '<input type="hidden" name="id" value="' + $id + '"><button type="submit" data-batch="' + $id + '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> View</button>' +
                '</form>' + 
                '<form role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/xls-batch-expand") }}"> {{ csrf_field() }}' + '<input type="hidden" name="id" value="' + $id + '"><button formtarget="_blank" type="submit" data-batch="' + $id + '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> XLS</button>' +
                '</form>' + 
                @endif
                @if (Auth::user()->usertype_id == 1)
                '<form role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-firm-revenue") }}"> {{ csrf_field() }}' + '<input type="hidden" name="id" value="' + $id + '"><button formtarget="_blank" type="submit" data-batch="' + $id + '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> FIRM REVENUE</button>' +
                '</form>' + 
                @endif
                '<div class="pull-right">' +
                @if (Auth::user()->usertype_id == 1)
                '<form class="hide" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-batch-expand") }}"> {{ csrf_field() }}' + '<input type="hidden" name="id" value="' + $id + '"><button formtarget="_blank" ="submit" data-batch="' + $id + '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> SAVE PDF</button>' +
                '</form>' + 
                @endif
                '</div></div></td></tr>');



          $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
        });

      });


      $("#page-content-wrapper").on("click", ".btn-advisor-expand", function() {
      $(".btn-advisor-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-advisor-expand").html('<i class="fa fa-plus"></i>');


        $selectedItemAdvisorID = '0';
        $selectedItemSubAdvisorID = '0';
        $loadingItemSubAdvisor = false;
        $loadingItemAdvisor = false;
        $selectedItemSubGroupAdvisorID = '0';
        $loadingItemSubGroupAdvisor = false;
        $selectedItemSubGroupUnitAdvisorID = '0';
        $loadingItemSubGroupUnitAdvisor = false;
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        if ($loadingAdvisor) {
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $_token = '{{ csrf_token() }}';
        $start = $(this).data('id');
        $end = $(this).parent().data('id');
        $period = $(this).data('period');

        $selected = '.row-advisor-' + $selectedAdvisorID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($start + $period + $id == $selectedAdvisorID) {
          $selectedAdvisorID = '0';
          return;
        }

        $selectedAdvisorID = $start + $period + $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loadingAdvisor = true;

        if ($period == "advisor") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-item') }}", { id: $id, start: $start, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr>' + 

              @if (Auth::user()->usertype_id == 1)
              '<td colspan="7" style="padding:10px">'
              @else
              '<td colspan="5" style="padding:10px">'
              @endif

               + '<div class="row-advisor-' + $selectedAdvisorID + '" style="display:none;">' + response +
               // '<form role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-expand") }}"> {{ csrf_field() }}' + 
               //  '<input type="hidden" name="id" value="' + $id + '"><input type="hidden" name="start" value="' + $start + '"><input type="hidden" name="end" value="' + $end + '"><button type="submit" data-batch="' + $id + '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Save PDF</button>' +
               //  '</form>' +
                '</div></div></td></tr>');


            $('.row-advisor-' + $selectedAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingAdvisor = false;
          });
        } else if ($period == "introducer") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-introducer') }}", { id: $id, start: $start, end: $end, _token: $_token }, function(response) {
            
            $tr.after('<tr>' + 

              @if (Auth::user()->usertype_id == 1)
              '<td colspan="7" style="padding:10px">'
              @else
              '<td colspan="5" style="padding:10px">'
              @endif

             + '<div class="row-advisor-' + $selectedAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-' + $selectedAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingAdvisor = false;

          });

        }

      });

      $("#page-content-wrapper").on("click", ".btn-advisor-item-expand", function() {
      $(".btn-advisor-item-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-advisor-item-expand").html('<i class="fa fa-plus"></i>');

        $selectedItemSubAdvisorID = '0';
        $loadingItemSubAdvisor = false;
        $selectedItemSubGroupAdvisorID = '0';
        $loadingItemSubGroupAdvisor = false;
        $selectedItemSubGroupUnitAdvisorID = '0';
        $loadingItemSubGroupUnitAdvisor = false;
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $user = $(this).data('user');
        $_token = '{{ csrf_token() }}';
        $start = $(this).data('id');
        $end = $(this).parent().data('id');
        $item = $(this).data('item');
        $expand = $(this).data('expand');
        
        if ($loadingItemAdvisor) {
          return;
        }

        $selected = '.row-advisor-item-' + $selectedItemAdvisorID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($expand == $selectedItemAdvisorID) {
          $selectedItemAdvisorID = '0';
          return;
        }

        $selectedItemAdvisorID = $expand;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loadingItemAdvisor = true;

        if ($item == "commissions") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission') }}", { id: $id, start: $start, item: $item, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-' + $selectedItemAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-' + $selectedItemAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemAdvisor = false;
          });
        } else if ($item == "overrides") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override') }}", { id: $id, start: $start, item: $item, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-' + $selectedItemAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-' + $selectedItemAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemAdvisor = false;
          });
        } else if ($item == "incentives") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-incentives') }}", { id: $id, start: $start, item: $item, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-' + $selectedItemAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-' + $selectedItemAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemAdvisor = false;
          });
        } else if ($item == "deductions") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-deductions') }}", { id: $id, start: $start, item: $item, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-' + $selectedItemAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-' + $selectedItemAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemAdvisor = false;
          });
        } else if ($item == "agent") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-assigned-policies') }}", { id: $id, user: $user, start: $start, item: $item, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-' + $selectedItemAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-' + $selectedItemAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemAdvisor = false;
          });
        }

      });
  
      $("#page-content-wrapper").on("click", ".btn-advisor-item-sub-expand", function() {
      $(".btn-advisor-item-sub-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-advisor-item-sub-expand").html('<i class="fa fa-plus"></i>');

        $selectedItemSubGroupAdvisorID = '0';
        $loadingItemSubGroupAdvisor = false;
        $selectedItemSubGroupUnitAdvisorID = '0';
        $loadingItemSubGroupUnitAdvisor = false;
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        $master = $(this).parent().parent();
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $user = $(this).data('user');
        $_token = '{{ csrf_token() }}';
        $start = $(this).data('id');
        $end = $(this).parent().data('id');
        $item = $(this).data('item');
        $provider = $(this).data('provider');


        if ($loadingItemSubAdvisor) {
          return;
        }

        $selected = '.row-advisor-item-sub-' + $selectedItemSubAdvisorID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($provider == $selectedItemSubAdvisorID) {
          $selectedItemSubAdvisorID = '0';
          return;
        }

        $selectedItemSubAdvisorID = $provider;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loadingItemSubAdvisor = true;

        if ($item == "commissions") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-provider') }}", { id: $id, start: $start, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $master.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-' + $selectedItemSubAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-' + $selectedItemSubAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubAdvisor = false;
          });
        } else if ($item == "overrides") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-provider') }}", { id: $id, start: $start, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $master.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-' + $selectedItemSubAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-' + $selectedItemSubAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubAdvisor = false;
          });
        } else if ($item == "introducer") {
          console.log($id + ' - ' + $user + ' - ' + $item + ' - ' + $provider + ' - ' + $end + ' - ' + $start);
          $.post("{{ url('payroll/payroll-summary-advisor/get-introducer-policies') }}", { id: $id, start: $start, item: $item, provider: $provider, user: $user, end: $end, _token: $_token }, function(response) {
            console.log(response);

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-' + $selectedItemSubAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-' + $selectedItemSubAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubAdvisor = false;
          });
        }

      });

      $("#page-content-wrapper").on("click", ".btn-advisor-item-sub-group-expand", function() {
      $(".btn-advisor-item-sub-group-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-advisor-item-sub-group-expand").html('<i class="fa fa-plus"></i>');

        $selectedItemSubGroupUnitAdvisorID = '0';
        $loadingItemSubGroupUnitAdvisor = false;
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;
        
        $master = $(this).parent().parent().parent();
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $user = $(this).data('user');
        $_token = '{{ csrf_token() }}';
        $start = $(this).data('id');
        $end = $(this).parent().data('id');
        $item = $(this).data('item');
        $provider = $(this).data('provider');
        $sub = $(this).data('sub');
        $breakdown = $(this).data('breakdown');
        $group = $(this).data('group');

        if ($loadingItemSubGroupAdvisor) {
          return;
        }

        $selected = '.row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID;

        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

        if ($sub == $selectedItemSubGroupAdvisorID) {
          $selectedItemSubGroupAdvisorID = '0';
          return;
        }

        $selectedItemSubGroupAdvisorID = $sub;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loadingItemSubGroupAdvisor = true;
        if ($breakdown == 'team-override') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-provider') }}", { id: $id, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupAdvisor = false;
          });
        } else if ($breakdown == 'no_team-override') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-nogroup-provider') }}", { id: $id, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupAdvisor = false;
          });
        } else if ($breakdown == 'team-commission') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-provider') }}", { id: $id, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupAdvisor = false;
          });
        } else if ($breakdown == 'no_team-commission') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-nogroup-provider') }}", { id: $id, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-' + $selectedItemSubGroupAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupAdvisor = false;
          });
        } 
        
      });

      $("#page-content-wrapper").on("click", ".btn-advisor-item-sub-group-unit-expand", function() {
      $(".btn-advisor-item-sub-group-unit-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-advisor-item-sub-group-unit-expand").html('<i class="fa fa-plus"></i>');
        
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        $master = $(this).parent().parent().parent();
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $user = $(this).data('user');
        $_token = '{{ csrf_token() }}';
        $start = $(this).data('id');
        $end = $(this).parent().data('id');
        $item = $(this).data('item');
        $type = $(this).data('type');
        $provider = $(this).data('provider');
        $group = $(this).data('group');
        $unit = $(this).data('unit');
        $user = $(this).data('user');
        $owner = $(this).data('owner');

        if ($loadingItemSubGroupUnitAdvisor) {
          return;
        }

        $selected = '.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID;

        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

        if ($item == $selectedItemSubGroupUnitAdvisorID) {
          $selectedItemSubGroupUnitAdvisorID = '0';
          return;
        }

        $selectedItemSubGroupUnitAdvisorID = $item;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loadingItemSubGroupUnitAdvisor = true;
        if ($type == 'unit') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-unit-provider') }}", { id: $id, unit: $unit, user: $user, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } else if ($type == 'supervisor') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-supervisor-provider') }}", { id: $id, user: $user, owner: $owner, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } else if ($type == 'unit-comm') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-unit-provider') }}", { id: $id, unit: $unit, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } else if ($type == 'unit-group-comm') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-unit-team-provider') }}", { id: $id, unit: $unit, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } else if ($type == 'unit-personal-comm') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-personal-provider') }}", { id: $id, unit: $unit, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } else if ($type == 'supervisor-comm') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-supervisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } else if ($type == 'unit-personal-override') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-personal-provider') }}", { id: $id, unit: $unit, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } else if ($type == 'unit-group-override') {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-unit-team-provider') }}", { id: $id, unit: $unit, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-' + $selectedItemSubGroupUnitAdvisorID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisor = false;
          });
        } 
        
      });

      $("#page-content-wrapper").on("click", ".btn-advisor-item-sub-group-unit-advisor-expand", function() {
      $(".btn-advisor-item-sub-group-unit-advisor-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-advisor-item-sub-group-unit-advisor-expand").html('<i class="fa fa-plus"></i>');
        
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        $master = $(this).parent().parent().parent();
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $user = $(this).data('user');
        $sub = $(this).data('sub');
        $_token = '{{ csrf_token() }}';
        $start = $(this).data('id');
        $end = $(this).parent().data('id');
        $item = $(this).data('item');
        $type = $(this).data('type');
        $provider = $(this).data('provider');
        $group = $(this).data('group');
        $unit = $(this).data('unit');
        $owner = $(this).data('owner');
        $supervisor = $(this).data('supervisor');

        if ($loadingItemSubGroupUnitAdvisorExpand) {
          return;
        }

        $selected = '.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID;

        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

        if ($sub == $selectedItemSubGroupUnitAdvisorExpandID) {
          $selectedItemSubGroupUnitAdvisorExpandID = '0';
          return;
        }
        
        $selectedItemSubGroupUnitAdvisorExpandID = $sub;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loadingItemSubGroupUnitAdvisorExpand = true;

        if ($type == "unit-override") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-unit-advisor-provider') }}", { id: $id, user: $user, owner: $owner, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        } else if ($type == "unit-commission") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-unit-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        } else if ($type == "comm-personal") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-unit-personal-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        } else if ($type == "comm-supervisor") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-unit-supervisor-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, supervisor: $supervisor, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        } else if ($type == "comm-advisor") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-unit-advisor-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, supervisor: $supervisor, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        } else if ($type == "override-personal") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-unit-personal-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        } else if ($type == "override-supervisor") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-unit-supervisor-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, supervisor: $supervisor, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        } else if ($type == "override-advisor") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-unit-advisor-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, supervisor: $supervisor, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-advisor-' + $selectedItemSubGroupUnitAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitAdvisorExpand = false;
          });
        }
        
      });

      $("#page-content-wrapper").on("click", ".btn-advisor-item-sub-group-unit-supervisor-advisor-expand", function() {
      $(".btn-advisor-item-sub-group-unit-supervisor-advisor-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-advisor-item-sub-group-unit-supervisor-advisor-expand").html('<i class="fa fa-plus"></i>');
        
        // $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        // $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        $master = $(this).parent().parent().parent();
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $user = $(this).data('user');
        $sub = $(this).data('sub');
        $_token = '{{ csrf_token() }}';
        $start = $(this).data('id');
        $end = $(this).parent().data('id');
        $item = $(this).data('item');
        $type = $(this).data('type');
        $provider = $(this).data('provider');
        $group = $(this).data('group');
        $unit = $(this).data('unit');
        $supervisor = $(this).data('supervisor');

        if ($loadingItemSubGroupUnitSupervisorAdvisorExpand) {
          return;
        }

        $selected = '.row-advisor-item-sub-group-unit-supervisor-advisor-' + $selectedItemSubGroupUnitSupervisorAdvisorExpandID;

        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

        if ($sub == $selectedItemSubGroupUnitSupervisorAdvisorExpandID) {
          $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
          return;
        }
        
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = $sub;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = true;

        if ($type == "unit-commission-advisor") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-commission-group-unit-user-supervisor-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-supervisor-advisor-' + $selectedItemSubGroupUnitSupervisorAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-supervisor-advisor-' + $selectedItemSubGroupUnitSupervisorAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;
          });
        } else if ($type == "unit-override-advisor") {
          $.post("{{ url('payroll/payroll-summary-advisor/get-override-group-unit-user-supervisor-advisor-provider') }}", { id: $id, user: $user, start: $start, group: $group, item: $item, provider: $provider, end: $end, _token: $_token }, function(response) {

            $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-advisor-item-sub-group-unit-supervisor-advisor-' + $selectedItemSubGroupUnitSupervisorAdvisorExpandID + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-advisor-item-sub-group-unit-supervisor-advisor-' + $selectedItemSubGroupUnitSupervisorAdvisorExpandID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;
          });
        }
        
      });

  });

    //Pagination
    $("#page-content-wrapper").on('click', '#row-pages-main .pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows-main");

        $selectedID = 0;
        $loading = false;
        $selectedAdvisorID = '0';
        $loadingAdvisor = false;
        $selectedItemAdvisorID = '0';
        $loadingItemAdvisor = false;
        $selectedItemSubAdvisorID = '0';
        $loadingItemSubAdvisor = false;
        $selectedItemSubGroupAdvisorID = '0';
        $loadingItemSubGroupAdvisor = false;
        $selectedItemSubGroupUnitAdvisorID = '0';
        $loadingItemSubGroupUnitAdvisor = false;
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        loading.removeClass("hide");

        $.post("{{ url('payroll/payroll-summary-advisor/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.batch_id + '">' +
                          '<td>' + row.name + '</td>' +
                          '<td data-id="' + moment(row.end).format('YYYY-MM-DD') + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="advisor" data-id="' + moment(row.start).format('YYYY-MM-DD') + '"><i class="fa fa-plus"></i></a>&nbsp;' + formatNumber(row.revenue) + '</td>' +
                          '<td data-id="' + moment(row.end).format('YYYY-MM-DD') + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="introducer" data-id="' + moment(row.start).format('YYYY-MM-DD') + '"><i class="fa fa-plus"></i></a>&nbsp;' + formatNumber(row.introducer) + '</td>' +
                          '<td class="hide">' + row.gross_revenue + '</td>' +
                          '<td>' +
                          @if ($rank == "Supervisor")
                          '<form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="' + row.batch_id + '" class="btn btn-table btn-xs">' +
                            '<i class="fa fa-eye"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form role="form" class="payroll-form" style="float: right; padding-left: 5px;" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}">' +
                            '{{ csrf_field() }}' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="id" value="' +  row.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                              '<button data-toggle="tooltip" title="View Personal Detailed Breakdown" type="submit" data-batch="' +  response.batch_id + '" class="btn btn-table btn-xs">' +
                              '<i class="fa fa-user"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                            '<button data-toggle="tooltip" title="Summary Report" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand">' +
                              '<i class="fa fa-print"></i>' +
                            '</button>' +
                          '</form>' +
                          @elseif ($rank == "Advisor")
                          '<form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="' + row.batch_id + '" class="btn btn-table btn-xs">' +
                            '<i class="fa fa-eye"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                            '<button formtarget="_blank" data-toggle="tooltip" title="Save PDF" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand">' +
                              '<i class="fa fa-print"></i>' +
                            '</button>' +
                          '</form>' +
                          @endif
                          '</td>' +
                        '</tr>';
          });
          
          table.html(body);
          $('#row-pages-main').html(response.pages);
          loading.addClass("hide");

        }, 'json');
      }
    });


    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows-main");

        $selectedID = 0;
        $loading = false;
        $selectedAdvisorID = '0';
        $loadingAdvisor = false;
        $selectedItemAdvisorID = '0';
        $loadingItemAdvisor = false;
        $selectedItemSubAdvisorID = '0';
        $loadingItemSubAdvisor = false;
        $selectedItemSubGroupAdvisorID = '0';
        $loadingItemSubGroupAdvisor = false;
        $selectedItemSubGroupUnitAdvisorID = '0';
        $loadingItemSubGroupUnitAdvisor = false;
        $selectedItemSubGroupUnitAdvisorExpandID = '0';
        $loadingItemSubGroupUnitAdvisorExpand = false;
        $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
        $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

        $.post("{{ url('payroll/payroll-summary-advisor/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";


          $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.batch_id + '">' +
                          '<td>' + row.name + '</td>' +
                          '<td data-id="' + moment(row.end).format('YYYY-MM-DD') + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="advisor" data-id="' + moment(row.start).format('YYYY-MM-DD') + '"><i class="fa fa-plus"></i></a>&nbsp;' + formatNumber(row.revenue) + '</td>' +
                          '<td data-id="' + moment(row.end).format('YYYY-MM-DD') + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="introducer" data-id="' + moment(row.start).format('YYYY-MM-DD') + '"><i class="fa fa-plus"></i></a>&nbsp;' + formatNumber(row.introducer) + '</td>' +
                          '<td class="hide">' + row.gross_revenue + '</td>' +
                          '<td>' +
                          @if ($rank == "Supervisor")
                          '<form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="' + row.batch_id + '" class="btn btn-table btn-xs">' +
                            '<i class="fa fa-eye"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form role="form" class="payroll-form" style="float: right; padding-left: 5px;" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}">' +
                            '{{ csrf_field() }}' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="id" value="' +  row.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                              '<button data-toggle="tooltip" title="View Personal Detailed Breakdown" type="submit" data-batch="' +  response.batch_id + '" class="btn btn-table btn-xs">' +
                              '<i class="fa fa-user"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                            '<button data-toggle="tooltip" title="Summary Report" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand">' +
                              '<i class="fa fa-print"></i>' +
                            '</button>' +
                          '</form>' +
                          @elseif ($rank == "Advisor")
                          '<form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="' + row.batch_id + '" class="btn btn-table btn-xs">' +
                            '<i class="fa fa-eye"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                            '<button formtarget="_blank" data-toggle="tooltip" title="Save PDF" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand">' +
                              '<i class="fa fa-print"></i>' +
                            '</button>' +
                          '</form>' +
                          @endif
                          '</td>' +
                        '</tr>';
          });
          
          table.html(body);
          $('#row-pages-main').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    });

 function search() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows-main");

      $selectedID = 0;
      $loading = false;
      $selectedAdvisorID = '0';
      $loadingAdvisor = false;
      $selectedItemAdvisorID = '0';
      $loadingItemAdvisor = false;
      $selectedItemSubAdvisorID = '0';
      $loadingItemSubAdvisor = false;
      $selectedItemSubGroupAdvisorID = '0';
      $loadingItemSubGroupAdvisor = false;
      $selectedItemSubGroupUnitAdvisorID = '0';
      $loadingItemSubGroupUnitAdvisor = false;
      $selectedItemSubGroupUnitAdvisorExpandID = '0';
      $loadingItemSubGroupUnitAdvisorExpand = false;
      $selectedItemSubGroupUnitSupervisorAdvisorExpandID = '0';
      $loadingItemSubGroupUnitSupervisorAdvisorExpand = false;

      $.post("{{ url('payroll/payroll-summary-advisor/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.batch_id + '">' +
                          '<td>' + row.name + '</td>' +
                          '<td data-id="' + moment(row.end).format('YYYY-MM-DD') + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="advisor" data-id="' + moment(row.start).format('YYYY-MM-DD') + '"><i class="fa fa-plus"></i></a>&nbsp;' + formatNumber(row.revenue) + '</td>' +
                          '<td data-id="' + moment(row.end).format('YYYY-MM-DD') + '"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="introducer" data-id="' + moment(row.start).format('YYYY-MM-DD') + '"><i class="fa fa-plus"></i></a>&nbsp;' + formatNumber(row.introducer) + '</td>' +
                          '<td class="hide">' + row.gross_revenue + '</td>' +
                          '<td>' +
                          @if ($rank == "Supervisor")
                          '<form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="' + row.batch_id + '" class="btn btn-table btn-xs">' +
                            '<i class="fa fa-eye"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form role="form" class="payroll-form" style="float: right; padding-left: 5px;" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}">' +
                            '{{ csrf_field() }}' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="id" value="' +  row.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                              '<button data-toggle="tooltip" title="View Personal Detailed Breakdown" type="submit" data-batch="' +  response.batch_id + '" class="btn btn-table btn-xs">' +
                              '<i class="fa fa-user"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<input type="hidden" name="personal" value="personal">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                            '<button data-toggle="tooltip" title="Summary Report" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand">' +
                              '<i class="fa fa-print"></i>' +
                            '</button>' +
                          '</form>' +
                          @elseif ($rank == "Advisor")
                          '<form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/view-batch-expand") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="' + row.batch_id + '" class="btn btn-table btn-xs">' +
                            '<i class="fa fa-eye"></i>' +
                            '</button>' +
                          '</form>' +
                          '<form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") }}"> {{ csrf_field() }}' +
                            '<input type="hidden" name="id" value="' + row.batch_id + '">' +
                            '<input type="hidden" name="start" value="'  + moment(row.start).format('YYYY-MM-DD') + '">' +
                            '<input type="hidden" name="end" value="'  + moment(row.end).format('YYYY-MM-DD') + '">' +
                            '<button formtarget="_blank" data-toggle="tooltip" title="Save PDF" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand">' +
                              '<i class="fa fa-print"></i>' +
                            '</button>' +
                          '</form>' +
                          @endif
                          '</td>' +
                        '</tr>';
          });
          
          table.html(body);
          $('#row-pages-main').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    
    }

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

</script>

@stop

@section('content')

<!-- Page Content -->
        <div id="page-content-wrapper" class="response">
        <!-- Title -->
        <div class="container-fluid main-title-container container-space">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
            <h3 class="main-title">PAYROLL SUMMARY</h3>
            <h5 class="bread-crumb">PAYROLL MANAGEMENT</h5>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin hide">
                <button type="button" class="btn btn-sm btn-success borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Save PDF</button>
                <button type="button" class="btn btn-sm btn-danger borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Print Summary</button>
          </div>
          </div>
          <div class="container-fluid default-container container-space hide">
              <div class="col-lg-12">
                  <p><strong>FILTER OPTIONS</strong></p>
              </div>
              <form class="form-horizontal">
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD FROM</strong></p></label>
                              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                  <select class=" form-control borderzero" id="sel1" >
                                    <option>Jan 2013</option>
                                    <option>Feb 2013</option>
                                    <option>Mar 2013</option>
                                    <option>Apr 2013</option>
                                    <option>May 2013</option>
                                  </select>
                              </div>
                      </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD TO</strong></p></label>
                              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                  <select class=" form-control borderzero" id="sel1" >
                                    <option>Mar 2015</option>
                                    <option>Apr 2015</option>
                                    <option>May 2015</option>
                                    <option>Jun 2015</option>
                                    <option>Jul 2015</option>
                                  </select>
                              </div>
                      </div>
                    </div>
              </form>
          </div>
          <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
            <div class="table-responsive block-content tbblock col-xs-12">
            <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
                </div>
                <table class="table table-striped table-pagination">
                  <thead class="tbheader">
                    <tr>
                    @if ($tier == 0 || $tier == 3)
                      <th class="col-lg-3 col-xs-3">PAYROLL MONTH</th>
                      <th class="col-lg-3 col-xs-3 hide">START DATE </th>
                      <th class="col-lg-3 col-xs-3 hide">END DATE </th>
                      @if ($tier == 3)
                      <th class="col-lg-3 col-xs-3 rightalign hide">TOOLS</th>
                      @else
                      <th class="col-lg-3 col-xs-3 rightalign hide">TOOLS</th>
                      @endif
                      @if (Auth::user()->usertype_id == 1)
                      <th class="col-lg-3 col-xs-3 text-center">VIEWABLE TO OTHER USERS</th>
                      <th class="col-lg-3 col-xs-3 rightalign">TOOLS</th>
                      @endif
                    @else 
                      <th class="th-sort" data-sort="batch_date"><i class="fa fa-sort-up"></i> PAYROLL MONTH</th>
                      <th class="th-sort" data-sort="revenue"><i></i> ADVISOR </th>
                      <th class="th-sort" data-sort="introducer"><i></i> INTRODUCER </th>
                      <th class="th-sort hide" data-sort="gross_revenue"><i></i>  GROSS REVENUE </th>
                      <th class="rightalign">TOOLS</th>
                    @endif
                    </tr>
                  </thead>
                  <tbody id="rows-main">
                    @if ($tier == 0 || $tier == 3)
                      @if(count($rows) > 0)
                        @foreach($rows as $row)
                          <tr data-id="{{ $row->id }}">
                            <td><a class="btn btn-xs btn-table btn-batch-expand"><i class="fa fa-plus"></i></a> {{ $row->name }}</td>
                            <td class="hide">{{ date_format(new DateTime($row->start), 'd-m-Y') }}</td>
                            <td class="hide">{{ date_format(new DateTime($row->end), 'd-m-Y') }}</td>
                            @if ($tier == 3)
                            <td class="rightalign hide">
                              <form role="form" method="post" action="{{ url('payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue') }}">
                                {{ csrf_field() }}   
                                <input type="hidden" name="personal" value="personal">
                                <input type="hidden" name="id" value="{{ $row->id }}">
                                <input type="hidden" name="start" value="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') }}">
                                <input type="hidden" name="end" value="{{ date_format(date_create(substr($row->end, 0,10)),'Y-m-d') }}">
                                <button data-toggle="tooltip" title="Summary Report" formtarget="_blank" type="submit" data-batch="{{ $row->id }}" class="btn btn-table btn-xs">
                                  <i class="fa fa-print"></i>
                                </button>
                              </form>
                            </td>
                            @else
                            <td class="rightalign hide">
                            </td>
                            @endif
                            @if (Auth::user()->usertype_id == 1)
                            <td class="col-lg-3 col-xs-3 text-center">
                              @if($row->is_sales == 0)
                                <span class="label label-danger borderzero"><i class="fa fa-times"></i></span>
                              @else
                                <span class="label label-success borderzero"><i class="fa fa-check"></i></span>
                              @endif
                            </td>
                            <td class="col-lg-3 col-xs-3 rightalign">
                              @if($row->is_sales == 1)
                                <a type="button" class="btn btn-xs btn-danger btn-hide borderzero" data-id="{{ $row->id }}">Hide</a>
                              @else
                                <a type="button" class="btn btn-xs btn-success btn-show borderzero" data-id="{{ $row->id }}">Show</a>
                              @endif
                            </td>
                            @endif
                          </tr>
                        @endforeach
                      @else
                        <tr>
                          <td class="text-center">No Payrol Month found.</td>
                        </tr>
                      @endif
                    @else 
                      @if(count($rows) > 0)
                        @foreach($rows as $row)
                        <tr data-id="{{ $row->batch_id }}">
                          <!-- <td>{{ date_format(new DateTime($row->start), 'M Y') }} ({{ date_format(new DateTime($row->start), 'd') }}-{{ date_format(new DateTime($row->end), 'd') }})</td> -->
                          <td> {{$row->name}} </td>
                          <td data-id="{{ date_format(date_create(substr($row->end, 0,10)),'Y-m-d') }}"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="advisor" data-batch_id="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') . '-' . $row->batch_id }}" data-id="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') }}"><i class="fa fa-plus"></i></a> {{ number_format($row->revenue, 2) }}</td>
                          <td data-id="{{ date_format(date_create(substr($row->end, 0,10)),'Y-m-d') }}"><a class="btn btn-xs btn-table btn-advisor-expand" data-period="introducer" data-batch_id="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') . '-' . $row->batch_id }}" data-id="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') }}"><i class="fa fa-plus"></i></a> {{ number_format($row->introducer, 2) }}</td>
                          <td class="hide">{{  number_format($row->gross_revenue, 2) }}</td>
                          <td>
                          @if ($rank == "Supervisor")
                          <form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('payroll/payroll-summary-advisor/view-batch-expand') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $row->batch_id }}">
                            <button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="{{ $row->batch_id }}" class="btn btn-table btn-xs">
                            <i class="fa fa-eye"></i>
                            </button>
                          </form>
                          <form role="form" class="payroll-form" style="float: right; padding-left: 5px;" method="post" action="{{ url('payroll/payroll-summary-advisor/view-batch-expand') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="personal" value="personal">
                            <input type="hidden" name="id" value="{{ $row->batch_id }}">
                            <input type="hidden" name="start" value="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') }}">
                            <input type="hidden" name="end" value="{{ date_format(date_create(substr($row->end, 0,10)),'Y-m-d') }}">
                              <button data-toggle="tooltip" title="View Personal Detailed Breakdown" type="submit" data-batch="{{ $row->batch_id }}" class="btn btn-table btn-xs">
                              <i class="fa fa-user"></i>
                            </button>
                          </form>
                          <form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $row->batch_id }}">
                            <input type="hidden" name="personal" value="personal">
                            <input type="hidden" name="start" value="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') }}">
                            <input type="hidden" name="end" value="{{ date_format(date_create(substr($row->end, 0,10)),'Y-m-d') }}">
                            <button data-toggle="tooltip" title="Summary Report" type="submit" class="btn btn-table btn-xs btn-pdf-batch-expand">
                              <i class="fa fa-print"></i>
                            </button>
                          </form>
                          @elseif ($rank == "Advisor")
                          <form class="payroll-form" style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('payroll/payroll-summary-advisor/view-batch-expand') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $row->batch_id }}">
                            <button data-toggle="tooltip" title="Detailed Breakdown" type="submit" data-batch="{{ $row->batch_id }}" class="btn btn-table btn-xs">
                            <i class="fa fa-eye"></i>
                            </button>
                          </form>
                          <form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $row->batch_id }}">
                            <input type="hidden" name="start" value="{{ date_format(date_create(substr($row->start, 0,10)),'Y-m-d') }}">
                            <input type="hidden" name="end" value="{{ date_format(date_create(substr($row->end, 0,10)),'Y-m-d') }}">
                            <button formtarget="_blank" data-toggle="tooltip" title="Summary Report" type="submit" data-batch="{{ $row->batch_id }}" class="btn btn-table btn-xs">
                            <i class="fa fa-print"></i>
                            </button>
                          </form>
                          @endif
                          </td>
                        </tr>
                        @endforeach
                      @else
                        <tr>
                          <td class="text-center">No Payrol Month found.</td>
                        </tr>
                      @endif
                    @endif
                  </tbody>
                </table>
          </div>
        </div>
        <div id="row-pages-main" class="col-lg-6 text-center borderzero leftalign">{!! $pages !!}</div>
        
        @if ($tier == 1 || $tier == 2)
        <div class="col-lg-6 content-table" style="text-align: -webkit-right;">
        <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
        </div>
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>
        @endif
      </div>
    <!-- /#page-content-wrapper -->

@include('payroll-management.payroll-summary-advisor.form')

@stop
