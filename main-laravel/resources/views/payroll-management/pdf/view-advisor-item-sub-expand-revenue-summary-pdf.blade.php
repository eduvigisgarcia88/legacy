<html>
<style>
body
{
  margin: 15px;
  background-color: #ffffff;
  font-family: 'Helvetica';
}
table {
  width: 100%;
}
table.table-main {
  background-color: #1e2631;
}
thead.thead-table {
  background-color: #26629A;
  color: #ffffff;
}
tbody.tbody-table {
  background-color: #F9F9F9;
}
h3 {
  margin: 10px;
  text-decoration: underline;
}
</style>
<body>
<table>
  <tr>
    <td>
      <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
    </td>
    <td style="line-height: 1; text-align: right;">
      <small><small>{{ $settings->address }}</small></small><br>
      <small><small>{{ $settings->address_2 }}</small></small><br>
      <small><small>{{ $settings->address_3 }}</small></small><br><br>
    </td>
  </tr>
</table>


@if (!$summary)
<br>SOURCE NAME: {{ $source_name }} <br>
SOURCE CODE: {{ $source_code }} <br><br>

<big><big><strong>SUMMARY STATEMENT OF ACCOUNT</strong></big></big><br>
LFA ONGOING {{ $type }} FOR <strong>{{ $batch_name }}</strong>
<div>
<br>

<div>
<br>
@else
<br>
<center><strong>LFA {{ $source_name }} {{ $type }}</strong></center>

@endif


<?php echo ($all_first); ?>

</div>
</body>
</html>