<html>
<style>
body
{
  margin: 15px;
  background-color: #ffffff;
  font-family: 'Helvetica';
}
table {
  width: 100%;
}
table.table-main {
  background-color: #1e2631;
}
thead.thead-table {
  background-color: #26629A;
  color: #ffffff;
}
tbody.tbody-table {
  background-color: #F9F9F9;
}
h3 {
  margin: 10px;
  text-decoration: underline;
}
</style>
<body>
    <center>
        <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
    </center>
<div>
  <table>
    <tr>
      <td style="text-align: left; text-decoration: underline; font-weight: bold;">{{ $title }}</td>
      <td style="text-align: right;"><b>Provider:</b> All Providers</td>
    </tr>
    <tr>
      <td><?php echo($agent_name) ?></td>
      <td style="text-align: right;"><b>Period From:</b> {{ $start }}</td>
    </tr>
    <tr>
      <td><?php echo($agent_rank) ?></td>
      <td style="text-align: right;"><b>Period To:</b> {{ $end }}</td>
    </tr>
  </table>
  <br>
  <table class="table-main">
    <thead class="thead-table">
      <tr>
        <th>PERIOD</th>
        <th>ADVISOR</th>
        <th>INTRODUCER</th>
        <th>AMOUNT</th>
      </tr>
    </thead>
    <tbody class="tbody-table">
      <?php
        echo($rows);
      ?>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <td><b>TOTAL</b></td>
    <td style="text-align: center;">{{ $advisor }}</td>
    <td style="text-align: center;">{{ $introducer }}</td>
    <td style="text-align: center;">{{ $gross_revenue }}</td>
    </tr>
    </tbody>
  </table>
</div>
</body>
</html>