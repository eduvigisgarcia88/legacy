<html>
<title>Legacy Corp</title>
<style>
body
{
  margin: 15px;
  background-color: #ffffff;
  font-family: 'Helvetica';
}
table {
  width: 100%;
  font-size: 12px !important;
}
table.table-main {
  background-color: #1e2631;
}
thead.thead-table {
  background-color: #26629A;
  color: #ffffff;
}
tbody.tbody-table {
  background-color: #F9F9F9;
}
h3 {
  margin: 10px;
  text-decoration: underline;
}
.page-break {
    page-break-after: always;
}
</style>
<body>
@if ($system_user)
<div>
  <center>
    <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
  </center>
  <table>
    <tr>
      <td style="text-align: left; text-decoration: underline; font-weight: bold;">{{ $title }}</td>
      <td style="text-align: right;"><b>Provider:</b> All Providers</td>
    </tr>
    <tr>
      <td><?php echo($agent_name) ?></td>
      <td style="text-align: right;"><b>Period From:</b> {{ $start }}</td>
    </tr>
    <tr>
      <td><?php echo($agent_rank) ?></td>
      <td style="text-align: right;"><b>Period To:</b> {{ $end }}</td>
    </tr>
  </table>
  <br>
  <table class="table-main">
    <thead class="thead-table">
      <tr>
        <th>PROVIDER</th>
        <th>1ST YEAR COMM</th>
        <th>RENEWAL COMM</th>
        <th>TOTAL</th>
      </tr>
    </thead>
    <tbody class="tbody-table">
      <?php
        echo($rows);
      ?>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><b>TOTAL</b></td>
      <td style="text-align: center;">{{ $total_first }}</td>
      <td style="text-align: center;">{{ $total_renewal }}</td>
      <td style="text-align: center;">{{ $total }}</td>
    </tr>
    </tbody>
  </table>
</div>
@endif

<?php echo($all_first) ?>

</body>
</html>