<html>
<head>
<title>Legacy Corp</title>
<style>
body
{
  margin: 15px;
  background-color: #ffffff;
  font-family: 'Helvetica';
}
table {
  width: 100%;
  font-size: 12px;
}
table .provider-tr {
  background-color: #5b9bd5;
}
table.table-main {
  background-color: #1e2631;
}
thead.thead-table {
  background-color: #26629A;
  color: #ffffff;
}
tbody.tbody-table {
  background-color: #F9F9F9;
}
h3 {
  margin: 10px;
  text-decoration: underline;
}
</style>
</head>
<body>
  <center>
    <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
  </center>
<div>

  <br>

  <?php echo($rows) ?>

</div>
</body>
</html>