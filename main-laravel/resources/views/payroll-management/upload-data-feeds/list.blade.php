@extends('layouts.master')

@section('scripts')

  <script type="text/javascript">
    $(function () {
      $('.row-upload_date').datetimepicker({
        format: 'L'
      });
    });

    $_token = '{{ csrf_token() }}';
    $selectedIDDelete = 0;
    $selectedID = 0;
    $loading = false;
    $th_header = $(".th-header").html();

    function search() {
      var loading = $(".loading-pane");
      var table = $("#rows");
      loading.removeClass('hide');

      $("#row-page").val(1);

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $selectedID = 0;
      $loading = false;

      $.post("{{ url('payroll/refresh/upload') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {

        var rows = '';
        $.each(response.rows.data, function(index, row) {
          var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

            rows += '<tr class="' + ((row.status == 9) ? 'danger' : '' ) + '" data-id="' + row.id + '">' +
                      '<td>' + row.display_name + '</td>' +
                      '<td>' + row.feed_type + '</td>' +
                      '<td>' + row.name +'</td>' +
                      '<td>' + ((row.status == 0) ? 'Unmap' : (row.status == 1) ? 'Mapped' : (row.status == 9) ? 'Invalid Document' : 'Allocated') + '</td>' +
                      '<td>' + moment(row.upload_date).format('DD-MM-YYYY') + '</td>' +
                      '<td class="rightalign">' +
                        (row.status == 0 ? '<a type="button" class="btn btn-xs btn-table btn-showmap">Map</a>' : row.status == 1 ? '<a type="button" class="btn btn-xs btn-table btn-allocate">Allocate</a>' : row.status == 9 ? '<a class="btn btn-xs btn-danger btn-error"><i class="fa fa-exclamation-triangle"></i><a> <span class="label label-danger">Invalid Document</span>' : '<span class="label label-default">Allocated</span>' ) + 
                        '&nbsp;<a type="button" class="btn-table btn btn-xs btn-default btn-delete"><i class="fa fa-trash"></i></a>' +
                      '</td>' +
                    '</tr>';
        });

        $('#rows').html(rows);
        $('#row-pages').html(response.pages);
        loading.addClass('hide');
      });
    } 

    function refresh() {
      var loading = $(".loading-pane");
      var table = $("#rows");
      loading.removeClass('hide');

      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $(".th-sort").find('i').removeAttr('class');
      $(".th-header").html($th_header);

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $selectedID = 0;
      $loading = false;

      $.post("{{ url('payroll/refresh/upload') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {

        var rows = '';
        $.each(response.rows.data, function(index, row) {
          var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

            rows += '<tr class="' + ((row.status == 9) ? 'danger' : '' ) + '" data-id="' + row.id + '">' +
                      '<td>' + row.display_name + '</td>' +
                      '<td>' + row.feed_type + '</td>' +
                      '<td>' + row.name +'</td>' +
                      '<td>' + ((row.status == 0) ? 'Unmap' : (row.status == 1) ? 'Mapped' : (row.status == 9) ? 'Invalid Document' : 'Allocated') + '</td>' +
                      '<td>' + moment(row.upload_date).format('DD-MM-YYYY') + '</td>' +
                      '<td class="rightalign">' +
                        (row.status == 0 ? '<a type="button" class="btn btn-xs btn-table btn-showmap">Map</a>' : row.status == 1 ? '<a type="button" class="btn btn-xs btn-table btn-allocate">Allocate</a>' : row.status == 9 ? '<a class="btn btn-xs btn-danger btn-error"><i class="fa fa-exclamation-triangle"></i><a> <span class="label label-danger">Invalid Document</span>' : '<span class="label label-default">Allocated</span>' ) + 
                        '&nbsp;<a type="button" class="btn-table btn btn-xs btn-default btn-delete"><i class="fa fa-trash"></i></a>' +
                      '</td>' +
                    '</tr>';
        });

        $('#rows').html(rows);
        $('#row-pages').html(response.pages);
        loading.addClass('hide');
      });
    }

    $(document).ready(function() {
      $("#row-provider_id").on("change", function() {
        var id = $("#row-provider_id").val();
        $_token = "{{ csrf_token() }}";
        $(".tracking_type").addClass("hide");
        feed = $("#row-category_id");
        feed.html("");
        parser = $("#row-parser_type");
        parser.html("");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        $.post("{{ url('payroll/get-feed') }}", { id: id, _token: $_token }, function(response) {
          if (response.categories.length > 0) {
            feed.append(
              '<option value="" class="hide">Select:</option>'
            );
            $.each(response.categories, function(index, row) {
              feed.append(
                '<option value="' + row.id + '">' + row.name + '</option>'
              );
            });
          }
        }, 'json');
      });

      $("#row-category_id").on("change", function() {
        var id = $("#row-category_id").val();
        $_token = "{{ csrf_token() }}";
        parser = $("#row-parser_type");
        parser.html("");
        $(".tracking_type").addClass("hide");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        $.post("{{ url('payroll/get-parser') }}", { id: id, _token: $_token }, function(response) {
          $("#row-parser_type").append(response);
        }, 'json');
      });

      $("#row-parser_type").on("change", function() {
        var id = $("#row-parser_type").val();
        var tracking = $(".tracking_type");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        if (id == "Custom") {
          tracking.removeClass("hide");
        } else {
          tracking.addClass("hide");
        }
      });

      $('#myModaldelete').on('click', '.btn-yes', function() {
        $.post("{{ url('payroll/delete') }}", { id: $selectedIDDelete, _token: $_token }, function(response) {
          toastr[response.type](response.body);
          refresh();
        });
      });

      $("#page-content-wrapper").on("click", ".btn-delete", function() {
        $("#remove-feed-id").val($(this).parent().parent().data('id'));
        $("#remove-feed-file").html($(this).parent().parent().find('td:nth-child(1)').html());
        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-remove-feed").modal("show");
      });

      $("#page-content-wrapper").on("click", ".btn-error", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        var title = $(this).parent().parent().find('td:nth-child(1)').html();
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-exclamation-triangle");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");

        $.post("{{ url('payroll/get-error') }}", { id: id, _token: $_token }, function(response) {

          $(".error-title").html('Error Log <small style="color: #ffffff; font-weight: bold;">('+ title + ')</small>');
          $(".error-error_log").html(response);
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-exclamation-triangle");
          $("#modal-error").modal("show");

        }, 'json');
      });

      $("#page-content-wrapper").on('click', '.btn-close', function() {
        $row = $(this).closest('tr');
        $id = $row.prev().data('id'); 
        $row.find('.content-table').slideUp(function() {
          $row.remove();
        });
        if ($id == $selectedID) {
          $selectedID = 0;
        }
      });

      $("#page-content-wrapper").on('click', '.btn-import', function() {
        $("#modal-form").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");

        $(".tracking_type").addClass("hide");
        feed = $("#row-category_id");
        feed.html("");
        parser = $("#row-parser_type");
        parser.html("");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        $("#row-upload_date").val(moment("{{ Carbon::now() }}").format('MM/DD/YYYY'));

        $(".provider_category").addClass('hide');
        $("#modal-form").modal('show');
      });

      $("#modal-allocate").find("form").submit(function(e) {
     
        var form = $("#modal-allocate").find("form");
        var loading = $("#load-allocate");
        var progress = $("#allocate-progress");

        e.preventDefault();
        loading.removeClass('hide');

        $.ajax({
          type: "post",
          url: form.attr('action'),
          data: form.serialize(),
          dataType: "json",
          success: function(response) {
            if(response.error) {
              $(".sup-errors").html("");
              $(".form-control").removeClass("required");
              $.each(response.error, function(index, value) {
                var errors = value.split("|");
                $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
                $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
              });

              loading.addClass("hide");
            } else {
              var ajaxQueue = $({});
              var ctr = -1;

              $.ajaxQueue = function(ajaxOpts) {
                var oldComplete = ajaxOpts.complete;

                ajaxQueue.queue(function(next) {
                  ajaxOpts.complete = function() {
                    if (oldComplete) {
                      oldComplete.apply(this, arguments);
                    }
                    var percent = (((ctr+2) / response.loop) * 100).toFixed(2);
                    progress.attr('aria-valuenow', percent);
                    progress.attr('style', 'width: ' + percent + '%');
                    progress.html(percent + '%');
                    ctr = ctr + 1;

                    if ((ctr+1) == response.loop) {
                      progress.html('Allocating...');
                    }
                    if ((ctr+1) > response.loop) {
                      loading.addClass("hide");
                      status('Success', 'Done.', 'alert-success');
                      $("#modal-allocate").modal("hide");
                      refresh();
                    }
                    next();
                  };

                  $.ajax(ajaxOpts);
                });
              };

              for (var i=1; i<=(response.loop + 1); i++) {
                if (i > response.loop) {
                  progress.html('Creating Batch...');
                  $.ajaxQueue({
                    url: response.url_total,
                    data: { id: response.id, loop: i-1, _token: '{{ csrf_token() }}' },
                    type: "POST",
                    success: function(data) {
                    }
                  });
                } else {
                  $.ajaxQueue({
                    url: response.url,
                    data: { id: response.id, loop: i-1, _token: '{{ csrf_token() }}' },
                    type: "POST",
                    success: function(data) {
                    }
                  });
                }
              }
            }
          }
        });
      });

      $("#page-content-wrapper").on('click', '.btn-allocate', function() {
        var progress = $("#allocate-progress");
        progress.attr('aria-valuenow', 0);
        progress.attr('style', 'width: 0%');
        progress.html("");

        $("#modal-allocate").find('form').trigger("reset");
        $("#allocate-id").val($(this).parent().parent().data('id'));
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-allocate").modal('show');
      });

      $("#page-content-wrapper").on('click', '.btn-showmap', function() {
        if ($loading) {
          return;
        }
        
        $selectedRow = $(this).parent().parent();
        $previousRow = '#row-' + $selectedID;
          $('#row-' + $selectedID).slideUp(function() {
          $($previousRow).parent().parent().remove();
          $button.html('Map');
        });

        if ($selectedID == $selectedRow.data('id')) {
          $selectedID = 0;
          return;
        }

        $selectedID = $selectedRow.data('id');
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('payroll/show-map') }}", { id: $selectedID, _token: $_token }, function(response) {

          var parse = new Array();

          var url = "{{ url('payroll/parse') }}";
          var token = '<input name="_token" type="hidden" value="' + '{{ csrf_token() }}' + '"><input type="hidden" name="parse_id" value="' + $selectedID + '">';
          var form = '<div id="form-parse"><div id="parse-notice"></div><form method="POST" action="' + url + '" accept-charset="UTF-8" role="form" class="form-horizontal" id="form-save_parse" enctype="multipart/form-data">' + token;
          var list =  '<tr><td colspan="6" style="padding:0;">' + form + 
                        '<div class="content-table panel panel-default" id="row-' + $selectedID + '" style="margin-bottom: 0; display: none">' +
                        '<div class="container-fluid">' +
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">' +
                            '<ul class="list-group fetch-data">' +
                              '<li class="list-group-item disabled">FETCHED DATA</li>';
                              
          $.each(response, function(index, value) {
            if (value != '') {
              list += '<li class="list-group-item" style="height:40px">' + value.temp_heading + '<input type="hidden" name="parse_head[' + index + ']" value="' + value.heading + '"></li>';
            }
          });

          list += '</ul>' +
                    '</div>' +
                  '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">' +
                    '<ul class="list-group system-map">' +
                    '<li class="list-group-item disabled">SYSTEM MAP</li>';

          $.each(response, function(index, value) {
            if (value != '') {
              list += '<li class="list-group-item" style="padding:0px; height:40px;">' +
                      '<select id="selectBox" class="form-control borderzero sel-parse" name="parse[' + index + ']" style="border:none; border-radius:0; height:40px;">' +
                      '<option value="">Select:</option>' +
                      '<option value="agent_code">Agent Code</option>' +
                      '<option value="policy_number">Policy No</option>' +
                      '<option value="policy_holder">Policy Holder</option>' +
                      '<option value="policy_nric">Policy Holder NRIC Number</option>' +
                      '<option value="component_code">Component Code</option>' +
                      '<option value="transaction_code">Transaction Code</option>' +
                      '<option value="billing_frequency">Billing Frequency</option>' +
                      '<option value="net_premium_paid">Premium</option>' +
                      '<option value="sum_insured">Sum Insured</option>' +
                      '<option value="premium_term">Premium Term</option>' +
                      '<option value="incept_date">Incept Date</option>' +
                      '</select></li>';
            }
          });

          list +=   '</ul>' +
                      ' <button type="submit" class="btn btn-success borderzero btn-tool pull-right btn-parse"><i class="fa fa-expand"></i> Map</button >' +
                      '&nbsp;<button type="button" class="btn btn-default borderzero btn-tool pull-right btn-close" data-collapse-group="myDivs2" data-toggle="collapse" data-target="#import" class="accordion-toggle"><i class="fa fa-close"></i> Close</button>' +
                      '</div>' +
                    '</div></td></tr></form></div>';

          $selectedRow.after(list);
          $('#row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-expand"></i>');
          $loading = false;
        });
      });
      
      $('#selectBox').val();
    });

    $("#page-content-wrapper").on("click", ".btn-parse", function(e) {
      var form = $("#form-parse").find("form");
      var loading = $(".loading-pane");

      e.preventDefault();
      loading.removeClass('hide');

      $.ajax({
        type: "post",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "json",
        success: function(response) {
          if(response.error) {
            var errors = '';
            $.each(response.error, function(index, value) {
              errors += value;
            });

            status('', errors, 'alert-danger', '#parse-notice');
          } else {
            status(response.title, response.body, 'alert-success');
            refresh();
          }

          loading.addClass('hide');
        }
      });
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();

      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());

        $per = $("#row-per").val();
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $selectedID = 0;
        $loading = false;

        $.post("{{ url('payroll/refresh/upload') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {

          var rows = '';
          $.each(response.rows.data, function(index, row) {
            var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

            rows += '<tr class="' + ((row.status == 9) ? 'danger' : '' ) + '" data-id="' + row.id + '">' +
                      '<td>' + row.display_name + '</td>' +
                      '<td>' + row.feed_type + '</td>' +
                      '<td>' + row.name +'</td>' +
                      '<td>' + ((row.status == 0) ? 'Unmap' : (row.status == 1) ? 'Mapped' : (row.status == 9) ? 'Invalid Document' : 'Allocated') + '</td>' +
                      '<td>' + moment(row.upload_date).format('DD-MM-YYYY') + '</td>' +
                      '<td class="rightalign">' +
                        (row.status == 0 ? '<a type="button" class="btn btn-xs btn-table btn-showmap">Map</a>' : row.status == 1 ? '<a type="button" class="btn btn-xs btn-table btn-allocate">Allocate</a>' : row.status == 9 ? '<a class="btn btn-xs btn-danger btn-error"><i class="fa fa-exclamation-triangle"></i><a> <span class="label label-danger">Invalid Document</span>' : '<span class="label label-default">Allocated</span>' ) + 
                        '&nbsp;<a type="button" class="btn-table btn btn-xs btn-default btn-delete"><i class="fa fa-trash"></i></a>' +
                      '</td>' +
                    '</tr>';
          });

          $('#rows').html(rows);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
        });
      }
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

      $('.loading-pane').removeClass('hide');
      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $selectedID = 0;
      $loading = false;

      $.post("{{ url('payroll/refresh/upload') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {

        var rows = '';
        $.each(response.rows.data, function(index, row) {
          var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

          rows += '<tr class="' + ((row.status == 9) ? 'danger' : '' ) + '" data-id="' + row.id + '">' +
                    '<td>' + row.display_name + '</td>' +
                    '<td>' + row.feed_type + '</td>' +
                    '<td>' + row.name +'</td>' +
                    '<td>' + ((row.status == 0) ? 'Unmap' : (row.status == 1) ? 'Mapped' : (row.status == 9) ? 'Invalid Document' : 'Allocated') + '</td>' +
                    '<td>' + moment(row.upload_date).format('DD-MM-YYYY') + '</td>' +
                    '<td class="rightalign">' +
                      (row.status == 0 ? '<a type="button" class="btn btn-xs btn-table btn-showmap">Map</a>' : row.status == 1 ? '<a type="button" class="btn btn-xs btn-table btn-allocate">Allocate</a>' : row.status == 9 ? '<a class="btn btn-xs btn-danger btn-error"><i class="fa fa-exclamation-triangle"></i><a> <span class="label label-danger">Invalid Document</span>' : '<span class="label label-default">Allocated</span>' ) + 
                      '&nbsp;<a type="button" class="btn-table btn btn-xs btn-default btn-delete"><i class="fa fa-trash"></i></a>' +
                    '</td>' +
                  '</tr>';
        });

        $('#rows').html(rows);
        $('#row-pages').html(response.pages);
        $('.loading-pane').addClass('hide');
      });
    });

  </script>

@stop

@section('content')

  <!-- Page Content -->
  <div id="page-content-wrapper" class="response">
    <!-- Title -->
    <div class="container-fluid main-title-container container-space">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">UPLOAD DATA FEEDS</h3>
        <h5 class="bread-crumb">PAYROLL MANAGEMENT</h5>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <button type="button" class="btn btn-success btn-sm borderzero btn-tool pull-right btn-refresh" onclick="refresh()"><i class="fa fa-refresh"></i> REFRESH</button> 
        <button type="button" class="btn btn-warning btn-sm borderzero btn-tool pull-right btn-import"><i class="fa fa-upload"></i> IMPORT</button> 
      </div>
    </div>
    <div class="container-fluid content-table" id="ajax-data_feeds" style="border-top:1px #e6e6e6 solid;">
      <div class="table-responsive block-content tbblock col-xs-12">
          <div class="loading-pane hide">
            <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
          </div>
          <div class="table-responsive">
            <table class="table table-pagination table-striped">
              <thead class="tbheader">
              <tr class="th-header">
                <th class="th-sort" data-sort="display_name"><i></i> FILE NAME</th>
                <th class="th-sort" data-sort="feed_type"><i></i> FEED TYPE</th>
                <th class="th-sort" data-sort="name"><i></i> PROVIDER</th>
                <th class="th-sort" data-sort="status"><i></i> STATUS</th>
                <th class="th-sort" data-sort="upload_date"><i class="fa fa-sort-up"></i> IMPORT DATE</th>
                <th class="rightalign">TOOLS</th>
              </tr>
            </thead>
            <tbody id="rows">
            @foreach ($rows as $row)
              <tr class="{{ ($row->status == 9) ? 'danger' : '' }}" data-id="{{ $row->id }}"> 
                <td>{{ $row->display_name }}</td> 
                <td>{{ $row->feed_type }}</td> 
                <td>{{ $row->name }}</td> 
                <td>
                @if ($row->status == 0)
                  Unmap
                @elseif ($row->status == 1)
                  Mapped
                @elseif ($row->status == 9)
                  Invalid Document
                @else
                  Allocated
                @endif
                </td>
                <td>{{ date_format(new DateTime($row->upload_date), 'd-m-Y') }}</td> 
                <td  class="rightalign"> 
                @if ($row->status == 0)
                  <a type="button" class="btn btn-xs btn-table btn-showmap">Map</a>
                @elseif ($row->status == 1)
                  <a type="button" class="btn btn-xs btn-table btn-allocate">Allocate</a>
                @elseif ($row->status == 9)
                  <a class="btn btn-xs btn-danger btn-error"><i class="fa fa-exclamation-triangle"></i><a> 
                  <span class="label label-danger">Invalid Document</span>
                @else
                  <span class="label label-default">Allocated</span>
                @endif
                  <a type="button" class="btn-table btn btn-xs btn-default btn-delete"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
    <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="50">100</option>
      </select>
    </div>
    <div class="text-center">
      <input type="hidden" id="row-page" value="1">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-order" value="">
    </div>
    @if (Session::has("notice"))
    <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <i class="fa fa-fw fa-check-circle"></i> {{ Session::get('notice.msg') }}
    </div>
    @endif
  </div>
  <!-- /#page-content-wrapper -->

@include('payroll-management.upload-data-feeds.form')

@stop
