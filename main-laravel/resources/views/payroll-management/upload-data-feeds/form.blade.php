<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'payroll/upload', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-upload"></i> Upload Data Feeds</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                  <label for="provider_id" class="col-sm-3">PROVIDER</label>
                  <div class="col-sm-9 error-provider_id">
                  <select class="form-control borderzero" name="provider_id" id="row-provider_id">
                    <option value="" class="hide">Select:</option>
                    @foreach($providers as $row)
                      <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endforeach
                  </select>
                  <sup class=" sup-errors"></sup>
                </div>
                </div>
                <div class="form-group">
                  <label for="category_id" class="col-sm-3">FEED TYPE</label>
                  <div class="col-sm-9 error-category_id">
                  <select class="form-control borderzero" name="category_id" id="row-category_id">
                  
                  </select>
                  <sup class=" sup-errors"></sup>
                </div>
                </div>
                <div class="form-group">
                  <label for="parser_type" class="col-sm-3">PARSER TYPE</label>
                  <div class="col-sm-9 error-parser_type">
                    <select class="form-control borderzero" name="parser_type" id="row-parser_type">

                    </select>
                  <sup class=" sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group tracking_type hide">
                  <label for="tracking_type" class="col-sm-3">TRACKING TYPE</label>
                  <div class="col-sm-9 error-tracking_type">
                    <div class="checkbox form-control">
                      <label>
                        <input id="row-agent_check" name="agent_check" type="checkbox">
                        Agent Code
                      </label>
                      <label>
                        <input id="row-policy_check" name="policy_check" type="checkbox">
                        Policy Code
                      </label>
                    </div>
                    <sup class=" sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="upload" class="col-sm-3">BROWSE</label>
                  <div class="col-sm-9 error-upload">
                    <input type="file" class="form-control borderzero file file_csv" name="upload" id="row-upload" multiple data-show-upload="false" placeholder="" data-show-preview="false">
                    <sup class=" sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="upload" class="col-sm-3">SET UPLOAD DATE</label>
                  <div class="col-sm-5 error-upload_date ">
                      <div class="input-group row-upload_date">
                        <input type="text" name="upload_date" class="form-control borderzero" id="row-upload_date" maxlength="28" placeholder="">
                        <span class="input-group-addon">
                          <span><i class="fa fa-calendar"></i></span>
                        </span>
                      </div>
                      <sup class="sup-errors"></sup>
                  </div>
                </div>
              </div>
              <div class="modal-footer borderzero">
            <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

      <div class="modal fade" id="modal-allocate" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-allocate" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'payroll/allocate-check', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_allocate', 'files' => 'true')) !!}
              <div class="modal-header modal-info">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="allocate-title">Allocate</h4>
              </div>
              <div class="modal-body">
               <div id="allocate-notice"></div>
                <p>Are you sure you want to allocate ?</p>
              <div class="modal-footer borderzero">
            <div class="progress">
              <div id="allocate-progress" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0%" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                
              </div>
            </div>
                <input type="hidden" name="id" id="allocate-id">
                <button type="submit" class="btn btn-submit borderzero"><i class="fa fa-save"></i> Yes</button>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-error" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => '', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_error', 'files' => 'true')) !!}
              <div class="modal-header modal-danger modal-error-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="error-title">Error Log</h4>
              </div>
              <div class="modal-body">
               <div id="error-notice"></div>
                <div class="form-group">
                  <div class="col-xs-12 error-error_log" style="max-height: 500px; font-size: 13px; overflow-y: scroll;">
                  
                  </div>
                </div>
              <div class="modal-footer borderzero">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="modal-remove-feed" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-remove-feed" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'payroll/delete', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_remove-feed', 'files' => 'true')) !!}
              <div class="modal-header modal-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="remove-feed-title">Data Feed Deletion</h4>
              </div>
              <div class="modal-body">
               <div id="remove-feed-notice"></div>
                <p>Delete Data Feed <strong id="remove-feed-file"></strong>?</p>
              </div>
              <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="remove-feed-id">
                <button type="submit" class="btn btn-submit borderzero"><i class="fa fa-save"></i> Yes</button>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
              </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>