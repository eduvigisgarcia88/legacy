@extends('layouts.master')

@section('scripts')
  <script>

    $(function () {
      $('.row-upload_date').datetimepicker({
        format: 'L'
      });
    });

    // Global Variable
    $_token = '{{ csrf_token() }}';
    $selectedIDDelete = 0;
    $selectedID = 0;
    $loading = false;

    function refresh() {

      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $selectedID = 0;
      $loading = false;

      $.post("{{ url('payroll/refresh/upload') }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

        var rows = '';
        $.each(response.rows.data, function(index, row) {
          var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

            rows += '<tr class="' + ((row.status == 9) ? 'danger' : '' ) + '" data-id="' + row.id + '">' +
                      '<td>' + row.display_name + '</td>' +
                      '<td>' + row.feed_type + '</td>' +
                      '<td>' + row.name +'</td>' +
                      '<td>' + ((row.status == 0) ? 'Unmap' : (row.status == 1) ? 'Mapped' : (row.status == 9) ? 'Invalid Document' : 'Allocated') + '</td>' +
                      '<td>' + moment(row.upload_date).format('MM-DD-YYYY') + '</td>' +
                      '<td class="rightalign">' +
                        (row.status == 0 ? '<a type="button" class="btn btn-xs btn-table btn-showmap">Map</a>' : row.status == 1 ? '<a type="button" class="btn btn-xs btn-table btn-allocate">Allocate</a>' : row.status == 9 ? '<span class="label label-danger">Invalid Document</span>' : '<span class="label label-default">Allocated</span>' ) + 
                        '&nbsp;<a type="button" class="btn-table btn btn-xs btn-default btn-delete"><i class="fa fa-trash"></i></a>' +
                      '</td>' +
                    '</tr>';
        });

        $('#rows').html(rows);
        $('#row-pages').html(response.pages);
        $('.loading-pane').addClass('hide');
        //$('.fetch-data').html(list);
          
      });

      // $('.loading-pane').removeClass('hide');

      // $.post("{{ url('payroll/refresh/upload') }}", { _token: $_token }, function(response) {
          
      //     var rows = '';
      //     $.each(response.rows, function(index, row) {
      //       var list = '<li class="list-group-item disabled">FETCHED DATA</li>';

      //       rows += '<tr data-id="' + row.id + '">' +
      //                 '<td>' + row.display_name + '</td>' +
      //                 '<td>' + row.feed_type + '</td>' +
      //                 '<td>' + row.get_feed_provider.name +'</td>' +
      //                 '<td>' + ((row.status == 0) ? 'Unmap' : (row.status == 1) ? 'Mapped' : 'Allocated') + '</td>' +
      //                 '<td>' + moment(row.upload_date).format('MM-DD-YYYY') + '</td>' +
      //                 '<td class="rightalign">' +
      //                   (row.status == 0 ? '<a type="button" class="btn btn-xs btn-table btn-showmap">Map</a>' : row.status == 1 ? '<a type="button" class="btn btn-xs btn-table btn-allocate">Allocate</a>' : '<span class="label label-default">Allocated</span>' ) + 
      //                   '&nbsp;<a type="button" class="btn-table btn btn-xs btn-default btn-delete"><i class="fa fa-trash"></i></a>' +
      //                 '</td>' +
      //               '</tr>';
      //     });

      //     $('#rows').html(rows);
      //     $('.loading-pane').addClass('hide');
      //     //$('.fetch-data').html(list);
          
      //   });

    }

    $(document).ready(function() {

      $("#row-provider_id").on("change", function() {
        var id = $("#row-provider_id").val();
        $_token = "{{ csrf_token() }}";
        $(".tracking_type").addClass("hide");
        feed = $("#row-category_id");
        feed.html("");
        parser = $("#row-parser_type");
        parser.html("");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        $.post("{{ url('payroll/get-feed') }}", { id: id, _token: $_token }, function(response) {
          if (response.categories.length > 0) {
            feed.append(
              '<option value="" class="hide">Select:</option>'
            );
            $.each(response.categories, function(index, row) {
              feed.append(
                '<option value="' + row.id + '">' + row.name + '</option>'
              );
            });
          }
        }, 'json');
      });

      $("#row-category_id").on("change", function() {
        var id = $("#row-category_id").val();
        $_token = "{{ csrf_token() }}";
        parser = $("#row-parser_type");
        parser.html("");
        $(".tracking_type").addClass("hide");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        $.post("{{ url('payroll/get-parser') }}", { id: id, _token: $_token }, function(response) {
          $("#row-parser_type").append(response);
        }, 'json');
      });

      $("#row-parser_type").on("change", function() {
        var id = $("#row-parser_type").val();
        var tracking = $(".tracking_type");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        if (id == "Custom") {
          tracking.removeClass("hide");
        } else {
          tracking.addClass("hide");
        }

      });

      // Delete File 
      $('#myModaldelete').on('click', '.btn-yes', function() {

        $.post("{{ url('payroll/delete') }}", { id: $selectedIDDelete, _token: $_token }, function(response) {

          toastr[response.type](response.body);
          refresh();

        });

      });


      // disable user
      $("#page-content-wrapper").on("click", ".btn-delete", function() {
        // var id = $(this).parent().parent().data('id');
        // var name = $(this).parent().parent().find('td:nth-child(1)').html();
        $("#remove-feed-id").val($(this).parent().parent().data('id'));
        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-remove-feed").modal("show");
        
        // dialog('Data Feed Deletion', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('payroll/delete') }}", id);
      });

      // Set Delete ID
      // $("#page-content-wrapper").on('click', '.btn-delete', function() {
      //   $selectedIDDelete = $(this).parent().parent().data('id');
      // });

      // Close Map Listings
       $("#page-content-wrapper").on('click', '.btn-close', function() {
        $row = $(this).closest('tr');
        $id = $row.prev().data('id'); 

        $row.find('.content-table').slideUp(function() {
          $row.remove();
        });

        if ($id == $selectedID) {
          $selectedID = 0;
        }
        
       });

      $("#page-content-wrapper").on('click', '.btn-import', function() {
        $("#modal-form").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");

        $(".tracking_type").addClass("hide");
        feed = $("#row-category_id");
        feed.html("");
        parser = $("#row-parser_type");
        parser.html("");
        $("#row-agent_check").prop('checked', false);
        $("#row-policy_check").prop('checked', false);

        $("#row-upload_date").val(moment("{{ Carbon::now() }}").format('MM/DD/YYYY'));

        $(".provider_category").addClass('hide');
        $("#modal-form").modal('show');
      });

    // submit form
    $("#modal-allocate").find("form").submit(function(e) {
   
      var form = $("#modal-allocate").find("form");
      var loading = $("#load-allocate");
      var progress = $("#allocate-progress");


      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {

          $(".sup-errors").html("");
          $(".form-control").removeClass("required");
          $.each(response.error, function(index, value) {
            var errors = value.split("|");
            $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
            $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
          });

          loading.addClass("hide");
          // status('Please correct the following:', errors, 'alert-danger', '#allocate-notice');
        } else {

          var ajaxQueue = $({});
          var ctr = -1;

          $.ajaxQueue = function(ajaxOpts) {
            // Hold the original complete function
            var oldComplete = ajaxOpts.complete;

            // Queue our ajax request
            ajaxQueue.queue(function(next) {
              // Create a complete callback to invoke the next event in the queue
              ajaxOpts.complete = function() {
                // Invoke the original complete if it was there
                if (oldComplete) {
                  oldComplete.apply(this, arguments);
                }
                var percent = (((ctr+2) / response.loop) * 100).toFixed(2);

                  progress.attr('aria-valuenow', percent);
                  progress.attr('style', 'width: ' + percent + '%');
                  progress.html(percent + '%');
                
                  ctr = ctr + 1;

                  if ((ctr+1) == response.loop) {
                    progress.html('Allocating...');
                  }

                  if ((ctr+1) > response.loop) {
                    loading.addClass("hide");
                    status('Success', 'Done.', 'alert-success');
                    $("#modal-allocate").modal("hide");
                    refresh();
                  }
                // Run the next query in the queue
                next();
              };
              // Run the query
              $.ajax(ajaxOpts);
            });

          };

          for (var i=1; i<=(response.loop + 1); i++) {
            if (i > response.loop) {
              progress.html('Creating Batch...');
              $.ajaxQueue({
                url: response.url_total,
                data: { id: response.id, _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }
              });
            } else {
              $.ajaxQueue({
                url: response.url,
                data: { id: response.id, _token: '{{ csrf_token() }}' },
                type: "POST",
                success: function(data) {

                }

              });
            }
          }
        }
      }

      });
    });


      $("#page-content-wrapper").on('click', '.btn-allocate', function() {

        var progress = $("#allocate-progress");
        progress.attr('aria-valuenow', 0);
        progress.attr('style', 'width: 0%');
        progress.html("");

        $("#modal-allocate").find('form').trigger("reset");
        $("#allocate-id").val($(this).parent().parent().data('id'));
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-allocate").modal('show');
      });

      // Show Map Listings
      $("#page-content-wrapper").on('click', '.btn-showmap', function() {
        if ($loading){
          return;
        }
        
        $selectedRow = $(this).parent().parent();

        // Uncollapse and removed 
        $previousRow = '#row-' + $selectedID;
          $('#row-' + $selectedID).slideUp(function() {
          $($previousRow).parent().parent().remove();
          $button.html('Map');
        });

        // Check id is already selected
        if ($selectedID == $selectedRow.data('id')) {

          // Set selected to 0
          $selectedID = 0;

          return;
        }

        $selectedID = $selectedRow.data('id');
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('payroll/show-map') }}", { id: $selectedID, _token: $_token }, function(response) {

          var parse = new Array();

          var url = "{{ url('payroll/parse') }}";
          var token = '<input name="_token" type="hidden" value="' + '{{ csrf_token() }}' + '"><input type="hidden" name="parse_id" value="' + $selectedID + '">';
          var form = '<div id="form-parse"><div id="parse-notice"></div><form method="POST" action="' + url + '" accept-charset="UTF-8" role="form" class="form-horizontal" id="form-save_parse" enctype="multipart/form-data">' + token;
          var list =  '<tr><td colspan="6" style="padding:0;">' + form + 
                        '<div class="content-table panel panel-default" id="row-' + $selectedID + '" style="margin-bottom: 0; display: none">' +
                        '<div class="container-fluid">' +
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">' +
                            '<ul class="list-group fetch-data">' +
                              '<li class="list-group-item disabled">FETCHED DATA</li>';
                              
          $.each(response, function(index, value) {
            if (value != '') {
              list += '<li class="list-group-item" style="height:40px">' + value.temp_heading + '<input type="hidden" name="parse_head[' + index + ']" value="' + value.heading/*.replace(/ /g,"_").toLowerCase()*/ + '"></li>';
            }
          });

          list += '</ul>' +
                    '</div>' +
                  '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">' +
                    '<ul class="list-group system-map">' +
                    '<li class="list-group-item disabled">SYSTEM MAP</li>';

          $.each(response, function(index, value) {
            if (value != '') {
              list += '<li class="list-group-item" style="padding:0px; height:40px;">' +
              '<select id="selectBox" class="form-control borderzero sel-parse" name="parse[' + index + ']" style="border:none; border-radius:0; height:40px;">' +
              '<option value="">Select:</option>' +
              '<option value="agent_code">Agent Code</option>' +
