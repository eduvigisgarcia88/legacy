@extends('layouts.master')

@section('scripts')

<script type="text/javascript">
  $th_header = $(".th-header").html();
  $_token = '{{ csrf_token() }}';

  function refresh() {
    var loading = $(".loading-pane");
    var table = $("#rows");
    loading.removeClass('hide');

    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');
    $(".th-sort").find('i').removeAttr('class');
    $(".th-header").html($th_header);

    $per = $("#row-per").val();
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $selectedID = 0;
    $loading = false;

    $.post("{{ url('payroll/payroll-computation/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
      $('#rows').html(response.table);
      $('#row-pages').html(response.pages);
      loading.addClass('hide');
    });
  }

  function search() {
    var loading = $(".loading-pane");
    var table = $("#rows");
    loading.removeClass('hide');

    $("#row-page").val(1);

    $per = $("#row-per").val();
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $selectedID = 0;
    $loading = false;

    $.post("{{ url('payroll/payroll-computation/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
      $('#rows').html(response.table);
      $('#row-pages').html(response.pages);
      loading.addClass('hide');
    });
  }

  $(document).ready(function() {

      $expand_id = 0;
      $loading_expand = false;
      $agent_id = 0;
      $loading_agent = false;
      $item_id = 0;
      $loading_item = false;
      $type_id = 0;
      $loading_type = false;

      $("#page-content-wrapper").on("click", ".btn-expand", function() {
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');

        $agent_id = 0;
        $item_id = 0;
        $type_id = 0;
        $loading_agent = false;
        $loading_item = false;
        $loading_type = false;

        if ($loading_expand){
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $_token = '{{ csrf_token() }}';

        $selected = '.row-expand-' + $expand_id;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($id == $expand_id) {
          $expand_id = 0;
          return
        }

        $expand_id = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading_expand = true;

        $.post("{{ url('payroll/payroll-computation/get-agents') }}", { id: $id, _token: $_token }, function(response) {
          if (!response.error) {
            $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-expand-' + $expand_id + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-expand-' + $expand_id).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loading_expand = false;
          } else {
            status(false, response.error, 'alert-danger');
          }
        });
      });


      $("#page-content-wrapper").on("click", ".btn-agent-expand", function() {
        $(".btn-agent-expand").html('<i class="fa fa-minus"></i>');
        $(".btn-agent-expand").html('<i class="fa fa-plus"></i>');

        $item_id = 0;
        $type_id = 0;
        $loading_item = false;
        $loading_type = false;

        if ($loading_agent){
          return;
        }

        $tr = $(this).parent().parent();
        $id = $(this).data('batch');
        $user = $(this).data('user');
        $_token = '{{ csrf_token() }}';
        $status = $(this).data('status');

        $selected = '.row-agent-expand-' + $agent_id;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($user == $agent_id) {
          $agent_id = 0;
          return
        }

        $agent_id = $user;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading_agent = true;

        $.post("{{ url('payroll/payroll-computation/get-payroll') }}", { id: $id, user: $user, status: $status, _token: $_token }, function(response) {
          if (!response.error) {
            $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-agent-expand-' + $agent_id + '" style="display:none;">' + response + '</div></td></tr>');

            $('.row-agent-expand-' + $agent_id).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loading_agent = false;
          } else {
            status(false, response.error, 'alert-danger');
          }

        });

      });

      $("#page-content-wrapper").on("click", ".btn-agent-item-expand", function() {
      $(".btn-agent-item-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-agent-item-expand").html('<i class="fa fa-plus"></i>');

        $type_id = 0;
        $loading_type = false;

        if ($loading_item){
          return;
        }

        $tr = $(this).parent().parent();
        $id = $(this).data('batch');
        $user = $(this).data('user');
        $type = $(this).data('type');
        $status = $(this).data('status');
        $_token = '{{ csrf_token() }}';

        $selected = '.row-agent-item-expand-' + $item_id;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($type == $item_id) {
          $item_id = 0;
          return
        }

        $item_id = $type;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading_item = true;
        if ($type == "prepayroll") {
          $.post("{{ url('payroll/payroll-computation/get-pre-payroll') }}", { id: $id, user: $user, status: $status, _token: $_token }, function(response) {
            if (!response.error) {
              $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-agent-item-expand-' + $item_id + '" style="display:none;">' + response + '</div></td></tr>');

              $('.row-agent-item-expand-' + $item_id).slideDown();
              $button.html('<i class="fa fa-minus"></i>');
              $loading_item = false;
            } else {
              status(false, response.error, 'alert-danger');
            }
          });
        } else if ($type == "computation") {
          $.post("{{ url('payroll/payroll-computation/get-policies') }}", { id: $id, user: $user, status: $status, _token: $_token }, function(response) {
            if (!response.error) {
              $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-agent-item-expand-' + $item_id + '" style="display:none;">' + response + '</div></td></tr>');

              $('.row-agent-item-expand-' + $item_id).slideDown();
              $button.html('<i class="fa fa-minus"></i>');
              $loading_item = false;
            } else {
              status(false, response.error, 'alert-danger');
            }
          });
        }

      });

      $("#page-content-wrapper").on("click", ".btn-agent-item-type-expand", function() {
      $(".btn-agent-item-type-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-agent-item-type-expand").html('<i class="fa fa-plus"></i>');

        if ($loading_type){
          return;
        }

        $tr = $(this).parent().parent();
        $policy = $tr.data('id');
        $id = $(this).data('batch');
        $user = $(this).data('user');
        $type = $(this).data('type');
        $status = $(this).data('status');
        $_token = '{{ csrf_token() }}';

        $selected = '.row-agent-item-type-expand-' + $type_id;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($type + $policy == $type_id) {
          $type_id = 0;
          return
        }

        $type_id = $type + $policy;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading_type = true;
        if ($type == "incentives") {
          $.post("{{ url('payroll/payroll-computation/get-incentives') }}", { id: $id, user: $user, status: $status, _token: $_token }, function(response) {
            if (!response.error) {
              $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-agent-item-type-expand-' + $type_id + '" style="display:none;">' + response + '</div></td></tr>');

              $('.row-agent-item-type-expand-' + $type_id).slideDown();
              $button.html('<i class="fa fa-minus"></i>');
              $loading_type = false;
            } else {
              status(false, response.error, 'alert-danger');
            }
          });
        } else if ($type == "deductions") {
          $.post("{{ url('payroll/payroll-computation/get-deductions') }}", { id: $id, user: $user, status: $status, _token: $_token }, function(response) {
            if (!response.error) {
              $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-agent-item-type-expand-' + $type_id + '" style="display:none;">' + response + '</div></td></tr>');

              $('.row-agent-item-type-expand-' + $type_id).slideDown();
              $button.html('<i class="fa fa-minus"></i>');
              $loading_type = false;
            } else {
              status(false, response.error, 'alert-danger');
            }
          });
        } else if ($type == "compute") {
          $.post("{{ url('payroll/payroll-computation/get-computation') }}", { id: $policy, user: $user, status: $status, _token: $_token }, function(response) {
            if (!response.error) {
              $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-agent-item-type-expand-' + $type_id + '" style="display:none;">' + response + '</div></td></tr>');

              $('.row-agent-item-type-expand-' + $type_id).slideDown();
              $button.html('<i class="fa fa-minus"></i>');
              $loading_type = false;
            } else {
              status(false, response.error, 'alert-danger');
            }
          });

        }

      });

      // Close Policy Listings
       $("#page-content-wrapper").on('click', '.btn-policies-close', function() {
        $row = $(this).closest('tr');
        $id = $row.prev().data('id');

        $row.find('.policies-table').slideUp(function() {
          $row.remove();
        });

        if ($id == $expand_id) {
          $expand_id = 0;
        }
        
       });

      // Close Computation
       $("#page-content-wrapper").on('click', '.btn-compute-close', function() {
        $row = $(this).closest('tr');
        $id = $row.prev().data('id');

        $row.find('.compute-table').slideUp(function() {
          $row.remove();
        });

        if ($id == $compute_id) {
          $compute_id = 0;
        }
        
       });
});

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      var loading = $(".loading-pane");
      var table = $("#rows");
      loading.removeClass('hide');

      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());

        $per = $("#row-per").val();
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $selectedID = 0;
        $loading = false;

      $.post("{{ url('payroll/payroll-computation/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
        $('#rows').html(response.table);
        $('#row-pages').html(response.pages);
        loading.addClass('hide');
      });
      }
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {
      var loading = $(".loading-pane");
      var table = $("#rows");
      loading.removeClass('hide');

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

      $('.loading-pane').removeClass('hide');
      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $selectedID = 0;
      $loading = false;

      $.post("{{ url('payroll/payroll-computation/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
        $('#rows').html(response.table);
        $('#row-pages').html(response.pages);
        loading.addClass('hide');
      });
    });

    $(function () {
      $(".export-report").datetimepicker({
          locale: "en",
          format: "MMM YYYY",
          ignoreReadonly: true,
          useCurrent: false
      });
    });

    $('#show-export').click(function(){
      $('#modal-export-report').modal('toggle');
    });

    function saveFile(url, filename) {
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function() {
        var a = document.createElement('a');
        a.href = window.URL.createObjectURL(xhr.response); // xhr.response is a blob
        a.download = filename; // Set the file name.
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
        delete a;
      };
      xhr.open('GET', url);
      // xhr.setRequestHeader('X-CSRF-Token', '{{csrf_token()}}');
      xhr.send();
    }

    var slug = function(str) {
      str = str.replace(/^\s+|\s+$/g, ''); // trim
      str = str.toLowerCase();

      // remove accents, swap ñ for n, etc
      var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
      var to   = "aaaaaeeeeeiiiiooooouuuunc------";
      for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

      return str;
    };

    $('#export-reports-btn').click(function(){
      var date = $('.export-reports-date').val();
      var file = 'report-' + slug(date) + '.xls';
      var uri = '{{url('if/export')}}/' + slug(date);
      $('.export-reports-date').css('background-color','#ffffff');
      $('.export-reports-date').css('background-color','#FFCCCB');
      if(date)
      {
        $('.export-reports-date').css('background-color','#ffffff');
        // saveFile(uri, file);
        $('#download-export').attr('action', uri).submit();

        $('#modal-export-report').modal('toggle');
      }
      else
      {
        $('.export-reports-date').css('background-color','#FFCCCB');
      }
    });

    $(document).on('dp.change','.export-report', function(){
      $('.export-reports-date').css('background-color','#ffffff');
    });
</script>

@stop

@section('content')

  <!-- Page Content -->
  <div id="page-content-wrapper" class="response">
  <!-- Title -->
    <div class="container-fluid main-title-container container-space">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">PAYROLL COMPUTATION</h3>
        <h5 class="bread-crumb">PAYROLL MANAGEMENT</h5>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <button type="button" id="show-export" class="btn btn-success btn-sm borderzero btn-tool pull-right btn-import"><i class="fa fa-print"></i> Export Report</button>
        <form class="hide" id="download-export" action="" method="get">
        </form>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin hide">
        <button type="button" class="btn btn-sm btn-success borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Save PDF</button>
        <button type="button" class="btn btn-sm btn-danger borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Print Summary</button>
      </div>
    </div>
    <div class="container-fluid default-container container-space hide">
      <div class="col-lg-12">
        <p><strong>FILTER OPTIONS</strong></p>
      </div>
      <form class="form-horizontal" >
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group hide-view">
              <label for="" class="col-sm-2 col-xs-5 control-label-left font-color">STATUS</label>
            <div class="col-sm-10 col-xs-7">
              <select class="form-control borderzero" id="search-rank">
                <option value="">All</option>
                <option value="1">Contract</option>
                <option value="0">Component</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group hide-view">
              <label for="" class="col-sm-2 col-xs-5 control-label-left font-color">SEARCH</label>
            <div class="col-sm-10 col-xs-7">
              <input type="text" class="form-control borderzero" onkeyup="search()" placeholder="Search for..." id="search">
            </div>
          </div>
        </div>   
      </form>
    </div>
    <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
      <div class="table-responsive block-content tbblock col-xs-12">
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
        </div>
        <table class="table table-striped table-pagination" id="my-table">
          <thead class="tbheader">
            <tr class="th-header">
              <th class="th-sort" data-sort="batch_date"><i></i> PAYROLL MONTH</th>
              <th class="th-sort" data-sort="start"><i></i> BATCH START</th>
              <th class="th-sort" data-sort="end"><i></i> BATCH END</th>
              <th class="rightalign th-sort" data-sort="gross_total"><i></i> GROSS REVENUE</th>
              <th class="rightalign th-sort" data-sort="status"><i></i> STATUS</th>
            </tr>
          </thead>
          <tbody id="rows">
            <?php echo($table) ?>
          </tbody>
        </table>
      </div>
    </div>
    <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
    <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="50">100</option>
      </select>
    </div>
    <div class="text-center">
      <input type="hidden" id="row-page" value="1">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-order" value="">
    </div>
  </div>
<!-- /#page-content-wrapper -->

@include('payroll-management.payroll-computation.form')

@stop
