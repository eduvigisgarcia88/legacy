@extends('layouts.master')
@section('scripts')
<script type="text/javascript">

  $expand_id = 0;
  $loading_expand = false;
  $type_id = 0;
  $loading_type = false;
  $_token = '{{ csrf_token() }}';

  function formatNumber(num) {
    if (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    } else {
      return '0.00';
    }
  }

  function refresh() {
    $_token = '{{ csrf_token() }}';
    var loading = $(".loading-pane");
    var table = $("#rows");

    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');
    $("#row-filter_status").val('');
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $status = $("#row-filter_status").val();
    $per = $("#row-per").val();

    loading.removeClass("hide");

    $.post("{{ url('payroll/admin-pre-payroll-entries/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
    
      // clear
      table.html("");
      var body = "";

      table.html(response.rows);

      $expand_id = 0;
      $type_id = 0;

      $(".th-sort").find('i').removeAttr('class');
      $('#row-pages').html(response.pages);

      loading.addClass("hide");

    }, 'json');
  }

  function search() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $status = $("#row-filter_status").val();
    $per = $("#row-per").val();

    var loading = $(".loading-pane");
    var table = $("#rows");

    $.post("{{ url('payroll/admin-pre-payroll-entries/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
    
      // clear
      table.html("");
      var body = "";

      table.html(response.rows);

      $expand_id = 0;
      $type_id = 0;

      $('#row-pages').html(response.pages);

      loading.addClass("hide");

    }, 'json');
  
  }

  $(document).ready(function() {
    $(".multi_select").select2();
    
    $("#row-sales_id").on("change", function() {

        batch = $("#row-batch_id");
        var id = $("#row-sales_id").val();
        $_token = "{{ csrf_token() }}";
        batch.html("");

        $.post("{{ url('payroll/admin-pre-payroll-entries/get-batch') }}", { id: id, _token: $_token }, function(response) {

          batch.append(
            '<option value="" class="hide"></option>'
          );
          console.log(response);
            $.each(response.batches, function(index, row) {

              batch.append(
                '<option value="' + row.id + '">' + '[' + row.name + ']' + '-' + moment(row.start).format('MM/DD/YYYY') + '-' + moment(row.end).format('MM/DD/YYYY') +'</option>'
              );

            });
          

        }, 'json');

        batch.select2("val", "");
      
      });

      $("#page-content-wrapper").on("click", ".btn-add", function() {

        // reset all form fields
        $("#modal-form").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $("#row-id").val("");

        $("#row-sales_id").select2("val", "");
        $("#row-policy_id").html("");
        
        $("#modal-form").find('input').removeAttr('readonly', 'readonly');

        //$(".modal-header").removeAttr("class").addClass("modal-header addnew-bg");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add Pre-Payroll Users");
        $("#modal-form").modal('show');
      
      });

      $("#page-content-wrapper").on("click", ".btn-edit", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      $_token = "{{ csrf_token() }}";

      btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

        // reset all form fields
        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        $("#modal-form").find('form').trigger("reset");
        $("#row-id").val("");

            $.post("{{ url('payroll/admin-pre-payroll-entries/view') }}", { id: id, _token: $_token }, function(response) {
                if(!response.error) {
                // set form title
                console.log(response);
                
                    $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View System User</strong>');
                    //$(".pic").html('<img src="uploads/theheck.jpg">');
                    var policy_id = null;
                    // output form data
                    $.each(response, function(index, value) {
                        var field = $("#row-" + index);

                        // field exists, therefore populate
                        if(field.length > 0) {
                            field.val(value);
                        }
                    });

                    $("#row-policy_id").append(
                      '<option value="" class="hide"></option>'
                    );

                    $.each(response.get_pre_payroll_policy.get_policy_sales, function(index, value) {
                      if(index == 'get_user_policies') {
                        $.each(response.get_pre_payroll_policy.get_policy_sales.get_user_policies, function(index2, value2) {
                            $("#row-policy_id").append(
                              '<option value="' + value2.id + '">[' + value2.get_policy_provider.name + '] ' + value2.contract_no + '</option>'
                            );
                        });
                      }
                    });

                    $.each(response.get_pre_payroll_policy, function(index, value) {

                      if (index == 'user_id') {
                        $("#row-sales_id").select2("val", value);
                      }

                      if (index == 'id') {
                        $("#row-policy_id").val(value);
                      }

                    });

                    $("#row-policy_id").select2("val", policy_id);

                    $("#modal-form").find('.hide-view').addClass('hide');
                    $("#modal-form").find('.preview').removeClass('hide');
                    $("#modal-form").find('.change').addClass('hide');
                    $("#modal-form").find('.show-view').removeClass('hide');
                    $("#modal-form").find('input').attr('readonly', 'readonly');
                    // show form
                    $("#modal-form").modal('show');
                } else {
                    status(false, response.error, 'alert-danger');
                }

                btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
            }, 'json');
      });

      $("#page-content-wrapper").on("click", ".btn-view", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      $_token = "{{ csrf_token() }}";


      btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

        // reset all form fields
        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        $("#modal-form").find('form').trigger("reset");
        $("#row-id").val("");

            $.post("{{ url('payroll/admin-pre-payroll-entries/view') }}", { id: id, _token: $_token }, function(response) {
                if(!response.error) {
                // set form title
                
                    $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View System User</strong>');

                    // output form data
                    $.each(response, function(index, value) {
                        var field = $("#row-" + index);


                        // field exists, therefore populate
                        if(field.length > 0) {
                            field.val(value);
                        }
                    });

                    $("#modal-form").find('.hide-view').addClass('hide');
                    $("#modal-form").find('.preview').removeClass('hide');
                    $("#modal-form").find('.change').addClass('hide');
                    $("#modal-form").find('.show-view').removeClass('hide');
                    $("#modal-form").find('input').attr('readonly', 'readonly');
                    // show form
                    $("#modal-form").modal('show');
                } else {
                    status(false, response.error, 'alert-danger');
                }

                btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
            }, 'json');
    });

     // disable user
    $("#page-content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      var name = $(this).parent().parent().find('td:nth-child(3)').html();

      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      
      dialog('Account Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('payroll/admin-pre-payroll-entries/disable') }}", id);
    });

    // enable user
    $("#page-content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      var name = $(this).parent().parent().find('td:nth-child(3)').html();

      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      
      dialog('Account Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('payroll/admin-pre-payroll-entries/enable') }}", id);
    });

    // show modal for multiple disable
    $("#page-content-wrapper").on("click", ".btn-disable-select", function() {
      $("#modal-multiple").find("input").val("");
      $("#row-multiple-hidden").val("0");
      $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Disable");
      $(".modal-multiple-body").html("Are you sure you want to disable the selected pre-payroll entries?");
      $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
      $("#modal-multiple").modal("show");
    });

    // show modal for multiple remove
    $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
      $("#modal-multiple").find("input").val("");
      $("#row-multiple-hidden").val("1");
      $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
      $(".modal-multiple-body").html("Are you sure you want to remove the selected pre-payroll entries?");
      $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
      $("#modal-multiple").modal("show");
    });

    $("#modal-multiple-button").click(function() {

      var ids = [];
      $_token = "{{ csrf_token() }}";

      $("input:checked").each(function(){
          ids.push($(this).val());
      });
      console.log('multiple');

      
      if (ids.length == 0) {
        status("Error", "Select pre-payroll entries first.", 'alert-danger');
      } else {

        if ($("#row-multiple-hidden").val() == "0") {
          $.post("{{ url('payroll/admin-pre-payroll-entries/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
            console.log(response);
            refresh();
          }, 'json');
        } else if ($("#row-multiple-hidden").val() == "1") {
          $.post("{{ url('payroll/admin-pre-payroll-entries/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
            refresh();
          }, 'json');
        } else {
          status("Error", "Error! Refresh the page.", 'alert-danger');
        }
      }

    });

    $("#page-content-wrapper").on("click", ".btn-expand", function() {
      // $(this).find('i').toggleClass('fa-plus fa-minus');
       $(".btn-expand").html('<i class="fa fa-minus"></i>');
       $(".btn-expand").html('<i class="fa fa-plus"></i>');


      $type_id = 0;

      if ($loading_expand){
        return;
      }
      $tr = $(this).parent().parent();
      $id = $tr.data('id');
      $_token = '{{ csrf_token() }}';

      $selected = '.row-expand-' + $expand_id;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

      if ($id == $expand_id) {
        $expand_id = 0;
        return
      }

      $expand_id = $id;
      $button = $(this);
      $button.html('<i class="fa fa-spinner fa-spin"></i>');
      $loading_expand = true;


      $.post("{{ url('payroll/admin-pre-payroll-entries/get-type') }}", { id: $id, _token: $_token }, function(response) {

        if (!response.error) {
          $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-expand-' + $expand_id + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-expand-' + $expand_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        } else {
          status(false, response.error, 'alert-danger');
        }

      });

    });

    $("#page-content-wrapper").on("click", ".btn-type-expand", function() {
    $(".btn-type-expand").html('<i class="fa fa-minus"></i>');
    $(".btn-type-expand").html('<i class="fa fa-plus"></i>');
       
      if ($loading_type){
        return;
      }
      $tr = $(this).parent().parent();
      $id = $tr.data('id');
      $_token = '{{ csrf_token() }}';
      $type = $(this).data('id');

      $selected = '.row-type-' + $type_id;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

      if ($id == $type_id) {
        $type_id = 0;
        return
      }

      $type_id = $id;
      $button = $(this); 
      $button.html('<i class="fa fa-spinner fa-spin"></i>');
      $loading_type = true;

      $.post("{{ url('payroll/admin-pre-payroll-entries/get-adjustments') }}", { type: $id, id: $type, _token: $_token }, function(response) {

        if (!response.error) {
          $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-type-' + $type_id + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-type-' + $type_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_type = false;
        } else {
          status(false, response.error, 'alert-danger');
        }

      });
    });

  });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ url('payroll/admin-pre-payroll-entries/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
          // clear
          table.html("");
          var body = "";
          // clear datatable cache

          table.html(response.rows);


          $expand_id = 0;
          $type_id = 0;

          $('#row-pages').html(response.pages);

          loading.addClass("hide");

        }, 'json');
      
    });

  $("#page-content-wrapper").on('click', '.pagination a', function (event) {
    event.preventDefault();
    if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");

      $.post("{{ url('payroll/admin-pre-payroll-entries/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
      
        // clear
        table.html("");
        var body = "";
        // clear datatable cache

        table.html(response.rows);


        $expand_id = 0;
        $type_id = 0;

        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);

        loading.addClass("hide");

      }, 'json');
    }
  });


</script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">INCENTIVES OR DEDUCTIONS</h3>
        <h5 class="bread-crumb">PAYROLL MANAGEMENT</h5>
    </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
              <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
              <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-disable-select"><i class="fa fa-adjust"></i> Disable</button>     
              <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-add"><i class="fa fa-plus"></i> Add</button>
              <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-refresh" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>
        </div>
  </div>
<div class="container-fluid default-container container-space hide">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
  <form class="form-horizontal" >     
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
                <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH OPTION</strong></p></label>
                        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                              <select class=" form-control borderzero" id="sel1" >
                                <option>All Providers</option>
                                <option>A</option>
                                <option>B</option>
                                <option>C</option>
                                <option>D</option>
                              </select>
                            </div>
                        </div>
                    </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                          <div class="input-group">
                          <input type="text" class="form-control borderzero " placeholder="Search for..." id="search">
                          <span class="input-group-btn">
                            <button class="btn btn-default borderzero" type="button"><i class="fa fa-eraser"></i> <span class="hidden-xs">CLEAR</span></button>
                          </span>
                        </div>
                        </div>
                    </div>
                </div>
              </form>
            </div>
          <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
            <div class="table-responsive block-content tbblock table-pagination col-xs-12">
            <div class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
                </div>
            <div class="table-responsive">  
            <table class="table table-striped" id="tblData">
              <thead class="tbheader">
                <tr>
                  <th class="th-sort" nowrap data-sort="batch_date"><i></i> PAYROLL MONTH</th>
                  <th class="th-sort" nowrap data-sort="start"><i></i> START DATE</th>
                  <th class="th-sort" nowrap data-sort="end"><i></i> END DATE</th>
                  <th class="th-sort" nowrap data-sort="total_incentives"><i></i> TOTAL INCENTIVES</th>
                  <th class="th-sort" nowrap data-sort="total_deductions"><i></i> TOTAL DEDUCTIONS</th>
                  <th class="rightalign th-sort" data-sort="status"><i></i> STATUS</th>
                </tr>
              </thead>
              <tbody id="rows">
                @foreach($rows as $row)
                <tr data-id="{{ $row->id }}">
                  <td><a class="btn btn-table btn-xs btn-expand"><i class="fa fa-plus"></i></a> {{ $row->name }}</td>
                  <td>{{ date_format(date_create(substr($row->start, 0,10)),'m/d/Y') }}</td>
                  <td>{{ date_format(date_create(substr($row->end, 0,10)),'m/d/Y') }}</td>
                  @if ($row->total_incentives)
                    <td>{{ number_format($row->total_incentives, 2) }}</td>
                  @else 
                    <td>0.00</td>
                  @endif
                  @if ($row->total_deductions)
                    <td>{{ number_format($row->total_deductions, 2) }}</td>
                  @else 
                    <td>0.00</td>
                  @endif
                  <td class="rightalign">
                    @if ($row->status == 0)
                      Unreleased
                    @else
                      Released
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>    
        </div>
        <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
        <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
        <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
      </div>
    <div class="text-center">
      <input type="hidden" id="row-page" value="1">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-order" value="">
    </div>
    </div>
    <!-- /#page-content-wrapper -->
@include('payroll-management.admin-pre-payroll-entries.form')
@stop
