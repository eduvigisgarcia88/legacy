<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
       <div class="modal-content borderzero">
           <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
           </div>
          {!! Form::open(array('url' => 'payroll/admin-pre-payroll-entries/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
           <div class="modal-header tbheader">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
           </div>
           <div class="modal-body">
           <div id="form-notice"></div>
            <div class="form-group select-supervisor">
              <label for="row-sales_id" class="col-sm-3">Agent ID</label>
            <div class="col-sm-9 error-sales_id ">
              <select name="sales_id[]" multiple="multiple" id="row-sales_id" class="form-control input-md borderzero sales_id multi_select" style="width: 100%;">
                <option value="" class="hide"></option>
                @foreach($sales as $row)
                  <option value="{{ $row->id }}">[{{ $row->code }}] {{ $row->name }}</option>
                @endforeach
              </select> 
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group select-supervisor">
              <label for="row-batch_id" class="col-sm-3">Payroll Batch</label>
            <div class="col-sm-9 error-batch_id ">
              <select name="batch_id" id="row-batch_id" class="form-control input-md borderzero" style="width: 100%;">

              </select> 
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <hr>
            <div class="form-group">
            <label for="row-type" class="col-sm-3">Type</label>
            <div class="col-sm-9 error-type ">
                  <select name="type" id="row-type" class="form-control borderzero">
                    <option value="" class="hide">Select:</option>
                    <option value="Incentives">Incentives</option>
                    <option value="Deductions">Deductions</option>
                  </select>
                  <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-remarks" class="col-sm-3">Remarks</label>
            <div class="col-sm-9 error-remarks">
              <textarea name="remarks" id="row-remarks" class="form-control borderzero" rows="5" id="comment"></textarea>
              <sup class=" sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-cost" class="col-sm-3">Cost</label>
            <div class="col-sm-9 error-cost ">
              <input name="cost" id="row-cost" type="text" class="form-control borderzero" id="">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            </div>
            <div class="modal-footer borderzero">
            <input type="hidden" name="id" id="row-id" value="">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>