
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LEGACYGROUP</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.2.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/navbar.css" rel="stylesheet">
    <link href="css/content2.css" rel="stylesheet">
    <link href="css/accordion.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-plus"></i> Add New</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">Username:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="email">Name:</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">Confirm Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="">
  </div>
  <div class="form-group">
    <label for="pwd">Type:</label>
    <select id="select" class="form-control borderzero" id="sel1">
        <option>CEO</option>
        <option>IT Users</option>
        <option>Admin Accounts</option>
        <option>Accounting</option>
    </select>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
<div id="myModaldelete" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-minus"></i> Remove</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to remove the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldeactivate" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-adjust"></i> Disable</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to disable the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaledit" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-pencil"></i> Edit</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">Username:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="stevejobs">
  </div>
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="stevejobs@email.com">
  </div>
  <div class="form-group">
    <label for="email">Name:</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="********">
  </div>
  <div class="form-group">
    <label for="pwd">Confirm Password:</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="********">
  </div>
  <div class="form-group">
    <label for="pwd">Type:</label>
    <select id="select" class="form-control borderzero" id="sel1">
        <option>CEO</option>
        <option>IT Users</option>
        <option>Admin Accounts</option>
        <option>Accounting</option>
    </select>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
<div id="myModalprint" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> Print Invoice</h4>
</div>
<div class="modal-body">
<p>Do you want to print the tables?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalsave" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> Save PDF</h4>
</div>
<div class="modal-body">
<p>Do you want to save the tables?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalintroducer" class="modal fade modal-wide" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> INTRODUCER SUMMARY</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
              <table class="table table-hover" style="margin:0; padding:0; background-color:#e6e6e6">
              <thead>
              <tr>
                <th>INTRODUCER ID <i class="fa fa-sort"></i></th>
                <th>INTRODUCER NAME <i class="fa fa-sort"></i></th>
                <th>LEVEL <i class="fa fa-sort"></i></th>
                <th>REMARKS <i class="fa fa-sort"></i></th>
                <th>POLICY CODE <i class="fa fa-sort"></i></th>
                <th>DATE <i class="fa fa-sort"></i></th>
                <th>AMOUNT <i class="fa fa-sort"></i></th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>12345</td>
                <td>Introducer A</td>
                <td>Level 1</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Introducer B</td>
                <td>Level 1</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Introducer C</td>
                <td>Level 1</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td><strong>TOTAL AMOUNT</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>300</strong></td>
              </tr>
              </tbody>
              </table> 
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<div id="myModaladmin" class="modal fade modal-wide" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> ADMIN PRE-PAYROLL SUMMARY</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
              <table class="table table-hover" style="margin:0; padding:0; background-color:#e6e6e6">
              <thead>
              <tr>
                <th>PRE PAYROLL ID <i class="fa fa-sort"></i></th>
                <th>ENTRY TYPE <i class="fa fa-sort"></i></th>
                <th>REMARKS <i class="fa fa-sort"></i></th>
                <th>POLICY CODE <i class="fa fa-sort"></i></th>
                <th>DATE <i class="fa fa-sort"></i></th>
                <th>AMOUNT <i class="fa fa-sort"></i></th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>12345</td>
                <td>Incentives A</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Incentives A</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td>12345</td>
                <td>Deduction A</td>
                <td>Remarks goes here</td>
                <td>12345</td>
                <td>12/06/1985</td>
                <td>100</td>
              </tr>
              <tr>
                <td><strong>TOTAL AMOUNT</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>300</strong></td>
                </tr>
              </tbody>
              </table> 
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<div id="myModalpolicy" class="modal fade modal-wide" role="dialog">
<div class="modal-dialog modal-lg">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-print"></i> POLICY SUMMARY</h4>
</div>
<div class="modal-body">
<div class="table-responsive">
              <table class="table table-hover" style="margin:0; padding:0; background-color:#e6e6e6">
              <thead>
              <tr>
                <th>POLICY ID <i class="fa fa-sort"></i></th>
                <th>POLICY NO <i class="fa fa-sort"></i></th>
                <th>POLICY TYPE <i class="fa fa-sort"></i></th>
                <th>TRANSACTION CODE <i class="fa fa-sort"></i></th>
                <th>POLICY CODE <i class="fa fa-sort"></i></th>
                <th>DATE <i class="fa fa-sort"></i></th>
                <th>AMOUNT <i class="fa fa-sort"></i></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                  <td>12345</td>
                  <td>12345</td>
                  <td>Plan A</td>
                  <td>12345</td>
                  <td>12345</td>
                  <td>12/06/1985</td>
                  <td>100</td>
              </tr>
              <tr>
                  <td>12345</td>
                  <td>12345</td>
                  <td>Plan B</td>
                  <td>12345</td>
                  <td>12345</td>
                  <td>12/06/1985</td>
                  <td>100</td>
              </tr>
              <tr>
                  <td>12345</td>
                  <td>12345</td>
                  <td>Plan C</td>
                  <td>12345</td>
                  <td>12345</td>
                  <td>12/06/1985</td>
                  <td>100</td>
              </tr>
              <tr>
                  <td><strong>TOTAL AMOUNT</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><strong>300</strong></td>
              </tr>
            </tbody>
            </table> 
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                    <a href="#menu-toggle3" id="menu-toggle3" ><i class="fa fa-close pull-right" style="color: #79828f; padding-top: 24px; padding-right: 18px;"></i></a>
                <li class="sidebar-brand">
                    <a href="#" style="color: #79828f;">
                    <i class="fa fa-cubes" style="color: #79828f;"></i><span id="hide"> LEGACYGROUP</span>
                    </a>
                </li>
                <div id="side">
                <li>
                    <a href="dashboard.html"><i class="fa fa-cube"></i><span id="hide"> Dashboard</span></a>
                </li>
                <div class="panel-group" id="accordion">
                  
      <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion"
                href="#accordionOne">
          <h5 class="panel-title">
            <a>
                <i class="fa fa-users"></i><span id="hide"> User Management</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h5>
        </div>
        <li id="accordionOne" class="panel-collapse collapse">
    <ul id="menu" class="inner menu-pad">
      <div class="panel-group" id="accordion1">
        <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"
                href="#a" style="background-color: transparent; padding-left: 0px;">
          <h5 class="panel-title">
            <a>
              <span id="hide"> System Users</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h5>
        </div>
        <li id="a" class="panel-collapse collapse">
           <ul id="menu" class="inner menu-padd">
                    <li><a href="system-users-all.html">All</a></li>
                    <li><a href="system-users-ceo.html">CEO</a></li>
                    <li><a href="system-users-it-users.html">IT Users</a></li>
                    <li><a href="system-users-admin-accounts.html">Admin Accounts</a></li>
                    <li><a href="system-users-accounting.html">Accounting</a></li>
                    <li><a href="system-users-sales-assistant.html">Sales Assistant</a></li>
                    <li><a href="system-users-permissions.html">Permissions</a></li>
                    <li><a href="system-users-log.html">System User Log</a></li>                
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"
                href="#b" style="background-color: transparent; padding-left: 0px;">
          <h5 class="panel-title">
            <a>
              <span id="hide"> Sales Users</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h5>
        </div>
        <li id="b" class="panel-collapse collapse">
           <ul id="menu" class="inner menu-padd">
                    <li><a href="agent-management-all.html">All</a></li>
                    <li><a href="agent-management-sales-agent.html">Sales Agent</a></li>
                    <li><a href="agent-management-assistant-manager.html">Assistant Manager</a></li>
                    <li><a href="agent-management-manager.html">Manager</a></li>
                    <li><a href="agent-management-assistant-director.html">Assistant Director</a></li>
                    <li><a href="agent-management-director.html">Director</a></li>
                    <li><a href="agent-management-partner.html">Partner</a></li>
                    <div class="panel panel-default">
                      <div class="panel-heading"  data-toggle="collapse" data-parent="#"
                              href="#e" style="background-color: transparent; padding-left: 0px;">
                        <h5 class="panel-title">
                          <a>
                            <span id="hide"> Team Management</span>
                            <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
                          </a>
                        </h5>
                      </div>
                      <li id="e" class="panel-collapse collapse">
                         <ul id="menu" class="inner menu-padding">
                                  <li><a href="agent-management-all2.html">All</a></li>
                                  <li><a href="agent-management-incomplete.html">Incomplete</a></li>
                                  <li><a href="agent-management-duplicate.html">Duplicate</a></li>
                          </ul>
                      </li>
                    </div>
                    <li><a href="agent-management-log.html">Sales User Log</a></li>
            </ul>
        </li>
      </div>
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion"
               href="#accordionFour">
          <h4 class="panel-title">
            <a style="color:white;">
              <i class="fa fa-credit-card"></i><span id="hide"> Payroll Management</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        <li id="accordionFour" class="panel-collapse collapse in">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="policy-management-import-feeds.html">Upload Data Feeds</a></li>
                    <li><a href="payroll-management-generate-payroll.html">Generate Payroll</a></li>
                    <li><a href="payroll-management-pre-payroll-entries.html">Admin Pre-payroll Entries</a></li>
                    <li><a href="payroll-management-computation2.html">Payroll Computation</a></li>
                    <li><a href="payroll-management-total-invoice-advisor.html">Payroll Summary (Advisor)</a></li>
                    <li><a href="payroll-management-total-invoice-supervisor.html">Payroll Summary (Supervisor)</a></li>
                    
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" 
               href="#accordionThree">
          <h4 class="panel-title">
            <a>
              <i class="fa fa-book"></i><span id="hide"> Policy Management</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        
        <li id="accordionThree" class="panel-collapse collapse">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="policy-management-policy.html">Policies</a></li>
                    <li><a href="policy-management-riders.html">Riders</a></li>
                    <li><a href="policy-management-introducers.html">Introducers</a></li>
                    <li><a href="policy-management-log.html">Policy History</a></li>
                    <li><a href="policy-management-orphan-policy.html">Orphan Policies</a></li>
                    <li><a href="policy-management-duplicate.html">Duplicates</a></li>
                    
                    <div class="panel panel-default">
                      <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"
                              href="#c" style="background-color: transparent; padding-left: 0px;">
                        <h5 class="panel-title">
                          <a>
                            <span id="hide"> Production</span>
                            <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
                          </a>
                        </h5>
                      </div>
                      <li id="c" class="panel-collapse collapse">
                         <ul id="menu" class="inner menu-padd">
                                  <li><a href="production-cases.html">Case Submission (Advisor)</a></li>
                                  <li><a href="production-cases-supervisor.html">Case Submission (Supervisor)</a></li>
                                  <li><a href="production-incept.html">Case Inceptions (Advisor)</a></li>
                                  <li><a href="production-incept-supervisor.html">Case Inceptions (Supervisor)</a></li>
                          </ul>
                      </li>
                    </div>
            </ul>
        </li>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion"
               href="#accordionFive">
          <h4 class="panel-title">
            <a>
              <i class="fa fa-clipboard"></i><span id="hide"> Provider & Products Mgt</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        <li id="accordionFive" class="panel-collapse collapse">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="provider&products-provider-list.html">Provider List</a></li>
                    <li><a href="provider&products-product-list.html">Product List</a></li>
                    <li><a href="provider&products-product-log.html">Product Log</a></li>
            </ul>
        </li>
      </div>
     <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion"
               href="#accordionNine">
          <h4 class="panel-title">
            <a>
              <i class="fa fa-credit-card"></i><span id="hide"> Summary and Reports</span>
              <i class="fa fa-angle-down pull-right" style="margin-right: 20px;"></i>
            </a>
          </h4>
        </div>
        <li id="accordionNine" class="panel-collapse collapse">
          <ul id="menu1" class="inner menu-pad">
                    <li><a href="reports-view-firm.html">View Firm Revenue Summary</a></li>
                    <li><a href="reports-view-daily.html">View Daily Production Report</a></li>
                    <li><a href="reports-export.html">Export Daily Production Report</a></li>   
          </ul>
        </li>
      </div>
    </div>
  </div>
</ul>
</div>
        <!-- /#sidebar-wrapper -->
        <!-- Static navbar -->
        <nav class="navbar navbar-default navbar-static-top" style="background-color: #ffffff">
        <div class="container-fluid">
        <div class="pull-left">
        <a ></a>
        <a href="#menu-toggle" id="menu-toggle" type="button" class="btn btn-default"><i class="fa fa-bars" style="color: #79828f;"></i></a>
        </div>
        <div style="text-align: right; padding-left: 20px">
        <button id="bell"type="button" class="btn btn-default"><i class="fa fa-bell" style="color: #79828f;"></i></button>
        <button id="profile"class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">username <img src="images/avatar.png" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30">
        <i class="fa fa-angle-down" style="color: #79828f;"></i></button>
        <ul id="userdrop" class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Inbox <i class="fa fa-envelope pull-right"></i></i></a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Profile <i class="fa fa-user pull-right"></i></a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Settings <i class="fa fa-cog pull-right"></i></a></li>
          <li role="presentation" class="divider"></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Log out <i class="fa fa-sign-out pull-right"></i></a></li>
        </ul>
        </div><!--/.nav-collapse -->
        </div>
        </nav>
        <!-- Page Content -->
        <div id="page-content-wrapper" class="response">
        <div class="container-fluid">
        <!-- Title -->
        <div class="row main-title-container container-space">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3 class="main-title">PAYROLL MANAGEMENT</h3>
                <h5 class="bread-crumb">PAYROLL MANAGEMENTY > INVOICE SUMMARY (SUPERVISOR) <h5>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <button type="button" class="btn btn-success borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Save PDF</button>
                <button type="button" class="btn btn-danger borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Print Summary</button>
          </div>
          </div>
          <div id="basicinfo" class="row default-container container-space">
              <div class="col-lg-12">
                  <p><strong>BASIC INFORMATION</strong></p>
              </div>
                <form class="form-horizontal" >     
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>AGENT NAME</strong></p></div>
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                <p class="p">Steve Jobs</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                          <div class="form-group">
                                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>DESIGNATION</strong></p></div>
                                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                        <p class="p">Sales Agent</p>
                                    </div>
                          </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" >
                          <div class="form-group">
                                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>AGENT ID</strong></p></div>
                                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                        <p class="p">12345</p>
                                    </div>
                          </div>
                    </div>
                </form>
          </div>
          <div class="row default-container container-space">
              <div class="col-lg-12">
                  <p><strong>FILTER OPTIONS</strong></p>
              </div>
              <form class="form-horizontal" >     
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PROVIDER</strong></p></label>
                            <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                <select class="select form-control borderzero" id="sel1">
                                  <option>All Providers</option>
                                  <option>A</option>
                                  <option>B</option>
                                  <option>C</option>
                                  <option>D</option>
                                </select>
                            </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD FROM</strong></p></label>
                              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                  <select class="select form-control borderzero" id="sel1" >
                                    <option>Jan 2013</option>
                                    <option>Feb 2013</option>
                                    <option>Mar 2013</option>
                                    <option>Apr 2013</option>
                                    <option>May 2013</option>
                                  </select>
                              </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD TO</strong></p></label>
                              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                  <select class="select form-control borderzero" id="sel1" >
                                    <option>Mar 2015</option>
                                    <option>Apr 2015</option>
                                    <option>May 2015</option>
                                    <option>Jun 2015</option>
                                    <option>Jul 2015</option>
                                  </select>
                              </div>
                      </div>
                    </div>
              </form>
          </div>
<div class="content-table" style="border-top:1px #e6e6e6 solid;">
    <div class="panel panel-default">
        <div class="row table-responsive panel-body">
            <table id="invoicetable" class="table">
                <thead id="thead">
                    <tr>
                      <th class="col-lg-3 col-xs-3">PERIOD <i class="fa fa-sort"></i></th>
                      <th class="col-lg-3 col-xs-3">ADVISOR <i class="fa fa-sort"></i></th>
                      <th class="col-lg-3 col-xs-3">INTRODUCER <i class="fa fa-sort"></i></th>
                      <th class="col-lg-3 col-xs-3">GROSS COMMSSION <i class="fa fa-sort"></i></th>
                    </tr>
                </thead>
                <tbody> 
                    <tr>
                      <td><strong> Jan 2015</strong></td>
                      <td data-collapse-group="myDivs2" data-toggle="collapse" data-target="#jan" class="accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 300</strong></td>
                      <td><strong> 300</strong></td>
                      <td><strong>400</strong></td>
                    </tr>
<td colspan="6" style="padding:0;">
  <div class="panel panel-default accordian-body collapse" id="jan" style="margin-bottom: 0;">
    <div class="panel-body">
      <table class="table">
          <thead>
              <tr>
                <th class="col-lg-10 col-xs-2">ITEM</th>
                <th class="col-lg-2 col-xs-2">AMOUNT ($)</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                <td data-collapse-group="myDivs" data-toggle="collapse" data-target="#commissions" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> COMISSIONS</strong></td>
                <td><strong>311.67</strong></td>
              </tr>
              <td colspan="7" style="padding:0">
  <!--  Commissions Table -->
  <div class="panel panel-default accordian-body collapse"  id="commissions">
      <div class="panel-body">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
              <h4 style="color: #666666;">COMMISIONS</h4>
          </div>      
                <table class="table">
                    <thead>
                        <tr>
                          <th>PROVIDER <i class="fa fa-sort"></i></th>
                          <th>1ST YEAR COMM <i class="fa fa-sort"></i></th>
                          <th>RENEWAL COMM <i class="fa fa-sort"></i></th>
                          <th>TOTAL ($) <i class="fa fa-sort"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>Aviva</td>
                          <td data-collapse-group="" data-toggle="collapse" data-target="#int" class="accordion-toggle"><i class="fa fa-plus-square-o"></i> 311.67</td>
                          <td>0</td>
                          <td></td>
                        </tr>
  <td colspan="7" style="padding:0">
  <div class="panel panel-default accordian-body collapse lifetable" id="int">
  <div class="panel-body">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <h4 style="color: #666666;">INTRODUCER</h4>
      </div>   
        <table class="table">
            <thead>
                <tr>
                  <th class="col-xs-2">INTRODUCER</th>
                  <th class="col-xs-2">LEVEL</th>
                  <th class="col-xs-2">INTRODUCER</th>
                  <th class="col-xs-2">ADVISOR</th>
                  <th class="col-xs-2">TOTAL ($)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td><strong>Advisor</strong></td>
                  <td><strong>Level 1</strong></td>
                  <td data-collapse-group="" data-toggle="collapse" data-target="#introducer" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 900</strong></td>
                  <td></td>
                  <td></td>
                </tr>
                <td colspan="7" style="padding:0">
<!-- Introducer Table -->
<div class="panel panel-default accordian-body collapse"  id="introducer">
    <div class="panel-body">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
            <h4 style="color: #666666;">INTRODUCER BREAKDOWN</h4>
        </div>
          <table class="table">
              <thead>
                  <tr>
                    <th>CLIENT <i class="fa fa-sort"></i></th>
                    <th>PLAN <i class="fa fa-sort"></i></th>
                    <th>INTRODUCER FEE ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>Client Given Name 1</td>
                    <td>Plan 1</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Client Given Name 2</td>
                    <td>Plan 2</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Client Given Name 3</td>
                    <td>Plan 3</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><strong>TOTAL AMOUNT</strong></td>
                    <td></td>
                    <td><strong></strong></td>
                  </tr>
              </tbody>
          </table> 
          <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#introducer" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
          <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
          <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
          </div>
          </div>
                <tr>
                  <td><strong>Introducer 1</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><strong>Introducer 2</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><strong>Introducer 3</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><strong>TOTAL</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
          </td>
        </tbody>
      </table>
      <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#int" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
      <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
      <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
    </div>
  </div>
      <tr>
        <td>Allianz</td>
        <td></i> 0</td>
        <td data-collapse-group="" data-toggle="collapse" data-target="#renewal" class=" accordion-toggle"><i class="fa fa-plus-square-o"></i> 0</td>
        <td></td>
      </tr>
      <tr>
<td colspan="7" style="padding:0">
<!--  Renewal Table -->
<div class="panel panel-default accordian-body collapse"  id="renewal">
  <div class="panel-body">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>LIFE</h4>
      </div>     
        <table class="table">
            <thead>
                <tr>
                  <th>BILL DATE (REF DATE) <i class="fa fa-sort"></i></th>
                  <th>POLICY NUMBER <i class="fa fa-sort"></i></th>
                  <th>PRODUCT NAME <i class="fa fa-sort"></i></th>
                  <th>POLICY OWNER <i class="fa fa-sort"></i></th>
                  <th>PREMIUM ($)<i class="fa fa-sort"></i></th>
                  <th>COMMISION ($) <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td>DD-MM-YY</td>
                  <td>Policy 1</td>
                  <td></td>
                  <td>Steve Jobs</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>DD-MM-YY</td>
                  <td>Policy 2</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
            </tbody>
        </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>HEALTH</h4>
      </div>     
          <table class="table">
              <thead>
                  <tr>
                    <th>BILL DATE (REF DATE) <i class="fa fa-sort"></i></th>
                    <th>POLICY NUMBER <i class="fa fa-sort"></i></th>
                    <th>PRODUCT NAME <i class="fa fa-sort"></i></th>
                    <th>POLICY OWNER <i class="fa fa-sort"></i></th>
                    <th>PREMIUM ($)<i class="fa fa-sort"></i></th>
                    <th>COMMISION ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 1</td>
                    <td></td>
                    <td>Steve Jobs</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 2</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>INVESTMENT</h4>
      </div>     
          <table class="table">
              <thead>
                  <tr>
                    <th>BILL DATE (REF DATE) <i class="fa fa-sort"></i></th>
                    <th>POLICY NUMBER <i class="fa fa-sort"></i></th>
                    <th>PRODUCT NAME <i class="fa fa-sort"></i></th>
                    <th>POLICY OWNER <i class="fa fa-sort"></i></th>
                    <th>PREMIUM ($)<i class="fa fa-sort"></i></th>
                    <th>COMMISION ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 1</td>
                    <td></td>
                    <td>Steve Jobs</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 2</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
      <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#renewal" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
  </div>
</div>      
                    <tr>
                      <td>Maybank-HL</td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td><strong>TOTAL AMOUNT</strong></td>
                      <td>311.67</td>
                      <td></td>
                      <td><strong></strong></td>
                      </tr>
                </tbody>
          </table>
      <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#commissions" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
      <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
      <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button> 
  </div>
  </div>


        <!-- Policy Table -->
        <div class="panel panel-default accordian-body collapse"  id="policy" style="margin:0; padding:0; background-color:#e6e6e6">
            <div class="panel-body">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                    <h4>POLICY SUMMARY</h4>
                </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"style="padding-right: 0;">
                        <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalpolicy"><i class="fa fa-expand"></i> Expand</button>
                        <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
                        <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
                    </div>
                  <table class="table table-hover" style="margin:0; padding:0; background-color:#e6e6e6">
                      <thead>
                          <tr>
                            <th>POLICY ID <i class="fa fa-sort"></i></th>
                            <th>POLICY NO <i class="fa fa-sort"></i></th>
                            <th>POLICY TYPE <i class="fa fa-sort"></i></th>
                            <th>TRANSACTION CODE <i class="fa fa-sort"></i></th>
                            <th>POLICY CODE <i class="fa fa-sort"></i></th>
                            <th>DATE <i class="fa fa-sort"></i></th>
                            <th>AMOUNT <i class="fa fa-sort"></i></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>12345</td>
                              <td>12345</td>
                              <td>Plan A</td>
                              <td>12345</td>
                              <td>12345</td>
                              <td>12/06/1985</td>
                              <td>100</td>
                          </tr>
                          <tr>
                              <td>12345</td>
                              <td>12345</td>
                              <td>Plan B</td>
                              <td>12345</td>
                              <td>12345</td>
                              <td>12/06/1985</td>
                              <td>100</td>
                          </tr>
                          <tr>
                              <td>12345</td>
                              <td>12345</td>
                              <td>Plan C</td>
                              <td>12345</td>
                              <td>12345</td>
                              <td>12/06/1985</td>
                              <td>100</td>
                          </tr>
                          <tr>
                              <td><strong>TOTAL AMOUNT</strong></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td><strong>300</strong></td>
                          </tr>
                      </tbody>
                  </table>    
              </div>
        </div>  
                          <tr>
                              <td><strong>OVERRIDES</strong></td>
                              <td data-collapse-group="" data-toggle="collapse" data-target="#overridesvalue" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 39.96</strong></td>
                          </tr>
<!--  Overrides Table -->
<td colspan="7" style="padding:0">
<div class="panel panel-default accordian-body collapse"  id="overridesvalue">
  <div class="panel-body">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4 style="color: #666666;">DIRECT TEAM</h4>
      </div>      
        <table class="table">
            <thead>
                <tr>
                  <th># <i class="fa fa-sort"></i></th>
                  <th>TEAM MEMBERS <i class="fa fa-sort"></i></th>
                  <th>RANK <i class="fa fa-sort"></i></th>
                  <th>STATUS <i class="fa fa-sort"></i></th>
                  <th>1ST YEAR<i class="fa fa-sort"></i></th>
                  <th>RENEWALS <i class="fa fa-sort"></i></th>
                  <th>AMOUNT ($) <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td>1</td>
                  <td>Jerry Chop</td>
                  <td>Director</td>
                  <td>Active</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Avis Ong</td>
                  <td>Advisor</td>
                  <td>Active</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Kevin Tan</td>
                  <td>Advisor</td>
                  <td>Active</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
            </tbody>
        </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4 style="color: #666666"> BUYOUT PAYMENTS</h4>
      </div>     
          <table class="table">
              <thead>
                  <tr>
                    <th># <i class="fa fa-sort"></i></th>
                    <th>AGENT NAME <i class="fa fa-sort"></i></th>
                    <th>RANK <i class="fa fa-sort"></i></th>
                    <th>SUPERVISOR <i class="fa fa-sort"></i></th>
                    <th>RANK <i class="fa fa-sort"></i></th>
                    <th>EFFECTIVE <i class="fa fa-sort"></i></th>
                    <th>AMOUNT <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>1</td>
                    <td>Enry Ong</td>
                    <td>Manager</td>
                    <td>Alvin Nathan</td>
                    <td>Partner</td>
                    <td>Jan 20 2016</td>
                    <td>38.96</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>TOTAL</td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4 style="color: #666666">INDIRECT TEAM</h4>
      </div>     
          <table class="table" style="margin:0; padding:0;">
              <thead>
                  <tr>
                    <th># <i class="fa fa-sort"></i></th>
                    <th>TEAM MEMBERS <i class="fa fa-sort"></i></th>
                    <th>RANK <i class="fa fa-sort"></i></th>
                    <th>STATUS <i class="fa fa-sort"></i></th>
                    <th>1ST YEAR <i class="fa fa-sort"></i></th>
                    <th>RENEWAL <i class="fa fa-sort"></i></th>
                    <th>AMOUNT ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>1</td>
                    <td>Manager1</td>
                    <td>Manager</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Advisor M1</td>
                    <td>Advisor</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>TEAM TOTAL</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
    <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#overridesvalue" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
    <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
    <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
  </div>
  </div>
</td>
                  <tr>
                      <td><strong>INCENTIVES</strong></td>
                      <td  data-collapse-group="" data-toggle="collapse" data-target="#incentives" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 0</strong></td>
                  </tr>    
<td colspan="7" style="padding:0">
<!--  Incentives Table -->
<div class="panel panel-default accordian-body collapse"  id="incentives">
<div class="panel-body">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h4>POLICY RELATED INCENTIVES</h4>
    </div>       
      <table class="table">
          <thead>
              <tr>
                <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
              </tr>
          </thead>
          <tbody>
                <tr>
                  <td>Policy Incentive</td>
                  <td></td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td></td>
                </tr>
          </tbody>
      </table>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>BUSINESS INCENTIVES</h4>
      </div>      
        <table class="table">
            <thead>
                <tr>
                  <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                  <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   <td>OTHERS</td>
                  <td></td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td></td>
                </tr>
                <tr>
                  <td>OVERALL TOTAL</td>
                  <td></td>
                </tr>
            </tbody>
        </table>
        <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#incentives" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
        <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
        <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
  </div>
</td>
              <tr>
                <td data-collapse-group="myDivs" data-toggle="collapse" data-target="#deductions" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> DEDUCTIONS</strong></td>
                <td><strong>0</strong></td>
              </tr>
<td colspan="7" style="padding:0">
<!--  Deductions Table -->
<div class="panel panel-default accordian-body collapse"  id="deductions">
    <div class="panel-body">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
            <h4>POLICY RELATED CHARGERS</h4>
        </div>        
          <table class="table">
              <thead>
                  <tr>
                    <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                    <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>Medical</td>
                    <td>4000</td>
                  </tr>
                  <tr>
                    <td>Clawbaks</td>
                    <td>50</td>
                  </tr>
                  <tr>
                    <td>Others</td>
                    <td></td>
                  </tr>
              </tbody>
          </table>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>BUSINESS CHARGES</h4>
      </div>
          <table class="table">
              <thead>
                  <tr>
                    <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                    <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>Photocopy</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Base Charge</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Office Rents</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Others</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>OVERALL TOTAL</td>
                    <td></td>
                  </tr>
              </tbody>
        </table>
        <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#deductions" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
        <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
        <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
    </div>
</div>
</td>
                  <tr>
                            <td><strong>TOTAL</strong></td>
                            <td><strong>311.67</strong></td>
                          </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
                <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
              </div>
            </div>
          </td>
          </td>
          <tr>
            <td></span><strong> Feb 2015</strong> </td>
            <td><strong></strong></td>
            <td><strong></strong></td>
            <td><strong></strong></td>
          </tr>
          <tr>
            <td></span><strong> Mar 2015</strong> </td>
            <td><strong></strong></td>
            <td><strong></strong></td>
            <td><strong></strong></td>
          </tr>
          <tr>
            <td></span><strong>TOTAL</strong> </td>
            <td><strong></strong></td>
            <td><strong></strong></td>
            <td><strong></strong></td>
          </tr>
          </tbody>
          </table>
          </div>
      </div> 
    </div>  
    <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    <script>
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").addClass("toggled");
        });
        $("#menu-toggle3").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        });
        $("#menu-toggle3").click(function(e) {
        e.preventDefault();
        $(".collapse").collapse('hide');
        });
    $("ul .sidebar-nav").click(function(){
    $("#wrapper").addClass("toggled");
    });
    $("#sidebar-wrapper i").click(function(){
    $("#wrapper").addClass("toggled");
    });
    $("#side").click(function(){
    $("#wrapper").addClass("toggled");
    });
    }
    </script>
    <script>
    $("[data-collapse-group='myDivs']").click(function () {
    var $this = $(this);
    $("[data-collapse-group='myDivs']:not([data-target='" + $this.data("target") + "'])").each(function () {
        $($(this).data("target")).removeClass("in").addClass('collapse');
    });
    });
    </script>
    <script>
    $("[data-collapse-group='myDivs2']").click(function () {
    var $this = $(this);
    $("[data-collapse-group='myDivs2']:not([data-target='" + $this.data("target") + "'])").each(function () {
        $($(this).data("target")).removeClass("in").addClass('collapse');
    });
    });
    </script>
    <script type="text/javascript">
    $(".modal-wide").on("show.bs.modal", function() {
    var height = $(window).height() - 200;
    $(this).find(".modal-body").css("max-height", height);
    });</script>
</body>

</html>
