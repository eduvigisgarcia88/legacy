@extends('layouts.master')

@section('content')


@include('payroll-management.payroll-summary-supervisor.form')

<!-- Page Content -->
        <div id="page-content-wrapper" class="response">
        <!-- Title -->
        <div class="container-fluid main-title-container container-space">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
                <h3 class="main-title">PAYROLL MANAGEMENT</h3>
                <h5 class="bread-crumb">PAYROLL MANAGEMENTY > INVOICE SUMMARY (SUPERVISOR) <h5>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
                <button type="button" class="btn btn-sm btn-success borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Save PDF</button>
                <button type="button" class="btn btn-sm btn-danger borderzero pull-right btn-tool" data-toggle="modal" data-target="#"><i class="fa fa-print"></i> Print Summary</button>
          </div>
          </div>
          <div id="basicinfo" class="container-fluid default-container container-space">
              <div class="col-lg-12">
                  <p><strong>BASIC INFORMATION</strong></p>
              </div>
                <form class="form-horizontal" >     
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>AGENT NAME</strong></p></div>
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                <p class="p">Steve Jobs</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                          <div class="form-group">
                                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>DESIGNATION</strong></p></div>
                                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                        <p class="p">Sales Agent</p>
                                    </div>
                          </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12" >
                          <div class="form-group">
                                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-6" style="text-align:left"><p class="p"><strong>AGENT ID</strong></p></div>
                                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                        <p class="p">12345</p>
                                    </div>
                          </div>
                    </div>
                </form>
          </div>
          <div class="container-fluid default-container container-space">
              <div class="col-lg-12">
                  <p><strong>FILTER OPTIONS</strong></p>
              </div>
              <form class="form-horizontal" >     
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PROVIDER</strong></p></label>
                            <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                <select class="select form-control borderzero" id="sel1">
                                  <option>All Providers</option>
                                  <option>A</option>
                                  <option>B</option>
                                  <option>C</option>
                                  <option>D</option>
                                </select>
                            </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD FROM</strong></p></label>
                              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                  <select class="select form-control borderzero" id="sel1" >
                                    <option>Jan 2013</option>
                                    <option>Feb 2013</option>
                                    <option>Mar 2013</option>
                                    <option>Apr 2013</option>
                                    <option>May 2013</option>
                                  </select>
                              </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" >
                      <div class="form-group">
                          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>PERIOD TO</strong></p></label>
                              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                                  <select class="select form-control borderzero" id="sel1" >
                                    <option>Mar 2015</option>
                                    <option>Apr 2015</option>
                                    <option>May 2015</option>
                                    <option>Jun 2015</option>
                                    <option>Jul 2015</option>
                                  </select>
                              </div>
                      </div>
                    </div>
              </form>
          </div>
<div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
    <div class="table-responsive block-content tbblock">
            <table class="table">
                <thead class="tbheader">
                    <tr>
                      <th class="col-lg-3 col-xs-3">PERIOD <i class="fa fa-sort"></i></th>
                      <th class="col-lg-3 col-xs-3">ADVISOR <i class="fa fa-sort"></i></th>
                      <th class="col-lg-3 col-xs-3">INTRODUCER <i class="fa fa-sort"></i></th>
                      <th class="col-lg-3 col-xs-3">GROSS COMMSSION <i class="fa fa-sort"></i></th>
                    </tr>
                </thead>
                <tbody> 
                    <tr>
                      <td><strong> Jan 2015</strong></td>
                      <td data-collapse-group="myDivs2" data-toggle="collapse" data-target="#jan" class="accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 300</strong></td>
                      <td><strong> 300</strong></td>
                      <td><strong>400</strong></td>
                    </tr>
<td colspan="6" style="padding:0;">
  <div class="panel panel-default accordian-body collapse" id="jan" style="margin-bottom: 0;">
    <div class="table-responsive block-content tbblock">
            <table class="table">
                <thead class="tbheader">
              <tr>
                <th class="col-lg-10 col-xs-2">ITEM</th>
                <th class="col-lg-2 col-xs-2">AMOUNT ($)</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                <td data-collapse-group="myDivs" data-toggle="collapse" data-target="#commissions" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> COMISSIONS</strong></td>
                <td><strong>311.67</strong></td>
              </tr>
              <td colspan="7" style="padding:0">
  <!--  Commissions Table -->
  <div class="panel panel-default accordian-body collapse"  id="commissions">
      <div class="table-responsive block-content tbblock">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
              <h4 style="color: #666666;">COMMISIONS</h4>
          </div>      
           
              <table class="table">
                <thead class="tbheader">
                        <tr>
                          <th>PROVIDER <i class="fa fa-sort"></i></th>
                          <th>1ST YEAR COMM <i class="fa fa-sort"></i></th>
                          <th>RENEWAL COMM <i class="fa fa-sort"></i></th>
                          <th>TOTAL ($) <i class="fa fa-sort"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>Aviva</td>
                          <td data-collapse-group="" data-toggle="collapse" data-target="#int" class="accordion-toggle"><i class="fa fa-plus-square-o"></i> 311.67</td>
                          <td>0</td>
                          <td></td>
                        </tr>
  <td colspan="7" style="padding:0">
  <div class="panel panel-default accordian-body collapse lifetable" id="int">
    <div class="table-responsive block-content tbblock">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <h4 style="color: #666666;">INTRODUCER</h4>
      </div>   
         <table class="table">
            <thead class="tbheader">
                <tr>
                  <th class="col-xs-2">INTRODUCER</th>
                  <th class="col-xs-2">LEVEL</th>
                  <th class="col-xs-2">INTRODUCER</th>
                  <th class="col-xs-2">ADVISOR</th>
                  <th class="col-xs-2">TOTAL ($)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td><strong>Advisor</strong></td>
                  <td><strong>Level 1</strong></td>
                  <td data-collapse-group="" data-toggle="collapse" data-target="#introducer" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 900</strong></td>
                  <td></td>
                  <td></td>
                </tr>
                <td colspan="7" style="padding:0">
<!-- Introducer Table -->
<div class="panel panel-default accordian-body collapse"  id="introducer">
    <div class="table-responsive block-content tbblock">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
            <h4 style="color: #666666;">INTRODUCER BREAKDOWN</h4>
        </div>
          <table class="table">
              <thead class="tbheader">
                  <tr>
                    <th>CLIENT <i class="fa fa-sort"></i></th>
                    <th>PLAN <i class="fa fa-sort"></i></th>
                    <th>INTRODUCER FEE ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>Client Given Name 1</td>
                    <td>Plan 1</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Client Given Name 2</td>
                    <td>Plan 2</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Client Given Name 3</td>
                    <td>Plan 3</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td><strong>TOTAL AMOUNT</strong></td>
                    <td></td>
                    <td><strong></strong></td>
                  </tr>
              </tbody>
          </table> 
          <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#introducer" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
          <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
          <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
          </div>
          </div>
                <tr>
                  <td><strong>Introducer 1</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><strong>Introducer 2</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><strong>Introducer 3</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><strong>TOTAL</strong></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
          </td>
        </tbody>
      </table>
      <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#int" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
      <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
      <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
    </div>
  </div>
      <tr>
        <td>Allianz</td>
        <td></i> 0</td>
        <td data-collapse-group="" data-toggle="collapse" data-target="#renewal" class=" accordion-toggle"><i class="fa fa-plus-square-o"></i> 0</td>
        <td></td>
      </tr>
      <tr>
<td colspan="7" style="padding:0">
<!--  Renewal Table -->
<div class="panel panel-default accordian-body collapse"  id="renewal">
  <div class="panel-body">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>LIFE</h4>
      </div>     
        <table class="table">
            <thead>
                <tr>
                  <th>BILL DATE (REF DATE) <i class="fa fa-sort"></i></th>
                  <th>POLICY NUMBER <i class="fa fa-sort"></i></th>
                  <th>PRODUCT NAME <i class="fa fa-sort"></i></th>
                  <th>POLICY OWNER <i class="fa fa-sort"></i></th>
                  <th>PREMIUM ($)<i class="fa fa-sort"></i></th>
                  <th>COMMISION ($) <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td>DD-MM-YY</td>
                  <td>Policy 1</td>
                  <td></td>
                  <td>Steve Jobs</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>DD-MM-YY</td>
                  <td>Policy 2</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
            </tbody>
        </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>HEALTH</h4>
      </div>     
          <table class="table">
              <thead>
                  <tr>
                    <th>BILL DATE (REF DATE) <i class="fa fa-sort"></i></th>
                    <th>POLICY NUMBER <i class="fa fa-sort"></i></th>
                    <th>PRODUCT NAME <i class="fa fa-sort"></i></th>
                    <th>POLICY OWNER <i class="fa fa-sort"></i></th>
                    <th>PREMIUM ($)<i class="fa fa-sort"></i></th>
                    <th>COMMISION ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 1</td>
                    <td></td>
                    <td>Steve Jobs</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 2</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>INVESTMENT</h4>
      </div>     
          <table class="table">
              <thead>
                  <tr>
                    <th>BILL DATE (REF DATE) <i class="fa fa-sort"></i></th>
                    <th>POLICY NUMBER <i class="fa fa-sort"></i></th>
                    <th>PRODUCT NAME <i class="fa fa-sort"></i></th>
                    <th>POLICY OWNER <i class="fa fa-sort"></i></th>
                    <th>PREMIUM ($)<i class="fa fa-sort"></i></th>
                    <th>COMMISION ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 1</td>
                    <td></td>
                    <td>Steve Jobs</td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>DD-MM-YY</td>
                    <td>Policy 2</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
      <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#renewal" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
  </div>
</div>      
                    <tr>
                      <td>Maybank-HL</td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td><strong>TOTAL AMOUNT</strong></td>
                      <td>311.67</td>
                      <td></td>
                      <td><strong></strong></td>
                      </tr>
                </tbody>
          </table>
      <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#commissions" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
      <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
      <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button> 
  </div>
  </div>


        <!-- Policy Table -->
        <div class="panel panel-default accordian-body collapse"  id="policy" style="margin:0; padding:0; background-color:#e6e6e6">
            <div class="panel-body">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
                    <h4>POLICY SUMMARY</h4>
                </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"style="padding-right: 0;">
                        <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalpolicy"><i class="fa fa-expand"></i> Expand</button>
                        <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
                        <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
                    </div>
                  <table class="table table-hover" style="margin:0; padding:0; background-color:#e6e6e6">
                      <thead>
                          <tr>
                            <th>POLICY ID <i class="fa fa-sort"></i></th>
                            <th>POLICY NO <i class="fa fa-sort"></i></th>
                            <th>POLICY TYPE <i class="fa fa-sort"></i></th>
                            <th>TRANSACTION CODE <i class="fa fa-sort"></i></th>
                            <th>POLICY CODE <i class="fa fa-sort"></i></th>
                            <th>DATE <i class="fa fa-sort"></i></th>
                            <th>AMOUNT <i class="fa fa-sort"></i></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>12345</td>
                              <td>12345</td>
                              <td>Plan A</td>
                              <td>12345</td>
                              <td>12345</td>
                              <td>12/06/1985</td>
                              <td>100</td>
                          </tr>
                          <tr>
                              <td>12345</td>
                              <td>12345</td>
                              <td>Plan B</td>
                              <td>12345</td>
                              <td>12345</td>
                              <td>12/06/1985</td>
                              <td>100</td>
                          </tr>
                          <tr>
                              <td>12345</td>
                              <td>12345</td>
                              <td>Plan C</td>
                              <td>12345</td>
                              <td>12345</td>
                              <td>12/06/1985</td>
                              <td>100</td>
                          </tr>
                          <tr>
                              <td><strong>TOTAL AMOUNT</strong></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td><strong>300</strong></td>
                          </tr>
                      </tbody>
                  </table>    
              </div>
        </div>  
                          <tr>
                              <td><strong>OVERRIDES</strong></td>
                              <td data-collapse-group="" data-toggle="collapse" data-target="#overridesvalue" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 39.96</strong></td>
                          </tr>
<!--  Overrides Table -->
<td colspan="7" style="padding:0">
<div class="panel panel-default accordian-body collapse"  id="overridesvalue">
  <div class="table-responsive block-content tbblock">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4 style="color: #666666;">DIRECT TEAM</h4>
      </div>      
        <table class="table">
            <thead class="tbheader">
                <tr>
                  <th># <i class="fa fa-sort"></i></th>
                  <th>TEAM MEMBERS <i class="fa fa-sort"></i></th>
                  <th>RANK <i class="fa fa-sort"></i></th>
                  <th>STATUS <i class="fa fa-sort"></i></th>
                  <th>1ST YEAR<i class="fa fa-sort"></i></th>
                  <th>RENEWALS <i class="fa fa-sort"></i></th>
                  <th>AMOUNT ($) <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                  <td>1</td>
                  <td>Jerry Chop</td>
                  <td>Director</td>
                  <td>Active</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Avis Ong</td>
                  <td>Advisor</td>
                  <td>Active</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Kevin Tan</td>
                  <td>Advisor</td>
                  <td>Active</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
            </tbody>
        </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4 style="color: #666666"> BUYOUT PAYMENTS</h4>
      </div>     
      <table class="table">
            <thead class="tbheader">
                  <tr>
                    <th># <i class="fa fa-sort"></i></th>
                    <th>AGENT NAME <i class="fa fa-sort"></i></th>
                    <th>RANK <i class="fa fa-sort"></i></th>
                    <th>SUPERVISOR <i class="fa fa-sort"></i></th>
                    <th>RANK <i class="fa fa-sort"></i></th>
                    <th>EFFECTIVE <i class="fa fa-sort"></i></th>
                    <th>AMOUNT <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>1</td>
                    <td>Enry Ong</td>
                    <td>Manager</td>
                    <td>Alvin Nathan</td>
                    <td>Partner</td>
                    <td>Jan 20 2016</td>
                    <td>38.96</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>TOTAL</td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4 style="color: #666666">INDIRECT TEAM</h4>
      </div>     
      <table class="table">
          <thead class="tbheader">
                  <tr>
                    <th># <i class="fa fa-sort"></i></th>
                    <th>TEAM MEMBERS <i class="fa fa-sort"></i></th>
                    <th>RANK <i class="fa fa-sort"></i></th>
                    <th>STATUS <i class="fa fa-sort"></i></th>
                    <th>1ST YEAR <i class="fa fa-sort"></i></th>
                    <th>RENEWAL <i class="fa fa-sort"></i></th>
                    <th>AMOUNT ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>1</td>
                    <td>Manager1</td>
                    <td>Manager</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Advisor M1</td>
                    <td>Advisor</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>TEAM TOTAL</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
              </tbody>
          </table> 
    <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#overridesvalue" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
    <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
    <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
  </div>
  </div>
</td>
                  <tr>
                      <td><strong>INCENTIVES</strong></td>
                      <td  data-collapse-group="" data-toggle="collapse" data-target="#incentives" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> 0</strong></td>
                  </tr>    
<td colspan="7" style="padding:0">
<!--  Incentives Table -->
<div class="panel panel-default accordian-body collapse"  id="incentives">
<div class="table-responsive block-content tbblock">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h4>POLICY RELATED INCENTIVES</h4>
    </div>       

      <table class="table">
          <thead class="tbheader">
              <tr>
                <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
              </tr>
          </thead>
          <tbody>
                <tr>
                  <td>Policy Incentive</td>
                  <td></td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td></td>
                </tr>
          </tbody>
      </table>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>BUSINESS INCENTIVES</h4>
      </div>      
        <table class="table">
            <thead class="tbheader">
                <tr>
                  <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                  <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   <td>OTHERS</td>
                  <td></td>
                </tr>
                <tr>
                  <td>TOTAL</td>
                  <td></td>
                </tr>
                <tr>
                  <td>OVERALL TOTAL</td>
                  <td></td>
                </tr>
            </tbody>
        </table>
        <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#incentives" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
        <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
        <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
  </div>
</td>
              <tr>
                <td data-collapse-group="myDivs" data-toggle="collapse" data-target="#deductions" class=" accordion-toggle"><strong><i class="fa fa-plus-square-o"></i> DEDUCTIONS</strong></td>
                <td><strong>0</strong></td>
              </tr>
<td colspan="7" style="padding:0">
<!--  Deductions Table -->
<div class="panel panel-default accordian-body collapse"  id="deductions">
    <div class="panel-body">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
            <h4>POLICY RELATED CHARGERS</h4>
        </div>        
        <table class="table">
            <thead class="tbheader">
                  <tr>
                    <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                    <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>Medical</td>
                    <td>4000</td>
                  </tr>
                  <tr>
                    <td>Clawbaks</td>
                    <td>50</td>
                  </tr>
                  <tr>
                    <td>Others</td>
                    <td></td>
                  </tr>
              </tbody>
          </table>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left:0;">
          <h4>BUSINESS CHARGES</h4>
      </div>
        <table class="table">
            <thead class="tbheader">
                  <tr>
                    <th class="col-lg-10">ITEMS <i class="fa fa-sort"></i></th>
                    <th class="col-lg-2">AMOUNT ($) <i class="fa fa-sort"></i></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    <td>Photocopy</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Base Charge</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Office Rents</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Others</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>OVERALL TOTAL</td>
                    <td></td>
                  </tr>
              </tbody>
        </table>
        <button type="button" class="btn btn-info borderzero pull-right btn-tool btn-sm" data-toggle="collapse" data-target="#deductions" class=" accordion-toggle"><i class="fa fa-close"></i> CLOSE</button>
        <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
        <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
    </div>
</div>
</td>
                  <tr>
                            <td><strong>TOTAL</strong></td>
                            <td><strong>311.67</strong></td>
                          </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-danger borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalsave"><i class="fa fa-print"></i> Save PDF</button> 
                <button type="button" class="btn btn-warning borderzero pull-right btn-tool btn-sm" data-toggle="modal" data-target="#myModalprint"><i class="fa fa-print"></i> Print Invoice</button>
              </div>
            </div>
          </td>
          </td>
          <tr>
            <td></span><strong> Feb 2015</strong> </td>
            <td><strong></strong></td>
            <td><strong></strong></td>
            <td><strong></strong></td>
          </tr>
          <tr>
            <td></span><strong> Mar 2015</strong> </td>
            <td><strong></strong></td>
            <td><strong></strong></td>
            <td><strong></strong></td>
          </tr>
          <tr>
            <td></span><strong>TOTAL</strong> </td>
            <td><strong></strong></td>
            <td><strong></strong></td>
            <td><strong></strong></td>
          </tr>
          </tbody>
          </table>
          </div>
      </div> 
    </div>  
    <!-- /#page-content-wrapper -->

@stop
