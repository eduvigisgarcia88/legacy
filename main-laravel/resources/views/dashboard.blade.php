@extends('layouts.master')

@section('scripts')
<script type="text/javascript">

$_token = '{{ csrf_token() }}';
$(function () {
  $(".dashboard_i-date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard_c-date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard-date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard-export").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard-export-yearly").datetimepicker({
      locale: "en",
      format: "YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard-case_date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$("#dashboard_i-date").val('<?php echo($dashboard_date) ?>');
$("#dashboard_i-hidden").val('<?php echo($dashboard_date) ?>');
$("#dashboard_i-hidden-refresh").val('<?php echo($dashboard_date) ?>');

$("#dashboard_c-date").val('<?php echo($dashboard_case_date) ?>');
$("#dashboard_c-hidden").val('<?php echo($dashboard_case_date) ?>');
$("#dashboard_c-hidden-refresh").val('<?php echo($dashboard_case_date) ?>');

$('.dashboard-export').on('dp.change', function(e){
  $('#incept_export_date').val($('#dashboard-export_date').val());
  $('#dashboard-export_date').css('background-color','#ffffff');
  $('#dashboard-export_date_yearly').css('background-color','#ffffff');
});

$('.dashboard-export-yearly').on('dp.change', function(e){
  $('#incept_export_date').val($('#dashboard-export_date_yearly').val());
  $('#dashboard-export_date').css('background-color','#ffffff');
  $('#dashboard-export_date_yearly').css('background-color','#ffffff');
});

$('#yearly_box').on('change',function(){
  $('#dashboard-export_date').css('background-color','#ffffff');
  $('#dashboard-export_date_yearly').css('background-color','#ffffff');
  $('#dashboard-export_date').val('');
  $('#dashboard-export_date_yearly').val('');
  if($(this).prop('checked'))
  {
    $('.dashboard-export-yearly').removeClass('hide');
    $('.dashboard-export').addClass('hide');
    $('#date_indicator').val(1);
  }
  else
  {
    $('.dashboard-export-yearly').addClass('hide');
    $('.dashboard-export').removeClass('hide');
    $('#date_indicator').val('');
  }
});
$(".dashboard_i-date").on("dp.change", function(e) {
  var ctr = parseInt($("#dashboard_i-ctr").val());
  if (ctr > 0) {
    search_inceptions();
    search_ranking();
  } else {
    $("#dashboard_i-ctr").val(ctr+1);
  }
});

$('#incept-export-btn').click(function(){
  var export_input_m = $('#dashboard-export_date').val();
  var export_input_y = $('#dashboard-export_date_yearly').val();
  if(export_input_m.length == 0 && export_input_y.length == 0)
  {
    $('#dashboard-export_date').css('background-color','#FFCCCB');
    $('#dashboard-export_date_yearly').css('background-color','#FFCCCB');
  }
  else
  {
    document.getElementById('incept_export').submit();
  }
});

function search_inceptions() {
  var date = $("#dashboard_i-date").val();
  var loading = $("#loading-panels");
  loading.removeClass('hide');

  $.post("{{ url('inception/month') }}", { date: date, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $(".inception-dashboard").html(response);
      loading.addClass('hide');
    }
  });
}

function search_ranking() {
  var date = $("#dashboard_i-date").val();
  $.post("{{ url('get/ranking') }}", { date: date, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $(".ranking-dashboard").html(response);
    }
  });
}  
  
$(".dashboard_c-date").on("dp.change", function(e) {
  var ctr = parseInt($("#dashboard_c-ctr").val());

  if (ctr > 0) {
    search_submissions();
  } else {
    $("#dashboard_c-ctr").val(ctr+1);
  }
});

function search_submissions() {
  var date = $("#dashboard_c-date").val();
  var loading = $("#loading-panels");
  loading.removeClass('hide');

  $.post("{{ url('case/month') }}", { date: date, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $(".case-dashboard").html(response);
      loading.addClass('hide');
    }
  });
}

@if(Auth::user()->usertype_id != 8)

  $("#dashboard-incept_date").val('<?php echo($batch_date) ?>');
  $("#dashboard-case_date").val('<?php echo($case_date) ?>');

  $("#inception-user_id").select2();


  $(function () {
    $(".inception-date").datetimepicker({
        locale: "en",
        format: "MMM YYYY",
        ignoreReadonly: true,
        useCurrent: false
    });
  });

  $(".inception-date").on("dp.change", function(e) {
    $("#inception-incept_date").val($("#inception-date").val());
    if ($('#inception-date').val() != $('#inception-hidden').val()) {
      $('#inception-hidden').val($('#inception-date').val());
      search();
    }
  });

  $("#inception-provider_id").change(function() {

    $id = $("#inception-provider_id").val();

    if ($id > 0 && $id) {
      $.post("{{ url('inception/get-provider') }}", { id: $id, _token: $_token }, function(response) {
        if (response.unauthorized) {
          window.location.href = response.unauthorized;
        } else {
          $("#inception-provider").val(response);
        }
      });
    } else if ($id == 0) {
      $("#inception-provider").val("Production Tagged to AA");
    } else {
      $("#inception-provider").val("");
    }
  });

  $("#inception-user_id").change(function() {
    $id = $("#inception-user_id").val();
    $.post("{{ url('inception/get-user') }}", { id: $id, _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $("#inception-agent_name").val(response.name);
        $("#inception-agent_code").val(response.code);
      }
    });
  });

  // submit form
  $("#modal-inception").find("form").submit(function(e) {
 
    var form = $("#inception-form");
    var loading = $("#loading-inception");
    // stop form from submitting
    e.preventDefault();

    loading.removeClass('hide');
    // push form data
    $.ajax({
      type: "post",
      url: "{{ url('inception/save') }}",
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if (response.unauthorized) {
          window.location.href = response.unauthorized;
        } else if(response.error) {
          $(".sup-errors").html("");
          $(".form-control").removeClass("required");
          $.each(response.error, function(index, value) {
            var errors = value.split("|");
            $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
            $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
          });

          loading.addClass('hide');
        } else {
          status(response.title, response.body, 'alert-success');

          after_save();
        }
      }
    });
  });


$('.btn-edit-inception').click(function() {

  var btn = $(this);
  btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-search").val("");
  $('.inception-data-sort i').removeAttr('class');
  $('.first-inception-td').find('i').addClass('fa fa-sort-down');
  $('.btn-inception-delete').addClass('hide');
  $('.form-incept_date').removeClass('hide');
  $('.form-edit-incept_date').addClass('hide');
  $('.btn-inception-edit').removeClass('hide');

  $.post("{{ url('inception/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $("#inception-rows").html(response.table);
      $("#inception-pages").html(response.pages);
      $("#inception-user_id").html(response.users);
      $("#inception-provider_id").html(response.providers);
      $("#inception-date").val(response.date);
      $("#inception-incept_date").val(response.date);
      $("#inception-hidden").val(response.date);
      $("#inception-hidden-refresh").val(response.date);
      $("#inception-user_id").select2("val", "");
      btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
      $("#modal-inception").modal("show");
    }
  }, 'json');

});

function refresh() {
  var loading = $(".loading-pane");
  var table = $("#inception-rows");
  loading.removeClass('hide');
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-user_id").select2("val", "");
  $('.inception-data-sort i').removeAttr('class');
  $('.first-inception-td').find('i').addClass('fa fa-sort-down');
  $('.btn-inception-edit').removeClass('hide');
  $('.btn-inception-delete').addClass('hide');
  $(".sup-errors").html("");
  $(".form-control").removeClass("required");
  $('.form-incept_date').removeClass('hide');
  $('.form-edit-incept_date').addClass('hide');
  $("#inception-incept_date").val($("#inception-date").val());

  $("#inception-page").val(1);
  $("#inception-order").val('');
  $("#inception-sort").val('');
  $("#inception-search").val('');
  $("#inception-date").val($("#inception-hidden-refresh").val());
  $page = $("#inception-page").val();
  $order = $("#inception-order").val();
  $sort = $("#inception-sort").val();
  $search = $("#inception-search").val();
  $status = $("#inception-date").val();

  $.post("{{ url('inception/refresh') }}", { status: $status, search: $search, page: $page, sort: $sort, order: $order, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $("#inception-rows").html(response.table);
      $("#inception-pages").html(response.pages);
      loading.addClass('hide');
    }
  });
}

function after_save() {
  var loading = $(".loading-pane");
  var table = $("#inception-rows");
  loading.removeClass('hide');
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-user_id").select2("val", "");
  $('.inception-data-sort i').removeAttr('class');
  $('.first-inception-td').find('i').addClass('fa fa-sort-down');
  $('.btn-inception-edit').removeClass('hide');
  $('.btn-inception-delete').addClass('hide');
  $(".sup-errors").html("");
  $(".form-control").removeClass("required");
  $('.form-incept_date').removeClass('hide');
  $('.form-edit-incept_date').addClass('hide');
  $("#inception-incept_date").val($("#inception-date").val());

  $("#inception-page").val(1);
  $("#inception-order").val('');
  $("#inception-sort").val('');
  $("#inception-search").val('');

  $page = $("#inception-page").val();
  $order = $("#inception-order").val();
  $sort = $("#inception-sort").val();
  $search = $("#inception-search").val();
  $status = $("#inception-date").val();

  $.post("{{ url('inception/refresh') }}", { status: $status, search: $search, page: $page, sort: $sort, order: $order, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $("#inception-rows").html(response.table);
      $("#inception-pages").html(response.pages);
      loading.addClass('hide');
    }
  });
}

function add() {
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-user_id").select2("val", "");
  $(".sup-errors").html("");
  $(".form-control").removeClass("required");
  $(".inception-tr").removeAttr('style').removeClass("inception-tr-active");
  $('.btn-inception-delete').addClass('hide');
  $('.btn-inception-edit').removeClass('hide');
  $('.form-incept_date').removeClass('hide');
  $('.form-edit-incept_date').addClass('hide');
  $("#inception-incept_date").val($("#inception-date").val());
}

function search() {

  var loading = $(".loading-pane");
  var table = $("#inception-rows");
  loading.removeClass('hide');
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-user_id").select2("val", "");
  $('.btn-inception-edit').removeClass('hide');
  $('.btn-inception-delete').addClass('hide');
  $(".sup-errors").html("");
  $(".form-control").removeClass("required");
  $('.form-incept_date').removeClass('hide');
  $('.form-edit-incept_date').addClass('hide');
  $("#inception-incept_date").val($("#inception-date").val());

  $("#inception-page").val(1);
  $page = $("#inception-page").val();
  $order = $("#inception-order").val();
  $sort = $("#inception-sort").val();
  $search = $("#inception-search").val();
  $status = $("#inception-date").val();
  
  $.post("{{ url('inception/refresh') }}", { status: $status, search: $search, page: $page, sort: $sort, order: $order, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $("#inception-rows").html(response.table);
      $("#inception-pages").html(response.pages);
      loading.addClass('hide');
    }
  });
}

$('#inception-search').keypress(function(event){
  var keycode = (event.keyCode ? event.keyCode : event.which);
  if(keycode == '13'){
    search();
  }
});

$("#inception-body").on('click', '.pagination a', function (event) {
  event.preventDefault();
  var loading = $(".loading-pane");
  var table = $("#inception-rows");
  loading.removeClass('hide');
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-user_id").select2("val", "");
  $('.btn-inception-edit').removeClass('hide');
  $('.btn-inception-delete').addClass('hide');
  $('.form-incept_date').removeClass('hide');
  $('.form-edit-incept_date').addClass('hide');
  $(".sup-errors").html("");
  $(".form-control").removeClass("required");
  $("#inception-incept_date").val($("#inception-date").val());

  if ( $(this).attr('href') != '#' ) {

    $("html, body").animate({ scrollTop: 0 }, "fast");
    $('.loading-pane').removeClass('hide');
    $("#inception-page").val($(this).html());

    $page = $("#inception-page").val();
    $order = $("#inception-order").val();
    $sort = $("#inception-sort").val();
    $status = $("#inception-date").val();
    $search = $("#inception-search").val();
    $selectedID = 0;
    $loading = false;

    $.post("{{ url('inception/refresh') }}", { status: $status, search: $search, page: $page, sort: $sort, order: $order, _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $("#inception-rows").html(response.table);
        $("#inception-pages").html(response.pages);
        loading.addClass('hide');
      }
    });
  }
});

$("#inception-body").on('click', '.inception-data-sort', function (event) {
  event.preventDefault();
  var loading = $(".loading-pane");
  var table = $("#inception-rows");
  loading.removeClass('hide');
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-user_id").select2("val", "");
  $('.btn-inception-edit').removeClass('hide');
  $('.btn-inception-delete').addClass('hide');
  $('.form-incept_date').removeClass('hide');
  $('.form-edit-incept_date').addClass('hide');
  $(".sup-errors").html("");
  $(".form-control").removeClass("required");
  $("#inception-incept_date").val($("#inception-date").val());

  if ($(this).find('i').hasClass('fa-sort-up')) {
    $('.inception-data-sort i').removeAttr('class');
    $order = $("#inception-order").val('asc');
    $sort = $("#inception-sort").val($(this).data('sort'));
    $(this).find('i').addClass('fa fa-sort-down');
  } else {
    $('.inception-data-sort i').removeAttr('class');
    $order = $("#inception-order").val('desc');
    $sort = $("#inception-sort").val($(this).data('sort'));
    $(this).find('i').addClass('fa fa-sort-up');
  }

  $page = $("#inception-page").val();
  $order = $("#inception-order").val();
  $sort = $("#inception-sort").val();
  $status = $("#inception-date").val();
  $search = $("#inception-search").val();
  $selectedID = 0;
  $loading = false;

  $.post("{{ url('inception/refresh') }}", { status: $status, search: $search, page: $page, sort: $sort, order: $order, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $("#inception-rows").html(response.table);
      $("#inception-pages").html(response.pages);
      loading.addClass('hide');
    }
  });
});

$("#inception-body").on('click', '.inception-tr', function (event) {
  $id = $(this).data('id');
  $(".inception-tr").removeAttr('style').removeClass("inception-tr-active");
  $(this).attr("style", "background-color: #5cb85c;").addClass("inception-tr-active");

  var loading = $(".loading-pane");
  loading.removeClass('hide');
  $("#inception-form").trigger("reset");
  $("#inception-id").val("");
  $("#inception-user_id").select2("val", "");
  $(".sup-errors").html("");
  $(".form-control").removeClass("required");
  $("#inception-incept_date").val($("#inception-date").val());

  $.post("{{ url('inception/view') }}", { id: $id, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else if (!response.error) {

      $.each(response, function(index, value) {
        var field = $("#inception-" + index);
        if(field.length > 0) {
          if (field.hasClass("inception-select2")) {
            field.select2("val", value);
          } else {
            field.val(value);
          }
        }
      });
      $('.btn-inception-edit').removeClass('hide');
      $('.btn-inception-delete').removeClass('hide');
      $('.form-incept_date').addClass('hide');
      $('.form-edit-incept_date').removeClass('hide');
    } else {
      status(false, response.error, 'alert-danger');
    }

    loading.addClass('hide');
  });
});


$('.btn-inception-delete-yes').click(function() {

  var loading = $(".loading-pane");
  loading.removeClass('hide');

  $.post("{{ url('inception/delete') }}", { id: $id, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      status(response.title, response.body, 'alert-success');
      after_save();
    }
  });

});

Morris.Bar({
  element: 'agent-production',
  data: [
    { y: 'Jan', a: {{ $policy_jan }}},
    { y: 'Feb', a: {{ $policy_feb }}},
    { y: 'Mar', a: {{ $policy_mar }}},
    { y: 'Apr', a: {{ $policy_apr }}},
    { y: 'May', a: {{ $policy_may }}},
    { y: 'Jun', a: {{ $policy_jun }}},
    { y: 'Jul', a: {{ $policy_jul }}},
    { y: 'Aug', a: {{ $policy_aug }}},
    { y: 'Sep', a: {{ $policy_sep }}},
    { y: 'Oct', a: {{ $policy_oct }}},
    { y: 'Nov', a: {{ $policy_nov }}},
    { y: 'Dec', a: {{ $policy_dec }}},
  ],
  xkey: 'y',
  ykeys: ['a'],
  labels: ['Policies', 'Series B']
});

<?php echo($groups) ?>

// function graph_agent() {
//   $("#agent-production").html('');
//   Morris.Bar({
//     element: 'agent-production',
//     data: [
//       { y: 'Jan', a: {{ $policy_jan }}},
//       { y: 'Feb', a: {{ $policy_feb }}},
//       { y: 'Mar', a: {{ $policy_mar }}},
//       { y: 'Apr', a: {{ $policy_apr }}},
//       { y: 'May', a: {{ $policy_may }}},
//       { y: 'Jun', a: {{ $policy_jun }}},
//       { y: 'Jul', a: {{ $policy_jul }}},
//       { y: 'Aug', a: {{ $policy_aug }}},
//       { y: 'Sep', a: {{ $policy_sep }}},
//       { y: 'Oct', a: {{ $policy_oct }}},
//       { y: 'Nov', a: {{ $policy_nov }}},
//       { y: 'Dec', a: {{ $policy_dec }}},
//     ],
//     xkey: 'y',
//     ykeys: ['a'],
//     labels: ['Policies', 'Series B']
//   });
// }

// function graph_team() {
//   $("#team-progression").html('');
//    echo($groups) 
// }

// $("#menu-toggle").click(function() {
//   // console.log('yo');
//   graph_agent();
//   graph_team();
// });
//Provider
$('#provider .all').click(function() {
  var provider = $("#provider-body");
  $(".loading-provider").removeClass("hide");
  provider.html('<i class="fa fa-calendar"></i> <small id="provider_day">All</small>' +
            '<h2><i class="loading-provider fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('provider-count/all/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      provider.html(response);
      $('#provider_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-provider").addClass("hide");
    }
  }, 'json');
});

$('#provider .today').click(function() {
  var provider = $("#provider-body");
  $(".loading-provider").removeClass("hide");
  provider.html('<i class="fa fa-calendar"></i> <small id="provider_day">Today</small>' +
            '<h2><i class="loading-provider fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('provider-count/today/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      provider.html(response);
      $('#provider_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-provider").addClass("hide");
    }
  }, 'json');
});


$('#provider .week').click(function(){
  var provider = $("#provider-body");
  $(".loading-provider").removeClass("hide");
  provider.html('<i class="fa fa-calendar"></i> <small id="provider_day">1 Week</small>' +
            '<h2><i class="loading-provider fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('provider-count/week/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      provider.html(response);
      $('#provider_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-provider").addClass("hide");
    }
  }, 'json');
});

$('#provider .month').click(function(){
  var provider = $("#provider-body");
  $(".loading-provider").removeClass("hide");
  provider.html('<i class="fa fa-calendar"></i> <small id="provider_day">6 Months</small>' +
            '<h2><i class="loading-provider fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('provider-count/month/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      provider.html(response);
      $('#provider_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-provider").addClass("hide");
    }
  }, 'json');
});


//Production
$('#production .all').click(function() {
  var production = $("#production-body");
  $(".loading-production").removeClass("hide");
  production.html('<i class="fa fa-calendar"></i> <small id="production_day">All</small>' +
            '<h2><i class="loading-production fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('production-count/all/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      production.html(response);
      $('#production_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-production").addClass("hide");
    }
  }, 'json');
});

$('#production .today').click(function() {
  var production = $("#production-body");
  $(".loading-production").removeClass("hide");
  production.html('<i class="fa fa-calendar"></i> <small id="production_day">Today</small>' +
            '<h2><i class="loading-production fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('production-count/today/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      production.html(response);
      $('#production_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-production").addClass("hide");
    }
  }, 'json');
});


$('#production .week').click(function(){
  var production = $("#production-body");
  $(".loading-production").removeClass("hide");
  production.html('<i class="fa fa-calendar"></i> <small id="production_day">1 Week</small>' +
            '<h2><i class="loading-production fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('production-count/week/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      production.html(response);
      $('#production_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-production").addClass("hide");
    }
  }, 'json');
});

$('#production .month').click(function(){
  var production = $("#production-body");
  $(".loading-production").removeClass("hide");
  production.html('<i class="fa fa-calendar"></i> <small id="production_day">6 Months</small>' +
            '<h2><i class="loading-production fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('production-count/month/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      production.html(response);
      $('#production_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-production").addClass("hide");
    }
  }, 'json');
});

//Agent
$('#agent .all').click(function() {
  var agent = $("#agent-body");
  $(".loading-agent").removeClass("hide");
  agent.html('<i class="fa fa-calendar"></i> <small id="agent_day">All</small>' +
            '<h2><i class="loading-agent fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('agent-count/all/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      agent.html(response);
      $('#agent_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-agent").addClass("hide");
    }
  }, 'json');
});

$('#agent .today').click(function() {
  var agent = $("#agent-body");
  $(".loading-agent").removeClass("hide");
  agent.html('<i class="fa fa-calendar"></i> <small id="agent_day">Today</small>' +
            '<h2><i class="loading-agent fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('agent-count/today/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      agent.html(response);
      $('#agent_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-agent").addClass("hide");
    }
  }, 'json');
});


$('#agent .week').click(function(){
  var agent = $("#agent-body");
  $(".loading-agent").removeClass("hide");
  agent.html('<i class="fa fa-calendar"></i> <small id="agent_day">1 Week</small>' +
            '<h2><i class="loading-agent fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('agent-count/week/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      agent.html(response);
      $('#agent_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-agent").addClass("hide");
    }
  }, 'json');
});

$('#agent .month').click(function(){
  var agent = $("#agent-body");
  $(".loading-agent").removeClass("hide");
  agent.html('<i class="fa fa-calendar"></i> <small id="agent_day">6 Months</small>' +
            '<h2><i class="loading-agent fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('agent-count/month/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      agent.html(response);
      $('#agent_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-agent").addClass("hide");
    }
  }, 'json');
});


//Total Policies
$('#policy .all').click(function() {
  var policy = $("#policy-body");
  $(".loading-policy").removeClass("hide");
  policy.html('<i class="fa fa-calendar"></i> <small id="policy_day">All</small>' +
            '<h2><i class="loading-policy fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('policy-count/all/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      policy.html(response);
      $('#policy_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-policy").addClass("hide");
    }
  }, 'json');
});

$('#policy .today').click(function() {
  var policy = $("#policy-body");
  $(".loading-policy").removeClass("hide");
  policy.html('<i class="fa fa-calendar"></i> <small id="policy_day">Today</small>' +
            '<h2><i class="loading-policy fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('policy-count/today/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      policy.html(response);
      $('#policy_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-policy").addClass("hide");
    }
  }, 'json');
});


$('#policy .week').click(function(){
  var policy = $("#policy-body");
  $(".loading-policy").removeClass("hide");
  policy.html('<i class="fa fa-calendar"></i> <small id="policy_day">1 Week</small>' +
            '<h2><i class="loading-policy fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('policy-count/week/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      policy.html(response);
      $('#policy_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-policy").addClass("hide");
    }
  }, 'json');
});

$('#policy .month').click(function(){
  var policy = $("#policy-body");
  $(".loading-policy").removeClass("hide");
  policy.html('<i class="fa fa-calendar"></i> <small id="policy_day">6 Months</small>' +
            '<h2><i class="loading-policy fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('policy-count/month/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      policy.html(response);
      $('#policy_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-policy").addClass("hide");
    }
  }, 'json');
});

//Orphan Policies
$('#orphan .all').click(function() {
  var orphan = $("#orphan-body");
  $(".loading-orphan").removeClass("hide");
  orphan.html('<i class="fa fa-calendar"></i> <small id="orphan_day">All</small>' +
            '<h2><i class="loading-orphan fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('orphan-count/all/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      orphan.html(response);
      $('#orphan_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-orphan").addClass("hide");
    }
  }, 'json');
});

$('#orphan .today').click(function() {
  var orphan = $("#orphan-body");
  $(".loading-orphan").removeClass("hide");
  orphan.html('<i class="fa fa-calendar"></i> <small id="orphan_day">Today</small>' +
            '<h2><i class="loading-orphan fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('orphan-count/today/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      orphan.html(response);
      $('#orphan_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-orphan").addClass("hide");
    }
  }, 'json');
});


$('#orphan .week').click(function(){
  var orphan = $("#orphan-body");
  $(".loading-orphan").removeClass("hide");
  orphan.html('<i class="fa fa-calendar"></i> <small id="orphan_day">1 Week</small>' +
            '<h2><i class="loading-orphan fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('orphan-count/week/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      orphan.html(response);
      $('#orphan_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-orphan").addClass("hide");
    }
  }, 'json');
});

$('#orphan .month').click(function(){
  var orphan = $("#orphan-body");
  $(".loading-orphan").removeClass("hide");
  orphan.html('<i class="fa fa-calendar"></i> <small id="orphan_day">6 Months</small>' +
            '<h2><i class="loading-orphan fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('orphan-count/month/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      orphan.html(response);
      $('#orphan_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-orphan").addClass("hide");
    }
  }, 'json');
});

//Duplicate Policies
$('#first_year .all').click(function() {
  var first = $("#first-body");
  $(".loading-first").removeClass("hide");
  first.html('<i class="fa fa-calendar"></i> <small id="first_day">All</small>' +
            '<h2><i class="loading-first fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('first-count/all/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      first.html(response);
      $('#first_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-first").addClass("hide");
    }
  }, 'json');
});

$('#first_year .today').click(function() {
  var first = $("#first-body");
  $(".loading-first").removeClass("hide");
  first.html('<i class="fa fa-calendar"></i> <small id="first_day">Today</small>' +
            '<h2><i class="loading-first fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('first-count/today/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      first.html(response);
      $('#first_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-first").addClass("hide");
    }
  }, 'json');
});


$('#first_year .week').click(function(){
  var first = $("#first-body");
  $(".loading-first").removeClass("hide");
  first.html('<i class="fa fa-calendar"></i> <small id="first_day">1 Week</small>' +
            '<h2><i class="loading-first fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('first-count/week/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      first.html(response);
      $('#first_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-first").addClass("hide");
    }
  }, 'json');
});

$('#first_year .month').click(function(){
  var first = $("#first-body");
  $(".loading-first").removeClass("hide");
  first.html('<i class="fa fa-calendar"></i> <small id="first_day">6 Months</small>' +
            '<h2><i class="loading-first fa fa-spinner fa-spin centered"></i></h2>');

  $.post("{{ url('first-count/month/refresh') }}", { _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      first.html(response);
      $('#first_data').each(function () {
          $(this).prop('Counter',0).animate({
              Counter: $(this).text()
          }, {
              duration: 250,
              easing: 'swing',
              step: function (now) {
                  $(this).text(Math.ceil(now));
              }
          });
      });
      $(".loading-first").addClass("hide");
    }
  }, 'json');
});

  //In-Force & Active Policies
  $('#force .all').click(function() {
    var force = $("#force-body");
    $(".loading-force").removeClass("hide");
    force.html('<i class="fa fa-calendar"></i> <small id="force_day">All</small>' +
              '<h2><i class="loading-force fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('force-count/all/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        force.html(response);
        $('#force_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-force").addClass("hide");
      }
    }, 'json');
  });

  $('#force .today').click(function() {
    var force = $("#force-body");
    $(".loading-force").removeClass("hide");
    force.html('<i class="fa fa-calendar"></i> <small id="force_day">Today</small>' +
              '<h2><i class="loading-force fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('force-count/today/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        force.html(response);
        $('#force_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-force").addClass("hide");
      }
    }, 'json');
  });


  $('#force .week').click(function(){
    var force = $("#force-body");
    $(".loading-force").removeClass("hide");
    force.html('<i class="fa fa-calendar"></i> <small id="force_day">1 Week</small>' +
              '<h2><i class="loading-force fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('force-count/week/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        force.html(response);
        $('#force_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-force").addClass("hide");
      }
    }, 'json');
  });

  $('#force .month').click(function(){
    var force = $("#force-body");
    $(".loading-force").removeClass("hide");
    force.html('<i class="fa fa-calendar"></i> <small id="force_day">6 Months</small>' +
              '<h2><i class="loading-force fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('force-count/month/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        force.html(response);
        $('#force_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-force").addClass("hide");
      }
    }, 'json');
  });

  $('#lapse .all').click(function() {
    var lapse = $("#lapse-body");
    $(".loading-lapse").removeClass("hide");
    lapse.html('<i class="fa fa-calendar"></i> <small id="lapse_day">All</small>' +
              '<h2><i class="loading-lapse fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('lapse-count/all/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        lapse.html(response);
        $('#lapse_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-lapse").addClass("hide");
      }
    }, 'json');
  });

  $('#lapse .today').click(function() {
    var lapse = $("#lapse-body");
    $(".loading-lapse").removeClass("hide");
    lapse.html('<i class="fa fa-calendar"></i> <small id="lapse_day">Today</small>' +
              '<h2><i class="loading-lapse fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('lapse-count/today/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        lapse.html(response);
        $('#lapse_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-lapse").addClass("hide");
      }
    }, 'json');
  });

  $('#lapse .week').click(function(){
    var lapse = $("#lapse-body");
    $(".loading-lapse").removeClass("hide");
    lapse.html('<i class="fa fa-calendar"></i> <small id="lapse_day">1 Week</small>' +
              '<h2><i class="loading-lapse fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('lapse-count/week/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        lapse.html(response);
        $('#lapse_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-lapse").addClass("hide");
      }
    }, 'json');
  });

  $('#lapse .month').click(function(){
    var lapse = $("#lapse-body");
    $(".loading-lapse").removeClass("hide");
    lapse.html('<i class="fa fa-calendar"></i> <small id="lapse_day">6 Months</small>' +
              '<h2><i class="loading-lapse fa fa-spinner fa-spin centered"></i></h2>');

    $.post("{{ url('lapse-count/month/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        lapse.html(response);
        $('#lapse_data').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 250,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $(".loading-lapse").addClass("hide");
      }
    }, 'json');
  });

  var groups = <?php echo($groups_dashboard) ?>;

  function dashboard_submission() {
    var case_date = $("#dashboard-case_date").val();
    $(".sup-errors").html("");
    $(".form-control").removeClass("required");

    if (!case_date) {
      $(".error-case_date .sup-errors").html("*Field required");
       $("#dashboard-case_date").addClass("required");
    } else {
      dashboard_submission_progress();
    }
  }

  function dashboard_submission_progress() {

      $(".dashboard-progress-submissions").removeClass('hide');
      $("#loading-panels").removeClass('hide');
      var progress = $(".progress-case");
      var ajaxQueue = $({});
      var ctr = 0;
      var dashboard_case = '';
      var case_date = $("#dashboard-case_date").val();

      $.ajaxQueue = function(ajaxOpts) {
        var oldComplete = ajaxOpts.complete;
        ajaxQueue.queue(function(next) {
          ajaxOpts.complete = function() {
            if (oldComplete) {
              oldComplete.apply(this, arguments);
            }

            ctr = ctr + 1;

            var percent = ((ctr / (groups.length-1)) * 100).toFixed(2);

            progress.attr('aria-valuenow', percent);
            progress.attr('style', 'width: ' + percent + '%;');

            if (ctr >= groups.length) {
              $(".case-dashboard").html(dashboard_case);
              dashboard_submissions_update();
              // $(".case-dashboard").html(dashboard_case);
              // $(".dashboard-progress-submissions").addClass('hide');
              // $("#loading-panels").addClass('hide');
            }
            next();
          };
          $.ajax(ajaxOpts);
        });
      };

      for (var i=1; i<=groups.length; i++) {
        $.ajaxQueue({
          url: '{{ url("case/refresh") }}',
          data: { id: groups[i-1], loop: i, date: case_date, end: groups.length, _token: '{{ csrf_token() }}' },
          type: "POST",
          success: function(data) {
            if (data.unauthorized) {
              window.location.href = data.unauthorized;
            } else {
              dashboard_case += data;
            }
          }
        });
      }
  }

  function dashboard_inceptions_update() {
    var incept_date = $("#dashboard-incept_date").val();
    $("#dashboard_i-date").val(incept_date);
    $("#dashboard_i-ctr").val(0)

    $.post("{{ url('inception/update') }}", { dashboard: $(".inception-dashboard").html(), date: incept_date, _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $(".dashboard-progress-inceptions").addClass('hide');
        $("#loading-panels").addClass("hide");
      }
    }, 'json');
  }

  function dashboard_submissions_update() {
    var case_date = $("#dashboard-case_date").val();
    $("#dashboard_c-date").val(case_date);
    $("#dashboard_c-ctr").val(0)

    $.post("{{ url('submission/update') }}", { dashboard: $(".case-dashboard").html(), date: case_date, _token: $_token }, function(response) {
      // console.log($(".case-dashboard").html());
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $(".dashboard-progress-submissions").addClass('hide');
        $("#loading-panels").addClass("hide");
      }
    }, 'json');
  }


  function dashboard_cpd() {

    $("#loading-cpd").removeClass('hide');
    $.post("{{ url('cpd/refresh') }}", { _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $(".cpd-dashboard").html(response);
        $("#loading-cpd").addClass("hide");
      }
    }, 'json');

  }

  function dashboard_inception() {
    var incept_date = $("#dashboard-incept_date").val();
    $(".sup-errors").html("");
    $(".form-control").removeClass("required");

    if (!incept_date) {
      $(".error-incept_date .sup-errors").html("*Field required");
       $("#dashboard-incept_date").addClass("required");
    } else {
      dashboard_inceptions_progress();
    }
  }

  function dashboard_inceptions_progress() {

      $(".dashboard-progress-inceptions").removeClass('hide');
      $("#loading-panels").removeClass('hide');
      var progress = $(".progress-inception");
      var ajaxQueue = $({});
      var ctr = 0;
      var dashboard_inception = '';
      var incept_date = $("#dashboard-incept_date").val();

      $.ajaxQueue = function(ajaxOpts) {
        var oldComplete = ajaxOpts.complete;
        ajaxQueue.queue(function(next) {
          ajaxOpts.complete = function() {
            if (oldComplete) {
              oldComplete.apply(this, arguments);
            }

            ctr = ctr + 1;

            var percent = ((ctr / (groups.length-1)) * 100).toFixed(2);

            progress.attr('aria-valuenow', percent);
            progress.attr('style', 'width: ' + percent + '%;');

            if (ctr >= groups.length) {
              $(".inception-dashboard").html(dashboard_inception);
              dashboard_inceptions_update();
            }
            next();
          };
          $.ajax(ajaxOpts);
        });
      };

      for (var i=1; i<=groups.length; i++) {
        $.ajaxQueue({
          url: '{{ url("inception/get-inception") }}',
          data: { id: groups[i-1], loop: i, end: groups.length, date: incept_date, _token: '{{ csrf_token() }}' },
          type: "POST",
          success: function(data) {
            if (data.unauthorized) {
              window.location.href = data.unauthorized;
            } else {
              dashboard_inception += data;
            }
          }
        });
      }
  }

  function dashboard_inceptions_update() {
    var incept_date = $("#dashboard-incept_date").val();
    $("#dashboard_i-date").val(incept_date);
    $("#dashboard_i-ctr").val(0)

    $.post("{{ url('inception/update') }}", { dashboard: $(".inception-dashboard").html(), date: incept_date, _token: $_token }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $(".dashboard-progress-inceptions").addClass('hide');
        $("#loading-panels").addClass("hide");
      }
    }, 'json');
  }

  function dashboard_replicate() {

      $(".dashboard-progress-replicates").removeClass('hide');
      $("#loading-panels").removeClass('hide');
      var progress = $(".progress-replicate");
      var ajaxQueue = $({});
      var ctr = 0;
      var dashboard_replicate = '';

      $.ajaxQueue = function(ajaxOpts) {
        var oldComplete = ajaxOpts.complete;
        ajaxQueue.queue(function(next) {
          ajaxOpts.complete = function() {
            if (oldComplete) {
              oldComplete.apply(this, arguments);
            }

            ctr = ctr + 1;

            var percent = ((ctr / (groups.length-1)) * 100).toFixed(2);

            progress.attr('aria-valuenow', percent);
            progress.attr('style', 'width: ' + percent + '%;');

            if (ctr >= groups.length) {
              $(".replicate-dashboard").html(dashboard_replicate);
              $(".dashboard-progress-replicates").addClass('hide');
              $("#loading-panels").addClass('hide');
            }
            next();
          };
          $.ajax(ajaxOpts);
        });
      };

      for (var i=1; i<=groups.length; i++) {
        $.ajaxQueue({
          url: '{{ url("replicate/refresh") }}',
          data: { id: groups[i-1], loop: i, end: groups.length, _token: '{{ csrf_token() }}' },
          type: "POST",
          success: function(data) {
            if (data.unauthorized) {
              window.location.href = data.unauthorized;
            } else {
              dashboard_replicate += data;
            }
          }
        });
      }
  }

@else
  var groups = {{ $groups }};

  $('.tr-resigned-agents').html('');
  $('.resigned-inactive-tr .first-td').html('Resigned Agents');

  function dashboard_case() {

      $(".dashboard-progress-submissions").removeClass('hide');
      $("#loading-panels").removeClass('hide');
      var progress = $(".progress-case");
      var ajaxQueue = $({});
      var ctr = 0;
      var dashboard_case = '';

      $.ajaxQueue = function(ajaxOpts) {
        var oldComplete = ajaxOpts.complete;
        ajaxQueue.queue(function(next) {
          ajaxOpts.complete = function() {
            if (oldComplete) {
              oldComplete.apply(this, arguments);
            }

            ctr = ctr + 1;

            var percent = ((ctr / (groups.length-1)) * 100).toFixed(2);

            progress.attr('aria-valuenow', percent);
            progress.attr('style', 'width: ' + percent + '%;');

            if (ctr >= groups.length) {
              $(".case-dashboard").html(dashboard_case);
              $(".dashboard-progress-submissions").addClass('hide');
              $("#loading-panels").addClass('hide');
            }
            next();
          };
          $.ajax(ajaxOpts);
        });
      };

      for (var i=1; i<=groups.length; i++) {
        $.ajaxQueue({
          url: '{{ url("case/refresh") }}',
          data: { id: groups[i-1], loop: i, end: groups.length, _token: '{{ csrf_token() }}' },
          type: "POST",
          success: function(data) {
            if (data.unauthorized) {
              window.location.href = data.unauthorized;
            } else {
              dashboard_case += data;
            }
          }
        });
      }
  }

  function dashboard_cpd(year) {

    $("#loading-cpd").removeClass('hide');
    $.post("{{ url('cpd/refresh') }}", { _token: $_token, year: year }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $(".cpd-dashboard").html(response);
        $("#loading-cpd").addClass("hide");
      }
    }, 'json');

  }

  function dashboard_cpd_2016(year) {

    $("#loading-cpd").removeClass('hide');
    $.post("{{ url('cpd/refresh') }}", { _token: $_token, year: year }, function(response) {
      if (response.unauthorized) {
        window.location.href = response.unauthorized;
      } else {
        $(".dasboard-cpd-2016").html(response);
        $("#loading-cpd").addClass("hide");
      }
    }, 'json');

  }

  // function dashboard_inception() {

  //     $(".dashboard-progress-inceptions").removeClass('hide');
  //     $("#loading-panels").removeClass('hide');
  //     var progress = $(".progress-inception");
  //     var ajaxQueue = $({});
  //     var ctr = 0;
  //     var dashboard_inception = '';

  //     $.ajaxQueue = function(ajaxOpts) {
  //       var oldComplete = ajaxOpts.complete;
  //       ajaxQueue.queue(function(next) {
  //         ajaxOpts.complete = function() {
  //           if (oldComplete) {
  //             oldComplete.apply(this, arguments);
  //           }

  //           ctr = ctr + 1;

  //           var percent = ((ctr / (groups.length-1)) * 100).toFixed(2);

  //           progress.attr('aria-valuenow', percent);
  //           progress.attr('style', 'width: ' + percent + '%;');

  //           if (ctr >= groups.length) {
  //             $(".inception-dashboard").html(dashboard_inception);
  //             dashboard_inceptions_update();
  //           }
  //           next();
  //         };
  //         $.ajax(ajaxOpts);
  //       });
  //     };

  //     for (var i=1; i<=groups.length; i++) {
  //       $.ajaxQueue({
  //         url: '{{ url("inception/get-inception") }}',
  //         data: { id: groups[i-1], loop: i, end: groups.length, _token: '{{ csrf_token() }}' },
  //         type: "POST",
  //         success: function(data) {
  //           dashboard_inception += data;
  //         }
  //       });
  //     }

  // }

  // function dashboard_inceptions_update() {
  //   $.post("{{ url('inception/update') }}", { dashboard: $(".inception-dashboard").html(), _token: $_token }, function(response) {
  //       $(".dashboard-progress-inceptions").addClass('hide');
  //       $("#loading-panels").addClass("hide");
  //   }, 'json');
  // }


  // function dashboard_replicate() {

  //     $(".dashboard-progress-replicates").removeClass('hide');
  //     $("#loading-panels").removeClass('hide');
  //     var progress = $(".progress-replicate");
  //     var ajaxQueue = $({});
  //     var ctr = 0;
  //     var dashboard_replicate = '';

  //     $.ajaxQueue = function(ajaxOpts) {
  //       var oldComplete = ajaxOpts.complete;
  //       ajaxQueue.queue(function(next) {
  //         ajaxOpts.complete = function() {
  //           if (oldComplete) {
  //             oldComplete.apply(this, arguments);
  //           }

  //           ctr = ctr + 1;

  //           var percent = ((ctr / (groups.length-1)) * 100).toFixed(2);

  //           progress.attr('aria-valuenow', percent);
  //           progress.attr('style', 'width: ' + percent + '%;');

  //           if (ctr >= groups.length) {
  //             $(".replicate-dashboard").html(dashboard_replicate);
  //             $(".dashboard-progress-replicates").addClass('hide');
  //             $("#loading-panels").addClass('hide');
  //           }
  //           next();
  //         };
  //         $.ajax(ajaxOpts);
  //       });
  //     };

  //     for (var i=1; i<=groups.length; i++) {
  //       $.ajaxQueue({
  //         url: '{{ url("replicate/refresh") }}',
  //         data: { id: groups[i-1], loop: i, end: groups.length, _token: '{{ csrf_token() }}' },
  //         type: "POST",
  //         success: function(data) {
  //           dashboard_replicate += data;
  //         }
  //       });
  //     }
  // }


@endif


  $('#file_is_love').change(function(){
    var label = $(this).val().replace(/\\/g, '/').replace(/.*\//, '');
    var ext = label.match(/\.([^\.]+)$/)[1];
    $('p.err').remove();
    switch(ext)
    {
        case 'csv':
        case 'xlsx':
        case 'xls':
            $('#file_show').val(label).css('color', 'black');;
            $('#submit-xls').prop('disabled', false);
            break;
        default:
            $('#file_show').val('Invalid file type. Must be .csv, .xls or .xlsx').css('color', 'red');
            $('#file_is_love').val('');
            $('#submit-xls').prop('disabled', true);
    }

    
  });

  $('#incept-xls_form').submit(function(e){
    e.preventDefault();
    var form_data = new FormData($(this)[0]);

    $.ajax({
      type: "POST",
      headers: { 'X-CSRF-Token' : '{{ csrf_token() }}' }, 
      url: "{{ url('inceptions/import') }}",
      processData: false,
      contentType: false,
      cache: false,
      data: form_data,
      beforeSend: function(){
        $('#load-form-xls').removeClass('hide');
        $('p.err').remove();
      },
      success: function(response){
        console.log(response);
        if(response.code == 1)
        {
          status('', response.saved + ' / ' + response.looped + ' records imported.', 'alert-success');
          $('#modal-xls').modal('toggle');
          $('#load-form-xls').addClass('hide');
        }
        else
        {
          $('#load-form-xls').addClass('hide');
          $('#xls-notice').append('<p class="err">'+response.message+'</p>');
        }
      }
    });
  });
</script>

@stop

@section('content')

    <!-- Page Content -->
      <div id="page-content-wrapper" class="response">
      <div class="container-fluid">
      <!-- Title -->
      <div class="row main-title-container container-space" style="background-color: white; background: url('images/dashlogo.jpg') right no-repeat;">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <h3 class="main-title">DASHBOARD</h3>
          <h5 class="bread-crumb">{{ $bread_crumb }}</h5>
        </div>
      </div>

  
  
      <style type="text/css">
        .required {
          background-color: #ffe5e5 !important;
        }
        .panel-toggle {
          overflow-x: hidden;
        }
        .panel-graph {
          overflow-x: scroll;
          margin-right: 15px;
          margin-left: 15px;
          overflow-y: hidden;
        }
        .no-padding {
          padding: 0;
        }
        .div-panel-dashboard, .div-panel-dashboard-team {
          background-color: #ffffff;
          padding: 15px 0;
          margin-top: 15px;
          border: 1px solid #e6e6e6;
        }
        .dashboard-responsive table {
          border-collapse: inherit !important;
        }
        .dashboard-responsive {
          margin-bottom: 0;
        }
        .dashboard-table {
          margin-bottom: 0;
        }
        .dashboard-inception-table {
          margin-bottom: 0;
        }
        .dashboard-inception-table thead tr th {
          padding: 3px;
          border-right: solid 1px #cccccc;
          border-bottom: solid 1px #cccccc;
          min-width: 91px;
          max-width: 91px;
          background-color: #263a55;
          color: #ffffff;
          vertical-align: middle !important;
          white-space: normal !important;
          font-size: 11px;
          text-align: center;
          border-right: 1px solid #cccccc;
          text-transform: uppercase;
        }
        .first-inception-td {
          border-left: solid 1px #cccccc !important;
        }
        .dashboard-inception-table tbody tr td {
          padding: 3px;
          border-right: solid 1px #cccccc;
          border-bottom: solid 1px #cccccc;
          min-width: 91px;
          max-width: 91px;
          color: #263a55;
          vertical-align: middle !important;
          white-space: normal !important;
          font-size: 11px;
          text-align: center;
          border-right: 1px solid #cccccc;
          text-transform: uppercase;
        }
        .dashboard-inception-table tr:hover {
          cursor: pointer;
          background-color: #dbeaff !important;
        }
        .dashboard-inception-table tr.inception-tr-active:hover {
          cursor: pointer;
          background-color: #8bbb8b !important;
        }
        .first-td-ranking {
          width: 70px !important;
          width: 70px !important;
          text-align: center !important;
          vertical-align: middle !important;
          font-size: 13px !important;
          font-weight: bold;
          border-left: solid 1px #263a55 !important;
        }
        .first-td-ranking-header {
          background-color: #2a3e59 !important;
          color: #ffffff !important;
        }
        .ranking-header {
          background-color: #2a3e59 !important;
          color: #ffffff !important;
          font-weight: bold !important;
          text-align: center !important;
          font-size: 13px !important;
          text-transform: uppercase;
        }
        .ranking-name {
          font-size: 13px !important;
          text-align: left !important;
          text-align: center !important;
        }
        .ranking-not {
          background-color: #cccccc !important;
        }
        .ranking-ape {
          font-size: 13px !important;
          text-align: center !important;
        }
        .first-tr-td {
          border-top: solid 1px #263a55 !important;
          text-align: center !important;
        }
        .case-td {
          background-color: #2a3e59;
          color: #ffffff !important;
          border: none !important;
          border-bottom: solid 1px #263a55 !important;
        }
        .mtd-td {
          font-size: 13px !important;
          background-color: #fce4d6;
          text-align: center !important;
        }
        .ytd-td {
          font-size: 13px !important;
          background-color: #c6e0b4;
          text-align: center !important;
        }
        .total-td {
          background-color: #bdd7ee;
        }
        .dashboard-table tbody tr td {
          padding: 3px;
          border-right: solid 1px #263a55;
          border-bottom: solid 1px #263a55;
          min-width: 91px;
          max-width: 91px;
          color: #263a55;
          /*height: 25px;*/
          vertical-align: middle !important;
          white-space: normal !important;
          font-size: 11px;
          text-align: right;
        }
        .first-td {
          min-width: 200px !important;
          max-width: 200px !important;
          vertical-align: middle !important;
          text-align: left;
          font-size: 13px !important;
          border-left: solid 1px #263a55 !important;
        }
        .total-tr td {
          font-weight: bold;
        }
        .total-tr .first-td {
          text-align: left;
        }
        .main-tr td {
          font-weight: bold;
          /*background-color: #f5f5f5;*/
        }
        .main-tr-total td {
          text-align: right;
          /*background-color: #ffffff !important;*/
        }
        .main-tr-manager td {
          /*background-color: #ffffff !important;*/
        }
        .expand-tr td {
          padding: 3px;
          border: solid 1px #e6e6e6;
          min-width: 102px;
          max-width: 102px;
          white-space: normal;
          font-size: 12px;
          text-align: right;
        }
        .expand-table {
          width: 100%;
        }
        .expand-td {
          padding: 0 !important;
          border: none !important;
          margin: 0;
        }
        .tbheader {
          border: 1px solid #26629a;
        }
        .expand-table .first-td {
          text-align: left !important;
        }
        .first-td-1 {
          padding-left: 15px !important;
        }
        .first-td-2 {
          padding-left: 30px !important;
        }
        .first-td-3 {
          padding-left: 45px !important;
        }
        .first-td-4 {
          padding-left: 60px !important;
        }
        .expand-tr-total td {
          font-weight: bold;
        }
        .expand-tr-total .first-td {
          font-weight: normal;
        }
        .main-user-tr td {
          /*background-color: #d9edf7 !important;*/
        }
        .main-user-agent-tr td {
          /*background-color: #fcf8e3 !important;*/
        }
        .span-dashboard {
          color: #26629a; display: block;
          background-color: #2a3e59;
          color: #ffffff;
          padding: 7px;
          font-weight: bold;
        }
        .td-hover:hover {
          cursor: pointer;
        }
        .total-tr .td-hover:hover {
          background-color: #e6f2fc;
        }
        .footer-td {
          border: none !important;
          text-align: left !important;
        }
        .sup-td {
          background-color: #fff2cc;
        }
        .bsc-td {
          background-color: #ddebf7;
          min-width: 114px !important;
          max-width: 114px !important;
          border-top: solid 1px #263a55 !important;
        }
        .cpd-td {
          background-color: #ddebf7 !important;
        }
        .total-tr td:nth-child(6), .total-tr td:nth-child(7), .total-tr td:nth-child(8), .total-tr td:nth-child(9) {
          background-color: #d1bdee;
        }
        .group-tr td:nth-child(6), .group-tr td:nth-child(7), .group-tr td:nth-child(8), .group-tr td:nth-child(9) {
          background-color: #d0cddf;
        }
        .sup-tr td:nth-child(6), .sup-tr td:nth-child(7), .sup-tr td:nth-child(8), .sup-tr td:nth-child(9) {
          background-color: #fcd6e1;
        }
        .group-td {
          background-color: #d6fcfa;
        }
        .group-cpd-td {
          background-color: #d6fcfa !important;
        }
        .group-cpd-tr .td-hover:hover {
          background-color: #e8fcfb;
        }
        .group-tr .td-hover:hover {
          background-color: #e8fcfb;
        }
        .sup-tr .td-hover:hover {
          background-color: #fffaea;
        }
        .dashboard-progress-inceptions, .dashboard-progress-submissions, .dashboard-progress-replicates {
          border-radius: 0;
          height: 5px;
          margin: 0;
        }
        #dashboard-tab-panel .nav-pills > li > a {
          border-radius: 4px 4px 0 0 ;
          color: #2a3e59;
          font-weight: bold;
        }
        #dashboard-tab-panel .tab-content {
          color : white;
          padding : 10px;
        }
        .dashboard-panels {
          padding-top: 0;
          background-color: #ffffff;
          margin-bottom: 15px;
          border: solid 5px #2a3e59;
        }
        .dashboard-panels .first-td {
          background-color: #ffffff;
        }
        .no-padding {
          padding: 0;
        }
        #dashboard-tab-panel .nav-pills > li.active > a {
          background-color: #ffffff;
          color: #2a3e59;
          font-weight: bold;
        }
        #dashboard-tab-panel .nav-pills > li > a:hover {
          background-color: #667282;
          color: #ffffff;
        }
        #dashboard-tab-panel .nav-pills > li.active > a:hover {
          background-color: #ffffff;
          color: #2a3e59;
        }
        #dashboard-tab-panel .nav-pills > li {
          margin: 5px 10px 0;
          background-color: #667282;
          border-radius: 4px 4px 0 0 ;
        }
        #dashboard-tab-panel .nav-pills {
          background-color: #2a3e59;
        }
        .btn-inception, .btn-submission, .btn-inception:hover, .btn-submission:hover, .btn-inception:focus {
          background-color: #2a3e59;
          color: #ffffff;
        }
        .btn-edit-inception, .btn-edit-inception:hover {
          background-color: #5bc0de;
          color: #ffffff;
        }
        .resigned-inactive-tr td {
          background-color: #ababab !important;
        }
        .adv-inactive-tr td {
          background-color: #cccccc !important;
        }
        .provider-inactive-tr td {
          background-color: #ececec !important;
        }
        .loading-soon {
          position: absolute;
          top: 0;
          left: 0;
          height: 100%;
          width: 100%;
          z-index: 1039;
          background-color: rgba(0,0,0,0.0);
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)";
          filter: alpha(opacity=10);
          -moz-opacity: 1;
          -khtml-opacity: 1;
          opacity: 1;
          text-align: center;
        }
        .loading-soon > div {
          position: absolute;
          top: 0;
          bottom: 0;
          right: 0;
          left: 0;
          margin: auto;
          height: 110px;
          width: 430px;
        }
        .input-inception {
          border: 1px solid black !important;
        }
        .no-padding-display {
          padding: 0 !important;
        }
        .display-row {
          display: table;
          width: 100%;
        }
        .label-padding {
          padding-top: 10px !important;
          font-size: 11px;
        }
        #inception-pages .pagination>li>a, .pagination>li>span {
          padding: 4px 8px;
          font-size: 11px;
          color: #2a3e59;
        }
        #inception-pages .pagination>li>a:focus, #inception-pages .pagination>li>a:hover, #inception-pages .pagination>li>span:focus, #inception-pages .pagination>li>span:hover {
          color: #2a3e59;
          background-color: #eee;
          border-color: #ddd;
        }
        .select2-results, .select2-search {
          font-size: 12px !important;
        }
        .select2-container {
          border-top: none !important;
          border-right: none !important;
          border-left: none !important;
          border-bottom: 1px solid #cccccc !important;
          border-radius: 0;
        }
        #inception-form  select.input-sm {
          border-top: none !important;
          border-right: none !important;
          border-left: none !important;
          border-bottom: 1px solid #cccccc !important;
        }
        #inception-form .select2-choice {
          height: 30px;
          padding: 9px 10px 5px 15px;
        }
        #inception-form .select2-container .select2-choice > .select2-chosen {
          line-height: 1;
        }
        @media (min-width: 768px) {
          .no-padding-bottom-sm {
            padding-bottom: 0 !important;
          }
        }
        @media (max-width: 767px) {
          .full-xs-width {
            width: 100%;
          }
        }
      </style>

      @if (Auth::user()->usertype_id != 8)

      <div class="col-xs-12 no-padding div-panel-dashboard dashboard-panels">
        <div id="loading-panels" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
        </div>
        <div id="dashboard-tab-panel"> 
          <ul class="nav nav-pills">
            <li class="active">
              <a href="#inceptions" data-toggle="tab">Inceptions</a>
            </li>
            <li class="">
              <a href="#ranking" data-toggle="tab">Ranking</a>
            </li>
            <li class="">
              <a href="#submissions" data-toggle="tab">Submissions</a>
            </li>
            <li class="">
              <a href="#mdrt" data-toggle="tab">MDRT</a>
            </li>
          </ul>
          <div class="tab-content no-padding-bottom-sm">
            <div class="tab-pane active" id="inceptions">
                <div class="form-group col-md-5" style="color: #2a3e59; padding: 0;  margin-bottom: 5px;">
                  <div class="col-lg-5 col-md-5 col-sm-8 col-xs-6" style="padding-left:0; margin-bottom: 5px;  padding-right: 0;">
                    <div>
                      <input type="hidden" id="dashboard_i-ctr" value="0">
                      <div class="input-group dashboard_i-date">
                        <input class="form-control" readonly="readonly" style="background:white;" name="date" id="dashboard_i-date" name="dashboard_month" type="text">
                        <input type="hidden" id="dashboard_i-hidden-refresh">
                        <input type="hidden" id="dashboard_i-hidden">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              <div class="inception-dashboard">
                <?php echo($dashboard_inception) ?>
                <div class="clearfix"></div>
              </div>
              <div class="col-sm-6 no-padding" style="padding-bottom: 10px;">
                <a class="btn btn-inception full-xs-width borderzero" style="width: 100%; text-align: left; border-radius: 3px 3px 0 0;" href="#inceptionsc" data-toggle="collapse"><i class="fa fa-angle-down"></i> Update Dashboard Inceptions</a>
                <div id="inceptionsc" class="collapse" style="border: 1px solid #2a3e59; color: #2a3e59; padding-right: 10px; padding-left: 10px;">
                  <div class="col-sm-6 form-group  error-incept_date" style="padding-top: 10px; margin-bottom: 10px;">
                    <label for="dashboard-incept_date" class="col-sm-4 control-label label-xs no-padding-display" style="font-size: 12px; padding-top: 5px !important;">Date</label>
                    <div class="input-group col-sm-8 dashboard-date">
                      <input type="text" name="incept_date" class="form-control borderzero input-sm" id="dashboard-incept_date" maxlength="28" placeholder="MMM YYYY" style="background-color: #ffffff; font-weight: normal;" readonly="readonly">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                    </div>
                    <sup class="sup-errors col-sm-offset-4"></sup>
                    <div class="text-right">
                      <a class="btn btn-inception full-xs-width borderzero" onclick="dashboard_inception();"><i class="fa fa-wrench"></i> Update</a>
                    </div>
                  </div>
                  <div class="col-sm-6 form-group" style="padding-top: 10px; margin-bottom: 10px;">
                    <label for="dashboard-incept_date" class="col-sm-4 control-label label-xs no-padding-display" style="font-size: 12px; padding-top: 5px !important;">Date</label>
                    <div class="input-group col-sm-8 dashboard-export">
                      <input type="text" name="export_date" class="form-control borderzero input-sm" id="dashboard-export_date" maxlength="28" placeholder="MMM YYYY" style="background-color: #ffffff; font-weight: normal;" readonly="readonly">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                    </div>
                    <div class="input-group col-sm-8 dashboard-export-yearly hide">
                      <input type="text" name="export_date_yearly" class="form-control borderzero input-sm" id="dashboard-export_date_yearly" maxlength="28" placeholder="YYYY" style="background-color: #ffffff; font-weight: normal;" readonly="readonly">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                    </div>
                    <sup class="sup-errors col-sm-offset-4"></sup>
                    <div class="text-right">
                      <p>Yearly? <input type="checkbox" id="yearly_box">
                      <a id="incept-export-btn" class="btn btn-success full-xs-width borderzero"><i class="fa fa-print"></i> Export</a>
                      </p>
                    </div>
                  </div>
                  <form class="hide" id="incept_export" action="{{url('inception/export')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="incept_export_date" id="incept_export_date">
                    <input type="hidden" name="date_indicator" id="date_indicator">
                  </form>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-6 no-padding btn-group" style="padding-bottom: 10px;">
                <p class="pull-right">
                <span><a class="btn btn-edit-inception full-xs-width borderzero"><i class="fa fa-pencil"></i> Edit</a></span>
                <span><a class="btn btn-success full-xs-width borderzero" data-toggle="modal" data-target="#modal-xls"><i class="fa fa-upload"></i> Upload</a></span>
                </p>
              </div>

              <div class="clearfix"></div>
            </div>
            <div class="tab-pane" id="submissions">

                <div class="form-group col-md-5" style="color: #2a3e59; padding: 0;  margin-bottom: 5px;">
                  <div class="col-lg-5 col-md-5 col-sm-8 col-xs-6" style="padding-left:0; margin-bottom: 5px;  padding-right: 0;">
                    <div>
                      <input type="hidden" id="dashboard_c-ctr" value="0">
                      <div class="input-group dashboard_c-date">
                        <input class="form-control" readonly="readonly" style="background:white;" name="date" id="dashboard_c-date" name="dashboard_month" type="text">
                        <input type="hidden" id="dashboard_c-hidden-refresh">
                        <input type="hidden" id="dashboard_c-hidden">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>


              <div class="case-dashboard">
                <?php echo($dashboard_case) ?>
                  <div class="clearfix"></div>
              </div>

              <div class="col-sm-6 no-padding" style="padding-bottom: 10px;">
                <a class="btn btn-inception full-xs-width borderzero" style="width: 100%; text-align: left; border-radius: 3px 3px 0 0;" href="#submissionsc" data-toggle="collapse"><i class="fa fa-angle-down"></i> Update Dashboard Submissions</a>
                <div id="submissionsc" class="collapse" style="border: 1px solid #2a3e59; color: #2a3e59; padding-right: 10px; padding-left: 10px;">
                  <div class="form-group  error-case_date" style="padding-top: 10px; margin-bottom: 10px;">
                    <label for="dashboard-case_date" class="col-sm-4 control-label label-xs no-padding-display" style="font-size: 12px; padding-top: 5px !important;">Date</label>
                    <div class="input-group col-sm-8 dashboard-case_date">
                      <input type="text" name="case_date" class="form-control borderzero input-sm" id="dashboard-case_date" maxlength="28" placeholder="MMM YYYY" style="background-color: #ffffff; font-weight: normal;" readonly="readonly">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                    </div>
                    <sup class="sup-errors col-sm-offset-4"></sup>
                    <div class="text-right">
                      <a class="btn btn-inception full-xs-width borderzero" onclick="dashboard_submission();"><i class="fa fa-wrench"></i> Update</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>
            </div>
            <div class="tab-pane col-xs-12" id="ranking" style="height: 505px; color: black; overflow-y: scroll;  padding: 0; margin-bottom: 10px;">
              <div class="ranking-dashboard">
                <?php echo($dashboard_rank) ?>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="tab-pane col-xs-12" id="mdrt" style="height: 450px;">
              <div id="loading-soon" class="loading-soon"  style="margin-top: -5px;">
                <div style="text-align: center;">
                  <div style="background-color: transparent !important; float: left;">
                    <img class="img-responsive" src="{{ url('images') . '/coming-soon.png'}}" style="height: 110px;">
                  </div>
                  <div style="background-color: #ffffff; float: right;">
                    <img class="img-responsive" src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-right: 15px; padding-left: 15px;">
                    <small><b style="color: #2a3e59;"><u>This page is currently under development.</u></b></small>
                  </div>
                </div>
              </div>
                <div class="col-xs-12 no-padding hide" style="display: block;">
                  <div class="table-responsive dashboard-responsive">
                    <table class="table dashboard-table">
                      <tr class="main-tr">
                        <td class="first-td first-tr-td">MDRT<br></td>
                      </tr>
                      <tr>
                        <td style="height: 110px !important; border-left: 1px solid #2a3e59;"></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 no-padding hide" style="padding-bottom: 15px;">
                </div>
                <div class="col-xs-12 no-padding hide" style="display: block;">
                  <div class="table-responsive dashboard-responsive">
                    <table class="table dashboard-table">
                      <tr class="main-tr">
                        <td class="first-td first-tr-td">EYFC<br></td>
                      </tr>
                      <tr>
                        <td style="height: 110px !important; border-left: 1px solid #2a3e59;"></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 no-padding hide" style="padding-bottom: 15px;">
                </div>
                <div class="col-xs-12 no-padding hide" style="display: block;">
                  <div class="table-responsive dashboard-responsive">
                    <table class="table dashboard-table">
                      <tr class="main-tr">
                        <td class="first-td first-tr-td">AFYC<br></td>
                      </tr>
                      <tr>
                        <td style="height: 110px !important; border-left: 1px solid #2a3e59;"></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 no-padding hide" style="padding-bottom: 15px;">
                </div>
            </div>
              <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="row hide">
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-provider hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader">PROVIDER COUNT<button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
             
            <ul id="provider" class="dropdown-menu pull-right borderzero">
                <li class="all"><a href="#">All</a></li>
                <li class="today"><a href="#">Today</a></li>
                <li class="week"><a href="#">1 Week</a></li>
                <li class="month"><a href="#">6 Months</a></li>
            </ul>
            </div>
            <div id="provider-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="provider_day">All</small>
              <h2><span class="count" id="provider_data">{{ $providers_count }}</span></h2>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-production hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader">
            PRODUCTION COUNT
            <button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
              <ul id="production" class="dropdown-menu pull-right borderzero">
                  <li class="all"><a href="#">All</a></li>
                  <li class="today"><a href="#">Today</a></li>
                  <li class="week"><a href="#">1 Week</a></li>
                  <li class="month"><a href="#">6 Months</a></li>
              </ul>
            </div>
            <div id="production-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="production_day">All</small>
              <h2><span class="count" id="production_data">{{ $productions_count }}</span></h2>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-agent hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader">AGENT COUNT<button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
              <ul id="agent" class="dropdown-menu pull-right borderzero">
                  <li class="all"><a href="#">All</a></li>
                  <li class="today"><a href="#">Today</a></li>
                  <li class="week"><a href="#">1 Week</a></li>
                  <li class="month"><a href="#">6 Months</a></li>
              </ul>
            </div>
            <div id="agent-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="agent_day">All</small>
              <h2><span class="count" id="agent_data">{{ $agents_count }}</span></h2>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-policy hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader">TOTAL POLICIES<button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
              <ul id="policy" class="dropdown-menu pull-right borderzero">
                  <li class="all"><a href="#">All</a></li>
                  <li class="today"><a href="#">Today</a></li>
                  <li class="week"><a href="#">1 Week</a></li>
                  <li class="month"><a href="#">6 Months</a></li>
              </ul>
            </div>
            <div id="policy-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="policy_day">All</small>
              <h2><span class="count" id="policy_data">{{ $policies_count }}</span></h2>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-orphan hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader">ORPHAN POLICIES<button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
              <ul id="orphan" class="dropdown-menu pull-right borderzero">
                  <li class="all"><a href="#">All</a></li>
                  <li class="today"><a href="#">Today</a></li>
                  <li class="week"><a href="#">1 Week</a></li>
                  <li class="month"><a href="#">6 Months</a></li>
              </ul>
            </div>
            <div id="orphan-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="orphan_day">All</small>
              <h2><span class="count" id="orphan_data">{{ $orphans_count }}</span></h2>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-force hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader"><small>IN-FORCE & ACTIVE POLICIES</small><button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
              <ul id="force" class="dropdown-menu pull-right borderzero">
                  <li class="all"><a href="#">All</a></li>
                  <li class="today"><a href="#">Today</a></li>
                  <li class="week"><a href="#">1 Week</a></li>
                  <li class="month"><a href="#">6 Months</a></li>
              </ul>
            </div>
            <div id="force-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="force_day">All</small>
              <h2><span class="count" id="force_data">{{ $forces_count }}</span></h2>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-first hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader">FIRST YEAR COUNT<button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
              <ul id="first_year" class="dropdown-menu pull-right borderzero">
                  <li class="all"><a href="#">All</a></li>
                  <li class="today"><a href="#">Today</a></li>
                  <li class="week"><a href="#">1 Week</a></li>
                  <li class="month"><a href="#">6 Months</a></li>
              </ul>
            </div>
            <div id="first-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="first_day">All</small>
              <h2><span class="count" id="first_data">{{ $firsts_count }}</span></h2>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="loading-pane loading-lapse hide">
            </div>
          <div class="panel panel-default">
            <div class="dropdown panel-heading leftalign tbheader">RENEWAL YEAR COUNT<button class="btn-header dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
              <ul id="lapse" class="dropdown-menu pull-right borderzero">
                  <li class="all"><a href="#">All</a></li>
                  <li class="today"><a href="#">Today</a></li>
                  <li class="week"><a href="#">1 Week</a></li>
                  <li class="month"><a href="#">6 Months</a></li>
              </ul>
            </div>
            <div id="lapse-body" class="text-center panel-body borderzero">
              <i class="fa fa-calendar"></i> <small id="lapse_day">All</small>
              <h2><span class="count" id="lapse_data">{{ $lapses_count }}</span></h2>
            </div>
          </div>
        </div>
      </div>

      <div class="row hide">
      <div class="col-lg-6 col-xs-12">
          <div class="content-table" style="border-top:1px #e6e6e6 solid;">
            <div class="panel-heading leftalign tbheader">SALES DASHBOARD</div>
              <div class="table-responsive block-content tbblock" style="padding-left: 0; padding-top: 0; padding-right: 0;">
                      <table class="table table-striped"> 
                        <tbody>
                          <tr>
                            <td class="leftalign">TOTAL NO. OF CASES SUBMITTED</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td class="leftalign">TOTAL NO. OF TEAM PRODUCTION</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td class="leftalign">TOTAL PERSONNAL PRODUCTION</td>
                            <td>1206</td>
                          </tr>
                          <tr>
                            <td class="leftalign">INFORCED CASES</td>
                            <td>432</td>
                          </tr>
                          <tr>
                            <td class="leftalign">LAPSED POLICES</td>
                            <td>1206</td>
                          </tr>
                        </tbody>
                      </table>
              </div>
          </div>
      </div>

      <div class="col-lg-6 col-xs-12">
          <div class="content-table" style="border-top:1px #e6e6e6 solid;">
            <div class="panel-heading leftalign tbheader">LIFE SUBMISSION</div>
              <div class="table-responsive block-content tbblock" style="padding-left: 0; padding-top: 0; padding-right: 0;">
                      <table class="table table-striped"> 
                        <tbody>
                          <tr>
                            <td class="leftalign">TOTAL NO. OF  CASES SUBMITTED</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td class="leftalign">TOTAL PRODUCTION</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td class="leftalign">INFORCED CASES</td>
                            <td>1206</td>
                          </tr>
                          <tr>
                            <td class="leftalign">PENDING OUTSTANDING</td>
                            <td>432</td>
                          </tr>
                          <tr>
                            <td class="leftalign">LAPSED POLICES</td>
                            <td>1206</td>
                          </tr>
                          <tr>
                            <td class="leftalign">PREMIUM DUE</td>
                            <td>1206</td>
                          </tr>
                          <tr>
                            <td class="leftalign">POLICIES ABOUT TO MATURE</td>
                            <td>1206</td>
                          </tr>
                        </tbody>
                      </table>
                  </div>
              </div>
          </div>
      <br>
      <div class="col-lg-6 col-xs-12 content-table text-center">
          <div class="panel panel-default" style="margin: 0;">
            <div class="panel-heading leftalign tbheader">AGENT PRODUCTION</div>
              <div id="agent-toggle" class="panel-body panel-toggle">
                 <div id="agent-production"></div>
              </div>
          </div>
      </div>
      <div class="col-lg-6 col-xs-12 content-table text-center">
          <div class="panel panel-default">
            <div class="panel-heading leftalign tbheader">TEAM PROGRESSION</div>
              <div id="team-toggle" class="panel-body panel-toggle">
                  <div id="team-progression"></div>
              </div>
          </div>
      </div>
      </div>

      @elseif (Auth::user()->usertype_id == 8)
      <div class="col-xs-12 div-panel-dashboard hide">
      <div class="col-xs-12">
        <span class="span-dashboard">PRODUCTION</span>
      </div>
        <div class="col-xs-12" style="display: block;">
          <div class="table-responsive dashboard-responsive">
            <table class="table dashboard-table">
              <tr class="main-tr">
                <td style="height: 150px; border-left: solid 1px #263a55 !important;">
                  <center>
                    <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
                    <br><big><u>This page is currently under development.</u></big>
                  </center>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div class="col-xs-12 div-panel-dashboard">
      <div class="col-xs-12">
        <span class="span-dashboard">PRODUCTION</span>
      </div>
        <div class="col-xs-12" style="display: block;">
          <div class="table-responsive dashboard-responsive">
            <table class="table dashboard-table">
              <tr class="main-tr">
                <td class="first-td first-tr-td" rowspan="2"><big class="pull-left">Rank: <big>{{ $rank }} / {{ $count_sales }}</big></big></td>
                <td colspan="4" class="first-tr-td mtd-td"><center><strong>MTD</strong></center></td>
                <td colspan="4" class="first-tr-td ytd-td"><center><strong>YTD</strong></center></td>
              </tr>
              <tr class="main-tr">
                <td class="mtd-td">APE</td>
                <td class="mtd-td">Gross Revenue</td>
                <td class="mtd-td">Aviva Cat 1</td>
                <td class="mtd-td">GI</td>
                <td class="ytd-td">APE</td>
                <td class="ytd-td">Gross Revenue</td>
                <td class="ytd-td">Aviva Cat 1</td>
                <td class="ytd-td">GI</td>
              </tr>
              <tr class="total-tr">
                <td class="first-td"><b>Inceptions</b></td>
                
                <td class="total-td i-cp-mtd-ape-{{Auth::user()->id}}">{{ $mtd_ape_inception_total }} <b>({{ $mtd_ape_inception_count }})</b></td>
                <td class="total-td i-cp-mtd-gross-{{Auth::user()->id}}">{{ $mtd_ape_gross_inception_total }}</td>
                <td class="total-td i-cp-mtd-cat-{{Auth::user()->id}}">{{ $mtd_ape_aviva_inception_total }} <b>({{ $mtd_ape_aviva_inception_count }})</td>
                <td class="total-td i-cp-mtd-gi-{{Auth::user()->id}}">{{ $mtd_ape_gi_inception_total }} <b>({{ $mtd_ape_gi_inception_count }})</td>
                <td class="total-td i-cp-ytd-ape-{{Auth::user()->id}}">{{ $ytd_ape_inception_total }} <b>({{ $ytd_ape_inception_count }})</b></td>
                <td class="total-td i-cp-ytd-gross-{{Auth::user()->id}}">{{ $ytd_ape_gross_inception_total }}</td>
                <td class="total-td i-cp-ytd-cat-{{Auth::user()->id}}">{{ $ytd_ape_aviva_inception_total }} <b>({{ $ytd_ape_aviva_inception_count }})</td>
                <td class="total-td i-cp-ytd-gi-{{Auth::user()->id}}">{{ $ytd_ape_gi_inception_total }} <b>({{ $ytd_ape_gi_inception_count }})</td>
                                                                                                                                              
                <!--
                <td class="total-td i-cp-mtd-ape-{{Auth::user()->id}}">0.00 <b>(0)</b></td>
                <td class="total-td i-cp-mtd-gross-{{Auth::user()->id}}">-</td>
                <td class="total-td i-cp-mtd-cat-{{Auth::user()->id}}">0.00 <b>(0)</td>
                <td class="total-td i-cp-mtd-gi-{{Auth::user()->id}}">{{ $mtd_ape_gi_inception_total }} <b>(0)</td>
                <td class="total-td i-cp-ytd-ape-{{Auth::user()->id}}">{{ $ytd_ape_inception_total }} <b>(0)</b></td>
                <td class="total-td i-cp-ytd-gross-{{Auth::user()->id}}">-</td>
                <td class="total-td i-cp-ytd-cat-{{Auth::user()->id}}">0.00 <b>(0)</td>
                <td class="total-td i-cp-ytd-gi-{{Auth::user()->id}}">0.00 <b>(0)</td>
                -->

              </tr>
              <tr class="total-tr">
                <td class="first-td"><b>Submissions</b></td>
                <td class="total-td s-cp-mtd-ape-{{Auth::user()->id}}">{{ $mtd_ape_case_total }} <b>({{ $mtd_ape_case_count }})</b></td>
                <td class="total-td">-</td>
                <td class="total-td s-cp-mtd-cat-{{Auth::user()->id}}">{{ $mtd_ape_aviva_case_total }} <b>({{ $mtd_ape_aviva_case_count }})</td>
                <td class="total-td s-cp-mtd-gi-{{Auth::user()->id}}">{{ $mtd_ape_gi_case_total }} <b>({{ $mtd_ape_gi_case_count }})</td>
                <td class="total-td s-cp-ytd-ape-{{Auth::user()->id}}">{{ $ytd_ape_case_total }} <b>({{ $ytd_ape_case_count }})</b></td>
                <td class="total-td">-</td>
                <td class="total-td s-cp-ytd-cat-{{Auth::user()->id}}">{{ $ytd_ape_aviva_case_total }} <b>({{ $ytd_ape_aviva_case_count }})</td>
                <td class="total-td s-cp-ytd-gi-{{Auth::user()->id}}">{{ $ytd_ape_gi_case_total }} <b>({{ $ytd_ape_gi_case_count }})</td>
              </tr>
            </table>
          </div>
        </div>
      </div>

      <div class="col-xs-12 div-panel-dashboard">
      <div id="loading-cpd" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
      </div>
      <div class="col-xs-12">
        <span class="span-dashboard">Training and Competency</span>
      </div>

      <div class="">
        <div class="col-xs-12" style="display: block;">
          <div class="table-responsive dashboard-responsive">
            <table class="table dashboard-table">
            
              <tr class="main-tr">
                <td class="first-td mtd-td first-tr-td" style="text-align: left !important;">CPD Hours</td>
                <td class="mtd-td first-tr-td">Structured ({{ number_format($default_structured, 0) }})</td>
                <td class="mtd-td first-tr-td">Ethics ({{ number_format($default_ethics, 0) }})</td>
                <td class="mtd-td first-tr-td">Rules and Regulations ({{ number_format($default_rules, 0) }})</td>
                <td class="mtd-td first-tr-td">Total ({{ number_format($default_total, 0) }})</td>
                <td class="mtd-td first-tr-td">GI ({{ number_format($default_gi, 0) }})</td>
              </tr>
              
            </table>
          </div>
        </div>
      </div>

      <div class="cpd-dashboard">
        <div class="col-xs-12" style="display: block;">
          <div class="table-responsive dashboard-responsive">
            <table class="table dashboard-table">
              
              <tr class="total-tr">
                <td class="first-td td-hover" onclick="dashboard_cpd('{{ $today_year }}');"><u>{{ $today_year }}</u></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_skills, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_knowledge, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_rules_and_regulations, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_final, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_gi, 2)}}</big></center></td>
              </tr>

{{--               <tr class="total-tr">
                <td class="first-td td-hover"><u>2016</u></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_skills_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_knowledge_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_rules_and_regulations_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_final_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_gi_2016, 2)}}</big></center></td>
              </tr> --}}

            </table>
          </div>
        </div>
      </div>
      <div class="dasboard-cpd-2016">
        <div class="col-xs-12" style="display: block;">
          <div class="table-responsive dashboard-responsive">
            <table class="table dashboard-table">

              <tr class="total-tr">
                <td class="first-td td-hover" onclick="dashboard_cpd_2016('2016');"><u>2016</u></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_skills_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_knowledge_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_rules_and_regulations_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_final_2016, 2) }}</big></center></td>
                <td class="cpd-td"><center><big>{{ number_format($total_cpd_hours_gi_2016, 2)}}</big></center></td>
              </tr>
              
            </table>
          </div>
        </div>
      </div>
      <br>
      </div>

      <div class="col-xs-12 div-panel-dashboard">
        <div class="col-xs-12">
          <span class="span-dashboard">BSC Grade</span>
        </div>
        <div class="col-xs-12" style="display: block;">
          <div class="table-responsive dashboard-responsive">
            <table class="table dashboard-table">
              <tr class="total-tr">
                <td class="first-td bsc-td">BSC</td>
                <td class="bsc-td"><center><big>Q1</big></center></td>
                <td class="bsc-td"><center><big>Q2</big></center></td>
                <td class="bsc-td"><center><big>Q3</big></center></td>
                <td class="bsc-td"><center><big>Q4</big></center></td>
              </tr>
              <tr>
                <td class="first-td"><span class="pull-left">Representative</span></td>
                <td class=""><center><big>{{ $bsc_q1 }}</big></center></td>
                <td class=""><center><big>{{ $bsc_q2 }}</big></center></td>
                <td class=""><center><big>{{ $bsc_q3 }}</big></center></td>
                <td class=""><center><big>{{ $bsc_q4 }}</big></center></td>
              </tr>
              @if ($bsc_rank == "supervisor")
              <tr>
                <td class="first-td"><span class="pull-left">Supervisor</span></td>
                <td class=""><center><big>{{ $bsc_supervisor_q1 }}</big></center></td>
                <td class=""><center><big>{{ $bsc_supervisor_q2 }}</big></center></td>
                <td class=""><center><big>{{ $bsc_supervisor_q3 }}</big></center></td>
                <td class=""><center><big>{{ $bsc_supervisor_q4 }}</big></center></td>
              </tr>
              @endif
            </table>
          </div>
        </div>
      </div>

      <div class="col-xs-12 no-padding div-panel-dashboard dashboard-panels">
        <div id="loading-panels" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
        </div>
        <div id="loading-soon" class="hide loading-soon">
          <div style="text-align: center;">
            <div style="background-color: transparent !important; float: left;">
              <img class="img-responsive" src="{{ url('images') . '/coming-soon.png'}}" style="height: 110px;">
            </div>
            <div style="background-color: #ffffff; float: right;">
              <img class="img-responsive" src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-right: 15px; padding-left: 15px;">
              <small><b style="color: #2a3e59;"><u>This page is currently under development.</u></b></small>
            </div>
          </div>
        </div>
        <div id="dashboard-tab-panel"> 
          <ul class="nav nav-pills">
            <li class="active">
              <a href="#inceptions" data-toggle="tab">Inceptions</a>
            </li>
            <li class="">
              <a href="#submissions" data-toggle="tab">Submissions</a>
            </li>
            <li class="">
              <a href="#mdrt" data-toggle="tab">MDRT</a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="inceptions">
                <div class="form-group col-md-5" style="color: #2a3e59; padding: 0;  margin-bottom: 5px;">
                  <div class="col-lg-5 col-md-5 col-sm-8 col-xs-6" style="padding-left:0; margin-bottom: 5px;  padding-right: 0;">
                    <div>
                      <input type="hidden" id="dashboard_i-ctr" value="0">
                      <div class="input-group dashboard_i-date">
                        <input class="form-control" readonly="readonly" style="background:white;" name="date" id="dashboard_i-date" name="dashboard_month" type="text">
                        <input type="hidden" id="dashboard_i-hidden-refresh">
                        <input type="hidden" id="dashboard_i-hidden">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              <div class="inception-dashboard">
                <?php echo($dashboard_inception) ?>
                <div class=" hide col-xs-12 no-padding" style="display: block; padding-bottom: 10px;">
                  <div class="table-responsive dashboard-responsive">
                    <table class="table dashboard-table tablesorter">
                      <tbody>
                              <tr class="main-tr">
                                <td class="first-td first-tr-td" rowspan="2">Group Production</td>
                                <td colspan="4" class="first-tr-td mtd-td"><center><strong>MTD</strong></center></td>
                                <td colspan="4" class="first-tr-td ytd-td"><center><strong>YTD</strong></center></td>
                              </tr>
                              <tr class="main-tr">
                                <td class="mtd-td">APE</td>
                                <td class="mtd-td">Gross Revenue</td>
                                <td class="mtd-td">Aviva Cat 1</td>
                                <td class="mtd-td">GI</td>
                                <td class="ytd-td">APE</td>
                                <td class="ytd-td">Gross Revenue</td>
                                <td class="ytd-td">Aviva Cat 1</td>
                                <td class="ytd-td">GI</td>
                              </tr>
                              <tr class="total-tr">
                                <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-inception"><u>Total:</u></td>
                                <td class="total-td"></td>
                                <td class="total-td"></td>
                                <td class="total-td"></td>
                                <td class="total-td"></td>
                                <td class="total-td"></td>
                                <td class="total-td"></td>
                                <td class="total-td"></td>
                                <td class="total-td"></td>
                              </tr>
                              <tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse in" id="expand-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table"><tbody>
                                    <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-2-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-4-inception"><b></b></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-5-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-6-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-7-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-8-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-9-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-10-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-11-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-12-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                  <tr class="group-tr">
                                    <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-14-inception"></td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                    <td class="group-td">&nbsp;</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="submissions">
            
                <div class="form-group col-md-5" style="color: #2a3e59; padding: 0;  margin-bottom: 5px;">
                  <div class="col-lg-5 col-md-5 col-sm-8 col-xs-6" style="padding-left:0; margin-bottom: 5px;  padding-right: 0;">
                    <div>
                      <input type="hidden" id="dashboard_c-ctr" value="0">
                      <div class="input-group dashboard_c-date">
                        <input class="form-control" readonly="readonly" style="background:white;" name="date" id="dashboard_c-date" name="dashboard_month" type="text">
                        <input type="hidden" id="dashboard_c-hidden-refresh">
                        <input type="hidden" id="dashboard_c-hidden">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>

              <div class="case-dashboard">
                <?php echo($dashboard_case) ?>
              </div>
            </div>
            <div class="tab-pane col-xs-12" id="mdrt" style="height: 450px;">
              <div id="loading-soon" class="loading-soon" style="margin-top: -5px;">
                <div style="text-align: center;">
                  <div style="background-color: transparent !important; float: left;">
                    <img class="img-responsive" src="{{ url('images') . '/coming-soon.png'}}" style="height: 110px;">
                  </div>
                  <div style="background-color: #ffffff; float: right;">
                    <img class="img-responsive" src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-right: 15px; padding-left: 15px;">
                    <small><b style="color: #2a3e59;"><u>This page is currently under development.</u></b></small>
                  </div>
                </div>
              </div>
                <div class="col-xs-12 no-padding hide" style="display: block;">
                  <div class="table-responsive dashboard-responsive">
                    <table class="table dashboard-table">
                      <tr class="main-tr">
                        <td class="first-td first-tr-td">MDRT<br></td>
                      </tr>
                      <tr>
                        <td style="height: 110px !important; border-left: 1px solid #2a3e59;"></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 no-padding hide" style="padding-bottom: 15px;">
                </div>
                <div class="col-xs-12 no-padding hide" style="display: block;">
                  <div class="table-responsive dashboard-responsive">
                    <table class="table dashboard-table">
                      <tr class="main-tr">
                        <td class="first-td first-tr-td">EYFC<br></td>
                      </tr>
                      <tr>
                        <td style="height: 110px !important; border-left: 1px solid #2a3e59;"></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 no-padding hide" style="padding-bottom: 15px;">
                </div>
                <div class="col-xs-12 no-padding hide" style="display: block;">
                  <div class="table-responsive dashboard-responsive">
                    <table class="table dashboard-table">
                      <tr class="main-tr">
                        <td class="first-td first-tr-td">AFYC<br></td>
                      </tr>
                      <tr>
                        <td style="height: 110px !important; border-left: 1px solid #2a3e59;"></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="col-xs-12 no-padding hide" style="padding-bottom: 15px;">
                </div>
            </div>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>

  @if(Auth::user()->usertype_id != 8)
  <!-- Edit Modal -->
  <div id="modal-inception" class="modal fade" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" style="width: 90%;">
      <div class="modal-content borderzero">
        <div id="loading-inception" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
        </div>
        <div id="modal-inception-header" class="modal-header modal-info">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h5 class="modal-inception-title" style="margin: 0;"><i class="fa fa-pencil"></i> Edit Inceptions Dashboard</h5>
        </div>
        <div class="modal-body">
          <div id="inception-body" class="col-md-8">
            <div class="form-group" style="margin: 0;">
              <label class="col-md-6 col-sm-6 col-xs-12 control-label no-padding hide">
              <label class="col-sm-3 col-xs-4 control-label label-padding hide" style="font-size: 11px; text-align: left; padding: 0;">Inceptions</label>
              <label class="col-sm-9 col-xs-8 control-label no-padding">
                <select class="input-sm form-control borderzero" onchange="changeClass()" id="inception-filter_status">
                  <option value="" selected>N/A</option>
                </select>
              </label>
              </label>
              <label class="col-md-6 col-sm-12 control-label no-padding">
              <div class="form-group">
                <label for="inception-date" class="col-sm-3 col-xs-5 control-label label-padding no-padding" style="font-size: 11px; text-align: left; padding: 0;">Inception Date</label>
                <div class="col-sm-9 col-xs-7 no-padding inception-date">
                  <input type="hidden" id="inception-hidden-refresh">
                  <input type="hidden" id="inception-hidden">
                  <div class="input-group inception-date">
                    <input type="text" name="date" class="form-control borderzero input-sm" id="inception-date" maxlength="28" placeholder="MMM YYYY" style="background-color: #ffffff; font-weight: normal;" readonly="readonly">
                    <span class="input-group-addon">
                      <span><i class="fa fa-calendar"></i></span>
                    </span>
                  </div>
                </div>
              </div>
              </label>
              <label class="col-md-6 col-sm-12 no-padding">
                <div class="input-group">
                  <input type="text" style="font-weight: normal !important;" class="form-control borderzero input-sm" placeholder="Search for..."  id="inception-search">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-default btn-sm btn-info borderzero" onclick="search()"><i class="fa fa-search"></i></button>
                    <button type="button" class="btn btn-sm btn-success btn-default borderzero" onclick="refresh()"><i class="fa fa-refresh"></i></button>
                    <button type="button" class="btn btn-sm btn-warning btn-default borderzero btn-inception-add" onclick="add();"><i class="fa fa-plus"></i></button>
                  </span>
                </div>
              </label>
              <div class="clearfix"></div>
            </div>
            <div class="table-responsive">
              <table class="table dashboard-inception-table">
                <thead>
                  <tr>
                    <th class="inception-data-sort first-inception-td" data-sort="agent_name"><i></i> AGENT NAME</th>
                    <th class="inception-data-sort" data-sort="agent_code"><i></i> AGENT CODE</th>
                    <th class="inception-data-sort" data-sort="provider"><i></i> PROVIDER</th>
                    <th class="inception-data-sort" data-sort="ape_mtd"><i></i> APE MTD</th>
                    <th class="inception-data-sort" data-sort="ape_ytd"><i></i> APE YTD</th>
                  </tr>
                </thead>
                <tbody id="inception-rows">

                </tbody>
              </table>
            </div>
            <div id="inception-pages" class="text-center borderzero leftalign"></div>
            <div class="text-center">
              <input type="hidden" id="inception-page" value="1">
              <input type="hidden" id="inception-sort" value="">
              <input type="hidden" id="inception-order" value="">
            </div>
          </div>
          <div class="col-md-4" style="border: 1px solid #cccccc; padding-top: 10px; padding-bottom: 10px;">
            {!! Form::open(array('url' => 'inception/save', 'role' => 'form', 'class' => '', 'id' => 'inception-form')) !!}
            <div class="form-group form-incept_date" style="margin-left: -15px; margin-right: -15px; background-color: #f0ad4e; padding-left: 15px; padding-right: 15px; margin-top: -10px; color: #ffffff; padding-bottom: 5px;">
              <label class="control-label label-xs no-padding-display label-padding">ADD INCEPTION</label>
            </div>
            <div class="form-group form-edit-incept_date hide" style="margin-left: -15px; margin-right: -15px; background-color: #5bc0de; padding-left: 15px; padding-right: 15px; margin-top: -10px; color: #ffffff; padding-bottom: 5px;">
              <label class="control-label label-xs no-padding-display label-padding">EDIT INCEPTION</label>
            </div>
            <div class="form-group">
              <label for="inception-user_id" class="col-sm-4 control-label label-xs no-padding-display label-padding">Agent</label>
              <div class="error-user_id col-sm-8 no-padding-display">
                <select type="text" name="user_id" class="input-inception form-control input-sm borderzero inception-select2" id="inception-user_id">
                </select> 
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="inception-provider_id" class="col-sm-4 control-label label-xs no-padding-display label-padding">Provider</label>
              <div class="error-provider_id col-sm-8 no-padding-display">
                <select type="text" name="provider_id" class="input-inception form-control input-sm borderzero" id="inception-provider_id">
                </select> 
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="clearfix" style="border-bottom: 1px solid #263a55; padding-bottom: 10px;"></div>
            <div class="form-group">
              <label for="inception-agent_name" class="col-sm-4 control-label label-xs no-padding-display label-padding">Agent Name</label>
              <div class="error-agent_name col-sm-8 no-padding-display">
                <input type="text" name="agent_name" class="input-inception form-control input-sm borderzero" id="inception-agent_name">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="inception-agent_code" class="col-sm-4 control-label label-xs no-padding-display label-padding">Agent Code</label>
              <div class="error-agent_code col-sm-8 no-padding-display">
                <input type="text" name="agent_code" class="input-inception form-control input-sm borderzero" id="inception-agent_code">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="inception-provider" class="col-sm-4 control-label label-xs no-padding-display label-padding">Provider</label>
              <div class="error-provider col-sm-8 no-padding-display">
                <input type="text" name="provider" class="input-inception form-control input-sm borderzero" id="inception-provider">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="inception-ape_mtd" class="col-sm-4 control-label label-xs no-padding-display label-padding">APE MTD</label>
              <div class="error-ape_mtd col-sm-8 no-padding-display">
                <input type="text" name="ape_mtd" class="input-inception form-control input-sm borderzero" id="inception-ape_mtd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="inception-ape_count_mtd" class="col-sm-4 control-label label-xs no-padding-display label-padding">APE Count MTD</label>
              <div class="error-ape_count_mtd col-sm-8 no-padding-display">
                <input type="text" name="ape_count_mtd" class="input-inception form-control input-sm borderzero" id="inception-ape_count_mtd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
            </div> 
            <div class="form-group">
              <label for="inception-cat_1_mtd" class="col-sm-4 control-label label-xs no-padding-display label-padding">Cat1 MTD</label>
              <div class="error-cat_1_mtd col-sm-8 no-padding-display">
                <input type="text" name="cat_1_mtd" class="input-inception form-control input-sm borderzero" id="inception-cat_1_mtd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="inception-cat_1_count_mtd" class="col-sm-4 control-label label-xs no-padding-display label-padding">Cat1 Count MTD</label>
              <div class="error-cat_1_count_mtd col-sm-8 no-padding-display">
                <input type="text" name="cat_1_count_mtd" class="input-inception form-control input-sm borderzero" id="inception-cat_1_count_mtd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
            </div> 
            <div class="form-group">
              <label for="inception-ape_ytd" class="col-sm-4 control-label label-xs no-padding-display label-padding">APE YTD</label>
              <div class="error-ape_ytd col-sm-8 no-padding-display">
                <input type="text" name="ape_ytd" class="input-inception form-control input-sm borderzero" id="inception-ape_ytd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="inception-ape_count_ytd" class="col-sm-4 control-label label-xs no-padding-display label-padding">APE Count YTD</label>
              <div class="error-ape_count_ytd col-sm-8 no-padding-display">
                <input type="text" name="ape_count_ytd" class="input-inception form-control input-sm borderzero" id="inception-ape_count_ytd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
            </div> 
            <div class="form-group">
              <label for="inception-cat_1_ytd" class="col-sm-4 control-label label-xs no-padding-display label-padding">Cat1 YTD</label>
              <div class="error-cat_1_ytd col-sm-8 no-padding-display">
                <input type="text" name="cat_1_ytd" class="input-inception form-control input-sm borderzero" id="inception-cat_1_ytd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group" style="margin: 0;">
              <label for="inception-cat_1_count_ytd" class="col-sm-4 control-label label-xs no-padding-display label-padding">Cat1 Count YTD</label>
              <div class="error-cat_1_count_ytd col-sm-8 no-padding-display">
                <input type="text" name="cat_1_count_ytd" class="input-inception form-control input-sm borderzero" id="inception-cat_1_count_ytd" maxlength="100">
                <sup class="sup-errors"></sup>
              </div>
              <div class="clearfix"></div>
            </div> 
            <div class="form-group hide form-incept_date" style="margin: 0;">
              <label for="inception-incept_date" class="col-sm-4 control-label label-xs no-padding-display label-padding">Inception Date</label>
              <div class="input-group col-sm-8 inception-date error-incept_date">
                <input type="text" name="incept_date" class="form-control borderzero input-sm" id="inception-incept_date" maxlength="28" placeholder="MMM YYYY" style="background-color: #ffffff; font-weight: normal;" readonly="readonly">
                <span class="input-group-addon">
                  <span><i class="fa fa-calendar"></i></span>
                </span>
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <input type="hidden" name="id" id="inception-id">
            <div class="form-group" style="border-top: 1px solid #cccccc; margin-top: 15px; margin-left: -15px; margin-right: -15px; margin-bottom: 0; padding: 10px 15px; padding-bottom: 0;">
              <div id="btn-inception-delete" class="dropdown hide btn-inception-delete">
                <button class="btn btn-sm btn-danger dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-trash"></i></button>
                <ul id="lapse" class="dropdown-menu pull-right borderzero">
                  <li><span style="padding-left: 20px;"><strong>Delete?</strong></span></li>
                  <li class="btn-inception-delete-yes"><a><small>Yes</small></a></li>
                  <li class="btn-inception-delete-no"><a><small>No</small></a></li>
                </ul>
              </div>
              <button type="submit" class="btn btn-sm btn-success pull-right btn-default borderzero btn-inception-edit hide"><i class="fa fa-save"></i> Save Changes</button>
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            {!! Form::close() !!}
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>


  <div aria-hidden="true" aria-labelledby="form-title" class="modal fade" data-backdrop="static" id="modal-xls" role="dialog" style="display: none;" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content borderzero">
      <div class="loading-pane hide" id="load-form-xls">
        <div>
          <i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i>
        </div>
      </div>
      <form action="{{url('inceptions/import')}}" method="post" class="form-horizontal" id="incept-xls_form" enctype="multipart/form-data">
        <div class="modal-header modal-warning">
          <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
          <h4 class="modal-title"><i class="fa fa-upload"></i> Upload Inceptions</h4>
        </div>
        <div class="modal-body">
          <div id="xls-notice" style="color:red;">
          </div>
          <div class="col-md-12">
            <div class="input-group">
                <input type="text" id="file_show" class="form-control" readonly style="background-color: white;">
                <label class="input-group-btn">
                    <span class="btn btn-primary">
                        Browse&hellip; <input id="file_is_love" name="xlsfile" type="file" style="display: none;" accept=".xls,.xlsx,.csv">
                    </span>
                </label>
            </div>
        </div>
        <div class="clearfix"></div>
        </div>
        <div class="modal-footer borderzero">
          <button class="btn btn-default borderzero" data-dismiss="modal" type="button">Cancel</button>
          <button class="btn btn-success borderzero" id="submit-xls" type="submit" disabled><i class="fa fa-upload"></i> Upload</button>
        </div>
      </form>
    </div>
  </div>
</div>

  @endif
<!-- /#page-content-wrapper -->

@stop