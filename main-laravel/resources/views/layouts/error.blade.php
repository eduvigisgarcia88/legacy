<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ $title }}</title>

    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/toastr.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Main Stylesheet -->
    {!! HTML::style('/css/content.css') !!}
    {!! HTML::style('/css/navbar.css') !!}
    {!! HTML::style('/css/sidebar.css') !!}
    {!! HTML::style('/css/accordion.css') !!}

</head>

<body>

    <div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a href="#menu-toggle3" id="menu-toggle3" ><i class="fa fa-close pull-right" style="color: #79828f; padding-top: 24px; padding-right: 18px;"></i></a>
            <li class="sidebar-brand">
                <a href="#" style="color: #79828f;">
                <i class="fa fa-cubes pull-left main-logo" style="color: #79828f;"><span id="hide"> LEGACYGROUP</span></i>
                </a>
            </li>   
    <div id="side">
            <li><a href="{{ url('/') }}"><i class="fa fa-cube"></i><span id="hide"> Dashboard</span></a></li>
    <div class="panel-group" id="accordion">           
            <div class="panel panel-default">
                <li class="panel-heading"  data-toggle="collapse" data-parent="#accordion" href="#accordionOne">
                    <a class="panel-title">
                        <i class="fa fa-users"></i><span id="hide"> User Management</span>
                        <i class="fa fa-angle-down pull-right angle"></i>
                    </a>
                </li>
        <li id="accordionOne" class="panel-collapse collapse">
            <ul id="menu" class="inner menu-pad">
    <div class="panel-group" id="accordion1">
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1" href="#a" style="background-color: transparent; padding-left: 0px;">
                <a class="panel-title">
                    <span id="hide"> System Users</span>
                    <i class="fa fa-angle-down pull-right angle"></i>
                </a>
            </div>
        <li id="a" class="panel-collapse collapse">
           <ul id="menu" class="inner menu-padd">
                <li><a href="{{ url('users') }}"><span id="hide">All</span></a></li>
                <li><a href="{{ url('users/ceo') }}"><span id="hide">CEO</span></a></li>
                <li><a href="{{ url('users/it-users') }}"><span id="hide">IT Users</span></a></li>
                <li><a href="{{ url('users/admin-accounts') }}"><span id="hide">Admin Accounts</span></a></li>
                <li><a href="{{ url('users/accounting') }}"><span id="hide">Accounting</span></a></li>
                <li><a href="{{ url('users/sales-assistant') }}"><span id="hide">Sales Assistant</span></a></li>
                <li><a href="{{ url('users/permissions') }}"><span id="hide">Permissions</span></a></li>
                <li><a href="{{ url('users/logs') }}"><span id="hide">System User Log</span></a></li>                
            </ul>
        </li>
        </div>
    <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1" href="#b" style="background-color: transparent; padding-left: 0px;">
            <a class="panel-title">
                <span id="hide"> Sales Users</span>
                <i class="fa fa-angle-down pull-right angle"></i>
            </a>
        </div>
        <li id="b" class="panel-collapse collapse">
            <ul id="menu" class="inner menu-padd">
                <li><a href="{{ url('sales') }}"><span id="hide">All</span></a></li>
                <li><a href="{{ url('sales/sales-agent') }}"><span id="hide">Sales Agent</span></a></li>
                <li><a href="{{ url('sales/assistant-manager') }}"><span id="hide">Assistant Manager</span></a></li>
                <li><a href="{{ url('sales/manager') }}"><span id="hide">Manager</span></a></li>
                <li><a href="{{ url('sales/assistant-director') }}"><span id="hide">Assistant Director</span></a></li>
                <li><a href="{{ url('sales/director') }}"><span id="hide">Director</span></a></li>
                <li><a href="{{ url('sales/partner') }}"><span id="hide">Partner</span></a></li>
                <!-- <div class="panel panel-default">
                    <div class="panel-heading"  data-toggle="collapse" data-parent="#" href="#e" style="background-color: transparent; padding-left: 0px;">
                        <a class="panel-title">
                            <span id="hide"> Team Management</span>
                            <i class="fa fa-angle-down pull-right angle"></i>
                        </a>
                    </div>
                <li id="e" class="panel-collapse collapse">
                     <ul id="menu" class="inner menu-padding">
                        <li><a href="{{ url('sales/team-management') }}">All</a></li>
                        <li><a href="{{ url('sales/team-management/incomplete') }}">Incomplete</a></li>
                        <li><a href="{{ url('sales/team-management/duplicate') }}">Duplicate</a></li>
                    </ul>
                </li>
                </div> -->
                <li><a href="{{ url('sales/logs') }}"><span id="hide">Sales User Log</span></a></li>
            </ul>
        </li>
        </div>
            </ul>
        </li>
    </div>
    <div class="panel panel-default">
        <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionFour">
            <a class="panel-title">
                <i class="fa fa-credit-card"></i><span id="hide"> Payroll Management</span>
                <i class="fa fa-angle-down pull-right angle"></i>
            </a>
        </li>
        <li id="accordionFour" class="panel-collapse collapse">
            <ul id="menu1" class="inner menu-pad">
                <li><a href="{{ url('payroll/upload-data-feeds') }}"><span id="hide">Upload Data Feeds</span></a></li>
                <li><a href="{{ url('payroll/generate-payroll') }}"><span id="hide">Generate Payroll</span></a></li>
                <li><a href="{{ url('payroll/admin-pre-payroll-entries') }}"><span id="hide">Admin Pre-payroll Entries</span></a></li>
                <li><a href="{{ url('payroll/payroll-computation') }}"><span id="hide">Payroll Computation</span></a></li>
                <li><a href="{{ url('payroll/invoice-summary-advisor') }}"><span id="hide">Invoice Summary (Advisor)</span></a></li>
                <li><a href="{{ url('payroll/invoice-summary-supervisor') }}"><span id="hide">Invoice Summary (Supervisor)</span></a></li>   
            </ul>
        </li>
    </div>
    <div class="panel panel-default">
        <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionThree">
            <a class="panel-title">
                <i class="fa fa-book"></i><span id="hide"> Policy Management</span>
                <i class="fa fa-angle-down pull-right angle"></i>
            </a>
        </li>
        <li id="accordionThree" class="panel-collapse collapse">
            <ul id="menu1" class="inner menu-pad">
                <li><a href="{{ url('policy') }}"><span id="hide">Policies</span></a></li>
                <li><a href="{{ url('policy/riders') }}"><span id="hide">Riders</span></a></li>
                <li><a href="{{ url('policy/introducers') }}"><span id="hide">Introducers</span></a></li>
                <li><a href="{{ url('policy/policy-history') }}"><span id="hide">Policy History</span></a></li>
                <li><a href="{{ url('policy/orphan-policies') }}"><span id="hide">Orphan Policies</span></a></li>
                <li><a href="{{ url('policy/duplicates') }}"><span id="hide">Duplicates</span></a></li>
                <div class="panel panel-default">
                    <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"href="#c" style="background-color: transparent; padding-left: 0px;">
                        <a class="panel-title">
                            <span id="hide"> Production</span>
                            <i class="fa fa-angle-down pull-right angle"></i>
                        </a>
                    </div>
                <li id="c" class="panel-collapse collapse">
                    <ul id="menu" class="inner menu-padd">
                      <li><a href="{{ url('policy/production/case-submission-advisor') }}"><span id="hide">Case Submission (Advisor)</span></a></li>
                      <li><a href="{{ url('policy/production/case-submission-supervisor') }}"><span id="hide">Case Submission (Supervisor)</span></a></li>
                      <li><a href="{{ url('policy/production/case-inception-advisor') }}"><span id="hide">Case Inception (Advisor)</span></a></li>
                      <li><a href="{{ url('policy/production/case-inception-supervisor') }}"><span id="hide">Case Inception (Supervisor)</span></a></li>
                    </ul>
                </li>
                </div>
            </ul>
        </li>
    </div>
        <div class="panel panel-default">
            <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionFive">
                <a class="panel-title">
                      <i class="fa fa-clipboard"></i><span id="hide"> Provider & Products Mgt</span>
                      <i class="fa fa-angle-down pull-right angle"></i>
                </a>
            </li>
                <li id="accordionFive" class="panel-collapse collapse">
                    <ul id="menu1" class="inner menu-pad">
                        <li><a href="{{ url('provider') }}"><span id="hide">Provider List</span></a></li>
                        <li><a href="{{ url('product') }}"><span id="hide">Product List</span></a></li>
                        <li><a href="{{ url('product/logs') }}"><span id="hide">Product Log</span></a></li>
                    </ul>
                </li>
        </div>
         <div class="panel panel-default">
            <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionNine">
                <a class="panel-title">
                    <i class="fa fa-credit-card"></i><span id="hide"> Summary and Reports</span>
                    <i class="fa fa-angle-down pull-right angle"></i>
                </a>
            </li>
                <li id="accordionNine" class="panel-collapse collapse">
                  <ul id="menu1" class="inner menu-pad">
                    <li><a href="{{ url('summary') }}"><span id="hide">View Firm Revenue Summary</span></a></li>
                    <li><a href="{{ url('reports') }}"><span id="hide">View Daily Production Report</span></a></li>
                    <li><a href="{{ url('reports/export') }}"><span id="hide">Export Daily Production Report</span></a></li>   
                  </ul>
                </li>
            </div>
          </div>
        </div>
      </ul>
    </div>
    <!-- #sidebar-wrapper -->
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top" style="background-color: #ffffff; padding-left: 0 !important;">
        <div class="container-fluid">
            <div class="pull-left">
            <a ></a>
            <a href="#menu-toggle" id="menu-toggle" type="button" class="btn btn-default"><i class="fa fa-bars" style="color: #79828f;"></i></a>
            </div>
            <div style="text-align: right; padding-left: 20px">
                <button id="bell"type="button" class="btn btn-default"><i class="fa fa-bell" style="color: #79828f;"></i></button>
                <button id="profile"class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">username <img src="{{ url('images/avatar.png') }}" style="color: #79828f;" class="img-circle" alt="Smiley face" height="30" width="30">
                <i class="fa fa-angle-down" style="color: #79828f;"></i></button>
                <ul id="userdrop" class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Profile <i class="fa fa-user pull-right"></i></a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Settings <i class="fa fa-cog pull-right"></i></a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('logout') }}">Logout <i class="fa fa-sign-out pull-right"></i></a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

        <div id="content">
        
        @yield('content')
        
        @if(Auth::check())
        <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-fw fa-close"></i>No</button>
                <button type="button" class="btn btn-success" data-url="" data-id="" id="dialog-confirm"><i class="fa fa-fw fa-check"></i>Yes</button>
              </div>
            </div>
          </div>
        </div>
        @endif

        </div>

    </div><!-- #wrapper -->

    <footer></footer>
    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}

    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    {!! HTML::script('/js/main.js') !!}

    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/moment.min.js') !!}

    @yield('scripts')

</body>

</html>
