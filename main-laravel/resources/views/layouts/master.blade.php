
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ $title }}</title>

    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/notification.css') !!}
    {!! HTML::style('/css/plugins/toastr.min.css') !!}
    {!! HTML::style('/css/plugins/fileinput.min.css') !!}
    {!! HTML::style('/css/plugins/jquery.dataTables.min.css') !!}
    {!! HTML::style('/css/plugins/select2-bootstrap.css') !!}
    {!! HTML::style('/css/plugins/select2.css') !!}
    {!! HTML::style('/css/plugins/bootstrap-multiselect.css') !!}
    {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! HTML::style('/plugins/tooltipster/css/tooltipster.bundle.min.css') !!}
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Main Stylesheet -->
    {!! HTML::style('/css/content.css') !!}
    {!! HTML::style('/css/navbar.css') !!}
    {!! HTML::style('/css/sidebar.css') !!}
    {!! HTML::style('/css/accordion.css') !!}
    {!! HTML::style('/css/animate.css') !!}
<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }} ">

<style type="text/css">
a{
    text-decoration: none;
    color: #79828F;
}
a:hover{
   color: #79828F; 
   text-decoration: none;
}
</style>
</head>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76378710-1', 'auto');
  ga('send', 'pageview');
  
  
  
  
  
  
</script>
<body>

    <div id="wrapper" class="toggled">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a href="#menu-toggle3" id="menu-toggle3" ><i class="fa fa-close pull-right" style="color: #79828f; padding-top: 18px; padding-right: 18px;"></i></a>
            <li class="sidebar-brand">
                <a href="#" style="color: #79828f;">
                <img src="{{ url('images/logo') . '/legacy-mini-logo.png'}}" class="xs-logo"><span id="hide"> <img class="sm-logo" src="{{ url('images/logo') . '/legacy-logo.png'}}"></span>
                </a>
            </li>   
    <div id="side">
            <li{{ Route::current()->getPath() == '/' ? ' class=active' : '' }}><a href="{{ url('/') }}"><i class="fa fa-cube"></i><span id="hide"> Dashboard</span></a></li>
   
    <div class="panel-group" id="accordion">  
        @if ($permission->view == 1)         
            <div class="panel panel-default">
                <li class="panel-heading"  data-toggle="collapse" data-parent="#accordion" href="#accordionOne">
                    <a class="panel-title accordion-toggle">
                        <i class="fa fa-users"></i><span id="hide"> User Management</span>
                        <i class="{{ Route::current()->getPath() == 'user-management/system-users' | Route::current()->getPath() == 'users/ceo' | Route::current()->getPath() == 'users/it-users' | Route::current()->getPath() == 'users/admin-accounts' | Route::current()->getPath() == 'users/compliance' | Route::current()->getPath() == 'users/accounting' | Route::current()->getPath() == 'users/sales-assistant' | Route::current()->getPath() == 'user-management/permissions' | Route::current()->getPath() == 'user-management/system-user-log' | Route::current()->getPath() == 'user-management/sales-users' | Route::current()->getPath() == 'user-management/sales-users/fsc-elite' | Route::current()->getPath() == 'user-management/sales-users/sfsc' | Route::current()->getPath() == 'user-management/sales-users/pfsc' | Route::current()->getPath() == 'user-management/sales-users/efsc' | Route::current()->getPath() == 'user-management/sales-users/fsc' | Route::current()->getPath() == 'user-management/sales-users/fsm' | Route::current()->getPath() == 'user-management/sales-users/afsd' | Route::current()->getPath() == 'user-management/sales-users/fsd' | Route::current()->getPath() == 'user-management/sales-users/partner' | Route::current()->getPath() == 'user-management/team-management' | Route::current()->getPath() == 'sales/team-management/incomplete' | Route::current()->getPath() == 'sales/team-management/duplicate' | Route::current()->getPath() == 'user-management/sales-user-log' | Route::current()->getPath() == 'user-management/sales-users/{id}/edit' |  Route::current()->getPath() == 'user-management/team-management-log' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                    </a>
                </li>
        <li id="accordionOne" class="{{ Route::current()->getPath() == 'user-management/system-users' | Route::current()->getPath() == 'users/ceo' | Route::current()->getPath() == 'users/it-users' | Route::current()->getPath() == 'users/admin-accounts' | Route::current()->getPath() == 'users/compliance' | Route::current()->getPath() == 'users/accounting' | Route::current()->getPath() == 'users/sales-assistant' | Route::current()->getPath() == 'user-management/permissions' | Route::current()->getPath() == 'user-management/system-user-log' | Route::current()->getPath() == 'user-management/sales-users' | Route::current()->getPath() == 'user-management/sales-users/fsc-elite' | Route::current()->getPath() == 'user-management/sales-users/sfsc' | Route::current()->getPath() == 'user-management/sales-users/pfsc' | Route::current()->getPath() == 'user-management/sales-users/efsc' | Route::current()->getPath() == 'user-management/sales-users/fsc' | Route::current()->getPath() == 'user-management/sales-users/fsm' | Route::current()->getPath() == 'user-management/sales-users/afsd' | Route::current()->getPath() == 'user-management/sales-users/fsd' | Route::current()->getPath() == 'user-management/sales-users/partner' | Route::current()->getPath() == 'user-management/team-management' | Route::current()->getPath() == 'sales/team-management/incomplete' | Route::current()->getPath() == 'sales/team-management/duplicate' | Route::current()->getPath() == 'user-management/sales-user-log' | Route::current()->getPath() == 'user-management/sales-users/{id}/edit' | Route::current()->getPath() == 'user-management/team-management-log' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">

        <ul id="menu" class="inner menu-pad">
            <div class="panel-group" id="accordion1">
                <li{{ Route::current()->getPath() == 'user-management/system-users' ? ' class=active' : '' }}><a href="{{ url('user-management/system-users') }}" style="padding-left: 27px;"><span id="hide">System Users</span></a></li> 
                <li{{ Route::current()->getPath() == 'user-management/sales-users' ? ' class=active' : '' }}><a href="{{ url('user-management/sales-users') }}" style="padding-left: 27px;"><span id="hide">Sales Users</span></a></li> 
                <li{{ Route::current()->getPath() == 'user-management/team-management' ? ' class=active' : '' }}><a href="{{ url('user-management/team-management') }}" style="padding-left: 27px;"><span id="hide">Team Management</span></a></li>
                <li{{ Route::current()->getPath() == 'user-management/permissions' ? ' class=active' : '' }}><a href="{{ url('user-management/permissions') }}" style="padding-left: 27px;"><span id="hide">Permissions</span></a></li>
                <li{{ Route::current()->getPath() == 'user-management/system-user-log' ? ' class=active' : '' }}><a href="{{ url('user-management/system-user-log') }}" style="padding-left: 27px;"><span id="hide">System User Log</span></a></li> 
                <li{{ Route::current()->getPath() == 'user-management/sales-user-log' ? ' class=active' : '' }}><a href="{{ url('user-management/sales-user-log') }}" style="padding-left: 27px;"><span id="hide">Sales User Log</span></a></li>
                <li{{ Route::current()->getPath() == 'user-management/team-management-log' ? ' class=active' : '' }}><a href="{{ url('user-management/team-management-log') }}" style="padding-left: 27px;"><span id="hide">Team Management Log</span></a></li> 
            </div>
        </ul>
        
        </li>

    </div>
   @endif

  

   
   @if($permission_payroll->view == 1)
    <div class="panel panel-default">
        <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionTwo">
            <a class="panel-title accordion-toggle">
                <i class="fa fa-credit-card"></i><span id="hide"> Payroll Management</span>
                <i class="{{ Route::current()->getPath() == 'payroll-management/upload-data-feeds' | Route::current()->getPath() == 'payroll-management/generate-payroll' | Route::current()->getPath() == 'payroll-management/incentives-or-deductions' | Route::current()->getPath() == 'payroll-management/payroll-computation' | Route::current()->getPath() == 'payroll-management/payroll-summary' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
            </a>
        </li>
        <li id="accordionTwo" class="{{ Route::current()->getPath() == 'payroll-management/upload-data-feeds' | Route::current()->getPath() == 'payroll-management/generate-payroll' | Route::current()->getPath() == 'payroll-management/incentives-or-deductions' | Route::current()->getPath() == 'payroll-management/payroll-computation' | Route::current()->getPath() == 'payroll-management/payroll-summary' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
            <ul id="menu1" class="inner menu-pad">
                @if($permission_rank == null || $permission_rank->rank!="Advisor")
                @if(Auth::user()->usertype_id != 8)
                <li{{ Route::current()->getPath() == 'payroll-management/upload-data-feeds' ? ' class=active' : '' }}><a href="{{ url('payroll-management/upload-data-feeds') }}"><span id="hide">Upload Data Feeds</span></a></li>
                <li{{ Route::current()->getPath() == 'payroll-management/generate-payroll' ? ' class=active' : '' }}><a href="{{ url('payroll-management/generate-payroll') }}"><span id="hide">Generate Payroll</span></a></li>
                @endif
                @if($permission_rank == null || $permission_rank->tier != "2")
                <?php
                    if (Auth::user()->usertype_id == 8) {
                        $get_sales = App\Sales::where('user_id', Auth::user()->id)->first();
                        if ($get_sales) {
                            if ($get_sales->designation_id != 1 && $get_sales->designation_id != 2 && $get_sales->designation_id != 3 && $get_sales->designation_id != 4) {
                                echo('<li' . (Route::current()->getPath() == 'payroll-management/incentives-or-deductions' ? ' class=active' : '') . '><a href="' . url('payroll-management/incentives-or-deductions') . '"><span id="hide">Incentives or Deductions</span></a></li>');
                            }
                        }
                    } else {
                        echo('<li' . (Route::current()->getPath() == 'payroll-management/incentives-or-deductions' ? ' class=active' : '') . '><a href="' . url('payroll-management/incentives-or-deductions') . '"><span id="hide">Incentives or Deductions</span></a></li>');
                    }
                ?>
                @if (Auth::user()->usertype_id != 8)
                <li{{ Route::current()->getPath() == 'payroll-management/payroll-computation' ? ' class=active' : '' }}><a href="{{ url('payroll-management/payroll-computation') }}"><span id="hide">Payroll Computation</span></a></li>
                @endif
                @endif
                @endif
                <li{{ Route::current()->getPath() == 'payroll-management/payroll-summary' ? ' class=active' : '' }}><a href="{{ url('payroll-management/payroll-summary') }}"><span id="hide">Payroll Summary</span></a></li>
             
<!--                 <li{{ Route::current()->getPath() == 'payroll/payroll-summary-supervisor' ? ' class=active' : '' }}><a href="{{ url('payroll/payroll-summary-supervisor') }}"><span id="hide">Payroll Summary</span></a></li>    -->
            </ul>
        </li>
    </div>
    @endif
   
    @if($permission_policy->view == 1)
    <div class="panel panel-default">
        <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionThree">
            <a class="panel-title accordion-toggle">
                <i class="fa fa-book"></i><span id="hide"> Production & Policy Mgt</span>
                <i class="{{ Route::current()->getPath() == 'production-and-policy-mgt/policies' | Route::current()->getPath() == 'production-and-policy-mgt/policies/riders' | Route::current()->getPath() == 'production-and-policy-mgt/introducers' | Route::current()->getPath() == 'production-and-policy-mgt/policy-log' | Route::current()->getPath() == 'production-and-policy-mgt/orphan-policy-service' | Route::current()->getPath() == 'production-and-policy-mgt/duplicates' | Route::current()->getPath() == 'production-and-policy-mgt/life-submission' | Route::current()->getPath() == 'production-and-policy-mgt/gi-submission' | Route::current()->getPath() == 'production-and-policy-mgt/life-submission' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
            </a>
        </li>
        <li id="accordionThree" class="{{ Route::current()->getPath() == 'production-and-policy-mgt/policies' | Route::current()->getPath() == 'production-and-policy-mgt/policies/riders' | Route::current()->getPath() == 'production-and-policy-mgt/introducers' | Route::current()->getPath() == 'production-and-policy-mgt/policy-log' | Route::current()->getPath() == 'production-and-policy-mgt/orphan-policy-service' | Route::current()->getPath() == 'production-and-policy-mgt/duplicates' | Route::current()->getPath() == 'production-and-policy-mgt/life-submission' | Route::current()->getPath() == 'production-and-policy-mgt/gi-submission' |Route::current()->getPath() == 'production-and-policy-mgt/pending-cases' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? 'panel-collapse collapse in' : 'panel-collapse collapse' }}">
            <ul id="menu1" class="inner menu-pad">
                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/life-submission' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/life-submission') }}"><span id="hide">Life Submission</span></a></li>
                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/gi-submission' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/gi-submission') }}"><span id="hide">GI Submission</span></a></li>
                @if (Auth::user()->usertype_id != 8)
                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/pending-cases' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/pending-cases') }}"><span id="hide">Pending Cases</span></a></li>
                @endif
                <div class="panel panel-default">

                


                    <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"href="#c" style="background-color: transparent; padding-left: 0px;">
                        <a class="panel-title">
                            <span id="hide"> Policies</span>
                            <i class="{{ Route::current()->getPath() == 'production-and-policy-mgt/policies' | Route::current()->getPath() == 'production-and-policy-mgt/policies/riders' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                        </a>
                    </div>

                    



                <li id="c" class="{{ Route::current()->getPath() == 'production-and-policy-mgt/policies'  | Route::current()->getPath() == 'production-and-policy-mgt/policies/riders' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                    <ul id="menu" class="inner menu-padd">
                      <li{{ Route::current()->getPath() == 'production-and-policy-mgt/policies' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/policies') }}"><span id="hide">All</span></a></li>
                      <!-- <li{{ Route::current()->getPath() == 'policy/production/case-submission-supervisor' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-submission-supervisor') }}"><span id="hide">Case Submission (Supervisor)</span></a></li> -->                      <!-- <li{{ Route::current()->getPath() == 'summary-and-reports/bsc-report' ? ' class=active' : '' }}><a href="{{ url('summary-and-reports/bsc-report') }}"><span id="hide">BSC</span></a></li> -->
                      @if (Auth::user()->usertype_id != 8)
                      <li{{ Route::current()->getPath() == 'production-and-policy-mgt/policies/riders' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/policies/riders') }}"><span id="hide">Riders</span></a></li>
                      @endif
                      <!-- <li{{ Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-inception-supervisor') }}"><span id="hide">Case Inception (Supervisor)</span></a></li>
                    </ul> -->
                </li>
                </div>
                <!-- <li{{ Route::current()->getPath() == 'production-and-policy-mgt/policies' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/policies') }}"><span id="hide">Policies</span></a></li>
                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/policies/riders' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/policies/riders') }}"><span id="hide">Riders</span></a></li> -->
                @if (Auth::user()->usertype_id != 8)
                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/introducers' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/introducers') }}"><span id="hide">Introducers</span></a></li>
                @endif
                
                @if(Auth::user()->usertype_id == 1) 


                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/orphan-policy-service' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/orphan-policy-service') }}"><span id="hide">Orphan Policy Service</span></a></li>
                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/duplicates' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/duplicates') }}"><span id="hide">Duplicates</span></a></li>


                @endif
                <!-- <div class="panel panel-default">
                    <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"href="#c" style="background-color: transparent; padding-left: 0px;">
                        <a class="panel-title">
                            <span id="hide"> Production</span>
                            <i class="{{ Route::current()->getPath() == 'production-and-policy-mgt/life-submission' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                        </a>
                    </div>
                <li id="c" class="{{ Route::current()->getPath() == 'production-and-policy-mgt/life-submission' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                    <ul id="menu" class="inner menu-padd">
                      <li{{ Route::current()->getPath() == 'production-and-policy-mgt/life-submission' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/life-submission') }}"><span id="hide">Case Submission</span></a></li>
                      <!-- <li{{ Route::current()->getPath() == 'policy/production/case-submission-supervisor' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-submission-supervisor') }}"><span id="hide">Case Submission (Supervisor)</span></a></li> -->
                      <!-- <li{{ Route::current()->getPath() == 'policy/production/case-inception' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-inception') }}"><span id="hide">Case Inception</span></a></li> -->
                      <!-- <li{{ Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-inception-supervisor') }}"><span id="hide">Case Inception (Supervisor)</span></a></li>
                    </ul> -->
                <!-- </li>
                </div> -->
                
                @if($permission_rank == null || $permission_rank->rank!="Advisor")
                @if($permission_rank == null || $permission_rank->tier != "2")
                @if(Auth::user()->usertype_id != 8)
                <li{{ Route::current()->getPath() == 'production-and-policy-mgt/policy-log' ? ' class=active' : '' }}><a href="{{ url('production-and-policy-mgt/policy-log') }}"><span id="hide">Policy Log</span></a></li>
                @endif
                @endif
                @endif
            </ul>
        </li>
    </div>
    @endif
    @if($permission_provider->view == 1)
        <div class="panel panel-default">
            <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionFour">
                <a class="panel-title accordion-toggle">
                      <i class="fa fa-clipboard"></i><span id="hide"> Provider & Products Mgt</span>
                      <i class="{{ Route::current()->getPath() == 'provider-and-products-mgt/provider-list' | Route::current()->getPath() == 'provider-and-products-mgt/product-list' | Route::current()->getPath() == 'provider-and-products-mgt/product-log' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                </a>
            </li>
                <li id="accordionFour" class="{{ Route::current()->getPath() == 'provider-and-products-mgt/provider-list' | Route::current()->getPath() == 'provider-and-products-mgt/product-list' | Route::current()->getPath() == 'provider-and-products-mgt/product-log' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                    <ul id="menu1" class="inner menu-pad">
                        @if($permission_rank == null || $permission_rank->rank!="Advisor" )
                        @if($permission_rank == null || $permission_rank->tier != "2")
                        @if (Auth::user()->usertype_id != 8)
                        <li{{ Route::current()->getPath() == 'provider-and-products-mgt/provider-list' ? ' class=active' : '' }}><a href="{{ url('provider-and-products-mgt/provider-list') }}"><span id="hide">Provider List</span></a></li>
                        @endif
                        @endif
                        @endif
                        <li{{ Route::current()->getPath() == 'provider-and-products-mgt/product-list' ? ' class=active' : '' }}><a href="{{ url('provider-and-products-mgt/product-list') }}"><span id="hide">Product List</span></a></li>
                        @if($permission_rank == null || $permission_rank->rank!="Advisor" )
                        @if($permission_rank == null || $permission_rank->tier != "2")
                        @if (Auth::user()->usertype_id != 8)
                        <li{{ Route::current()->getPath() == 'provider-and-products-mgt/product-log' ? ' class=active' : '' }}><a href="{{ url('provider-and-products-mgt/product-log') }}"><span id="hide">Product Log</span></a></li>
                        @endif
                        @endif
                        @endif
                    </ul>
                </li>
        </div>
    @endif
         <div class="panel panel-default">
            @if($permission_reports->view == 1)
            <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionFive">
                <a class="panel-title accordion-toggle">
                    <i class="fa fa-credit-card"></i><span id="hide"> Summary and Reports</span>
                    <i class="{{ Route::current()->getPath() == 'summary' | Route::current()->getPath() == 'summary-and-reports/bsc-report' | Route::current()->getPath() == 'reports' | Route::current()->getPath() == 'summary-and-reports/cpd-report' | Route::current()->getPath() == 'reports/export' | Route::current()->getPath() == 'summary-and-reports/cpd-management' | Route::current()->getPath() == 'summary-and-reports/ongoing-fees-report' | Route::current()->getPath() == 'summary-and-reports/initial-fees-report' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                </a>
            </li>
                <li id="accordionFive" class="{{ Route::current()->getPath() == 'summary' | Route::current()->getPath() == 'summary-and-reports/bsc-report' | Route::current()->getPath() == 'reports' | Route::current()->getPath() == 'summary-and-reports/cpd-report' | Route::current()->getPath() == 'reports/export' | Route::current()->getPath() == 'summary-and-reports/ongoing-fees-report' | Route::current()->getPath() == 'summary-and-reports/cpd-management' | Route::current()->getPath() == 'summary-and-reports/initial-fees-report' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                  <ul id="menu1" class="inner menu-pad">
                    <li{{ Route::current()->getPath() == 'summary' ? ' class=active' : '' }}><a><span id="hide">Consolidated Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports' ? ' class=active' : '' }}><a><span id="hide">Consolidated Report - Summary</span></a></li>
                    <li{{ Route::current()->getPath() == 'summary-and-reports/ongoing-fees-report' ? ' class=active' : '' }}><a href="{{ url('summary-and-reports/ongoing-fees-report') }}"><span id="hide"> Ongoing Fees Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'summary-and-reports/initial-fees-report' ? ' class=active' : '' }}><a href="{{ url('summary-and-reports/initial-fees-report') }}"><span id="hide"> Initial Fees Report</span></a></li>  
                    <li{{ Route::current()->getPath() == 'reports/export' ? ' class=active' : '' }}><a><span id="hide">Production Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports/export' ? ' class=active' : '' }}><a><span id="hide">Persistency Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'summary-and-reports/bsc-report' ? ' class=active' : '' }}><a href="{{ url('summary-and-reports/bsc-report') }}"><span id="hide">BSC Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'summary-and-reports/cpd-management' ? ' class=active' : '' }}><a href="{{ url('summary-and-reports/cpd-management') }}" style="padding-left: 27px;"><span id="hide">CPD Management</span></a></li>    
                    @if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 || Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10 )
                    <li{{ Route::current()->getPath() == 'summary-and-reports/cpd-report' ? ' class=active' : '' }}><a href="{{ url('summary-and-reports/cpd-report') }}"><span id="hide">CPD Report</span></a></li>   
                    @endif

                  </ul>
                </li>
            @endif
                @if(Auth::user()->usertype_id == 1)   
                <li{{ Route::current()->getPath() == 'settings' ? ' class=active' : '' }}><a href="{{ url('settings') }}"><i class="fa fa-cog"></i><span id="hide"> Settings</span></a></li>   
                @endif
            </div>
          </div>
        </div>
      </ul>
    </div>
    <!-- #sidebar-wrapper -->
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #ffffff">
        <div class="container-fluid">
            <div class="pull-left">
            <a ></a>
            <a href="#menu-toggle" id="menu-toggle" type="button" class="btn btn-default"><i class="fa fa-bars"></i></a>
            </div>
            <div style="text-align: right; padding-left: 20px;">                

<div class="dropdown">
  <a id="dLabel" role="button">
    <button id="bell" type="button" class="btn btn-default" ><i class="fa fa-bell"></i>
    </button>
  </a>
  <a class="hide" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
    
@if ($noti_count >= 1)
 <button id="bell" type="button" class="btn btn-default btn-circle" ><i class="fa fa-bell" style="color: #ffffff;"></i>
    <span class="noti-count">{{ $noti_count }}</span>
@else
<button id="bell" type="button" class="btn btn-default" ><i class="fa fa-bell"></i>
@endif  
       </button>
  </a>
    <ul class="notifications dropdown-menu dropdown-menu pull-right" style="width: 280px; padding-top:0; border-radius: 0;" role="menu" aria-labelledby="dLabel" >
    <div class="notification-heading"><span class="menu-title"><small>Notifications</small></span>
    </div>
   <div class="notifications-wrapper">
     @foreach($notification as $row)
     <a style="display:block;" class="content" href="{{ $row->link }}">
       <div class="notification-item">
            <h4 class="item-title">{!! link_to_action($row->controller, $row->name, $row->notification_id) !!} {!! link_to_action($row->controller, $row->activity, $row->notification_id) !!} <br>
            <small>
            {{ $ago = $row->created_at->diffForHumans( Carbon::now() ) }}
            </small>
            </h4>
        </div> 
    </a>
      @endforeach
      @if (count($notification) < 1)
        <a style="display:block;" class="content">
            <div class="notification-item">
                <h4 class="item-title"> No Available Notifications.<br>
                    <small>
                    </small>
                </h4>
            </div> 
        </a>
      @endif
   </div>
</ul>
</div>
                <a id="profile" class="btn btn-default dropdown-toggle profile_padding" type="button" data-toggle="dropdown"><span id="name-auth" style="padding-left:5px;padding-right:5px;">{{ Auth::user()->name }}</span> <img src="{{ url('uploads', Auth::user()->photo) }}" style="color: #79828f;" id="photo-auth" class="img-circle" alt="Smiley face" height="23" width="23">
                <i class="fa fa-angle-down" style="color: #79828f;padding-right:10px; padding-left:5px;"></i></a>
                <ul id="userdrop" class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">
                  <li class="hide" role="presentation"><a role="menuitem" tabindex="-1" href="#">Profile <i class="fa fa-user pull-right"></i></a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="settings-btn">Settings <i class="fa fa-cog pull-right"></i></a></li>
                  <li role="presentation" class="divider"></li>
                  @if(Auth::user()->usertype_id == 1)
                  <li class="hide" role="presentation"><a role="menuitem" tabindex="-1" class="db-wipe">Wipe <i class="fa fa-table pull-right"></i></a></li>
                  @endif
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('logout') }}">Logout <i class="fa fa-sign-out pull-right"></i></a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

        <div id="content">
        
        @yield('content')
        
        @if(Auth::check())
        <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-dialog" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default borderzero" data-url="" data-id="" id="dialog-confirm">Yes</button>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modal-profile" tabindex="-1" role="dialog" aria-labelledBy="profile-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-md">
            <div class="modal-content borderzero">
                <div id="load-profile" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'profile/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_profile', 'files' => 'true')) !!}
              <div class="modal-header tbheader">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="profile-title"><i class="fa fa-cog"></i> Settings</h4>
              </div>
              <div class="modal-body">
               <div id="profile-notice"></div>
                <div class="form-group">
                    <div id="image" class="container-fluid col-sm-12 col-xs-7">
                        <!-- <div style="margin-bottom:2px;" id="image_settings"></div> -->
                        <input type="hidden" id="image_fb_link_for_system_user" name="image_link">
                        <div id="system_image_pane"></div>
                        <input type="file" class="file file_photo" name="photo" id="row-photo" data-show-upload="false" placeholder="Upload a photo..." file-accept="jpg gif jpeg png bmp">
                        <br>
                <div class="text-center col-lg-12 ">
                <span id="name-auth">{{ Auth::user()->system_id }}</span>
                </div>
                </div>
                </div>
                <hr>
                <div class="col-sm-12 nopad">
                <div class="col-sm-6">
                <div class="form-group error-code">
                <label class="col-xs-12" for="settings-code">User Code</label>
                <div class="col-xs-12">
                <input type="text" name="code" class="form-control borderzero" id="settings-code" maxlength="15" placeholder="Enter your user code...">
                </div>
                <sup class="sup-errors"></sup>
                </div>
                <div class="form-group error-name">
                <label class="col-xs-12" for="settings-name">Last Name</label>
                <div class="col-xs-12">
                <input type="text" name="name" class="form-control borderzero" id="settings-name" maxlength="15" placeholder="Enter your last name">
                </div>
                <sup class="sup-errors"></sup>
                </div>
                <div class="form-group error-email">
                <label class="col-xs-12" for="settings-email">Email</label>
                <div class="col-xs-12">
                <input type="email" name="email" class="form-control borderzero" id="settings-email" maxlength="15" placeholder="Enter your email">
                </div>
                <sup class="sup-errors"></sup>
                </div>
                <div class="form-group hide">
                <label for="row-social_media" class="col-xs-12">Social Media</label>
                <div class="col-xs-12">
                <fb:login-button size="medium" scope='public_profile,email' onlogin='checkLoginState();' data-show-faces="false" data-auto-logout-link="true" class="pull-left"  data-toggle="tooltip" title="This will automatically fetch your facebook profile picture">
                 Link with Facebook
                </fb:login-button>
                </div>
                </div>
                <div class="form-group error-mobile">
                <label class="col-xs-12" for="settings-mobile">Contact Number</label>
                <div class="col-xs-12">
                <input type="text" name="mobile" class="form-control borderzero" id="settings-mobile" maxlength="15" placeholder="Enter your contact number">
                </div>
                <sup class="sup-errors"></sup>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group error-password">
                <label class="col-xs-12" for="settings-password">Password</label>
                <div class="col-xs-12">
                <input type="password" name="password" class="form-control borderzero pwd-profile" id="settings-password" maxlength="15" placeholder="Leave blank for unchanged">
                </div>
                <sup class="sup-errors"></sup>
                </div>
                <div class="form-group error-password_confirmation">
                <label class="col-xs-12" for="settings-password_confirmation">Confirm Password</label>
                <div class="col-xs-12">
                <input type="password" name="password_confirmation" class="form-control borderzero" id="settings-password_confirmation" maxlength="15" placeholder="Leave blank for unchanged">
                </div>
                <sup class="sup-errors"></sup>
                </div>
                <div id="pwd-container_profile">
                <input type="hidden" class="total-score" name="pass_score">
                <div class="pwstrength_viewport_progress"></div>
                </div>
                <div class="error-pass_score">
                <sup class="sup-errors"></sup>
                </div>
            </div>
        </div>
              </div>
              <div class="clearfix"></div>
              <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="settings-id" value="">

                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit borderzero"><i class="fa fa-save"></i> Save changes</button>
                <button type="button" class="btn btn-primary borderzero" id="settings-photo-change">Change</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
        @endif

        </div>

    </div><!-- #wrapper -->

    <footer></footer>
    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}

    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    
    <!-- Sidebar Scripts -->
    {!! HTML::script('/js/sidebar.js') !!}

    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/passwordstrength.js') !!}
    {!! HTML::script('/js/plugins/passwordstrength-profile.js') !!}
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/raphael-min.js') !!}
    {!! HTML::script('/js/plugins/morris.js') !!}
    {!! HTML::script('/js/plugins/graph.js') !!}
    {!! HTML::script('/js/plugins/modernizr.js') !!}
    {!! HTML::script('/js/plugins/jquery.dataTables.js') !!}
    {!! HTML::script('/js/plugins/fileinput.min.js') !!}
    {!! HTML::script('/js/plugins/select2.min.js') !!}
    {!! HTML::script('/js/plugins/bootstrap-multiselect.js') !!}
    {!! HTML::script('/js/plugins/clipboard.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! HTML::script('/plugins/tooltipster/js/tooltipster.bundle.min.js') !!}

    {!! HTML::script('/js/main.js') !!}

    @yield('scripts')
    
    <script type="text/javascript">
   @if(Auth::check())
   @if(Auth::user()->usertype_id == 8)
    $_token = '{{ csrf_token() }}';
$(function () {
  $(".dashboard_i-date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard_c-date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard-date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$(function () {
  $(".dashboard-case_date").datetimepicker({
      locale: "en",
      format: "MMM YYYY",
      ignoreReadonly: true,
      useCurrent: false
  });
});

$("#dashboard_i-date").val('<?php isset($dashboard_date) ? $dashboard_date : '' ?>');
$("#dashboard_i-hidden").val('<?php isset($dashboard_date) ? $dashboard_date : '' ?>');
$("#dashboard_i-hidden-refresh").val('<?php isset($dashboard_date) ? $dashboard_date : '' ?>');

$("#dashboard_c-date").val('<?php isset($dashboard_case_date) ? $dashboard_case_date : '' ?>');
$("#dashboard_c-hidden").val('<?php isset($dashboard_case_date) ? $dashboard_case_date : '' ?>');
$("#dashboard_c-hidden-refresh").val('<?php isset($dashboard_case_date) ? $dashboard_case_date : '' ?>');

$(".dashboard_i-date").on("dp.change", function(e) {
  var ctr = parseInt($("#dashboard_i-ctr").val());
  if (ctr > 0) {
    search_inceptions();
  } else {
    $("#dashboard_i-ctr").val(ctr+1);
  }
});

function search_inceptions() {
  var date = $("#dashboard_i-date").val();
  var loading = $("#loading-panels");
  loading.removeClass('hide');

  $.post("{{ url('inception/month') }}", { date: date, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $(".inception-dashboard").html(response);
      updateProductionInception();
      loading.addClass('hide');
    }
  });
}

$(".dashboard_c-date").on("dp.change", function(e) {
  var ctr = parseInt($("#dashboard_c-ctr").val());

  if (ctr > 0) {
    search_submissions();
  } else {
    $("#dashboard_c-ctr").val(ctr+1);
  }
});

function search_submissions() {
  var date = $("#dashboard_c-date").val();
  var loading = $("#loading-panels");
  loading.removeClass('hide');

  $.post("{{ url('case/month') }}", { date: date, _token: $_token }, function(response) {
    if (response.unauthorized) {
      window.location.href = response.unauthorized;
    } else {
      $(".case-dashboard").html(response);
      updateProductionSubmission();
      loading.addClass('hide');
    }
  });
}

$(document).ready(function(){

    var i_str_mtd_ape = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-mtd-ape-{{Auth::user()->id}}').text();
    var i_str_mtd_gross = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-mtd-gross-{{Auth::user()->id}}').text();
    var i_str_mtd_cat = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-mtd-cat-{{Auth::user()->id}}').text();
    var i_str_mtd_gi = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-mtd-gi-{{Auth::user()->id}}').text();
    var i_str_ytd_ape = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-ytd-ape-{{Auth::user()->id}}').text();
    var i_str_ytd_gross = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-ytd-gross-{{Auth::user()->id}}').text();
    var i_str_ytd_cat = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-ytd-cat-{{Auth::user()->id}}').text();
    var i_str_ytd_gi = $(document).find('tr#i-user-id-{{Auth::user()->id}}').children('td.i-ytd-gi-{{Auth::user()->id}}').text();

    var s_str_mtd_ape = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-mtd-ape-{{Auth::user()->id}}').text();
    var s_str_mtd_cat = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-mtd-cat-{{Auth::user()->id}}').text();
    var s_str_mtd_gi = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-mtd-gi-{{Auth::user()->id}}').text();
    var s_str_ytd_ape = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-ytd-ape-{{Auth::user()->id}}').text();
    var s_str_ytd_cat = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-ytd-cat-{{Auth::user()->id}}').text();
    var s_str_ytd_gi = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-ytd-gi-{{Auth::user()->id}}').text();

    if($(document).find('tr#i-user-id-{{Auth::user()->id}}').length > 0)
    {
        $('td.i-cp-mtd-ape-{{Auth::user()->id}}').text(i_str_mtd_ape);
        $('td.i-cp-mtd-gross-{{Auth::user()->id}}').text(i_str_mtd_gross);
        $('td.i-cp-mtd-cat-{{Auth::user()->id}}').text(i_str_mtd_cat);
        $('td.i-cp-mtd-gi-{{Auth::user()->id}}').text(i_str_mtd_gi);
        $('td.i-cp-ytd-ape-{{Auth::user()->id}}').text(i_str_ytd_ape);
        $('td.i-cp-ytd-gross-{{Auth::user()->id}}').text(i_str_ytd_gross);
        $('td.i-cp-ytd-cat-{{Auth::user()->id}}').text(i_str_ytd_cat);
        $('td.i-cp-ytd-gi-{{Auth::user()->id}}').text(i_str_ytd_gi); 
    }
    
    if($(document).find('tr#s-user-id-{{Auth::user()->id}}').length > 0)
    {
        $('td.s-cp-mtd-ape-{{Auth::user()->id}}').text(s_str_mtd_ape);
        $('td.s-cp-mtd-cat-{{Auth::user()->id}}').text(s_str_mtd_cat);
        $('td.s-cp-mtd-gi-{{Auth::user()->id}}').text(s_str_mtd_gi);
        $('td.s-cp-ytd-ape-{{Auth::user()->id}}').text(s_str_ytd_ape);
        $('td.s-cp-ytd-cat-{{Auth::user()->id}}').text(s_str_ytd_cat);
        $('td.s-cp-ytd-gi-{{Auth::user()->id}}').text(s_str_ytd_gi);
    }

});

function updateProductionInception(){
    var str_mtd_ape = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-mtd-ape-{{Auth::user()->id}}').text();
    var str_mtd_gross = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-mtd-gross-{{Auth::user()->id}}').text();
    var str_mtd_cat = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-mtd-cat-{{Auth::user()->id}}').text();
    var str_mtd_gi = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-mtd-gi-{{Auth::user()->id}}').text();
    var str_ytd_ape = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-ytd-ape-{{Auth::user()->id}}').text();
    var str_ytd_gross = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-ytd-gross-{{Auth::user()->id}}').text();
    var str_ytd_cat = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-ytd-cat-{{Auth::user()->id}}').text();
    var str_ytd_gi = $(document).find('tr#user-id-{{Auth::user()->id}}').children('td.i-ytd-gi-{{Auth::user()->id}}').text();
    
    if($(document).find('tr#i-user-id-{{Auth::user()->id}}').length > 0)
    {
        $('td.i-cp-mtd-ape-{{Auth::user()->id}}').text(str_mtd_ape);
        $('td.i-cp-mtd-gross-{{Auth::user()->id}}').text(str_mtd_gross);
        $('td.i-cp-mtd-cat-{{Auth::user()->id}}').text(str_mtd_cat);
        $('td.i-cp-mtd-gi-{{Auth::user()->id}}').text(str_mtd_gi);
        $('td.i-cp-ytd-ape-{{Auth::user()->id}}').text(str_ytd_ape);
        $('td.i-cp-ytd-gross-{{Auth::user()->id}}').text(str_ytd_gross);
        $('td.i-cp-ytd-cat-{{Auth::user()->id}}').text(str_ytd_cat);
        $('td.i-cp-ytd-gi-{{Auth::user()->id}}').text(str_ytd_gi);
    }
}

function updateProductionSubmission(){
    var s_str_mtd_ape = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-mtd-ape-{{Auth::user()->id}}').text();
    var s_str_mtd_cat = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-mtd-cat-{{Auth::user()->id}}').text();
    var s_str_mtd_gi = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-mtd-gi-{{Auth::user()->id}}').text();
    var s_str_ytd_ape = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-ytd-ape-{{Auth::user()->id}}').text();
    var s_str_ytd_cat = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-ytd-cat-{{Auth::user()->id}}').text();
    var s_str_ytd_gi = $(document).find('tr#s-user-id-{{Auth::user()->id}}').children('td.s-ytd-gi-{{Auth::user()->id}}').text();

    if($(document).find('tr#s-user-id-{{Auth::user()->id}}').length > 0)
    {
        $('td.s-cp-mtd-ape-{{Auth::user()->id}}').text(s_str_mtd_ape);
        $('td.s-cp-mtd-cat-{{Auth::user()->id}}').text(s_str_mtd_cat);
        $('td.s-cp-mtd-gi-{{Auth::user()->id}}').text(s_str_mtd_gi);
        $('td.s-cp-ytd-ape-{{Auth::user()->id}}').text(s_str_ytd_ape);
        $('td.s-cp-ytd-cat-{{Auth::user()->id}}').text(s_str_ytd_cat);
        $('td.s-cp-ytd-gi-{{Auth::user()->id}}').text(s_str_ytd_gi);
    }
}
    @endif  
    @endif
    
    $(document).ready(function() 
    {
    $('.panel-title').click(function(e) 
    { 
     $('.inner a span').addClass('animated fadeInLeft');
    });
    });

    function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('fa-angle-down fa-angle-left');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);

    function refresh_profile() {

        $_token = '{{ csrf_token() }}';

        $.post("{{ url('profile/refresh') }}", { _token: $_token }, function(response) {

            $("#name-auth").html("");
            $('#name-auth').html(response.rows.name);
            $('#photo-auth').attr("src", "{{ url('uploads') }}/" + response.rows.photo + "?" + new Date().getTime());

        }, 'json');
    }

    $(document).ready(function(){
        $('#sales_id').multiselect();
        $('[data-toggle="tooltip"]').tooltip();

        $("#userdrop").on("click", ".settings-btn", function() {
            $_token = "{{ csrf_token() }}";
            
            $(".pwstrength_viewport_progress div:nth-child(2)").removeClass("progress").html("");

              $(".pwstrength_viewport_progress .progress-bar").attr('style', 'width: ' + 0 + '%;');
              $(".password-verdict").html("Weak");
              
            $('#modal-save_profile').find('.file-input').addClass('hide');
            $("#system_image_pane").removeClass("hide");
              // reset all form fields
            $("#modal-profile").find('form').trigger("reset");
                $.post("{{ url('profile/info') }}", { _token: $_token }, function(response) {
                    
                    if(!response.error) {
                        // set form title
                        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
                        $(".profile-title").html("<i class='fa fa-cog'></i> <strong>" + response.name + '</strong>');

                        // output form data
                        $.each(response, function(index, value) {
                              var field = $("#settings-" + index);

                            // field exists, therefore populate
                            if(field.length > 0) {
                                field.val(value);
                            }
                        });
                        $("#system_image_pane").html('<img id="settings-photo" class="img-responsive img-circle" src="'+response.photo+'" height="100" width="100">');
                        // Hide photo input.
                        $('#modal-save_profile').find('.file-input').addClass('hide');
                        // Remove selected photo input.
                        $('#modal-save_profile').find('.fileinput-remove').trigger('click');
                        // reset back to change
                        $('#settings-photo-change').text('Change Photo');
                        // show current photo
                        $('#settings-photo').removeClass('hide');

                        // show form
                        $('#settings-photo').attr('src', '{{ url('uploads') }}/' + response.photo + '?' + new Date().getTime());
                        $("#modal-profile").modal('show');
                    } else {
                        status(false, response.error, 'alert-danger');
                    }

                }, 'json');
        });


        $("#userdrop").on("click", ".db-wipe", function() {
            $_token = "{{ csrf_token() }}";
            console.log('wipe');
            $.post("{{ url('wipe') }}", { _token: $_token }, function(response) {

            });
        });
    
        // photo handler
        $('#settings-photo-change').click(function() {
            if ($(this).text() == 'Change Photo') {
                $(this).text('Cancel');
                $('#settings-photo').addClass('hide');
                $('#modal-save_profile').find('.file-input').removeClass('hide');
                $('#photo').click();
                $("#system_image_pane").addClass("hide");
            } else {
                $(this).text('Change Photo');
                $('#system_image_pane').removeClass('hide');
                $('.img-circle').removeClass("hide");
                // Hide photo input.
                $('#modal-save_profile').find('.file-input').addClass('hide');
                // Remove selected photo input.
                $('#modal-save_profile').find('.fileinput-remove').click();
            }
        });

        // profile closing
        $('#modal-profile').on('hidden.bs.modal', function () {
            $('#profile-notice').html("");
        });
    });
     function statusChangeCallback(response) {
    //$(".custom-photo-pane").addClass("hide");
    //$(".default-photo-pane").removeClass("hide");
   // console.log('statusChangeCallback');
   // console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {

     
      // Logged into your app and Facebook.
       $("#standard_photo").addClass("hide");
       $(".standard_photo_pane").addClass("hide");
       $("#standard_photo_for_user").addClass("hide");
       // $(".default-photo-pane").addClass("hide");
       $(".facebook-photo-pane").removeClass("hide");
       //console.log(response.authResponse.accessToken);
       ///$("#fb-connect").attr('title','Logout your facebook account');
       FB.api('/me', function(response) {
      
        $(".system-photo-pane").html("<img class='img-circle image-responsive' id='row-photo_row' height='100' width='100' style='margin: 5px;' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        //$(".system-photo-pane").append("");
        $("#image_settings").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_pane_edit_photo").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_fb_link").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_for_system_user").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_edit_photo").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        //$("#settings-photo").attr("src","http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#system_image_pane").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_pane_edit_photo").removeClass("hide");
        $("#image_pane").removeClass("hide");
        $("#user_photo_pane").addClass("hide");
        $("#image_settings").removeClass("hide");
        $("#row-photo").addClass("hide");
        $("#settings-photo-change").text('Change Photo');

        });
      $("#status").val(1);

    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
    
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");


    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
        $("#row-photo").removeClass("hide");
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");
        $("#image_pane").addClass("hide");
        $("#image_pane_edit_photo").addClass("hide");
        $("#user_photo_pane").removeClass("hide");
        $(".facebook-photo-pane").addClass("hide");
        $("#image_settings").addClass("hide");
        $("#image_settings").html("");
        $("#system_image_pane").html('<img id="settings-photo" class="img-responsive img-circle" src="{{ url("uploads")."/".Auth::user()->photo }}" height="100" width="100">');
        $(".system-photo-pane").html("<img id='row-photo_row' src='{{url('uploads').'/'.$settings->default_photo}}' class='preview img-circle' name='photo' height='100' width='100' style='margin:5px;'/>");
        $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        
        //$(".system-photo-pane").append("<div class='change'><input name='photo' id='row-photo_upload' type='file' class='file borderzero file_photo' data-show-upload='false' placeholder='Upload a photo...'></div>");
        $("#status").val(0);
        // $(".custom-photo-pane").html("");
        // $(".default-photo-pane").html("");


        //$("#row-photo_row").removeClass("hide");
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '395487573951234',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);

  });
  };
 
    // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
    });

    </script>

</body>

</html>
