
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ $title }}</title>

    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/toastr.min.css') !!}
    {!! HTML::style('/css/plugins/fileinput.min.css') !!}
    {!! HTML::style('/css/plugins/jquery.dataTables.min.css') !!}
    {!! HTML::style('/css/plugins/select2-bootstrap.css') !!}
    {!! HTML::style('/css/plugins/select2.css') !!}
    {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Main Stylesheet -->
    {!! HTML::style('/css/content.css') !!}
    {!! HTML::style('/css/navbar.css') !!}
    {!! HTML::style('/css/sidebar.css') !!}
    {!! HTML::style('/css/accordion.css') !!}
    {!! HTML::style('/css/animate.css') !!}

</head>

<body>

    <div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a href="#menu-toggle3" id="menu-toggle3" ><i class="fa fa-close pull-right" style="color: #79828f; padding-top: 24px; padding-right: 18px;"></i></a>
            <li class="sidebar-brand">
                <a href="#" style="color: #79828f;">
                <i class="fa fa-cubes pull-left main-logo" style="color: #79828f;"><span id="hide"> LEGACYGROUP</span></i>
                </a>
            </li>   
    <div id="side">
            <li{{ Route::current()->getPath() == '/' ? ' class=active' : '' }}><a href="{{ url('/') }}"><i class="fa fa-cube"></i><span id="hide"> Dashboard</span></a></li>
   
    <div class="panel-group" id="accordion">  
        @if ($permission->view == 1)         
            <div class="panel panel-default">
                <li class="panel-heading"  data-toggle="collapse" data-parent="#accordion" href="#accordionOne">
                    <a class="panel-title accordion-toggle">
                        <i class="fa fa-users"></i><span id="hide"> User Management</span>
                        <i class="{{ Route::current()->getPath() == 'users' | Route::current()->getPath() == 'users/ceo' | Route::current()->getPath() == 'users/it-users' | Route::current()->getPath() == 'users/admin-accounts' | Route::current()->getPath() == 'users/accounting' | Route::current()->getPath() == 'users/sales-assistant' | Route::current()->getPath() == 'users/permissions' | Route::current()->getPath() == 'users/logs' | Route::current()->getPath() == 'sales' | Route::current()->getPath() == 'sales/fsc-elite' | Route::current()->getPath() == 'sales/sfsc' | Route::current()->getPath() == 'sales/pfsc' | Route::current()->getPath() == 'sales/efsc' | Route::current()->getPath() == 'sales/fsc' | Route::current()->getPath() == 'sales/fsm' | Route::current()->getPath() == 'sales/afsd' | Route::current()->getPath() == 'sales/fsd' | Route::current()->getPath() == 'sales/partner' | Route::current()->getPath() == 'sales/team-management' | Route::current()->getPath() == 'sales/team-management/incomplete' | Route::current()->getPath() == 'sales/team-management/duplicate' | Route::current()->getPath() == 'sales/logs' | Route::current()->getPath() == 'sales/{id}/edit' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                    </a>
                </li>
        <li id="accordionOne" class="{{ Route::current()->getPath() == 'users' | Route::current()->getPath() == 'users/ceo' | Route::current()->getPath() == 'users/it-users' | Route::current()->getPath() == 'users/admin-accounts' | Route::current()->getPath() == 'users/accounting' | Route::current()->getPath() == 'users/sales-assistant' | Route::current()->getPath() == 'users/permissions' | Route::current()->getPath() == 'users/logs' | Route::current()->getPath() == 'sales' | Route::current()->getPath() == 'sales/fsc-elite' | Route::current()->getPath() == 'sales/sfsc' | Route::current()->getPath() == 'sales/pfsc' | Route::current()->getPath() == 'sales/efsc' | Route::current()->getPath() == 'sales/fsc' | Route::current()->getPath() == 'sales/fsm' | Route::current()->getPath() == 'sales/afsd' | Route::current()->getPath() == 'sales/fsd' | Route::current()->getPath() == 'sales/partner' | Route::current()->getPath() == 'sales/team-management' | Route::current()->getPath() == 'sales/team-management/incomplete' | Route::current()->getPath() == 'sales/team-management/duplicate' | Route::current()->getPath() == 'sales/logs' | Route::current()->getPath() == 'sales/{id}/edit' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
            <ul id="menu" class="inner menu-pad">
    <div class="panel-group" id="accordion1">
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1" href="#a" style="background-color: transparent; padding-left: 0px;">
                <a class="panel-title">
                    <span id="hide"> System Users</span>
                    <i class="{{ Route::current()->getPath() == 'users' | Route::current()->getPath() == 'users/ceo' | Route::current()->getPath() == 'users/it-users' | Route::current()->getPath() == 'users/admin-accounts' | Route::current()->getPath() == 'users/accounting' | Route::current()->getPath() == 'users/sales-assistant' | Route::current()->getPath() == 'users/permissions' | Route::current()->getPath() == 'users/logs' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                </a>
            </div>
        <li id="a" class="{{ Route::current()->getPath() == 'users' | Route::current()->getPath() == 'users/ceo' | Route::current()->getPath() == 'users/it-users' | Route::current()->getPath() == 'users/admin-accounts' | Route::current()->getPath() == 'users/accounting' | Route::current()->getPath() == 'users/sales-assistant' | Route::current()->getPath() == 'users/permissions' | Route::current()->getPath() == 'users/logs' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
           <ul id="menu" class="inner menu-padd">
                <li{{ Route::current()->getPath() == 'users' ? ' class=active' : '' }}><a href="{{ url('users') }}"><span id="hide">All</span></a></li>
                <li{{ Route::current()->getPath() == 'users/ceo' ? ' class=active' : '' }}><a href="{{ url('users/ceo') }}"><span id="hide">CEO</span></a></li>
                <li{{ Route::current()->getPath() == 'users/it-users' ? ' class=active' : '' }}><a href="{{ url('users/it-users') }}"><span id="hide">IT Users</span></a></li>
                <li{{ Route::current()->getPath() == 'users/admin-accounts' ? ' class=active' : '' }}><a href="{{ url('users/admin-accounts') }}"><span id="hide">Admin Accounts</span></a></li>
                <li{{ Route::current()->getPath() == 'users/accounting' ? ' class=active' : '' }}><a href="{{ url('users/accounting') }}"><span id="hide">Accounting</span></a></li>
                <li{{ Route::current()->getPath() == 'users/sales-assistant' ? ' class=active' : '' }}><a href="{{ url('users/sales-assistant') }}"><span id="hide">Sales Assistant</span></a></li>
                <li{{ Route::current()->getPath() == 'users/permissions' ? ' class=active' : '' }}><a href="{{ url('users/permissions') }}"><span id="hide">Permissions</span></a></li>
                <li{{ Route::current()->getPath() == 'users/logs' ? ' class=active' : '' }}><a href="{{ url('users/logs') }}"><span id="hide">System User Log</span></a></li>                
            </ul>
        </li>
        </div>
        
        
    <div class="panel panel-default">
        <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1" href="#b" style="background-color: transparent; padding-left: 0px;">
            <a class="panel-title">
                <span id="hide"> Sales Users</span>
                <i class="{{ Route::current()->getPath() == 'sales' | Route::current()->getPath() == 'sales/fsc-elite' | Route::current()->getPath() == 'sales/sfsc' | Route::current()->getPath() == 'sales/pfsc' | Route::current()->getPath() == 'sales/efsc' | Route::current()->getPath() == 'sales/fsc' | Route::current()->getPath() == 'sales/fsm' | Route::current()->getPath() == 'sales/afsd' | Route::current()->getPath() == 'sales/fsd' | Route::current()->getPath() == 'sales/partner' | Route::current()->getPath() == 'sales/team-management' | Route::current()->getPath() == 'sales/team-management/incomplete' | Route::current()->getPath() == 'sales/team-management/duplicate' | Route::current()->getPath() == 'sales/logs' | Route::current()->getPath() == 'sales/{id}/edit' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
            </a>
        </div>
        <li id="b" class="{{ Route::current()->getPath() == 'sales' | Route::current()->getPath() == 'sales/fsc-elite' | Route::current()->getPath() == 'sales/sfsc' | Route::current()->getPath() == 'sales/pfsc' | Route::current()->getPath() == 'sales/efsc' | Route::current()->getPath() == 'sales/fsc' | Route::current()->getPath() == 'sales/fsm' | Route::current()->getPath() == 'sales/afsd' | Route::current()->getPath() == 'sales/fsd' | Route::current()->getPath() == 'sales/partner' | Route::current()->getPath() == 'sales/team-management' | Route::current()->getPath() == 'sales/team-management/incomplete' | Route::current()->getPath() == 'sales/team-management/duplicate' | Route::current()->getPath() == 'sales/logs' | Route::current()->getPath() == 'sales/{id}/edit' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">


            <ul id="menu" class="inner menu-padd">
                <li{{ Route::current()->getPath() == 'sales' ? ' class=active' : '' }}><a href="{{ url('sales') }}"><span id="hide">All</span></a></li>
                <!-- <li{{ Route::current()->getPath() == 'sales/sales-agent' ? ' class=active' : '' }}><a href="{{ url('sales/sales-agent') }}"><span id="hide">FSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/assistant-manager' ? ' class=active' : '' }}><a href="{{ url('sales/assistant-manager') }}"><span id="hide">EFSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/manager' ? ' class=active' : '' }}><a href="{{ url('sales/manager') }}"><span id="hide">PFSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/assistant-director' ? ' class=active' : '' }}><a href="{{ url('sales/assistant-director') }}"><span id="hide">SFSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/director' ? ' class=active' : '' }}><a href="{{ url('sales/director') }}"><span id="hide">FCS</span></a></li> -->
                <li{{ Route::current()->getPath() == 'sales/fsc-elite' ? ' class=active' : '' }}><a href="{{ url('sales/fsc-elite') }}" ><span id="hide" data-toggle="tooltip"  data-placement="right" title="Financial Services Consultant  - Elite">FSC - Elite</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/sfsc' ? ' class=active' : '' }}><a href="{{ url('sales/sfsc') }}"><span id="hide" data-toggle="tooltip" data-placement="right" title="Senior  Financial Services Consultant">SFSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/pfsc' ? ' class=active' : '' }}><a href="{{ url('sales/pfsc') }}"><span id="hide" data-toggle="tooltip" data-placement="right" title="Premier  Financial Services Consultant">PFSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/efsc' ? ' class=active' : '' }}><a href="{{ url('sales/efsc') }}"><span id="hide" data-toggle="tooltip" data-placement="right" title="Executive Financial Services Consultant">EFSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/fsc' ? ' class=active' : '' }}><a href="{{ url('sales/fsc') }}"><span id="hide" data-toggle="tooltip" data-placement="right" title="Financial Services Consultant">FSC</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/fsm' ? ' class=active' : '' }}><a href="{{ url('sales/fsm') }}"><span id="hide" data-toggle="tooltip" data-placement="right" title="Financial Services Manager">FSM</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/afsd' ? ' class=active' : '' }}><a href="{{ url('sales/afsd') }}"><span id="hide" data-toggle="tooltip" data-placement="right" title="Assistant Financial Services Director">AFSD</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/fsd' ? ' class=active' : '' }}><a href="{{ url('sales/fsd') }}"><span id="hide" data-toggle="tooltip" data-placement="right" title="Financial Services Director">FSD</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/partner' ? ' class=active' : '' }}><a href="{{ url('sales/partner') }}"><span id="hide">Partner</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/team-management' ? ' class=active' : '' }}><a href="{{ url('sales/team-management') }}"><span id="hide">Team Management</span></a></li>
                <li{{ Route::current()->getPath() == 'sales/cpd-management' ? ' class=active' : '' }}><a href="{{ url('sales/cpd-management') }}"><span id="hide">CPD Management</span></a></li>
                <!-- <div class="panel panel-default">
                    <div class="panel-heading"  data-toggle="collapse" data-parent="#" href="#e" style="background-color: transparent; padding-left: 0px;">
                        <a class="panel-title">
                            <span id="hide"> Team Management</span>
                            <i class="fa fa-angle-down pull-right angle"></i>
                        </a>
                    </div>
                <li id="e" class="{{ Route::current()->getPath() == 'sales/team-management' | Route::current()->getPath() == 'sales/team-management/incomplete' | Route::current()->getPath() == 'sales/team-management/duplicate' | Route::current()->getPath() == 'sales/logs' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                     <ul id="menu" class="inner menu-padding">
                        <li{{ Route::current()->getPath() == 'sales/team-management' ? ' class=active' : '' }}><a href="{{ url('sales/team-management') }}">All</a></li>
                        <li{{ Route::current()->getPath() == 'sales/team-management/incomplete' ? ' class=active' : '' }}><a href="{{ url('sales/team-management/incomplete') }}">Incomplete</a></li>
                        <li{{ Route::current()->getPath() == 'sales/team-management/duplicate' ? ' class=active' : '' }}><a href="{{ url('sales/team-management/duplicate') }}">Duplicate</a></li>
                    </ul>
                </li>
                </div> -->
                <li{{ Route::current()->getPath() == 'sales/logs' ? ' class=active' : '' }}><a href="{{ url('sales/logs') }}"><span id="hide">Sales User Log</span></a></li>
            </ul>
        </li>
        </div>
            </ul>
        </li>
    </div>
   @endif
  

   
   @if($permission_payroll->view == 1)
    <div class="panel panel-default">
        <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionTwo">
            <a class="panel-title accordion-toggle">
                <i class="fa fa-credit-card"></i><span id="hide"> Payroll Management</span>
                <i class="{{ Route::current()->getPath() == 'payroll/upload-data-feeds' | Route::current()->getPath() == 'payroll/generate-payroll' | Route::current()->getPath() == 'payroll/admin-pre-payroll-entries' | Route::current()->getPath() == 'payroll/payroll-computation' | Route::current()->getPath() == 'payroll/payroll-summary' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
            </a>
        </li>
        <li id="accordionTwo" class="{{ Route::current()->getPath() == 'payroll/upload-data-feeds' | Route::current()->getPath() == 'payroll/generate-payroll' | Route::current()->getPath() == 'payroll/admin-pre-payroll-entries' | Route::current()->getPath() == 'payroll/payroll-computation' | Route::current()->getPath() == 'payroll/payroll-summary' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
            <ul id="menu1" class="inner menu-pad">
                @if($permission_rank == null || $permission_rank->rank!="Advisor")
                @if(Auth::user()->usertype_id != 8)
                <li{{ Route::current()->getPath() == 'payroll/upload-data-feeds' ? ' class=active' : '' }}><a href="{{ url('payroll/upload-data-feeds') }}"><span id="hide">Upload Data Feeds</span></a></li>
                <li{{ Route::current()->getPath() == 'payroll/generate-payroll' ? ' class=active' : '' }}><a href="{{ url('payroll/generate-payroll') }}"><span id="hide">Generate Payroll</span></a></li>
                @endif
                @if($permission_rank == null || $permission_rank->tier != "2")
                <li{{ Route::current()->getPath() == 'payroll/admin-pre-payroll-entries' ? ' class=active' : '' }}><a href="{{ url('payroll/admin-pre-payroll-entries') }}"><span id="hide">Incentives or Deductions</span></a></li>
                <li{{ Route::current()->getPath() == 'payroll/payroll-computation' ? ' class=active' : '' }}><a href="{{ url('payroll/payroll-computation') }}"><span id="hide">Payroll Computation</span></a></li>
                @endif
                @endif
                <li{{ Route::current()->getPath() == 'payroll/payroll-summary' ? ' class=active' : '' }}><a href="{{ url('payroll/payroll-summary') }}"><span id="hide">Payroll Summary</span></a></li>
             
<!--                 <li{{ Route::current()->getPath() == 'payroll/payroll-summary-supervisor' ? ' class=active' : '' }}><a href="{{ url('payroll/payroll-summary-supervisor') }}"><span id="hide">Payroll Summary</span></a></li>    -->
            </ul>
        </li>
    </div>
    @endif
   
    @if($permission_policy->view == 1)
    <div class="panel panel-default">
        <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionThree">
            <a class="panel-title accordion-toggle">
                <i class="fa fa-book"></i><span id="hide"> Policy Management</span>
                <i class="{{ Route::current()->getPath() == 'policy' | Route::current()->getPath() == 'policy/riders' | Route::current()->getPath() == 'policy/introducers' | Route::current()->getPath() == 'policy/policy-history' | Route::current()->getPath() == 'policy/orphan-policies' | Route::current()->getPath() == 'policy/duplicates' | Route::current()->getPath() == 'policy/production/case-submission' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
            </a>
        </li>
        <li id="accordionThree" class="{{ Route::current()->getPath() == 'policy' | Route::current()->getPath() == 'policy/riders' | Route::current()->getPath() == 'policy/introducers' | Route::current()->getPath() == 'policy/policy-history' | Route::current()->getPath() == 'policy/orphan-policies' | Route::current()->getPath() == 'policy/duplicates' | Route::current()->getPath() == 'policy/production/case-submission' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? 'panel-collapse collapse in' : 'panel-collapse collapse' }}">
            <ul id="menu1" class="inner menu-pad">
              
                <li{{ Route::current()->getPath() == 'policy' ? ' class=active' : '' }}><a href="{{ url('policy') }}"><span id="hide">Policies</span></a></li>
                <li{{ Route::current()->getPath() == 'policy/riders' ? ' class=active' : '' }}><a href="{{ url('policy/riders') }}"><span id="hide">Riders</span></a></li>
                <li{{ Route::current()->getPath() == 'policy/introducers' ? ' class=active' : '' }}><a href="{{ url('policy/introducers') }}"><span id="hide">Introducers</span></a></li>
            
                @if(Auth::user()->usertype_id == 1) 
                <li{{ Route::current()->getPath() == 'policy/orphan-policies' ? ' class=active' : '' }}><a href="{{ url('policy/orphan-policies') }}"><span id="hide">Orphan Policy Service</span></a></li>
                <li{{ Route::current()->getPath() == 'policy/duplicates' ? ' class=active' : '' }}><a href="{{ url('policy/duplicates') }}"><span id="hide">Duplicates</span></a></li>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading"  data-toggle="collapse" data-parent="#accordion1"href="#c" style="background-color: transparent; padding-left: 0px;">
                        <a class="panel-title">
                            <span id="hide"> Production</span>
                            <i class="{{ Route::current()->getPath() == 'policy/production/case-submission' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                        </a>
                    </div>
                <li id="c" class="{{ Route::current()->getPath() == 'policy/production/case-submission' | Route::current()->getPath() == 'policy/production/case-submission-supervisor' | Route::current()->getPath() == 'policy/production/case-inception' | Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                    <ul id="menu" class="inner menu-padd">
                      <li{{ Route::current()->getPath() == 'policy/production/case-submission' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-submission') }}"><span id="hide">Case Submission</span></a></li>
                      <!-- <li{{ Route::current()->getPath() == 'policy/production/case-submission-supervisor' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-submission-supervisor') }}"><span id="hide">Case Submission (Supervisor)</span></a></li> -->
                      <li{{ Route::current()->getPath() == 'policy/production/case-inception' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-inception') }}"><span id="hide">Case Inception</span></a></li>
                      <!-- <li{{ Route::current()->getPath() == 'policy/production/case-inception-supervisor' ? ' class=active' : '' }}><a href="{{ url('policy/production/case-inception-supervisor') }}"><span id="hide">Case Inception (Supervisor)</span></a></li>
                    </ul> -->
                </li>
                </div>
                
                @if($permission_rank == null || $permission_rank->rank!="Advisor")
                @if($permission_rank == null || $permission_rank->tier != "2")
                <li{{ Route::current()->getPath() == 'policy/policy-history' ? ' class=active' : '' }}><a href="{{ url('policy/policy-history') }}"><span id="hide">Policy Log</span></a></li>
                @endif
                @endif
            </ul>
        </li>
    </div>
    @endif
    @if($permission_provider->view == 1)
        <div class="panel panel-default">
            <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionFour">
                <a class="panel-title accordion-toggle">
                      <i class="fa fa-clipboard"></i><span id="hide"> Provider & Products Mgt</span>
                      <i class="{{ Route::current()->getPath() == 'provider' | Route::current()->getPath() == 'product' | Route::current()->getPath() == 'product/logs' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                </a>
            </li>
                <li id="accordionFour" class="{{ Route::current()->getPath() == 'provider' | Route::current()->getPath() == 'product' | Route::current()->getPath() == 'product/logs' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                    <ul id="menu1" class="inner menu-pad">
                        @if($permission_rank == null || $permission_rank->rank!="Advisor" )
                        @if($permission_rank == null || $permission_rank->tier != "2")
                        <li{{ Route::current()->getPath() == 'provider' ? ' class=active' : '' }}><a href="{{ url('provider') }}"><span id="hide">Provider List</span></a></li>
                        @endif
                        @endif
                        <li{{ Route::current()->getPath() == 'product' ? ' class=active' : '' }}><a href="{{ url('product') }}"><span id="hide">Product List</span></a></li>
                        @if($permission_rank == null || $permission_rank->rank!="Advisor" )
                        @if($permission_rank == null || $permission_rank->tier != "2")
                        <li{{ Route::current()->getPath() == 'product/logs' ? ' class=active' : '' }}><a href="{{ url('product/logs') }}"><span id="hide">Product Log</span></a></li>
                        @endif
                        @endif
                    </ul>
                </li>
        </div>
    @endif
         <div class="panel panel-default">
            @if($permission_reports->view == 1)
            <li class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#accordionFive">
                <a class="panel-title accordion-toggle">
                    <i class="fa fa-credit-card"></i><span id="hide"> Summary and Reports</span>
                    <i class="{{ Route::current()->getPath() == 'summary' | Route::current()->getPath() == 'reports' | Route::current()->getPath() == 'reports/cpd' | Route::current()->getPath() == 'reports/export' ? ' indicator fa fa-angle-down pull-right angle' : 'indicator fa fa-angle-left pull-right angle' }}"></i>
                </a>
            </li>
                <li id="accordionFive" class="{{ Route::current()->getPath() == 'summary' | Route::current()->getPath() == 'reports' | Route::current()->getPath() == 'reports/cpd' | Route::current()->getPath() == 'reports/export' ? ' panel-collapse collapse in' : 'panel-collapse collapse' }}">
                  <ul id="menu1" class="inner menu-pad">
                    <li{{ Route::current()->getPath() == 'summary' ? ' class=active' : '' }}><a href="{{ url('summary') }}"><span id="hide">Consolidated Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports' ? ' class=active' : '' }}><a href="{{ url('reports') }}"><span id="hide">Consolidated Report - Summary</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports/export' ? ' class=active' : '' }}><a href="{{ url('reports/export') }}"><span id="hide">Ongoing Fees Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports/export' ? ' class=active' : '' }}><a href="{{ url('reports/export') }}"><span id="hide">Initial Fees Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports/export' ? ' class=active' : '' }}><a href="{{ url('reports/export') }}"><span id="hide">Production Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports/export' ? ' class=active' : '' }}><a href="{{ url('reports/export') }}"><span id="hide">Persistency Report</span></a></li>
                    <li{{ Route::current()->getPath() == 'reports/cpd' ? ' class=active' : '' }}><a href="{{ url('reports/cpd') }}"><span id="hide">CPD Report</span></a></li>   
                  </ul>
                </li>
            @endif
                @if(Auth::user()->usertype_id == 1)   
                <li{{ Route::current()->getPath() == 'settings' ? ' class=active' : '' }}><a href="{{ url('settings') }}"><i class="fa fa-cog"></i><span id="hide"> Settings</span></a></li>   
                @endif
            </div>
          </div>
        </div>
      </ul>
    </div>
    <!-- #sidebar-wrapper -->
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #ffffff">
        <div class="container-fluid">
            <div class="pull-left">
            <a ></a>
            <a href="#menu-toggle" id="menu-toggle" type="button" class="btn btn-default"><i class="fa fa-ellipsis-v" style="color: #79828f;"></i></a>
            </div>
            <div style="text-align: right; padding-left: 20px">
                <button id="bell"type="button" class="btn btn-default"><i class="fa fa-bell" style="color: #79828f;"></i></button>
                <button id="profile" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><span id="name-auth">{{ Auth::user()->name }}</span> <img src="{{ url('uploads', Auth::user()->photo) }}" style="color: #79828f;" id="photo-auth" class="img-circle" alt="Smiley face" height="30" width="30">
                <i class="fa fa-angle-down" style="color: #79828f;"></i></button>
                <ul id="userdrop" class="dropdown-menu pull-right" role="menu" aria-labelledby="menu1">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Profile <i class="fa fa-user pull-right"></i></a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="settings-btn">Settings <i class="fa fa-cog pull-right"></i></a></li>
                  <li role="presentation" class="divider"></li>
                  @if(Auth::user()->usertype_id == 1)
                  <li role="presentation"><a role="menuitem" tabindex="-1" class="db-wipe">Wipe <i class="fa fa-table pull-right"></i></a></li>
                  @endif
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('logout') }}">Logout <i class="fa fa-sign-out pull-right"></i></a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

        <div id="content">
        
        @yield('content')
        
        @if(Auth::check())
        <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default borderzero" data-url="" data-id="" id="dialog-confirm">Yes</button>
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modal-profile" tabindex="-1" role="dialog" aria-labelledBy="profile-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-md">
            <div class="modal-content borderzero">
                <div id="load-profile" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'profile/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_profile', 'files' => 'true')) !!}
              <div class="modal-header tbheader">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="profile-title"><i class="fa fa-cog"></i> Settings</h4>
              </div>
              <div class="modal-body">
               <div id="profile-notice"></div>
                <div class="form-group">
                    <div id="image" class="container-fluid col-sm-12 col-xs-7">
                        <div style="margin-bottom:2px;" id="image_pane_for_system_user" class="fb_photo"></div>
                        <input type="hidden" id="image_fb_link_for_system_user" name="image_link">
                        <div id="default_image_pane"><img id="settings-photo" class="img-responsive img-circle" src="..." height="100" width="100"></div>
                        <input type="file" class="file file_photo" name="photo" id="row-photo" data-show-upload="false" placeholder="Upload a photo..." file-accept="jpg gif jpeg png bmp">
                        <br>
                <div class="text-center col-lg-12 ">
                <span id="name-auth">{{ Auth::user()->system_id }}</span>
                </div>
                </div>
                </div>
                <hr>
                <div class="col-sm-12 nopad">
                <div class="col-sm-6">
                <div class="form-group">
                <label class="col-xs-12" for="settings-code">User Code</label>
                <div class="col-xs-12">
                <input type="text" name="code" class="form-control borderzero" id="settings-code" maxlength="15" placeholder="Enter your user code...">
                </div>
                </div>
                <div class="form-group">
                <label class="col-xs-12" for="settings-name">Last Name</label>
                <div class="col-xs-12">
                <input type="text" name="name" class="form-control borderzero" id="settings-name" maxlength="15" placeholder="Enter your last name">
                </div>
                </div>
                <div class="form-group">
                <label class="col-xs-12" for="settings-email">Email</label>
                <div class="col-xs-12">
                <input type="email" name="email" class="form-control borderzero" id="settings-email" maxlength="15" placeholder="Enter your email">
                </div>
                </div>
                <div class="form-group">
                <label for="row-social_media" class="col-xs-12">Social Media</label>
                <div class="col-xs-12">
                <fb:login-button size="medium" scope='public_profile,email' onlogin='checkLoginState();' data-show-faces="false" data-auto-logout-link="true" class="pull-left"  data-toggle="tooltip" title="This will automatically fetch your facebook profile picture">
                 Link with Facebook
                </fb:login-button>
                </div>
                </div>
                </div>
                <div class="col-sm-6">
                <div class="form-group">
                <label class="col-xs-12" for="settings-password">Password</label>
                <div class="col-xs-12">
                <input type="password" name="password" class="form-control borderzero" id="settings-password" maxlength="15" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group">
                <label class="col-xs-12" for="settings-password_confirmation">Confirm Password</label>
                <div class="col-xs-12">
                <input type="password" name="password_confirmation" class="form-control borderzero" id="settings-password_confirmation" maxlength="15" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                <label class="col-xs-12" for="settings-mobile">Contact Number</label>
                <div class="col-xs-12">
                <input type="text" name="mobile" class="form-control borderzero" id="settings-mobile" maxlength="15" placeholder="Enter your contact number">
                </div>
                </div>
            </div>
        </div>
              </div>
              <div class="clearfix"></div>
              <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="settings-id" value="">

                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit borderzero"><i class="fa fa-save"></i> Save changes</button>
                <button type="button" class="btn btn-primary borderzero" id="settings-photo-change">Change</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
        @endif

        </div>

    </div><!-- #wrapper -->

    <footer></footer>
    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}

    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    
    <!-- Sidebar Scripts -->
    {!! HTML::script('/js/sidebar.js') !!}

    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/raphael-min.js') !!}
    {!! HTML::script('/js/plugins/morris.js') !!}
    {!! HTML::script('/js/plugins/graph.js') !!}
    {!! HTML::script('/js/plugins/jquery.dataTables.js') !!}
    {!! HTML::script('/js/plugins/fileinput.min.js') !!}
    {!! HTML::script('/js/plugins/select2.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}

    {!! HTML::script('/js/main.js') !!}

    @yield('scripts')

    <script type="text/javascript">
    

    $(document).ready(function() 
    {
    $('.panel-title').click(function(e) 
    { 
     $('.inner a span').addClass('animated fadeInLeft');
    });
    });

    function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('fa-angle-down fa-angle-left');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);

    function refresh_profile() {

        $_token = '{{ csrf_token() }}';

        $.post("{{ url('profile/refresh') }}", { _token: $_token }, function(response) {

            $("#name-auth").html("");
            $('#name-auth').html(response.rows.name);
            $('#photo-auth').attr("src", "{{ url('uploads') }}/" + response.rows.photo + "?" + new Date().getTime());

        }, 'json');
    }

    $(document).ready(function(){

        $('[data-toggle="tooltip"]').tooltip();

        $("#userdrop").on("click", ".settings-btn", function() {
            $_token = "{{ csrf_token() }}";
            
            $('#modal-save_profile').find('.file-input').addClass('hide');

              // reset all form fields
            $("#modal-profile").find('form').trigger("reset");
                $.post("{{ url('profile/info') }}", { _token: $_token }, function(response) {
                    
                    if(!response.error) {
                        // set form title
                        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
                        $(".profile-title").html("<i class='fa fa-cog'></i> <strong>" + response.name + '</strong>');

                        // output form data
                        $.each(response, function(index, value) {
                              var field = $("#settings-" + index);

                            // field exists, therefore populate
                            if(field.length > 0) {
                                field.val(value);
                            }
                        });

                        // Hide photo input.
                        $('#modal-save_profile').find('.file-input').addClass('hide');
                        // Remove selected photo input.
                        $('#modal-save_profile').find('.fileinput-remove').trigger('click');
                        // reset back to change
                        $('#settings-photo-change').text('Change Photo');
                        // show current photo
                        $('#settings-photo').removeClass('hide');

                        // show form
                        $('#settings-photo').attr('src', '{{ url('uploads') }}/' + response.photo + '?' + new Date().getTime());
                        $("#modal-profile").modal('show');
                    } else {
                        status(false, response.error, 'alert-danger');
                    }

                }, 'json');
        });


        $("#userdrop").on("click", ".db-wipe", function() {
            $_token = "{{ csrf_token() }}";
            console.log('wipe');
            $.post("{{ url('wipe') }}", { _token: $_token }, function(response) {

            });
        });
    
        // photo handler
        $('#settings-photo-change').click(function() {
            if ($(this).text() == 'Change Photo') {
                $(this).text('Cancel');
                $('#settings-photo').addClass('hide');
                $('#modal-save_profile').find('.file-input').removeClass('hide');
                $('#photo').click();
            } else {

                $(this).text('Change Photo');
                $('#settings-photo').removeClass('hide');

                // Hide photo input.
                $('#modal-save_profile').find('.file-input').addClass('hide');
                // Remove selected photo input.
                $('#modal-save_profile').find('.fileinput-remove').click();
            }
        });

        // profile closing
        $('#modal-profile').on('hidden.bs.modal', function () {
            $('#profile-notice').html("");
        });
    });

    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
    });

    </script>

</body>

</html>
