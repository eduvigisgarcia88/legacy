<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>LEGACY GROUP</title>

    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/notification.css') !!}
    {!! HTML::style('/css/plugins/toastr.min.css') !!}
    {!! HTML::style('/css/plugins/fileinput.min.css') !!}
    {!! HTML::style('/css/plugins/jquery.dataTables.min.css') !!}
    {!! HTML::style('/css/plugins/select2-bootstrap.css') !!}
    {!! HTML::style('/css/plugins/select2.css') !!}
    {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Main Stylesheet -->
    {!! HTML::style('/css/content.css') !!}
    {!! HTML::style('/css/navbar.css') !!}
    {!! HTML::style('/css/sidebar.css') !!}
    {!! HTML::style('/css/accordion.css') !!}
    {!! HTML::style('/css/animate.css') !!}
    {!! HTML::style('/css/pdf.css') !!}

</head>

<body>

<div class="container-fluid" id="content">
    <button type="button" id="btn-print" onclick="print()" class="btn btn-danger btn-md hidden-print" style="position:absolute;top:10px;left:10px;"><i class="fa fa-print fa-fw"></i>Print</button>
    <center>
        <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
    </center>
    @yield('content')

</div>



    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}

    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    
    <!-- Sidebar Scripts -->
    {!! HTML::script('/js/sidebar.js') !!}

    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/raphael-min.js') !!}
    {!! HTML::script('/js/plugins/morris.js') !!}
    {!! HTML::script('/js/plugins/graph.js') !!}
    {!! HTML::script('/js/plugins/modernizr.js') !!}
    {!! HTML::script('/js/plugins/jquery.dataTables.js') !!}
    {!! HTML::script('/js/plugins/fileinput.min.js') !!}
    {!! HTML::script('/js/plugins/select2.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}

    {!! HTML::script('/js/main.js') !!}
    {!! HTML::script('/js/plugins/jquery.print.js') !!}

    <script type="text/javascript">
    function print() {
        $("body").print();
    }

    </script>

    @yield('scripts')

</body>

</html>