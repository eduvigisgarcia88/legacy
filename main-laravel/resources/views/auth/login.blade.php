<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Latest compiled and minified CSS -->
    <title>{{ $title }}</title>
    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    {!! HTML::style('/css/login-style.css') !!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="bg-blue">
    <div class="container v-centered">
      <div class="row">
        <div class="centered">
            <div id="form-login" class="panel panel-default">
                {!! Form::open(array('url' => 'login', 'role' => 'form')) !!}
                @if(Session::has('notice'))
                <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('notice.msg') }}
                </div>
                @endif
                @if(Session::has('expired'))
                <div class="alert alert-dismissable alert-{{ Session::get('expired.type') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('expired.msg') }}
                </div>
                @endif
                <div class="panel-logo">
                    <img src="{{ url('images/logo') . '/legacy-main-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
                </div>
                <div class="panel-heading">
                    <div class="form-group">
                        <label class="control-label">Login</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="E-mail Address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
<!--                     <div class="form-group">
                      <div class="g-recaptcha" data-sitekey="6Lf9_wsUAAAAAEsZnDZuPD7uKgwaxQeqqZxIBDKq"></div>
                    </div> -->
                    <div class="form-group auth-group">
                        <div class="checkbox col-xs-8">
                            <label><input type="checkbox" name="remember"> <span id="rememberme">&nbsp;Remember me</span></label>
                        </div>
                        <div class="col-xs-4 padding-none"><input class="hide" name="honeypot" value=""><button type="submit" class="btn btn-warning btn-responsive btn-login">Login</button></div>
                    </div>
                </div>

                <div class="panel-forgot hide">
                    <div class="panel-body padding-none text-justify">
                        <input type="hidden" value="PVCCn3FqAlNCV4ulpJu205eapLO2QqnAXm234aAOBZY=">
                    </div>
                </div>
                {!! Form::close() !!}
                    <div class="panel-forgot" style="margin-bottom: -20px;">
                        <div class="panel-body padding-none text-justify panel-heading">
                            <label>Forgot password?</label> <a style="text-decoration:none;" class="forgot-btn" href="#">Click here</a>
                            <br>
                            <sup class="sup-errors" id="forgot_errors"></sup>
                            <br>
                            <div id="forgot-password" class="form-group" style="display:none; padding-bottom: 8px;">
                              <input style="margin-bottom: 14px;" id="user_email" name="email" type="text" class="form-control" placeholder="* E-mail Address">
                              <input style="margin-bottom: 14px;" id="user_nric" name="nric" type="text" class="form-control" placeholder="* NRIC">
                              <input style="margin-bottom: 5px;" id="user_code" name="code" type="text" class="form-control" placeholder="* Representative Code">
                            <div class="pull-right">
                                <button id="forgot_submit" type="button" class="btn btn-success btn-responsive btn-reset">Reset</button>
                            </div>
{{--                               <span class="input-group-btn">
                                <button id="forgot_submit" style="height:34px; color:white;" class="btn btn-default" type="button"><i class="glyphicon glyphicon-send"></i></button>
                              </span> --}}
                            </div>
                            
                        </div>
                    </div>
                
            </div>
      </div>
  </div>
    </div>

    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}
    <script src='https://www.google.com/recaptcha/api.js'></script>
    
    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
    
    <script>
    var _token = '{{csrf_token()}}';
    var forgot_submit = $('#forgot_submit');
    var email_box = $('#user_email');
    var nric_box = $('#user_nric');
    var code_box = $('#user_code');
    var error_box = $('#forgot_errors');

        $('.forgot-btn').click(function(){
            var btn_text = $(this).text();
            $('#forgot-password').toggle(300, function(){
                var current_text = (btn_text == 'Click here' ? 'Close' : 'Click here');
                $('.forgot-btn').text(current_text);
            });
        });

        forgot_submit.click(function(){
            var email = email_box.val();
            var nric = nric_box.val();
            var code = code_box.val();
            if(email)
            {
                $.ajax({
                    type: 'POST',
                    data: { _token: _token, email: email, nric: nric, code: code },
                    url: '{{url('forgot/passwd')}}',
                    beforeSend: function(){
                        email_box.prop('disabled', true);
                        nric_box.prop('disabled', true);
                        code_box.prop('disabled', true);
                        forgot_submit.prop('disabled', true);
                        error_box.text('Requesting...');
                    },
                    success: function(response){
                        email_box.prop('disabled', false);
                        forgot_submit.prop('disabled', false);
                        nric_box.prop('disabled', false);
                        code_box.prop('disabled', false);
                        if(response.code == 0)
                        {
                            error_box.text(response.errors);
                        }
                        else
                        {
                            var email = email_box.val('');
                            var nric = nric_box.val('');
                            var code = code_box.val('');
                            error_box.text(response.message);
                        }
                    }
                });
            }
            else
            {
                error_box.text('Fill the required fields *');
            }
        });
    </script>
  </body>
</html>