@extends('layouts.master')

@section('scripts')
  <script>
    //Global Vars
    $_token = '{{ csrf_token() }}';

    function refresh() {

        $('.loading-pane').removeClass('hide');
        $("#row-page").val(1);
        $("#row-order").val('');
        $("#row-sort").val('');

        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

              body += '<tr>' + 
                '<td>' + row.system_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    }

    function search() {

        $('.loading-pane').removeClass('hide');
        $("#row-page").val(1);

        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

              body += '<tr>' + 
                '<td>' + row.system_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    }

    $(function () {
      $('.export-start').datetimepicker({format: 'L'});
      $('.export-end').datetimepicker({
        format: 'L',
          useCurrent: false //Important! See issue .1075
      });
      $(".export-start").on("dp.change", function (e) {
          $('.export-end').data("DateTimePicker").minDate(e.date);
      });
      $(".export-end").on("dp.change", function (e) {
          $('.export-start').data("DateTimePicker").maxDate(e.date);
      });
    });

    $('#export-check').change(function() {

      if($(this).is(":checked")) {
        $(".dates").removeAttr('readonly');
      } else {
        $(".dates").attr('readonly', 'readonly');
      }
    });

    $('.btn-export').click(function() {

      $(".dates").attr('readonly', 'readonly');
      $("#modal-export").find('form').trigger("reset");
      $("#modal-export").modal('show');
    });

    $('.btn-archive').click(function() {

    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + "<tr><td>Sample</td></tr>";
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, 'Test file.xls');
        }
    } else {
        $('#test').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
        $('#test').attr('download', 'Test file.xls');
    }

    });

  // submit form
//     $("#modal-export").find("#modal-save_export").submit(function(e) {
//       var form = $("#modal-export").find("form");
//       var loading = $("#load-export");

//       // $.post(form.attr('action'), { _token: $_token }, function(response) {

//       //   if (response.unauthorized) {
//       //     window.location.href = response.unauthorized;
//       //   }
//       //   // console.log(response);
//       //   var blob=new Blob([response.file]);
//       //   var link=document.createElement('a');
//       //   link.href=window.URL.createObjectURL(blob);
//       //   link.download=response.filename;
//       //   link.click();

//       // });

//       // stop form from submitting
//       e.preventDefault();

//       loading.removeClass('hide');
//       // console.log(form.serialize());
//       push form data
//       $.ajax({
//       type: "post",
//       url: '{{ url("users/logs/export-check") }}',
//       //data: form.serialize(),
//       data: new FormData($("#modal-save_export")[0]),
//       dataType: "json", 
//       async: true,
//       processData: false,
//       contentType: false,
//       success: function(response) {
//         if (response.unauthorized) {
//           window.location.href = '{{ url("login")}}';
//         }

//         if(response.error) {
//           $(".sup-errors").html("");
//           $(".form-control").removeClass("required");
//           $.each(response.error, function(index, value) {
//             var errors = value.split("|");
//             $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
//             $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
//           });
//         } else {

//           // console.log('doodz');

//           // var blob = new Blob([response]);
//           // var link = document.createElement('a');
//           // link.href = window.URL.createObjectURL(blob);
//           // link.download = "myfile.xls";
//           // link.click();
// var TableToExcel = (function () {
//     var uri = 'data:application/vnd.ms-excel;base64,'
//     , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table cellspacing="0" rules="rows" border="1" style="color:Black;background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;width:100%;border-collapse:collapse;font-size:9pt;text-align:center;">{table}</table></body></html>'
//     , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
//     , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
//     return function (table, name) {
//         if (!table.nodeType) table = document.getElementById(table)
//         var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
//         if (navigator.msSaveBlob) {
//             var blob = new Blob([format(template, ctx)], { type: 'application/vnd.ms-excel', endings: 'native' });
//             navigator.msSaveBlob(blob, 'export.xls')
//         } else {
//             window.location.href = uri + base64(format(template, ctx))
//         }
//     }
// })()
//           // console.log(response);
//           // // $("#load-success").removeClass('hide');
//           //   $.ajax({
//           //       type: 'POST',
//           //       url: '{{ url("users/logs/export") }}',
//           //       data: { _token: $_token, usertype_id: response.usertype_id, start: response.start, end: response.end }
//           //   }).done(function () {
//           //       // window.location.href = '{{ url("admin/news")}}';
//           //   });
//         }
//         loading.addClass('hide');
//       }

//       });
//     });


    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

              body += '<tr>' + 
                '<td>' + row.system_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {
            console.log(response.rows.data);
              body += '<tr>' + 
                '<td>' + row.system_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
      }
    });

    $("#test").click(function() {
      console.log('asdasd');
    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';

    tab_text = tab_text + '<x:Name>Test Sheet</x:Name>';

    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';

    tab_text = tab_text + "<table border='1px'>";
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.ms-excel';
    
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, 'Test file.xls');
        }
    } else {
        $('#test').attr('href', data_type + ', ' + encodeURIComponent(tab_text));
        $('#test').attr('download', 'Test file.xls');
    }

  });


  </script>
@stop

@section('content')

  <!-- Page Content -->
  <div id="page-content-wrapper" class="response">
    <!-- Title -->
    <div class="container-fluid main-title-container container-space">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">USER MANAGEMENT</h3>
        <h5 class="bread-crumb">USER MANAGEMENT > SYSTEM USER LOGS </h5>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
      <button type="button" id="test"><i class="fa fa-archive"></i> Archive</button>
        <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-archive"><i class="fa fa-archive"></i> Archive</button>
        <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-export"><i class="fa fa-file-excel-o"></i> Export</button>
      </div>
    </div>

  <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
    <div class="block-content tbblock col-xs-12">  
      <div class="loading-pane hide">
        <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
      </div>
      <div class="table-responsive">
        <table class="table table-striped table-pagination" id="my-table">
          <thead class="tbheader">
            <tr>
              <th class="th-sort" nowrap data-sort="system_id"><i></i> AGENT ID</th>
              <th class="th-sort" nowrap data-sort="code"><i></i> AGENT CODE</th>
              <th class="th-sort" nowrap data-sort="name"><i></i> AGENT NAME</th>
              <th class="th-sort" nowrap data-sort="activity"><i></i> ACTIVITY</th>
              <th class="th-sort" nowrap data-sort="created_at"><i></i> DATE</th>
              <th class="th-sort" nowrap data-sort="edit_name"><i></i> EDITED BY</th>
            </tr>
          </thead>
          <tbody id="rows">
            @foreach ($rows as $row)
            <tr>
              <td>{{ $row->system_id }}</td>
              <td>{{ $row->code }}</td>
              <td>{{ $row->name }}</td>
              <td>{{ $row->activity }}</td>
              <td>{{ $row->created_at }}</td>
              <td>{{ $row->edit_name }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div> 
  </div>

  <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
    <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
    <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
      <option value="10">10</option>
      <option value="25">25</option>
      <option value="50">50</option>
      <option value="50">100</option>
    </select>
    </div>
    <div class="text-center">
      <input type="hidden" id="row-page" value="1">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-order" value="">
    </div>
  </div>

  <!-- /#page-content-wrapper -->
@include('user-management.system-users.log.form')

@stop