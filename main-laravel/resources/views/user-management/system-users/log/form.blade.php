<!-- Modal -->
<div id="myModalview" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-eye"></i> Profile</h4>
</div>
<div class="modal-body">
<form role="form">
	<div class="form-group">
		<label for="email">PHOTO</label><br>
		<img src="images/avatar.png" style="color: #79828f;" class="img-circle" alt="Smiley face" height="100" width="100"> 
	</div>
	<div class="form-group">
		<label for="email">AGENT ID</label>
		<input type="email" class="form-control borderzero" id="email" placeholder="steve12345" disabled>
	</div>
	<div class="form-group">
		<label for="email">AGENT CODE</label>
		<input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs" disabled>
	</div>
	<div class="form-group">
		<label for="pwd">USER NAME</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="Sales Agent" disabled>
	</div>
	<div class="form-group">
		<label for="pwd">AGENT NAME</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="12345678" disabled>
	</div>
	<div class="form-group">
		<label for="pwd">DESIGNATION</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="steve@email.com" disabled>
	</div>
	<div class="form-group">
		<label for="pwd">EMAIL</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="12/06/1985" disabled>
	</div>
	<div class="form-group">
		<label for="pwd">CONTACT NO.</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="+65 123 4567" disabled>
	</div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Edit</button>
</div>
</div>
</div>
</div>
<!-- Modal -->
<div id="myModalexport" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<form role="form" method="post" action="{{ url('users/logs/export-all') }}">
<div class="modal-content borderzero">
<div class="modal-header tbheader">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">EXPORT </h4>
</div>
<div class="modal-body">
	{!! csrf_field() !!}
	<div class="form-group">
		<label for="pwd" class="col-sm-3 col-xs-5">DESTINATION</label>
		<div class="col-sm-9 col-xs-7 nopad">
			<select id="select" class="form-control borderzero" id="sel1" name="destination">
				<option value="1">CEO</option>
				<option value="2">IT Users</option>
				<option value="3">Admin Accounts</option>
				<option value="5">Accounting</option>
			</select>
			<input type="submit" class="btn btn-default borderzero pull-right" name="export-destination" value="Export">
		</div>
	</div>
	<label for="pwd" class="col-sm-12 col-xs-12">DATE</label>
	<div class="form-group">
	<label for="" class="col-sm-3 col-xs-5">FROM:</label>
	<div class="col-sm-9 col-xs-7 input-group">
	<input class="form-control borderzero" type="date" id="register4-firstname" name="from" placeholder="..">
	<span class="input-group-addon borderzero"><i class="fa fa-calendar"></i></span>
	</div>
	</div>
	<div class="form-group">
	<label for="" class="col-sm-3 col-xs-5">TO:</label>
	<div class="col-sm-9 col-xs-7 input-group">
	<input class="form-control borderzero" type="date" id="register4-firstname" name="to" placeholder="..">
	<span class="input-group-addon borderzero"><i class="fa fa-calendar"></i></span>
	</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<input type="submit" class="btn btn-default borderzero tbheader" name= "export-all" value="Export All">
</div>
</div>
</div>
</div>
</form>
<!-- Modal -->
<div id="myModalarchive" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content borderzero">
<div class="modal-header tbheader">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-archive"></i> ARCHIVE</h4>
</div>
<div class="modal-body">
	<p>Are you want to Archive System User Log?</p>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero tbheader" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldelete" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-minus"></i> Remove</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to remove the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldeactivate" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-adjust"></i> Disable</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to disable the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaledit" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-pencil"></i> Edit</h4>
</div>
<div class="modal-body">
<form role="form">
	<div class="form-inline">
		<label for="email">PHOTO</label><br>
		<img src="images/avatar.png" style="margin-bottom: 5px; color: #79828f;" class="img-circle" alt="Smiley face" height="100" width="100">
		<div class="form-group">
		<input type="email" class="form-control borderzero" id="email" placeholder="file:///C:/Users/Desktop/Users/picture.jpg"><br>
		<button type="button" class="btn btn-default borderzero" data-dismiss="">UPLOAD</button>
	</div>
	</div>  
	<div class="form-group">
		<label for="email">AGENT ID</label>
		<input type="email" class="form-control borderzero" id="email" placeholder="steve12345">
	</div>
	<div class="form-group">
		<label for="email">AGENT CODE</label>
		<input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs">
	</div>
	<div class="form-group">
		<label for="pwd">USER NAME</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="Sales Agent">
	</div>
	<div class="form-group">
		<label for="pwd">AGENT NAME</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="12345678">
	</div>
	<div class="form-group">
		<label for="pwd">DESTINATION</label>
			<select id="select" class="form-control borderzero" id="sel1">
			<option>CEO</option>
			<option>IT Users</option>
			<option>Admin Accounts</option>
			<option>Accounting</option>
			</select>
	</div>
	<div class="form-group">
		<label for="pwd">EMAIL</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="12/06/1985">
	</div>
	<div class="form-group">
		<label for="pwd">CONTACT NO.</label>
		<input type="password" class="form-control borderzero" id="pwd" placeholder="+65 123 4567">
	</div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
