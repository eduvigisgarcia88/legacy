@extends('layouts.master')

@section('scripts')
  <script>
    //Global Vars
    $_token = '{{ csrf_token() }}';

    function refresh() {

        $('.loading-pane').removeClass('hide');
        $("#row-page").val(1);
        $("#row-order").val('');
        $("#row-sort").val('');

        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

              body += '<tr>' + 
                '<td>' + row.system_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    }

    function search() {

        $('.loading-pane').removeClass('hide');
        $("#row-page").val(1);

        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

              body += '<tr>' + 
                '<td>' + row.system_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    }

    $(document).ready(function() {
    // //datatable
    //     myTable = $('#my-table').dataTable({
    //           "aoColumnDefs": [{ 
    //             'bSortable': false, 
    //             'aTargets': [4],
    //           }],
    //           "order": [[ 4, "desc" ]],
    //           "dom": '<"top"f>rt<"bottom"lip><"clear">'
    //     });

    //     // Show Table
    //     $('.content-table').removeClass('hide');

    //     //search/filter
    //     /*
    //     $('#search-status').change(function(){
    //         myTable.fnFilter($(this).val(), 0);
    //     });

    //     $('#search-rank').change(function(){
    //         myTable.fnFilter($(this).val(), 3);
    //     });*/

    //     $('#search').keyup(function(){
    //         myTable.fnFilter($(this).val());
    //     });

    //     $('#page-content-wrapper').on('click', '.btn-export', function() {

    //     });
        
    //     $('.btn-export-all').on('click', function() {
    //       console.log('btn-export-all');
    //       $.post("{{ url('users/logs/export-all') }}", { _token: $_token }, function(response) {
    //         console.log('response: ' + response);
    //         var blob=new Blob([response]);
    //         var link=document.createElement('a');
    //         link.href=window.URL.createObjectURL(blob);
    //         link.download="myfile.csv";
    //         link.click();

    //       });

/*
          $.ajax({
          data: { _token: $_token },
           dataType:'blob',
           type:'post',
           url:'{{ url('users/logs/export-all') }}'
          }).done(function(blob){
            console.log('tes');
            var blob=new Blob([blob]);
            var link=document.createElement('a');
            link.href=window.URL.createObjectURL(blob);
            link.download="myfile.csv";
            link.click();
          });
*/
        });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {
            body += '<tr>' + 
                      '<td>' + row.system_id + '</td>' +
                      '<td>' + row.code + '</td>' +
                      '<td>' + row.name + '</td>' +
                      '<td>' + row.activity + '</td>' +
                      '<td>' + row.created_at + '</td>' +
                      '<td>' + row.edit_name + '</td>' +
                    '</tr>';
          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ url('users/logs/refresh') }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {
            console.log(response.rows.data);
              body += '<tr>' + 
                '<td>' + row.system_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
      }
    });

  </script>
@stop

@section('content')

  <!-- Page Content -->
  <div id="page-content-wrapper" class="response">
    <!-- Title -->
    <div class="container-fluid main-title-container container-space">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
            <h3 class="main-title">SYSTEM USER LOG</h3>
            <h5 class="bread-crumb">USER MANAGEMENT</h5>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
              <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalarchive"><i class="fa fa-archive"></i> Archive</button>     
              <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-export"><i class="fa fa-plus"></i> Export</button>
        </div>
        </div>
      <!-- <div class="container-fluid default-container container-space">
        <div class="col-lg-12">
              <p><strong>FILTER OPTIONS</strong></p>
        </div>
          <form class="form-horizontal" >     
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH OPTION</strong></p></label>
              <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <select class="form-control borderzero" id="sel1" >
                      <option>All Providers</option>
                      <option>A</option>
                      <option>B</option>
                      <option>C</option>
                      <option>D</option>
                    </select>
                  </div>
              </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                  <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                  <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <div class="input-group">
                    <input type="text" class="form-control borderzero" placeholder="Search for..." id="search">
                    <span class="input-group-btn">
                      <button class="btn btn-default borderzero" type="button"><i class="fa fa-eraser"></i> <span class="hidden-xs">CLEAR</span></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div> -->
    <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
      <div class="block-content tbblock col-xs-12">  
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
        </div>
      <div class="table-responsive">
      <table class="table table-striped table-pagination" id="my-table">
        <thead class="tbheader">
          <tr>
            <th class="th-sort" nowrap data-sort="system_id"><i></i> AGENT ID</th>
            <th class="th-sort" nowrap data-sort="code"><i></i> AGENT CODE</th>
            <th class="th-sort" nowrap data-sort="name"><i></i> AGENT NAME</th>
            <th class="th-sort" nowrap data-sort="activity"><i></i> ACTIVITY</th>
            <th class="th-sort" nowrap data-sort="created_at"><i></i> DATE - TIME</th>
            <th class="th-sort" nowrap data-sort="edit_name"><i></i> EDITED BY</th>
          </tr>
        </thead>
        <tbody id="rows">
          @foreach ($rows as $row)
          <tr>
            <td>{{ $row->system_id }}</td>
            <td>{{ $row->code }}</td>
            <td>{{ $row->name }}</td>
            <td>{{ $row->activity }}</td>
            <td>{{ $row->created_at }}</td>
            <td>{{ $row->edit_name }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div> 
</div>
<div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
      <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="50">100</option>
      </select>
      </div>
      <div class="text-center">
        <input type="hidden" id="row-page" value="1">
        <input type="hidden" id="row-sort" value="">
        <input type="hidden" id="row-order" value="">
      </div>
</div>
    <!-- /#page-content-wrapper -->
@include('user-management.system-users.log.form')

@stop