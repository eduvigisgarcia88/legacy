<!-- modal add user -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'users/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
               <!--  <div class="form-group default-photo-pane">
                      <label for="row-default_photo" class="col-sm-3 control-label">Photo</label>
                      <div class="col-sm-9">
                      <img class="image-responsive img-circle" src="{{ url('uploads')."/".$row_settings->default_photo }}" height="100" width="100" style="margin: 5px;">
                      <button type="button" id="btn-custom-photo" class="borderzero btn btn-primary btn-sm"><i class="fa fa-camera"></i> CHANGE</button>
                      </div>
                </div> -->
                <!-- <div class="form-group facebook-photo-pane"> 
                </div> -->
                <div class="form-group system-photo-div">
                     <label for="row-photo" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                     <div class="col-sm-9 col-xs-7 system-photo-pane">
                     <div class="change">
                     <input name="photo" id="row-photo_upload" type="file" class="file borderzero file_photo" data-show-upload="false" placeholder="Upload a photo...">
                     </div>
                 <!--  <div class="row col-lg-12 content-table"> -->
                       
               <!--  </div> -->
                </div>
                </div>
                <div class="form-group">
                    <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">User Code</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="code" class="form-control borderzero" id="row-code" maxlength="30" placeholder="Enter your user code...">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="name" class="form-control borderzero" id="row-name" maxlength="30" placeholder="Enter your last name">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="email" class="form-control borderzero" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control borderzero" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control borderzero" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                      <label for="row-usertype_id" class="col-sm-3 col-xs-5 control-label font-color">Designation</label>
                <div class="col-sm-9 col-xs-7">
                <select name="usertype_id" class="form-control hide-view borderzero" id="row-usertype_id">
                    <option value="" class="hide">Select:</option>
                    @foreach ($types as $row)
                      @if($row->id == $type_id)
                        <option value="{{ $row->id }}" selected="true">{{ $row->type_name }}</option>
                      @else
                        <option value="{{ $row->id }}">{{ $row->type_name }}</option>
                      @endif
                    @endforeach
                  </select>
                <input type="text" class="form-control hide disabled show-view borderzero" id="row-usertype_id-static" value="" readonly="">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="mobile" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                </div>
                </div>
                <div class="form-group social_media_pane">
                    <label for="row-social-media" class="col-sm-3 col-xs-5 control-label font-color">Social Media</label>
                <div class="col-sm-9 col-xs-7">
                   <fb:login-button size="medium" scope='public_profile,email' onlogin='checkLoginState();' data-show-faces="false" data-auto-logout-link="true" class="pull-left">
                   Link with Facebook
                   </fb:login-button>
                </div>
                </div>
              </div>
              <div class="modal-footer borderzero">
                <input type="hidden" id="image_fb_link_edit_photo">
                <input type="hidden" name="id" id="row-id">
                <input type="hidden" id="image_fb_link" name="image_link">
                <button type="button" class="btn btn-danger borderzero" data-dismiss="modal"><i class="fa fa-times"></i>Close</button>
                <button type="submit" class="btn btn-success hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

  <!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

