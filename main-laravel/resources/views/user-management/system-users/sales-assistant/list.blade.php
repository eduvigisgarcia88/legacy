@extends('layouts.master')

@section('scripts')
  <script>

    var selectedID = 0;
    $_token = "{{ csrf_token() }}";
    $loading_expand = false;
    $expand_id = 0;
    // refresh collapse table
    // function refresh() {

    //   $('#row').html('<tr><td colspan="4"><i class="fa fa-spinner fa-spin fa-3x centered"></i><td></tr>');
      

    //   $.post("{{ url('sales-assistant/get-agents') }}", { id: selectedID, _token: $_token }, function(response) {

    //     var table = '';

    //     $.each(response.rows, function(index, row) {
    //       table += '<tr>' +
    //                   '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
    //                   '<td>' + row.code + '</td>' +
    //                   '<td>' + row.username + '</td>' +
    //                   '<td>' + row.designation + '</td>' +
    //                   '<td class="rightalign">' +
    //                     ' <a href="{{ url('sales') }}/' + row.id + '/edit" type="button" class="btn-color btn btn-xs btn-default"><i class="fa fa-eye"></i></a>' +
    //                     ' <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-minus"></i></button>' +
    //                     ' <input id="checkbox" type="checkbox" value="' + row.assigned_sales_id + '">' +
    //                   '</td>' +
    //                 '</tr>';
    //     });

    //     $('#row').html(table);
    //     //$tr.parent().append(table);

    //   }, 'json');

    // }

    // refresh the list
    function refresh() {
      var loading = $(".loading-pane");
      var table = $("#rows");
      $loading_expand = false;
      $expand_id = 0;

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              '<td><a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;<img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td class="hidden-xs hide">' + row.system_id + '</td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td>' + row.email + '</td>' +
              '<td>' + row.mobile + '</td>' +
              '<td class="rightalign">';
                  if ({{ $permission->view }} == 1) {
                    body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                  }
                  if (row.id != {{ Auth::user()->id }}) {
                    if ({{ $permission->edit }} == 1) {
                      body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                    }
                      body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                      '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                  }
              body += '</td>'+
            '</tr>';
        });

        table.html(body);
        $('#row-pages').html(response.pages);

      loading.addClass("hide");
      }, 'json');
    } 

    // refresh the list
    function search() {
      var loading = $(".loading-pane");
      var table = $("#rows");
      $loading_expand = false;
      $expand_id = 0;

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              '<td><a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;<img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td class="hidden-xs hide">' + row.system_id + '</td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td>' + row.email + '</td>' +
              '<td>' + row.mobile + '</td>' +
              '<td class="rightalign">';
                  if ({{ $permission->view }} == 1) {
                    body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                  }
                  if (row.id != {{ Auth::user()->id }}) {
                    if ({{ $permission->edit }} == 1) {
                      body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                    }
                      body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                      '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                  }
              body += '</td>'+
            '</tr>';
        });

        table.html(body);
        $('#row-pages').html(response.pages);

      loading.addClass("hide");
      }, 'json');
    } 

    $(document).ready(function() {
// refresh();
      //add user
      $(".btn-add").click(function() {

      $('#row-search').keypress(function(event){
          var keycode = (event.keyCode ? event.keyCode : event.which);
          if(keycode == '13'){
              search();
          }
      });
        // reset all form fields
        $("#modal-form").find('form').trigger("reset");
        $("#row-id").val("");

        $("#modal-form").find('.hide-view').removeClass('hide');
        $("#modal-form").find('.show-view').addClass('hide');
        $("#modal-form").find('.preview').addClass('hide');
        $("#modal-form").find('input').removeAttr('readonly', 'readonly');

        $("#photo_row").addClass('hide'); 
        $(".change").removeClass('hide');
        $("#row-password").attr("placeholder","Enter your password");
        $("#row-password_confirmation").attr("placeholder","Repeat your password");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
        $("#modal-form").modal('show');

        // User Id Enable
        $('#modal-form').find("input[name='system_id']").removeAttr('disabled', 'disabled');

         // reset photo
        //$('#photo_row').addClass('hide');
        $('#edit-photo-change').addClass('hide');
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();

      });

      // disable user
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Account Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('users/disable') }}", id);
      });

      // enable user
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Account Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('users/enable') }}", id);
      });

      // show modal for multiple disable
      $("#page-content-wrapper").on("click", ".btn-disable-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Disable");
        $(".modal-multiple-body").html("Are you sure you want to disable the selected user/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected user/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      // show modal for multiple remove agents
      $("#page-content-wrapper").on("click", ".btn-remove-agents", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("2");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Agents Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected agent/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });


      $('#page-content-wrapper').on('click', '.btn-expand', function() {
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');

        if ($loading_expand){
          return;
        }

        $tr = $(this).parent().parent();
        $id = $tr.data('id');

        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $expand_id;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($id == $expand_id) {
          $expand_id = 0;
          return
        }

        $expand_id = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading_expand = true;

        $.post("{{ url('sales-assistant/get-agents') }}", { id: $id, _token: $_token }, function(response) {
          // console.log(response);

          var table = '<tr><td colspan="7" style="padding:0">' +
                        '<div class="row-' + $expand_id + '" style="display: none;" id="agents-list">' +
                          '<div class="table-responsive content-table block-content tbblock" id="my-table2">' +
                            '<span><strong>ASSIGNED TO FOLLOWING SALES AGENT<strong></span>' +
                            '<table class="table">' +
                              '<thead class="tbheader">' +
                                '<tr>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> PHOTO</th>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> AGENT CODE</th>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> USER NAME</th>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> DESIGNATION</th>' +
                                  '<th class="rightalign tbheader">TOOLS</th>' +
                                '</tr>' +
                              '</thead>' +
                              '<tbody id="row">';

          $.each(response.rows, function(index, row) {
            table += '<tr>' +
                        '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                        '<td>' + row.code + '</td>' +
                        '<td>' + row.username + '</td>' +
                        '<td>' + row.designation + '</td>' +
                        '<td class="rightalign">' +
                          ' <a href="{{ url('sales') }}/' + row.id + '/edit" type="button" class="btn-color btn btn-xs btn-default"><i class="fa fa-eye"></i></a>' +
                          ' <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-minus"></i></button>' +
                          ' <input id="checkbox" type="checkbox" value="' + row.assigned_sales_id + '">' +
                        '</td>' +
                      '</tr>';
          });

          table += '</tbody>' +
                  '</table>' +
                  '<hr>' +
                    '<button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-agents" data-toggle="modal"><i class="fa fa-minus"></i> Remove</button>' +
                    '<button type="button" data-id="' + $id + '"class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-assign"><i class="fa fa-plus"></i> Assign</button>' +
                '</div>' +
              '</div>' +
              '</td></tr>';

          $tr.after(table);
          $('.row-' + $expand_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        }, 'json');


      });
      //  // Table row click
      // $('#my-table').on('click', '.btn-expand', function() {
      //   $tr = $(this).parent();
      //   $id = $tr.data('id');

      //   // Deselect
      //   if ($id == selectedID) {

      //     // Deselect row
      //     $tr.removeClass('info');
        
      //     // Uncollapse and Remove Previous Row
      //     previousRow = '.row-' + selectedID;
      //     $(previousRow).slideUp(function() {
      //       $(previousRow).parent().parent().remove();
      //       // Remove Selected id
      //       selectedID = 0;

      //     });
      
      //     return;
      //   }

      //   // Uncollapse and Remove Previous Row
      //   previousRow = '.row-' + selectedID;
      //   $(previousRow).slideUp(function() {
      //     $(previousRow).parent().parent().remove();
      //   });


      //   $('#my-table').find('tbody tr').removeClass('info');
      //   $tr.addClass('info');

      //   $tr.after('<tr class="loading-row"><td colspan="6"><center><i class="fa fa-spinner fa-spin"></i></center></td></tr>');
      //   $_token = "{{ csrf_token() }}";
     
      //   selectedID = $id;

      //   $('#myModalassign').find(".btn-save").data('id', $id);

      //   $.post("{{ url('sales-assistant/get-agents') }}", { id: $id, _token: $_token }, function(response) {
      //     console.log(response);

      //     var table = '<tr><td colspan="4" style="padding:0">' +
      //                   '<div class="row-' + selectedID + '" style="display: none;" id="agents-list">' +
      //                     '<div class="table-responsive content-table block-content tbblock" id="my-table2">' +
      //                       '<span><strong>ASSIGNED TO FOLLOWING SALES AGENT<strong></span>' +
      //                       '<table class="table">' +
      //                         '<thead class="tbheader">' +
      //                           '<tr>' +
      //                             '<th class="tbheader"><i class="fa fa-sort"></i> PHOTO</th>' +
      //                             '<th class="tbheader"><i class="fa fa-sort"></i> AGENT CODE</th>' +
      //                             '<th class="tbheader"><i class="fa fa-sort"></i> USER NAME</th>' +
      //                             '<th class="tbheader"><i class="fa fa-sort"></i> DESIGNATION</th>' +
      //                             '<th class="rightalign tbheader">TOOLS</th>' +
      //                           '</tr>' +
      //                         '</thead>' +
      //                         '<tbody id="row">';

      //     $.each(response.rows, function(index, row) {
      //       table += '<tr>' +
      //                   '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
      //                   '<td>' + row.code + '</td>' +
      //                   '<td>' + row.username + '</td>' +
      //                   '<td>' + row.designation + '</td>' +
      //                   '<td class="rightalign">' +
      //                     ' <a href="{{ url('sales') }}/' + row.id + '/edit" type="button" class="btn-color btn btn-xs btn-default"><i class="fa fa-eye"></i></a>' +
      //                     ' <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-minus"></i></button>' +
      //                     ' <input id="checkbox" type="checkbox" value="' + row.assigned_sales_id + '">' +
      //                   '</td>' +
      //                 '</tr>';
      //     });

      //     table += '</tbody>' +
      //             '</table>' +
      //             '<hr>' +
      //               '<button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-agents" data-toggle="modal"><i class="fa fa-minus"></i> remove</button>' +
      //               '<button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-assign" data-toggle="modal" data-target="#myModalassign"><i class="fa fa-plus"></i> assign</button>' +
      //           '</div>' +
      //         '</div>' +
      //         '</td></tr>';

      //     $tr.after(table);
      //     $('.row-' + selectedID).slideDown();
      //     $('.loading-row').fadeOut(function() {
      //       $('.loading-row').remove();
      //     });
      //   }, 'json');

      // });

      $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("my-table input:checked").each(function(){
            ids.push($(this).val());
        });
      
        if (ids.length == 0 && !$($("#row-multiple-hidden").val() == "2")) {
          status("Error", "Select users first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('users/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('users/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
          } else {

            if ($("#row-multiple-hidden").val() == "2") {
              var ids = [];

              $('#row input:checked').each(function(){
                ids.push($(this).val());
              });

              $.post("{{ url('sales-assistant/remove-agents') }}", { ids: ids, _token: $_token }, function(response) {
                toastr[response.type](response.body);
                refresh();
              }, 'json'); 

            } else {
              status("Error", "Error! Refresh the page.", 'alert-danger');
            }

          }
        }
      });

    $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');

        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        $('#edit-photo-change').addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          // reset all form fields
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-form").find('form').trigger("reset");
          $("#row-id").val("");


              $.post("{{ url('users/view') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title

                  
                      $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View System User</strong>');
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);

                          if(index == "user_type") {
                            $("#row-usertype_id-static").val(value.type_name);
                          }

                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                          }
                      });

                      $("#photo_row").removeClass('hide');
                      $("#photo_row").attr('src', '{{ url('uploads') }}/' + response.photo); // photo
                      $("#modal-form").find('.hide-view').addClass('hide');
                      $("#modal-form").find('.preview').removeClass('hide');
                      $("#modal-form").find('.change').addClass('hide');
                      $("#modal-form").find('.show-view').removeClass('hide');
                      $("#modal-form").find('input').attr('readonly', 'readonly');
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
      });

      //edit user
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        // reset all form fields
        $("#modal-form").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // reset photo
        $('#edit-photo-change').removeClass('hide');
        $('#edit-photo-change').text('Change');
        $('#photo_row').removeClass('hide');
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

              $.post("{{ url('users/view') }}", { id: id, _token: $_token }, function(response) {
                      console.log(response);
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit System User</strong>');

                      // output form data
                      console.log(response);
                      $("#row-password").attr("placeholder","Leave blank for unchanged");
                      $("#row-password_confirmation").attr("placeholder","Leave blank for unchanged");
                      $.each(response, function(index, value) {
                           
                          var field = $("#row-" + index);
                         
                          if(index == "user_type") {
                            $("#row-usertype_id-static").val(value.type_name);
                          }
                          if(index == "photo"){
                            $("#row_photo").attr('src', '{{ url('uploads') }}/' + response.photo); // photo
                          }
                         
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);

                          }
                      });
            
                     
                      $("#modal-form").find('.hide-view').removeClass('hide');
                      $("#modal-form").find('.show-view').addClass('hide');
                      $("#modal-form").find('.preview').removeClass('hide');
                      $("#modal-form").find('.change').addClass('hide');
                      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

      // // Designation Select Change
      // $("#select-designation").on("change", function() {
      //   // console.log('changed');
      //   // console.log($('#select-designation').val());
      //   $_token = "{{ csrf_token() }}";

      //   $options = "";

      //   $.post("{{ url('sales-assistant/get-available-agents') }}", {designation_id: $('#select-designation').val(), _token: $_token }, function(response) {
      //     // console.log('row change: ');
      //     // console.log(response);

      //     $.each(response.rows, function(index, row) {
      //       $options += '<option value="' + row.id + '"">' + row.name + '</option>';
      //     });

      //     //  console.log($options);
      //     $('#agents').select2('val', '');
      //     $('#agents').html($options);

      //   });

      // });

      // assign button
      $("#page-content-wrapper").on("click", ".btn-assign", function() {
        $_token = "{{ csrf_token() }}";

        // Reset Select Options
        $("#agents").val('').change();

        $options = "";

        $.post("{{ url('sales-assistant/get-available-agents') }}", { _token: $_token }, function(response) {
          // console.log(response);

          $.each(response.rows, function(index, row) {
            $options += '<option value="' + row.id + '"">' + row.name + '</option>';
          });

          $('#agents').html($options);
        });
        $("#assign-id").val($(this).data('id'));
        $("#modal-assign").modal('show');

      });

      // assign save button
      $('#myModalassign').find(".btn-save").click(function() {

        $_token = "{{ csrf_token() }}";

        $row = $(this).data('id');
        // console.log($row);
        $ids = $('#agents').select2('val');

        $.post("{{ url('sales-assistant/save-agents') }}", { row: $row, ids: $ids, _token: $_token }, function(response) {
          toastr[response.type](response.body);
          refresh();
        });

      });

      // photo handler
      $('#edit-photo-change').click(function() {
          if ($(this).text() == 'Change') {
              $(this).text('Cancel');
              $('#photo_row').addClass('hide');
              $(".change").removeClass('hide');
              
          } else {

              $(this).text('Change');
              $('#photo_row').removeClass('hide');

              // Remove selected photo input.
              $('.change').find('.fileinput-remove').click();
              $(".change").addClass('hide');
          }
      });

     $('.select2').select2();
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      $loading_expand = false;
      $expand_id = 0;
      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        table.html("");
        
        var body = "";
        // console.log(response);

          $.each(response.rows.data, function(index, row) {

              body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
                '<td class="hide">' + row.status + '</td>' +
                '<td><a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;<img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                '<td class="hidden-xs hide">' + row.system_id + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td class="hidden-xs">' + row.type_name + '</td>' +
                '<td>' + row.email + '</td>' +
                '<td>' + row.mobile + '</td>' +
                '<td class="rightalign">';
                    if ({{ $permission->view }} == 1) {
                      body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                    }
                    if (row.id != {{ Auth::user()->id }}) {
                      if ({{ $permission->edit }} == 1) {
                        body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                      }
                        body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                        '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                    }
                body += '</td>'+
              '</tr>';

          });
        
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

        }, 'json');
      
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      $loading_expand = false;
      $expand_id = 0;
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";
          // console.log(response);

          $.each(response.rows.data, function(index, row) {

              body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
                '<td class="hide">' + row.status + '</td>' +
                '<td><a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;<img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                '<td class="hidden-xs hide">' + row.system_id + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td class="hidden-xs">' + row.type_name + '</td>' +
                '<td>' + row.email + '</td>' +
                '<td>' + row.mobile + '</td>' +
                '<td class="rightalign">';
                    if ({{ $permission->view }} == 1) {
                      body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                    }
                    if (row.id != {{ Auth::user()->id }}) {
                      if ({{ $permission->edit }} == 1) {
                        body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                      }
                        body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                        '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                    }
                body += '</td>'+
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
      }
    });

  </script>
@stop

@section('content')
        <!-- Page Content -->
        <div id="page-content-wrapper" class="response">
        <!-- Title -->
        <div class="container-fluid main-title-container container-space">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
                <h3 class="main-title">USER MANAGEMENT</h3>
                <h5 class="bread-crumb">{{ $bread_crumbs }}</h5>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
            @if ($permission->delete == 1)
              <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
            @endif
              <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-disable-select"><i class="fa fa-adjust"></i> Disable</button>     
              <button type="button" class="btn btn-sm btn-warning borderzero btn-tool btn-add pull-right" id="user-add"><i class="fa fa-plus"></i> Add</button>
            </div>
            </div>
       <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>    
  <div class=" form-horizontal" >     
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
              <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>STATUS</strong></p></label>
                <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                      <!--<select class="form-control borderzero" id="sel1">
                        @foreach ($types as $row)
                          <option value="{{ $row->status }}">{{ $row->status }}</option>
                        @endforeach
                      </select>-->
                      <select class="form-control borderzero" onchange="search()" id="row-filter_status">
                        <option value="">All</option>
                        <option value="1">Active</option>
                        <option value="0">Disabled</option>
                      </select>
                      <!--<input type="text" value="" id="kwd_search2">-->
                    </div>
                </div>
            </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                        <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                        </span>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>  
              </div>
    <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
      <div class="block-content tbblock col-xs-12">
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-pagination" id="my-table">
            <thead class="tbheader">
              <tr>
                <th>PHOTO</th>
                <th class="th-sort" data-sort="name"><i></i> NAME</th>
                <th class="th-sort" data-sort="code"><i></i> USER CODE</th>
                <th class="th-sort" data-sort="type_name"><i></i> ROLE</th>
                <th class="th-sort" data-sort="email"><i></i> EMAIL</th>
                <th class="th-sort" data-sort="mobile"><i></i> MOBILE NO</th>
                <th class="rightalign">TOOLS</th>
              </tr>
              </thead>
              <tbody id="rows">
              @foreach ($rows as $row)
                <tr data-id="{{ $row->id }}" {{ ($row->status == 1) ? ($row->id == Auth::user()->id) ? 'class=info' : '' : 'class=tr-disabled' }}>
                  <td><a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a> <img src="{{ asset('/uploads/'.$row->photo) }}" height="30" width="30" class="img-circle"/></td>
                  <td>{{ $row->name }}</td>
                  <td>{{ $row->code }}</td>
                  <td>{{ $row->type_name }}</td>
                  <td>{{ $row->email }}</td>
                  <td>{{ $row->mobile }}</td>
                  <td class="rightalign">
                    @if ($permission->view == 1)
                      <button type="button" class="btn btn-xs btn-table btn-view borderzero" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>
                    @endif
                    @if ($permission->edit == 1)
                      <button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>
                    @endif
                    @if($row->id != Auth::user()->id)
                      <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }} borderzero" data-toggle="tooltip" title="{{ ($row->status == 1) ? 'Disable' : 'Enable' }}"><i class="fa fa-{{ ($row->status == 1) ? 'adjust' : 'check-circle' }}"></i></button>
                      <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                    @endif
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>      
        </div>
      </div>
      <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
        <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
        <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
        </div>
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>
    </div>

    <!-- /#page-content-wrapper -->
@include('user-management.system-users.sales-assistant.form')

@stop