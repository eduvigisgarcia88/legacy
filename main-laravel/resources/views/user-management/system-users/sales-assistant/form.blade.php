<!-- modal add user -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'users/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                  <label for="row-photo" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                  <div class="col-sm-9 col-xs-7">
                     <img id="row_photo" src="..." class="preview img-circle" height="100" width="100"/>
                  <div class="change">
                    <input name="photo" id="row-photo_upload" type="file" class="file borderzero file_photo" data-show-upload="false" placeholder="Upload a photo...">
                  </div>
                  <div class="row col-lg-12 content-table">
                  <button type="button" class="btn btn-primary borderzero" id="edit-photo-change">Change</button>
                </div>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">User Code</label>
                <div class="col-sm-9 col-xs-7 error-code ">
                  <input type="text" name="code" class="form-control borderzero" id="row-code" maxlength="30" placeholder="Enter your user code...">
                  <sup class="sup-errors"></sup>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">User Last Name</label>
                <div class="col-sm-9 col-xs-7 error-name">
                  <input type="text" name="name" class="form-control borderzero" id="row-name" maxlength="30" placeholder="Enter your last name">
                  <sup class=" sup-errors"></sup>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7 error-email">
                  <input type="text" name="email" class="form-control borderzero" id="row-email" maxlength="100" placeholder="Enter your email address...">
                  <sup class=" sup-errors"></sup>
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7 error-password ">
                  <input type="password" name="password" class="form-control borderzero" id="row-password" maxlength="30" placeholder="Enter your password...">
                  <sup class="sup-errors"></sup>
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control borderzero" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                      <label for="row-usertype_id" class="col-sm-3 col-xs-5 control-label font-color">Role</label>
                <div class="col-sm-9 col-xs-7 error-usertype_id ">
                <select name="usertype_id" class="form-control hide-view borderzero" id="row-usertype_id">
                    <option value="" class="hide">Select:</option>
                    @foreach ($types as $row)
                      @if($row->id == $type_id)
                        <option value="{{ $row->id }}" selected="true">{{ $row->type_name }}</option>
                      @else
                        <option value="{{ $row->id }}">{{ $row->type_name }}</option>
                      @endif
                    @endforeach
                  </select>
                  <sup class="sup-errors"></sup>
                <input type="text" class="form-control hide disabled show-view borderzero" id="row-usertype_id-static" value="" readonly="">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7 error-mobile ">
                  <input type="text" name="mobile" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                  <sup class="sup-errors"></sup>
                </div>
                </div>
              </div>
              <div class="modal-footer borderzero">
            <input type="hidden" id="row-photo">
            <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero btn-close" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

  <!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

<!-- modal add user -->
<div class="modal fade" id="modal-assign" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-assign" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'sales-assistant/save-agents', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_assign', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="assign-title"><i class="fa fa-plus"></i> Assign Sales Users</h4>
              </div>
              <div class="modal-body">
               <div id="assign-notice"></div>
              <div class="form-horizontal">
                <label for="agents">Agents</label>
                <select id="agents" class="form-control select2" name="ids[]" multiple="multiple">
                  
                </select>
              </div>
              </div>
              <div class="clearfix"></div>
              <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="assign-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>


<div id="myModalassign" class="modal fade" role="dialog">
  <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header modal-warning">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title"><i class="fa fa-plus"></i> Assign Sales Users</h4>
      </div>
      <div class="modal-body">
      <form role="form">
        <div class="form-group">
          <label for="select-designation">Designation</label>
          <select id="select-designation" class="form-control" style="width: 100%">
            @foreach ($designations as $row)
              <option value="{{ $row->id }}">{{ $row->designation }}</option>  
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="agents">Agents</label>
          <select id="agents" class="form-control select2" multiple="multiple">
            
          </select>
        </div>
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-default borderzero btn-save" data-dismiss="modal">Save Changes</button>
      </div>
    </div>
  </div>
</div>