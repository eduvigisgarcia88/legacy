@extends('layouts.master')

@section('scripts')
  <script>
    function statusChangeCallback(response) {

    if (response.status === 'connected') {

      $("#standard_photo").addClass("hide");
      $(".standard_photo_pane").addClass("hide");
      $("#standard_photo_for_user").addClass("hide");
      $(".facebook-photo-pane").removeClass("hide");
      FB.api('/me', function(response) {
        $("#image_settings").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $(".system-photo-pane").html("<img class='img-circle image-responsive' id='row-photo_row' name='fb_photo' height='100' width='100' style='margin: 5px;' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        $("#image_pane").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_pane_edit_photo").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_fb_link").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_for_system_user").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_edit_photo").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#system_image_pane").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#default_image_pane").addClass("hide");
        $("#image_pane_edit_photo").removeClass("hide");
        $("#image_pane").removeClass("hide");
        $("#row-photo").addClass("hide");
        $("#user_photo_pane").addClass("hide");
        $("#image_settings").removeClass("hide");
        $("#btn-cancel-photos").click();
      });

      $("#status").val(1);

    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      $("#standard_photo").removeClass("hide");
      $(".standard_photo_pane").removeClass("hide");

    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      $("#row-photo").removeClass("hide");
      $("#standard_photo").removeClass("hide");
      $(".standard_photo_pane").removeClass("hide");
      $("#image_pane").addClass("hide");
      $("#image_pane_edit_photo").addClass("hide");
      $("#default_image_pane").removeClass("hide");
      $("#image_pane_for_system_user").html("");
      $("#user_photo_pane").removeClass("hide");
      $(".facebook-photo-pane").addClass("hide");
      $("#system_image_pane").html('<img id="settings-photo" class="img-responsive img-circle" src="{{url("uploads")."/".Auth::user()->photo}}" height="100" width="100">');
      $("#image_settings").addClass("hide");
      $(".system-photo-pane").html("<img src='{{ url('uploads').'/'.$settings->default_photo }}' class='preview img-circle' height='100' width='100' id = 'row-photo_row' style='margin:5px;'/>");
      $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
      $("#status").val(0);

    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '395487573951234',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);

  });
  };
 
    // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  } (document, 'script', 'facebook-jssdk'));

  $_token = '{{ csrf_token() }}';

    $loading_expand = false;
    $expand_id = 0;
    // refresh the list
    function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {

        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        // clear
        table.html("");
        var body = "";

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              '<td>';

            if (row.usertype_id == 7) {
              body +=  '<a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;';
            }

            body += '<img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td class="hidden-xs hide">' + row.system_id + '</td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td>' + row.email + '</td>' +
              '<td>' + row.mobile + '</td>' +
              '<td class="rightalign">';
                  if ({{ $permission->view }} == 1) {
                    body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                  }
                  if ({{ $permission->edit }} == 1) {
                    body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                  }
                  if (row.id != {{ Auth::user()->id }}) {
                    body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                            '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                  }
              body += '</td>'+
            '</tr>';
        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);

        loading.addClass("hide");

      }, 'json');
    } 

    function search() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
      table.html("");
      
      var body = "";

        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              '<td>';

            if (row.usertype_id == 7) {
              body +=  '<a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;';
            }

            body += '<img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td class="hidden-xs hide">' + row.system_id + '</td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td>' + row.email + '</td>' +
              '<td>' + row.mobile + '</td>' +
              '<td class="rightalign">';
                  if ({{ $permission->view }} == 1) {
                    body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                  }
                  if ({{ $permission->edit }} == 1) {
                    body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                  }
                  if (row.id != {{ Auth::user()->id }}) {
                    body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                            '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                  }
              body += '</td>'+
            '</tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");

      }, 'json');
    
    }

    $(document).ready(function() {

      // show modal for multiple remove agents
      $("#page-content-wrapper").on("click", ".btn-remove-agents", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("2");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Agents Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected agent/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      // assign button
      $("#page-content-wrapper").on("click", ".btn-assign", function() {
        $_token = "{{ csrf_token() }}";

        // Reset Select Options
        $("#agents").val('').change();

        $options = "";

        $.post("{{ url('sales-assistant/get-available-agents') }}", { _token: $_token }, function(response) {
          // console.log(response);

          $.each(response.rows, function(index, row) {
            $options += '<option value="' + row.id + '"">' + row.name + '</option>';
          });

          $('#agents').html($options);
        });
        $("#assign-id").val($(this).data('id'));
        $("#modal-assign").modal('show');

      });

      // assign save button
      $('#myModalassign').find(".btn-save").click(function() {

        $_token = "{{ csrf_token() }}";

        $row = $(this).data('id');
        // console.log($row);
        $ids = $('#agents').select2('val');

        $.post("{{ url('sales-assistant/save-agents') }}", { row: $row, ids: $ids, _token: $_token }, function(response) {
          toastr[response.type](response.body);
          refresh();
        });

      });


      $('#page-content-wrapper').on('click', '.btn-expand', function() {

        console.log('clicked');
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');

        if ($loading_expand){
          return;
        }

        $tr = $(this).parent().parent();
        $id = $tr.data('id');

        $_token = '{{ csrf_token() }}';

        $selected = '.row-' + $expand_id;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($id == $expand_id) {
          $expand_id = 0;
          return
        }

        $expand_id = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-cog fa-spin"></i>');
        $loading_expand = true;

        $.post("{{ url('sales-assistant/get-agents') }}", { id: $id, _token: $_token }, function(response) {
          // console.log(response);

          var table = '<tr><td colspan="7" style="padding:0">' +
                        '<div class="row-' + $expand_id + '" style="display: none;" id="agents-list">' +
                          '<div class="table-responsive content-table block-content tbblock" id="my-table2">' +
                            '<span><strong>ASSIGNED TO FOLLOWING SALES AGENT<strong></span>' +
                            '<table class="table">' +
                              '<thead class="tbheader">' +
                                '<tr>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> PHOTO</th>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> AGENT CODE</th>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> USER NAME</th>' +
                                  '<th class="tbheader"><i class="fa fa-sort"></i> DESIGNATION</th>' +
                                  '<th class="rightalign tbheader">TOOLS</th>' +
                                '</tr>' +
                              '</thead>' +
                              '<tbody id="row">';

          $.each(response.rows, function(index, row) {
            table += '<tr>' +
                        '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                        '<td>' + row.code + '</td>' +
                        '<td>' + row.username + '</td>' +
                        '<td>' + row.designation + '</td>' +
                        '<td class="rightalign">' +
                          ' <a href="{{ url('sales') }}/' + row.id + '/edit" type="button" class="btn-color btn btn-xs btn-default"><i class="fa fa-eye"></i></a>' +
                          ' <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-minus"></i></button>' +
                          ' <input id="checkbox" type="checkbox" value="' + row.assigned_sales_id + '">' +
                        '</td>' +
                      '</tr>';
          });

          table += '</tbody>' +
                  '</table>' +
                  '<hr>' +
                    '<button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-agents" data-toggle="modal"><i class="fa fa-minus"></i> Remove</button>' +
                    '<button type="button" data-id="' + $id + '"class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-assign"><i class="fa fa-plus"></i> Assign</button>' +
                '</div>' +
              '</div>' +
              '</td></tr>';

          $tr.after(table);
          $('.row-' + $expand_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        }, 'json');


      });

      $(".uploader-pane").addClass("hide");

      $('#btn-custom-photo').on('click', function() {
        $(".default-photo-pane").addClass("hide");
        $(".custom-photo-pane").removeClass("hide");
      });
      $(".system-photo-pane").html("<img id='row-photo_row' src='..' class='preview img-circle' height='100' width='100' style='margin:5px;'/>");
      $(".btn-refresh").click(function() {
        refresh();
      });
       @foreach($settings as $row_settings)@endforeach
      //add user
      $(".btn-add").click(function() {
        var status = $("#status").val();

        if(status == 0){
         $(".system-photo-pane").html("<img src='{{ url('uploads')."/".$settings->default_photo }}' id='row-photo_row' class='preview img-circle' height='100' width='100' style='margin:5px;'/>");  
         $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
       }
        $('#edit-change-photo').removeClass('hide');
        $(".change").addClass("hide");
        $("#row-photo_row").removeClass("hide");
        $(".button-pane").removeClass("hide");
        $(".uploader-pane").addClass("hide");
        $(".system-photo-pane").removeClass("hide");
        // reset all form fields
        $("#modal-form").find('form').trigger("reset");
        $("#row-id").val("");
        // $("#edit-photo-change").removeClass("hide");
        $(".social_media_pane").removeClass('hide');
        $("#modal-form").find('.hide-view').removeClass('hide');
        // $("#modal-form").find('.show-view').addClass('hide');
        // $("#modal-form").find('.preview').addClass('hide');
        $("#modal-form").find('input').removeAttr('readonly', 'readonly');

        //$("#photo_row").addClass('hide'); 
        //$(".change").removeClass('hide');
        $("#row-usertype_id-static").addClass("hide");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
        $("#modal-form").modal('show');

        // User Id Enable
        $('#modal-form').find("input[name='system_id']").removeAttr('disabled', 'disabled');

         // reset photo
        //$('#photo_row').addClass('hide');
        $('#edit-photo-change').removeClass('hide');
        // Remove selected photo input.
        //$('.change').find('.fileinput-remove').click();
      });
      
      $('[required],[pattern]')
       .parent()
       .append('<span class="valid8"></span>');
        
      // disable user
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Account Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('users/disable') }}", id);
      });

      // enable user
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Account Activation', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('users/enable') }}", id);
      });

      // show modal for multiple disable
      $("#page-content-wrapper").on("click", ".btn-disable-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Disable");
        $(".modal-multiple-body").html("Are you sure you want to disable the selected user/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected user/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      $("#modal-multiple-button").click(function() {

        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });

        
        if (ids.length == 0) {
          status("Error", "Select users first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('users/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
              }
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('users/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
              }
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });

      $("#page-content-wrapper").on("click", ".btn-view", function() {
        // This is called with the results from from FB.getLoginStatus().
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $(".uploader-pane").addClass("hide");
        //$(".system-photo-pane").removeClass("hide");
        $_token = "{{ csrf_token() }}";
        $("#image_pane_for_system_user").addClass("hide");
        $("#user_photo_pane").removeClass("hide");
        $('#edit-change-photo').addClass('hide');
        var url = "{{ url('/') }}";
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

          // reset all form fields
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-form").find('form').trigger("reset");
          $("#row-id").val("");

          
              $.post("{{ url('users/view') }}", { id: id, _token: $_token }, function(response) {
                  if (response.unauthorized) {
                    window.location.href = '{{ url("login")}}';
                  }

                  if(!response.error) {
                  // set form title

                  
                      $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View System User</strong>');
                      //$(".pic").html('<img src="uploads/theheck.jpg">');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          
                          if(index == "user_type") {
                            $("#row-usertype_id-static").val(value.type_name);
                          }

                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                              
                          }
                      });
                     var status =  $("#status").val();
                      $(".system-photo-pane").removeClass("hide");  
                      
                      $(".system-photo-pane").html("<img id = 'row-photo_row' src='{{ url('uploads') }}/"+response.photo+ '?' + new Date().getTime()+"' class='preview img-circle' height='100' width='100' style='margin:5px;'/>");
                      $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero hide' id='edit-change-photo'>CHANGE</button>");
                      $(".custom-photo-pane").removeClass("hide");
                      $(".facebook-photo-pane").addClass("hide");
                      $(".default-photo-pane").addClass("hide");
                      $(".button-pane").addClass("hide");
                      $("#row-photo_row").removeClass('hide');
                      $(".social_media_pane").addClass('hide');
                      //$("#photo_row").attr('src', '{{ url('uploads') }}/' + response.photo); // photo
                      $("#modal-form").find('.hide-view').addClass('hide');
                      $("#modal-form").find('.preview').removeClass('hide');
                      //$("#modal-form").find('.change').addClass('hide');
                      $("#modal-form").find('.show-view').removeClass('hide');
                      $("#modal-form").find('input').attr('readonly', 'readonly');
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
      });

    $('#row-search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            search();
        }
    });
      //edit user
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        $(".social_media_pane").removeClass('hide');
        // reset photo

        $('#edit-change-photo').removeClass('hide');
        //$('#edit-photo-change').text("CHANGE");
        //$('#photo_row').removeClass('hide');

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('users/view') }}", { id: id, _token: $_token }, function(response) {
                  if (response.unauthorized) {
                    window.location.href = '{{ url("login")}}';
                  }

                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit System User</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          $("#row-password").attr("placeholder","Leave blank for unchanged");
                          $("#row-password_confirmation").attr("placeholder","Leave blank for unchanged");
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }

                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                              //console.log(value);
                              //console.log(value);
                          }
                      });
                      $(".system-photo-pane").removeClass("hide"); 
                      if($("#status").val() == 0){
                         $(".system-photo-pane").html("<img src='{{ url('uploads') }}/"+response.photo+ '?' + new Date().getTime()+"' class='preview img-circle' id = 'row-photo_row' height='100' width='100' style='margin:5px;'/>");
                         $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
                      }
                     
                      // $(".default-photo-pane").addClass("hide");
                      // $(".social_media_pane").removeClass('hide');
                      $("#modal-form").find('.hide-view').removeClass('hide');
                      $("#modal-form").find('.show-view').addClass('hide');
                      $("#modal-form").find('.preview').removeClass('hide');
                      //$("#modal-form").find('.change').addClass('hide');
                      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
                      //$(".custom-photo-pane").removeClass("hide");
                      //$(".facebook-photo-pane").addClass("hide");
                     

                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
 
      // photo handler
      //$('').click(function() {
      $('#modal-form').on('click', '#edit-change-photo', function() {
            //console.log("checked");
          if ($(this).text() == 'CHANGE') {
              $('#row-photo_row').removeClass('hide');
              $(".change").removeClass('hide');
              $("#image_pane_edit_photo").addClass("hide");
              $(".uploader-pane").removeClass("hide");
              $(".system-photo-pane").addClass("hide");

            
         } //else {

          //     $(this).text("CHANGE");
          //     $('#row-photo_row').removeClass('hide');
          //     $(".uploader-pane").addClass("hide");
          //     $(".system-photo-pane").removeClass("hide");

          //     // Remove selected photo input.
          //     $('.change').find('.fileinput-remove').click();
          //     $(".change").addClass('hide');
          // }
      });
       $('#btn-cancel-photos').on('click', function() {
              $(".change").addClass('hide');
              $(".uploader-pane").addClass("hide");
              $(".system-photo-pane").removeClass("hide");
    });

    });

     $('.select2').select2();
    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
          
        table.html("");
        
        var body = "";

        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              '<td>';

            if (row.usertype_id == 7) {
              body +=  '<a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;';
            }

            body += '<img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td class="hidden-xs hide">' + row.system_id + '</td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td>' + row.email + '</td>' +
              '<td>' + row.mobile + '</td>' +
              '<td class="rightalign">';
                  if ({{ $permission->view }} == 1) {
                    body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                  }
                  if ({{ $permission->edit }} == 1) {
                    body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                  }
                  if (row.id != {{ Auth::user()->id }}) {
                    body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                            '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                  }
              body += '</td>'+
            '</tr>';
        });
        
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

        }, 'json');
      
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          $.each(response.rows.data, function(index, row) {

              body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
                '<td class="hide">' + row.status + '</td>' +
                '<td>';

              if (row.usertype_id == 7) {
                body +=  '<a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>&nbsp;';
              }

              body += '<img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                '<td class="hidden-xs hide">' + row.system_id + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td class="hidden-xs">' + row.type_name + '</td>' +
                '<td>' + row.email + '</td>' +
                '<td>' + row.mobile + '</td>' +
                '<td class="rightalign">';
                    if ({{ $permission->view }} == 1) {
                      body += '<button type="button" class="btn btn-xs btn-table btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>';
                    }
                    if ({{ $permission->edit }} == 1) {
                      body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>';
                    }
                    if (row.id != {{ Auth::user()->id }}) {
                      body += '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                              '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id + '">';
                    }
                body += '</td>'+
              '</tr>';
          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
      }
    });
    // This is called with the results from from FB.getLoginStatus().
  
  </script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
    <h3 class="main-title">SYSTEM USERS</h3>
    <h5 class="bread-crumb">USER MANAGEMENT</h5>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
    <button type="button" class="btn btn-sm btn-success borderzero btn-tool btn-refresh pull-right" id="user-add"><i class="fa fa-refresh"></i> Refresh</button>
    @if ($permission->delete == 1)
    <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
    @endif
    <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-disable-select"><i class="fa fa-adjust"></i> Disable</button>     
    <button type="button" class="btn btn-sm btn-warning borderzero btn-tool btn-add pull-right" id="user-add"><i class="fa fa-plus"></i> Add</button>   
  </div>
</div>
<div class="container-fluid default-container container-space">
  <div class="col-lg-12">
    <p><strong>FILTER OPTIONS</strong></p>
  </div>    
  <div class=" form-horizontal" >     
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="form-group hide-view">
        <label for="" class="col-sm-2 col-xs-5 control-label-left font-color">STATUS</label>
        <div class="col-sm-10 col-xs-7">
          <select class="form-control borderzero" onchange="search()"  id="row-filter_status">
            <option value="">All</option>
            <option value="1">Active</option>
            <option value="0">Disabled</option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="form-group">
      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
          <div class="input-group">
            <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
            <span class="input-group-btn">
            <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
          </span>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>
<div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
  <div class="block-content tbblock col-xs-12">
    <div class="loading-pane hide">
      <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-pagination" id="my-table">
        <thead class="tbheader">
          <tr>
            <th>PHOTO</th>
            <th class="th-sort" data-sort="name"><i></i> NAME</th>
            <th class="th-sort" data-sort="code"><i></i> USER CODE</th>
            <th class="th-sort" data-sort="type_name"><i></i> ROLE</th>
            <th class="th-sort" data-sort="email"><i></i> EMAIL</th>
            <th class="th-sort" data-sort="mobile"><i></i> MOBILE NO</th>
            <th class="rightalign">TOOLS</th>
          </tr>
          </thead>
          <tbody id="rows">
          @foreach ($rows as $row)
            <tr data-id="{{ $row->id }}" {{ ($row->status == 1) ? ($row->id == Auth::user()->id) ? 'class=info' : '' : 'class=tr-disabled' }}>
              <td>
              @if ($row->usertype_id == 7)
              <a type="button" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a>
              @endif
              <img src="{{ asset('/uploads/'.$row->photo) }}" height="30" width="30" class="img-circle"/></td>
              <td>{{ $row->name }}</td>
              <td>{{ $row->code }}</td>
              <td>{{ $row->type_name }}</td>
              <td>{{ $row->email }}</td>
              <td>{{ $row->mobile }}</td>
              <td class="rightalign">
                @if ($permission->view == 1)
                <button type="button" class="btn btn-xs btn-table btn-view borderzero" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>
                @endif
                @if ($permission->edit == 1)
                <button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>
                @endif
                @if($row->id != Auth::user()->id)
                <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }} borderzero" data-toggle="tooltip" title="{{ ($row->status == 1) ? 'Disable' : 'Enable' }}"><i class="fa fa-{{ ($row->status == 1) ? 'adjust' : 'check-circle' }}"></i></button>
                <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                @endif
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
  <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
  <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
    <option value="10">10</option>
    <option value="25">25</option>
    <option value="50">50</option>
    <option value="50">100</option>
  </select>
  </div>
  <div class="text-center">
    <input type="hidden" id="row-page" value="1">
    <input type="hidden" id="row-sort" value="">
    <input type="hidden" id="row-order" value="">
  </div> 
  </div>
    <!-- /#page-content-wrapper -->

@include('user-management.system-users.form')

@stop