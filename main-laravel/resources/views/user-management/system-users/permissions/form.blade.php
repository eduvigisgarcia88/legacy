<div id="myModalundo" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-undo"></i> UNDO</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to undo the changes?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModalsave" class="modal fade" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-save"></i> SAVE</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to save the changes?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default borderzero modal-yes" data-dismiss="modal">Yes</button>
        <button type="button" class="btn btn-default borderzero modal-no" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<div id="myModaledit" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-pencil"></i> Edit</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-inline">
    <label for="email">PHOTO</label><br>
    <img src="images/avatar.png" style="margin-bottom: 5px; color: #79828f;" class="img-circle" alt="Smiley face" height="100" width="100">
    <div class="form-group">
    <input type="email" class="form-control borderzero" id="email" placeholder="file:///C:/Users/Desktop/Users/picture.jpg"><br>
    <button type="button" class="btn btn-default borderzero" data-dismiss="">UPLOAD</button>
  </div>
  </div>  
  <div class="form-group">
    <label for="email">AGENT ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="steve12345">
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs">
  </div>
  <div class="form-group">
    <label for="pwd">USER NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="Sales Agent">
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12345678">
  </div>
  <div class="form-group">
    <label for="pwd">DESIGNATION</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="steve@email.com">
  </div>
  <div class="form-group">
    <label for="pwd">EMAIL</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12/06/1985">
  </div>
  <div class="form-group">
    <label for="pwd">CONTACT NO.</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="+65 123 4567">
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>