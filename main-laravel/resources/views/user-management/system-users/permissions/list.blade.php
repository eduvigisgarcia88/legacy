@extends('layouts.master')

@section('scripts')
<script>

  function refresh() {
    var loading = $(".loading-pane");
      var url = '{{ url("users/refresh/permissions") }}';
      var token = '{{ csrf_token() }}';
      var table = $('#rows');

      loading.removeClass("hide");
      $.post(url, { _token: token }, function(response) {
    
        var body = "";

        $.each(response.rows, function(index, row) {
            if (row.id != "4" && row.id != "6" && row.id != "10") {
              body += '<div class="container-fluid panel-heading tbheader">' + row.type_name + '</div>' +
                '<table class="table table-bordered">' +
                '<thead>' +
                  '<tr>' +
                    '<th class="col-lg-6">MODULES</th>' +
                    '<th class="col-lg-2 text-center">VIEW</th>' +
                    '<th class="col-lg-2 text-center">EDIT</th>' +
                    '<th class="col-lg-2 text-center">DELETE</th>' +
                  '</tr>'+
                '</thead>' +
                '<tbody>';

                $.each(row.modules, function(index, module) {
                  body +='<tr data-id="' + module.id + '">' +
                    '<td data-collapse-group="" data-toggle="collapse" data-target="#inner" class="accordion-toggle">' + module.permission_modules.module + '</td>' +
                    '<td class="text-center"><input type="checkbox" class="view"' + ((module.view == 1) ? ' checked' : '') + '></td>' +
                    '<td class="text-center"><input type="checkbox" class="edit"' + ((module.edit == 1) ? ' checked' : '') + '></td>' +
                    '<td class="text-center"><input type="checkbox" class="delete"' + ((module.delete == 1) ? ' checked' : '') + '></td>' +
                  '</tr>';
                });
                body +=  '</tbody></table>';
            }
        });

        loading.addClass("hide");

        table.html(body);

      }, 'json');
  }

  $(document).ready(function() {

    // save
    $("#page-content-wrapper").on("click", ".save-permissions", function() {
      var url = '{{ url("users/permissions/save") }}';
      var token = "{{ csrf_token() }}";
      var rows = [];
     
      var loading = $(".loading-pane");
      loading.removeClass("hide");

      $('tr').each(function() {
        if ($(this).data('id')) { 
          rows.push({'id': $(this).data('id'), 'view': $(this).find('.view').prop('checked'), 'edit': $(this).find('.edit').prop('checked'), 'delete': $(this).find('.delete').prop('checked')});
        }
      });

      $.post(url, { rows: rows, _token: token }, function(response) {
        if (response.success) {
          loading.addClass("hide");
          toastr["success"](response.success);
        }

        refresh();
        location.reload();
      }, 'json');
    });

    // undo
    $("#page-content-wrapper").on("click", ".undo-permissions", function() {
      refresh();
    });

  });


</script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
  <div class="loading-pane hide">
    <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
  </div>
<!-- Title -->
  <div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
      <h3 class="main-title">PERMISSIONS</h3>
      <h5 class="bread-crumb">USER MANAGEMENT</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
      <button type="button" class="btn btn-warning borderzero btn-tool pull-right save-permissions"><i class="fa fa-save"></i> SAVE</button>
      <button type="button" class="btn btn-danger borderzero btn-tool pull-right undo-permissions"><i class="fa fa-undo"></i> UNDO</button> 
    </div>
  </div>
  <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
    <div class="panel panel-default table-responsive block-content tbblock">
    <!-- Default panel contents -->
      <div id="rows">
        @foreach($rows as $row)
          @if ($row->id != "4" && $row->id != "6" && $row->id != "10")
          @if ($row->id == 9)
          <div class="container-fluid panel-heading tbheader">Compliance Officer</div>
          @else          
          <div class="container-fluid panel-heading tbheader">{{ $row->type_name }}</div>
          @endif
          <!-- Table -->        
          <table class="table table-striped">
            <thead>
              <tr>
                <th class="col-lg-6">MODULES</th>
                <th class="col-lg-2 text-center">VIEW</th>
                <th class="col-lg-2 text-center">EDIT</th>
                <th class="col-lg-2 text-center">DELETE</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($row->modules as $module)
              <tr data-id="{{ $module->id }}">
                <td data-collapse-group="" data-toggle="collapse" data-target="#inner" class="accordion-toggle">{{ $module->permissionModules->module }}</td>
                <td class="text-center"><input type="checkbox" class="view"{{ ($module->view == 1) ? ' checked' : '' }}></td>
                <td class="text-center"><input type="checkbox" class="edit"{{ ($module->edit == 1) ? ' checked' : '' }}></td>
                <td class="text-center"><input type="checkbox" class="delete"{{ ($module->delete == 1) ? ' checked' : '' }}></td>
              </tr>
            @endforeach
            </tbody>
          </table>
          @endif
        @endforeach
      </div>
    </div>
    <button type="button" class="btn btn-warning borderzero btn-tool pull-right save-permissions"><i class="fa fa-save"></i> SAVE</button>
    <button type="button" class="btn btn-danger borderzero btn-tool pull-right undo-permissions"><i class="fa fa-undo"></i> UNDO</button> 
  </div>
</div>
    <!-- /#page-content-wrapper -->

@include('user-management.system-users.permissions.form')

@stop