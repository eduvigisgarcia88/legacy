<!-- modal add user -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'users/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
              </div>
              <div class="modal-viewing modal-body">
               <div id="form-notice"></div>
                <div class="form-group system-photo-div">
                     <label for="row-photo" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                     <div class="col-sm-9 col-xs-7 system-photo-pane">
<!--                      <button type="button" class="btn btn-primary borderzero hide photo_view" id="edit-change-photo">CHANGE</button> -->
                     </div>
                     <div class="col-sm-9 col-xs-7 uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
                        <input name='photo' id='row-photo_upload' type='file' class='file borderzero file_photo' data-show-upload='false' placeholder='Upload a photo...'>
                        <button type="button" class="btn btn-primary borderzero" id="btn-cancel-photos">Cancel</button>
                      </div>
                     </div>
                </div>
                <div class="form-group">
                    <label for="row-code" class="col-sm-3 col-xs-5 control-label font-color">User Code</label>
                <div class="col-sm-9 col-xs-7 error-code ">
                  <input type="text" name="code" class="form-control borderzero" id="row-code" maxlength="30" placeholder="Enter your user code...">
                  <sup class="sup-errors"></sup>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7 error-name ">
                  <input type="text" name="name" class="form-control borderzero" id="row-name" maxlength="30" placeholder="Enter your last name">
                  <sup class="sup-errors"></sup>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7 error-email ">
                  <input type="email" name="email" class="form-control borderzero" id="row-email" maxlength="100" placeholder="Enter your email address...">
                  <sup class="sup-errors"></sup>
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7 error-password ">
                  <input type="password" name="password" class="form-control borderzero pwd-password" id="row-password" maxlength="30" placeholder="Enter your password...">
                  <sup class="sup-errors"></sup>
                </div>

            <div class="col-sm-9 col-sm-offset-3" id="pwd-container">
            <input type="hidden" class="total-score" name="pass_score">
            <div class="pwstrength_viewport_progress"></div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 error-pass_score">
            <sup class="sup-errors"></sup>
            </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control borderzero" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                      <label for="row-usertype_id" class="col-sm-3 col-xs-5 control-label font-color">Role</label>
                <div class="col-sm-9 col-xs-7 error-usertype_id ">
                <select name="usertype_id" class="form-control hide-view borderzero" id="row-usertype_id">
                    <option value="" class="hide">Select:</option>
                    @foreach ($types as $row)
                      @if($row->id == $type_id)
                        <option value="{{ $row->id }}" selected="true">{{ $row->type_name }}</option>
                      @else
                        <option value="{{ $row->id }}">{{ $row->type_name }}</option>
                      @endif
                    @endforeach
                  </select>
                  <sup class="sup-errors"></sup>
                <input type="text" class="form-control hide disabled show-view borderzero" id="row-usertype_id-static" value="" readonly="">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7 error-mobile ">
                  <input type="text" name="mobile" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                  <sup class="sup-errors"></sup>
                </div>
                </div>
                <!-- <div class="form-group social_media_pane hide">
                    <label for="row-social-media" class="col-sm-3 col-xs-5 control-label font-color">Social Media</label>
                <div class="col-sm-9 col-xs-7">
                   <fb:login-button size="medium" scope='public_profile,email' onlogin='checkLoginState();' data-show-faces="false" data-auto-logout-link="true" class="pull-left">
                   Link with Facebook
                   </fb:login-button>
                </div>
                </div> -->
              </div>
              <div class="modal-footer borderzero">
                <input type="hidden" id="status">
                <input type="hidden" id="image_fb_link_edit_photo">
                <input type="hidden" name="id" id="row-id">
                <input type="hidden" id="image_fb_link" name="image_link">
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view borderzero btn-submit"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

  <!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>



<!-- modal add user -->
<div class="modal fade" id="modal-assign" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-assign" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'sales-assistant/save-agents', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_assign', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="assign-title"><i class="fa fa-plus"></i> Assign Sales Users</h4>
              </div>
              <div class="modal-body">
               <div id="assign-notice"></div>
              <div class="form-horizontal">
                <label for="agents">Agents</label>
                <select id="agents" class="form-control select2" name="ids[]" multiple="multiple">
                  
                </select>
              </div>
              </div>
              <div class="clearfix"></div>
              <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="assign-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>


<div id="myModalassign" class="modal fade" role="dialog">
  <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header modal-warning">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title"><i class="fa fa-plus"></i> Assign Sales Users</h4>
      </div>
      <div class="modal-body">
      <form role="form">
        <div class="form-group">
          <label for="select-designation">Designation</label>
          <select id="select-designation" class="form-control" style="width: 100%">
            @foreach ($designations as $row)
              <option value="{{ $row->id }}">{{ $row->designation }}</option>  
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="agents">Agents</label>
          <select id="agents" class="form-control select2" multiple="multiple">
            
          </select>
        </div>
      </form>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
      <button type="button" class="btn btn-default borderzero btn-save" data-dismiss="modal">Save Changes</button>
      </div>
    </div>
  </div>
</div>