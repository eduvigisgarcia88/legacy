@extends('layouts.master')

@section('scripts')
<script type="text/javascript">

function statusChangeCallback(response) {

  var loading = $(".loading-pane");
    //$(".custom-photo-pane").addClass("hide");
    //$(".default-photo-pane").removeClass("hide");
   // console.log('statusChangeCallback');
   // console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {

      loading.removeClass("hide");
      // Logged into your app and Facebook.
       $("#standard_photo").addClass("hide");
       $(".standard_photo_pane").addClass("hide");
       $("#standard_photo_for_user").addClass("hide");
       // $(".default-photo-pane").addClass("hide");
       $(".facebook-photo-pane").removeClass("hide");
       //console.log(response.authResponse.accessToken);
       ///$("#fb-connect").attr('title','Logout your facebook account');
       FB.api('/me', function(response) {
      
        $(".system-photo-pane").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' style='margin: 5px;' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        //$(".system-photo-pane").append("");
        $("#image_pane").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_pane_edit_photo").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_fb_link").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_for_system_user").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_edit_photo").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#default_image_pane").addClass("hide");
        $("#image_pane_edit_photo").removeClass("hide");
        $("#image_pane").removeClass("hide");
        $("#row-photo").addClass("hide");
        $("#user_photo_pane").addClass("hide");



        });
      $("#status").val(1);
      
      loading.addClass("hide");
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
    
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");


    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.

        $("#row-photo").removeClass("hide");
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");
        $("#image_pane").addClass("hide");
        $("#image_pane_edit_photo").addClass("hide");
        $("#default_image_pane").removeClass("hide");
        $("#image_pane_for_system_user").html("");
        $("#user_photo_pane").removeClass("hide");
        $(".facebook-photo-pane").addClass("hide");
        //$(".system-photo-div").html("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-photo-change'>CHANGE</button>");
        //$(".system-photo-pane").html("<img id='row-photo_row' src='....' class='preview img-circle' height='100' width='100' style='margin:5px;'/>");
      
          $(".system-photo-pane").html("<img src='{{ url('uploads')."/".$settings->default_photo }}' class='img-circle' id='row-photo_row' height='100' width='100' style='margin:5px;'/>");
          $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
  
       
        //$(".system-photo-pane").append("<div class='change'><input name='photo' id='row-photo_upload' type='file' class='file borderzero file_photo' data-show-upload='false' placeholder='Upload a photo...'></div>");
        $("#status").val(0);
        // $(".custom-photo-pane").html("");
        // $(".default-photo-pane").html("");


        //$("#row-photo_row").removeClass("hide");
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '395487573951234',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);

  });
  };
 
    // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
function refresh() {
    var id = "{{ $sales_id }}";
    $_token = "{{ csrf_token() }}";
    var url = "{{ url('/') }}";
    var loading = $(".loading-pane");
    var tbl_designation = $("#info_designations");
    var tbl_bonding = $("#info_bondings");
    var tbl_team = $("#info_teams");

     
        
    loading.removeClass("hide");

      $.post("{{ url('sales/' . $sales_id . '/get-info') }}", { id: id, _token: $_token }, function(response) {

        tbl_designation.html("");
        tbl_bonding.html("");
        //tbl_team.html("");

        //console.log(response);
          // output form data
          $.each(response, function(index, object) {
            
            console.log(index);
            if (index == "rows") {
              $.each(object, function(vindex, value) {
                  console.log(vindex);
                var field = $("#label-" + vindex);
                //console.log(vindex + '=' + value);
                if (vindex == "photo") {
                  $("#img-photo").attr('src', url + '/uploads/' + value + '?' + new Date().getTime());
                }
                //console.log(vindex);
                if (vindex == "status") {
                 // console.log(value);
                  if (value == 1) {
                    $('#label-static-status').val('Active');
                  } else if (value == 0) {
                    $('#label-static-status').val('DISABLED');
                  }
                }
              
                if (vindex == "sales_info") {
                  $('#label-designation').html(value.designations.designation);
                  $('#label-sales_id').html(value.sales_id); 
                }
                // field exists, therefore populate
                if(field.length > 0) {
                    field.html(value);
                }
              });
            
            } else if (index == "sales_designation") {
              $.each(object, function(windex, row) {
                var prev = row.prev_designation.designation;
                tbl_designation.append(
                  "<tr>"
                    + "<td>" + moment(row.created_at).format('MM-DD-YYYY') + "</td>"
                    + "<td>" + row.modify.name + "</td>"
                    + "<td>" + prev + "</td>"
                    + "<td>" + row.new_designation.designation +"</td>"
                    + "<td class='rightalign'>" + moment(row.effective_date).format('MM-DD-YYYY') + "</td>" + 
                  "</tr>"
                );
             
              });

            } else if (index == "sales_bonding") {
              console.log(bonding);
              $.each(object, function(xindex, row) {
                console.log(xindex+"="+row);
                tbl_bonding.append(
                  "<tr data-id ="+row.id+">"
                    + "<td>" + moment(row.created_at).format('MM-DD-YYYY')  + "</td>"
                    + "<td>" + row.bonding_modify.name + "</td>"
                    + "<td>" + row.bonding_rate + "%</td>"
                    + "<td class='rightalign'>" + moment(row.effective_date).format('MM-DD-YYYY') + "</td>" + 
                  "</tr>"
                );
              });
            } else if(index == "get_provider_codes"){

                     $.each(object, function(sindex, svalue) {

                        $("#row-provider_code["+svalue.id+"]").val(svalue.provider_code);

                      });    

            }
            // } else if (index == "add_info") {
            //   $.each(object, function(aindex, row) {
            //        row.date_of_birth == null ? $("#label-add_info_dob").text("N/A"):$("#label-add_info_dob").text(moment(row.date_of_birth).format('MM-DD-YYYY'));
            //        row.uin_no == "" ? $("#label-uin_no").text("N/A"):$("#label-uin_no").text(row.uin_no);
            //        row.house_no == "" ? $("#label-house_no").text("N/A"):$("#label-house_no").text(row.house_no);
            //        row.mgt_email == "" ? $("#label-mgt_email").text("N/A"):$("#label-mgt_email").text(row.mgt_email);
            //        row.designation_tag == "" ? $("#label-designation_tag").text("N/A"):$("#label-designation_tag").text(row.designation_tag);
            //        row.unit_code == "" ? $("#label-unit_code").text("N/A"):$("#label-unit_code").text(row.unit_code);
            //        row.unit_name == "" ? $("#label-unit_name").text("N/A"):$("#label-unit_name").text(row.unit_name);
            //        row.contract_start == null ? $("#label-contract_start").text("N/A"):$("#label-contract_start").text(moment(row.contract_start).format('MM-DD-YYYY'));
            //        row.contract_end == null ? $("#label-contract_end").text("N/A"):$("#label-contract_end").text(moment(row.contract_end).format('MM-DD-YYYY'));
            //        row.days_since_contracted == "" ? $("#label-days_since_contracted").text("N/A"):$("#label-days_since_contracted").text(row.days_since_contracted);
            //        row.elite_scheme_start == null ? $("#label-elite_scheme_start").text("N/A"):$("#label-elite_scheme_start").text(moment(row.elite_scheme_start).format('MM-DD-YYYY'));
            //        row.elite_scheme_end == null ? $("#label-elite_scheme_end").text("N/A"):$("#label-elite_scheme_end").text(moment(row.elite_scheme_end).format('MM-DD-YYYY'));
            //        row.eccs_appset_tranche == "" ? $("#label-eccs_appset_tranche").text("N/A"):$("#label-eccs_appset_tranche").text(row.eccs_appset_tranche);
            //        row.be_code == "" ? $("#label-be_code").text("N/A"):$("#label-be_code").text(row.be_code);
            //        row.fighter_leads_start == null ? $("#label-fighter_leads_start").text("N/A"):$("#label-fighter_leads_start").text(moment(row.fighter_leads_start).format('MM-DD-YYYY'));
            //        row.fighter_leads_stop == null ? $("#label-fighter_leads_stop").text("N/A"):$("#label-fighter_leads_stop").text(moment(row.fighter_leads_stop).format('MM-DD-YYYY'));
            //        row.care_leads_start == null ? $("#label-care_leads_start").text("N/A"):$("#label-care_leads_start").text(moment(row.care_leads_start).format('MM-DD-YYYY'));
            //        row.care_leads_stop == null ? $("#label-care_leads_stop").text("N/A"):$("#label-care_leads_stop").text(moment(row.care_leads_stop).format('MM-DD-YYYY'));
            //        row.annual_target_ape == "" ? $("#label-annual_target_ape").text("N/A"):$("#label-annual_target_ape").text(row.annual_target_ape);
            //        row.final_sprint_target_ape == "" ? $("#label-final_sprint_target_ape").text("N/A"):$("#label-final_sprint_target_ape").text(row.final_sprint_target_ape);
            //        row.did_last_checked == "" ? $("#label-did_last_checked").text("N/A"):$("#label-did_last_checked").text(row.did_last_checked);
            //        row.description == "" ? $("#label-description").text("N/A"):$("#label-description").text(row.description);
            //        row.organic_js == "" ? $("#label-organic_js").text("N/A"):$("#label-organic_js").text(row.organic_js);
            //        row.list_matrix == "" ? $("#label-list_matrix").text("N/A"):$("#label-list_matrix").text(row.list_matrix);
            //        row.mgt_candidates == "" ? $("#label-mgt_candidates").text("N/A"):$("#label-mgt_candidates").text(row.mgt_candidates);
            //        row.holland_trip == "" ? $("#label-holland_trip").text("N/A"):$("#label-holland_trip").text(row.holland_trip);
            //        row.cbdp_tier == "" ? $("#label-cbdp_tier").text("N/A"):$("#label-cbdp_tier").text(row.cbdp_tier);
            //        row.emergency_contacts == "" ? $("#label-emergency_contacts").text("N/A"):$("#label-emergency_contacts").text(row.emergency_contacts);
            //        row.home_address == "" ? $("#label-home_address").text("N/A"):$("#label-home_address").text(row.home_address);
            //        row.passport_expired == null ? $("#label-passport_expired").text("N/A"):$("#label-passport_expired").text(moment(row.passport_expired).format('MM-DD-YYYY'));
            //        row.passport_issued == null ? $("#label-passport_issued").text("N/A"):$("#label-passport_issued").text(moment(row.passport_issued).format('MM-DD-YYYY'));
            //        row.postal_code == "" ? $("#label-postal_code").text("N/A"):$("#label-postal_code").text(row.postal_code);
            //        row.remarks == "" ? $("#label-remarks").text("N/A"):$("#label-remarks").text(row.remarks);
            //        row.final_sprint_goal == "" ? $("#label-final_sprint_goal").text("N/A"):$("#label-final_sprint_goal").text(row.final_sprint_goal);
            //        row.base_income == "" ? $("#label-base_income").text("N/A"):$("#label-base_income").text(row.base_income);
            //        row.passport_number == "" ? $("#label-passport_number").text("N/A"):$("#label-passport_number").text(row.passport_number);
            //        row.yahoo_messenger == "" ? $("#label-yahoo_messenger").text("N/A"):$("#label-yahoo_messenger").text(row.yahoo_messenger);
            //        row.accounting_id == "" ? $("#label-accounting_id").text("N/A"):$("#label-accounting_id").text(row.accounting_id);
            //        row.cbdp_group == "" ? $("#label-cbdp_group").text("N/A"):$("#label-cbdp_group").text(row.cbdp_group);
            //        row.skype == "" ? $("#label-skype").text("N/A"):$("#label-skype").text(row.skype);
            //        row.going_for_retreat == "y" ?$("#label-going_for_retreat").text("YES"):$("#label-going_for_retreat").text("NO");
            //        row.going_for_retreat == null ? $("#label-going_for_retreat").text("N/A"):"";
            //       //$("#label-house_no").text(row.house_no);
            //       //$("#label-mgt_email").text(row.mgt_email); 
            //       //$("#label-designation_tag").text(row.designation_tag);
            //       //$("#label-unit_code").text(row.unit_code);
            //       //$("#label-unit_name").text(row.unit_name);
            //      // $("#label-contract_start").text(moment(row.contract_start).format('MM-DD-YYYY'));
            //       //$("#label-contract_end").text(moment(row.contract_end).format('MM-DD-YYYY'));
            //       //$("#label-days_since_contracted").text(row.days_since_contracted);
            //       //$("#label-elite_scheme_start").text(moment(row.elite_scheme_start).format('MM-DD-YYYY'));
            //       //$("#label-elite_scheme_end").text(moment(row.elite_scheme_end).format('MM-DD-YYYY'));
            //       //$("#label-eccs_appset_tranche").text(row.eccs_appset_tranche);
            //       //$("#label-be_code").text(row.be_code);
            //       //$("#label-fighter_leads_start").text(moment(row.fighter_leads_start).format('MM-DD-YYYY'));
            //       //$("#label-fighter_leads_stop").text(moment(row.fighter_leads_stop).format('MM-DD-YYYY'));
            //       //$("#label-care_leads_start").text(moment(row.care_leads_start).format('MM-DD-YYYY'));
            //       //$("#label-care_leads_stop").text(moment(row.care_leads_stop).format('MM-DD-YYYY'));
            //       //$("#label-annual_target_ape").text(row.annual_target_ape); 
            //       //$("#label-final_sprint_target_ape").text(row.final_sprint_target_ape);
            //       //$("#label-did_last_checked").text(moment(row.did_last_checked).format('MM-DD-YYYY'));
            //       //$("#label-description").text(row.description);
            //       //$("#label-organic_js").text(row.organic_js);
            //       //$("#label-list_matrix").text(row.list_matrix);
            //       //$("#label-holland_trip").text(row.holland_trip);
            //       //$("#label-cbdp_tier").text(row.cbdp_tier);
            //       //$("#label-emergency_contacts").text(row.emergency_contacts);
            //       //$("#label-home_address").text(row.home_address);
            //       //$("#label-postal_code").text(row.postal_code);
            //       //$("#label-remarks").text(row.remarks);
            //       //$("#label-final_sprint_goal").text(row.final_sprint_goal);
            //       //$("#label-base_income").text(row.base_income);
            //       //$("#label-passport_number").text(row.passport_number); 
            //       //$("#label-passport_expired").text(moment(row.passport_expired).format('MM-DD-YYYY'));
            //       //$("#label-passport_issued").text(moment(row.passport_issued).format('MM-DD-YYYY'));
            //       //$("#label-yahoo_messenger").text(row.yahoo_messenger);
            //       //$("#label-accounting_id").text(row.accounting_id);
            //       //$("#label-cbdp_group").text(row.cbdp_group);
            //       //$("#label-skype").text(row.skype);

                 
            //   });
            // } else if (index == "company_info") {
            //   $.each(object, function(cindex, row) {
            //        row.company_email_address == "" ? $("#label-company_email_address").text("N/A"):$("#label-company_email_address").text(row.company_email_address);
            //        row.company_email_address_new == "" ? $("#label-company_email_address_new").text("N/A"):$("#label-company_email_address_new").text(row.company_email_address_new);
            //        row.rnf_date == null ? $("#label-rnf_date").text("N/A"):$("#label-rnf_date").text(moment(row.rnf_date).format('MM-DD-YYYY'));
            //        row.department == "" ? $("#label-department").text("N/A"):$("#label-department").text(row.department);
            //       //$("#label-company_email_address").text(row.company_email_address);
            //       //$("#label-company_email_address_new").text(row.company_email_address_new);
            //       //$("#label-rnf_date").text(moment(row.rnf_date).format('MM-DD-YYYY'));
            //       //("#label-department").text(row.department); 
            //   });
            // } else if (index == "dialer_info") {
            //   $.each(object, function(dindex, row) {
            //        row.dialer_un == "" ? $("#label-dialer_un").text("N/A"):$("#label-dialer_un").text(row.dialer_un);
            //        row.dialer_pw == "" ? $("#label-dialer_pw").text("N/A"):$("#label-dialer_pw").html("<i class='fa fa-eye-slash'></i>");
            //        row.dialer_status == "" ? $("#label-dialer_status").text("N/A"):$("#label-dialer_status").text(row.dialer_status);
            //        row.list == "" ? $("#label-list").text("N/A"):$("#label-list").text(row.list);
            //        row.user_group == "" ? $("#label-user_group").text("N/A"):$("#label-user_group").text(row.user_group);
            //        row.campaign == "" ? $("#label-campaign").text("N/A"):$("#label-campaign").text(row.campaign);
            //       //$("#label-dialer_un").text(row.dialer_un);
            //       //$("#label-dialer_pw").text(row.dialer_pw);
            //       //$("#label-dialer_status").text(row.dialer_status);
            //       //$("#label-list").text(row.list);
            //       //$("#label-user_group").text(row.user_group); 
            //       //$("#label-campaign").text(row.campaign);  
            //   });
            // } else if (index == "sales_force_info") {
            //   $.each(object, function(dindex, row) {
            //       row.sales_force_id == "" ? $("#label-sales_force_id").text("N/A"):$("#label-sales_force_id").text(row.sales_force_id);
            //       row.google_account == "" ? $("#label-google_account").text("N/A"):$("#label-google_account").text(row.google_account);
            //       //$("#label-sales_force_id").text(row.sales_force_id);
            //       //$("#label-google_account").text(row.google_account); 
            //   });
            // } else if (index == "jumpsite_info") {
            //   $.each(object, function(jindex, row) {
            //        row.jumpsite_batch == "" ? $("#label-jumpsite_batch").text("N/A"):$("#label-jumpsite_batch").text(row.jumpsite_batch);
            //        row.jumpsite == "" ? $("#label-jumpsite").text("N/A"):$("#label-jumpsite").text(row.jumpsite);
            //        row.js_clients == "" ? $("#label-js_clients").text("N/A"):$("#label-js_clients").text(row.js_clients);
            //        row.jumpsite_user == "" ? $("#label-jumpsite_user").text("N/A"):$("#label-jumpsite_user").text(row.jumpsite_user);
            //       //$("#label-jumpsite_batch").text(row.jumpsite_batch);
            //       //$("#label-jumpsite").text(row.jumpsite);
            //       //$("#label-js_clients").text(row.js_clients);
            //       //$("#label-jumpsite_user").text(row.jumpsite_user); 
            //   });
            // } else if (index == "team_info") {
            //   $.each(object, function(tindex, row) {
            //        row.direct_reporting_to == "" ? $("#label-direct_reporting_to").text("N/A"):$("#label-direct_reporting_to").text(row.direct_reporting_to);
            //        row.group_name == "" ? $("#label-group_name").text("N/A"):$("#label-group_name").text(row.group_name);
            //        row.reporting_tag == "" ? $("#label-reporting_tag").text("N/A"):$("#label-reporting_tag").text(row.reporting_tag);
            //        row.indirect_reporting_to == "" ? $("#label-indirect_reporting_to").text("N/A"):$("#label-indirect_reporting_to").text(row.indirect_reporting_to);
            //        row.group_tag == "" ? $("#label-group_tag").text("N/A"):$("#label-group_tag").text(row.group_tag);
            //       //$("#label-direct_reporting_to").text(row.direct_reporting_to);
            //       //$("#label-group_name").text(row.group_name);
            //       //$("#label-reporting_tag").text(row.reporting_tag);
            //       //$("#label-indirect_reporting_to").text(row.indirect_reporting_to);
            //       //$("#label-group_tag").text(row.group_tag);  
            //   });
            // } else if (index == "security_info") {
            //   $.each(object, function(sindex, row) {
            //        row.two_digit_randomizer == "" ? $("#label-two_digit_randomizer").text("N/A"):$("#label-two_digit_randomizer").text(row.two_digit_randomizer);
            //        row.temp_password == "" ? $("#label-temp_password").text("N/A"):$("#label-temp_password").html("<i class='fa fa-eye-slash'></i>");
            //        row.laptop_wlan == "" ? $("#label-laptop_wlan").text("N/A"):$("#label-laptop_wlan").text(row.laptop_wlan);
            //        row.mac_address == "" ? $("#label-mac_address").text("N/A"):$("#label-mac_address").text(row.mac_address);
                   
            //       //$("#label-two_digit_randomizer").text(row.two_digit_randomizer);
            //       //$("#label-temp_password").text(row.temp_password);
            //       //$("#label-laptop_wlan").text(row.laptop_wlan);
            //       //$("#label-mac_address").text(row.mac_address); 
            //   });
            // } else if (index == "iserver_info") {
            //   $.each(object, function(isindex, row) {
            //        row.iserver_username == "" ? $("#label-iserver_username").text("N/A"):$("#label-iserver_username").text(row.iserver_username);
            //        row.iserver_password == "" ? $("#label-iserver_password").text("N/A"):$("#label-iserver_password").html("<i class='fa fa-eye-slash'></i>");
            //        row.iserver_email_address == "" ? $("#label-iserver_email_address").text("N/A"):$("#label-iserver_email_address").text(row.iserver_email_address);
            //        row.iserver_group_name == "" ? $("#label-iserver_group_name").text("N/A"):$("#label-iserver_group_name").text(row.iserver_group_name);
            //        row.iserver_bay_folder == "" ? $("#label-iserver_bay_folder").text("N/A"):$("#label-iserver_bay_folder").text(row.iserver_bay_folder);
            //        row.iserver_ftp_path == "" ? $("#label-iserver_ftp_path").text("N/A"):$("#label-iserver_ftp_path").text(row.iserver_ftp_path);
            //       //$("#label-iserver_username").text(row.iserver_username);
            //       //$("#label-iserver_password").text(row.iserver_password);
            //       //$("#label-iserver_email_address").text(row.iserver_email_address);
            //       //$("#label-iserver_group_name").text(row.iserver_group_name);
            //       //$("#label-iserver_bay_folder").text(row.iserver_bay_folder);
            //       //$("#label-iserver_ftp_path").text(row.iserver_ftp_path);  
            //   });
            // } else if (index == "misc_info") {
            //   $.each(object, function(mindex, row) {
            //       row.contract == "y" ?$("#label-contract").text("YES"):$("#label-contract").text("NO");
            //         row.contract == null ? $("#label-contract").text("N/A"):"";
            //       row.original_is == "y" ?$("#label-original_is").text("YES"):$("#label-original_is").text("NO");
            //         row.original_is == null ? $("#label-original_is").text("N/A"):"";
            //       row.authorisation == "y" ?$("#label-authorisation").text("YES"):$("#label-authorisation").text("NO");
            //         row.authorisation == null ? $("#label-authorisation").text("N/A"):"";
            //       row.laptop_ethernet_mac_address == "y" ?$("#label-laptop_ethernet_mac_address").text("YES"):$("#label-laptop_ethernet_mac_address").text("NO");
            //         row.laptop_ethernet_mac_address == null ? $("#label-laptop_ethernet_mac_address").text("N/A"):"";
            //       row.photocopy_is == "y" ?$("#label-photocopy_is").text("YES"):$("#label-photocopy_is").text("NO");
            //         row.photocopy_is == null ? $("#label-photocopy_is").text("N/A"):"";
            //       row.production_report == "y" ?$("#label-production_report").text("YES"):$("#label-production_report").text("NO");
            //         row.production_report == null ? $("#label-production_report").text("N/A"):"";
            //       row.nric_copy == "y" ?$("#label-nric_copy").text("YES"):$("#label-nric_copy").text("NO");
            //         row.nric_copy == null ? $("#label-nric_copy").text("N/A"):"";
            //       row.resignation_letter == "y" ?$("#label-resignation_letter").text("YES"):$("#label-resignation_letter").text("NO");
            //         row.resignation_letter == null ? $("#label-resignation_letter").text("N/A"):"";
            //       row.highest_cert == "y" ?$("#label-highest_cert").text("YES"):$("#label-highest_cert").text("NO");
            //         row.highest_cert == null ? $("#label-highest_cert").text("N/A"):"";
            //       row.m5 == "y" ?$("#label-m5").text("YES"):$("#label-m5").text("NO");
            //         row.m5 == null ? $("#label-m5").text("N/A"):"";
            //       row.m9 == "y" ?$("#label-m9").text("YES"):$("#label-m9").text("NO");
            //         row.m9 == null ? $("#label-m9").text("N/A"):"";
            //       row.hi == "y" ?$("#label-hi").text("YES"):$("#label-hi").text("NO");
            //         row.hi == null ? $("#label-hi").text("N/A"):"";
            //       row.m8 == "y" ?$("#label-m8").text("YES"):$("#label-going_for_retreat").text("NO");
            //         row.m8 == null ? $("#label-m8").text("N/A"):"";
            //       row.cpf == "y" ?$("#label-cpf").text("YES"):$("#label-cpf").text("NO");
            //         row.cpf == null ? $("#label-cpf").text("N/A"):"";
            //       row.adv_data == "y" ?$("#label-adv_data").text("YES"):$("#label-adv_data").text("NO");
            //         row.adv_data == null ? $("#label-adv_data").text("N/A"):"";
            //       row.n_card == "y" ?$("#label-n_card").text("YES"):$("#label-n_card").text("NO");
            //         row.n_card == null ? $("#label-n_card").text("N/A"):"";
            //       row.misc_photo == "y" ?$("#label-misc_photo").text("YES"):$("#label-misc_photo").text("NO");
            //         row.misc_photo == null ? $("#label-misc_photo").text("N/A"):"";
            //       row.training == "y" ?$("#label-training").text("YES"):$("#label-training").text("NO");
            //         row.training == null ? $("#label-training").text("N/A"):"";
            //       row.photocopy_cert == "y" ?$("#label-photocopy_cert").text("YES"):$("#label-photocopy_cert").text("NO");
            //         row.photocopy_cert == null ? $("#label-photocopy_certs").text("N/A"):"";
            //       row.cpf_liability == "y" ?$("#label-cpf_liability").text("YES"):$("#label-cpf_liability").text("NO");
            //         row.cpf_liability == null ? $("#label-cpf_liability").text("N/A"):"";
            //       row.original_cert == "" ? $("#label-original_cert").text("N/A"):$("#label-original_cert").text(row.original_cert);
            //   });
            // }                                                            
            //else if (index == "sales_team") {
               //if (object) {
               //  $.each(object, function(xindex, row) {
               //    tbl_team.append(
                   //  "<tr>"
                      // + "<td>" + moment(row.created_at).format('MM-DD-YYYY')  + "</td>"
                     //  + "<td>" + row.team_modified.name + "</td>"
                      // + "<td>" + row.team_code.supervisor.visor.partner_info.team.team_code + "</td>"
                      // + "<td>" + row.team_code.supervisor.sales_id + "</td>"
                    //  // + "<td class='rightalign'>" + row.team_code.supervisor.users.name + "</td>"
                   // + "</tr>"
                //  );
                // });
              // } else {
                // tbl_team.append(
                //     "<tr>"
                 //     + "<td colspan='6'><center>SALES HAS NO TEAM.</center></td>"
                //    + "</tr>"
               //   );
               //}
            //}
          });
      
        loading.addClass("hide");
          //location.reload();
      }, 'json');
}

$('#edit').click(function(){
  $('#row-designation').prop('disabled', false);
  $('#photo').prop('disabled', false);
  $('#row-code').prop('disabled', false);
  $('#row-username').prop('disabled', false);
  $('#row-name').prop('disabled', false);
  $('#row-email').prop('disabled', false);
  $('#row-mobile').prop('disabled', false);
  $('.file').prop('disabled', false);
  $('#customfileupload').prop('disabled', false);

  //$('.status').prop('disabled', false);
});
$('#cancel').click(function(){
  $('#row-sales_id').prop('disabled', true);
  $('#row-designation').prop('disabled', true);
  $('#photo').prop('disabled', true);true
  $('#row-code').prop('disabled', true);
  $('#row-username').prop('disabled', true);
  $('#row-name').prop('disabled', true);
  $('#row-email').prop('disabled', true);
  $('#row-mobile').prop('disabled', true);
  $('.file').prop('disabled', true);
  $('#customfileupload').prop('disabled', true);
  //$('.status').prop('disabled', false);
});

$(document).ready(function() {
  $(function () {
        $('.date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

    $_token = "{{ csrf_token() }}";

    $.post("{{ url('sales/get-groups') }}", { _token: $_token }, function(response) {
      var group = $('#row-group_code');
      group.html('<option value="" class="hide">Select:</option>');
      $.each(response.get_group, function(index, row) {
        group.append('<option value="' + row.id + '">' + row.code + '</option>');
      });
    }, 'json');

      // if($("#user_rank").text()=="Supervisor"){
      //       $(".unit_code_undropdown").removeClass("hide");
      //       $(".unit_code_dropdown").addClass("hide");
      // }
      // if($("#user_rank").text()=="Advisor"){
      //       $(".unit_code_dropdown").removeClass("hide");
      //       $(".unit_code_undropdown").addClass("hide");

      // }




    //select2

    $("#row-supervisor_id").select2();

    $(function () {
        $('.effective-date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
    });

    $("#page-content-wrapper").on("click", ".btn-bonding", function() {
      $("#modal-bonding").modal('show');
       // reset all form fields
      $("#modal-bonding").find('form').trigger("reset");
      console.log("{{ $sales_id }}");
      $("#bonding-id").val("{{ $sales_id }}")
    });

    $("#page-content-wrapper").on("click", ".btn-team", function() {
      $("#modal-team").modal('show');
      $("#row-supervisor_id").select2("val", "");

       // reset all form fields
      $("#modal-team").find('form').trigger("reset");
      $("#team-id").val("{{ $sales_id }}")

      var supervisor = $("#row-supervisor_id");
      var id = $("#team-id").val();
      $_token = "{{ csrf_token() }}";

      $.post("{{ url('sales/get-sales-supervisor') }}", { id: id, _token: $_token }, function(response) {

        supervisor.append(
          '<option value="" class="hide"></option>'
        );
        $.each(response.rows, function(result, row) {

          $.each(row.sales_designations, function(index, object) {
            supervisor.append(
              '<option value="' + object.id + '"> [' + object.users.code + '] ' + object.users.name + '</option>'
            );
          });

        });

      }, 'json');
    });

    $("#page-content-wrapper").on("click", ".btn-designate", function() {
      $("#modal-designate").modal('show');
       // reset all form fields
      $("#modal-designate").find('form').trigger("reset");
      $("#designate-id").val("{{ $sales_id }}")
      $("#row-rate").val("")
    });
    
    ///edit banding rates
    // $("#page-content-wrapper").on("click", ".btn-edit-banding", function() {
    //    var id = $(this).parent().parent().data('id');
    //    var btn = $(this);
    //     // reset all form fields
    //     $("#modal-banding-edit").find('form').trigger("reset");
    //     //$(".modal-header").removeAttr("class").addClass("modal-header tbheader");
    //     $("#row-id").val("");
    //     $("#edit-bonding-id").val(id);
    //     $_token = "{{ csrf_token() }}";
        
    //     btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
      
    //           $.post("{{ url('sales/edit/banding') }}", { id: id, _token: $_token }, function(response) {
    //               if(!response.error) {
    //               // set form title
    //                   $(".modal-title").html('<i class="fa fa-pencil"></i> Edit Banding Settings');

    //                   // output form data
    //                   $.each(response, function(index, value) {
    //                       var field = $("#row-" + index);
    //                       // field exists, therefore populate
    //                       if(field.length > 0) {
    //                           field.val(value);
    //                       }
    //                   });
                       
                      
    //                   // show form
    //                   $("#modal-banding-edit").modal('show');
    //               } else {
    //                   status(false, response.error, 'alert-danger');
    //               }

    //               btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
    //           }, 'json');
    // });
         
    $("#modal-form").on("click", ".btn-change-group-code", function() {

      var designation_rank =  "{{ $get_rank->designations->rank}}";
        $("#active_group_code").addClass("hide");
        $("#active_unit_code").addClass("hide");
           
      if(designation_rank == "Supervisor"){
          $("#group_code_pane").removeClass("hide");
          $(".unit_code_undropdown").removeClass("hide");

       }
      else if(designation_rank == "Advisor"){
         $("#group_code_pane").removeClass("hide");
         $(".unit_code_dropdown").removeClass("hide");
        }
      else{
        $("#active_group_code").addClass("hide");
        $("#active_unit_code").addClass("hide");
      }
  
     });
    $("#page-content-wrapper").on("click", ".btn-edit", function() {
      $('#password_div').removeClass('hide');
      $(".pwstrength_viewport_progress div:nth-child(2)").removeClass("progress").html("");
      $(".pwstrength_viewport_progress div:nth-child(3)").removeClass("progress").html("");
      $(".pwstrength_viewport_progress div:nth-child(4)").removeClass("progress").html("");

      $(".pwstrength_viewport_progress .progress-bar").attr('style', 'width: ' + 0 + '%;');
      $(".password-verdict").html("Weak");
      
    var rank = "{{$get_rank->designations->rank}}";
    var members = "{{$get_members}}";
    if(rank == "Advisor"){
       if(members==""){
        
        $("#group_code_pane").removeClass("hide");
        $(".unit_code_dropdown").removeClass("hide");
         $(".unit_code_undropdown").addClass("hide");
       }
    }
  if(rank == "Supervisor"){
       if(members==""){
        
        $("#group_code_pane").removeClass("hide");
        $(".unit_code_dropdown").addClass("hide");
        $(".unit_code_undropdown").removeClass("hide");
       }
    }
    $(".help-info").addClass("hide");
    $(".uploader-pane").addClass("hide");
    $(".system-photo-pane").removeClass("hide");
    $("#row-photo_row").removeClass("hide");
    $(".old_user").removeClass("hide");
       // reset all form fields
    
       var loading = $(".loading-pane");

      $("#modal-form").find('form').trigger("reset");
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      $("#row-user_sales_id").val("");
      

      $("#modal-form").find('.hide-view').removeClass('hide');
      $("#modal-form").find('.show-view').addClass('hide');
      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
      $(".photo_view").removeClass('hide');
      $(".photo_upload").addClass('hide');

      var id = "{{ $sales_id }}";
      $_token = "{{ csrf_token() }}";
      var url = "{{ url('/') }}";
       var btn = $(this);
       btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
      
      $('#row-user_sales_id').val(id);

      $.post("{{ url('sales/' . $sales_id . '/get-info') }}", { id: id, _token: $_token }, function(response) {

        try
        {
          if(response.rows.deactivated_date)
          {
            var deactivated_date = response.rows.deactivated_date.substr(0,10).split('-');
            console.log(deactivated_date);
            $('#row-deactivated').val(deactivated_date[1] + '/' + deactivated_date[2] + '/' + deactivated_date[0]);
          }
          else
          {
            $('#deactivated_form').addClass('hide');
          }
        }
        catch(e)
        {
          console.log(e);
        }

          $(".modal-title").html('<i class="fa fa-eye"></i> Editing <strong>' + response.rows.name + '</strong>');
          // output form data
          //console.log(response);
          $.each(response.rows, function(index, value) {

              console.log(index);
              var field = $("#row-" + index);
              //console.log(response.rows);
              //console.log(response);
              if (index == "photo") {
                 // $("#row-photo_row").attr('src', url + '/uploads/' + value);
                  $(".system-photo-pane").html("<img src='{{ url('uploads')."/"}}"+value+"' class='preview img-circle' height='100' width='100' id = 'row-photo_row' style='margin:5px;'/>");
                 $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
                 
              }
            
                $("#row-password").attr("placeholder","Leave blank for unchanged");
                $("#row-password_confirmation").attr("placeholder","Leave blank for unchanged");

              if (index == "sales_info") {

                $.each(value, function(sindex, svalue) {
                  //console.log(sindex);
                    if(sindex =="designation_id"){
                       $("#row-usertype_id").val(svalue);
                    }
                    var sfield = $("#row-" + sindex);
                    if (sfield.length > 0) {
                      sfield.val(svalue);
                    }

                    if(sindex=="get_designation_rate"){
                       $.each(svalue, function(xindex, xvalue) {
                          //console.log(xindex);
                          var sfield = $("#row-" + xindex);
                            if (sfield.parent().hasClass("date")) {
                              $("#date-" +"designation_"+xindex).data("DateTimePicker").date(moment.utc(svalue.effective_date));
                            }
                            $("#row-designation_rate").val(svalue.rate);
                        });
                    }
                    if(sindex=="get_bandinginfo"){
                       $.each(svalue, function(xindex, xvalue) {
                          //console.log(xindex);
                          var sfield = $("#row-" + xindex);
                            if (sfield.parent().hasClass("date")) {
                              $("#date-" +"banding_"+xindex).data("DateTimePicker").date(moment.utc(svalue.effective_date));
                            }
                        });
                    }
                  });
                
                $('#row-designation').val(value.designations.designation);
              }
              
              if(index == "get_sales_user_info"){

                   $.each(value, function(sindex, svalue) {
                    var sfield = $("#row-" + sindex);
                           if (sfield.parent().hasClass("date")) {
                              $("#date-" + sindex).data("DateTimePicker").date(moment.utc(svalue));
                            }
                         else {
                            if (sfield.length > 0) {
                               sfield.val(svalue);

                             }
                        }
                  });

              }
                console.log(index);
              if(index == "get_provider_codes"){
                     $.each(value, function(sindex, svalue) {

                           $.each(svalue, function(nindex, nvalue) {
                                    $("#row-provider_code_"+svalue.classification_id).val(svalue.provider_code);
                                    $("#row-provider_id_"+svalue.provider_id).val(svalue.provider_id);
                                    $("#row-classification_id"+svalue.classification_id).val(svalue.classification_id);
                          });   
                          
                            
                      });    

              }
              
              if(index == "get_advisor_info"){

                if(value == null){
                     var designation_rank =  "{{ $get_rank->designations->rank}}";
                        $("#active_group_code").addClass("hide");
                        $("#active_unit_code").addClass("hide");
                           
                      if(designation_rank == "Supervisor"){
                          $("#group_code_pane").removeClass("hide");
                          $(".unit_code_undropdown").removeClass("hide");
                       }
                      else if(designation_rank == "Advisor"){
                         $("#group_code_pane").removeClass("hide");
                         $(".unit_code_dropdown").removeClass("hide");
                        }
                      else{
                        $("#active_group_code").addClass("hide");
                        $("#active_unit_code").addClass("hide");
                      }
                    }else{  

                    $.each(value, function(sindex, svalue) {


                        //console.log(sindex);

                        if(sindex == "get_supervisor_info"){
                             $.each(svalue, function(xindex, xvalue) {

                                if(xindex == "group_info"){

                                   $.each(xvalue, function(yindex , yvalue) {

                                       if(yindex == "sales_supervisor"){
                                              $(".unit_code_undropdown").addClass("hide");
                                              $("#row-unit_id").html("");
                                           $.each(yvalue, function(zindex, zvalue) {
                                            $("#row-unit_id").append(
                                                '<option value="' + zvalue.id + '">' + zvalue.unit_code + "</option>"
                                              );
                                             }); 
                                       }
                                    }); 

                                }
                              }); 

                        $("#row-group_code").val(svalue.group_id);
                         $("#row-unit_id").val(svalue.id);
                        }
                      // if(sindex=="get_advisors"){
                          
                      //    $.each(svalue, function(xindex, xvalue) {

                      //       if(xindex == "id"){
                      //           //$("#row-unit_id").val(xvalue);
                      //            //$("#row-unit_id[id='"+xvalue+"']").prop('selected', true);
                      //       }  
                      //       // if(xindex=="group_info"){
                                
                      //       //    $.each(xvalue, function(gindex, gvalue) {
                                  
                      //       //      if(gindex == "id"){ 
                      //       //           //console.log(gvalue.id);
                      //       //               //$("#row-group_code").val(gvalue);
                      //       //               //$("#row-group_code[value='"+gvalue+"']").change('selected', true);
                      //       //           }
            
                      //       //    }); 
                      //       // }
                      //   });
                      // }
                  });

                   

                  }                
              }
              if(index == "get_provider_codes"){
                     $.each(value, function(sindex, svalue) {

                           $.each(svalue, function(nindex, nvalue) {
                                if(nindex == "provider_code"){
                                    
                                    
                                }  
                          });   
                          
                            
                      });    

              }
              if(index == "get_supervisor_info"){
                  if(value == null){
                     var designation_rank =  "{{ $get_rank->designations->rank}}";
                        $("#active_group_code").addClass("hide");
                        $("#active_unit_code").addClass("hide");
                           
                      if(designation_rank == "Supervisor"){
                          $("#group_code_pane").removeClass("hide");
                          $(".unit_code_undropdown").removeClass("hide");
                       }
                      else if(designation_rank == "Advisor"){
                         $("#group_code_pane").removeClass("hide");
                         $(".unit_code_dropdown").removeClass("hide");
                        }
                      else{
                        $("#active_group_code").addClass("hide");
                        $("#active_unit_code").addClass("hide");
                      }
                    }else{
                    $.each(value, function(sindex, svalue) {
                        console.log(sindex+"="+svalue);

                       if(sindex == "unit_code"){

                               $("#row-unit_codename").val(svalue);
                               $("#row-unit_code_final").val(value.unit_code);
                               $(".unit_code_dropdown").addClass("hide");
                            } 
                          console.log(sindex);
                          if(sindex == "group_info"){
                              
                                 $.each(svalue, function(nindex, nvalue) { 

                                     if(nindex == "code"){ 

                                           //console.log("groud_id "+value.group_info.id);
                                          $("#row-group_code").val(value.group_info.id);
                                      }
                               });  
                            //$("#row-unit_code_final").val(svalue)
                          }
                      });
                    }  

              }
          
              // field exists, therefore populate
              if(field.length > 0) {
                  field.val(value);
              }


            //$("#image_pane_edit_photo").addClass("hide");
            // $(".modal-title").html("<i class='fa fa-plus'></i> Add Sales Users");
            $('.nav-tabs a[href="#basic-info"]').tab('show');
            $("#modal-form").modal('show');  
          });
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil"); 
          loading.addClass("hide");   
      }, 'json');
    });

    $("#btn-change_photo").on("click", function() {
      $(".photo_upload").removeClass("hide");
      $(".photo_view").addClass("hide");
      $("#image_pane_edit_photo").addClass("hide");
      $("#image_pane").addClass("hide");

    });
     $("#new_banding_rate").on("change", function() {
        var id = $("#new_banding_rate").val();
         $_token = "{{ csrf_token() }}";
         $.post("{{ url('sales/get-tiers') }}", { id: id, _token: $_token }, function(response) {
          $.each(response, function(index, row) {
            $("#row-bonding_rate_new").val(row.rate);         
          });
        }, 'json');

      });
    $("#btn-cancel_photo").on("click", function() {
      $(".photo_upload").addClass("hide");
      $(".photo_view").removeClass("hide");
    });
     $('.btn-add-director-code').on('click', function() {
       window.open('{{ url("sales/team-management#new") }}','_blank');
       $(".btn-create-group").trigger("click");
    });
    // Disable Current User
    $('.btn-disable').on('click', function() {
      var id = {{ $sales_id }};

      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");

      dialog('Account Disable', 'Are you sure you want to disable <strong>{{ $row->name }}</strong>?', "{{ url('sales/disable') }}", id);

    });

    // Remove Current User
    $('.btn-remove').on('click', function() {

      $('#modal-remove').modal('show');
      $('.modal-remove-title').html('Account Remove');
      $('.modal-remove-body').html('Are you sure you want to disable <strong>{{ $row->name }}</strong>?');
      
    });

    $("#row-usertype_id").on("change", function() { 
        var id = $("#row-usertype_id").val();

        $_token = "{{ csrf_token() }}";

        $.post("{{ url('sales/get-groups') }}", { id: id, _token: $_token }, function(response) {

          if(response.get_rank.rank=="Supervisor") {
            $(".unit_code_undropdown").removeClass("hide");
            $(".unit_code_dropdown").addClass("hide");
          }
          if(response.get_rank.rank=="Advisor") {
            $(".unit_code_dropdown").removeClass("hide");
            $(".unit_code_undropdown").addClass("hide");
          }
          $("#row-group_code").removeAttr('disabled','disabled');
          $("#row-unit_id").removeAttr('disabled','disabled');
          $("#row-unit_id").html("");
          var group = $('#row-group_code');
          group.html('<option value="" class="hide">Select:</option>');
          $.each(response.get_group, function(index, row) {
            group.append('<option value="' + row.id + '">' + row.code + '</option>');
          });


        }, 'json');

        $.post("{{ url('sales/get-designation_rate') }}", { id: id, _token: $_token }, function(response) {

      
          $.each(response.get_rate, function(index, row) {
            $("#row-designation_rate").val(response.get_rate.rate);
          });


        }, 'json');



      });
    //  Remove confirm
    $('#modal-remove-button').on('click', function() {
      var id = {{ $sales_id }};
      $_token = "{{ csrf_token() }}";

      $.post("{{ url('sales/remove') }}", { id: id, _token: $_token }, function(response) {
          status(response.title, response.body, 'alert-success');
          window.location = "{{ url('user-management/sales-users') }}";
      });

    });
     //datatable
        myTable = $('#my-table').dataTable({
              "aoColumnDefs": [{ 
                'bSortable': false, 
                'aTargets': [0],
              }],
              "dom": '<"top"i>rt<"bottom"flp><"clear">'
        });
        myTable = $('#my-table2').dataTable({
              "aoColumnDefs": [{ 
                'bSortable': false, 
                'aTargets': [0],
              }],
              "dom": '<"top"i>rt<"bottom"flp><"clear">'
        });
         myTable = $('#my-table3').dataTable({
              "aoColumnDefs": [{ 
                'bSortable': false, 
                'aTargets': [0],
              }],
              "dom": '<"top"i>rt<"bottom"flp><"clear">'
        });

});
   $('#modal-form').on('click', '#edit-change-photo', function() {
            //console.log("checked");
          if ($(this).text() == 'CHANGE') {
              //$(this).text('Cancel');
              $('#row-photo_row').removeClass('hide');
              $(".change").removeClass('hide');
              $("#image_pane_edit_photo").addClass("hide");
              $(".uploader-pane").removeClass("hide");
              $(".system-photo-pane").addClass("hide");

            
          // } else {

          //     $(this).text("CHANGE");
          //     $('#row-photo_row').removeClass('hide');
          //     $(".uploader-pane").addClass("hide");
          //     $(".system-photo-pane").removeClass("hide");

          //     // Remove selected photo input.
          //     $('.change').find('.fileinput-remove').click();
          //     $(".change").addClass('hide');
          }
      });
      $('#btn-cancel-photos').on('click', function() {
              $(".change").addClass('hide');
              $(".uploader-pane").addClass("hide");
              $(".system-photo-pane").removeClass("hide");
      });
  
  $("#row-banding_rank").on("change", function() { 
        var id = $("#row-banding_rank").val();
     
         $_token = "{{ csrf_token() }}";
         $.post("{{ url('sales/get-tiers') }}", { id: id, _token: $_token }, function(response) {
          $.each(response, function(index, row) {
            $("#row-bonding_rate").val(row.rate);         
          });
        }, 'json');

      }); 
  $("#row-group_code").on("change", function() { 
    var id = $("#row-group_code").val();
    $_token = "{{ csrf_token() }}";
    var unit = $("#row-unit_id");
    unit.html("");
    console.log("yes");
    $.post("{{ url('sales/get-units') }}", { id: id, _token: $_token }, function(response) {
      unit.append(response);
   }, 'json');

 });
$('#file').onclick = function() {
  $('#my_file').click();
};

$('input[type=file]').change(function (e) {
    $('#customfileupload').html($(this).val());
});

</script>
<script type="text/javascript">
    function ChangeText(oFileInput, sTargetID) {
    document.getElementById(sTargetID).value = oFileInput.value;
}
</script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<div class="container-fluid">
  <div class="loading-pane hide">
    <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  </div>
  <!-- Title -->
  <div class="row main-title-container container-space">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <h3 class="main-title">SALES USER PROFILE</h3>
          <h5 class="bread-crumb">USER MANAGEMENT</h5>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
            <a href="{{ url('user-management/sales-users') }}" type="button" class="btn btn-success borderzero btn-sm btn-tool pull-right"  id="back"><i class="fa fa-chevron-left"></i> BACK</a>  
            <button type="" href="" class="btn btn-yeah borderzero btn-sm btn-tool pull-right btn-remove"><i class="fa fa-minus"></i> Remove</button>
            <a href="javascript:void(0)" type="button" class="btn btn-success borderzero btn-sm btn-tool pull-right" style="display:none" id="cancel"><i class="fa fa-undo"></i> CANCEL</a>
            <button type="" href="" class="btn btn-info borderzero btn-sm btn-tool pull-right btn-disable"><i class="fa fa-adjust"></i> Disable</button>  
            <button type="{{ url('user-management/sales-users'.$row->id.'edit') }}" href="" class="btn btn-warning borderzero btn-sm btn-tool pull-right btn-edit"><i class="fa fa-pencil"></i> Edit</button>
            <button type="submit" class="btn btn-warning borderzero btn-sm btn-tool pull-right" style="display:none" id="save" form="form-save_sales"><i class="fa fa-save"></i> SAVE</button>
      </div>
      </div>
      <div id="form-sales-info" class="row default-container container-space">
        <div class="col-lg-12">
            <p><strong>BASIC</strong></p>
        </div>
            <div class="content-table text-center col-lg-2 col-md-2 col-sm-12 col-xs-12">
              <img src="{{ url('uploads/' . $row->photo) }}" class="img-responsive img-circle" alt="Generic placeholder thumbnail" id="img-photo">
            </div>
            <div class="content-table row col-lg-5 col-md-5 col-sm-5 col-xs-12">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">AGENT ID</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-sales_id">{{ $row->salesInfo->sales_id }}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">AGENT CODE</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-code">{{ $row->code }}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">AGENT NAME</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">   
                      <span id="label-name">{{ $row->name }}</span>
                    </div>
                </div>
              </div>
            </div>
            <div class="content-table row col-lg-5 col-md-5 col-sm-5 col-xs-12">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">STATUS</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      @if($row->status == 1) 
                        <span id="label-static-status">ACTIVE</span>
                      @elseif($row->status == 0)
                        <span id="label-static-status">DISABLED</span>
                      @else
                        <span id="label-static-status">REMOVED</span>
                      @endif
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">DESIGNATION</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-designation">{{ $row->salesInfo->designations->designation }}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">EMAIL</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-email">{{ $row->email}}</span>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-12 control-label" style="text-align:left">CONTACT</label>
                    <div class="col-lg-8 col-md-6 col-sm-8 col-xs-12">
                      <span id="label-mobile">{{ $row->mobile }}</span>
                    </div>
                </div>
              </div>
            </div>
          </div>


<section class="content-table container-fluid">
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-6 col-xs-6">
  <p><strong>DESIGNATION</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#designation" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="designation" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
  <div class="content-table table-responsive container-fluid" >   
  <table class="table table-striped">
  <thead class="tbheader">
    <tr>
      <th><i class="fa fa-calendar"></i> DATE MODIFIED</th>
      <th><i class="fa fa-sort"></i> MODIFIED BY</th>
      <th><i class="fa fa-sort"></i> PREVIOUS DESIGNATION</th>
      <th><i class="fa fa-sort"></i> NEW DESIGNATION</th>
      <th><i class="fa fa-sort"></i> RATE</th>
      <th class="rightalign"><i class="fa fa-calendar"></i> EFFECTIVE DATE</th>
    </tr>
  </thead>
  <tbody id="info_designations">
  @foreach($info_designations as $row)
    <tr>
      <td>{{ date_format(new DateTime($row->created_at), 'm-d-Y') }}</td>
      <td>{{ $row->modify->name }}</td>
      <td>{{ $row->prevDesignation->designation }}</td>
      <td>{{ $row->newDesignation->designation }}</td>
      <td>{{ $row->rate }}%</td>
      <td class="hide" id="user_rank">{{ $row->newDesignation->rank }}</td>
      <td class="rightalign">{{ date_format(new DateTime($row->effective_date), 'm-d-Y') }}</td>
    </tr>
  @endforeach
  </tbody>
</table>
<button type="" href="" class="btn btn-primary borderzero btn-sm btn-tool pull-right btn-designate"><i class="fa fa-map-marker"></i> Designate</button>   
  </div>
</div>
</div>
</section>
<section class="content-table container-fluid">
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-6 col-xs-6">
  <p><strong>TEAM</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#team" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="team" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">

  <div class="content-table table-responsive container-fluid" >    
  <table class="table table-striped">
    @if($get_members!=null)
      @foreach($info_designations as $rows)@endforeach
      @if($rows->newDesignation->rank == "Supervisor")
           <caption><h5><span class="label label-warning">{{$row->group_id}}</span>&nbsp;<span class="label label-info">MEMBERS</span></h5></caption>
      @elseif($rows->newDesignation->rank == "Advisor")
           <caption><h5><span class="label label-warning">{{$row->group_id}}</span>&nbsp;<span class="label label-success">SUPERVISOR</span></h5></caption>
      @endif
    @else


    @endif
  <thead class="tbheader">
    <tr>
      @foreach($info_designations as $rows)@endforeach
      @if($rows->newDesignation->rank == "Supervisor")
        <th><i class="fa fa-calendar"></i> CODE</th>
        <th><i class="fa fa-calendar"></i> NAME</th>
        <th><i class="fa fa-calendar"></i> DESIGNATION</th>
        <th><i class="fa fa-calendar"></i> EMAIL</th>
        <th><i class="fa fa-calendar"></i> MOBILE</th>
      @elseif($rows->newDesignation->rank == "Advisor")
        <th><i class="fa fa-calendar"></i> CODE</th>
        <th><i class="fa fa-calendar"></i> NAME</th>
        <th><i class="fa fa-calendar"></i> EMAIL</th>
        <th><i class="fa fa-calendar"></i> MOBILE</th>
        <th class="rightalign"><i class="fa fa-calendar"></i> DESIGNATION</th>
      @endif

    </tr>
  </thead>
  <tbody id="info_teams">
      @foreach($info_designations as $rows)@endforeach
    @if($rows->newDesignation->rank == "Supervisor")
      @if($get_members!=null)

        @foreach($get_members as $rows)
            <tr>
              <td>{{$rows->advisorInfo->code}}</td>
              <td>{{$rows->advisorInfo->name}}</td>
              <td>{{$rows->advisorInfo->salesInfo->designations->designation}}</td>
              <td>{{$rows->advisorInfo->email}}</td>
              <td>{{$rows->advisorInfo->mobile}}</td>
            </tr>
        @endforeach
      @else
           <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><span class="label label-danger">USER HAS NO TEAM YET</span></td>
              <td>&nbsp;</td>
           </tr>
         
      @endif
       @if(count($get_members)==0)
           <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><span class="label label-danger">USER HAS NO MEMBERS</span></td>
              <td>&nbsp;</td>
           </tr>
       @endif
    @elseif($rows->newDesignation->rank == "Advisor")
      @if($get_members!=null)
      
        @foreach($get_members as $rows)
            <tr>
              <td>{{$rows->supervisorinfo->code}}</td>
              <td>{{$rows->supervisorinfo->name}}</td>
              <td>{{$rows->supervisorinfo->email}}</td>
              <td>{{$rows->supervisorinfo->mobile}}</td>
              <td class="rightalign">{{$rows->supervisorinfo->salesInfo->designations->designation}}</td>
            </tr>
        @endforeach
      @else
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><span class="label label-danger">USER HAS NO TEAM YET</span></td>
              <td>&nbsp;</td>
            </tr>
      @endif
    @endif

  <!-- @if($info_teams == null)
    <tr>
      <td colspan="5"><center>SALES HAS NO TEAM.</center></td>
    </tr>
  @else

    @if ($check == 2)
      @if ($info_teams == 'none')
        <tr>
          <td colspan="5"><center>SALES HAS NO MEMBERS.</center></td>
        </tr>
      @else
        @foreach($info_teams as $row)
        <tr>
          <td>{{ date_format(new DateTime($row->updated_at), 'm-d-Y') }}</td>
          <td>{{ $row->teamSales->getAdvisor->teamModified->name }}</td>
          <td>{{ $row->teamID->team_code }}</td>
          <td>{{ $row->teamSales->users->code }}</td>
          <td class="rightalign">{{ $row->teamSales->users->name }}</td>
        </tr>
        @endforeach
        @endif
    @else
      @foreach($info_teams as $row)
      <tr>
        <td>{{ date_format(new DateTime($row->created_at), 'm-d-Y') }}</td>
        <td>{{ $row->teamModified->name }}</td>
        @if ($check == 1)
        <td>{{ $row->salesTeamInfo->salesSupervisor->teamID->team_code }}</td>
        @elseif ($check == 0)
        <td>none</td>
        @endif
        <td>{{ $row->getSupervisor->sales_id }}</td>
        <td class="rightalign">{{ $row->getSupervisor->users->name }}</td>
      </tr>
       @endforeach
      @endif  
    @endif  -->
  </tbody>
</table>



<!-- @if ($check != 2)
<button type="" href="" class="btn btn-success borderzero btn-sm btn-tool pull-right btn-team"><i class="fa fa-group"></i> Supervisor</button>   
@endif -->
</div>
</div>
</div>
</section>
<section class="content-table container-fluid">
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-6 col-xs-6">
  <p><strong>BANDING</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#bonding" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="bonding" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
<div class="content-table table-responsive container-fluid">
    <table class="table table-striped">
    <thead class="tbheader">
      <tr>
      <th><i class="fa fa-calendar"></i> DATE MODIFIED</th>
      <th><i class="fa fa-sort"></i> MODIFIED BY</th>
      <th ><i class="fa fa-sort"></i> BANDING RATE</th>
      <th class="rightalign"><i class="fa fa-sort"></i> EFFECTIVE DATE</th>
      </tr>
    </thead>
    <tbody id="info_bondings">
      @foreach($info_bondings as $row)
        <tr data-id="{{$row->id}}">
          <td>{{ date_format(new DateTime($row->created_at), 'm-d-Y') }}</td>
          <td>{{ $row->bondingModify->name }}</td>
          <td>{{ $row->bonding_rate }}%</td>
          <td class="rightalign">{{ $row->effective_date }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
  <button type="" href="" class="btn btn-danger borderzero btn-sm btn-tool pull-right btn-bonding"><i class="fa fa-cog"></i> Banding Setting</button> 
</div>
</div><!-- test comment -->
</div>
</section>
<section class="content-table container-fluid">
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-6 col-xs-6">
  <p><strong>POLICY</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#policy" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="policy" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
  <div class="content-table table-responsive container-fluid" >     
  <table class="table table-striped">
    <thead class="tbheader">
      <tr>
        <th><i class="fa fa-sort"></i> POLICY CODE</th>
        <th><i class="fa fa-sort"></i> PROVIDER</th>
        <th><i class="fa fa-sort"></i> POLICY CATEGORY</th>
        <th><i class="fa fa-sort"></i> POLICY TYPE</th>
        <th><i class="fa fa-sort"></i> INCEPT DATE</th>
      </tr>
    </thead>
    <tbody id="info_policies">
      @foreach($info_policies as $row)
      <tr>
        <td>{{ $row->contract_no }}</td>
        <td>{{ $row->getPolicyProvider->name }}</td>
        <td>{{ $row->getPolicyCategoryInfo->name}}</td>
        <td>{{ $row->policy_type }}</td>
        <td>{{ date_format(new DateTime($row->incept_date), 'm-d-Y') }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
</div>
</section>
<section class="content-table container-fluid">
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-6 col-xs-6">
  <p><strong>PRE-PAYROLL</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#payroll" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="payroll" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
 <div class="content-table table-responsive container-fluid" >     
  <table class="table table-striped">
    <thead class="tbheader">
      <tr>
        <th><i class="fa fa-sort"></i> BATCH NAME</th>
        <th><i class="fa fa-sort"></i> TYPE</th>
        <th><i class="fa fa-sort"></i> REMARKS</th>
        <th><i class="fa fa-sort"></i> COST</th>
      </tr>
    </thead>
    <tbody id="info_policies">
      @foreach($pre_payroll_info as $row)
      <tr>
        <td>{{$row->getPrePayrollBatch->name}}</td>
        <td>{{ $row->type}}</td>
        <td>{{ $row->remarks}}</td>
        <td>{{ $row->cost }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
 </div>
</div>
</div>
</section>
<!-- 
<section class="content-table container-fluid">
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-6 col-xs-6">
  <p><strong>TAX INFORMATION</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#tax" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="tax" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
<div class="content-table table-responsive container-fluid" >     
  <table class="table table-striped">
    <thead class="tbheader">
      <tr>
        <th><i class="fa fa-calendar"></i> MONTH</th>
        <th class="rightalign">TOOLS</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>January 2015</td>
        <td class="rightalign">
          <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalview"><i class="fa fa-eye"></i></a>
          <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaldownload">DOWNLOAD</a>
        </td>
      </tr>
      <tr>
        <td>February 2015</td>
        <td class="rightalign">
          <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalview"><i class="fa fa-eye"></i></a>
          <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaldownload">DOWNLOAD</a>
        </td>
      </tr>
      <tr>
        <td>March 2015</td>
        <td class="rightalign">
          <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModalview"><i class="fa fa-eye"></i></a>
          <a type="button" class="btn-color btn btn-xs btn-default" data-toggle="modal" data-target="#myModaldownload">DOWNLOAD</a>
        </td>
      </tr>
    </tbody>
  </table>
</div>
</div>
</div>
</section>
-->
<section class="content-table container-fluid">
<div id="basicinfo" class="row default-container container-space">
<div class="col-lg-6 col-xs-6">
  <p><strong>INTRODUCERS</strong></p>
</div>
<div class="col-lg-6 col-xs-6" style="padding-left:0;">
  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#introducers" aria-expanded="false" aria-controls="collapseTwo"></i>
</div>
<div id="introducers" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
  <div class="content-table table-responsive container-fluid" >      
  <table class="table table-striped">
    <thead class="tbheader">
      <tr>
        <th><i class="fa fa-sort"></i> NAME</th>
        <th><i class="fa fa-sort"></i> CODE</th>
        <th><i class="fa fa-sort"></i> EMAIL</th>
        <th><i class="fa fa-sort"></i> ZIP CODE</th>
      </tr>
    </thead>
    <tbody id="intro_info">
      @foreach($intro_info as $row)
      <tr>
        <td>{{ $row->code }}</td>
        <td>{{ $row->name }}</td>
        <td>{{ $row->email}}</td>
        <td>{{ $row->zipcode }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
</div>
</section>
</div>
</div>

@include('user-management.sales-users.form')

@stop