<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
       <div class="modal-content borderzero">
           <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
           </div>
          {!! Form::open(array('url' => 'sales/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
           <div class="modal-header tbheader">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
           </div>
           <div class="modal-body">
            <div id="form-notice"></div>
            <ul class="nav nav-tabs nav-justified">
             <li class="active"><a data-toggle="tab" href="#basic-info" title="Basic Information"><i class="fa fa-user"></i> <span class="visible-xs">Basic Information</span></a></li>
            <!--  <li><a data-toggle="tab" href="#additional-info" title="Addition Information"><i class="fa fa-plus-square"></i></a></li> -->
             <!--<li><a data-toggle="tab" href="#designation-info" title="Designation Information"><i class="fa fa-exchange"></i></a></li>-->
            <!--  <li><a data-toggle="tab" href="#team-info" title="Group Management"><i class="fa fa-flag-checkered"></i></a></li> -->
             <!-- <li><a data-toggle="tab" href="#banding-info" title="Banding Information"><i class="fa fa-chain-broken"></i></a></li>
             <li><a data-toggle="tab" href="#policy-info" title="Policy Information"><i class="fa fa-th-list"></i></a></li>
             -->
             <li><a data-toggle="tab" href="#contact-info" title="Contact Information"><i class="fa fa-phone"></i> <span class="visible-xs">Contact Information</span></a></li>
             <li><a data-toggle="tab" href="#company-info" title="Company Information"><i class="fa fa-briefcase"></i> <span class="visible-xs">Company Information</span></a></li>
             <li><a data-toggle="tab" href="#extra-info" title="Extra Information"><i class="fa fa-info"></i> <span class="visible-xs">Extra Information</span></a></li>
             <li><a data-toggle="tab" href="#provider-info" title="Provider Codes"><i class="fa fa-code-fork"></i> <span class="visible-xs">Provider Code</span></a></li>
      <!--   <li><a data-toggle="tab" href="#sales-force-info" title="Sales Force Information"><i class="fa fa-line-chart"></i></a></li>
            
             <li><a data-toggle="tab" href="#jumpsite-info" title="Jumpsite Information"><i class="fa fa-location-arrow"></i></a></li>
             <li><a data-toggle="tab" href="#security-info" title="Security Information"><i class="fa fa-key"></i></a></li>
             <li><a data-toggle="tab" href="#dialer-info" title="Dialer Information"><i class="fa fa-tty"></i></a></li>
             <li><a data-toggle="tab" href="#iserver-info" title="IServer Information"><i class="fa fa-server"></i></a></li>
             <li><a data-toggle="tab" href="#misc-info" title="Miscellaneous Information"><i class="fa fa-medium"></i></a></li> -->
           <!--   <li><a data-toggle="tab" href="#other-info" title="Other Information"><i class="fa fa-medium"></i></a></li> -->
            </ul>
            <div class="tab-content">
            <div id="basic-info" class="tab-pane fade in active"><!-- BASIC INFORMATION-->
            <h3>LOGIN INFORMATION</h3>
            <div class="form-group">
              <label for="row-email" class="col-sm-3 control-label">*Email</label>
            <div class="col-sm-9 error-email ">
              <input type="text" name="email" class="form-control borderzero" id="row-email" maxlength="100" placeholder="Enter your email address...">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="hide" id="password_div">
            <div class="form-group hide-view">
                <label for="row-password" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-9 error-password ">
              <input type="password" name="password" class="form-control borderzero pwd-password" id="row-password" maxlength="30" placeholder="Enter your password...">
              <sup class="sup-errors"></sup>
            </div>
            <div class="col-sm-9 col-sm-offset-3" id="pwd-container">
            <input type="hidden" class="total-score" name="pass_score">
            <div class="pwstrength_viewport_progress"></div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 error-pass_score">
            <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group hide-view">
              <label for="row-password_confirmation" class="col-sm-3 control-label">Confirm Password</label>
            <div class="col-sm-9">
              <input type="password" name="password_confirmation" class="form-control borderzero" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password...">
            </div>
            </div>
            </div>
            <hr></hr>
            <h3>BASIC INFORMATION</h3>
             <div class="form-group system-photo-div">
                     <label for="row-photo" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                     <div class="col-sm-9 col-xs-7 system-photo-pane">
                     <button type="button" class="btn btn-primary borderzero hide photo_view" id="edit-change-photo">CHANGE</button>
                     <img id="row-photo_row" height="100" width="100" class='img-circle image-responsive'>
                    

                     </div>
                    <input type="hidden" id="image_fb_link_edit_photo" name="image_fb_link_edit_photo">
                     <div class="col-sm-9 col-xs-7 uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
                        <input name='photo' id='row-photo_upload' type='file' class='file borderzero file_photo' data-show-upload='false' placeholder='Upload a photo...'>
                        <button type="button" class="btn btn-primary borderzero" id="btn-cancel-photos">Cancel</button>
                      </div>
                    </div>
             </div>
           <!-- <div class="form-group default-photo-pane">
              <label for="row-photo" class="col-sm-3 control-label">Photo</label>
              <div class="col-sm-3">
                  <div style="margin-bottom:2px;" id="image_pane">
                  </div>
                  <input type="hidden" id="image_fb_link" name="image_link">
                  <img id="standard_photo" class="image-responsive img-circle" src="{{ url('uploads')."/".$settings->default_photo }}" height="100" width="100" style="margin: 5px;">
                  <div class="button-pane">
                  </div>
              </div>
            </div>
            <button type="button" id="btn-custom-photo" class="borderzero btn btn-primary btn-sm">Change</button>
           <div class="form-group hide-view uploader hide">
            <label for="row-photo" class="col-sm-3 control-label">Photo</label>
            <div class="col-sm-9">
              <div style="margin-bottom:2px;" id="image_pane_edit_photo"></div>
              
              <img src="..." class="img-responsive img-circle hide photo_view" height="100" width="100" alt="Generic placeholder thumbnail" id="row-photo">
              <button type="button" class="btn btn-primary borderzero hide photo_view" id="btn-change_photo">Change</button>
              <div class="photo_upload">
                <button type="button" class="btn btn-primary borderzero hide" id="btn-cancel_photo">Cancel</button> 
                <input name="photo" id="row-photo_upload" type="file" class="file borderzero file_photo" data-show-upload="false" placeholder="Upload a photo...">
              </div>
            </div>

            </div> -->
            @if ($type_id == 'none')
           
            <div class="form-group">
              <label for="row-sales_id" class="col-sm-3 control-label">Agent ID</label>
            <div class="col-sm-9">
              <input type="text" name="sales_id" class="form-control borderzero" id="row-sales_id" maxlength="30" placeholder="Enter your agent code" disabled="disabled">
            </div>
            </div>
            @endif

            <div class="form-group">
              <label for="row-code" class="col-sm-3 control-label">*Agent Code</label>
            <div class="col-sm-9 error-code ">
              <input type="text" name="code" class="form-control borderzero" id="row-code" maxlength="30" placeholder="Enter your agent code">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-nric" class="col-sm-3 control-label">*NRIC</label>
            <div class="col-sm-9 error-nric">
              <input type="text" name="nric" class="form-control borderzero" id="row-nric" maxlength="30" placeholder="Enter your nric">
              <sup class=" sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-salesforce_id" class="col-sm-3 control-label">*Salesforce ID</label>
            <div class="col-sm-9 error-salesforce_id ">
              <input type="text" name="salesforce_id" class="form-control borderzero" id="row-salesforce_id" maxlength="30" placeholder="Enter your salesforce id">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-salutation" class="col-sm-3 control-label">Salutation</label>
            <div class="col-sm-9">
              <select class="form-control" name="salutation" id="row-salutation">
                <option value="" class="hide">Select:</option>
                <option value="Mr">Mr</option>
                <option value="Ms">Ms</option>
                <option value="Mrs">Mrs</option>
              </select>
            </div>
            </div>
            <!-- <div class="form-group">
              <label for="row-account_id" class="col-sm-3 control-label">Account ID</label>
            <div class="col-sm-9">
              <input type="text" name="account_id" class="form-control borderzero" id="row-account_id" maxlength="30" placeholder="Enter your account id..">
            </div>
            </div> -->
            
           
           
            <!-- <div class="form-group select-supervisor">
              <label for="row-supervisor_id" class="col-sm-3 control-label">Supervisor</label>
            <div class="col-sm-9">
              <select name="supervisor_id" id="row-supervisor_id" class="form-control input-md borderzero" style="width: 100%;">
               
              </select>
            </div>
            </div> -->
            
            <!-- <div class="form-group">
                <label for="row-bonding_rate" class="col-sm-3 control-label">Banding Rate</label>
            <div class="col-sm-9">
                
            </div>
            </div> -->
            <!-- <div class="form-group">
                <label for="row-mobile" class="col-sm-3 control-label">Contact Number</label>
            <div class="col-sm-9">
                <input type="text" name="mobile" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
            </div>
            </div> -->
            <!-- <div class="form-group"> 
              <label for="row-migrant" class="col-sm-3 control-label">Migrant</label>
            <div class="col-sm-9">
                <select class="form-control" id="row-migrant" name="migrant">
                  <option value="" class="hide">Select:</option>
                  <option value="y">Yes</option>
                  <option value="n">No</option>
                </select>
            </div>
            </div> -->
            <div class="form-group">
              <label for="row-name" class="col-sm-3 control-label">*Full Name</label>
            <div class="col-sm-9 error-name ">
              <input type="text" name="name" class="form-control borderzero" id="row-name" maxlength="30" placeholder="Enter your full name">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-preferred_name" class="col-sm-3 control-label">*Preferred Name</label>
            <div class="col-sm-9 error-preferred_name ">
              <input type="text" name="preferred_name" class="form-control borderzero" id="row-preferred_name" maxlength="30" placeholder="Enter your preferred name">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-gender" class="col-sm-3 control-label">*Gender</label>
            <div class="col-sm-9 error-gender ">
              <select class="form-control" name="gender" id="row-gender">
                <option value="" class="hide">Select:</option>
                <option value="m">Male</option>
                <option value="f">Female</option>
              </select>
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
                <label for="row-date_of_birth" class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9 error-date_of_birth">
                    <div class="input-group date" id="date-date_of_birth">
                      <input type="text" name="date_of_birth" class="form-control borderzero" id="row-date_of_birth" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                      <sup class=" sup-errors"></sup>
                    </div>
                </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 control-label">Mobile No</label>
            <div class="col-sm-9">
              <input type="text" name="mobile" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your mobile no">
            </div>
            </div>
           <!--  <div class="form-group">
              <label for="row-marital_status" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <select class="form-control" id="row-marital_status" name="marital_status">
                <option value="" class="hide">Select:</option>
                <option value="1">Active</option>
                <option value="2">Inactive</option>
              </select>
            </div>
            </div> -->
          <!--   <div class="form-group">
              <label for="row-race" class="col-sm-3 control-label">Race</label>
            <div class="col-sm-9">
              <input type="text" name="race" class="form-control borderzero" id="row-race" maxlength="30" placeholder="Enter your race...">
            </div>
            </div> -->
            <!-- <div class="form-group">
              <label for="row-nationality" class="col-sm-3 control-label">Nationality</label>
            <div class="col-sm-9">
              <input type="text" name="nationality" class="form-control borderzero" id="row-nationality" maxlength="30" placeholder="Enter your nationality...">
            </div>
            </div> -->
            <div class="form-group social_media_pane hide">
              <label for="row-nationality" class="col-sm-3 control-label">Social Media</label>
                <div class="col-sm-9">
                  <fb:login-button size="medium" scope='public_profile,email' onlogin='checkLoginState();' data-show-faces="false" data-auto-logout-link="true" class="pull-left">
                   Link with Facebook
                   </fb:login-button>
                </div>
            </div>
            </div><!--end -->
            <div id="contact-info" class="tab-pane fade">
              <h3>CONTACT INFORMATION</h3>
               <div class="form-group">
                <label for="row-home_address" class="col-sm-3 control-label">Home Address</label>
                <div class="col-sm-9">
                <input type="text" name="home_address" class="form-control borderzero" id="row-home_address">
                </div>
               </div>
               <div class="form-group">
                <label for="row-postal_code" class="col-sm-3 control-label">Postal Code</label>
                <div class="col-sm-9">
                <input type="text" name="postal_code" class="form-control borderzero" id="row-postal_code">
                </div>
               </div>
               <div class="form-group">
                <label for="row-advisors_mobile" class="col-sm-3 control-label">Advisors Mobile</label>
                <div class="col-sm-9">
                <input type="text" name="advisors_mobile" class="form-control borderzero" id="row-advisors_mobile">
                </div>
               </div>
               <div class="form-group">
                <label for="row-advisors_office_mobile" class="col-sm-3 control-label">Advisors Office Phone</label>
                <div class="col-sm-9">
                <input type="text" name="advisors_office_mobile" class="form-control borderzero" id="row-advisors_office_mobile">
                </div>
               </div>
            </div>
            <div id="extra-info" class="tab-pane fade">
            <h3>EXTRA INFORMATION</h3>
              <div class="form-group">
                <label for="row-contracted_date" class="col-sm-3 control-label">Contracted Date(Old)</label>
                <div class="col-sm-9">
                   <div class="input-group date" id="date-contracted_date">
                      <input type="text" name="contracted_date" class="form-control borderzero" id="row-contracted_date" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                   </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-rnf_date" class="col-sm-3 control-label">RNF Date(under legacyFA)</label>
                <div class="col-sm-9">
                  <div class="input-group date" id="date-rnf_date">
                      <input type="text" name="rnf_date" class="form-control borderzero" id="row-rnf_date" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                   </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-off_boarding_date" class="col-sm-3 control-label">Off-Boarding Date</label>
                <div class="col-sm-9">
                   <div class="input-group date" id="date-off_boarding_date">
                      <input type="text" name="off_boarding_date" class="form-control borderzero" id="row-off_boarding_date" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                   </div>
                </div>
               </div>
              <div class="form-group">
                <label for="row-notes" class="col-sm-3 control-label">Notes</label>
                <div class="col-sm-9">
                <textarea class="form-control borderzero" name="notes" id="row-notes"></textarea>
                </div>
               </div>
               <div class="form-group">
                <label for="row-representative_type" class="col-sm-3 control-label">Representative Type</label>
                <div class="col-sm-9">
                  <select class="form-control" name="representative_type" id="row-representative_type">
                    <option value="" class="hide">Select:</option>
                    <option value="New Rep">New Rep</option>
                    <option value="Exp-Rep">Exp-Rep</option>
                    <option value="Rep-ET">Rep-ET</option>
                  </select>
                </div>
               </div>

               <div class="form-group" id="deactivated_form">
                <label for="row-off_boarding_date" class="col-sm-3 control-label">Deactivated Date</label>
                <div class="col-sm-9">
                   <div class="input-group">
                      <input type="text" class="form-control borderzero" id="row-deactivated" maxlength="28" placeholder="MM/DD/YYYY" disabled="disabled" style="background-color: white;">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                   </div>
                </div>
               </div>

              </div>
          <!--   <div id="additional-info" class="tab-pane fade">
               <h3>ADDITIONAL INFORMATION</h3>
               
               <div class="form-group">
                <label for="row-uin_no" class="col-sm-3 control-label">UIN No</label>
                <div class="col-sm-9">
                <input type="text" name="uin_no" class="form-control borderzero" id="row-uin_no">
                </div>
               </div>
               <div class="form-group">
                <label for="row-house_no" class="col-sm-3 control-label">House No</label>
                <div class="col-sm-9">
                <input type="text" name="house_no" class="form-control borderzero" id="row-house_no">
                </div>
               </div>
               <div class="form-group">
                <label for="row-mgt_email" class="col-sm-3 control-label">Management Email(GOOGLE APPS)</label>
                <div class="col-sm-9">
                <input type="text" name="mgt_email" class="form-control borderzero" id="row-mgt_email">
                </div>
               </div>
               <div class="form-group">
                <label for="row-designation_tag" class="col-sm-3 control-label">Designation Tag</label>
                <div class="col-sm-9">
                <input type="text" name="designation_tag" class="form-control borderzero" id="row-designation_tag">
                </div>
               </div>
      
               <div class="form-group">
                <label for="row-unit_name" class="col-sm-3 control-label">Unit Name</label>
                <div class="col-sm-9">
                <input type="text" name="unit_name" class="form-control borderzero" id="row-unit_name">
                </div>
               </div>
               <div class="form-group">
                <label for="row-contract_start" class="col-sm-3 control-label">Contract Start</label>
                <div class="col-sm-9">
                    <div class="input-group date" id="date-contract_start">
                      <input type="text" name="contract_start" class="form-control borderzero" id="row-contract_start" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                    </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-contract_end" class="col-sm-3 control-label">Contract End(RESIGNED)</label>
                <div class="col-sm-9">
                   <div class="input-group date" id="date-contract_end">
                      <input type="text" name="contract_end" class="form-control borderzero" id="row-contract_end" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                   </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-days_since_contracted" class="col-sm-3 control-label">Days Since Contracted</label>
                <div class="col-sm-9">
                <input type="text" name="days_since_contracted" class="form-control borderzero" id="row-days_since_contracted">
                </div>
               </div>
               <div class="form-group">
                <label for="row-elite_scheme_start" class="col-sm-3 control-label">Elite Scheme(START)</label>
                <div class="col-sm-9">
                   <div class="input-group date" id="date-elite_scheme_start">
                      <input type="text" name="elite_scheme_start" class="form-control borderzero" id="row-elite_scheme_start" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span1
                      </span>
                   </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-elite_scheme_end" class="col-sm-3 control-label">Elite Scheme(END)</label>
                <div class="col-sm-9">
                 <div class="input-group date" id="date-elite_scheme_end">
                      <input type="text" name="elite_scheme_end" class="form-control borderzero" id="row-elite_scheme_end" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                   </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-eccs_appset_tranche" class="col-sm-3 control-label">Eccs Appset Tranche</label>
                <div class="col-sm-9">
                <input type="text" name="eccs_appset_tranche" class="form-control borderzero" id="row-eccs_appset_tranche">
                </div>
               </div>
               <div class="form-group">
                <label for="row-be_code" class="col-sm-3 control-label">Be Code</label>
                <div class="col-sm-9">
                <input type="text" name="be_code" class="form-control borderzero" id="row-be_code">
                </div>
               </div>
               <div class="form-group">
                <label for="row-fighter_leads_start" class="col-sm-3 control-label">Fighter Leads(START)</label>
                <div class="col-sm-9">
                  <div class="input-group date" id="date-fighter_leads_start">
                      <input type="text" name="fighter_leads_start" class="form-control borderzero" id="row-fighter_leads_start" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                  </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-fighter_leads_stop" class="col-sm-3 control-label">Fighter Leads(STOP)</label>
                <div class="col-sm-9">
                 <div class="input-group date" id="date-fighter_leads_stop">
                      <input type="text" name="fighter_leads_stop" class="form-control borderzero" id="row-fighter_leads_stop" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                  </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-care_leads_start" class="col-sm-3 control-label">CARE Leads(START)</label>
                <div class="col-sm-9">
                   <div class="input-group date" id="date-care_leads_start">
                      <input type="text" name="care_leads_start" class="form-control borderzero" id="row-care_leads_start" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                  </div>
                </div>
               </div>
              <div class="form-group">
                <label for="row-care_leads_stop" class="col-sm-3 control-label">CARE Leads (STOP)</label>
                <div class="col-sm-9">
                  <div class="input-group date" id="date-care_leads_stop">
                      <input type="text" name="care_leads_stop" class="form-control borderzero" id="row-care_leads_stop" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                  </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-annual_target_ape" class="col-sm-3 control-label">2013 Annual Target APE</label>
                <div class="col-sm-9">
                <input type="text" name="annual_target_ape" class="form-control borderzero" id="row-annual_target_ape">
                </div>
               </div>
                <div class="form-group">
                <label for="row-final_sprint_target_ape" class="col-sm-3 control-label">2013 Final Sprint Target APE</label>
                <div class="col-sm-9">
                <input type="text" name="final_sprint_target_ape" class="form-control borderzero" id="row-final_sprint_target_ape">
                </div>
               </div>
               <div class="form-group">
                <label for="row-did_last_checked" class="col-sm-3 control-label">DID Last Checked</label>
                <div class="col-sm-9">
                  <div class="input-group date" id="date-did_last_checked">
                      <input type="text" name="did_last_checked" class="form-control borderzero" id="row-did_last_checked" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                  </div>
                </div>
               </div>
               <div class="form-group">
                <label for="row-description" class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                <input type="text" name="description" class="form-control borderzero" id="row-description">
                </div>
               </div>
               <div class="form-group">
                <label for="row-organic_js" class="col-sm-3 control-label">Organic JS</label>
                <div class="col-sm-9">
                <input type="text" name="organic_js" class="form-control borderzero" id="row-organic_js">
                </div>
               </div>
               <div class="form-group">
                <label for="row-list_matrix" class="col-sm-3 control-label">List Matrix</label>
                <div class="col-sm-9">
                <input type="text" name="list_matrix" class="form-control borderzero" id="row-list_matrix">
                </div>
               </div>
               <div class="form-group">
                <label for="row-mgt_candidates" class="col-sm-3 control-label">Mgt Candidate</label>
                <div class="col-sm-9">
                <input type="text" name="mgt_candidates" class="form-control borderzero" id="row-mgt_candidates">
                </div>
               </div>
               <div class="form-group">
                <label for="row-holland_trip" class="col-sm-3 control-label">Holland Trip</label>
                <div class="col-sm-9">
                <input type="text" name="holland_trip" class="form-control borderzero" id="row-holland_trip">
                </div>
               </div>
               <div class="form-group">
                <label for="row-cbdp_tier" class="col-sm-3 control-label">CBDP TIER</label>
                <div class="col-sm-9">
                <input type="text" name="cbdp_tier" class="form-control borderzero" id="row-cbdp_tier">
                </div>
               </div>
               <div class="form-group">
                <label for="row-emergency_contact" class="col-sm-3 control-label">Emergency Contact</label>
                <div class="col-sm-9">
                <input type="text" name="emergency_contact" class="form-control borderzero" id="row-emergency_contact">
                </div>
               </div>
               <div class="form-group">
                <label for="row-home_address" class="col-sm-3 control-label">Home Address</label>
                <div class="col-sm-9">
                <input type="text" name="home_address" class="form-control borderzero" id="row-home_address">
                </div>
               </div>
               <div class="form-group">
                <label for="row-postal_code" class="col-sm-3 control-label">Postal Code</label>
                <div class="col-sm-9">
                <input type="text" name="postal_code" class="form-control borderzero" id="row-postal_code">
                </div>
               </div>
               <div class="form-group">
                <label for="row-remarks" class="col-sm-3 control-label">Remarks</label>
                <div class="col-sm-9">
                <input type="text" name="remarks" class="form-control borderzero" id="row-remarks">
                </div>
               </div>
               <div class="form-group">
                <label for="row-final_sprint_goal" class="col-sm-3 control-label">Final Sprint Goal</label>
                <div class="col-sm-9">
                <input type="text" name="final_sprint_goal" class="form-control borderzero" id="row-final_sprint_goal">
                </div>
               </div>
               <div class="form-group">
                <label for="row-base_income" class="col-sm-3 control-label">Base Income(Pre-Migration)</label>
                <div class="col-sm-9">
                <input type="text" name="base_income" class="form-control borderzero" id="row-base_income">
                </div>
               </div>
               <div class="form-group">
                <label for="row-passport_number" class="col-sm-3 control-label">Passport Number</label>
                <div class="col-sm-9">
                <input type="text" name="passport_number" class="form-control borderzero" id="row-passport_number">
                </div>
               </div>
                <div class="form-group">
                <label for="row-passport_expired" class="col-sm-3 control-label">Passport Expired</label>
                <div class="col-sm-9">
                  <div class="input-group date" id="date-passport_expired">
                      <input type="text" name="passport_expired" class="form-control borderzero" id="row-passport_expired" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                  </div>
                </div>
               </div>
                <div class="form-group">
                <label for="row-passport_issued" class="col-sm-3 control-label">Passport Issued</label>
                <div class="col-sm-9">
                  <div class="input-group date" id="date-passport_issued">
                      <input type="text" name="passport_issued" class="form-control borderzero" id="row-passport_issued" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                  </div>
                </div>
               </div>
                <div class="form-group">
                <label for="row-yahoo_messenger" class="col-sm-3 control-label">Messenger(YAHOO!)</label>
                <div class="col-sm-9">
                <input type="text" name="yahoo_messenger" class="form-control borderzero" id="row-yahoo_messenger">
                </div>
               </div>
                <div class="form-group">
                <label for="row-accounting_id" class="col-sm-3 control-label">Accounting ID</label>
                <div class="col-sm-9">
                <input type="text" name="accounting_id" class="form-control borderzero" id="row-accounting_id">
                </div>
               </div>
                <div class="form-group">
                <label for="row-cbdp_group" class="col-sm-3 control-label">CBDP Group</label>
                <div class="col-sm-9">
                <input type="text" name="cbdp_group" class="form-control borderzero" id="row-cbdp_group">
                </div>
               </div>
                <div class="form-group">
                <label for="row-skype" class="col-sm-3 control-label">Skype</label>
                <div class="col-sm-9">
                <input type="text" name="skype" class="form-control borderzero" id="row-skype">
                </div>
               </div>
                <div class="form-group">
                <label for="row-going_for_retreat" class="col-sm-3 control-label">Going for Retreat</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-going_for_retreat" name="going_for_retreat">
                    <option value="" class="hide">Select:</option>
                    <option value="y">Yes</option>
                    <option value="n">No</option>
                  </select>
                </div>
               </div>
            </div> -->
            <!-- <div id="designation-info" class="tab-pane fade">
              <h3>DESIGNATION INFORMATION</h3>
            </div> -->
            <div id="team-info" class="tab-pane fade"><!-- TEAM INFORMATION-->
              
              
               <div class="form-group">
                <label for="row-group_name" class="col-sm-3 control-label">Group Name</label>
                <div class="col-sm-9">
                <input type="text" name="group_name" class="form-control borderzero" id="row-group_name">
                </div>
               </div>
        <!--        <div class="form-group">
                <label for="row-direct_reporting_to" class="col-sm-3 control-label">Direct ReportingTo(1st Level)</label>
                <div class="col-sm-9">
                <input type="text" name="direct_reporting_to" class="form-control borderzero" id="row-direct_reporting_to">
                </div>
               </div>
                <div class="form-group">
                <label for="row-reporting_tag" class="col-sm-3 control-label">Reporting Tag (Agent Name)</label>
                <div class="col-sm-9">
                <input type="text" name="reporting_tag" class="form-control borderzero" id="row-reporting_tag">
                </div>
               </div>
               <div class="form-group">
                <label for="row-indirect_reporting_to" class="col-sm-3 control-label">InDirect ReportingTo</label>
                <div class="col-sm-9">
                <input type="text" name="indirect_reporting_to" class="form-control borderzero" id="row-indirect_reporting_to">
                </div>
               </div>
               <div class="form-group">
                <label for="row-group_tag" class="col-sm-3 control-label">Group Tag</label>
                <div class="col-sm-9">
                <input type="text" name="group_tag" class="form-control borderzero" id="row-group_tag">
                </div>
               </div> -->
            </div>
            <!-- <div id="sales-force-info" class="tab-pane fade">
              <h3>SALES FORCE INFORMATION</h3>
               <div class="form-group">
                <label for="row-sales_force_id" class="col-sm-3 control-label">Sales Force ID</label>
                <div class="col-sm-9">
                <input type="text" name="sales_force_id" class="form-control borderzero" id="row-sales_force_id">
                </div>
               </div>
               <div class="form-group">
                <label for="row-google_account" class="col-sm-3 control-label">Google Apps Account</label>
                <div class="col-sm-9">
                <input type="text" name="google_account" class="form-control borderzero" id="row-google_account">
                </div>
               </div>
            </div> -->
           <div id="provider-info" class="tab-pane fade">
               <h3>PROVIDER AGENT CODE</h3><!-- PROVIDER INFORMATION-->
                @foreach($providers as $row)
              <section class="content-table container-fluid">
                <div id="basicinfo" class="row default-container container-space">
                <div class="col-lg-6 col-xs-6">
                  <p><strong>{{$row->name}}</strong></p>
                </div>
                <div class="col-lg-6 col-xs-6" style="padding-left:0;">
                  <i class=" btn fa fa-angle-down pull-right collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#providers_{{$row->id}}" aria-expanded="false" aria-controls="collapseTwo"></i>
                </div>
                <div id="providers_{{$row->id}}" class="panel-collapse collapse col-lg-12" role="tabpanel" aria-labelledby="headingTwo">
                        @foreach($row->getProviderClassifications as $row_classification)
                            @if(count($row_classification) > 0)
                             <div class="form-group">
                              <label for="row-provider_id" class="col-sm-3 control-label">{{$row_classification->name}}</label>
                             <div class="col-sm-9">
                              <input type="text" name="provider_code[]" id="row-provider_code_{{$row_classification->id}}" class="form-control borderzero">
                              <input type="hidden" name="provider_id[]" id="row-provider_id_{{$row->id}}" class="form-control borderzero" value="{{$row->id}}">
                              <input type="hidden" name="classification_id[]" id="row-classification_id_{{$row_classification->id}}" class="form-control borderzero" value="{{$row_classification->id}}">
                            </div>
                            </div>
                            @endif
                        @endforeach
                </div>
                </div>
                </section>
              @endforeach
            </div>
            <div id="company-info" class="tab-pane fade">
              <h3>COMPANY INFORMATION</h3><!-- COMPANY INFORMATION-->
              
                <div class="form-group">
                   <label for="row-usertype_id" class="col-sm-3 control-label">Designation(Existing)</label>
                  <div class="col-sm-2">
                    <select id="row-usertype_id" class="form-control borderzero" name="usertype_id">
                        <option value="" class="hide">Select:</option>
                        <optgroup label="Advisors">
                          @foreach ($advisors as $row)
                            @if($row->id == $type_id)
                              <option  class="rank-{{ $row->rank }}" value="{{$row->id}}" selected="true">{{ $row->designation }}</option>
                            @else
                              <option  class="rank-{{ $row->rank }}" value="{{$row->id}}">{{ $row->designation }}</option>
                            @endif
                          @endforeach
                        </optgroup>
                        <optgroup label="Supervisors">
                          @foreach ($supervisors as $row)
                            @if($row->id == $type_id)
                              <option class="rank" value="{{ $row->id }}" selected="true">{{ $row->designation }}</option>
                            @else
                              <option class="rank" value="{{ $row->id }}">{{ $row->designation }}</option>
                            @endif
                          @endforeach
                        </optgroup>
                        <!-- 
                      @foreach ($designations as $row)
                        @if($row->id == $type_id)
                            <option value="{{ $row->id }}" selected="true">{{ $row->designation }}</option>
                          @else
                            <option value="{{ $row->id }}">{{ $row->designation }}</option>
                          @endif
                      @endforeach -->
                    </select>
                   <input type="text" class="form-control hide disabled show-view borderzero" id="row-usertype_id-static" value="" readonly="">
                 </div>
                 <div class="col-sm-4">
                  <div class="input-group" style="margin-bottom: 5px;">
                    <input type="text" name="designation_rate" class="form-control borderzero" aria-describedby="basic-addon2" id="row-designation_rate" maxlength="3" value="0">        
                     <span class="input-group-addon">
                              <span>%</span>
                    </span>
                  </div>
                  </div>
                   <div class="col-sm-3 error-designation_effective_date ">
                      <div class="input-group date effective-date" id="date-designation_effective_date">
                         <input type="text" name="designation_effective_date" class="form-control borderzero" id="row-designation_effective_date" maxlength="28" placeholder="MM/DD/YYYY">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>    
                      </div>
                      <sup class="sup-errors"></sup>
                  </div>
                </div>
              <div class="form-group">
                <label for="row-designation_updated" class="col-sm-3 control-label">Designation(Updated)</label>
                <div class="col-sm-9">
                     <select id="row-designation_updated" class="form-control borderzero" name="designation_updated">
                        <option value="" class="hide">Select:</option>
                        <optgroup label="Advisors">
                          @foreach ($advisors as $row)
                            @if($row->id == $type_id)
                              <option  class="rank-{{ $row->rank }}" value="{{$row->id." ".$row->rank}}" selected="true">{{ $row->designation }}</option>
                            @else
                              <option  class="rank-{{ $row->rank }}" value="{{$row->id." ".$row->rank}}">{{ $row->designation }}</option>
                            @endif
                          @endforeach
                        </optgroup>
                        <optgroup label="Supervisors">
                          @foreach ($supervisors as $row)
                            @if($row->id == $type_id)
                              <option class="rank" value="{{ $row->id }}" selected="true">{{ $row->designation }}</option>
                            @else
                              <option class="rank" value="{{ $row->id }}">{{ $row->designation }}</option>
                            @endif
                          @endforeach
                        </optgroup>
                        <!-- 
                      @foreach ($designations as $row)
                        @if($row->id == $type_id)
                            <option value="{{ $row->id }}" selected="true">{{ $row->designation }}</option>
                          @else
                            <option value="{{ $row->id }}">{{ $row->designation }}</option>
                          @endif
                      @endforeach -->
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label for="row-designation_old" class="col-sm-3 control-label">Designation(Old Entry)</label>
                <div class="col-sm-9">
                    <select id="row-designation_old" class="form-control borderzero" name="designation_old">
                        <option value="" class="hide">Select:</option>
                        <optgroup label="Advisors">
                          @foreach ($advisors as $row)
                            @if($row->id == $type_id)
                              <option  class="rank-{{ $row->rank }}" value="{{$row->id." ".$row->rank}}" selected="true">{{ $row->designation }}</option>
                            @else
                              <option  class="rank-{{ $row->rank }}" value="{{$row->id." ".$row->rank}}">{{ $row->designation }}</option>
                            @endif
                          @endforeach
                        </optgroup>
                        <optgroup label="Supervisors">
                          @foreach ($supervisors as $row)
                            @if($row->id == $type_id)
                              <option class="rank" value="{{ $row->id }}" selected="true">{{ $row->designation }}</option>
                            @else
                              <option class="rank" value="{{ $row->id }}">{{ $row->designation }}</option>
                            @endif
                          @endforeach
                        </optgroup>
                        <!-- 
                      @foreach ($designations as $row)
                        @if($row->id == $type_id)
                            <option value="{{ $row->id }}" selected="true">{{ $row->designation }}</option>
                          @else
                            <option value="{{ $row->id }}">{{ $row->designation }}</option>
                          @endif
                      @endforeach -->
                    </select>
                </div>
              </div>
              <div class="form-group">
                <label for="row-banding_rank" class="col-sm-3 control-label">*Banding Rank</label>
                <div class="col-sm-2 error-banding_rank">
                     <select class="form-control borderzero" id="row-banding_rank" name="banding_rank">
                          <option value=""  class="hide">Select</option>
                        @foreach($tiers as $row)
                          <option value="{{ $row->id }}">{{$row->name}}</option>
                        @endforeach
                      </select>
                       <sup class="sup-errors"></sup>
                      
                </div>
                <div class="col-sm-4 error-bonding_rate ">
                <div class="input-group" style="margin-bottom: 5px;">
                  <input type="text" name="bonding_rate" class="form-control borderzero" aria-describedby="basic-addon2" id="row-bonding_rate" maxlength="3" value="0">        
                   <span class="input-group-addon">
                            <span>%</span>
                  </span>
                  <sup class="sup-errors"></sup>
                </div>
                </div>
                 <div class="col-sm-3 error-banding_effective_date">
                      <div class="input-group date effective-date" id="date-banding_effective_date">
                         <input type="text" name="banding_effective_date" class="form-control borderzero" id="row-banding_effective_date" maxlength="28" placeholder="MM/DD/YYYY">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                       </div>
                       <sup class="sup-errors"></sup>
                  </div>
              </div>
              <div class="form-group">
                <label for="row-gi_banding_rank" class="col-sm-3 control-label">GI Banding Rank</label>
                <div class="col-sm-2 error-gi_banding_rank">
                     <select class="form-control borderzero" id="row-gi_banding_rank" name="gi_banding_rank">
                          <option value="">Select</option>
                        @foreach($gis as $row)
                          <option value="{{ $row->id }}">Rank {{$row->rank}}</option>
                        @endforeach
                      </select>
                       <sup class="sup-errors"></sup>
                </div>
              </div>
               <div class="form-group">
                <label for="row-latest_ytd_production" class="col-sm-3 control-label">Latest YTD Production</label>
                <div class="col-sm-9 error-latest_ytd_production">
                <input type="text" name="latest_ytd_production" class="form-control borderzero" id="row-latest_ytd_production">
                </div>
               </div>
           
               <div class="form-group">
                <label for="row-smartermail_username" class="col-sm-3 control-label">SmarterMail Username</label>
                <div class="col-sm-9 error-smartermail_username">
                <input type="text" name="smartermail_username" class="form-control borderzero" id="row-smartermail_username">
                </div>
               </div>
                <hr></hr>
                <!-- <a href = "#" class="btn btn-xs btn-info borderzero pull-right btn-add-director-code"><i class="fa fa-plus"></i> Create Director Code</a> -->
                <h3>GROUP MANAGEMENT</h3>
               <div class="form-group">
                <label for="row-agent_serial_no" class="col-sm-3 control-label">*Agent Serial No</label>
                <div class="col-sm-9 error-agent_serial_no ">
                <input type="text" name="agent_serial_no" class="form-control borderzero" id="row-agent_serial_no">
                <sup class="sup-errors"></sup>
                </div>
               </div>
               <!--  <div class="form-group" id="active_group_code">
                <label for="row-group_id" class="col-sm-3 control-label">Director Code</label>
                <div class="col-sm-7">
                <input type="text" name="group_id" class="form-control borderzero" id="row-group_id">
                </div>
                <div class="col-sm-2">
                  <button href="" type="button" class="btn-change-group-code"><i class="fa fa-pencil"></i> CHANGE</button>
                </div>
               </div>
               <div class="form-group" id="active_unit_code">
                <label for="row-unit_codename" class="col-sm-3 control-label">Manager Code</label>
                <div class="col-sm-7">
                <input type="text" name="unit_codename" class="form-control borderzero" id="row-unit_codename">
                </div>
                <div class="col-sm-2">

                </div>
               </div> -->

              <div class="form-group" id="group_code_pane">
                <label for="row-group_code" class="col-sm-3 control-label">Director Code</label>
                <div class="col-sm-9 error-group_code_owner">
                <select class="form-control" id="row-group_code" name="group_code">
                  <option value="" class="hide">Select:</option>
                  @foreach($groups as $row)
                    <option value="{{$row->id}}">{{$row->code}}</option>
                  @endforeach
                </select>
                <input type="text" name="group_code_owner" class="form-control borderzero hide" id="row-group_code_owner">
                <sup class="sup-errors"></sup>
                </div>
               </div>
               <div class="form-group unit_code_undropdown">
                <label for="user_id" class="col-sm-3 col-xs-5"></label>
                <div class="col-sm-9 col-xs-7">
                  <input name="owner" id="row-owner" type="checkbox" value="yes"> Create Group and Assign as Group Owner</b>.
                </div>
               </div>
                <div class="form-group unit_code_undropdown error-unit_id error-unit_code">
                <label for="row-unit_code" class="col-sm-3 control-label">Manager Code</label>
                <div class="col-sm-9">
                <input type="text" name="unit_code" class="form-control borderzero" id="row-unit_code_final">
                <sup class="sup-errors"></sup>
                </div>
               </div>
               <div class="form-group unit_code_dropdown error-unit_code error-unit_id">
                <label for="row-unit_id" class="col-sm-3 control-label">Manager Code</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-unit_id" name="unit_id">
                  <option value="" class="hide">Select:</option>
                  </select>
                  <sup class="sup-errors"></sup>
                  <sub class="help-info"><span class="label label-danger"><i class="fa fa-info"></i></span> Please select designation to enable dropdown boxes</sub>
                </div>
                </div>


               <!-- <div class="form-group">
                <label for="row-rnf_date" class="col-sm-3 control-label">RNF Date</label>
                <div class="col-sm-9">
                    <div class="input-group date" id="date-rnf_date">
                      <input type="text" name="rnf_date" class="form-control borderzero" id="row-rnf_date" maxlength="28" placeholder="MM/DD/YYYY">
                      <span class="input-group-addon">
                        <span><i class="fa fa-calendar"></i></span>
                      </span>
                   </div>
                </div>
               </div> -->
              <!--   <div class="form-group">
                <label for="row-department" class="col-sm-3 control-label">Department</label>
                <div class="col-sm-9">
                <input type="text" name="department" class="form-control borderzero" id="row-department">
                </div>
               </div> -->
            </div>
           <!--  <div id="jumpsite-info" class="tab-pane fade">
              <h3>JUMPSITE INFORMATION</h3>
                <div class="form-group">
                <label for="row-jumpsite_batch" class="col-sm-3 control-label">JumpSite Batch</label>
                <div class="col-sm-9">
                <input type="text" name="jumpsite_batch" class="form-control borderzero" id="row-jumpsite_batch">
                </div>
               </div>
                <div class="form-group">
                <label for="row-jumpsite" class="col-sm-3 control-label">Jumpsite</label>
                <div class="col-sm-9">
                <input type="text" name="jumpsite" class="form-control borderzero" id="row-jumpsite">
                </div>
               </div>
                <div class="form-group">
                <label for="row-js_clients" class="col-sm-3 control-label">JS Clients</label>
                <div class="col-sm-9">
                <input type="text" name="js_clients" class="form-control borderzero" id="row-js_clients">
                </div>
               </div>
                <div class="form-group">
                <label for="row-jumpsite_user" class="col-sm-3 control-label">Jumpsite User</label>
                <div class="col-sm-9">
                <input type="text" name="jumpsite_user" class="form-control borderzero" id="row-jumpsite_user">
                </div>
               </div>
            </div> -->
        <!--     <div id="security-info" class="tab-pane fade">
              <h3>SECURITY INFORMATION</h3>
               <div class="form-group">
                <label for="row-two_digit_randomizer" class="col-sm-3 control-label">Two Digit Randomizer</label>
                <div class="col-sm-9">
                <input type="text" name="two_digit_randomizer" class="form-control borderzero" id="row-two_digit_randomizer">
                </div>
               </div>
               <div class="form-group">
                <label for="row-temp_password" class="col-sm-3 control-label">Temp Password</label>
                <div class="col-sm-9">
                <input type="password" name="temp_password" class="form-control borderzero" id="row-temp_password">
                </div>
               </div>
                <div class="form-group">
                <label for="row-laptop_wlan" class="col-sm-3 control-label">Laptop WLAN</label>
                <div class="col-sm-9">
                <input type="text" name="laptop_wlan" class="form-control borderzero" id="row-laptop_wlan">
                </div>
               </div>
               <div class="form-group">
                <label for="row-mac_address" class="col-sm-3 control-label">MAC Address</label>
                <div class="col-sm-9">
                <input type="text" name="mac_address" class="form-control borderzero" id="row-mac_address">
                </div>
               </div>
            </div> -->
          <!--   <div id="dialer-info" class="tab-pane fade">
              <h3>DIALER INFORMATION</h3>
               <div class="form-group">
                <label for="row-dialer_un" class="col-sm-3 control-label">Dialer Username</label>
                <div class="col-sm-9">
                <input type="text" name="dialer_un" class="form-control borderzero" id="row-dialer_un">
                </div>
               </div>
               <div class="form-group">
                <label for="row-dialer_pw" class="col-sm-3 control-label">Dialer Password</label>
                <div class="col-sm-9">
                <input type="password" name="dialer_pw" class="form-control borderzero" id="row-dialer_pw">
                </div>
               </div>
                <div class="form-group">
                <label for="row-dialer_status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-9">
                <input type="text" name="dialer_status" class="form-control borderzero" id="row-dialer_status">
                </div>
               </div>
               <div class="form-group">
                <label for="row-campaign" class="col-sm-3 control-label">Campaign</label>
                <div class="col-sm-9">
                <input type="text" name="campaign" class="form-control borderzero" id="row-campaign">
                </div>
               </div>
               <div class="form-group">
                <label for="row-list" class="col-sm-3 control-label">List</label>
                <div class="col-sm-9">
                <input type="text" name="list" class="form-control borderzero" id="row-list">
                </div>
               </div>
               <div class="form-group">
                <label for="row-user_group" class="col-sm-3 control-label">User Group</label>
                <div class="col-sm-9">
                <input type="text" name="user_group" class="form-control borderzero" id="row-user_group">
                </div>
               </div>
            </div> -->
            <!-- <div id="iserver-info" class="tab-pane fade">
               <h3>iSERVER INFORMATION</h3>
               <div class="form-group">
                <label for="row-temp_username" class="col-sm-3 control-label">Temp Username</label>
                <div class="col-sm-9">
                <input type="text" name="iserver_username" class="form-control borderzero" id="row-iserver_username">
                </div>
               </div>
               <div class="form-group">
                <label for="row-iserver_password" class="col-sm-3 control-label">Temp Password</label>
                <div class="col-sm-9">
                <input type="password" name="iserver_password" class="form-control borderzero" id="row-iserver_password">
                </div>
               </div>
               <div class="form-group">
                <label for="row-iserver_email_address" class="col-sm-3 control-label">iServer Email Address</label>
                <div class="col-sm-9">
                <input type="text" name="iserver_email_address" class="form-control borderzero" id="row-iserver_email_address">
                </div>
               </div>
              <div class="form-group">
                <label for="row-iserver_group_name" class="col-sm-3 control-label">iServer Group Name</label>
                <div class="col-sm-9">
                <input type="text" name="iserver_group_name" class="form-control borderzero" id="row-iserver_group_name">
                </div>
               </div>
               <div class="form-group">
                <label for="row-iserver_bay_folder" class="col-sm-3 control-label">iServer i-bay Folder</label>
                <div class="col-sm-9">
                <input type="text" name="iserver_bay_folder" class="form-control borderzero" id="row-iserver_bay_folder">
                </div>
               </div>
               <div class="form-group">
                <label for="row-iserver_ftp_path" class="col-sm-3 control-label">iServer FTP Path</label>
                <div class="col-sm-9">
                <input type="text" name="iserver_ftp_path" class="form-control borderzero" id="row-iserver_ftp_path">
                </div>
               </div>    
            </div> -->
            <!-- <div id="misc-info" class="tab-pane fade">
              <h3>MISCELLANEOUS INFORMATION</h3>
               <div class="form-group">
                <label for="row-contract" class="col-sm-3 control-label">Contract</label>
                <div class="col-sm-9">
                 <select class="form-control" id="row-contract" name="contract">
                    <option value="" class="hide">Select:</option>
                    <option value="y">Yes</option>
                    <option value="n">No</option>
                  </select>
                </div>
               </div>
               <div class="form-group">
                <label for="row-original_is" class="col-sm-3 control-label">Original IS</label>
                <div class="col-sm-9">
                 <select class="form-control" id="row-original_is" name="original_is">
                    <option value="" class="hide">Select:</option>
                    <option value="y">Yes</option>
                    <option value="n">No</option>
                  </select>
                </div>
               </div>
               <div class="form-group">
                <label for="row-authorisation" class="col-sm-3 control-label">Authorisation Letter</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-authorisation" name="authorisation">
                    <option value="" class="hide">Select:</option>
                    <option value="y">Yes</option>
                    <option value="n">No</option>
                  </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-laptop_ethernet_mac_address" class="col-sm-3 control-label">Laptop Ethernet MAC Address</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-laptop_ethernet_mac_address" name="laptop_ethernet_mac_address">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-photocopy_is" class="col-sm-3 control-label">Photocopy IS</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-photocopy_is" name="photocopy_is">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-production_report" class="col-sm-3 control-label">Production Report</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-production_report" name="production_report">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
                <div class="form-group">
                <label for="row-nric_copy" class="col-sm-3 control-label">NRIC COPY</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-nric_copy" name="nric_copy">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-resignation_letter" class="col-sm-3 control-label">Resignation Letter</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-resignation_letter" name="resignation_letter">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
                <div class="form-group">
                <label for="row-highest_cert" class="col-sm-3 control-label">Highest Cert</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-highest_cert" name="highest_cert">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-m5" class="col-sm-3 control-label">M5</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-m5" name="m5">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-m9" class="col-sm-3 control-label">M9</label>
                <div class="col-sm-9">
                  <select class="form-control" id="row-m9" name="m9">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                  </select>
               </div>
               </div>
                <div class="form-group">
                <label for="row-hi" class="col-sm-3 control-label">HI</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-hi" name="hi">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-m8" class="col-sm-3 control-label">M8</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-m8" name="m8">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
                <div class="form-group">
                <label for="row-cpf" class="col-sm-3 control-label">CPF</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-cpf" name="cpf">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-adv_data" class="col-sm-3 control-label">Adv Data</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-adv_data" name="adv_data">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-n_card" class="col-sm-3 control-label">N Card</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-n_card" name="n_card">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
                <div class="form-group">
                <label for="row-misc_photo" class="col-sm-3 control-label">Photo</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-misc_photo" name="misc_photo">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
                <div class="form-group">
                <label for="row-original_cert" class="col-sm-3 control-label">Original Cert</label>
                <div class="col-sm-9">
                <input type="text" name="original_cert" class="form-control borderzero" id="row-original_cert">
               </div>
               </div>
               <div class="form-group">
                <label for="row-training" class="col-sm-3 control-label">Training</label>
                <div class="col-sm-9">
                    <select class="form-control" id="row-training" name="training">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
                <div class="form-group">
                <label for="row-photocopy_cert" class="col-sm-3 control-label">Photocopy Cert</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-photocopy_cert" name="photocopy_cert">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div>
               <div class="form-group">
                <label for="row-cpf_liability" class="col-sm-3 control-label">CPF Liability</label>
                <div class="col-sm-9">
                     <select class="form-control" id="row-cpf_liability" name="cpf_liability">
                      <option value="" class="hide">Select:</option>
                      <option value="y">Yes</option>
                      <option value="n">No</option>
                     </select>
               </div>
               </div> -->
<!--         
            </div><!--end of tab-->
            </div>
            </div>
            <div class="modal-footer borderzero">
            <input type="hidden" name="id" id="row-user_sales_id" value="">
                <!-- <div class="fb-login-button pull-left" data-max-rows="1" data-size="large" scope='public_profile,email' onlogin='checkLoginState();' data-show-faces="false" data-auto-logout-link="true"></div> -->
                <!-- <div class="fb-login-button pull-left" data-max-rows="1"   data-size="icon" data-show-faces="false" data-auto-logout-link="true"></div> -->
                
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view borderzero btn-submit"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

  <!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Designate Modal -->
  <div class="modal fade" id="modal-designate" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
          {!! Form::open(array('url' => 'sales/designate', 'role' => 'form', 'id' => 'modal-save_designate', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-primary">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="designate-title">Designate</h4>
           </div>
           <div class="modal-body">
           <div id="designate-notice"></div>
            <div class="form-group">
               <label for="row-designation_id" class="col-sm-3 control-label">Designation</label>
            <div class="col-sm-9">
                <select id="select" class="form-control borderzero" id="sel1" name="designation_id">
                  <option value="" class="hide">Select:</option>
                  @foreach ($designations as $row)
                    @if($row->id == $type_id)
                        <option value="{{ $row->id }}" selected="true">{{ $row->designation }}</option>
                      @else
                        <option value="{{ $row->id }}">{{ $row->designation }}</option>
                      @endif
                  @endforeach
                </select>
            </div>
            </div>
            <div class="form-group">
            <label for="row-effective_date" class="col-sm-3 control-label">Effective Date</label>
            <div class="col-sm-9">
                <div class="input-group date effective-date" id="date-effective_date">
                  <input class="form-control" id="row-effective_date" required="required" name="effective_date" type="text">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
            </div>
            <div class="form-group">
                <label for="row-rate" class="col-sm-3 control-label">Rate</label>
                <div class="col-sm-9">
                <input type="text" name="rate" class="form-control borderzero" id="row-rate">
               </div>
               </div>
            <div class="clearfix"></div>
            </div>
            <div class="modal-footer borderzero">

                <input type="hidden" name="id" id="designate-id" value="">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-map-marker"></i> Designate</button>
            </div>
            {!! Form::close() !!}
          </div>
        </div>
    </div>


  <!-- Team Modal -->
  <div class="modal fade" id="modal-team" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
          {!! Form::open(array('url' => 'sales/team', 'role' => 'form', 'id' => 'modal-save_team', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-success">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="team-title"><i class="fa fa-cog"></i> Supervisor</h4>
           </div>
           <div class="modal-body">
            <div id="team-notice"></div>
            <div class="form-group">
              <label for="row-supervisor_id" class="col-sm-3 control-label">Supervisor</label>
            <div class="col-sm-9">
              <select name="supervisor_id" id="row-supervisor_id" class="form-control input-md borderzero" style="width: 100%;">
              </select>
            </div>
            </div>
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
                <input type="hidden" name="id" id="team-id" value="">
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view borderzero btn-submit"><i class="fa fa-save"></i> Save changes</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div>

    <!-- Edit Banding Modal -->
 <!--<div class="modal fade" id="modal-banding-edit" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
          {!! Form::open(array('url' => 'sales/edit-banding/save', 'role' => 'form', 'id' => 'modal-edit_banding', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-danger">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"></h4>
           </div>
           <div class="modal-body">
            <div id="banding-notice"></div>
            <div class="form-group">
              <label for="row-bonding_rate" class="col-sm-3 control-label">Banding Rate</label>
            <div class="col-sm-9">
                <div class="input-group date">
                  <input type="text" name="bonding_rate" class="form-control borderzero" id="row-bonding_rate" maxlength="30" placeholder="">
                  <span class="input-group-addon">
                    <span>%</span>
                  </span>
                </div>
            </div>
            </div>
            <div class="form-group">
               <label for="row-effective_date" class="col-sm-3 control-label">Effective Date</label>
            <div class="col-sm-9">
                <div class="input-group date effective-date">
                  <input class="form-control" id="row-effective_date" required="required" name="effective_date" type="text">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
              <input type="hidden" name="id" id="edit-bonding-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-map-marker"></i> Save Changes</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
    </div> -->

  <!-- Bonding Modal -->
  <div class="modal fade" id="modal-bonding" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
       <div class="modal-content borderzero">
          {!! Form::open(array('url' => 'sales/bond', 'role' => 'form', 'id' => 'modal-save_bonding', 'class' => 'form-horizontal')) !!}
           <div class="modal-header modal-danger">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="bonding-title"><i class="fa fa-cog"></i> Banding Settings</h4>
           </div>
           <div class="modal-body">
            <div id="bonding-notice"></div>
            <div class="form-group">
               <label for="row-banding_rank_new" class="col-sm-3 control-label">Banding Rank</label>
            <div class="col-sm-9">
                <select class="form-control borderzero" name="banding_rank" id="new_banding_rate">
                  <option value="" class="hide">Select:</option>
                  @foreach($tiers as $row)
                          <option value="{{ $row->id }}">{{$row->name}}</option>
                  @endforeach
                </select>
                <sup class="error-banding_rank sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-bonding_rate" class="col-sm-3 control-label">Banding Rate</label>
             <div class="col-sm-9">
                <div class="input-group">
                  <input type="text" name="bonding_rate" class="form-control borderzero" id="row-bonding_rate_new" maxlength="30" placeholder="">
                  <span class="input-group-addon">
                    <span>%</span>
                  </span>
                </div>
            </div>
            </div>
             <div class="form-group">
            <label for="row-effective_date" class="col-sm-3 control-label">Effective Date</label>
            <div class="col-sm-9">
                <div class="input-group date effective-date" id="date-effective_date">
                  <input class="form-control" id="row-effective_date" required="required" name="effective_date" type="text">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
            </div>
            <div class="clearfix"></div>
            </div>
           <div class="modal-footer borderzero">
              <input type="hidden" name="id" id="bonding-id">
                <button type="button" class="btn btn-danger borderzero btn-close" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view borderzero btn-submit"><i class="fa fa-save"></i> Save changes</button>
            </div>
          {!! Form::close() !!}
        </div>
    </div>
  </div>
  <!-- Remove Modal -->
  <div id="modal-remove" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-remove-header" class="modal-header modal-yeah">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-remove-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-remove-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-remove-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-remove-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>



