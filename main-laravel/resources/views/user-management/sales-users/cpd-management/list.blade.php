@extends('layouts.master')

@section('scripts')
	<script type="text/javascript">
    $_token = '{{ csrf_token() }}';
    $loading = false;
    $selectedID = 0;
    // refresh the list
    function refresh() {

      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');

      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $per = $("#row-per").val();

      $search = $("#row-search").val();
      $structured_req = "{{ number_format($settings_cpd_structured->no_of_hours, 2) }}";
      $ethics_req = "{{ number_format($settings_cpd_ethics->no_of_hours, 2) }}";
      $rules_and_regulations_req = "{{ number_format($settings_cpd_rules_and_regulation->no_of_hours, 2) }}";
      $total_req = "{{ number_format($settings_cpd_structured->no_of_hours + $settings_cpd_ethics->no_of_hours + $settings_cpd_rules_and_regulation->no_of_hours, 2)}}";
      $gi_req = "{{ number_format($settings_cpd_gi->no_of_hours, 2) }}";

      var body = $("#rows");
      $.post("{{ $refresh_route }}", { per: $per,page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

        $.each(response.rows.data, function(index, row) {
             
          body += '<tr data-id="'+row.user_id+'" data-name = "'+row.name+'" data-cpd_id="'+row.cpd_id+'">'+
                  '<td><a class="btn btn-xs btn-table btn-expand" data-code = "'+row.code+'"><i class="fa fa-plus"></i></a>&nbsp;'+row.name+'</td>'+
                  '<td>'+(row.supervisor_name ? row.supervisor_name:row.name)+'</td>'+
                  '<td>'+row.code+'</td>'+
                  '<td style="text-align:center;">'+formatNumber($structured_req)+'</td>'+
                  '<td style="text-align:center;">'+formatNumber($ethics_req)+'</td>'+
                  '<td style="text-align:center;">'+formatNumber($rules_and_regulations_req)+'</td>'+
                  '<td style="text-align:center;">'+formatNumber($total_req)+'</td>'+
                  '<td style="text-align:center;">'+formatNumber($gi_req)+'</td>'+
                  '<td style="text-align:center;">'+formatNumber($total_req - row.cpd_hours_both)+'</td>'+
                  '<td class="rightalign">'+
                      '<button type="button" class="btn-add btn btn-xs btn-default"><i class="fa fa-clock-o"></i></button>'+
                    '</td>';
        });

        $loading = false;
        $selectedID = 0;
        $('#rows').html(body);
        $('#row-pages').html(response.pages);
        $('.loading-pane').addClass('hide');
      });
    } 

    function search() {

      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');

      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $per = $("#row-per").val();

      $search = $("#row-search").val();

      $structured_req = "{{ number_format($settings_cpd_structured->no_of_hours, 2) }}";
      $ethics_req = "{{ number_format($settings_cpd_ethics->no_of_hours, 2) }}";
      $rules_and_regulations_req = "{{ number_format($settings_cpd_rules_and_regulation->no_of_hours, 2) }}";
      $total_req = "{{ number_format($settings_cpd_structured->no_of_hours + $settings_cpd_ethics->no_of_hours + $settings_cpd_rules_and_regulation->no_of_hours, 2)}}";
      $gi_req = "{{ number_format($settings_cpd_gi->no_of_hours, 2) }}";

      var body = $("#rows");
      $.post("{{ $refresh_route }}", { per: $per,page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

        $.each(response.rows.data, function(index, row) {
           
          body += '<tr data-id="' + row.user_id + '" data-name = "' + row.name + '" data-cpd_id="' + row.cpd_id + '">' + 
                  '<td><a class="btn btn-xs btn-table btn-expand" data-code = "' + row.code + '"><i class="fa fa-plus"></i></a>&nbsp;' + row.name + '</td>' + 
                  '<td>' + (row.supervisor_name ? row.supervisor_name:row.name) + '</td>' + 
                  '<td>' + row.code + '</td>' + 
                  '<td style="text-align:center;">' + formatNumber($structured_req) + '</td>' + 
                  '<td style="text-align:center;">' + formatNumber($ethics_req) + '</td>' + 
                  '<td style="text-align:center;">' + formatNumber($rules_and_regulations_req) + '</td>' + 
                  '<td style="text-align:center;">' + formatNumber($total_req) + '</td>' + 
                  '<td style="text-align:center;">'+formatNumber($gi_req)+'</td>'+
                  '<td style="text-align:center;">' + formatNumber($total_req - row.cpd_hours_both) + '</td>' + 
                  '<td class="rightalign">' + 
                    '<button type="button" class="btn-add btn btn-xs btn-default"><i class="fa fa-clock-o"></i></button>' + 
                  '</td>';
        });

        $('#rows').html(body);
        $('#row-pages').html(response.pages);
        $('.loading-pane').addClass('hide');
      });    
    }


    function formatNumber(num) {
      if (num) {
        return parseFloat(num).toFixed(2)
      } else {
        return '0.00';
      }
    }

    function compute_hours() {

      var cpd_hours_req_skills = $("#row-cpd_hours_req_skills").val();
      var cpd_hours_req_rules_and_regulations = $("#row-cpd_hours_req_rules_and_regulations").val();
      var cpd_hours_req_knowledge = $("#row-cpd_hours_req_knowledge").val();
      var cpd_hours_req_gi = $("#row-cpd_hours_req_gi").val();

      var cpd_hours_skills = $("#row-total_cpd_hours_skills").val();
      var cpd_hours_knowledge = $("#row-total_cpd_hours_knowledge").val();
      var cpd_hours_rules_and_regulations = $("#row-total_cpd_hours_rules_and_regulations").val();
      var cpd_hours_gi = $("#row-total_cpd_hours_gi").val();

      var cpd_hours_total = parseFloat(cpd_hours_skills) + parseFloat(cpd_hours_knowledge) + parseFloat(cpd_hours_rules_and_regulations);

      var cpd_hours_req_total = parseFloat(cpd_hours_req_skills) + parseFloat(cpd_hours_req_knowledge) + parseFloat(cpd_hours_req_rules_and_regulations);

      $("#row-total_cpd_hours_final").val(cpd_hours_total);
      $("#row-short_fall").val(parseFloat(cpd_hours_req_total) - parseFloat(cpd_hours_total));
      // var req_skill = parseFloat(cpd_hours_req_skills);
      // var req_knowledge = parseFloat(cpd_hours_req_knowledge);
      // var req_rules_and_regulations = parseFloat(cpd_hours_req_rules_and_regulations);
      // var req_gi = parseFloat(cpd_hours_req_gi);

      // var unparsetotal_req = req_skill + req_knowledge + req_rules_and_regulations + req_gi;

      // var for_skill = parseFloat(cpd_hours_skill);
      // var for_knowledge = parseFloat(cpd_hours_knowledge);
      // var for_rules_and_regulations = parseFloat(cpd_hours_rules_and_regulations);
      // var for_gi = parseFloat(cpd_hours_gi);

      // var unparsetotal = for_skill + for_knowledge + for_rules_and_regulations + for_gi;
      // var total = parseFloat(unparsetotal);
      // var shortfall = parseFloat(unparsetotal_req) - parseFloat(total);
      // var final_shortfall = parseFloat(shortfall);


      // if(cpd_hours_req_knowledge == 0) {
      //   $("#row-cpd_hours_req_knowledge").val(0);
      //   $("#row-short_fall").val(final_shortfall);
      // }

      // if(cpd_hours_req_skills == 0) {
      //   $("#row-cpd_hours_req_skills").val(0);
      //   $("#row-short_fall").val(final_shortfall);
      // }

      // if(cpd_hours_req_rules_and_regulations == 0) {
      //   $("#row-cpd_hours_req_rules_and_regulations").val(0);
      //   $("#row-short_fall").val(final_shortfall);
      // }

      // if(cpd_hours_skill == 0) {
      //   $("#row-total_cpd_hours_skills").val(0)
      //   var new_total = for_knowledge + 0; 
      //   var new_partial_skill=parseFloat(new_total);
      //   $("#row-total_cpd_hours_final").val(new_partial_skill);
      // }

      // if(cpd_hours_knowledge == 0) {    
      //   $("#row-total_cpd_hours_knowledge").val(0);
      //   var new_total = for_skill + 0; 
      //   var new_partial_knowledge=parseFloat(new_total);
      //   $("#row-total_cpd_hours_final").val(new_partial_knowledge);
      // }

      // if(cpd_hours_req_rules_and_regulations == 0) {    
      //   $("#row-cpd_hours_req_rules_and_regulations").val(0);
      //   var new_total = for_skill + for_knowledge + 0; 
      //   var new_partial_rules_and_regulations=parseFloat(new_total);
      //   $("#row-total_cpd_hours_final").val(new_partial_rules_and_regulations);
      // }

      // if(cpd_hours_skill == 0 && cpd_hours_knowledge == 0 && cpd_hours_rules_and_regulations == 0) {
      //   $("#row-total_cpd_hours_final").val(0);
      // }

      // if(isNaN(total)) {
      //   var total = 0;
      // } else {
      //   $("#row-total_cpd_hours_final").val(total);
      // }

      // if(isNaN(final_shortfall)) {
      //   var final_shortfall = 0;
      // } else {
      //   $("#row-short_fall").val(final_shortfall);
      // }
    }

    $(document).ready(function() {
      $(".multi_select").select2();

      $('#row-sales_id').multiselect({
        maxHeight: 400,
        enableCaseInsensitiveFiltering: true,
        selectAllValue: 'multiselect-all',
        selectAllText: 'Select All',
        includeSelectAllOption: true,
        numberDisplayed: 2
      });

      $("#page-content-wrapper").on("click", ".btn-expand", function() {

        $(".btn-expand").html('<i class="fa fa-minus"></i>');
        $(".btn-expand").html('<i class="fa fa-plus"></i>');
        if ($loading){
          return;
        }
        $tr = $(this).parent().parent();
        $id = $tr.data('id');

        $selected = '.row-' + $selectedID;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

        if ($id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('sales/get-cpds') }}", { id: $id, _token: $_token }, function(response) {

        $tr.after('<tr><td colspan="10" style="padding:10px"><div class="row-' + $selectedID + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
        });

      });

      $(function () {
        $('.date').datetimepicker({
          locale: 'en',
          format: 'MM/DD/YYYY'
        });
      });
    });

   $("#page-content-wrapper").on("click", ".btn-new", function() {
    
      $("#row-cpdfromdate").removeAttr("disabled","true");
      $("#row-cpdtodate").removeAttr("disabled","true");
      $("#row-cpd_status").removeAttr("disabled","false");
      $("#row-cpd_period").removeAttr("disabled","false");
      $("#row-cpd_tracking_period").removeAttr("disabled","false");
      $("#row-cpd_hours_req_skills").removeAttr("disabled","false");
      $("#row-cpd_hours_req_knowledge").removeAttr("disabled","false");
      $("#row-total_cpd_hours_skills").removeAttr("disabled","false");
      $("#row-total_cpd_hours_knowledge").removeAttr("disabled","false");
      $("#row-total_cpd_hours_final").removeAttr("disabled","false");
      $("#row-cpd_hours_req_knowledge").removeAttr("disabled","true");
      $("#row-cpd_tracking_period_from").removeAttr("disabled","true");
      $("#row-cpd_tracking_period_to").removeAttr("disabled","true");
      $("#row-cpd_hours_req_rules_and_regulations").removeAttr("disabled","true");
      $("#row-total_cpd_hours_rules_and_regulations").removeAttr("disabled","true");
      $("#row-course").removeAttr("disabled","true");
      $("#row-short_fall").removeAttr("disabled","true");
      $("#modal-form").find('form').trigger("reset");
      $(".sales_user_undropdown").removeClass("hide");
      $(".sales_user_dropdown").addClass("hide");

      $(".sales_user_undropdown").addClass("hide");
      $(".sales_user_dropdown").removeClass("hide");
      $(".sales_user_undropdown").removeAttr('name');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      $("#row-id").val("");
      $("#modal-form").find('input').removeAttr('readonly', 'readonly');

      $(".cpd-input").val(0);
      $("#row-cpd_hours_req_skills").val({{$settings_cpd_structured->no_of_hours}});
      $("#row-cpd_hours_req_rules_and_regulations").val({{$settings_cpd_ethics->no_of_hours}});
      $("#row-cpd_hours_req_knowledge").val({{$settings_cpd_rules_and_regulation->no_of_hours}});
      $("#row-cpd_hours_req_gi").val({{$settings_cpd_gi->no_of_hours}});

      var cpd_hours_req_skills = $("#row-cpd_hours_req_skills").val();
      var cpd_hours_req_rules_and_regulations = $("#row-cpd_hours_req_rules_and_regulations").val();
      var cpd_hours_req_knowledge = $("#row-cpd_hours_req_knowledge").val();
      var cpd_hours_req_gi = $("#row-cpd_hours_req_gi").val();

      var cpd_hours_req_total = parseFloat(cpd_hours_req_skills) + parseFloat(cpd_hours_req_knowledge) + parseFloat(cpd_hours_req_rules_and_regulations) + parseFloat(cpd_hours_req_gi);

      $("#row-short_fall").val(parseFloat(cpd_hours_req_total));

      $(".modal-title").html("<i class='fa fa-plus'></i> Add CPD Hours");
      $("#modal-form").modal('show');
    });

    $("#page-content-wrapper").on("click", ".btn-remove", function() {
      $("#modal-form").find('form').trigger("reset");
      $(".sales_user_undropdown").removeClass("hide");
      
      $tr = $(this).parent().parent();
      $id = $tr.data('cpd_id');
     
      $(".sales_user_dropdown").removeAttr('name');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      $(".btn-submit").removeClass("hide");
      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
      $(".modal-title").html("<i class='fa fa-plus'></i> Add CPD Hours");
      $("#modal-multiple").modal('show');
    });

    $("#page-content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('cpd_id');
      var name = $(this).parent().parent().find('td:nth-child(3)').html();

      $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
      
      dialog('CPD Deletion', 'Delete CPD?', "{{ url('sales/cpd/delete') }}", id);
    });

    $("#page-content-wrapper").on("click", ".btn-add", function() {

      $("#row-cpdfromdate").removeAttr("disabled","true");
      $("#row-cpdtodate").removeAttr("disabled","true");
      $("#row-cpd_status").removeAttr("disabled","false");
      $("#row-cpd_period").removeAttr("disabled","false");
      $("#row-cpd_tracking_period").removeAttr("disabled","false");
      $("#row-cpd_hours_req_skills").removeAttr("disabled","false");
      $("#row-cpd_hours_req_knowledge").removeAttr("disabled","false");
      $("#row-total_cpd_hours_skills").removeAttr("disabled","false");
      $("#row-total_cpd_hours_knowledge").removeAttr("disabled","false");
      $("#row-total_cpd_hours_final").removeAttr("disabled","false");
      $("#row-cpd_hours_req_knowledge").removeAttr("disabled","true");
      

      $("#row-cpd_tracking_period_from").removeAttr("disabled","true");
      $("#row-cpd_tracking_period_to").removeAttr("disabled","true");

      $("#row-cpd_hours_req_rules_and_regulations").removeAttr("disabled","true");
      $("#row-total_cpd_hours_rules_and_regulations").removeAttr("disabled","true");
      $("#row-course").removeAttr("disabled","true");
      $("#row-short_fall").removeAttr("disabled","true");
      $("#modal-form").find('form').trigger("reset");
      $(".sales_user_undropdown").removeClass("hide");
      $(".sales_user_dropdown").addClass("hide");
      $(".sales_user_undropdown").attr('name','user_id');
      
      $tr = $(this).parent().parent();
      $name = $tr.data('name');
      $id = $tr.data('id');

      $("#row-user_id").val($id);
     
      $(".sales_user_dropdown").removeAttr('name');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      //$("#row-id").val("");
      $(".btn-submit").removeClass("hide");

      $(".cpd-input").val(0);
      $("#row-cpd_hours_req_skills").val({{$settings_cpd_structured->no_of_hours}});
      $("#row-cpd_hours_req_rules_and_regulations").val({{$settings_cpd_ethics->no_of_hours}});
      $("#row-cpd_hours_req_knowledge").val({{$settings_cpd_rules_and_regulation->no_of_hours}});
      $("#row-cpd_hours_req_gi").val({{$settings_cpd_gi->no_of_hours}});

      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
      $(".modal-title").html("<i class='fa fa-plus'></i> Add CPD Hours - "+$name);
      $("#modal-form").modal('show');
    });

    $("#page-content-wrapper").on("click", ".btn-view", function() {

      $("#modal-form").find('form').trigger("reset");
      $(".sales_user_undropdown").removeClass("hide");
      $(".sales_user_dropdown").addClass("hide");
      var btn = $(this);
      $_token = "{{ csrf_token() }}";

      $tr = $(this).parent().parent();
      $name = $tr.data('name');
      $id = $tr.data('cpd_id');

      btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

      $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
      $("#modal-form").find('form').trigger("reset");
      $("#row-id").val("");

      $.post("{{ url('sales/cpd/view') }}", { id: $id, _token: $_token }, function(response) {

        if(!response.error) {

          var cpd_start = response.cpd_starting_date.substring(0,10).split('-');
          var cpd_end = response.cpd_ending_date.substring(0,10).split('-');
          $('#row-cpdfromdate').val(cpd_start[1]+'/'+cpd_start[2]+'/'+cpd_start[0]);
          $('#row-cpdtodate').val(cpd_end[1]+'/'+cpd_end[2]+'/'+cpd_end[0]);

          $.each(response, function(index, value) {
            if(index == "shortfall") {
              $("#row-short_fall").val(formatNumber(value));
            }

            var field = $("#row-" + index);
            if(field.length > 0) {
              field.val(value);
              $('#row-cpd_status['+response.cpd_status+']').attr("selected", "selected");
            }
          });

          $(".sales_user_id").val("");
          $("#row-cpd_status").attr("disabled","true");
          $("#row-cpd_period").attr("disabled","true");
          $("#row-cpd_tracking_period").attr("disabled","true");
          $("#row-cpd_hours_req_skills").attr("disabled","true");

          $("#row-cpdfromdate").attr("disabled","true");
          $("#row-cpdtodate").attr("disabled","true");

          $("#row-cpd_tracking_period_from").attr("disabled","true");
          $("#row-cpd_tracking_period_to").attr("disabled","true");

          $("#row-cpd_hours_req_knowledge").attr("disabled","true");
          $("#row-cpd_hours_req_rules_and_regulations").attr("disabled","true");
          $("#row-course").attr("disabled","true");
          $("#row-short_fall").attr("disabled","true");

          $("#row-total_cpd_hours_skills").attr("disabled","true");
          $("#row-total_cpd_hours_knowledge").attr("disabled","true");
          $("#row-total_cpd_hours_final").attr("disabled","true");
          $("#row-total_cpd_hours_rules_and_regulations").attr("disabled","true");
          $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
          //$("#row-id").val("");
          $(".sales_user_id").val("");
          $("#modal-form").find('input').removeAttr('readonly', 'readonly');
          //$(".modal-header").removeAttr("class").addClass("modal-header addnew-bg");
          $(".modal-title").html("<i class='fa fa-plus'></i> Edit CPD Hours");
          $("#modal-form").modal('show');
          // show form
          $("#modal-form").modal('show');

          $(".btn-submit").addClass("hide");
        } else {
          status(false, response.error, 'alert-danger');
        }

        btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
      }, 'json');
    });

    $("#page-content-wrapper").on("click", ".btn-edit", function() {
      $("#row-cpdfromdate").removeAttr("disabled","true");
      $("#row-cpdtodate").removeAttr("disabled","true");
      
      $("#row-cpd_status").removeAttr("disabled","false");
      $("#row-cpd_period").removeAttr("disabled","false");
      $("#row-cpd_tracking_period").removeAttr("disabled","false");
      $("#row-cpd_hours_req_skills").removeAttr("disabled","false");
      $("#row-cpd_hours_req_knowledge").removeAttr("disabled","false");
      $("#row-total_cpd_hours_skills").removeAttr("disabled","false");
      $("#row-total_cpd_hours_knowledge").removeAttr("disabled","false");
      $("#row-total_cpd_hours_final").removeAttr("disabled","false");
      $("#row-cpd_hours_req_knowledge").removeAttr("disabled","true");
      $("#row-cpd_tracking_period_from").removeAttr("disabled","true");
      $("#row-cpd_tracking_period_to").removeAttr("disabled","true");
      $("#row-cpd_hours_req_rules_and_regulations").removeAttr("disabled","true");
      $("#row-total_cpd_hours_rules_and_regulations").removeAttr("disabled","true");
      $("#row-course").removeAttr("disabled","true");
      $("#row-short_fall").removeAttr("disabled","true");
      $("#modal-form").find('form').trigger("reset");
      $(".sales_user_undropdown").removeClass("hide");
      $(".sales_user_dropdown").addClass("hide");
      $(".btn-submit").removeClass("hide");
      $(".sales_user_undropdown").attr('name','sales_id');
      
      var btn = $(this);
      $tr = $(this).parent().parent();
      $user_id = $tr.data('user_id');

      $id = $tr.data('cpd_id');

      btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

      $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
      $("#modal-form").find('form').trigger("reset");
      $("#row-id").val("");

      $.post("{{ url('sales/cpd/view') }}", { id: $id, _token: $_token }, function(response) {

        if(!response.error) {

        var cpd_start = response.cpd_starting_date.substring(0,10).split('-');
        var cpd_end = response.cpd_ending_date.substring(0,10).split('-');
        $('#row-cpdfromdate').val(cpd_start[1]+'/'+cpd_start[2]+'/'+cpd_start[0]);
        $('#row-cpdtodate').val(cpd_end[1]+'/'+cpd_end[2]+'/'+cpd_end[0]);

          $.each(response, function(index, value) {

            var field = $("#row-" + index);

            if(index == "shortfall"){
              $("#row-short_fall").val(formatNumber(value));
            }

            if(field.length > 0) {
              if (field.parent().hasClass("date")) {
                $("#date-" + index).data("DateTimePicker").date(moment.utc(value));
              } else if (field.parent().hasClass("decimal")) {
                field.val(formatNumber(value));
              } else {
                field.val(value);
              }
            }

            $('#row-cpd_status['+response.cpd_status+']').attr("selected", "selected");
          });

          $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");

          $(".sales_user_id").val("");
          $("#modal-form").find('input').removeAttr('readonly', 'readonly');
          $(".modal-title").html("<i class='fa fa-plus'></i> Edit CPD Hours");
          $("#modal-form").modal('show');
        } else {
          status(false, response.error, 'alert-danger');
        }

        btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
      }, 'json');
    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

      $('.loading-pane').removeClass('hide');
      $("#row-page").val($(this).html());
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      var body = $('#rows');
      $search = $("#row-search").val();
      $structured_req = "{{ number_format($settings_cpd_structured->no_of_hours, 2) }}";
      $ethics_req = "{{ number_format($settings_cpd_ethics->no_of_hours, 2) }}";
      $rules_and_regulations_req = "{{ number_format($settings_cpd_rules_and_regulation->no_of_hours, 2) }}";
      $total_req = "{{ number_format($settings_cpd_structured->no_of_hours + $settings_cpd_ethics->no_of_hours + $settings_cpd_rules_and_regulation->no_of_hours, 2)}}";

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

        $.each(response.rows.data, function(index, row) {

          body += '<tr data-id="'+row.user_id+'" data-name = "'+row.name+'" data-cpd_id="'+row.cpd_id+'">'+
                    '<td><a class="btn btn-xs btn-table btn-expand" data-code = "'+row.code+'"><i class="fa fa-plus"></i></a>&nbsp;'+row.name+'</td>'+
                    '<td>'+(row.supervisor_name ? row.supervisor_name:row.name)+'</td>'+
                    '<td>'+row.code+'</td>'+
                    '<td style="text-align:center;">'+formatNumber($structured_req)+'</td>'+
                    '<td style="text-align:center;">'+formatNumber($ethics_req)+'</td>'+
                    '<td style="text-align:center;">'+formatNumber($rules_and_regulations_req)+'</td>'+
                    '<td style="text-align:center;">'+formatNumber($total_req)+'</td>'+
                    '<td style="text-align:center;">'+formatNumber($gi_req)+'</td>'+
                    '<td style="text-align:center;">'+formatNumber($total_req - row.cpd_hours_both)+'</td>'+
                    '<td class="rightalign">'+
                        '<button type="button" class="btn-add btn btn-xs btn-default"><i class="fa fa-clock-o"></i></button>'+
                      '</td>';
        });

        $('#rows').html(body);
        $('#row-pages').html(response.pages);
        $('.loading-pane').addClass('hide');

      });
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");

        $('.loading-pane').removeClass('hide');
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();
        $search = $("#row-search").val();
        $structured_req = "{{ number_format($settings_cpd_structured->no_of_hours, 2) }}";
        $ethics_req = "{{ number_format($settings_cpd_ethics->no_of_hours, 2) }}";
        $rules_and_regulations_req = "{{ number_format($settings_cpd_rules_and_regulation->no_of_hours, 2) }}";
        $total_req = "{{ number_format($settings_cpd_structured->no_of_hours + $settings_cpd_ethics->no_of_hours + $settings_cpd_rules_and_regulation->no_of_hours, 2)}}";
        $gi_req = "{{ number_format($settings_cpd_gi->no_of_hours, 2) }}";

        var body = $("#rows");
         $.post("{{ $refresh_route }}", { per: $per,page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

          $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="'+row.user_id+'" data-name = "'+row.name+'" data-cpd_id="'+row.cpd_id+'">'+
                      '<td><a class="btn btn-xs btn-table btn-expand" data-code = "'+row.code+'"><i class="fa fa-plus"></i></a>&nbsp;'+row.name+'</td>'+
                      '<td>'+(row.supervisor_name ? row.supervisor_name:row.name)+'</td>'+
                      '<td>'+row.code+'</td>'+
                      '<td style="text-align:center;">'+formatNumber($structured_req)+'</td>'+
                      '<td style="text-align:center;">'+formatNumber($ethics_req)+'</td>'+
                      '<td style="text-align:center;">'+formatNumber($rules_and_regulations_req)+'</td>'+
                      '<td style="text-align:center;">'+formatNumber($total_req)+'</td>'+
                      '<td style="text-align:center;">'+formatNumber($gi_req)+'</td>'+
                      '<td style="text-align:center;">'+formatNumber($total_req - row.cpd_hours_both)+'</td>'+
                      '<td class="rightalign">'+
                          '<button type="button" class="btn-add btn btn-xs btn-default"><i class="fa fa-clock-o"></i></button>'+
                        '</td>';
          });

          $('#rows').html(body);
          $('#row-pages').html(response.pages);
          $('.loading-pane').addClass('hide');
            
        });
      }
    });

    $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
      $("#modal-multiple").find("input").val("");
      $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
      $(".modal-multiple-body").html("Are you sure you want to remove the selected cpd/s?");
      $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
      $("#modal-multiple").modal("show");
    });

    $("#modal-multiple-button").click(function() {
      var ids = [];
      $_token = "{{ csrf_token() }}";

      $("input:checked").each(function(){
          ids.push($(this).val());
      });

      if (ids.length == 0) {
        status("Error", "Select cpds first.", 'alert-danger');
      } else {
        $.post("{{ url('sales/cpd/remove') }}", { ids: ids, _token: $_token }, function(response) {
          refresh();
        }, 'json');
      }
    });


    $('#row-search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            search();
        }
    });

	</script>
@stop

@section('content')
  <!-- Page Content -->
  <div id="page-content-wrapper" class="response">
  <!-- Title -->
    <div class="container-fluid main-title-container container-space">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">CPD MANAGEMENT</h3>
        <h5 class="bread-crumb">SUMMARY AND REPORTS</h5>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <button type="button" onclick = "refresh()" class="btn btn-sm btn-success borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalimport"><i class="fa fa-refresh"></i> Refresh</button>
        <button type="button" class="btn btn-sm btn-remove-select btn-danger borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-minus"></i> Remove</button>
        <button type="button" class="btn btn-sm btn-new btn-warning borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalimport"><i class="fa fa-clock-o"></i> Add</button> 
      </div>
    </div>
    <div class="container-fluid default-container container-space">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
              <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                <div class="input-group">
                <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                </span>
              </div>
              </div>
            </div>
          </div>
    </div>
    <div class="container-fluid content-table col-xs-12" style="border-top:1px #e6e6e6 solid;">
      <div class="table-responsive block-content tbblock col-xs-12">
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div> 
        <div class="table-responsive">
          <table class="table table-striped table-pagination">
            <thead class="tbheader">
              <tr>
                <th class="th-sort" nowrap data-sort="name"><i></i> AGENT NAME</th>
                <th class="th-sort" nowrap data-sort="supervisor_name"><i></i> REPORTING TO</th>
                <th class="th-sort" nowrap data-sort="code"><i></i> AGENT CODE</th>
                <th class="th-sort" nowrap data-sort="cpd_hours_skills"><i></i><center>TOTAL CPD HOURS REQ<br>(STRUCTURED)</center></th>
                <th class="th-sort" nowrap data-sort="cpd_hours_knowledge"><i></i><center>TOTAL CPD HOURS REQ<br>(ETHICS)</center></th>
                <th class="th-sort" nowrap data-sort="cpd_hours_rules_and_regulations"><i></i><center>TOTAL CPD HOURS REQ<br>(RULES & REGULATIONS)</center></th>
                <th class="th-sort" nowrap data-sort="cpd_hours_both"><i></i><center>TOTAL CPD HOURS REQ<br>(S,E & R)</center></th>
                <th class="th-sort" nowrap data-sort="cpd_hours_gi"><i></i><center>TOTAL CPD HOURS REQ<br>(GI)</center></th>
                <th class="th-sort" nowrap data-sort="cpd_hours_both"><i></i><center>TOTAL<br>SHORTFALL</center></th>
                <th class="th-sort rightalign"><i></i> TOOLS</th>
              </tr>
            </thead>
            <tbody id="rows">
              @foreach($rows as $row)
              <tr data-id="{{ $row->user_id }}" data-name = "{{ $row->name }}" data-cpd_id="{{ $row->cpd_id}}">
                <td><a class="btn btn-xs btn-table btn-expand"data-code="{{ $row->code }}"><i class="fa fa-plus"></i></a> {{ $row->name }}</td>
                <td>{{ $row->supervisor_name?:$row->name }}</td>
                <td>{{ $row->code }}</td>
                <td style="text-align:center;">{{ number_format($settings_cpd_structured->no_of_hours, 2)?:0 }}</td>
                <td style="text-align:center;">{{ number_format($settings_cpd_ethics->no_of_hours, 2)?:0 }}</td>
                <td style="text-align:center;">{{ number_format($settings_cpd_rules_and_regulation->no_of_hours, 2)?:0}}</td>
                <td style="text-align:center;">{{ number_format($settings_cpd_structured->no_of_hours + $settings_cpd_ethics->no_of_hours + $settings_cpd_rules_and_regulation->no_of_hours, 2)}}</td>
                <td style="text-align:center;">{{ number_format($settings_cpd_gi->no_of_hours, 2)?:0}}</td>
                <td style="text-align:center;">{{ number_format(($settings_cpd_structured->no_of_hours + $settings_cpd_ethics->no_of_hours + $settings_cpd_rules_and_regulation->no_of_hours) - ($row->cpd_hours_both),2) }}</td>
                <td class="rightalign">
                  <button type="button" class="btn-add btn btn-xs btn-default"><i class="fa fa-clock-o"></i></button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>       
    </div>
    <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
    <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="50">100</option>
      </select>
    </div>
    <div class="text-center">
      <input type="hidden" id="row-page" value="1">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-order" value="">
    </div>
  </div>

@include('user-management.sales-users.cpd-management.form')

@stop