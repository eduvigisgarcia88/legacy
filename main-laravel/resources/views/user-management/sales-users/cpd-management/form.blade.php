<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
       <div class="modal-content borderzero">
           <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
           </div>
          {!! Form::open(array('url' => 'sales/cpd/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
           <div class="modal-header tbheader">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Modal title</h4>
           </div>
           <div class="modal-body">
           <div id="form-notice"></div>
            <div class="form-group sales_user_dropdown">
              <label for="row-sales_id" class="col-sm-3 control-label">Agents</label>
            <div class="col-sm-9 error-sales_id ">
              <select name="sales_id[]" id="row-sales_id" multiple="multiple" class="form-control input-md borderzero sales_id multiselect" style="width: 100%;">
                @foreach($sales_users as $row)
                  <option value="{{ $row->id }}">[{{ ($row->salesforce_id ? $row->salesforce_id : $row->code) }}] {{ $row->name }}</option>
                @endforeach
              </select> 
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <input class="sales_user_undropdown" type="hidden" name="user_id" id="row-user_id">
            <div class="form-group select-supervisor">
              <label for="row-cpd_status" class="col-sm-3 control-label">New or Existing</label>
            <div class="col-sm-9 error-cpd_status ">
              <select class="form-control" id="row-cpd_status" name="cpd_status">
                <option value="" class="hide">Select:</option>
                <option value="New">New</option>
                <option value="Existing">Existing</option>
              </select>
              <sup class="sup-errors"></sup>
            </div>
            </div>

            {{-- <div class="form-group select-supervisor">
              <label for="row-cpd_period" class="col-sm-3 control-label">CPD Period</label>
            <div class="col-sm-9 decimal error-cpd_period ">
              <input name="cpd_period" id="row-cpd_period" type="text" class="form-control borderzero" value="{{ $today_year }}">
              <sup class="sup-errors"></sup>
            </div>
            </div> --}}

            <!-- CPD RANGE START -->
            <div class="form-group select-supervisor">
              <label for="row-cpd_period" class="col-sm-3 control-label">CPD Period</label>
            <div class="col-sm-4 decimal error-datefromcpd">
              <div class="input-group date" id="datefromcpd">
                  <input type="text" name="datefromcpd" class="form-control borderzero" id="row-cpdfromdate" maxlength="28" placeholder="MM/DD/YYYY">
                  <span class="input-group-addon">
                    <span><i class="fa fa-calendar"></i></span>
                  </span>
                </div>
            </div>

            <div class="col-sm-1">
              <p style="padding-top: 10px;">TO</p>
            </div>

            <div class="col-sm-4 decimal error-datetocpd">
              <div class="input-group date" id="datetocpd">
                  <input type="text" name="datetocpd" class="form-control borderzero" id="row-cpdtodate" maxlength="28" placeholder="MM/DD/YYYY">
                  <span class="input-group-addon">
                    <span><i class="fa fa-calendar"></i></span>
                  </span>
                </div>
            </div>
            </div>
            <!-- CPD RANGE END -->

             <div class="form-group">
              <label for="row-course" class="col-sm-3 control-label">Course</label>
            <div class="col-sm-9 error-course ">
              <input name="course" id="row-course" type="text" class="form-control borderzero" id="">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-cpd_hours_req_skills" class="col-sm-3 control-label">CPD Hours Req(Structured)</label>
            <div class="col-sm-9 decimal error-cpd_hours_req_skills ">
              <input name="cpd_hours_req_skills" id="row-cpd_hours_req_skills" onkeyup="compute_hours()" type="number" step="0.01" class="form-control cpd-input borderzero">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-cpd_hours_req_knowledge" class="col-sm-3 control-label">CPD Hours Req(Ethics)</label>
            <div class="col-sm-9 decimal error-cpd_hours_req_knowledge ">
              <input name="cpd_hours_req_knowledge" id="row-cpd_hours_req_knowledge" onkeyup="compute_hours()"type="number" step="0.01" class="form-control cpd-input borderzero">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-cpd_hours_req_rules_and_regulations" class="col-sm-3 control-label">CPD Hours Req(Rules and Regulations)</label>
            <div class="col-sm-9 decimal error-cpd_hours_req_rules_and_regulations ">
              <input name="cpd_hours_req_rules_and_regulations" id="row-cpd_hours_req_rules_and_regulations" onkeyup="compute_hours()" type="number" step="0.01" class="form-control cpd-input borderzero">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-cpd_hours_req_gi" class="col-sm-3 control-label">CPD Hours Req(GI)</label>
            <div class="col-sm-9 decimal error-cpd_hours_req_gi ">
              <input name="cpd_hours_req_gi" id="row-cpd_hours_req_gi" onkeyup="compute_hours()" type="number" step="0.01" class="form-control cpd-input borderzero">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-total_cpd_hours_skills" class="col-sm-3 control-label">Total CPD Hours(Structured)</label>
            <div class="col-sm-9 decimal error-total_cpd_hours_skills">
              <input name="total_cpd_hours_skills" id="row-total_cpd_hours_skills" onkeyup="compute_hours()" type="number" step="0.01" class="form-control cpd-input borderzero" id="">
              <sup class=" sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-total_cpd_hours_knowledge" class="col-sm-3 control-label">Total CPD Hours(Ethics)</label>
            <div class="col-sm-9 decimal error-total_cpd_hours_knowledge">
              <input name="total_cpd_hours_knowledge" id="row-total_cpd_hours_knowledge"onkeyup="compute_hours()" type="number" step="0.01" class="form-control cpd-input borderzero"  id="">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
            <label for="row-total_cpd_hours_rules_and_regulations" class="col-sm-3 control-label">Total CPD Hours(Rules and Regulations)</label>
            <div class="col-sm-9 decimal error-total_cpd_hours_rules_and_regulations">
              <input name="total_cpd_hours_rules_and_regulations" id="row-total_cpd_hours_rules_and_regulations" onkeyup="compute_hours()" type="number" step="0.01" readonly="" class="form-control cpd-input borderzero">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
            <label for="row-total_cpd_hours_gi" class="col-sm-3 control-label">Total CPD Hours(GI)</label>
            <div class="col-sm-9 decimal error-total_cpd_hours_gi">
              <input name="total_cpd_hours_gi" id="row-total_cpd_hours_gi" onkeyup="compute_hours()"  onchange="compute_hours()" type="number" step="0.01" readonly="" class="form-control cpd-input borderzero">
              <sup class="sup-errors"></sup>
            </div>
            </div>
            <div class="form-group">
              <label for="row-total_cpd_hours_final" class="col-sm-3 control-label">Total CPD Hours</label>
            <div class="col-sm-9 decimal">
              <input name="total_cpd_hours_final" id="row-total_cpd_hours_final" type="number" step="0.01" readonly="" class="form-control cpd-input borderzero">
            </div>
            </div>
            <div class="form-group hide">
              <label for="row-short_fall" class="col-sm-3 control-label">Shortfall/Excess Hours</label>
            <div class="col-sm-9 decimal">
              <input name="short_fall" id="row-short_fall" type="number" step="0.01" readonly="" class="form-control cpd-input borderzero">
            </div>
            </div>
            </div>
            <div class="modal-footer borderzero">
            <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero btn-close" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
    </div>
  </div>