<!-- modal add product -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create New Group</h4>
              </div>
              <div class="modal-body">
               <div id="product-notice"></div>
                <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">DIRECTOR CODE</label>
                <div class="col-sm-9 col-xs-7 error-group_code ">
                <input type="text" name="code" class="form-control borderzero" id="row-code" placeholder="">
                <sup class="sup-errors"></sup>
              </div>
              </div>
                <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">OWNER</label>
                <div class="col-sm-9 col-xs-7 error-owner ">
                <select class="form-control" id="row-owner" name="owner">
                </select>
                <sup class="sup-errors"></sup>
              </div>
              </div>
                <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">OWNER MANAGER CODE</label>
                <div class="col-sm-9 col-xs-7 error-manager_code ">
                <input type="text" name="manager_code" class="form-control borderzero" id="row-manager_code" placeholder="">
                </select>
                <sup class="sup-errors"></sup>
              </div>
              </div>
                <div class="form-group form-designation hide">
                  <label for="designation" class="col-sm-3 col-xs-5">SELECT NEW DESIGNATION</label>
                  <div class="col-sm-9 col-xs-7 error-designation">
                    <select class="form-control" id="row-designation" name="designation">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>                   
                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero btn-close" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>


<!-- modal add product -->
<div class="modal fade" id="modal-code" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-code" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/supervisor/save-code', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_code')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="code-title">Edit Supervisor</h4>
              </div>
              <div class="modal-body">
                <div id="code-notice"></div>
                <div class="form-group">
                  <label for="unit_code" class="col-sm-3 col-xs-5">MANAGER CODE</label>
                  <div class="col-sm-9 col-xs-7 error-unit_code">
                    <input type="text" name="unit_code" class="form-control borderzero" id="code-unit_code" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="user_id" class="col-sm-3 col-xs-5">SUPERVISOR</label>
                  <div class="col-sm-9 col-xs-7 error-user_id">
                    <select class="form-control" id="code-user_id" name="user_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div id="code-designation" class="form-group">

                </div>
                <div id="code-check" class="form-group">

                </div>
                <div id="code-codes" class="form-group">

                </div>
                <div id="code-sup_designation" class="form-group">

                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="code-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>

<!-- modal add product -->
<div class="modal fade" id="modal-relocate" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-relocate" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/supervisor/save-relocate', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_relocate')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="relocate-title">Relocation</h4>
              </div>
              <div class="modal-body">
                <div id="relocate-notice"></div>
                <div class="form-group">
                  <label for="unit_code" class="col-sm-3 col-xs-5">MANAGER CODE</label>
                  <div class="col-sm-9 col-xs-7 error-unit_code">
                    <input type="text" name="unit_code" class="form-control borderzero" id="relocate-unit_code" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="group_id" class="col-sm-3 col-xs-5">GROUP</label>
                  <div class="col-sm-9 col-xs-7 error-group_id">
                    <select class="form-control" id="relocate-group_id" name="group_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div id="relocate-check" class="form-group">

                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="relocate-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>

<!-- modal add product -->
<div class="modal fade" id="modal-demote" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-demote" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/supervisor/save-demote', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_demote')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="demote-title"></h4>
              </div>
              <div class="modal-body">
                <div id="demote-notice"></div>
                <div class="form-group">
                  <label for="group_id" class="col-sm-3 col-xs-5">NEW GROUP</label>
                  <div class="col-sm-9 col-xs-7 error-group_id">
                    <select class="form-control" id="demote-group_id" name="group_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="supervisor_id" class="col-sm-3 col-xs-5">NEW MANAGER CODE</label>
                  <div class="col-sm-9 col-xs-7 error-supervisor_id">
                    <select class="form-control" id="demote-supervisor_id" name="supervisor_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="designation" class="col-sm-3 col-xs-5">NEW DESIGNATION</label>
                  <div class="col-sm-9 col-xs-7 error-designation">
                    <select class="form-control" id="demote-designation" name="designation">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="demote-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-ban"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>


<!-- modal add product -->
<div class="modal fade" id="modal-promote" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-promote" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/advisor/save-promote', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_promote')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="promote-title">Promote Advisor</h4>
              </div>
              <div class="modal-body">
                <div id="promote-notice"></div>
                <div class="form-group">
                  <label for="group_id" class="col-sm-3 col-xs-5">GROUP CODE</label>
                  <div class="col-sm-9 col-xs-7 error-group_id">
                    <select class="form-control" id="promote-group_id" name="group_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="unit_code" class="col-sm-3 col-xs-5">MANAGER CODE</label>
                  <div class="col-sm-9 col-xs-7 error-unit_code">
                    <input type="text" name="unit_code" class="form-control borderzero" id="promote-unit_code" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="designation_id" class="col-sm-3 col-xs-5">NEW DESIGNATION</label>
                  <div class="col-sm-9 col-xs-7 error-designation_id">
                    <select class="form-control" id="promote-designation_id" name="designation_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="promote-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-ban"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>

<!-- modal add product -->
<div class="modal fade" id="modal-transfer" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-transfer" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/advisor/save-transfer', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_transfer')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="transfer-title">Promote Advisor</h4>
              </div>
              <div class="modal-body">
                <div id="transfer-notice"></div>
                <div class="form-group">
                  <label for="group_id" class="col-sm-3 col-xs-5">GROUP CODE</label>
                  <div class="col-sm-9 col-xs-7 error-group_id">
                    <select class="form-control" id="transfer-group_id" name="group_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="supervisor_id" class="col-sm-3 col-xs-5">MANAGER CODE</label>
                  <div class="col-sm-9 col-xs-7 error-supervisor_id">
                    <select class="form-control" id="transfer-supervisor_id" name="supervisor_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="transfer-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-ban"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>


<!-- modal add product -->
<div class="modal fade" id="modal-relocation" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-relocation" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/advisor/save-relocation', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_promotion')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="relocation-title">Promote Advisor</h4>
              </div>
              <div class="modal-body">
                <div id="relocation-notice"></div>
                <div class="form-group">
                  <label for="group_id" class="col-sm-3 col-xs-5">GROUP CODE</label>
                  <div class="col-sm-9 col-xs-7 error-group_id">
                    <select class="form-control" id="relocation-group_id" name="group_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="supervisor_id" class="col-sm-3 col-xs-5">MANAGER CODE</label>
                  <div class="col-sm-9 col-xs-7 error-supervisor_id">
                    <select class="form-control" id="relocation-supervisor_id" name="supervisor_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="relocation-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-ban"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>


<!-- modal add product -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-delete" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'sales/team-management/delete', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_delete')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="delete-title">Group Deletion</h4>
              </div>
              <div class="modal-body">
                <div id="delete-notice"></div>
                <div class="form-group">
                  <label for="group_id" class="col-sm-3 col-xs-5">GROUP CODE</label>
                  <div class="col-sm-9 col-xs-7 error-group_id">
                    <select class="form-control" id="delete-group_id" name="group_id">
                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="delete-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default btn-submit hide-view borderzero"><i class="fa fa-ban"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
       </div>
</div>