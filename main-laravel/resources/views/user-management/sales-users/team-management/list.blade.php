@extends('layouts.master')

@section('scripts')
  <script>

    function refresh() {

    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $("#row-order").val('');
    $("#row-sort").val('');
    $("#row-search").val('');

    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    //console.log($sort);
    $search = $("#row-search").val();
    var body = $("#rows");
    $.post("{{ url('sales/team-management/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, _token: $_token }, function(response) {

    $.each(response.rows, function(index, row) {

            body += '<tr data-id="'+row.id+'">'+
                    '<td><a class="btn btn-table btn-xs btn-expand"><i class="fa fa-plus"></i></a>&nbsp;'+row.code+'</td>'+
                    '<td>&nbsp;'+row.name+'</td>'+
                    '<td class="rightalign">'+
                           '<button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>'+
                           (row.sales_supervisor.length < 2 ? '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete borderzero" data-toggle="tooltip" title="Delete" ><i class="fa fa-trash"></i></button>' : '' ) + 
                    '</td>';
      });
      $loading = false;
      $selectedID = 0;
      $('#rows').html(body);
      $('#row-pages').html(response.pages);
      $('.loading-pane').addClass('hide');
      //$('.fetch-data').html(list);
        
    });    
    } 


    $loading_expand = false;
    $selectedExpandID = 0;
    $loading_item = false;
    $selectedItemID = 0;
        $_token = '{{ csrf_token() }}';

      $("#page-content-wrapper").on("click", ".btn-expand", function() {
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');

        $loading_item = false;
        $selectedItemID = 0;

        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $_token = '{{ csrf_token() }}';


        if ($loading_expand) {
          return;
        }

        $selected = '.row-expand-' + $selectedExpandID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedExpandID) {
          $selectedExpandID = '0';
          return;
        }

        $selectedExpandID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
        $loading_expand = true;

        $.post("{{ url('sales/team-management/get-supervisors') }}", { id: $id, _token: $_token }, function(response) {
          // console.log(response);
          $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-expand-' + $selectedExpandID + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-expand-' + $selectedExpandID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        });
      });

      $("#page-content-wrapper").on("click", ".btn-item-expand", function() {
      $(".btn-item-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-item-expand").html('<i class="fa fa-plus"></i>');

        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $user = $(this).data('user');
        $_token = '{{ csrf_token() }}';


        if ($loading_item) {
          return;
        }

        $selected = '.row-item-expand-' + $selectedItemID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedItemID) {
          $selectedItemID = '0';
          return;
        }

        $selectedItemID = $id;
        $button = $(this); 
        $button.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
        $loading_item = true;

        $.post("{{ url('sales/team-management/get-advisors') }}", { id: $id, user: $user, _token: $_token }, function(response) {

          $tr.after('<tr><td colspan="5" style="padding:10px"><div class="row-item-expand-' + $selectedItemID + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-item-expand-' + $selectedItemID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_item = false;
        });
      });

      $("#demote-group_id").on("change", function() {
        $id = $(this).val();
        $super_id = $("#demote-id").val();
        $("#demote-supervisor_id").html("");

        $.post("{{ url('sales/team-management/supervisor/get-demote-code') }}", { id: $id, super_id: $super_id, _token: $_token }, function(response) {
          $("#demote-supervisor_id").html(response);
        }, 'json');
      });


      //add user
      $("#page-content-wrapper").on("click", ".btn-demote", function() {
        $id = $(this).parent().parent().data('id');
        $("#modal-demote").find('form').trigger("reset");

        var btn = $(this);
        $_token = "{{ csrf_token() }}";
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-ban");

        $("#demote-group_id").html("");

        $.post("{{ url('sales/team-management/supervisor/get-demote') }}", { id: $id, _token: $_token }, function(response) {
          if(!response.error) {

            $("#demote-id").val(response.id);
            $("#demote-supervisor_id").html(response.supervisor_id);
            $(".demote-title").html(response.title);
            $("#demote-group_id").html(response.group_id);
            $("#demote-designation").html(response.designation);
            $("#modal-demote").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-ban");

        }, 'json');

      });


      $("#page-content-wrapper").on("click", ".btn-delete", function() {
        $id = $(this).parent().parent().data('id');
        $("#modal-delete").find('form').trigger("reset");

        var btn = $(this);
        $_token = "{{ csrf_token() }}";
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-trash");

        $("#delete-group_id").html("");

        $.post("{{ url('sales/team-management/get-group') }}", { id: $id, _token: $_token }, function(response) {
          if(!response.error) {

            $("#delete-id").val(response.id);
            $(".delete-title").html(response.title);
            $("#delete-group_id").html(response.group_id);
            $("#modal-delete").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-trash");

        }, 'json');

      });

      //add user
      $("#page-content-wrapper").on("click", ".btn-relocate", function() {
        $id = $(this).parent().parent().data('id');
        $("#modal-relocate").find('form').trigger("reset");

        var btn = $(this);
        $_token = "{{ csrf_token() }}";
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-retweet");

        $("#relocate-group_id").html("");

        $.post("{{ url('sales/team-management/supervisor/get-relocate') }}", { id: $id, _token: $_token }, function(response) {
          if(!response.error) {

            $("#relocate-id").val(response.id);
            $("#relocate-unit_code").val(response.unit_code);
            $("#relocate-group_id").html(response.rows);
            $("#modal-relocate").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-retweet");

        }, 'json');

      });

      //add user
      $("#page-content-wrapper").on("click", ".btn-edit_code", function() {
        $id = $(this).parent().parent().data('id');
        $("#modal-code").find('form').trigger("reset");

        $("#code-check").html("");
        $("#code-codes").html("");
        $("#code-designation").html("");
        $("#code-sup_designation").html("");

        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

        // reset all form fields
        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        $("#modal-code").find('form').trigger("reset");
        var owner = '';

        $.post("{{ url('sales/team-management/supervisor/manipulate') }}", { id: $id, _token: $_token }, function(response) {
          if(!response.error) {
            $.each(response, function(index, value) {

              if (index == "user_id") {
                owner = value;
              }

              var field = $("#code-" + index);
              // field exists, therefore populate
              if(field.length > 0) {
                  field.val(value);    
              }
            });

            // show form
          } else {
            status(false, response.error, 'alert-danger');
          }

        }, 'json');


        $.post("{{ url('sales/team-management/get-owner-supervisor') }}", { id: $id, _token: $_token }, function(response) {
          $("#code-user_id").html(response.rows);
          $("#code-user_id").val(owner);
          $("#modal-code").modal('show');
          $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
          $(".code-title").html(response.title);
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
        }, 'json');
     
      });

      $("#code-user_id").on("change", function() {
        $id = $(this).val();
        $super = $("#code-id").val();
        $("#code-unit_code").val("");

        $("#code-check").html("");
        $("#code-codes").html("");
        $("#code-sup_designation").html("");
        $.post("{{ url('sales/team-management/supervisor/check-if-supervisor') }}", { id: $id, super: $super, _token: $_token }, function(response) {
          $("#code-check").html(response.rows);
          $("#code-designation").html(response.designation);
          $("#code-unit_code").val(response.code);
        }, 'json');
      });

      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        $id = $(this).parent().parent().data('id');
        
        $("#modal-form").find('form').trigger("reset");
        $(".form-designation").addClass("hide");

        var btn = $(this);
        $_token = "{{ csrf_token() }}";
        var owner = '';
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
          // reset all form fields
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-form").find('form').trigger("reset");

              $.post("{{ url('sales/team-management/manipulate') }}", { id: $id, _token: $_token }, function(response) {
                  //console.log(response);
                  if(!response.error) {
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if (index == "owner") {
                            owner = value;
                          }
                          if(field.length > 0) {
                              field.val(value);
                          }
                      });

                      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
                      $(".modal-title").html("<i class='fa fa-plus'></i> Edit Director Code");

                      // show form
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');


        $.post("{{ url('sales/team-management/get-owner') }}", { id: $id, _token: $_token }, function(response) {
          $("#row-owner").html(response);
          $("#row-owner").val(owner);
          $("#modal-form").modal('show');
        }, 'json');

      });



      $("#relocation-group_id").on("change", function() {
        $id = $(this).val();
        $super_id = $("#relocation-id").val();
        $("#relocation-supervisor_id").html("");

        $.post("{{ url('sales/team-management/advisor/get-relocate-code') }}", { id: $id, super_id: $super_id, _token: $_token }, function(response) {
          $("#relocation-supervisor_id").html(response);
        }, 'json');
      });



      $("#page-content-wrapper").on("click", ".btn-relocate-advisor", function() {
        $id = $(this).parent().parent().data('id');

        $("#modal-relocation").find('form').trigger("reset");
        $("#relocation-group_id").html("");
        $("#relocation-supervisor_id").html("");

        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-retweet");

          $("#modal-relocation").find('form').trigger("reset");

              $.post("{{ url('sales/team-management/advisor/get-relocate') }}", { id: $id, _token: $_token }, function(response) {

                  if(!response.error) {

                    $("#relocation-id").val(response.id);
                    $(".relocation-title").html(response.title);
                    $("#relocation-supervisor_id").html(response.supervisor_id);
                    $("#relocation-group_id").html(response.group_id);

                    $("#modal-relocation").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-retweet");
              }, 'json');
    
      });


      $("#page-content-wrapper").on("click", ".btn-promote-advisor", function() {
        $id = $(this).parent().parent().data('id');
        
        $("#modal-promote").find('form').trigger("reset");
        $("#promote-group_id").html("");
        $("#promote-designation_id").html("");

        var btn = $(this);
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-arrow-up");

          $("#modal-promote").find('form').trigger("reset");

              $.post("{{ url('sales/team-management/advisor/manipulate') }}", { id: $id, _token: $_token }, function(response) {

                  if(!response.error) {
                    $("#promote-group_id").html(response.groups);
                    $("#promote-designation_id").html(response.designation_id);
                    $(".promote-title").html(response.title);

                      $.each(response.row, function(index, value) {

                          var field = $("#promote-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);    
                          }
                           
                      });

                      $("#modal-promote").modal('show');
                      // show form
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-arrow-up");
              }, 'json');
    
      });


      $("#page-content-wrapper").on("click", ".btn-remove-advisor", function() {
        var id = $(this).parent().parent().data('id');
      

      });

      $("#row-group_code").on("change", function() { 
          var id = $("#row-group_code").val();

          $_token = "{{ csrf_token() }}";
          var unit = $("#row-unit_id");
          unit.html("");
          $.post("{{ url('sales/get-units') }}", { id: id, _token: $_token }, function(response) {
            unit.append(response);
         }, 'json');

       });


      $(".btn-create-group").click(function() {

        $(".form-designation").addClass("hide");
        // reset all form fields
        $("#rate").show();
        $("#row-rate").show();
        $("#row-provider_name").hide();
        $("#s2id_row-provider_id").show();
        $("#modal-form").find('form').trigger("reset");
        $("#row-id").val("");

        $.post("{{ url('sales/team-management/get-owner') }}", { _token: $_token }, function(response) {
          $("#row-owner").html(response);
          $("#modal-form").modal('show');
         }, 'json');

        $("#modal-form").find('.hide-view').removeClass('hide');
        $("#modal-form").find('.show-view').addClass('hide');
        $("#modal-form").find('.preview').addClass('hide');
        $("#modal-form").find('input').removeAttr('readonly', 'readonly');

        $("#row-provider_id").select2("val", "");
        $("#photo_row").addClass('hide'); 
        $(".change").removeClass('hide');

        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Create New Group");
      });

      $("#row-owner").change(function() {
        $id = $(this).val();
        $(".form-designation").addClass("hide");

        $.post("{{ url('sales/team-management/get-manager-code') }}", { id: $id, _token: $_token }, function(response) {
          if (!response.supervisor) {
            $("#row-manager_code").val(response.rows);
          } else {
            $("#row-manager_code").val("");
            $(".form-designation").removeClass("hide");
            $("#row-designation").html(response.rows);
          }
         }, 'json');
      });

      $('[required],[pattern]')
       .parent()
       .append('<span class="valid8"></span>');


      // $("#page-content-wrapper").on("click", ".btn-unit-expand", function() {
      // $(".btn-unit-expand").html('<i class="fa fa-minus"></i>');
      // $(".btn-unit-expand").html('<i class="fa fa-plus"></i>');

      //   $item_id = 0;
      //   $type_id = 0;
      //   $loading_item = false;
      //   $loading_type = false;

      //   if ($loading_agent){
      //     return;
      //   }

      //   $tr = $(this).parent().parent();
      //   $id = $(this).data('batch');
      //   $user = $(this).data('user');
      //   $_token = '{{ csrf_token() }}';
      //   $status = $(this).data('status');

      //   $selected = '.row-unit-expand-' + $agent_id;
      //     $($selected).slideUp(function () {
      //       $($selected).parent().parent().remove();
      //     });

      //   if ($user == $agent_id) {
      //     $agent_id = 0;
      //     return
      //   }

      //   $agent_id = $user;
      //   $button = $(this); 
      //   $button.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
      //   $loading_agent = true;

      //   $.post("{{ url('sales/team-management/get-users') }}", { id: $id, user: $user, status: $status, _token: $_token }, function(response) {
      //     if (!response.error) {
      //       $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-unit-expand-' + $agent_id + '" style="display:none;">' + response + '</div></td></tr>');

      //       $('.row-unit-expand-' + $agent_id).slideDown();
      //       $button.html('<i class="fa fa-minus"></i>');
      //       $loading_agent = false;
      //     } else {
      //       status(false, response.error, 'alert-danger');
      //     }

      //   });

      // });
    $(document).ready(function(){

      var keyword = $(location).attr('href');
            var res = keyword.split("#");
          if(res[1] == "new"){
             $(".btn-create-group").trigger("click");
          }
    });
      
  
  </script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">TEAM MANAGEMENT</h3>
        <h5 class="bread-crumb">USER MANAGEMENT<h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right" onClick="refresh();"><i class="fa fa-refresh"></i> Refresh</button>
          <button type="button" class="btn btn-sm hide btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i> Disable</button>     
          <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-create-group" data-toggle="modal" data-target="#myModaladd"><i class="fa fa-plus"></i> Create Group</button>
    </div>
    </div>
  <div class="container-fluid default-container container-space hide">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
  <form class="form-horizontal" >     
    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
              <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>RANK</strong></p></label>
                <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                      <select class="form-control borderzero select" id="sel1">
                        <option>Sales Agent</option>
                        <option>Assistant Manager</option>
                        <option>Manager</option>
                        <option>Assistant Director</option>
                        <option>Director</option>
                        <option>Partner</option>
                      </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH</strong></p></label>
                      <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <input type="text" class="form-control borderzero select" placeholder="Search for..." id="search">
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
              <div class="form-group">
                    <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>STATUS</strong></p></label>
                      <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class="select form-control borderzero" id="sel1" >
                              <option>ENABLED</option>
                              <option>DISABLED</option>
                            </select>
                          </div>
                      </div>
                  </div>
                  <button type="button" class="btn btn-default borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModal" style="margin-right: 14px;">FILTER</button>
                </form>  
              </div>
        <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
         <div class="block-content tbblock col-xs-12">  
           <div class="loading-pane hide">
            <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
          </div>
            <div class="table-responsive">
              <table class="table table-striped" id="tblData">
                <thead class="tbheader">
                  <tr>
                    <th><i></i> DIRECTOR CODE</th>
                    <th><i></i> OWNER</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>
                <tbody id="rows">
                  @foreach ($rows as $row)
                  <tr data-id="{{ $row->id }}">
                    <td><a class="btn btn-table btn-xs btn-expand"><i class="fa fa-plus"></i></a> {{ $row->code }}</td>
                    <td>{{ $row->name }}</td>
                    <td class="rightalign">
                      <button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>
                      @if (count($row->salesSupervisor) < 2)
                      <button type="button" class="btn btn-xs btn-table btn-delete borderzero" data-toggle="tooltip" title="Delete" ><i class="fa fa-trash"></i></button>
                      @endif
                      <!--<button type="button" class="btn btn-xs btn-table borderzero" data-toggle="tooltip" title="Movement History"><i class="fa fa-book"></i></button>
                      <button type="button" class="btn btn-xs btn-table borderzero" data-toggle="tooltip" title="Designate"><i class="fa fa-map-marker"></i></button>-->
                     <!--  <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}  borderzero" data-toggle="tooltip" title="{{ ($row->status == 1) ? 'Disable' : 'Enable' }}"><i class="fa fa-{{ ($row->status == 1) ? 'adjust' : 'check-circle' }}"></i></button> -->
                     <!--  <input id="checkbox" type="checkbox" value="{{ $row->id }}"> -->
                    </td>
                  </tr>
                  @endforeach
            <!--  <tr>
                    <td>12345</td>
                    <td>SLS-12345</td>
                    <td class="rightalign">
                      <a href="{{ url('sales/view') }}" type="button" class="btn-color btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                    <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#myModal">Movement History</button>
                    <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#myModaledit">Designate</button>
                    <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i> Disable</button>
                    <input id="checkbox" type="checkbox" value="">
                    </td>
                  </tr> -->
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
        <!-- /#page-content-wrapper -->

@include('user-management.sales-users.team-management.form')
@stop