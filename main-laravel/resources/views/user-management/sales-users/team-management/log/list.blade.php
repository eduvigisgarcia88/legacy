@extends('layouts.master')

@section('scripts')
  <script>

    $_token = '{{ csrf_token() }}';
    // refresh the list
    function refresh() {
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $per = $("#row-per").val();
      //$search = $("#row-search").val();
      //$status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
       
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();
        
        $.each(response.rows.data, function(index, row) {
         
            body += '<tr data-id="' + row.id + '">'+
                '<td>' + row.sales_id + '</td>' + 
                '<td>' + row.code + '</td>' + 
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at+ '</td>' +
                '<td>' + row.edit_name + '</td>' +
                // '<td class="rightalign">'+
                //         '<a href="{{ url("policy/'+ row.id +'/view") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                //         '&nbsp;<a href="{{ url("policy/orphan-policies/edit") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>'+
                //         '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>'+
                //         '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>'+ 
                //         '&nbsp;<input id="checkbox" type="checkbox" value="">'+
                // '</td>' +
            '</tr>';

        });
        
        $("#modal-form").find('form').trigger("reset");

         table.html(body);
        $('#row-pages').html(response.pages);

        // $(".tablesorter").trigger('update');

        // // populate cache
        // $('#my-table').find('tbody tr').each(function() {
        //   $('#my-table').dataTable().fnAddData(this);
        // });

        loading.addClass("hide");

      }, 'json');
    }

    function search() {

      $('.loading-pane').removeClass('hide');

      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $per = $("#row-per").val();
      //$search = $("#row-search").val();
      //$status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        console.log(response);
        var body = "";
       
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();
        
        $.each(response.rows.data, function(index, row) {
           console.log(response);
            body += '<tr data-id="' + row.id + '">'+
                '<td>' + row.sales_id + '</td>' + 
                '<td>' + row.code + '</td>' + 
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at+ '</td>' +
                '<td>' + row.edit_name + '</td>' +
                // '<td class="rightalign">'+
                //         '<a href="{{ url("policy/'+ row.id +'/view") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                //         '&nbsp;<a href="{{ url("policy/orphan-policies/edit") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>'+
                //         '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>'+
                //         '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>'+ 
                //         '&nbsp;<input id="checkbox" type="checkbox" value="">'+
                // '</td>' +
                '</tr>';

        });
        $("#modal-form").find('form').trigger("reset");

         table.html(body);
        $('#row-pages').html(response.pages);

        // $(".tablesorter").trigger('update');

        // // populate cache
        // $('#my-table').find('tbody tr').each(function() {
        //   $('#my-table').dataTable().fnAddData(this);
        // });

        loading.addClass("hide");

      }, 'json');
    

    }

     $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";

          $.each(response.rows.data, function(index, row) {

              body += '<tr>' + 
                '<td>' + row.sales_id + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at + '</td>' +
                '<td>' + row.edit_name + '</td>' +
              '</tr>';

          });
          
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {
       
        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $per = $("#row-per").val();
        //$search = $("#row-search").val();
         console.log($per);
        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, per: $per, _token: $_token }, function(response) {
         
          // clear
          table.html("");
          // $('#my-table').dataTable().fnClearTable();
          var body = "";

          $.each(response.rows.data, function(index, row) {
            console.log(response);
               body += '<tr data-id="' + row.id + '">'+
                '<td>' + row.sales_id + '</td>' + 
                '<td>' + row.code + '</td>' + 
                '<td>' + row.name + '</td>' +
                '<td>' + row.activity + '</td>' +
                '<td>' + row.created_at+ '</td>' +
                '<td>' + row.edit_name + '</td>' +
                // '<td class="rightalign">'+
                //         '<a href="{{ url("policy/'+ row.id +'/view") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>'+
                //         '&nbsp;<a href="{{ url("policy/orphan-policies/edit") }}" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>'+
                //         '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>'+
                //         '&nbsp;<button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>'+ 
                //         '&nbsp;<input id="checkbox" type="checkbox" value="">'+
                // '</td>' +
            '</tr>';
          });

          table.html(body);
          $('#row-pages').html(response.pages);
          $(".tablesorter").trigger('update');

          // populate cache
          // $('#my-table').find('tbody tr').each(function() {
          //   $('#my-table').dataTable().fnAddData(this);
          // });

          loading.addClass("hide");

        }, 'json');

      }
    });
  
      //datatable
     //  myTable = $('#my-table').dataTable({
     //        "aoColumnDefs": [{ 
     //          'bSortable': false, 
     //          'aTargets': [0],
     //        }],
     //        //"order": [[ 4, "desc" ]],
     //        "dom": '<"top"f>rt<"bottom"lip><"clear">'
     //  });
     //  //show table
     //  $('.content-table').removeClass('hide');

     //  //search/filter
     // /* 
     //  $('#search-designation').change(function(){
     //      myTable.fnFilter($(this).val(), 0);
     //  });

     //  /*
     //  $('#search-rank').change(function(){
     //      myTable.fnFilter($(this).val(), 3);
     //  });*/

     //  $('#search').keyup(function(){
     //      myTable.fnFilter($(this).val());
     //  });
 
      /*$('.btn-sales-export-all').on('click', function() {
          console.log('btn-sales-export-all');
          $.post("{{ url('sales/logs/export-all') }}", { _token: $_token }, function(response) {
            console.log('response: ' + response);
            var blob=new Blob([response]);
            var link=document.createElement('a');
            link.href=window.URL.createObjectURL(blob);
            link.download="myfile.csv";
            link.click();

          });
         });*/

   
  </script>
@stop

@section('content')
    <!-- Page Content -->
    <div id="page-content-wrapper" class="response">
    <!-- Title -->
    <div class="container-fluid main-title-container container-space">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">TEAM MANAGEMENT LOG</h3>
        <h5 class="bread-crumb">USER MANAGEMENT<h5>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
              <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalarchive"><i class="fa fa-archive"></i> Archive</button>     
              <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModalexport"><i class="fa fa-plus"></i> Export</button>
        </div>
        </div>
 <!-- <div class="container-fluid default-container container-space">
  <div class="col-lg-12">
    <p><strong>FILTER OPTIONS</strong></p>
  </div>
<form class="form-horizontal" >     
<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
          <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>DESIGNATION</strong></p></label>
            <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                  <select class="form-control borderzero select" id="search-designation">
                    <option value="" class="hide">Select</option>
                    @foreach($designations as $row)
                    <option value="{{$row->id}}">{{ $row->name."(".$row->designation.")"}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
                <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH</strong></p></label>
                  <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <input type="text" class="form-control borderzero select" placeholder="Search for..." id="search">
                      </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
          <div class="form-group">
                <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>STATUS</strong></p></label>
                  <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <select class="select form-control borderzero" id="sel1" >
                          <option>ENABLED</option>
                          <option>DISABLED</option>
                        </select>
                      </div>
                  </div>
              </div>
              <button type="button" class="btn btn-default borderzero btn-tool pull-right" data-toggle="modal" data-target="#myModal" style="margin-right: 14px;">FILTER</button>
            </form>  
          </div> -->
        <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
          <div class="table-responsive block-content tbblock col-xs-12">
           <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
            </div>
            <div class="table-responsive"> 
            <table class="table table-striped table-pagination" id="my-table">
              <thead class="tbheader">
                <tr>
                  <th class="th-sort" nowrap data-sort="sales_id"><i></i> AGENT ID</th>
                  <th class="th-sort" nowrap data-sort="code"><i></i> AGENT CODE</th>
                  <th class="th-sort" nowrap data-sort="name"><i></i> AGENT NAME</th>
                  <th class="th-sort" nowrap data-sort="activity"><i></i> ACTIVITY</th>
                  <th class="th-sort" nowrap data-sort="created_at"><i></i> DATE</th>
                  <th class="th-sort" nowrap data-sort="edit_name"><i></i> EDITED BY</th>
                </tr>
              </thead>
              <tbody id="rows">
               @foreach ($rows as $row)
                  <tr>
                    <td>{{ $row->sales_id }}</td>
                    <td>{{ $row->code }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->activity }}</td>
                    <td>{{ $row->created_at }}</td>
                    <td>{{ $row->edit_name }}</td>
                  </tr>
               @endforeach
              </tbody>
            </table>
          </div>
        </div>
        </div>
        <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
        <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
        <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
      </div>
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>
    </div>
  </div>
</div>
<!-- /#page-content-wrapper -->

@stop