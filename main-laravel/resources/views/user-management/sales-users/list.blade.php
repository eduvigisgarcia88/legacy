@extends('layouts.master')

@section('scripts')

  <script>
    function statusChangeCallback(response) {
    //$(".custom-photo-pane").addClass("hide");
    //$(".default-photo-pane").removeClass("hide");
   // console.log('statusChangeCallback');
   // console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {

     
      // Logged into your app and Facebook.
       $("#standard_photo").addClass("hide");
       $(".standard_photo_pane").addClass("hide");
       $("#standard_photo_for_user").addClass("hide");
       // $(".default-photo-pane").addClass("hide");
       $(".facebook-photo-pane").removeClass("hide");
       //console.log(response.authResponse.accessToken);
       ///$("#fb-connect").attr('title','Logout your facebook account');
       FB.api('/me', function(response) {
      
        $(".system-photo-pane").html("<img class='img-circle image-responsive' id='row-photo_row' name='fb_photo' height='100' width='100' style='margin: 5px;' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        //$(".system-photo-pane").append("");
        $("#image_pane").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_pane_edit_photo").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_fb_link").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_for_system_user").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_edit_photo").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_settings").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#system_image_pane").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_pane_edit_photo").removeClass("hide");
        $("#image_pane").removeClass("hide");
        $("#row-photo").addClass("hide");
        $("#user_photo_pane").addClass("hide");
        $("#image_settings").removeClass("hide");
        $("#btn-cancel-photos").click();


        });
      $("#status").val(1);

    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
    
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");


    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
        alert("logout");
        $("#row-photo").removeClass("hide");
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");
        $("#image_pane").addClass("hide");
        $("#image_pane_edit_photo").addClass("hide");
        $("#default_image_pane").removeClass("hide");
        $("#system_image_pane").html('<img id="settings-photo" class="img-responsive img-circle" src="{{url("uploads")."/".Auth::user()->photo}}" height="100" width="100">');
        
        $("#image_pane_for_system_user").html("");
        $("#user_photo_pane").removeClass("hide");
        $(".facebook-photo-pane").addClass("hide");

        $(".system-photo-pane").html("<img src='{{ url('uploads').'/'.$settings->default_photo }}' class='preview img-circle' height='100' width='100' id = 'row-photo_row' style='margin:5px;'/>");
        $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        
        $("#status").val(0);
        $("#image_settings").addClass("hide");

       
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '395487573951234',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);

  });
  };
 
    // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


      $_token = '{{ csrf_token() }}';
   // refresh the list
    function refresh() {
      var loading = $(".loading-pane");
      var table = $("#rows");
       var rank = $(this).parent().parent().data("rank");

        if(rank == "Advisor"){
          $("#group_code_pane").removeClass("hide");
          $(".unit_code_dropdown").removeClass("hide");
          $(".unit_code_undropdown").addClass("hide");


        }else if(rank == "Supervisor"){
          $("#group_code_pane").removeClass("hide");
          $(".unit_code_dropdown").addClass("hide");
          $(".unit_code_undropdown").removeClass("hide");
        }
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        // clear
        table.html("");
        var body = "";
          
        $.each(response.rows.data, function(index, row) {
            console.log(row.designation_rank);
            body += '<tr data-id="' + row.id + '" ' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' +
              '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td>' + row.advisors_mobile + '</td>' +
              '<td>' + row.email + '</td>';

              body += '<td>' + row.director_code + '</td>' ; 
              body += '<td>' + row.super_code + '</td>' ;
             
              body += '<td>' + row.position_name + '</td>'+
                  '<td>' + (row.rnf_date ? moment(row.rnf_date).format('MM-DD-YYYY') : 'N/A' )+ '</td>';
              
              if (row.id == {{ Auth::user()->id }}) {
                body += '<td class="rightalign">' +
                          '<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +
                         '</td>' +
                        '</tr>';
              } else {
                body += '<td class="rightalign">' +
                    '<button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>'+
                    '&nbsp;<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +       
                    '<button type="button" class="btn btn-xs btn-table btn-edit borderzero hide" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable') + ' borderzero" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable') + '"><i class="fa fa-' + ((row.status == 1) ? 'adjust' : 'check-circle') + '"></i></button>' +
                    '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id +'">'
                '</td>' +
              '</tr>';
                }
        });

        table.html(body);
        $('#row-pages').html(response.pages);

        loading.addClass("hide");

      }, 'json');
    } 

    function search() {
      var loading = $(".loading-pane");
      var table = $("#rows");

      $("#row-page").val(1);

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
       
        // clear
        table.html("");
        var body = "";

        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" ' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' +
              '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td>' + row.advisors_mobile + '</td>' +
              '<td>' + row.email + '</td>';
             
              body += '<td>' + row.director_code + '</td>' ; 
              body += '<td>' + row.super_code + '</td>' ;
             
              body += '<td>' + row.position_name + '</td>'+
                  '<td>' + (row.rnf_date ? moment(row.rnf_date).format('MM-DD-YYYY') : 'N/A' )+ '</td>';
              if (row.id == {{ Auth::user()->id }}) {
                body += '<td class="rightalign">' +
                          '<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +
                         '</td>' +
                        '</tr>';
              } else {
                body += '<td class="rightalign">' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>'+
                    '&nbsp;<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +       
                    '<button type="button" class="btn btn-xs btn-table btn-edit borderzero hide" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable') + ' borderzero" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable') + '"><i class="fa fa-' + ((row.status == 1) ? 'adjust' : 'check-circle') + '"></i></button>' +
                    '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id +'">'
                '</td>' +
              '</tr>';
                }
        });

        table.html(body);
        $('#row-pages').html(response.pages);

        loading.addClass("hide");

      }, 'json');
    } 

    $(document).ready(function() {

    $('#row-search').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            search();
        }
    });
         $(".uploader-pane").addClass("hide");
         $("input.file").fileinput({
        maxFileCount: 1,
        maxFileSize: 1024,
        allowedFileTypes: ['image'],
        allowedFileExtensions: ['jpg', 'bmp', 'png', 'jpeg'],
      });

      $(".select-supervisor").addClass("hide");
      $("#row-supervisor_id").select2();
      var supervisor = $("#row-supervisor_id");
      $(".unit_code_dropdown").addClass("hide");
      $(".unit_code_undropdown").addClass("hide");
      
      $("#row-usertype_id").on("change", function() { 
        var id = $("#row-usertype_id").val();
        $_token = "{{ csrf_token() }}";

        $.post("{{ url('sales/get-groups') }}", { id: id, _token: $_token }, function(response) {

          if(response.get_rank.rank=="Supervisor") {
            $(".unit_code_undropdown").removeClass("hide");
            $(".unit_code_dropdown").addClass("hide");
          }
          if(response.get_rank.rank=="Advisor") {
            $(".unit_code_dropdown").removeClass("hide");
            $(".unit_code_undropdown").addClass("hide");
          }
          $("#row-group_code").removeAttr('disabled','disabled');
          $("#row-unit_id").removeAttr('disabled','disabled');
          $(".help-info").addClass("hide");
          $("#row-unit_id").html("");

          $("#row-group_code").removeClass("hide");
          $("#row-group_code_owner").addClass("hide");

          $("#modal-save_form").find("input[type='checkbox']").prop('checked', false);

          var group = $('#row-group_code');
          group.html('<option value="" class="hide">Select:</option>');
          $.each(response.get_group, function(index, row) {
            group.append('<option value="' + row.id + '">' + row.code + '</option>');
          });


        }, 'json');

        $.post("{{ url('sales/get-designation_rate') }}", { id: id, _token: $_token }, function(response) {

      
          $.each(response.get_rate, function(index, row) {
            $("#row-designation_rate").val(response.get_rate.rate);
          });


        }, 'json');



      });

      $("#row-banding_rank").on("change", function() { 
        var id = $("#row-banding_rank").val();
     
         $_token = "{{ csrf_token() }}";
         $.post("{{ url('sales/get-tiers') }}", { id: id, _token: $_token }, function(response) {
          $.each(response, function(index, row) {
            $("#row-bonding_rate").val(row.rate);         
          });
        }, 'json');

      });

       $("#row-designation_updated").on("change", function() { 
        var id = $("#row-designation_updated").val();
     
         $_token = "{{ csrf_token() }}";
         $.post("{{ url('sales/get-tiers') }}", { id: id, _token: $_token }, function(response) {
          $.each(response, function(index, row) {
            $("#row-bonding_rate").val(row.rate);         
          });
        }, 'json');

      });


      $(".btn-refresh").on("click", function() { 
        refresh();
      });

      $("#row-group_code").on("change", function() { 
        var id = $("#row-group_code").val();
        $_token = "{{ csrf_token() }}";
        var unit = $("#row-unit_id");
        unit.html("");
        if ($("#row-usertype_id").val()) {
          $.post("{{ url('sales/get-units') }}", { id: id, _token: $_token }, function(response) {
            unit.append(response);
          }, 'json');
        }
      });

       
      // $("#row-usertype_id").on("change", function() { 

      //   if ($("#row-group_code").val()) {
      //   var id = $("#row-group_code").val();
      //   $_token = "{{ csrf_token() }}";
      //   var unit = $("#row-unit_id");
      //   unit.html("");
      //     $.post("{{ url('sales/get-units') }}", { id: id, _token: $_token }, function(response) {
      //       unit.append(response);
      //     }, 'json');
      //   }
      // });

      // $("#row-usertype_id").on("change", function() {

      //   $("#row-supervisor_id").select2("val", "");

      //   if ($('#row-usertype_id').val() == 6) {
      //     $(".select-supervisor").addClass("hide");
      //     $("#row-supervisor_id").html("");
      //   } else {
      //     $(".select-supervisor").removeClass("hide");
      //     var id = $("#row-usertype_id").val();
      //     $_token = "{{ csrf_token() }}";
      //     supervisor.html("");

      //     $.post("{{ url('sales/get-supervisor') }}", { id: id, _token: $_token }, function(response) {

      //         supervisor.append(
      //           '<option value="" class="hide"></option>'
      //         );
      //         $.each(response.rows, function(result, row) {

      //           $.each(row.sales_designations, function(index, object) {
      //             supervisor.append(
      //               '<option value="' + object.id + '"> [' + object.users.code + '] ' + object.users.name + '</option>'
      //             );
      //           });

      //         });

      //     }, 'json');

      //   $("#row-supervisor_id").val("");
      //   }
      // });

  
      $(function () {
        $('.date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY',
            showClear: true,
        });
      });
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip(); 
          $(".fb-modal").attr("id","");
          //$("#date-designation_effective_date").datetimepicker("setDate", formatted);
      });
      // disable user
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('Account Disable', 'Are you sure you want to disable <strong>' + name + '</strong>?', "{{ url('sales/disable') }}", id);
      });

      // enable user
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(3)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        
        dialog('Account Enable', 'Are you sure you want to enable <strong>' + name + '</strong>?', "{{ url('sales/enable') }}", id);
      });

      // show modal for multiple disable
      $("#page-content-wrapper").on("click", ".btn-disable-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Disable");
        $(".modal-multiple-body").html("Are you sure you want to disable the selected user/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("2");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected user/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      //default photo
     
       
      $("#modal-multiple-button").click(function() {
        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });
      
        if ($("#row-multiple-hidden").val() == "0") {
          $.post("{{ url('users/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
            if (response.error) {
              status("Error", response.error, 'alert-danger');
            } else {
              status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);
              refresh();
            }
          }, 'json');
        } else if ($("#row-multiple-hidden").val() == "2") {
          $.post("{{ url('users/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
            if (response.error) {
              status("Error", response.error, 'alert-danger');
            } else {
              status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);
              refresh();
            }
          }, 'json');
        } else {
          status("Error", "Error! Refresh the page.", 'alert-danger');
        }

      });


      $("#page-content-wrapper").on("click", ".btn-add", function() {
        $('#password_div').addClass('hide');
        $('#deactivated_form').addClass('hide');
        $(".pwstrength_viewport_progress div:nth-child(2)").removeClass("progress").html("");
        $(".pwstrength_viewport_progress div:nth-child(3)").removeClass("progress").html("");
        $(".pwstrength_viewport_progress div:nth-child(4)").removeClass("progress").html("");

        $(".pwstrength_viewport_progress .progress-bar").attr('style', 'width: ' + 0 + '%;');
        $(".password-verdict").html("Weak");
        $('.nav-tabs a[href="#basic-info"]').tab('show');
        $("#row-group_code").removeClass("hide");
        $("#row-group_code_owner").addClass("hide");
        $("#modal-save_form").find("input[type='checkbox']").prop('checked', false);
        
         // reset all form fields
        $("#modal-form").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $("#row-id").val("");
        $(".system-photo-pane").html("<img src='{{ url('uploads')."/".$settings->default_photo }}' class='preview img-circle' height='100' width='100' id = 'row-photo_row' style='margin:5px;'/>");
        $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        $(".help-info").removeClass("hide");
        $('#active_group_code').addClass("hide");
        $('.unit_code_undropdown').addClass("hide");
        $('#active_unit_code').addClass("hide");
        $("#row-group_code").val("");
        // $("#row-group_code").append("<option>Please select designation</option>");
        $('#group_code_pane').removeClass("hide");
        $('.unit_code_dropdown').removeClass("hide");
        $(".new_user").removeClass("hide");
        var dateNow = new Date();
        $("#date-designation_effective_date").data("DateTimePicker").date(dateNow);
        $("#date-banding_effective_date").data("DateTimePicker").date(dateNow);
        $("#row-group_code").attr('disabled','disabled');
        $("#row-unit_id").attr('disabled','disabled');
          
        var unit = $("#row-unit_id");
        unit.html("");
       
        $("#row-supervisor_id").select2("val", "");
        
        $("#modal-form").find('input').removeAttr('readonly', 'readonly');

        // $(".modal-header").removeAttr("class").addClass("modal-header addnew-bg");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add Sales Users");
        $("#row-user_sales_id").val("");
        $("#modal-form").modal('show');
        
      });
      
    });

$('[required],[pattern]')
       .parent()
       .append('<span class="valid8"></span>');

    $(".close").click(function(){
var isValid = true;
    $('.required').each(function() {
        if ($.trim($(this).val()) == '') {
            isValid = false;
            $("#modal-form .alert").addClass("hide");
            $(this).attr("placeholder", "");
            $(this).css({
                "border-color": "",
                "background-color": "",

            });
        }
    });
});
$(".btn-close").click(function(){
var isValid = true;
    $('.required').each(function() {
        if ($.trim($(this).val()) == '') {
            isValid = false;
            $("#modal-form .alert").addClass("hide");
            $(this).attr("placeholder", "");
            $(this).css({
                "border-color": "",
                "background-color": "",

            });
        }
    });
});
$(".btn-submit").click(function(){
var isValid = true;
    $('.required').each(function() {
        if ($.trim($(this).val()) == '') {
            isValid = false;
            $("#modal-form .alert").removeClass("hide");
            $(this).attr("placeholder", "Fill the fields");
            $(this).css({
                "border-color": "#FF6666",
                "background-color": "#f2dede",

            });
        }
        else {
            $("#modal-form .alert").addClass("hide");
            $(this).css({
                "border-color": "",
                "background-color": "",
            });
        }
    });
});
  
    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {
      if ($(this).find('i').hasClass('fa-sort-up')) {
            $('.table-pagination th i').removeAttr('class');
            $order = $("#row-order").val('asc');
            $sort = $("#row-sort").val($(this).data('sort'));
            $(this).find('i').addClass('fa fa-sort-down');
          } else {
            $('.table-pagination th i').removeAttr('class');
            $order = $("#row-order").val('desc');
            $sort = $("#row-sort").val($(this).data('sort'));
            $(this).find('i').addClass('fa fa-sort-up');
          }

            $('.loading-pane').removeClass('hide');
            $page = $("#row-page").val();
            $order = $("#row-order").val();
            $sort = $("#row-sort").val();
            $search = $("#row-search").val();
            $status = $("#row-filter_status").val();
            $per = $("#row-per").val();

            var loading = $(".loading-pane");
            var table = $("#rows");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
         
          // clear
          table.html("");
          var body = "";

          $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '" ' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' +
              '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td>' + row.advisors_mobile + '</td>' +
              '<td>' + row.email + '</td>';
              
              body += '<td>' + row.director_code + '</td>' ; 
              body += '<td>' + row.super_code + '</td>' ;
             
              body += '<td>' + row.position_name + '</td>'+
                  '<td>' + (row.rnf_date ? moment(row.rnf_date).format('MM-DD-YYYY') : 'N/A' )+ '</td>';
              '<td>' + row.position_name + '</td>'+
              '<td>' + (row.rnf_date ? moment(row.rnf_date).format('MM-DD-YYYY') : 'N/A' )+ '</td>';
              if (row.id == {{ Auth::user()->id }}) {
                body += '<td class="rightalign">' +
                          '<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +
                         '</td>' +
                        '</tr>';
              } else {
                body += '<td class="rightalign">' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>'+
                    '<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +       
                    '<button type="button" class="btn btn-xs btn-table btn-edit borderzero hide" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>' +
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable') + ' borderzero" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable') + '"><i class="fa fa-' + ((row.status == 1) ? 'adjust' : 'check-circle') + '"></i></button>' +
                    '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id +'">'
                '</td>' +
              '</tr>';
              }
          });

          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
          $(".modal-fb-title").html('<i class="fa fa-file-image-o"></i> Facebook Connect');
          $(".modal-fb-body").html("Do you want to use your facebook profile picture?");
          $("#fb_photo_pane").find("#modal-fb-photo").modal("show");

        }, 'json');

    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
         
          // clear
          table.html("");

          var body = "";

          $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '" ' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' +
              '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td>' + row.advisors_mobile + '</td>' +
              '<td>' + row.email + '</td>';
                
              body += '<td>' + row.director_code + '</td>' ; 
              body += '<td>' + row.super_code + '</td>' ;
             
              body += '<td>' + row.position_name + '</td>'+
                  '<td>' + (row.rnf_date ? moment(row.rnf_date).format('MM-DD-YYYY') : 'N/A' )+ '</td>';
                if (row.id == {{ Auth::user()->id }}) {
                  body += '<td class="rightalign">' +
                            '<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +
                           '</td>' +
                          '</tr>';
                } else {
                  body += '<td class="rightalign">' +
                      '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>'+
                      '&nbsp;<a href="' + '{{ url('user-management/sales-users') }}/' + row.id + '/edit" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>' +       
                      '<button type="button" class="btn btn-xs btn-table btn-edit borderzero hide" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>' +
                      '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable') + ' borderzero" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable') + '"><i class="fa fa-' + ((row.status == 1) ? 'adjust' : 'check-circle') + '"></i></button>' +
                      '&nbsp;<input id="checkbox" type="checkbox" value="' + row.id +'">'
                  '</td>' +
                '</tr>';
                }
          });

        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

        }, 'json');

      }
    });
    
    $("#page-content-wrapper").on("click", ".btn-edit", function() {

      $(".pwstrength_viewport_progress div:nth-child(2)").removeClass("progress").html("");
      $(".pwstrength_viewport_progress div:nth-child(3)").removeClass("progress").html("");
      $(".pwstrength_viewport_progress div:nth-child(4)").removeClass("progress").html("");

      $(".pwstrength_viewport_progress .progress-bar").attr('style', 'width: ' + 0 + '%;');
      $(".password-verdict").html("Weak");
      $('#password_div').removeClass('hide');
      var rank = $(this).parent().parent().data("rank");
      if(rank == "Advisor") {
        $("#group_code_pane").removeClass("hide");
        $(".unit_code_dropdown").removeClass("hide");
        $(".unit_code_undropdown").addClass("hide");
      } else if(rank == "Supervisor") {
        $("#group_code_pane").removeClass("hide");
        $(".unit_code_dropdown").addClass("hide");
        $(".unit_code_undropdown").removeClass("hide");
      }

      $("#modal-form").find('form').trigger("reset");
      $("#row-unit_id").html("");
      $(".uploader-pane").addClass("hide");
      $(".system-photo-pane").removeClass("hide");

      $("#date-rnf_date").data("DateTimePicker").date(null);
      $("#date-contracted_date").data("DateTimePicker").date(null);
      $("#date-off_boarding_date").data("DateTimePicker").date(null);
      $("#row-photo_row").removeClass("hide");
      $(".old_user").removeClass("hide");
      $(".help-info").addClass("hide");

      $("#row-group_code").removeClass("hide");
      $("#row-group_code_owner").addClass("hide");
      $("#modal-save_form").find("input[type='checkbox']").prop('checked', false);
        
      $("#row-group_code").removeAttr('disabled','disabled');
      $("#row-unit_id").removeAttr('disabled','disabled');
      $('#row-group_code').val("");

      var loading = $(".loading-pane");

      var name = $(this).parent().parent().find('td:nth-child(2)').html();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");

      $("#row-user_sales_id").val("");
      
      $("#modal-form").find('.hide-view').removeClass('hide');
      $("#modal-form").find('.show-view').addClass('hide');
      $("#modal-form").find('input').removeAttr('readonly', 'readonly');
      $(".photo_view").removeClass('hide');
      $(".photo_upload").addClass('hide');

      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      
      $_token = "{{ csrf_token() }}";
      var url = "{{ url('/') }}";

      $('#row-user_sales_id').val(id);
      btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
      $.post("{{ url('sales/get-info') }}", { id: id, _token: $_token }, function(response) {
        
        try
        {
          if(response.rows.deactivated_date)
          {
            var deactivated_date = response.rows.deactivated_date.substr(0,10).split('-');
            console.log(deactivated_date);
            $('#row-deactivated').val(deactivated_date[1] + '/' + deactivated_date[2] + '/' + deactivated_date[0]);
          }
          else
          {
            $('#deactivated_form').addClass('hide');
          }
        }
        catch(e)
        {
          console.log(e);
        }

          $(".modal-title").html('<i class="fa fa-eye"></i> Editing <strong>' + response.rows.name + '</strong>');

          $.each(response.rows, function(index, value) {

            var field = $("#row-" + index);

            if (index == "photo") {
              $(".system-photo-pane").html("<img src='{{ url('uploads') . '/' }}" + value + "' class='preview img-circle' height='100' width='100' id = 'row-photo_row' style='margin:5px;'/>");
              $(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
            }
          
            $("#row-password").attr("placeholder","Leave blank for unchanged");
            $("#row-password_confirmation").attr("placeholder","Leave blank for unchanged");

            if (index == "sales_info") {
              if (value) {
                console.log(value);
                try
                {
                  var designation_dates = value.get_designation_rate.effective_date.substr(0,10).split('-');
                  $('#row-designation_effective_date').val(designation_dates[1] + '/' + designation_dates[2] + '/' + designation_dates[0]);
                }
                catch(e)
                {
//                     console.log(e);
                }
                $.each(value, function(sindex, svalue) {
                  var sfield = $("#row-" + sindex);
                  if(sindex == "designation_id") {
                    $("#row-usertype_id").val(svalue);
                  }
                  if (sfield.length > 0) {
                    sfield.val(svalue);
                  }
                  // if(sindex=="get_designation_rate") {
                  //   if (svalue) {
                  //    $.each(svalue, function(xindex, xvalue) {
                  //       var sfield = $("#row-" + xindex);
                  //       if (sfield.length > 0) {
                  //         if (sfield.parent().hasClass("date")) {
                  //           $("#date-" + "designation_" + xindex).data("DateTimePicker").date(moment.utc(svalue.effective_date));
                  //         }
                  //       }
                  //       $("#row-designation_rate").val(svalue.rate);
                  //     });
                  //   }
                  // }
                  if(sindex=="get_bandinginfo") {
                    if (svalue) {
                      $.each(svalue, function(xindex, xvalue) {
                        var sfield = $("#row-" + xindex);
                        if (sfield.length > 0) {
                          if (sfield.parent().hasClass("date")) {
                            $("#date-" +"banding_" + xindex).data("DateTimePicker").date(moment.utc(svalue.effective_date));
                          }
                        }
                      });
                    }
                  }
                });

              $('#row-designation').val(value.designations.designation);
              }
            }

            if(index == "get_sales_user_info") {
              if (value) {
                $.each(value, function(sindex, svalue) {
                  var sfield = $("#row-" + sindex);
                  if (sfield.parent().hasClass("date")) {
                    $("#date-" + sindex).data("DateTimePicker").date(moment.utc(svalue));
                  } else {
                    if (sfield.length > 0) {
                       sfield.val(svalue);
                    }
                  }
                });
              }
            }

            if(index == "get_provider_codes") {
              if (value.length > 0) {
                $.each(value, function(sindex, svalue) {
                  $.each(svalue, function(tindex, tvalue) {
                    $("#row-provider_code_" + svalue.classification_id).val(svalue.provider_code);
                    $("#row-provider_id_" + svalue.provider_id).val(svalue.provider_id);
                    $("#row-classification_id" + svalue.classification_id).val(svalue.classification_id);
                  });
                });
              } 
            }
            
            if(index == "get_advisor_info") {
              if (value) {
                $.each(value, function(sindex, svalue) {
                  if(sindex == "get_supervisor_info") {
                    if (svalue) {
                      $.each(svalue, function(xindex, xvalue) {
                        if(xindex =="id") {
                          $("#row-unit_id").val(xvalue);
                        }
                        if(xindex == "group_info") {
                          if (xvalue) {
                            $.each(xvalue, function(yindex, yvalue) {
                              if(yindex == "sales_supervisor") {
                                if (yvalue.length > 0) {
                                  $(".unit_code_undropdown").addClass("hide");
                                  $(".unit_code_dropdown").removeClass("hide");
                                  $("#row-unit_id").html("");
                                  $.each(yvalue, function(zindex, zvalue) {
                                    $("#row-unit_id").append(
                                      '<option value="' + zvalue.id + '">' + zvalue.unit_code + "</option>"
                                    );
                                  });
                                }
                              }
                            });
                          }
                        }
                      });
                    }
                    
                    $("#row-group_code").val(svalue.group_id);
                    $("#row-unit_id").val(svalue.id);
                  }
                });
              }
            }

            if(index == "get_supervisor_info") {
              if (value) {
                $.each(value, function(sindex, svalue) {
                  if(sindex == "unit_code") {
                    if (sindex.length > 0) {
                      $("#row-unit_codename").val(svalue);
                      $("#row-unit_code_final").val(value.unit_code);
                      $(".unit_code_dropdown").addClass("hide");
                      $(".unit_code_undropdown").removeClass("hide");
                    }
                  }
                  if(sindex == "group_info") {
                    if (svalue) {
                      $.each(svalue, function(nindex, nvalue) { 
                        if(nindex == "code"){ 
                          if (nindex.length > 0) {
                            $("#row-group_code").val(value.group_info.id);
                          }
                        }
                      });
                    }
                  }
                });
              }  
            }
        
            // field exists, therefore populate
            if(field.length > 0) {
              field.val(value);
            }

          });
             
          // $(".modal-title").html("<i class='fa fa-plus'></i> Add Sales Users");
          $('.nav-tabs a[href="#basic-info"]').tab('show');
          $("#modal-form").modal('show');
          loading.addClass("hide");
          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");

      }, 'json');

      //$("#image_pane_edit_photo").addClass("hide");

    });
    $('.btn-add-director-code').on('click', function() {
       window.open('{{ url("sales/team-management#new") }}','_blank');
       $(".btn-create-group").trigger("click");
    });
    $('#btn-cancel-photos').on('click', function() {
              $(".change").addClass('hide');
              $(".uploader-pane").addClass("hide");
              $(".system-photo-pane").removeClass("hide");
    });
    // $("#row-group_code").on("change", function() { 
    //       var id = $("#row-group_code").val();
    //       $_token = "{{ csrf_token() }}";
    //       var unit = $("#row-unit_id");
    //       unit.html("");
    //       console.log("yes");
    //       $.post("{{ url('sales/get-units') }}", { id: id, _token: $_token }, function(response) {
    //         unit.append(response);
    //      }, 'json');

    //    });
    $('#modal-form').on('click', '#edit-change-photo', function() {
            //console.log("checked");
          if ($(this).text() == 'CHANGE') {

              $('#row-photo_row').removeClass('hide');
              $(".change").removeClass('hide');
              $("#image_pane_edit_photo").addClass("hide");
              $(".uploader-pane").removeClass("hide");
              $(".system-photo-pane").addClass("hide");

            
           } //else {

          //     $(this).text("CHANGE");
          //     $('#row-photo_row').removeClass('hide');
          //     $(".uploader-pane").addClass("hide");
          //     $(".system-photo-pane").removeClass("hide");

          //     // Remove selected photo input.
          //     $('.change').find('.fileinput-remove').click();
          //     $(".change").addClass('hide');
          // }
      });

      $("#row-owner").on("change", function() {

        if($(this).is(":checked")) {
          $("#row-group_code").val("");
          $("#row-group_code").addClass("hide");
          $("#row-group_code_owner").val("");
          $("#row-group_code_owner").removeClass("hide");
        } else {
          $("#row-group_code").val("");
          $("#row-group_code").removeClass("hide");
          $("#row-group_code_owner").val("");
          $("#row-group_code_owner").addClass("hide");
        }
      });
  </script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">{{ $main_title }}</h3>
        <h5 class="bread-crumb">USER MANAGEMENT</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
      <button type="button" class="btn btn-sm btn-success borderzero btn-tool btn-refresh pull-right"><i class="fa fa-refresh"></i> Refresh</button>
      <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
      <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-disable-select"><i class="fa fa-adjust"></i> Disable</button>     
      <button type="button" class="btn btn-sm btn-warning borderzero btn-tool btn-add pull-right" id="user-add"><i class="fa fa-plus"></i> Add</button>
      <form style="float: right; padding-left: 5px;" role="form" method="post" action="{{ url('sales/export') }}">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-export"><i class="fa fa-print"></i> Export</button>
      </form>
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
          <p><strong>FILTER OPTIONS</strong></p>
      </div>
          <div class="form-horizontal" >     
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="" class="col-lg-4 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>STATUS</strong></p></label>
              <div class="col-lg-8 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                    <select class=" form-control borderzero" onchange="search()" id="row-filter_status" >
                      <option value="">All</option>
                      @foreach($groups as $row)
                      <option value="{{ $row->code }}">{{ $row->code }}</option>
                      @endforeach
                    </select>
                  </div>
              </div>
            </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                        <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                        </span>
                      </div>
                      </div>
                    </div>
                  </div>
                  <!--<button type="button" class="btn btn-default borderzero btn-tool pull-right" style="margin-right: 14px;">FILTER</button>-->
                </div>  
              </div>
        <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
         <div class="table-responsive block-content tbblock col-xs-12">
            <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-cog fa-spin fa-3x centered"></i></div>
            </div>
            <div class="table-responsive">
              <table class="table table-striped table-pagination" id="my-table">
                  <thead class="tbheader">
                    <tr>
                      <th>PHOTO</th>
                     <!-- <th><i></i> AGENT ID</th> -->
                      <th class="th-sort" nowrap data-sort="name"><i></i> PREFERRED NAME</th>
                      <th class="th-sort" nowrap data-sort="code"><i></i> AGENT CODE</th>
                      <th class="th-sort" nowrap data-sort="mobile"><i></i> MOBILE NO</th>
                      <th class="th-sort" nowrap data-sort="email"><i></i> EMAIL</th>
                      <th class="th-sort" nowrap data-sort="director_code"><i></i> DIRECTOR CODE</th>
                      <th class="th-sort" nowrap data-sort="super_code"><i></i> MANAGER CODE</th>
                      <th class="th-sort" nowrap data-sort="rank"><i></i> DESIGNATION</th>
                      <th class="th-sort" nowrap data-sort="rnf_date"><i></i> RNF DATE</th>
                     <!--  <th><i></i> DIRECT REPORTING TO</th> -->
                      <th class="rightalign">TOOLS</th>
                    </tr>
                  </thead>
                  <tbody id="rows">
                   @foreach($rows as $row)
                    <tr data-rank = "{{$row->designation_rank}}" data-id="{{ $row->id }}" {{ ($row->status == 1) ? ($row->id == Auth::user()->id) ? 'class=info' : '' : 'class=tr-disabled' }}>
                      <td><img src="{{ asset('/uploads/'.$row->photo) }}" height="30" width="30" class="img-circle"/></td>
                      <!-- <td>{{ $row->system_id }}</td> -->
                      <td>{{ $row->name }}</td>
                      <td>{{ $row->code }}</td>
                      <td>{{ $row->advisors_mobile}}</td>
                      <td>{{ $row->email }}</td>
                      <td>{{ $row->director_code }}</td>
                      <td>{{ $row->super_code }}</td>
                      <td>{{ $row->position_name }}</td>
                      <td>{{ ( $row->rnf_date ? date_format(new DateTime($row->rnf_date), 'm-d-Y') : 'N/A' ) }}</td>
                      <td class="rightalign">
                        <button type="button" class="btn btn-xs btn-table btn-edit borderzero" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>
                        <a href="{{ url('user-management/sales-users/'.$row->id.'/edit') }}" type="button" class="btn-table borderzero btn btn-xs btn-default" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
                        @if($row->id != Auth::user()->id)
                          <button type="button" class="btn btn-xs btn-table btn-edit borderzero hide" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></button>
                          <!--<button type="button" class="btn btn-xs btn-table borderzero" data-toggle="tooltip" title="Movement History"><i class="fa fa-book"></i></button>
                          <button type="button" class="btn btn-xs btn-table borderzero" data-toggle="tooltip" title="Designate"><i class="fa fa-map-marker"></i></button>-->
                          <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}  borderzero" data-toggle="tooltip" title="{{ ($row->status == 1) ? 'Disable' : 'Enable' }}"><i class="fa fa-{{ ($row->status == 1) ? 'adjust' : 'check-circle' }}"></i></button>
                          <input id="checkbox" type="checkbox" value="{{ $row->id }}">
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>      
            </div>
            <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
            <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
            <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
              <option value="50">100</option>
            </select>
          </div>
            <div class="text-center">
              <input type="hidden" id="row-page" value="1">
              <input type="hidden" id="row-sort" value="">
              <input type="hidden" id="row-order" value="">
            </div>
          </div>
        </div>
        <!-- /#page-content-wrapper -->


@include('user-management.sales-users.form')

@stop



