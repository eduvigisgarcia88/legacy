<!-- modal add provider -->
<div class="modal fade" id="modal-provider" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'provider/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_provider')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
              </div>
              <div class="modal-viewing modal-body">
                <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">PROVIDER NAME</label>
                <div class="col-sm-9 col-xs-7 error-name">
                <input type="text" name="name" class="form-control borderzero" id="row-name" placeholder="">
                <sup class=" sup-errors"></sup>
              </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">PROVIDER CODE</label>
                <div class="col-sm-9 col-xs-7 error-code">
                <input type="text" name="code" class="form-control borderzero" id="row-code" placeholder="">
                <sup class="sup-errors"></sup>
              </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">DESCRIPTION</label>
                <div class="col-sm-9 col-xs-7 error-description">
                <input type="text" name="description" class="form-control borderzero" id="row-description"  placeholder="">
                <sup class="sup-errors"></sup>
              </div>
              </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-provider-property" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg">
          <div class="modal-content borderzero">
            <div id="load-provider-property" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
            </div>
            {!! Form::open(array('url' => 'provider/property/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_provider_classification')) !!}
            <div class="modal-header modal-warning">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
            <div id="provider-property-notice"></div>
            <div class="form-group edit-property">
              <label for="email" class="col-sm-3 col-xs-5">SELECT PROPERTY</label>
              <div class="col-sm-9 col-xs-7 error-property_id">
                <select name="property_id" class="form-control borderzero" id="provider-property_id" placeholder="">
                  <option value="" class="hide">Select</option>
                  <option value="1">Product classification</option>
                  <option value="2">Product category</option>
                </select>
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-sm-3 col-xs-5">NAME</label>
              <div class="col-sm-9 col-xs-7 error-name">
                <input type="text" name="name" class="form-control borderzero" id="provider-name"  placeholder="">
                <sup class="sup-errors"></sup>
              </div>
            </div>
            <div class="row modal-footer borderzero">
              <input type="hidden" name="id" id="provider-id">
              <input type="hidden" name="provider_id" id="provider-provider-id">
              <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>

<!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>