@extends('layouts.master')

@section('scripts')
<script>
  $expand_id = 0;
  $loading_expand = false;
  $type_id = 0;
  $loading_type = false;
  $_token = "{{ csrf_token() }}";

  function refresh() {
    var loading = $(".loading-pane");
    var table = $("#rows");
    $expand_id = 0;
    $loading_expand = false;
    $type_id = 0;
    $loading_type = false;
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();

    loading.removeClass("hide");

    $.post("{{ url('provider/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
      // clear
      table.html("");
      
      var body = "";

      $.each(response.rows.data, function(index, row) {
        if (row.status != 2) {
          table.append(
            '<tr data-id="' + row.id + '"' + ((row.status == 1) ? '' : 'class="tr-disabled"') + '>' +
              '<td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td>' + row.description + '</td>' + 
              '<td class="rightalign">' +
                '<button type="button" class="btn btn-xs btn-table btn-add-product"><i class="fa fa-plus"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-remove-provider"><i class="fa fa-trash"></i></button>' +
              '</td>' +
            '</tr>'
          );
        }
      });

      $(".th-sort").find('i').removeAttr('class');
      $('#row-pages').html(response.pages);

      loading.addClass("hide");
    }, 'json');
  }

  function search() {
    
    $('.loading-pane').removeClass('hide');
    $("#row-page").val(1);
    $page = $("#row-page").val();
    $order = $("#row-order").val();
    $sort = $("#row-sort").val();
    $search = $("#row-search").val();
    $per = $("#row-per").val();

    var loading = $(".loading-pane");
    var table = $("#rows");

    $.post("{{ url('provider/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
      // clear
      table.html("");
      
      var body = "";

      $.each(response.rows.data, function(index, row) {
        if (row.status != 2) {
          table.append(
            '<tr data-id="' + row.id + '"' + ((row.status == 1) ? '' : 'class="tr-disabled"') + '>' +
              '<td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' + row.name + '</td>' +
              '<td>' + row.code + '</td>' +
              '<td>' + row.description + '</td>' + 
              '<td class="rightalign">' +
                '<button type="button" class="btn btn-xs btn-table btn-add-product"><i class="fa fa-plus"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                '&nbsp;<button type="button" class="btn btn-xs btn-table btn-remove-provider"><i class="fa fa-trash"></i></button>' +
              '</td>' +
            '</tr>'
          );
        }
      });

      $(".th-sort").find('i').removeAttr('class');
      $('#row-pages').html(response.pages);

      loading.addClass("hide");
    }, 'json');
  
  }


    $(document).ready(function() {
    //add provider
      $(".btn-add").click(function() {

        // reset all form fields
        $("#modal-provider").find('form').trigger("reset");
        $("#row-id").val("");

        $("#modal-provider").find('.hide-view').removeClass('hide');
        $("#modal-provider").find('.show-view').addClass('hide');
        $("#modal-provider").find('.preview').addClass('hide');
        $("#modal-provider").find('input').removeAttr('readonly', 'readonly');


        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add New");
        $("#modal-provider").modal('show');

      });

      $("#page-content-wrapper").on("click", ".btn-add-product", function() {

        $("#provider-name").attr("disabled","disabled");
        var name = $(this).parent().parent().find('td:nth-child(2)').html();
        $(".edit-property").removeClass("hide");
        // reset all form fields
        $("#modal-provider-property").find('form').trigger("reset");
        $("#provider-provider-id").val($(this).parent().parent().data('id')); //note this
        $("#provider-id").val($(this).data('id')); //note this
        $("#property-id").val($(this).data('property_id')); //note this

        $("#modal-provider-property").find('.hide-view').removeClass('hide');
        $("#modal-provider-property").find('.show-view').addClass('hide');
        $("#modal-provider-property").find('.preview').addClass('hide');
        $("#modal-provider-property").find('input').removeAttr('readonly', 'readonly');
        $("#modal-provider-property").find('select').removeAttr('readonly', 'readonly');

        $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add New <strong>(" + name +")</strong>");
        $("#modal-provider-property").modal('show');
      });

      $('#provider-property_id').change(function() {
        if ($(this).is(":checked")) {
          $("#provider-property_id").attr("value",0);
          $("#provider-name").attr("disabled","disabled"); 
        } else {
          $("#provider-property_id").attr("value",1);
          $("#provider-name").removeAttr("disabled","disabled");   
        }
      });

      $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        var name = $(this).parent().parent().find('td:nth-child(2)').html();
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

    // reset all form fields
    $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
    $("#modal-provider").find('form').trigger("reset");
    $("#row-id").val("");


        $.post("{{ url('provider/view') }}", { id: id, _token: $_token }, function(response) {
          if(!response.error) {
          // set form title

          $(".modal-title").html("<i class='fa fa-eye'></i> Viewing <strong>(" + name +")</strong>");

            // output form data
            $.each(response, function(index, value) {
                var field = $("#row-" + index);

                // field exists, therefore populate
                if(field.length > 0) {
                    field.val(value);
                }
            });

            $("#modal-provider").find('.hide-view').addClass('hide');
            $("#modal-provider").find('.preview').removeClass('hide');
            $("#modal-provider").find('.change').addClass('hide');
            $("#modal-provider").find('.show-view').removeClass('hide');
            $("#modal-provider").find('input').attr('readonly', 'readonly');
            // show form
            $("#modal-provider").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
        }, 'json');
      });

      //edit provider
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        var name = $(this).parent().parent().find('td:nth-child(2)').html();

        // reset all form fields
        $("#modal-provider").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $("#provider-name").attr("readonly", "readonly");
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

        $.post("{{ url('provider/view') }}", { id: id, _token: $_token }, function(response) {
          if(!response.error) {

          // set form title
          $(".modal-title").html("<i class='fa fa-pencil'></i> Editing <strong>(" + name +")</strong>");

            // output form data
            $.each(response, function(index, value) {
                var field = $("#row-" + index);

                if(index == "user_type") {
                  $("#row-usertype_id-static").val(value.type_name);
                }
                // field exists, therefore populate
                if(field.length > 0) {
                    field.val(value);
                }
            });

            $("#modal-form").find('.hide-view').removeClass('hide');
            $("#modal-form").find('.show-view').addClass('hide');
            $("#modal-form").find('.preview').removeClass('hide');
            $("#modal-form").find('.change').addClass('hide');
            $("#modal-provider").find('input').removeAttr('readonly', 'readonly');

            // show form
            $("#modal-provider").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
        }, 'json');
      });

      //edit property
      $("#page-content-wrapper").on("click", ".btn-property-edit", function() {
        // reset all form fields
        $("#modal-provider-property").find('form').trigger("reset");
        var id = $(this).parent().parent().data('id');
        var property_id = $(this).parent().parent().data('property_id');
        var btn = $(this);
      
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#provider-id").val("");
        $(".edit-property").addClass("hide");
        $("#provider-name").removeClass('disabled', 'disabled');
        $_token = "{{ csrf_token() }}";

        btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

        $.post("{{ url('provider/property/view') }}", { property_id: $property_id, id, _token: $_token }, function(response) {
          if(!response.error) {
            // set form title
            $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Property</strong>');

            // output form data
            $.each(response, function(index, value) {
                var field = $("#provider-" + index);

                // field exists, therefore populate
                if(field.length > 0) {
                    field.val(value);
                }
            });


            $("#modal-provider-property").find('.hide-view').removeClass('hide');

            $("#modal-provider-property").find('input').removeAttr('readonly', 'readonly');
             $("#provider-property_id").removeAttr('readonly', 'readonly');   
             $("#provider-value").removeAttr('readonly', 'readonly'); 
             $("#provider-value").removeAttr("disabled", "disabled"); 

            // show form
            $("#modal-provider-property").modal('show');
          } else {
            status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
        }, 'json');
        
      });

      //view property
      $("#page-content-wrapper").on("click", ".btn-property-view", function() {

        var id = $(this).parent().parent().data('id');
        var property_id = $(this).parent().parent().data('property_id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();
        var btn = $(this);
        $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
        $(".edit-property").addClass("hide");

        btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

          // reset all form fields
         
          $("#modal-provider-propertyr").find('form').trigger("reset");
          $("#row-id").val("");

          $.post("{{ url('provider/property/view') }}", { property_id: $property_id, id: id, _token: $_token }, function(response) {
            if(!response.error) {
              // set form title

              var property = ( property_id == 1 ? 'Classification' : 'Category' );
              $(".modal-title").html('<i class="fa fa-eye"></i> Viewing <strong>' + name + '</strong> (' + property + ')');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#provider-" + index);

                  // field exists, therefore populate
                  if(field.length > 0) {
                      field.val(value);
                  }
              });

              $("#modal-provider-property").find('.hide-view').addClass('hide');
              $("#modal-provider-property").find('.preview').removeClass('hide');
              $("#modal-provider-property").find('.change').addClass('hide');
              $("#modal-provider-property").find('.show-view').removeClass('hide');
              $("#modal-provider-property").find('input').attr('readonly', 'readonly');
              // show form
              $("#modal-provider-property").modal('show');
            } else {
              status(false, response.error, 'alert-danger');
            }

            btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
          }, 'json');
      });

      // disable user
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(2)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('<i class="fa fa-adjust"></i> Disable', 'Are you sure you want to disable provider <strong>' + name + '</strong>?', "{{ url('provider/disable') }}", id);
      });

      // enable user
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(2)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('<i class="fa fa-check-circle"></i> Enable', 'Are you sure you want to enable provider <strong>' + name + '</strong>?', "{{ url('provider/enable') }}", id);
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-provider", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(2)').html();
        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        dialog('<i class="fa fa-trash"></i> Delete', 'Are you sure you want to delete provider <strong>' + name + '</strong>?', "{{ url('provider/remove') }}", id);
      });

      $("#page-content-wrapper").on("click", ".btn-delete-classification", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();
        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        dialog('<i class="fa fa-trash"></i> Delete', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('provider/classification/delete') }}", id);
      });

       $("#page-content-wrapper").on("click", ".btn-delete-category", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();
        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        dialog('<i class="fa fa-trash"></i> Delete', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('provider/category/delete') }}", id);
      });

      // show modal for multiple disable
      $("#page-content-wrapper").on("click", ".btn-disable-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Disable");
        $(".modal-multiple-body").html("Are you sure you want to disable the selected provider/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });

      $("#modal-multiple-button").click(function() {
        var ids = [];
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
            ids.push($(this).val());
        });

        if (ids.length == 0) {
          status("Error", "Select providers first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('provider/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              console.log(response);
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('provider/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }
      });
    });


   $(document).ready(function() {

    $(".multi_select").select2();
    $("#row-value").attr("disabled","disabled");

     $('#row-property_id').change(function() {
      if($(this).is(":checked")) {
           $("#row-value").attr("disabled","disabled");
      } else {
        $("#row-value").removeAttr("disabled","disabled");  
      }
    });

    $("#row-sales_id").on("change", function() {
        batch = $("#row-batch_id");
        var id = $("#row-sales_id").val();
        $_token = "{{ csrf_token() }}";
        batch.html("");

        $.post("{{ url('provider/get-batch') }}", { id: id, _token: $_token }, function(response) {
          batch.append(
            '<option value="" class="hide"></option>'
          );
          $.each(response.batches, function(index, row) {
            batch.append(
              '<option value="' + row.id + '">' + '[' + row.name + ']' + '-' + moment(row.start).format('MM/DD/YYYY') + '-' + moment(row.end).format('MM/DD/YYYY') +'</option>'
            );
          });
        }, 'json');

        batch.select2("val", "");
    });

    // disable user
    $("#modal-multiple-button").click(function() {

      var ids = [];
      $_token = "{{ csrf_token() }}";

      $("input:checked").each(function(){
          ids.push($(this).val());
      });
      console.log('multiple');

      
      if (ids.length == 0) {
        status("Error", "Select pre-payroll entries first.", 'alert-danger');
      } else {

        if ($("#row-multiple-hidden").val() == "0") {
          $.post("{{ url('payroll/admin-pre-payroll-entries/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
            console.log(response);
            refresh();
          }, 'json');
        } else if ($("#row-multiple-hidden").val() == "1") {
          $.post("{{ url('payroll/admin-pre-payroll-entries/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
            refresh();
          }, 'json');
        } else {
          status("Error", "Error! Refresh the page.", 'alert-danger');
        }
      }

    });

    $("#page-content-wrapper").on("click", ".btn-expand", function() {
      // $(this).find('i').toggleClass('fa-plus fa-minus');
       $(".btn-expand").html('<i class="fa fa-minus"></i>');
       $(".btn-expand").html('<i class="fa fa-plus"></i>');


      $type_id = 0;

      $property_id = $(this).data('property_id');  //note this

      if ($loading_expand){
        return;
      }
      $tr = $(this).parent().parent();
      $id = $tr.data('id');
      $_token = '{{ csrf_token() }}';

      $selected = '.row-expand-' + $expand_id;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

      if ($id == $expand_id) {
        $expand_id = 0;
        return
      }

      $expand_id = $id;
      $button = $(this);
      $button.html('<i class="fa fa-spinner fa-spin"></i>');
      $loading_expand = true;


      $.post("{{ url('provider/get-provider-property') }}", { id: $id, property_id: $property_id, _token: $_token }, function(response) {

        if (!response.error) {
          $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-expand-' + $expand_id + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-expand-' + $expand_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        } else {
          status(false, response.error, 'alert-danger');
        }

      });

    });

    $("#page-content-wrapper").on("click", ".btn-type-expand", function() {
    $(".btn-type-expand").html('<i class="fa fa-minus"></i>');
    $(".btn-type-expand").html('<i class="fa fa-plus"></i>');

      if ($loading_type){
        return;
      }
      $tr = $(this).parent().parent();
      $id = $tr.data('id');
      $property = $(this).data('property_id');
      $_token = '{{ csrf_token() }}';
      $type = $(this).data('id');
      $property_id =  $(this).data('property_id');
      $selected = '.row-type-' + $type_id;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

      if ($property == $type_id) {
        $type_id = 0;
        return
      }

      $type_id = $property;
      $button = $(this); 
      $button.html('<i class="fa fa-spinner fa-spin"></i>');
      $loading_type = true;

      $.post("{{ url('provider/get-provider-product') }}", { type: $id, id: $type, property_id: $property_id, _token: $_token }, function(response) {

        if (!response.error) {
          $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-type-' + $type_id + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-type-' + $type_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_type = false;
        } else {
          status(false, response.error, 'alert-danger');
        }

      });
    });

  });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

      $.post("{{ url('provider/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";

        $.each(response.rows.data, function(index, row) {
          if (row.status != 2) {
            table.append(
              '<tr data-id="' + row.id + '"' + ((row.status == 1) ? '' : 'class="tr-disabled"') + '>' +
                '<td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' + row.name + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.description + '</td>' + 
                '<td class="rightalign">' +
                  '<button type="button" class="btn btn-xs btn-table btn-add-product"><i class="fa fa-plus"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-remove-provider"><i class="fa fa-trash"></i></button>' +
                '</td>' +
              '</tr>'
            );
          }
        });

        $('#row-pages').html(response.pages);

        loading.addClass("hide");
      }, 'json');
      
    });

  $("#page-content-wrapper").on('click', '.pagination a', function (event) {
    event.preventDefault();
    if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      loading.removeClass("hide");

      $.post("{{ url('provider/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";

        $.each(response.rows.data, function(index, row) {
          if (row.status != 2) {
            table.append(
              '<tr data-id="' + row.id + '"' + ((row.status == 1) ? '' : 'class="tr-disabled"') + '>' +
                '<td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' + row.name + '</td>' +
                '<td>' + row.code + '</td>' +
                '<td>' + row.description + '</td>' + 
                '<td class="rightalign">' +
                  '<button type="button" class="btn btn-xs btn-table btn-add-product"><i class="fa fa-plus"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-view"><i class="fa fa-eye"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-' + ((row.status == 1) ? 'disable' : 'enable' ) + '" data-toggle="tooltip" title="' + ((row.status == 1) ? 'Disable' : 'Enable' ) + '"><i class="fa fa-' + ((row.status == 1) ?'adjust' : 'check-circle' ) + '"></i></button>' +
                  '&nbsp;<button type="button" class="btn btn-xs btn-table btn-remove-provider"><i class="fa fa-trash"></i></button>' +
                '</td>' +
              '</tr>'
            );
          }
        });

        $('#row-pages').html(response.pages);

        loading.addClass("hide");
      }, 'json');
    }

  });


    $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
    });

</script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">PROVIDER LIST</h3>
        <h5 class="bread-crumb">PROVIDER & PRODUCTS MANAGEMENT</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
      <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button> 
      <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-disable-select"><i class="fa fa-adjust"></i> Disable</button>    
      <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-add"><i class="fa fa-plus"></i> Add</button> 
    </div>
    </div>
    <div class="container-fluid default-container container-space">
    <div class="col-lg-12">
      <p><strong>FILTER OPTIONS</strong></p>
    </div>   
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hide">
        <div class="form-group">
              <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH OPTION</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                            <select class="form-control borderzero" id="sel1" >
                              <option>All Providers</option>
                              <option>A</option>
                              <option>B</option>
                              <option>C</option>
                              <option>D</option>
                            </select>
                          </div>
                      </div>
                  </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                  <div class="input-group">
                  <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                  </span>
                </div>
                </div>
              </div>
            </div>
              </div>
    <div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
        <div class="table-responsive block-content tbblock col-xs-12"> 
          <div class="loading-pane hide">
            <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
          </div>
          <table class="table table-striped table-pagination" id="my-table">
              <thead class="tbheader">
                <tr>
                  <th class="th-sort" data-sort="name"><i></i> PROVIDER NAME</th>
                  <th class="th-sort" data-sort="code"><i></i> PROVIDER CODE</th>
                  <th class="th-sort" data-sort="description"><i></i> PROVIDER DESCRIPTION</th>
                  <th class="rightalign">TOOLS</th>
                </tr>
              </thead>
              <tbody id="rows">
                @foreach($rows as $row)
                  @if ($row->status != 2)
                  <tr data-id="{{ $row->id }}" {{ ($row->status == 1) ? '' : 'class=tr-disabled' }}>
                    <td>
                      <a class="btn btn-xs btn-table btn-expand" data-upload="{{ $row->upload_id }}" data-policy="{{ $row->policy_no }}"><i class="fa fa-plus"></i></a> {{ $row->name }}
                    </td>
                    <td>{{ $row->code }}</td>
                    <td>{{ $row->description }}</td>
                    <td class="rightalign">
                      <button type="button" class="btn btn-xs btn-add-product btn-table"><i class="fa fa-plus"></i></button>
                      <button type="button" class="btn btn-xs btn-view btn-table"><i class="fa fa-eye"></i></button>
                      <button type="button" class="btn btn-xs btn-table btn-edit"><i class="fa fa-pencil"></i></button>
                      <button type="button" class="btn btn-xs btn-table btn-{{ ($row->status == 1) ? 'disable' : 'enable' }}"><i class="fa fa-{{ ($row->status == 1) ? 'adjust' : 'check-circle' }}"></i></button>
                      <button type="button" class="btn btn-xs btn-table btn-remove-provider"><i class="fa fa-trash"></i></button>
                      <!-- <input id="checkbox" type="checkbox" value="{{ $row->id }}"> -->
                    </td>
                  </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>           
        </div>
      <div id="row-pages" class="col-lg-6 col-md-6 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
      <div class="col-lg-6 col-md-6 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
      <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="50">100</option>
      </select>
      </div>
      <div class="text-center">
        <input type="hidden" id="row-page" value="1">
        <input type="hidden" id="row-sort" value="">
        <input type="hidden" id="row-order" value="">
      </div> 
    </div>

    <!-- /#page-content-wrapper -->
@include('provider-product-management.provider.form')
@stop