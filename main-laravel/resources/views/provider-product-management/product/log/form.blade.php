<!-- Modal -->
<div id="myModalview" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-eye"></i> Profile</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-group">
    <label for="email">PHOTO</label><br>
    <img src="images/avatar.png" style="color: #79828f;" class="img-circle" alt="Smiley face" height="100" width="100"> 
  </div>
  <div class="form-group">
    <label for="email">AGENT ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="steve12345" disabled>
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">USER NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="Sales Agent" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12345678" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">DESIGNATION</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="steve@email.com" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">EMAIL</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12/06/1985" disabled>
  </div>
  <div class="form-group">
    <label for="pwd">CONTACT NO.</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="+65 123 4567" disabled>
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Edit</button>
</div>
</div>
</div>
</div>
<!-- modal add product -->
<div class="modal fade" id="modal-export" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div id="load-form" class="loading-pane hide">
                <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
              </div>
              {!! Form::open(array('url' => 'product/logs/export', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_export')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Export By</h4>
              </div>
              <div class="modal-body">
               <div id="export-notice"></div>
                <div class="form-group">
                <label for="row-designation">DESIGNATION</label>
                  <select id="select" class="form-control borderzero" id="row-designation_id" name="designation_id">
                    <option value="1">CEO</option>
                    <option value="2">IT Users</option>
                    <option value="3">Admin Accounts</option>
                    <option value="4">Admin Assistant</option>
                    <option value="5">Accounting</option>
                    <option value="6">Accounting Assistant</option>
                  </select>
                  <input type="submit" class="btn btn-default borderzero" name="product-log" value="Export">
                </div>      
                <hr>
                <div class="form-group">
                  <label for="pwd">DATE</label>
                </div>
                <div class="form-group">
                <label for="">FROM:</label>
                  <div class="input-group">
                    <input class="form-control borderzero" type="date" id="row-from" name="from" placeholder="..">
                    <span class="input-group-addon borderzero"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
                <div class="form-group">
                <label for="">TO:</label>
                  <div class="input-group">
                    <input class="form-control borderzero" type="date" id="row-to" name="to" placeholder="..">
                    <span class="input-group-addon borderzero"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
                <div class="row modal-footer borderzero">
                  <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-default hide-view borderzero" name="product-log-all" value="Export All">
                </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>

<!-- Modal -->
<div id="myModalarchive" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-plus"></i> ARCHIVE</h4>
</div>
<div class="modal-body">
  <p>Are you want to Archive System User Log?</p>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldelete" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-minus"></i> Remove</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to remove the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaldeactivate" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-adjust"></i> Disable</h4>
</div>
<div class="modal-body">
<p>Are you sure you want to disable the User?</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Yes</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
</div>
</div>
</div>
</div>
<div id="myModaledit" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"><i class="fa fa-pencil"></i> Edit</h4>
</div>
<div class="modal-body">
<form role="form">
  <div class="form-inline">
    <label for="email">PHOTO</label><br>
    <img src="images/avatar.png" style="margin-bottom: 5px; color: #79828f;" class="img-circle" alt="Smiley face" height="100" width="100">
    <div class="form-group">
    <input type="email" class="form-control borderzero" id="email" placeholder="file:///C:/Users/Desktop/Users/picture.jpg"><br>
    <button type="button" class="btn btn-default borderzero" data-dismiss="">UPLOAD</button>
  </div>
  </div>  
  <div class="form-group">
    <label for="email">AGENT ID</label>
    <input type="email" class="form-control borderzero" id="email" placeholder="steve12345">
  </div>
  <div class="form-group">
    <label for="email">AGENT CODE</label>
    <input type="email" class="form-control borderzero" id="email"  placeholder="Steve Jobs">
  </div>
  <div class="form-group">
    <label for="pwd">USER NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="Sales Agent">
  </div>
  <div class="form-group">
    <label for="pwd">AGENT NAME</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12345678">
  </div>
  <div class="form-group">
    <label for="pwd">DESTINATION</label>
      <select id="select" class="form-control borderzero" id="sel1">
      <option>CEO</option>
      <option>IT Users</option>
      <option>Admin Accounts</option>
      <option>Accounting</option>
      </select>
  </div>
  <div class="form-group">
    <label for="pwd">EMAIL</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="12/06/1985">
  </div>
  <div class="form-group">
    <label for="pwd">CONTACT NO.</label>
    <input type="password" class="form-control borderzero" id="pwd" placeholder="+65 123 4567">
  </div>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-default borderzero" data-dismiss="modal">Save Changes</button>
</div>
</div>
</div>
</div>
