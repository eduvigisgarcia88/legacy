<!-- modal add product -->
<div class="modal fade" id="modal-product" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'product/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_product')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
              </div>
              <div class="modal-viewing modal-body">
                <div id="product-notice"></div>
                <div class="form-group">
                  <label for="email" class="col-sm-3 col-xs-5">PROVIDER</label>
                  <div class="col-sm-9 col-xs-7">
                  <input type="text" id="row-static-provider" class="form-control borderzero" placeholder="" disabled="disabled">
                  </div>
                </div>
                <div class="form-group error-product_name">
                  <label for="email" class="col-sm-3 col-xs-5">PRODUCT NAME</label>
                  <div class="col-sm-9 col-xs-7">
                    <input type="text" name="product_name" class="form-control borderzero" id="row-product_name" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group hide">
                  <label for="email" class="col-sm-3 col-xs-5">SEQUENCE NO.</label>
                  <div class="col-sm-9 col-xs-7 error-sequence_no">
                    <input type="text" name="sequence_no" class="form-control borderzero" id="row-sequence_no" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="classification_id" class="col-sm-3 col-xs-5">PRODUCT CLASSIFICATION</label>
                  <div class="col-sm-9 col-xs-7 error-classification_id">
                    <select class="form-control borderzero" name="classification_id" id="row-classification_id">

                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-3 col-xs-5">PRODUCT CODE</label>
                  <div class="col-sm-9 col-xs-7 error-code">
                    <input type="text" name="code" class="form-control borderzero" id="row-code" placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-3 col-xs-5">PRODUCT CATEGORY</label>
                  <div class="col-sm-9 col-xs-7 error-category_id">
                    <select name="category_id" class="form-control borderzero" id="row-category_id" placeholder="">

                    </select>
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
                <div class="hide form-group">
                  <label for="email" class="col-sm-3 col-xs-5">PRODUCT FEATURE/DESCRIPTION</label>
                  <div class="col-sm-9 col-xs-7 error-description">
                    <input type="text" name="description" class="form-control borderzero" id="row-description"  placeholder="">
                    <sup class="sup-errors"></sup>
                  </div>
                </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">EFFECTIVE START</label>
                <div class="col-sm-9 col-xs-7 error-start">
                  <div class="input-group effect-start date">
                    <input type="text" name="start" class="form-control borderzero" id="row-start" maxlength="28" placeholder="">
                    <span class="input-group-addon">
                      <span><i class="fa fa-calendar"></i></span>
                    </span>
                  </div>
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">EFFECTIVE END</label>
                <div class="col-sm-9 col-xs-7 error-end">
                  <div class="input-group effect-end date">
                    <input type="text" name="end" class="form-control borderzero" id="row-end" maxlength="30" placeholder="">
                    <span class="input-group-addon">
                      <span><i class="fa fa-calendar"></i></span>
                    </span>
                  </div>
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <!-- <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">CLASSIFICATION</label>
                <div class="col-sm-9 col-xs-7 error-classification">
                  <select name="classification" class="form-control borderzero" id="row-classification" placeholder="">
                    <option value="" class="hide">Select</option>
                    <option value="Main">Main</option>
                    <option value="Rider">Rider</option>
                  </select>
                  <sup class="sup-errors"></sup>
                </div>
              </div> -->
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">RENEWAL</label>
                <div class="col-sm-9 col-xs-7 error-renewal">
                  <input type="text" name="renewal" class="form-control borderzero" id="row-renewal" placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
               <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">DETAILS</label>
                <div class="col-sm-9 col-xs-7 error-details">
                  <input type="text" name="details" class="form-control borderzero" id="row-details" placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
             
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">REMARKS</label>
                <div class="col-sm-9 col-xs-7 error-remarks">
                  <input type="text" name="remarks" class="form-control borderzero" id="row-remarks"  placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">TERM</label>
                <div class="col-sm-9 col-xs-7 error-term">
                  <input type="text" name="term" class="form-control borderzero" id="row-term"  placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label for="endterm" class="col-sm-3 col-xs-5">END TERM</label>
                <div class="col-sm-9 col-xs-7 error-endterm">
                  <input type="text" name="endterm" class="form-control borderzero" id="row-endterm"  placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">CONVERSION RATE</label>
                <div class="col-sm-9 col-xs-7 error-conversion_rate">
                  <input type="text" name="conversion_rate" class="form-control borderzero" id="row-conversion_rate"  placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5"></label>
                <div class="col-sm-9 col-xs-7">
                  <div id="conversions_view"></div>
                  <input type="checkbox" name="conversion_5" id="row-conversion_5years">if 5 yrs then = .5  
                  <input type="checkbox" name="conversion_8" id="row-conversion_8years"> if 8 yrs = .8
                </div>
              </div>
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-lg-12">
                  <div class="form-group">
                      <label for="row-code" class="col-sm-3 col-lg-2"><button type="button" value="" class="clone btn-add-gross hide-view"><i class="fa fa-plus Add"></i></button> GROSS REV</label>
                      <button type="button" value="" class="clone btn-reset-gross hide-view">Reset</button>
                      <div class="col-lg-12">
                      <div class="table-responsive">
                      <table id="gross-table" class="table">
                          <tbody id="gross-table-body">
                              <tr>
                                  <td class="col-xs-2">
                                  <div class="input-group col-xs-12">
                                  <label id="gross-label">YEAR 1</label>
                                  </div>
                                  </td>
                                  <td id="gross-error" class="col-xs-6 error-gross_value_1">
                                    <div class="input-group">
                                    <input type="text" name="gross_value[]" class="form-control borderzero" id="row-gross_value" maxlength="30">
                                      <span class="input-group-addon">
                                        <span>%</span>
                                      </span>
                                    </div>
                                    <sup class="sup-errors"></sup>
                                  </td>
                                  <td class="hide-sta leftalign">
                                      <input type="hidden" name="gross_id[]" value="">
                                      <input type="hidden" id="row-gross_year_id" name="gross_year_id[]" value="1">
                                      <button type="button" value="" class="btn-del-gross hide-view" title="Remove"> <i class="fa fa-close"> </i></button>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            <hr>
              <div class="form-group">
                <label id="rate" for="email" class="col-sm-3 col-xs-5">RPBC</label>
                <div class="col-sm-9 col-xs-7 table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th><small><center>Year 1</center></small></th>
                        <th><small><center>Year 2</center></small></th>
                        <th><small><center>Year 3</center></small></th>
                        <th><small><center>Year 4</center></small></th>
                        <th><small><center>Year 5</center></small></th>
                        <th><small><center>Year 6</center></small></th>
                        <th><small><center>Year 99</center></small></th>
                        <th><small><center>SPBC</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="error-rpbc_y1"><input type="text" name="rpbc_y1" class="form-control borderzero" id="row-rpbc_y1"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpbc_y2"><input type="text" name="rpbc_y2" class="form-control borderzero" id="row-rpbc_y2"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpbc_y3"><input type="text" name="rpbc_y3" class="form-control borderzero" id="row-rpbc_y3"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpbc_y4"><input type="text" name="rpbc_y4" class="form-control borderzero" id="row-rpbc_y4"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpbc_y5"><input type="text" name="rpbc_y5" class="form-control borderzero" id="row-rpbc_y5"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpbc_y6"><input type="text" name="rpbc_y6" class="form-control borderzero" id="row-rpbc_y6"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpbc_y99"><input type="text" name="rpbc_y99" class="form-control borderzero" id="row-rpbc_y99"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-spbc"><input type="text" name="spbc" class="form-control borderzero" id="row-spbc"  placeholder=""><sup class="sup-errors"></sup></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="form-group">
                <label id="rate" for="email" class="col-sm-3 col-xs-5">RPOR</label>
                <div class="col-sm-9 col-xs-7 table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th><small><center>Year 1</center></small></th>
                        <th><small><center>Year 2</center></small></th>
                        <th><small><center>Year 3</center></small></th>
                        <th><small><center>Year 4</center></small></th>
                        <th><small><center>Year 5</center></small></th>
                        <th><small><center>Year 6</center></small></th>
                        <th><small><center>Year 99</center></small></th>
                        <th><small><center>SPOR</center></small></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="error-rpor_y1"><input type="text" name="rpor_y1" class="form-control borderzero" id="row-rpor_y1"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpor_y2"><input type="text" name="rpor_y2" class="form-control borderzero" id="row-rpor_y2"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpor_y3"><input type="text" name="rpor_y3" class="form-control borderzero" id="row-rpor_y3"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpor_y4"><input type="text" name="rpor_y4" class="form-control borderzero" id="row-rpor_y4"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpor_y5"><input type="text" name="rpor_y5" class="form-control borderzero" id="row-rpor_y5"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpor_y6"><input type="text" name="rpor_y6" class="form-control borderzero" id="row-rpor_y6"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-rpor_y99"><input type="text" name="rpor_y99" class="form-control borderzero" id="row-rpor_y99"  placeholder=""><sup class="sup-errors"></sup></td>
                        <td class="error-spor"><input type="text" name="spor" class="form-control borderzero" id="row-spor"  placeholder=""><sup class="sup-errors"></sup></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row modal-footer borderzero">
                <input type="hidden" name="provider_id" id="row-provider_id">
                <input type="hidden" name="main_id" id="row-main_id">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>

      <!-- modal edit rate -->
      <div class="modal fade" id="modal-case" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-case" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'product/save-case', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_product')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-case-title"></h4>
              </div>
              <div class="modal-body">
              <div id="case-notice"></div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">PROVIDER</label>
                <div class="col-sm-9 col-xs-7">
                <input type="text" id="case-static-provider" class="form-control borderzero" placeholder="" disabled="disabled">
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="col-sm-3 col-xs-5">PRODUCT CATEGORY</label>
                <div class="col-sm-9 col-xs-7 error-category_id">
                  <select name="category_id" class="form-control borderzero" id="case-category_id" placeholder="">

                  </select>
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label for="case-name" class="col-sm-3 col-xs-5">NAME</label>
                <div class="col-sm-9 col-xs-7 error-name">
                <input type="text" name="name" class="form-control borderzero" id="case-name" placeholder="">
                <sup class="sup-errors"></sup>
              </div>
              </div>
              <div class="row modal-footer borderzero">
                <input type="hidden" name="provider_id" id="case-provider_id">
                <input type="hidden" name="main_id" id="case-main_id">
                <input type="hidden" name="id" id="case-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>


      <!-- modal edit rate -->
      <div class="modal fade" id="modal-rate" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content borderzero">
                <div id="load-rate" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'product/rate', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_product')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Rate</h4>
              </div>
              <div class="modal-body">
               <div id="rate-notice"></div>
              <div class="form-group">
                <label for="row-rate" class="col-sm-3 col-xs-5">RATE</label>
                <div class="col-sm-9 col-xs-7">
                <input type="text" name="rate" class="form-control borderzero" id="row-rate" placeholder="">
              </div>
              </div>
              <div class="row modal-footer borderzero">
            <input type="hidden" name="product_id" id="rate-product_id">
            <input type="hidden" name="id" id="rate-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>

      <!-- modal edit rate -->
      <div class="modal fade" id="modal-main" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderzero">
                <div id="load-main" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'product/save-main', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_product')) !!}
                  <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-main-title"></h4>
              </div>
              <div class="modal-body">
              <div id="main-notice"></div>
              <div class="form-group select-provider">
                <label for="main-provider_id" class="col-sm-3 col-xs-5">PROVIDER</label>
                <div class="col-sm-9 col-xs-7 error-provider_id">
                  <select name="provider_id" id="main-provider_id" class="form-control input-md borderzero">
                  <option value="" class="hide"></option>
                  @foreach ($providers as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                  @endforeach
                  </select>
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label for="main-name" class="col-sm-3 col-xs-5">NAME</label>
                <div class="col-sm-9 col-xs-7 error-name">
                  <input type="text" name="name" class="form-control borderzero" id="main-name" placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group main-conversion-form">
                <label for="main-premium_paying_term" class="col-sm-3 col-xs-5">PREMIUM PAYING TERM</label>
                <div class="col-sm-9 col-xs-7 error-premium_paying_term">
                  <input type="text" name="premium_paying_term" class="form-control borderzero" id="main-premium_paying_term" readonly="readonly"  placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group main-conversion-form">
                <label for="main-maturity_term" class="col-sm-3 col-xs-5">MATURITY TERM</label>
                <div class="col-sm-9 col-xs-7 error-maturity_term">
                  <input type="text" name="maturity_term" class="form-control borderzero" id="main-maturity_term" readonly="readonly"  placeholder="">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group main-conversion-form">
                <label for="main-conversion_rate" class="col-sm-3 col-xs-5">CONVERSION RATE</label>
                <div class="col-sm-9 col-xs-7 error-conversion_rate">
                  <input type="text" name="conversion_rate" class="form-control borderzero" id="main-conversion_rate" readonly="readonly" value="1">
                  <sup class="sup-errors"></sup>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-offset-3 col-sm-9 col-xs-12"><input type="checkbox" name="conversion" id="main-conversion" value="1"> ADD CONVERSION RATE</label>
              </div>
              <div class="form-group hide">
                <label for="email" class="col-sm-3 col-xs-5"></label>
                <div class="col-sm-9 col-xs-7">
                  <div id="conversions_view"></div>
                  <input type="checkbox" name="conversion_5" id="main-conversion_5years">if 5 yrs then = .5  
                  <input type="checkbox" name="conversion_8" id="main-conversion_8years"> if 8 yrs = .8
                </div>
              </div>
              <div class="row modal-footer borderzero">
                <input type="hidden" name="id" id="main-id">
                <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-submit hide-view borderzero"><i class="fa fa-save"></i> Save changes</button>
              </div>
            </div>
          {!! Form::close() !!}
          </div>
        </div>
      </div>


  <!-- Multiple Selection Modal -->
  <div id="modal-multiple" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="load-multiple" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
        <div id="modal-multiple-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-multiple-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-multiple-body"></p> 
        </div>
        <div class="modal-footer borderzero">
          <input type="hidden" id="row-multiple-hidden">
          <button type="button" class="btn btn-default borderzero" id="modal-multiple-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>