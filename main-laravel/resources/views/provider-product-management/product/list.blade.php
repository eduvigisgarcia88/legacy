@extends('layouts.master')


@section('scripts')
  <script>
$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });
  @if (Auth::user()->usertype_id != 8)
    $rows_rider = '<tr>' +
              '<th class="th-sort" data-sort="main_name"><i></i> <small>MAIN PRODUCT</small></th>' +
              '<th class="th-sort" data-sort="product_name"><i></i> <small>DESCRIPTION</small></th>' +
              '<th class="th-sort" data-sort="provider_name"><i></i> <small>PROVIDER</small></th>' +
              '<th class="th-sort" data-sort="code"><i></i> <small>CODE</small></th>' +
              '<th class="th-sort" data-sort="cat_name"><i></i> <small>PRODUCT<br>CATEGORY</small></th>' +
              '<th class="th-sort" data-sort="class_name"><i></i> <small>PRODUCT<br>CLASSIFICATION</small></th>' +
              '<th class="rightalign no-sort"><small>TOOLS</small></th>' +
            '</tr>';

    $rows_main = '<tr>' +
              '<th class="th-sort" nowrap data-sort="name"><i></i> MAIN PRODUCT</th>' +
              '<th class="th-sort" nowrap data-sort="provider_name"><i></i> PROVIDER</th>' +
              '<th class="rightalign no-sort">TOOLS</th>' +
            '</tr>';

    $rows_case = '<tr>' +
              '<th class="th-sort" data-sort="main_name"><i></i> <small>MAIN PRODUCT</small></th>' +
              '<th class="th-sort" data-sort="name"><i></i> <small>DESCRIPTION</small></th>' + 
              '<th class="th-sort" data-sort="provider_name"><i></i> <small>PROVIDER</small></th>' +
              '<th class="th-sort" data-sort="cat_name"><i></i> <small>PRODUCT<br>CATEGORY</small></th>' +
              '<th class="rightalign no-sort"><small>TOOLS</small></th>' +
            '</tr>';
  @else
    $rows_rider = '<tr>' +
              '<th class="th-sort" data-sort="main_name"><i></i> <small>MAIN PRODUCT</small></th>' +
              '<th class="th-sort" data-sort="product_name"><i></i> <small>DESCRIPTION</small></th>' +
              '<th class="th-sort" data-sort="provider_name"><i></i> <small>PROVIDER</small></th>' +
              '<th class="th-sort" data-sort="code"><i></i> <small>CODE</small></th>' +
              '<th class="th-sort" data-sort="cat_name"><i></i> <small>PRODUCT<br>CATEGORY</small></th>' +
              '<th class="th-sort" data-sort="class_name"><i></i> <small>PRODUCT<br>CLASSIFICATION</small></th>' +
            '</tr>';

    $rows_main = '<tr>' +
              '<th class="th-sort" nowrap data-sort="name"><i></i> MAIN PRODUCT</th>' +
              '<th class="th-sort" nowrap data-sort="provider_name"><i></i> PROVIDER</th>' +
            '</tr>';

    $rows_case = '<tr>' +
                    '<th class="th-sort" data-sort="main_name"><i></i> <small>MAIN PRODUCT</small></th>' +
                    '<th class="th-sort" data-sort="name"><i></i> <small>DESCRIPTION</small></th>' + 
                    '<th class="th-sort" data-sort="provider_name"><i></i> <small>PROVIDER</small></th>' +
                    '<th class="th-sort" data-sort="cat_name"><i></i> <small>PRODUCT<br>CATEGORY</small></th>' +
                  '</tr>';
  @endif

  $_token = '{{ csrf_token() }}';
  $gross_tbody = $("#gross-table-body").html();
  $selectedID = 0;
  $loading = false;
  function refresh() {
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();
      var loading = $(".loading-pane");
      var table = $("#rows");

      var headers = $("#rows_header").html();

      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, headers: headers, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        // clear
        table.html("");
      
        table.html(response.paginate);
        $("#rows_header").html(response.paginate_headers);

        $selectedID = 0;
        $loading = false;
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }

    function search() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();
      
      var loading = $(".loading-pane");
      var table = $("#rows");
      var headers = $("#rows_header").html();

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, headers: headers, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        // clear
        table.html("");
        table.html(response.paginate);
        $("#rows_header").html(response.paginate_headers);

        $selectedID = 0;
        $loading = false;
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    
    }

    function changeClass() {

      var headers = '';

      $status = $("#row-filter_status").val();
      if ($status == 'Main') {
        $("#row-per").val(10);
        headers = $rows_main;
      } else if ($status == 'Rider') {
        $("#row-per").val(100);
        headers = $rows_rider;
      } else if ($status == 'Case') {
        $("#row-per").val(100);
        headers = $rows_case;
      }

      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $per = $("#row-per").val();
      var loading = $(".loading-pane");
      var table = $("#rows");


      loading.removeClass("hide");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, headers: headers, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        
        // clear
        table.html("");
      
        table.html(response.paginate);
        $("#rows_header").html(response.paginate_headers);

        $selectedID = 0;
        $loading = false;
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    
    }

    $('.effect-start').datetimepicker({
      format: 'MM/DD/YYYY'
    });

    $('.effect-end').datetimepicker({
      format: 'MM/DD/YYYY',
       useCurrent: false,
    });

    $(".effect-start").on("dp.change", function (e) {
        $('.effect-end').data("DateTimePicker").minDate(e.date);
    });

    $(".effect-end").on("dp.change", function (e) {
        $('.effect-start').data("DateTimePicker").maxDate(e.date);
    });
   
    $(document).ready(function() {

      $(".btn-refresh").click(function() {
        refresh();
      });

    $(".btn-reset-gross").click(function() {
        var table = $("#gross-table tbody");
        table.html($gross_tbody);
    });

    $(".btn-add-gross").click(function() {
        var table = $("#gross-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();
        var length = parseInt(table.find("input[name='gross_year_id[]']:last").val());

        newrow.appendTo(table);
        table.find("input[name='gross_value[]']:last").focus();
        table.find("tr:last #gross-label").html("YEAR " + (length+1));

        var last_tr = table.find("tr:last");
        last_tr.find('select').val("");
        last_tr.find('input').val("");
        last_tr.find("#gross-error").removeAttr('class').attr('class', 'col-xs-6 error-gross_value_' + (length+1));
        last_tr.find("input[name='gross_year_id[]']:last").val(length+1);
    });

      // disable user
      $("#page-content-wrapper").on("click", ".btn-delete", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        
        dialog('Product Deletion', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('product/delete') }}", id);
      });


      // disable user
      $("#page-content-wrapper").on("click", ".btn-delete-case", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");
        
        dialog('Case Submission Rider Deletion', 'Are you sure you want to delete  <strong>' + name + '</strong>?', "{{ url('product/delete-case') }}", id);
      });


    // delete riders
    $(document).on('click', '.btn-del-gross', function() {
        var table = $("#gross-table tbody");
        var row = $(this).parent().parent();
        var rowcount = table.find("tr").length;

        if(rowcount > 1) {
            row.remove();
            rowcount--;
        } else {
            table.html($gross_tbody);
        }
    });

    //edit product
    $("#page-content-wrapper").on("click", ".btn-add-main", function() {

      $("#main-premium_paying_term").attr("readonly", "readonly");
      $("#main-maturity_term").attr("readonly", "readonly");
      $("#main-conversion_rate").attr("readonly", "readonly");
      $("#main-conversion").prop('checked', false);

      $("#modal-main").find("form").trigger("reset");
      $("#main-id").val("");
      $("#main-conversion").val(1);
      $(".modal-main-title").html("<i class='fa fa-upload'></i> Add Main Product");
      $("#modal-main").modal("show");
    });

    $('#main-conversion').change(function() {
      if($(this).is(":checked")) {
        $("#main-premium_paying_term").removeAttr("readonly");
        $("#main-maturity_term").removeAttr("readonly");
        $("#main-conversion_rate").removeAttr("readonly");

        $(".main-conversion-form .sup-errors").html("");
        $(".main-conversion-form .form-control").removeClass("required");
      } else {
        $("#main-premium_paying_term").attr("readonly", "readonly");
        $("#main-maturity_term").attr("readonly", "readonly");
        $("#main-conversion_rate").attr("readonly", "readonly");

        $("#main-premium_paying_term").val("");
        $("#main-maturity_term").val("");
        $("#main-conversion_rate").val("1");

        $(".main-conversion-form .sup-errors").html("");
        $(".main-conversion-form .form-control").removeClass("required");
      }
    });

      //edit product
      $("#page-content-wrapper").on("click", ".btn-edit-main", function() {

        $("#modal-main").find("form").trigger("reset");
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        $("#main-premium_paying_term").attr("readonly", "readonly");
        $("#main-maturity_term").attr("readonly", "readonly");
        $("#main-conversion_rate").attr("readonly", "readonly");
        $("#main-conversion").prop('checked', false);

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");

        $.post("{{ url('product/view-main') }}", { id: id, _token: $_token }, function(response) {
              if(!response.error) {
                // set form title
                $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View Product</strong>');

                // output form data
                $(".modal-main-title").html("<i class='fa fa-upload'></i> Add Main Product");

                $.each(response, function(index, value) {
                  var field = $("#main-" + index);

                  if (field.length > 0) {
                    field.val(value);
                  }

                  if (response.conversion_year == "5") {
                    $("#main-conversion_5years").prop('checked', true);
                    $("#main-conversion_rate").attr("readonly", "readonly");
                  } else if (response.conversion_year  == "8") {
                    $("#main-conversion_8years").prop('checked', true);
                    $("#main-conversion_rate").attr("readonly", "readonly");
                  } else if (response.conversion_year  == "5and8") {
                    $("#main-conversion_5years").prop('checked', true);
                    $("#main-conversion_8years").prop('checked', true);
                    $("#main-conversion_rate").attr("readonly", "readonly");
                  }
            
                  if (response.conversion > 0) {
                    $("#main-premium_paying_term").removeAttr("readonly");
                    $("#main-maturity_term").removeAttr("readonly");
                    $("#main-conversion_rate").removeAttr("readonly");
                    $("#main-conversion").prop('checked', true);

                    $("#main-premium_paying_term").val(response.premium_paying_term);
                    $("#main-maturity_term").val(response.maturity_term);
                    $("#main-conversion_rate").val(response.conversion_rate);
                  } else {
                    $("#main-premium_paying_term").attr("readonly", "readonly");
                    $("#main-maturity_term").attr("readonly", "readonly");
                    $("#main-conversion_rate").attr("readonly", "readonly");

                    $("#main-premium_paying_term").val("");
                    $("#main-maturity_term").val("");
                    $("#main-conversion_rate").val(response.conversion_rate);
                  }
                });

                $("#main-conversion").val(1);
                $("#modal-main").modal("show");
              } else {
                  status(false, response.error, 'alert-danger');
              }

              btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");

        }, 'json');
      });


      $("#row-provider_id").on("change", function() {

        if($(this).is(":checked")) {
          $("#row-category_id").attr("disabled","disabled");
          $("#row-classification_id").attr("disabled","disabled");
        } else {
          $("#row-category_id").removeAttr("disabled","disabled");
          $("#row-classification_id").removeAttr("disabled","disabled");
        }
      });

      $('#modal-product').on('hidden.bs.modal', function () {
        $("#modal-product").find('input').removeAttr('readonly', 'readonly');
        $("#modal-product").find('select').removeAttr('disabled', 'disabled');
        $("#modal-product").find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
        $("#modal-product").find('form').trigger("reset");
      });


      $('#modal-main').on('hidden.bs.modal', function () {
        $("#modal-main").find('input[type="checkbox"]').removeAttr('disabled', 'disabled');
      });


      $("#page-content-wrapper").on("click", ".btn-add-case", function() {

        $(".hide-view").removeClass("hide");
        $("#modal-case").find('form').trigger("reset");
        $(".modal-case-title").html("<i class='fa fa-plus'></i> Add Case Submission Rider (" + $(this).parent().parent().find('td:nth-child(1)').html() + " )");
        $(".modal-case-title").find("a").remove();
        $("#case-static-provider").val($(this).parent().parent().find('td:nth-child(2)').html());

        var id = $(this).data('provider');
        var main_id = $(this).data('id');
        var btn = $(this);

        btn.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
        var category = $("#case-category_id");
        category.html("");

        $.post("{{ url('product/get-property') }}", { id: id, _token: $_token }, function(response) {

          if (response.categories.length > 0) {
            category.append(
              '<option value="" class="hide">Select:</option>'
            );
            $.each(response.categories, function(index, row) {
              category.append(
                '<option value="' + row.id + '">' + row.name + '</option>'
              );
            });
          } else {
            category.append(
              '<option value="" class="hide">Select:</option>'
            );
          }

          $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
          $("#case-main_id").val(main_id);
          $("#case-provider_id").val(id);
          $("#modal-case").modal('show');


          btn.html("+ <i class='fa fa-briefcase'></i>");

        }, 'json');

      });

      //add user
      $("#page-content-wrapper").on("click", ".btn-add", function() {

        $(".hide-view").removeClass("hide");
        $("#gross-table tbody").html($gross_tbody);
        $("#modal-product").find('form').trigger("reset");
        $("#row-id").val("");
        $(".modal-title").html("<i class='fa fa-plus'></i> Add Case Submission Rider (" + $(this).parent().parent().find('td:nth-child(1)').html() + " )");
        $(".modal-title").find("a").remove();
        $("#row-static-provider").val($(this).parent().parent().find('td:nth-child(2)').html());

        var classification = $("#row-classification_id");
        var category = $("#row-category_id");
        var id = $(this).data('provider');
        var main_id = $(this).data('id');
        var btn = $(this);

        btn.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
        classification.html("");
        category.html("");

        $.post("{{ url('product/get-property') }}", { id: id, _token: $_token }, function(response) {

          if (response.classifications.length > 0) {
            classification.append(
              '<option value="" class="hide">Select:</option>'
            );

            $.each(response.classifications, function(index, row) {
              classification.append(
                '<option value="' + row.id + '">' + row.name + '</option>'
              );
            });
          } else {
            classification.append(
              '<option value="" class="hide">Select:</option>'
            );
          }

          if (response.categories.length > 0) {
            category.append(
              '<option value="" class="hide">Select:</option>'
            );
            $.each(response.categories, function(index, row) {
              category.append(
                '<option value="' + row.id + '">' + row.name + '</option>'
              );
            });
          } else {
            category.append(
              '<option value="" class="hide">Select:</option>'
            );
          }

          $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
          $("#row-main_id").val(main_id);
          $("#row-provider_id").val(id);
          $("#modal-product").modal('show');


          btn.html("+ <i class='fa fa-line-chart'></i>");

        }, 'json');

      });

      $("#page-content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-eye");

          // reset all form fields\
          $(".modal-header").removeAttr("class").addClass("modal-header modal-success");
          $("#modal-product").find('form').trigger("reset");
          $("#row-id").val("");
          $(".hide-view").addClass("hide");
          $("#gross-table tbody").html($gross_tbody);
          $("#modal-product").find('form').trigger("reset");
          $(".modal-title").html("<i class='fa fa-plus'></i> Editing (" + $(this).parent().parent().find('td:nth-child(1)').html() + " )");
          $(".modal-title").find("a").remove();
          $("#row-static-provider").val($(this).parent().parent().find('td:nth-child(2)').html());

          var classification = $("#row-classification_id");
          var category = $("#row-category_id");
          var main_id = $(this).data('id');
          var btn = $(this);

          btn.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
          classification.html("");
          category.html("");

          $.post("{{ url('product/get-property') }}", { id: id, _token: $_token }, function(response) {

            if (response.classifications.length > 0) {
              classification.append(
                '<option value="" class="hide">Select:</option>'
              );

              $.each(response.classifications, function(index, row) {
                classification.append(
                  '<option value="' + row.id + '">' + row.name + '</option>'
                );
              });
            } else {
              classification.append(
                '<option value="" class="hide">Select:</option>'
              );
            }

            if (response.categories.length > 0) {
              category.append(
                '<option value="" class="hide">Select:</option>'
              );
              $.each(response.categories, function(index, row) {
                category.append(
                  '<option value="' + row.id + '">' + row.name + '</option>'
                );
              });
            } else {
              category.append(
                '<option value="" class="hide">Select:</option>'
              );
            }
          }, 'json');

          $.post("{{ url('product/view') }}", { id: id, _token: $_token }, function(response) {
              if(!response.error) {
              // set form title
                  $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View Product</strong>');

                  // output form data
                  $.each(response, function(index, value) {
                    // console.log(response);
                      var field = $("#row-" + index);

                      if (index == "get_gross") {
                        
                        if (value.length > 0) {
                          var table = $("#gross-table tbody");
                          var row = table.find("tr:first");

                          table.html("");

                          $.each(value, function(cindex, cvalue) {
                            var newrow = row.clone();

                            $.each(cvalue, function(ptindex, ptvalue) {

                              var rows = "input[name='gross_" + ptindex + "[]']";
                              //console.log(rows);
                              var field = newrow.find(rows);

                              if(field.length > 0) {
                                if (ptindex == "year_id") {
                                  field.parent().parent().find('label').html("YEAR " + ptvalue);
                                }
                                // console.log(ptindex+"="+ptvalue);
                                field.val(ptvalue);
                              }
                            });

                            newrow.appendTo(table);

                          });

                        // delete empty first row
                        row.remove();
                        }
                      }

                      if (index == "get_provider") {
                        if (value) {
                          $.each(value, function(cindex, cvalue) {
                            if (cindex == "id") {
                              $("#row-provider_id").val(cvalue);
                            } else if (cindex == "get_provider_category") {
                              if (cvalue.length > 0) {
                                $.each(cvalue, function(pcindex, pcvalue) {
                                  $("#row-category_id").append(
                                    '<option value="' +  pcvalue.id + '">' + pcvalue.name + '</option>' 
                                    );
                                });
                                $("#row-category_id").val(response.category_id);
                              }
                            } else if (cindex == "get_provider_classification") {
                              if (cvalue.length > 0) {
                                $.each(cvalue, function(pcindex, pcvalue) {
                                  $("#row-classification_id").append(
                                    '<option value="' +  pcvalue.id + '">' + pcvalue.name + '</option>' 
                                    );
                                });
                                $("#row-classification_id").val(response.classification_id);
                              }
                            }

                          });
                        }
                      }

                      // field exists, therefore populate
                      if(field.length > 0) {
                        if (field.parent().hasClass('date')) {
                          if (value != "0000-00-00") {
                            field.val(moment(value).format('MM/DD/YYYY'));
                          }
                        } else {
                          field.val(value);
                        }
                      }

                      if (response.conversion_year == "5") {
                        $("#row-conversion_5years").prop('checked', true);
                      } else if (response.conversion_year  == "8") {
                        $("#row-conversion_8years").prop('checked', true);
                      } else if (response.conversion_year  == "5and8") {
                        $("#row-conversion_5years").prop('checked', true);
                        $("#row-conversion_8years").prop('checked', true);
                      }

                  });

                  $("#modal-product").find('.hide-view').addClass('hide');
                  $("#modal-product").find('.preview').removeClass('hide');
                  $("#modal-product").find('.change').addClass('hide');
                  $("#modal-product").find('.show-view').removeClass('hide');
                  $("#modal-product").find('input').attr('readonly', 'readonly');
                  $("#modal-product").find('select').attr('disabled', 'disabled');
                  $("#modal-product").find('input[type="checkbox"]').attr('disabled', 'disabled');
                  // show form
                  $("#modal-product").modal('show');
              } else {
                  status(false, response.error, 'alert-danger');
              }

              btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-eye");
          }, 'json');
      });

      $("#page-content-wrapper").on("click", ".btn-edit-case", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);

        // reset all form fields
        $("#modal-product").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#case-id").val("");
        $(".hide-view").removeClass("hide");
        $("#gross-table tbody").html($gross_tbody);
        $("#modal-product").find('form').trigger("reset");
        $(".modal-title").html("<i class='fa fa-plus'></i> Editing (" + $(this).parent().parent().find('td:nth-child(1)').html() + " )");
        $(".modal-title").find("a").remove();
        $("#case-static-provider").val($(this).parent().parent().find('td:nth-child(2)').html());

        var category = $("#case-category_id");
        var main_id = $(this).data('id');
        var provider_id = $(this).data('provider');
        var btn = $(this);

        btn.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
        category.html("");

        $.post("{{ url('product/get-property') }}", { id: provider_id, _token: $_token }, function(response) {

          if (response.categories.length > 0) {
            category.append(
              '<option value="" class="hide">Select:</option>'
            );
            $.each(response.categories, function(index, row) {
              category.append(
                '<option value="' + row.id + '">' + row.name + '</option>'
              );
            });
          } else {
            category.append(
              '<option value="" class="hide">Select:</option>'
            );
          }
        }, 'json');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
        $.post("{{ url('product/view-case') }}", { id: id, _token: $_token }, function(response) {
        if(!response.error) {
        // set form title
          $(".modal-case-title").html('<i class="fa fa-pencil"></i> <strong>Edit Case Submission Rider</strong>');

            $.each(response, function(index, value) {

              var field = $("#case-" + index);

              if (index == "get_provider") {
                if (value) {
                  $.each(value, function(cindex, cvalue) {
                    if (cindex == "id") {
                      $("#case-provider_id").val(cvalue);
                    } else if (cindex == "get_provider_category") {
                      if (cvalue.length > 0) {
                        $.each(cvalue, function(pcindex, pcvalue) {
                          $("#case-category_id").append(
                            '<option value="' +  pcvalue.id + '">' + pcvalue.name + '</option>' 
                            );
                        });
                        $("#case-category_id").val(response.category_id);
                      }
                    }

                  });
                }
              }

              // field exists, therefore populate
              if(field.length > 0) {
                field.val(value);
              }
            });

            // show product
            $("#modal-case").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
        }, 'json');

      });

      //edit product
      $("#page-content-wrapper").on("click", ".btn-edit", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);

      // reset all form fields
      $("#modal-product").find('form').trigger("reset");
      $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
      $("#row-id").val("");
      $(".hide-view").removeClass("hide");
      $("#gross-table tbody").html($gross_tbody);
      $("#modal-product").find('form').trigger("reset");
      $(".modal-title").html("<i class='fa fa-plus'></i> Editing (" + $(this).parent().parent().find('td:nth-child(1)').html() + " )");
      $(".modal-title").find("a").remove();
      $("#row-static-provider").val($(this).parent().parent().find('td:nth-child(2)').html());

      var classification = $("#row-classification_id");
      var category = $("#row-category_id");
      var main_id = $(this).data('id');
      var btn = $(this);

      btn.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
      classification.html("");
      category.html("");

      $.post("{{ url('product/get-property') }}", { id: id, _token: $_token }, function(response) {

        if (response.classifications.length > 0) {
          classification.append(
            '<option value="" class="hide">Select:</option>'
          );

          $.each(response.classifications, function(index, row) {
            classification.append(
              '<option value="' + row.id + '">' + row.name + '</option>'
            );
          });
        } else {
          classification.append(
            '<option value="" class="hide">Select:</option>'
          );
        }

        if (response.categories.length > 0) {
          category.append(
            '<option value="" class="hide">Select:</option>'
          );
          $.each(response.categories, function(index, row) {
            category.append(
              '<option value="' + row.id + '">' + row.name + '</option>'
            );
          });
        } else {
          category.append(
            '<option value="" class="hide">Select:</option>'
          );
        }
      }, 'json');

      btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
      $.post("{{ url('product/view') }}", { id: id, _token: $_token }, function(response) {
        if(!response.error) {
        // set form title
          $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Product</strong>');


                  // output form data
                  $.each(response, function(index, value) {
                      var field = $("#row-" + index);

                      if (index == "get_gross") {
                        
                        if (value.length > 0) {
                          var table = $("#gross-table tbody");
                          var row = table.find("tr:first");

                          table.html("");

                          $.each(value, function(cindex, cvalue) {
                            var newrow = row.clone();

                            $.each(cvalue, function(ptindex, ptvalue) {

                              var rows = "input[name='gross_" + ptindex + "[]']";
                              //console.log(rows);
                              var field = newrow.find(rows);


                              if(field.length > 0) {

                                if (ptindex == "year_id") {
                                  field.parent().parent().find("td:nth-child(2)").removeAttr("class").attr("class", "col-xs-6 error-gross_value_" + ptvalue);
                                  field.parent().parent().find('label').html("YEAR " + ptvalue);
                                }
                                // console.log(ptindex+"="+ptvalue);
                                field.val(ptvalue);
                              }
                            });

                            newrow.appendTo(table);

                          });

                        // delete empty first row
                        row.remove();
                        }
                      }

                      if (index == "get_provider") {
                        if (value) {
                          $.each(value, function(cindex, cvalue) {
                            if (cindex == "id") {
                              $("#row-provider_id").val(cvalue);
                            } else if (cindex == "get_provider_category") {
                              if (cvalue.length > 0) {
                                $.each(cvalue, function(pcindex, pcvalue) {
                                  $("#row-category_id").append(
                                    '<option value="' +  pcvalue.id + '">' + pcvalue.name + '</option>' 
                                    );
                                });
                                $("#row-category_id").val(response.category_id);
                              }
                            } else if (cindex == "get_provider_classification") {
                              if (cvalue.length > 0) {
                                $.each(cvalue, function(pcindex, pcvalue) {
                                  $("#row-classification_id").append(
                                    '<option value="' +  pcvalue.id + '">' + pcvalue.name + '</option>' 
                                    );
                                });
                                $("#row-classification_id").val(response.classification_id);
                              }
                            }

                          });
                        }
                      }

                      // field exists, therefore populate
                      if(field.length > 0) {
                        if (field.parent().hasClass('date')) {
                          if (value != "0000-00-00") {
                            field.val(moment(value).format('MM/DD/YYYY'));
                          }
                        } else {
                          field.val(value);
                        }
                      }
                      

                  });


            $("#modal-product").find('.hide-view').removeClass('hide');
            $("#modal-product").find('.show-view').addClass('hide');
            $("#modal-product").find('.preview').removeClass('hide');
            $("#modal-product").find('.change').addClass('hide');
            $("#modal-product").find('input').removeAttr('readonly', 'readonly');
            $("#modal-product").find('select').removeAttr('disabled', 'disabled');

            if (response.conversion_year == "5") {
              $("#row-conversion_5years").prop('checked', true);
              $("#row-conversion_rate").attr("readonly", "readonly");
            } else if (response.conversion_year  == "8") {
              $("#row-conversion_8years").prop('checked', true);
              $("#row-conversion_rate").attr("readonly", "readonly");
            } else if (response.conversion_year  == "5and8") {
              $("#row-conversion_5years").prop('checked', true);
              $("#row-conversion_8years").prop('checked', true);
              $("#row-conversion_rate").attr("readonly", "readonly");
            }
            
            // show product
            $("#modal-product").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
        }, 'json');

      });
        
      //edit rate
      $("#page-content-wrapper").on("click", ".btn-edit-rate", function() {
          $("#modal-rate").modal('show');
          // reset all form fields
          $("#modal-rate").find('form').trigger("reset");
          $("#rate-product_id").val($(this).data('id'));
        });

      // disable user
      $("#page-content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-danger");

        dialog('<i class="fa fa-adjust"></i> Remove', 'Are you sure you want to remove <strong class="dialog-name">' + name + '</strong>?', "{{ url('product/disable') }}", id);
        $(".dialog-name").find('a').remove();
      });

      // enable user
      $("#page-content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        var name = $(this).parent().parent().find('td:nth-child(1)').html();

        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        
        dialog('<i class="fa fa-check-circle"></i> Enable', 'Are you sure you want to enable <strong class="dialog-name">' + name + '</strong>?', "{{ url('product/enable') }}", id);
        $(".dialog-name").find('a').remove();
      });

      // show modal for multiple disable
      $("#page-content-wrapper").on("click", ".btn-disable-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("0");
        $(".modal-multiple-title").html("<i class='fa fa-adjust'></i> Multiple Disable");
        $(".modal-multiple-body").html("Are you sure you want to disable the selected [produtcs]/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-info");
        $("#modal-multiple").modal("show");
      });

      // show modal for multiple remove
      $("#page-content-wrapper").on("click", ".btn-remove-select", function() {
        $("#modal-multiple").find("input").val("");
        $("#row-multiple-hidden").val("1");
        $(".modal-multiple-title").html("<i class='fa fa-minus'></i> Multiple Remove");
        $(".modal-multiple-body").html("Are you sure you want to remove the selected [produtcs]/s?");
        $("#modal-multiple-header").removeAttr("class").addClass("modal-header modal-danger");
        $("#modal-multiple").modal("show");
      });

      $("#modal-multiple-button").click(function() {

        var ids = [];
        var loading = $("#load-multiple");
        $_token = "{{ csrf_token() }}";

        $("input:checked").each(function(){
          ids.push($(this).val());
        });
        loading.removeClass("hide");
        
        if (ids.length == 0) {
          status("Error", "Select product/s first.", 'alert-danger');
        } else {

          if ($("#row-multiple-hidden").val() == "0") {
            $.post("{{ url('product/disable-select') }}", { ids: ids, _token: $_token }, function(response) {
              loading.addClass("hide");
              $("#modal-multiple").modal("hide");
              refresh();
            }, 'json');
          } else if ($("#row-multiple-hidden").val() == "1") {
            $.post("{{ url('product/remove-select') }}", { ids: ids, _token: $_token }, function(response) {
              loading.addClass("hide");
              $("#modal-multiple").modal("hide");
              refresh();
            }, 'json');
          } else {
            status("Error", "Error! Refresh the page.", 'alert-danger');
          }
        }

      });

      $("#page-content-wrapper").on("click", ".btn-expand", function() {
      // $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
      $(".btn-expand.rider").html('<i class="fa fa-plus-square"></i>');
      // $(".btn-expand.rider").html('<i class="fa fa-minus-square"></i>');
         
        if ($loading) {
          return;
        }

        $tr = $(this).parent().parent();
        $id = $tr.data('id');
        $type = $(this).data('type');

        $type_id = $id + $type;

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $($selected).parent().parent().remove();
            
          });

        if ($type_id == $selectedID) {
          $selectedID = 0;
          return
        }

        $selectedID = $type_id;
        $button = $(this); 
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading_type = true;

        if ($type == "rider") {
          $.post("{{ url('product/product-case') }}", { id: $id, _token: $_token }, function(response) {
            $tr.after('<tr><td colspan="7" style="padding:0"><div class="row-' + $selectedID + ' container-fluid" style="display:none;">' + response + '</div></td></tr>');
            $('.row-' + $selectedID).slideDown();
            $button.html('<i class="fa fa-minus-square"></i>');
            $loading = false;
          });     
        } else if ($type == "product") {
          $.post("{{ url('product/product-rider') }}", { id: $id, _token: $_token }, function(response) {
            $tr.after('<tr><td colspan="7" style="padding:0"><div class="row-' + $selectedID + ' container-fluid" style="display:none;">' + response + '</div></td></tr>');
            $('.row-' + $selectedID).slideDown();
            $button.html('<i class="fa fa-minus"></i>');
            $loading = false;
          });     
        }

      });

    });

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        var headers = $("#rows_header").html();

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, headers: headers, search: $search, status: $status, per: $per, _token: $_token }, function(response) {

        $("#rows_header").html("");
       
          table.html(response.paginate);
          $("#rows_header").html(response.paginate_headers);

          $selectedID = 0;
          $loading = false;
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

         }, 'json');
      
    });

   $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");

        loading.removeClass("hide");

        var headers = $("#rows_header").html();

        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, headers: headers, search: $search, status: $status, per: $per, _token: $_token }, function(response) {

          table.html(response.paginate);
          $("#rows_header").html(response.paginate_headers);

          $selectedID = 0;
          $loading = false;
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

         }, 'json');

      }
    });

    $('#main-conversion_5years').change(function() {
      if($(this).is(":checked")) {
        $("#main-conversion_rate").val(1);
        $("#main-conversion_rate").attr("readonly","readonly");
      } else {
        if ($('#main-conversion_8years').is(":checked")) {
        } else {
          $("#main-conversion_rate").val("");
          $("#main-conversion_rate").removeAttr("readonly");
        }
      }
    });

    $('#main-conversion_8years').change(function() {
      if($(this).is(":checked")) {
        $("#main-conversion_rate").val(1);
        $("#main-conversion_rate").attr("readonly","readonly");
      } else {
        if ($('#main-conversion_5years').is(":checked")) {
        } else {
          $("#main-conversion_rate").val("");
          $("#main-conversion_rate").removeAttr("readonly");
        }
      }
    });

    $('#row-conversion_5years').change(function() {
      if($(this).is(":checked")) {
        $("#row-conversion_rate").val(1);
        $("#row-conversion_rate").attr("readonly","readonly");
      } else {
        if ($('#row-conversion_8years').is(":checked")) {
        } else {
          $("#row-conversion_rate").val("");
          $("#row-conversion_rate").removeAttr("readonly");
        }
      }
    });

    $('#row-conversion_8years').change(function() {
      if($(this).is(":checked")) {
        $("#row-conversion_rate").val(1);
        $("#row-conversion_rate").attr("readonly","readonly");
      } else {
        if ($('#row-conversion_5years').is(":checked")) {
        } else {
          $("#row-conversion_rate").val("");
          $("#row-conversion_rate").removeAttr("readonly");
        }
      }
    });

$('#row-search').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        search();
    }
});


</script>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper" class="response">
<!-- Title -->
<div class="container-fluid main-title-container container-space">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
        <h3 class="main-title">PRODUCT LIST</h3>
        <h5 class="bread-crumb">PROVIDER & PRODUCTS MANAGEMENT</h5>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 title-margin">
          @if(Auth::user()->usertype_id != 8)
          <button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-remove-select"><i class="fa fa-minus"></i> Remove</button>
          <button type="button" class="btn btn-sm btn-info borderzero btn-tool pull-right btn-disable-select"><i class="fa fa-adjust"></i> Disable</button>    
          <button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-add-main"><i class="fa fa-upload"></i> Add</button> 
          @endif
          <button type="button" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-refresh"><i class="fa fa-refresh"></i> Refresh</button> 
          @if(Auth::user()->usertype_id != 8)
          <form style="float: right; padding-left: 0px;" role="form" method="post" action="{{ url('product/export') }}">
            {{ csrf_field() }}
            <input type="hidden" name="export" id="row-export">
            <button type="submit" class="btn btn-sm btn-success borderzero btn-tool pull-right btn-export"><i class="fa fa-print"></i> Export Main</button>
          </form>
      
          <form style="float: right; padding-left: 0px;" role="form" method="post" action="{{ url('riders/product/export') }}">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-sm btn-success borderzero btn-tool pull-right"><i class="fa fa-print"></i> Export Case</button>
          </form>
<!--           <a role="button" href="{{url('riders/product/export')}}" class="btn btn-sm btn-success borderzero btn-tool pull-right"><i class="fa fa-print" download="RIDERS-PRODUCTS.xls"></i> Export Case</a> -->
          @endif
    </div>
    </div>
      <div class="container-fluid default-container container-space">
      <div class="col-lg-12">
            <p><strong>FILTER OPTIONS</strong></p>
      </div>
<div class="form-horizontal" >
    @if(Auth::user()->usertype_id != 8)
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="form-group">
    <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>CLASSIFICATION TYPE</strong></p></label>
        <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
          <select class=" form-control borderzero" onchange="changeClass()" id="row-filter_status">
            <option value="Rider" selected>Rider</option>
            <option value="Main">Main</option>
            <option value="Case">Case Submission Rider</option>
          </select>
        </div>
      </div>
    </div>
    @endif
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="" class="col-lg-3 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left"><p class="p"><strong>SEARCH VALUE</strong></p></label>
                      <div class="col-lg-9 col-md-6 col-sm-8 col-xs-6" style="padding-left:0;">
                        <div class="input-group">
                        <input type="text" class="form-control borderzero " placeholder="Search for..."  id="row-search">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-clear_search borderzero" type="button" onclick="search()"><i class="fa fa-search"></i> <span class="hidden-xs">SEARCH</span></button>
                        </span>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<div class="container-fluid content-table" style="border-top:1px #e6e6e6 solid;">
  <div class="block-content tbblock col-xs-12">
    <div class="loading-pane hide">
      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
    </div>
<div class="table-responsive">
  <table class="table table-striped table-pagination" id="my-table">
        <thead id="rows_header" class="tbheader">
          @if (Auth::user()->usertype_id == 8)
          <tr>
            <th class="th-sort" data-sort="main_name"><i></i> <small>MAIN PRODUCT</small></th>
            <th class="th-sort" data-sort="name"><i></i> <small>DESCRIPTION</small></th> 
            <th class="th-sort" data-sort="provider_name"><i></i> <small>PROVIDER</small></th>
            <th class="th-sort" data-sort="cat_name"><i></i> <small>PRODUCT<br>CATEGORY</small></th>
          </tr>
          @else
          <tr>
            <th class="th-sort" data-sort="main_name"><i></i> <small>MAIN PRODUCT</small></th>
            <th class="th-sort" data-sort="product_name"><i></i> <small>DESCRIPTION</small></th>
            <th class="th-sort" data-sort="provider_name"><i></i> <small>PROVIDER</small></th>
            <th class="th-sort" data-sort="code"><i></i> <small>CODE</small></th>
            <th class="th-sort" data-sort="cat_name"><i></i> <small>PRODUCT<br>CATEGORY</small></th>
            <th class="th-sort" data-sort="class_name"><i></i> <small>PRODUCT<br>CLASSIFICATION</small></th>
            @if(Auth::user()->usertype_id != 8)<th class="rightalign no-sort"><small>TOOLS</small></th>@endif
          </tr>
          @endif
        </thead>
        <tbody id="rows">
          <?php echo($paginate) ?>
        </tbody>
        </table>
      </div>
    </div>
  </div>
  <div id="row-pages" class="col-lg-6 col-md-8 col-sm-10 col-xs-12 text-center borderzero leftalign">{!! $pages !!}</div>
    <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 content-table" style="text-align: -webkit-right;">
    <select class="form-control borderzero rightalign" onchange="search()" id="row-per" style="width:70px; border: 0px;">
      <option value="10" >10</option>
      <option value="25">25</option>
      <option value="50">50</option>
      <option value="100" selected>100</option>
    </select>
  </div>
    <div class="text-center">
      <input type="hidden" id="row-page" value="1">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-order" value="">
    </div>
   </div>
    <!-- /#page-content-wrapper -->
@include('provider-product-management.product.form')

@stop