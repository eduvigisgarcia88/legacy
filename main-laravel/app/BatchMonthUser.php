<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchMonthUser extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'batch_month_users';

    public function designations() {
        return $this->belongsTo('App\Designation', 'designation_id');
    }
    
}