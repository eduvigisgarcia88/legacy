<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'log_users';

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'edit_id', 'activity'];

    public function userLog() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function editUserLog() {
        return $this->belongsTo('App\User', 'edit_id');
    }
}
