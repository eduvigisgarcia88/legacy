<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadFeed extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'upload_feeds';

    public function getFeedProvider() {
        return $this->belongsTo('App\Provider', 'provider_id');
    }

    public function getFeedType() {
        return $this->belongsTo('App\ProviderClassification', 'category_id');
    }
}