<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesSupervisorComputation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_supervisor_computations';

    public function salesCompAdvisor() {
        return $this->hasMany('App\SalesAdvisorComputation', 'supervisor_id');
    }

}