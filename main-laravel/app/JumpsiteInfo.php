<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JumpsiteInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jumpsite_info';


     public function jumpsiteinfo(){

    	return $this->belongsTo('App\User','user_id');
    }

}
