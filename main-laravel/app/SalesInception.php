<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesInception extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_inceptions';
}
