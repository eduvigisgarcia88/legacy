<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationPerUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications_per_users';

    public function notifications()
    {
        return $this->belongsTo('Notification', 'notification_id');
    }

}