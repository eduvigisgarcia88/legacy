<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrePayrollBSC extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pre_payroll_bsc';

}