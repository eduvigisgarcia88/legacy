<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollComputation extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payroll_computations';

    public function getPayrollComputationUser() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getPayrollComputationIntroducer() {
        return $this->belongsTo('App\Introducer', 'introducer_id');
    }

    public function getPayrollComputationProvider() {
        return $this->belongsTo('App\Provider', 'provider_id');
    }

    public function getPayrollComputationBatchPolicy() {
        return $this->belongsTo('App\BatchPolicy', 'batch_policy_id');
    }

    public function getPayrollComputationCategory() {
        return $this->belongsTo('App\ProviderClassification', 'category_id');
    }

    public function getPayrollComputationGross() {
        return $this->hasMany('App\ProductGross', 'product_id', 'product_id');
    }

}