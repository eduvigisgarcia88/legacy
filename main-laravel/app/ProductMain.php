<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMain extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_mains';

    public function getRiders() {
        return $this->hasMany('App\Product', 'main_id')->where('status', '1');
    }

    public function getCaseRiders() {
        return $this->hasMany('App\ProductRider', 'main_id')->where('status', '1');
    }
}
