<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyCount extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'policy_counts';

}
