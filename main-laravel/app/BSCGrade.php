<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BSCGrade extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bsc_grades';
  
}