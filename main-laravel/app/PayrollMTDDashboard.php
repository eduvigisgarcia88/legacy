<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollMTDDashboard extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payroll_mtd_dashboards';

}
