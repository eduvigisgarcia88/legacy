<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesUser extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_user_extension';

    public function userinfo() {
        return $this->belongsTo('App\User', 'user_id');
    }
  
}