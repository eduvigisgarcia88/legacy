<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Input;
use Validator;
use App\Permission;
use App\User;
use App\Sales;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\CPDHours;
use App\CPDReport;
use App\CPDReportContent;
use Paginator;
use Response;
use Carbon;
use Request;
use Excel;
use PDF;
use Redirect;
use App\Notification;
use App\NotificationPerUser;
use App\Settings;
use App\Group;

class ReportController extends Controller
{
    
    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    /**
     * Display a listing of the reports.
     *
     * @return Response
     */
    public function index()
    {
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_rank'] = Sales::with('designations')
                                          ->find(Auth::user()->id);
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['settings'] = Settings::first();
        $this->data['title'] = "System Users";
        return view('summary-and-reports.view-daily-production-report.list', $this->data);
    }

    /**
     * Display a listing of the exported reports.
     *
     * @return Response
     */

    public function getPerGroup(){
        // prevent non-admin from viewing deactivated row
        $get_group = Group::where('status','=',1)->get();

        $rows = '<option class="hide" value="">Select:</option>';

          foreach ($get_group as $key => $field) {
            $rows .= '<option value="' . $field->id . '">' . $field->code . '</option>';
          }

          return Response::json($rows);
    }
    public function getPerUser(){

       $get_user = User::where('status','=',1)->where('usertype_id','=',8)->get();

        $rows = '<option class="hide" value="">Select:</option>';

          foreach ($get_user as $key => $field) {
            $rows .= '<option value="' . $field->id . '">'. $field->name . '</option>';
          }

          return Response::json($rows);

    }
    public function indexExport()
    {
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::with('designations')
                                          ->find(Auth::user()->id);
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['settings'] = Settings::first();
        $this->data['title'] = "System Users";
        return view('summary-and-reports.export-daily-production-report.list', $this->data);
    }
  
  public function fix_cpd_total(){
      $row = CPDReportContent::get();
      foreach($row as $fix)
      {
        $fix->total_cpd_hours_final = $fix->total_cpd_hours_skills + $fix->total_cpd_hours_knowledge + $fix->total_cpd_hours_rules_and_regulations;
        $fix->save();
      }
      return 'DONE';
    }
  
    public function getReport($id){
        // prevent non-admin from viewing deactivated row
        $row = CPDReportContent::join('users','users.id','=','cpd_hours_reports.user_id')
                              ->leftJoin('sales_advisors','users.id', '=', 'sales_advisors.user_id')
                              ->leftJoin('sales_supervisors', 'sales_advisors.supervisor_id','=','sales_supervisors.id')
                              ->leftJoin('users as supervisors', 'sales_supervisors.user_id', '=',  'supervisors.id')
                              ->select('users.*','cpd_hours_reports.*','supervisors.name as supervisor')
                              ->where('report_id','=',$id)->get();

        $get_info =  CPDReport::where('id','=',$id)->first();
 
        $this->data['get_info'] = $get_info;
       
        $this->data['rows'] = $row;
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_rank'] = Sales::with('designations')
                                          ->find(Auth::user()->id);
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['title'] = "CPD REPORT - ".$get_info->name;
        $this->data['report_name'] = $get_info->name;
        $this->data['top_left_note'] = $get_info->left_note;
        $this->data['top_right_note'] = $get_info->right_note;
        $this->data['start'] = $get_info->start;
        $this->data['end'] = $get_info->end;

        return view('summary-and-reports.cpd-report.view',$this->data);
      
    }


public function exportExcel($id) {

  $name = 'CPDReport-' . Carbon::now()->format('d-m-Y');

  Excel::create($name, function($excel) use ($id)  {

    $excel->sheet('Sheet1', function($sheet) use ($id) {

      $rows = CPDReportContent::join('users','users.id','=','cpd_hours_reports.user_id')
                              ->leftJoin('sales_advisors','users.id', '=', 'sales_advisors.user_id')
                              ->leftJoin('sales_supervisors', 'sales_advisors.supervisor_id','=','sales_supervisors.id')
                              ->leftJoin('users as supervisors', 'sales_supervisors.user_id', '=',  'supervisors.id')
                              ->select('users.*','cpd_hours_reports.*','supervisors.name as supervisor')
                              ->where('report_id','=',$id)->get();   

      $get_info =  CPDReport::where('id','=',$id)->first();

      $this->data['get_info'] = $rows;
      $this->data['top_left_note'] = $get_info->left_note;
      $this->data['top_right_note'] = $get_info->right_note;

      // excel style
      $sheet->cells('A1:P1', function($cells) {
      $cells->setFontSize(13);
      $cells->setAlignment('center');
      $cells->setFontWeight('bold');
      });

      $sheet->setFontFamily('Comic Sans MS');

      $sheet->setStyle(array(
        'font' => [
                    'name'      =>  'Helvetica',
                    'size'      =>  10,
                  ]
      ));

      $sheet->setWidth('A', 48);
      $sheet->setWidth('B', 48);
      $sheet->setWidth('C', 17);
      $sheet->setWidth('D', 24);
      $sheet->setWidth('E', 18);
      $sheet->setWidth('F', 33);
      $sheet->setWidth('G', 33);
      $sheet->setWidth('H', 33);
      $sheet->setWidth('I', 36);
      $sheet->setWidth('J', 45);
      $sheet->setWidth('K', 40);
      $sheet->setWidth('L', 40);
      $sheet->setWidth('M', 40);
      $sheet->setWidth('N', 37);
      $sheet->setWidth('O', 37);

      //excel content
      $row_cell = 0;

      $sheet->appendRow(array(
      'AGENT NAME', 'REPORTING TO', 'AGENT CODE', 'NEW OR EXISTING', 'DATE JOINED', 'CPD PERIOD', 'CPD COURSE', 'CPD HOURS REQ (STRUCTURED)', 'CPD HOURS REQ (ETHICS)', 
      'CPD HOURS REQ (RULES & REGULATIONS)', 'TOTAL CPD HOURS (STRUCTURED)', 'TOTAL CPD HOURS (ETHICS)', 'TOTAL CPD HOURS(RULES & REGULATION)', 'TOTAL CPD HOURS (S,E & R)', 'SHORTFALL/ EXCESS HOURS'

      ));
      $row_cell += 1;

      foreach ($rows as $row) {
      $sheet->appendRow(array(
      $row->name,$row->supervisor ?:$row->name, $row->code, $row->cpd_status, date_format(date_create(substr($row->date_joined, 0,10)),'m/d/Y'),
      $row->cpd_period, $row->course, $row->cpd_hours_req_skills, $row->cpd_hours_req_knowledge,  $row->cpd_hours_req_rules_and_regulations, $row->total_cpd_hours_skills,
      $row->total_cpd_hours_knowledge, $row->total_cpd_hours_rules_and_regulations, $row->total_cpd_hours_final, $row->shortfall?:0
      )); 
      $row_cell += 1;
      }
      $sheet->appendRow(array(
      '', 
      ));
      $sheet->appendRow(array(
      '(*This document and the information in it are provided in confidence,For the sole purpose of LEGACY, And may not be disclosed to any third party or used for any other purpose without the express written permission of LEGACY.)'
      ));

      $sheet->cells('A1:O1', function($cells) {
      $cells->setFontSize(13);
      $cells->setFontColor('white');
      });

      $sheet->cells('A1:O' . $row_cell, function($cells) {
      $cells->setAlignment('center');
      });

    });

  })->download('xls'); 

  return view('summary-and-reports.cpd-report.view');
}

  public function exportPdf($id) {

    $rows = CPDReportContent::join('users','users.id','=','cpd_hours_reports.user_id')
                              ->leftJoin('sales_advisors','users.id', '=', 'sales_advisors.user_id')
                              ->leftJoin('sales_supervisors', 'sales_advisors.supervisor_id','=','sales_supervisors.id')
                              ->leftJoin('users as supervisors', 'sales_supervisors.user_id', '=',  'supervisors.id')
                              ->select('users.*','cpd_hours_reports.*','supervisors.name as supervisor')
                              ->where('report_id','=',$id)
                              ->get();

    $get_info =  CPDReport::where('id','=',$id)->first();

     $this->data['get_info'] = $rows;


    $this->data['permission'] = Permission::where(function($query) {
                                $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                      ->where('module_id', '=', '1');
                          })->first();
    $this->data['permission_payroll'] = Permission::where(function($query) {
                                $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                      ->where('module_id', '=', '2');
                          })->first();
    $this->data['permission_policy'] = Permission::where(function($query) {
                                $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                      ->where('module_id', '=', '3');
                          })->first();
    $this->data['permission_provider'] = Permission::where(function($query) {
                                $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                      ->where('module_id', '=', '4');
                          })->first();
    $this->data['permission_rank'] = Sales::with('designations')
                                      ->find(Auth::user()->id);
    $this->data['permission_reports'] = Permission::where(function($query) {
                                $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                      ->where('module_id', '=', '5');
                          })->first();

    $this->data['title'] = "CPD REPORT - ".$get_info->name;
    $this->data['report_name'] = $get_info->name;
    $this->data['top_left_note'] = $get_info->left_note;
    $this->data['top_right_note'] = $get_info->right_note;
    $this->data['start'] = $get_info->start;
    $this->data['end'] = $get_info->end;

    // return view('summary-and-reports.cpd-report.pdf',$this->data);
    $pdf = PDF::loadView('summary-and-reports.cpd-report.pdf',$this->data);
    return $pdf->setPaper('legal')->setOrientation('landscape')->download('download.pdf');
  }

    public function doListCPDReport(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
    
       $per = Request::input('per') ?: 10;
       
                   if (Request::input('page') != '»') {

                      Paginator::currentPageResolver(function () {
                          return Request::input('page'); 
                      });            
                                 
              $rows = CPDReport::select('id','name','start','end','left_note','right_note','status')
                              ->where('status','!=',2)
                              ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('left_note', 'LIKE', '%' . $search . '%')
                                        ->orWhere('right_note', 'LIKE', '%' . $search . '%');
                                })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
              //dd($result['sort']);
             } else {   

              $count = CPDReport::select('id','name','start','end','left_note','right_note','status')
                              ->where('status','!=',2)
                              ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('left_note', 'LIKE', '%' . $search . '%')
                                        ->orWhere('right_note', 'LIKE', '%' . $search . '%');
                                })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
                              

                       Paginator::currentPageResolver(function () use ($count) {
                       return ceil($count->total() / $per);
                       });

               $rows = CPDReport::select('id','name','start','end','left_note','right_note','status')
                              ->where('status','!=',2)
                              ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('left_note', 'LIKE', '%' . $search . '%')
                                        ->orWhere('right_note', 'LIKE', '%' . $search . '%');
                                })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
           }

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }

    }
    public function indexCPDReport(){
        $result = $this->doListCPDReport();
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_rank'] = Sales::with('designations')
                                          ->find(Auth::user()->id);
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();

        $this->data['agents'] = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 1)->get();

         // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        $this->data['settings'] = Settings::first();
        $this->data['noti_count'] = count($this->data['notification']);
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['refresh_route'] = url('reports/cpd/refresh');
        $this->data['title'] = "CPD Report";
       return view('summary-and-reports.cpd-report.list', $this->data);
    }

    public function viewCPDReport(){

      $id = Request::input('id');
      $agent = '';

      // prevent non-admin from viewing deactivated row
      $row = CPDReport::where('id','=',$id)->first();

      if($row) {
        $group_code = '';
        $agent = '';
        if ($row->type == "All") {

        } else if ($row->type == "Per Team") {
          $group = Group::find($row->group_id);
          if ($group) {
            $group_code = $group->code;
          }
        } else if ($row->type == "Per User") {
          $user = User::find($row->user_id);
          if ($user) {
            $agent = $user->name;
          }
        }

        $row = $row->toArray();
        $row['agent_name'] = $agent;
        $row['group_code'] = $group_code;

        return Response::json($row);
      } else {
        return Response::json(['error' => "Invalid row specified"]);
      }
    }

    public function deleteCPDReport(){

      $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select cpd report/s first.']); 
        } else {

            // check if auth user is selected
            // foreach ($ids as $id_check) {
            //     $row = User::find($id_check);
            //     if ($row->id == Auth::user()->id) {
            //         return Response::json(['error' => "Access denied."]);
            //     } 
            // }

            // process
          // /dd($ids);
           
            foreach ($ids as $id) {
                $row = CPDReport::find($id);
                //$row_user = User::where('user_id','='$row->user_id)->first();
                if(!is_null($row)) {
                    $status = $row->status;
                    // you cannot remove user that already been remove
                    if ($status != 2){
                      $row->status = 2;

                      $row->save();
                      //create log
                      //LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Removed']);
                    }
                    
                }
            }

            return Response::json(['body' => 'Selected users has been Removed.']);
        }
    }

    public function generateCPD() {
      $new = true;
      $input = Input::all();

      // check if an ID is passed
      if(array_get($input, 'id')) {
        // get the user info
        $row = CPDReport::find(array_get($input, 'id'));

        if(is_null($row)) {
            return Response::json(['error_prompt' => "The requested item was not found in the database."]);
        }
        // this is an existing row
        $new = false;
      }

      $rules = [
        'date_from' => 'required|date',
        'date_to' => 'required|date',
        'report_name' => 'required',
      ];

      if ($new) {
        $rules['report_type'] = 'required';
        if (array_get($input, 'report_type') && array_get($input, 'report_type') != "1") {
          $rules['per_name'] = 'required';
        }
      }

      // field name overrides
      $names = [

      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
        return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      $start_year = substr(array_get($input,'date_from'), 6, 4);
      $start_month = substr(array_get($input,'date_from'), 0, 2);
      $start_day = substr(array_get($input,'date_from'), 3, 2);
     
      $start = $start_year."-".$start_month."-".$start_day;
      $end_year = substr(array_get($input,'date_to'), 6, 4);
      $end_month = substr(array_get($input,'date_to'), 0, 2);
      $end_day = substr(array_get($input,'date_to'), 3, 2);
      $per_name = array_get($input,'per_name');
      $end = $end_year."-".$end_month."-".$end_day;

      if ($new) {
        $report_type = array_get($input,'report_type');
      } else {
        $report_type = '';
        if ($row->type == "All") {
          $report_type = 1;
        } else if ($row->type == "Per Team") {
          $report_type = 2;
        } else if ($row->type == "Per User") {
          $report_type = 3;
        }
      }

      if ($report_type == "1") {
        $get_users = User::where('usertype_id', 8)->where('status', 1)->lists('id');
        $row = CPDHours::where(function($query) use ($start, $end) {
                                $query->where('cpd_starting_date', '>=', $start . " 00:00:00")
                                      ->where('cpd_starting_date', '<=', $end . " 23:59:59")
                                      ->where('status', '!=', 2);                        
                              })
                              ->whereIn('user_id', $get_users)->get();

        $users_id = CPDHours::where(function($query) use ($start, $end) {
                                $query->where('cpd_starting_date', '>=', $start . " 00:00:00")
                                      ->where('cpd_starting_date', '<=', $end . " 23:59:59")
                                      ->where('status', '!=', 2);                        
                              })
                              ->whereIn('user_id', $get_users)->lists('user_id');
        $all_users = '';
        foreach ($users_id as $uikey => $uivalue) {
          $all_users .= $uivalue . ',';
        }

      } else if ($report_type == "2") {
        if ($new) {
          $group = array_get($input, 'per_name');
        } else {
          $group = $row->group_id;
        }

        $users = [];
        $group_users = '';
        $ctr = 0;

        if ($group) {
          $check_group = Group::find($group);

          if ($check_group) {

            $get_supervisors = SalesSupervisor::where('group_id', $check_group->id)->lists('user_id');

            foreach ($get_supervisors as $gskey => $gsvalue) {
              $users[$ctr] = $gsvalue;
              $ctr++;
              $group_users .= $gsvalue . ',';
            }

            $get_supervisor_ids = SalesSupervisor::where('group_id', $check_group->id)->lists('id');
            $get_advisors = SalesAdvisor::whereIn('supervisor_id', $get_supervisor_ids)->lists('user_id');

            foreach ($get_advisors as $gakey => $gavalue) {
              $users[$ctr] = $gavalue;
              $ctr++;
              $group_users .= $gavalue . ',';
            }
          }
        }

        $row = CPDHours::where(function($query) use ($start, $end) {
                                $query->where('cpd_starting_date', '>=', $start . " 00:00:00")
                                      ->where('cpd_starting_date', '<=', $end . " 23:59:59")
                                      ->where('status', '!=', 2);                        
                              })
                              ->whereIn('user_id', $users)->get();

      } else if($report_type == "3") {
        if ($new) {
          $user = array_get($input, 'per_name');
        } else {
          $user = $row->user_id;
        }

        $row = CPDHours::where(function($query) use ($start, $end) {
                                $query->where('cpd_starting_date', '>=', $start . " 00:00:00")
                                      ->where('cpd_starting_date', '<=', $end . " 23:59:59")
                                      ->where('status', '!=', 2);                        
                              })
                              ->where('user_id', $user)->get();
      } else {
        $users = array_unique(CPDReportContent::where('report_id', $row->id)->lists('user_id')->toArray());

        $row = CPDHours::where(function($query) use ($start, $end) {
                                $query->where('cpd_starting_date', '>=', $start . " 00:00:00")
                                      ->where('cpd_starting_date', '<=', $end . " 23:59:59")
                                      ->where('status', '!=', 9);                        
                              })
                              ->where('user_id', $users)->get();

        $all_users = '';
        foreach ($users as $ukey => $uvalue) {
          $all_users .= $uvalue . ',';
        }
      }

      if ($new) {
        if (count($row) > 0) {
          $row_headers = new CPDReport;
          $row_headers->name = array_get($input,'report_name');
          $row_headers->start = Carbon::createFromFormat('m/d/Y',array_get($input,'date_from'));
          $row_headers->end = Carbon::createFromFormat('m/d/Y',array_get($input,'date_to'));
          $row_headers->left_note = array_get($input,'left_note');
          $row_headers->right_note = array_get($input,'right_note');

          if (array_get($input,'report_type') == 1) {
            $row_headers->type = "All";
            $row_headers->all_users = rtrim($all_users, ",");
          } else if (array_get($input,'report_type') == 2) {
            $row_headers->group_users = rtrim($group_users, ",");
            $row_headers->group_id = array_get($input,'per_name');
            $row_headers->type = "Per Team";
          } else if (array_get($input,'report_type') == 3) {
            $row_headers->user_id = array_get($input,'per_name');
            $row_headers->type = "Per User";
          } else {
            return Response::json(['error_prompt' => "The requested item was not found in the database."]);
          }

          $row_headers->save();

          foreach ($row as $key => $field) {
            $new_row = new CPDReportContent;
            $new_row->report_id = $row_headers->id;
            $new_row->user_id = $field->user_id;
            $new_row->course = $field->course;
            $new_row->cpd_status = $field->cpd_status;
            $new_row->cpd_period = $field->cpd_period;
            $new_row->cpd_status = $field->cpd_status;
            $new_row->date_joined = $field->date_joined;
            $new_row->cpd_tracking_period = $field->cpd_tracking_period_from.'&nbsp;to&nbsp; '.$field->cpd_tracking_period_to;
            $new_row->cpd_hours_req_skills = $field->cpd_hours_req_skills;
            $new_row->cpd_hours_req_knowledge = $field->cpd_hours_req_knowledge;
            $new_row->cpd_hours_req_rules_and_regulations = $field->cpd_hours_req_rules_and_regulations;
            $new_row->total_cpd_hours_skills = $field->total_cpd_hours_skills;
            $new_row->total_cpd_hours_knowledge = $field->total_cpd_hours_knowledge;
            $new_row->total_cpd_hours_rules_and_regulations = $field->total_cpd_hours_rules_and_regulations;
            $new_row->total_cpd_hours_final = $field->total_cpd_hours_final;
            $new_row->shortfall = $field->shortfall;
            $new_row->save();
          }
        } else {
          return Response::json(['error_prompt' => 'No Record Found']);
        }
      } else {
        if (count($row) > 0) {
          $del_row = CPDReportContent::where('report_id','=',array_get($input,'id'));
          $del_row->delete();
          $row_headers = CPDReport::where('id','=',array_get($input,'id'))->first();
          $row_headers->name = array_get($input,'report_name');
          $row_headers->start = Carbon::createFromFormat('m/d/Y',array_get($input,'date_from'));
          $row_headers->end = Carbon::createFromFormat('m/d/Y',array_get($input,'date_to'));
          $row_headers->left_note = array_get($input,'left_note');
          $row_headers->right_note = array_get($input,'right_note');

          if ($row_headers->type == "All") {
            $row_headers->all_users = rtrim($all_users, ",");
          } else if ($row_headers->type == "Per Team") {
            $row_headers->group_users = rtrim($group_users, ",");
            $row_headers->group_id = array_get($input,'per_name');
          } else {
            $row_headers->all_users = rtrim($all_users, ",");
          }

          $row_headers->save();

          foreach ($row as $key => $field) {
            $new_row = new CPDReportContent;
            $new_row->report_id = $row_headers->id;
            $new_row->user_id = $field->user_id;
            $new_row->course = $field->course;
            $new_row->cpd_status = $field->cpd_status;
            $new_row->cpd_period = $field->cpd_period;
            $new_row->cpd_status = $field->cpd_status;
            $new_row->date_joined = $field->date_joined;
            $new_row->cpd_tracking_period = $field->cpd_tracking_period_from.'&nbsp;&nbsp;to&nbsp;'.$field->cpd_tracking_period_to;
            $new_row->cpd_hours_req_skills = $field->cpd_hours_req_skills;
            $new_row->cpd_hours_req_knowledge = $field->cpd_hours_req_knowledge;
            $new_row->cpd_hours_req_rules_and_regulations = $field->cpd_hours_req_rules_and_regulations;
            $new_row->total_cpd_hours_skills = $field->total_cpd_hours_skills;
            $new_row->total_cpd_hours_knowledge = $field->total_cpd_hours_knowledge;
            $new_row->total_cpd_hours_final = $field->total_cpd_hours_final;
            $new_row->total_cpd_hours_rules_and_regulations = $field->total_cpd_hours_rules_and_regulations;
            $new_row->shortfall = $field->shortfall;
            $new_row->save();
          }
        } else {

          $row_headers = CPDReport::where('id','=',array_get($input,'id'))->first();
          $row_headers->name = array_get($input,'report_name');
          $row_headers->start = Carbon::createFromFormat('m/d/Y',array_get($input,'date_from'));
          $row_headers->end = Carbon::createFromFormat('m/d/Y',array_get($input,'date_to'));
          $row_headers->left_note = array_get($input,'left_note');
          $row_headers->right_note = array_get($input,'right_note');

          if ($row_headers->type == "All") {
            $row_headers->all_users = rtrim($all_users, ",");
          } else if ($row_headers->type == "Per Team") {
            $row_headers->group_users = rtrim($group_users, ",");
            $row_headers->group_id = array_get($input,'per_name');
          } else {
            $row_headers->all_users = rtrim($all_users, ",");
          }

          $row_headers->save();

          return Response::json(['body' => 'Report Generated.']);
        }
      }

      return Response::json(['body' => 'Report Generated.']);
    }

    public function notification($id){  
 $row = NotificationPerUser::where(function($query) use ($id) {
                                $query->where('user_id', '=', Auth::user()->id)
                                      ->where('notification_id', '=', $id);

                            })->update(['is_read' => 1]);
return Redirect::to('/policy/production/case-submission');
   }
}