<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Validator;
use Carbon;
use Input;
use Auth;
use Excel;

use App\Permission;
use App\Settings;
use App\Product;
use App\ProductMain;
use App\ProductGross;
use App\ProviderCategory;
use App\ProviderClassification;
use App\ProviderProperty;
use App\LogProduct;
use App\ProductRate;
use App\ProductRider;
use App\Provider;
use App\UploadFeed;
use App\Policy;
use App\ProviderCodes;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paginator;
use App\User;
use App\Sales;
use App\SalesUser;
use App\Tier;
use App\Nationality;
use App\Notification;
use App\NotificationPerUser;
use Redirect;

class ProductController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    /**
     * Display a listing of the product.
     *
     * @return Response
     */
    public function index()
    {
      
        $result = $this->doList();
        $this->data['rows'] = $result['rows'];
        $this->data['paginate'] = $result['paginate'];
        $this->data['paginate_headers'] = $result['paginate_headers'];

        $this->data['pages'] = $result['pages'];
        $this->data['refresh_route'] = url('product/refresh');

        $this->data['providers'] = Provider::where('status', 1)->get();
        $this->data['title'] = "Product List";
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        $this->data['settings'] = Settings::first();
        $this->data['noti_count'] = count($this->data['notification']);

        return view('provider-product-management.product.list', $this->data);
    }

    public function doList() {

      // sort and order
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status') ?: 'Rider';
      
      if (Auth::user()->usertype_id == 8) {
        $status = 'Case';
      }
      $per = Request::input('per') ?: 100;

      $paginate = '';
      $paginate_headers = '';

      if (Request::input('page') != '»') {

        if ($status == "Main") {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          $rows = ProductMain::select('product_mains.*', 'providers.name as provider_name', 'products.code', 'product_riders.name as rider_name')
                                ->leftJoin('products', 'products.main_id', '=', 'product_mains.id')
                                ->leftJoin('product_riders', 'product_riders.main_id', '=', 'product_mains.id')
                                ->leftJoin('providers', 'product_mains.provider_id', '=', 'providers.id')
                                ->with('getRiders')
                                ->with('getCaseRiders')
                                  ->where(function($query) use ($search) {
                                      $query->where('product_mains.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('products.code', 'LIKE', '%' . $search . '%')
                                            ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('product_riders.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->where('product_mains.status', '!=', '2')
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('product_mains.id')
                                ->paginate($per);

          $paginate_headers = Request::input('headers');

          foreach ($rows as $key => $row) {
            $paginate .= '<tr data-id="' . $row->id . '" ' . ($row->status == 1 ? '' : 'class="tr-disabled"') . '>
              <td>';
            if (Auth::user()->usertype_id != 8) {
              $paginate .= '<a type="button" data-toggle="tooltip" data-type="product" title="Riders" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a> ';
            }
            $paginate .= '<a type="button" data-type="rider" title="Case Submission Riders" class="btn-table btn btn-xs btn-expand rider btn-default"><i class="fa fa-plus-square"></i></a> ' . $row->name . '</td>
              <td>' . $row->provider_name . '</td>';

              if (Auth::user()->usertype_id != 8) {
              $paginate .= '<td class="rightalign">
                <button type="button" class="btn btn-xs btn-table btn-add" data-toggle="tooltip" title="Add Rider" data-provider="' . $row->provider_id . '" data-id="' . $row->id . '">+ <i class="fa fa-line-chart"></i></button>
                <button type="button" class="btn btn-xs btn-table btn-add-case" data-toggle="tooltip" title="Add Case Submission Rider" data-provider="' . $row->provider_id . '" data-id="' . $row->id . '">+ <i class="fa fa-briefcase"></i></button>
                <button type="button" class="btn btn-xs btn-table btn-edit-main"><i class="fa fa-pencil"></i></button>';

                if (count($row->getRiders) < 1 && count($row->getCaseRiders) < 1) {
                  $paginate .= '<button type="button" class="btn btn-xs btn-table btn-disable" title="Delete"><i class="fa fa-trash"></i></button>';
                }
                $paginate .= '</td>';
              }
            $paginate .= '</tr>';
          }
        } else if ($status == "Rider") {
          
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          $rows = Product::select('products.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name', 'provider_classifications.name as class_name')
                                ->leftJoin('providers', 'products.provider_id', '=', 'providers.id')
                                ->leftJoin('product_mains', 'products.main_id', '=', 'product_mains.id')
                                ->leftJoin('provider_categories', 'products.category_id', '=', 'provider_categories.id')
                                ->leftJoin('provider_classifications', 'products.classification_id', '=', 'provider_classifications.id')
                                ->where('products.status', 1)
                                ->where(function($query) use ($search) {
                                    $query->where('product_name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('products.code', 'LIKE', '%' . $search . '%')
                                          ->orWhere('sequence_no', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_classifications.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_categories.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

          $paginate_headers = Request::input('headers');

          foreach ($rows as $key => $field) {
            $paginate .= '<tr data-id="' . $field->id . '">' .
                      '<td>' . $field->main_name . '</td>' .
                      '<td>' . $field->product_name . '</td>' .
                      '<td>' . $field->provider_name . '</td>' .
                      '<td>' . $field->code . '</td>' .
                      '<td>' . $field->cat_name . '</td>' .
                      '<td>' . $field->class_name . '</td>' .
                      (Auth::user()->usertype_id != 8 ? '<td class="rightalign">' .
                        '<button type="button" class="btn btn-xs btn-table btn-view" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-trash"></i></button>' .
                      '</td>' : '') .
                      '</tr>';
          }
        } else if ($status == "Case") {

          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          $rows = ProductRider::select('product_riders.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name')
                                ->leftJoin('product_mains', 'product_riders.main_id', '=', 'product_mains.id')
                                ->leftJoin('provider_categories', 'product_riders.category_id', '=', 'provider_categories.id')
                                ->leftJoin('providers', 'product_riders.provider_id', '=', 'providers.id')
                                ->where('product_riders.status', 1)
                                ->where(function($query) use ($search) {
                                    $query->where('product_riders.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_categories.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);


          $paginate_headers = Request::input('headers');

          foreach ($rows as $key => $field) {
            $paginate .= '<tr data-id="' . $field->id . '">' .
                      '<td>' . $field->main_name . '</td>' .
                      '<td>' . $field->name . '</td>' .
                      '<td>' . $field->provider_name . '</td>' .
                      '<td>' . $field->cat_name . '</td>' .
                      (Auth::user()->usertype_id != 8 ?  '<td class="rightalign">' .
                        '<button type="button" class="btn btn-xs btn-table btn-edit-case" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete-case" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-trash"></i></button>' .
                      '</td>' : '')
                      .
                      '</tr>';
          }
        }
      } else {

        if ($status == "Main") {
          $count = ProductMain::select('product_mains.*', 'providers.name as provider_name', 'products.code', 'product_riders.name as rider_name')
                                  ->leftJoin('products', 'products.main_id', '=', 'product_mains.id')
                                  ->leftJoin('product_riders', 'product_riders.main_id', '=', 'product_mains.id')
                                  ->leftJoin('providers', 'product_mains.provider_id', '=', 'providers.id')
                                  ->with('getRiders')
                                  ->with('getCaseRiders')
                                  ->where(function($query) use ($search) {
                                      $query->where('product_mains.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('products.code', 'LIKE', '%' . $search . '%')
                                            ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('product_riders.name', 'LIKE', '%' . $search . '%');
                                  })
                                  ->where('product_mains.status', '!=', '2')
                                  ->orderBy('provider_name', 'asc')
                                  ->orderBy($result['sort'], $result['order'])
                                  ->groupBy('product_mains.id')
                                  ->paginate($per);
        } else if ($status == "Rider") {
          $count = Product::select('products.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name', 'provider_classifications.name as class_name')
                                ->leftJoin('providers', 'products.provider_id', '=', 'providers.id')
                                ->leftJoin('product_mains', 'products.main_id', '=', 'product_mains.id')
                                ->leftJoin('provider_categories', 'products.category_id', '=', 'provider_categories.id')
                                ->leftJoin('provider_classifications', 'products.classification_id', '=', 'provider_classifications.id')
                                ->where('products.status', 1)
                                ->where(function($query) use ($search) {
                                    $query->where('product_name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('products.code', 'LIKE', '%' . $search . '%')
                                          ->orWhere('sequence_no', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_classifications.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_categories.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        } else if ($status == "Case") {
          $count = ProductRider::select('product_riders.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name')
                                ->leftJoin('product_mains', 'product_riders.main_id', '=', 'product_mains.id')
                                ->leftJoin('provider_categories', 'product_riders.category_id', '=', 'provider_categories.id')
                                ->leftJoin('providers', 'product_riders.provider_id', '=', 'providers.id')
                                ->where('product_riders.status', 1)
                                ->where(function($query) use ($search) {
                                    $query->where('product_riders.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_categories.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        }

        if ($status == "Main") {

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          $rows = ProductMain::select('product_mains.*', 'providers.name as provider_name', 'products.code', 'product_riders.name as rider_name')
                                ->leftJoin('products', 'products.main_id', '=', 'product_mains.id')
                                ->leftJoin('product_riders', 'product_riders.main_id', '=', 'product_mains.id')
                                ->leftJoin('providers', 'product_mains.provider_id', '=', 'providers.id')
                                ->with('getRiders')
                                ->with('getCaseRiders')
                                  ->where(function($query) use ($search) {
                                      $query->where('product_mains.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('products.code', 'LIKE', '%' . $search . '%')
                                            ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('product_riders.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->where('product_mains.status', '!=', '2')
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('product_mains.id')
                                ->paginate($per);

          $paginate_headers = Request::input('headers');

          foreach ($rows as $key => $row) {
            $paginate .= '<tr data-id="' . $row->id . '" ' . ($row->status == 1 ? '' : 'class="tr-disabled"') . '>
              <td>';
            if (Auth::user()->usertype_id != 8) {
              $paginate .= '<a type="button" data-toggle="tooltip" data-type="product" title="Riders" class="btn-table btn btn-xs btn-expand btn-default"><i class="fa fa-plus"></i></a> ';
            }
            $paginate .= '<a type="button" data-type="rider" title="Case Submission Riders" class="btn-table btn btn-xs btn-expand rider btn-default"><i class="fa fa-plus-square"></i></a> ' . $row->name . '</td>
              <td>' . $row->provider_name . '</td>';

              if (Auth::user()->usertype_id != 8) {
              $paginate .= '<td class="rightalign">
                <button type="button" class="btn btn-xs btn-table btn-add" data-toggle="tooltip" title="Add Rider" data-provider="' . $row->provider_id . '" data-id="' . $row->id . '">+ <i class="fa fa-line-chart"></i></button>
                <button type="button" class="btn btn-xs btn-table btn-add-case" data-toggle="tooltip" title="Add Case Submission Rider" data-provider="' . $row->provider_id . '" data-id="' . $row->id . '">+ <i class="fa fa-briefcase"></i></button>
                <button type="button" class="btn btn-xs btn-table btn-edit-main"><i class="fa fa-pencil"></i></button>';

                if (count($row->getRiders) < 1 && count($row->getCaseRiders) < 1) {
                  $paginate .= '<button type="button" class="btn btn-xs btn-table btn-disable" title="Delete"><i class="fa fa-trash"></i></button>';
                }
                $paginate .= '</td>';
              }
            $paginate .= '</tr>';
          }
        } else if ($status == "Rider") {

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          $rows = Product::select('products.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name', 'provider_classifications.name as class_name')
                                ->leftJoin('providers', 'products.provider_id', '=', 'providers.id')
                                ->leftJoin('product_mains', 'products.main_id', '=', 'product_mains.id')
                                ->leftJoin('provider_categories', 'products.category_id', '=', 'provider_categories.id')
                                ->leftJoin('provider_classifications', 'products.classification_id', '=', 'provider_classifications.id')
                                ->where('products.status', 1)
                                ->where(function($query) use ($search) {
                                    $query->where('product_name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('products.code', 'LIKE', '%' . $search . '%')
                                          ->orWhere('sequence_no', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_classifications.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_categories.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

          $paginate_headers = Request::input('headers');

          foreach ($rows as $key => $field) {
            $paginate .= '<tr data-id="' . $field->id . '">' .
                      '<td>' . $field->main_name . '</td>' .
                      '<td>' . $field->product_name . '</td>' .
                      '<td>' . $field->provider_name . '</td>' .
                      '<td>' . $field->code . '</td>' .
                      '<td>' . $field->cat_name . '</td>' .
                      '<td>' . $field->class_name . '</td>' .
                      (Auth::user()->usertype_id != 8 ? '<td class="rightalign">' .
                        '<button type="button" class="btn btn-xs btn-table btn-view" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-trash"></i></button>' .
                      '</td>' : '') .
                      '</tr>';
          }
        } else if ($status == "Case") {

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          $rows = ProductRider::select('product_riders.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name')
                                ->leftJoin('product_mains', 'product_riders.main_id', '=', 'product_mains.id')
                                ->leftJoin('provider_categories', 'product_riders.category_id', '=', 'provider_categories.id')
                                ->leftJoin('providers', 'product_riders.provider_id', '=', 'providers.id')
                                ->where('product_riders.status', 1)
                                ->where(function($query) use ($search) {
                                    $query->where('product_riders.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('provider_categories.name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy('provider_name', 'asc')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);


          $paginate_headers = Request::input('headers');

          foreach ($rows as $key => $field) {
            $paginate .= '<tr data-id="' . $field->id . '">' .
                      '<td>' . $field->main_name . '</td>' .
                      '<td>' . $field->name . '</td>' .
                      '<td>' . $field->provider_name . '</td>' .
                      '<td>' . $field->cat_name . '</td>' .
                      (Auth::user()->usertype_id != 8 ?  '<td class="rightalign">' .
                        '<button type="button" class="btn btn-xs btn-table btn-edit-case" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete-case" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-trash"></i></button>' .
                      '</td>' : '')
                      .
                      '</tr>';
          }
        }
      }

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            $result['paginate'] = $paginate;
            $result['paginate_headers'] = $paginate_headers;
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            $result['paginate'] = $paginate;
            $result['paginate_headers'] = $paginate_headers;
            return $result;
        }
    }
  
    public function exportRiders() {

      $name = 'CASE-SUBMISSION-' . Carbon::now()->format('Y-m-d');

      Excel::create($name, function($excel) use ($name) {
      
      $rows = ProductRider::select('product_riders.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name')
                            ->with('getProvider')
                            ->with('getCategory')
                                ->leftJoin('product_mains', 'product_riders.main_id', '=', 'product_mains.id')
                                ->leftJoin('provider_categories', 'product_riders.category_id', '=', 'provider_categories.id')
                                ->leftJoin('providers', 'product_riders.provider_id', '=', 'providers.id')
                                ->where('product_riders.status', 1)->orderBy('provider_name','asc')->orderBy('main_name','asc')->get();

        $excel->sheet($name, function($sheet) use ($rows)
        {

            $row_cell = 1;
            $sheet->appendRow(array(
              'MAIN PRODUCT',
              'RIDERS',
              'PROVIDER',
              'PRODUCT CATEGORY',
            ));
            $sheet->cell('A' . $row_cell . ':D' . $row_cell, function($cells) {
              $cells->setFont(array(
                  'bold' => true,
              ));
              $cells->setAlignment('center');
              $cells->setValignment('middle');
            });
            foreach ($rows as $key => $row) {
              $row_cell += 1;
              $sheet->appendRow(array(
                $row->main_name,
                $row->name,
                $row->provider_name,
                $row->cat_name
              ));
            }
        });

      })->download('xls');
    }
    

    public function exportProduct() {


      $name = 'PRODUCTS-' . Carbon::now()->format('Y-m-d');

      Excel::create($name, function($excel) use ($name) {

      $rows = Product::select('products.*', 'product_mains.name as main_name', 'providers.name as provider_name', 'provider_categories.name as cat_name', 'provider_classifications.name as class_name')
                            ->with('getGross1')
                            ->with('getGross2')
                            ->with('getGross3')
                            ->with('getGross4')
                            ->with('getGross5')
                            ->with('getGross6')
                            ->leftJoin('providers', 'products.provider_id', '=', 'providers.id')
                            ->leftJoin('product_mains', 'products.main_id', '=', 'product_mains.id')
                            ->leftJoin('provider_categories', 'products.category_id', '=', 'provider_categories.id')
                            ->leftJoin('provider_classifications', 'products.classification_id', '=', 'provider_classifications.id')
                            ->where('products.status', 1)
                            ->orderBy('provider_name', 'asc')
                            ->orderBy('products.product_name', 'asc')
                            ->get();

                            // dd($rows);
        $excel->sheet($name, function($sheet) use ($rows) {

            $row_cell = 1;

            $sheet->appendRow(array(
              'PRODUCT NO',
              'PROVIDER',
              'CATEGORY',
              'PRODUCT NAME',
              'CODE',
              'REMARKS',
              'MAIN PRODUCT',
              'TERM',
              'END TERM',
              'GROSS_REV_YEAR1',
              'GROSS_REV_YEAR2',
              'GROSS_REV_YEAR3',
              'GROSS_REV_YEAR4',
              'GROSS_REV_YEAR5',
              'GROSS_REV_YEAR6',
              'CONVERSION RATE',
              'RPBC_Y1',
              'RPBC_Y2',
              'RPBC_Y3',
              'RPBC_Y4',
              'RPBC_Y5',
              'RPBC_Y6',
              'RPBC_Y99',
              'SPBC',
              'RPOR_Y1',
              'RPOR_Y2',
              'RPOR_Y3',
              'RPOR_Y4',
              'RPOR_Y5',
              'RPOR_Y6',
              'RPOR_Y99',
              'SPOR',
              'CREATED AT',
              'UPDATED AT',
            ));

            $sheet->cell('A' . $row_cell . ':AH' . $row_cell, function($cells) {
              $cells->setFont(array(
                  'bold' => true,
              ));
              $cells->setAlignment('center');
              $cells->setValignment('middle');
            });


            foreach ($rows as $key => $row) {
              
              $row_cell += 1;

              $grossrevyear1 = '';
              if ($row->getGross1) {
                $grossrevyear1 = $row->getGross1->value;
              }

              $grossrevyear2 = '';
              if ($row->getGross2) {
                $grossrevyear2 = $row->getGross2->value;
              }

              $grossrevyear3 = '';
              if ($row->getGross3) {
                $grossrevyear3 = $row->getGross3->value;
              }

              $grossrevyear4 = '';
              if ($row->getGross4) {
                $grossrevyear4 = $row->getGross4->value;
              }

              $grossrevyear5 = '';
              if ($row->getGross5) {
                $grossrevyear5 = $row->getGross5->value;
              }

              $grossrevyear6 = '';
              if ($row->getGross6) {
                $grossrevyear6 = $row->getGross6->value;
              }

              $sheet->appendRow(array(
                $row->sequence_no,
                $row->provider_name,
                $row->cat_name,
                $row->product_name,
                $row->code,
                $row->remarks,
                $row->main_name,
                $row->term,
                $row->endterm,
                $grossrevyear1,
                $grossrevyear2,
                $grossrevyear3,
                $grossrevyear4,
                $grossrevyear5,
                $grossrevyear6,
                $row->conversion_rate,
                $row->rpbc_y1,
                $row->rpbc_y2,
                $row->rpbc_y3,
                $row->rpbc_y4,
                $row->rpbc_y5,
                $row->rpbc_y6,
                $row->rpbc_y99,
                $row->spbc,
                $row->rpor_y1,
                $row->rpor_y2,
                $row->rpor_y3,
                $row->rpor_y4,
                $row->rpor_y5,
                $row->rpor_y6,
                $row->rpor_y99,
                $row->spor,
                $row->created_at,
                $row->updated_at,
              ));

              $sheet->cell('A' . $row_cell . ':G' . $row_cell, function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            }

        });

      })->download('xls');
    }

    public function view() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Product::with('getGross')->with('getProvider')->with('getProvider.getProviderCategory')->with('getProvider.getProviderClassification')->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function caseView() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = ProductRider::with('getProvider')->with('getProvider.getProviderCategory')->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function mainView() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = ProductMain::find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function indexLog()
    {   
      $result = $this->doListLog();

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];

      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                              ->select('sales.*','designations.*')
                                              ->where('sales.user_id','=',Auth::user()->id)
                                              ->first();
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
      if ( Auth::user()->usertype_id == '8') { //if sales user
       $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                    ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                      ->select('users.*','notifications_per_users.*','notifications.*')
                                    ->where(function($query) {
                                          $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                ->where('notifications.usertype_id', '=', '1')
                                                ->where('notifications_per_users.is_read', '=', '0');                             
                                      })->get();
   
      }
      else
      {
           $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                    ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                      ->select('users.*','notifications_per_users.*','notifications.*')
                                    ->where(function($query) {
                                          $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                ->where('notifications.usertype_id', '=', '2')
                                                ->where('notifications_per_users.is_read', '=', '0');       
                                      })->get();
      }   
      $this->data['settings'] = Settings::first();
      $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Product Log";
      return view('provider-product-management.product.log.list', $this->data);
    }

    /**
     * Display a listing of the system users.
     *
     * @return Response
     */
    public function doListLog() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'log_products.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = LogProduct::join('users', 'users.id', '=', 'log_products.edit_id')
                        ->join('products', 'products.id', '=', 'log_products.product_id')
                        ->select('log_products.activity', 'log_products.created_at', 'products.sequence_no', 'products.code', 'users.name')
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

        // dd($rows);
      } else {
        $count = UploadFeed::paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = LogProduct::join('users', 'users.id', '=', 'log_products.edit_id')
                        ->join('products', 'products.id', '=', 'log_products.product_id')
                        ->select('log_products.activity', 'log_products.created_at', 'products.sequence_no', 'products.code', 'users.name')
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      }


      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;

          return $result;
      }
    }
   
    public function mainSave() {

        $new = true;

        $input = Input::all();
        // dd($input);
        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = ProductMain::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
        
        $rules = [
          'name' => 'required',
          'provider_id' => 'required',
          'conversion_rate' => 'required',
        ];

        if (array_get($input, 'conversion')) {
          $rules['premium_paying_term'] = 'required|numeric';
          $rules['maturity_term'] = 'required|numeric';
        }

        // field name overrides
        $names = [

        ];

        $messages = [
          'provider_id.required' => 'error-provider_id|*Select value',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules, $messages);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
          $row = new ProductMain;
        }

        if (array_get($input, 'conversion_5') &&  !array_get($input, 'conversion_8')) {
          $row->conversion_year = '5';
          $conversion_year = '5';
          $row->conversion_rate = 1;
        } elseif (array_get($input, 'conversion_8') &&  !array_get($input, 'conversion_5')) {
          $row->conversion_year = '8';
          $conversion_year = '8';
          $row->conversion_rate = 1;
        } elseif (array_get($input, 'conversion_8') &&  array_get($input, 'conversion_5')) {
          $row->conversion_year = '5and8';
          $conversion_year = '5and8';
          $row->conversion_rate = 1;
        } elseif (!array_get($input, 'conversion_8') &&  !array_get($input, 'conversion_5')) {
          $row->conversion_year = 'none';
          $conversion_year = 'none';
          $row->conversion_rate = array_get($input, 'conversion_rate');
        }

        $row->name = array_get($input, 'name');
        $row->provider_id = array_get($input, 'provider_id');
        $row->premium_paying_term = array_get($input, 'premium_paying_term') ?: 0;
        $row->maturity_term = array_get($input, 'maturity_term') ?: 0;
        $row->conversion_rate = array_get($input, 'conversion_rate');

        if (strlen(strval(array_get($input, 'conversion'))) > 0) {
          $row->conversion = 1;
        } else {
          $row->conversion = 0;
        }

        $row->save();

        if ($new) {
          return Response::json(['body' => 'Main product successfully created.']);
        } else {
          return Response::json(['body' => 'Main product successfully updated.']);
        }

    }

    /**
     * Save model
     *
     */
    public function save() {

        $new = true;

        $input = Input::all();
        
        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Product::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
          'provider_id' => 'required',
          'product_name' => 'required',
          // 'sequence_no' => 'required',
          'category_id' => 'required',
          'code' => 'required',
          'classification_id' => 'required',
          'start' => 'date|date_format:"m/d/Y"|before:end',
          'end' => 'date|date_format:"m/d/Y"|after:start',
          // 'renewal' => 'required',
          'details' => 'required',
          'remarks' => 'required',
          'term' => 'numeric|required',
          'endterm' => 'numeric|required',
          'conversion_rate' => 'numeric|required',
          'rpbc_y1' => 'numeric',
          'rpbc_y2' => 'numeric',
          'rpbc_y3' => 'numeric',
          'rpbc_y4' => 'numeric',
          'rpbc_y5' => 'numeric',
          'rpbc_y6' => 'numeric',
          'rpbc_y99' => 'numeric',
          'spbc' => 'numeric',
          'rpor_y1' => 'numeric',
          'rpor_y2' => 'numeric',
          'rpor_y3' => 'numeric',
          'rpor_y4' => 'numeric',
          'rpor_y5' => 'numeric',
          'rpor_y6' => 'numeric',
          'rpor_y99' => 'numeric',
          'spor' => 'numeric',
        ];


        if (array_get($input, 'start') || array_get($input, "end")) {
          $rules['start'] = 'required|date|date_format:"m/d/Y"|before:end';
          $rules['end'] = 'required|date|date_format:"m/d/Y"|after:start';
        }

        // field name overrides
        $names = [

        ];

        $messages = [
            'provider_id.required' => 'error-provider_id|*Select value',
            'category_id.required' => 'error-category_id|*Select value',
            'classification_id.required' => 'error-classification_id|*Select value',
            'classification.required' => 'error-classification|*Select value',
            'conversion_rate.numeric' => 'error-conversion_rate|*Numeric value',
            'term.numeric' => 'error-term|*Numeric value',
            'endterm.numeric' => 'error-endterm|*Numeric value',
            'rpbc_y1.numeric' => 'error-rpbc_y1|*Numeric value',
            'rpbc_y2.numeric' => 'error-rpbc_y2|*Numeric value',
            'rpbc_y3.numeric' => 'error-rpbc_y3|*Numeric value',
            'rpbc_y4.numeric' => 'error-rpbc_y4|*Numeric value',
            'rpbc_y5.numeric' => 'error-rpbc_y5|*Numeric value',
            'rpbc_y6.numeric' => 'error-rpbc_y6|*Numeric value',
            'rpbc_y99.numeric' => 'error-rpbc_y99|*Numeric value',
            'spbc.numeric' => 'error-spbc|*Numeric value',
            'rpor_y1.numeric' => 'error-rpor_y1|*Numeric value',
            'rpor_y2.numeric' => 'error-rpor_y2|*Numeric value',
            'rpor_y3.numeric' => 'error-rpor_y3|*Numeric value',
            'rpor_y4.numeric' => 'error-rpor_y4|*Numeric value',
            'rpor_y5.numeric' => 'error-rpor_y5|*Numeric value',
            'rpor_y6.numeric' => 'error-rpor_y6|*Numeric value',
            'rpor_y99.numeric' => 'error-rpor_y99|*Numeric value',
            'spor.numeric' => 'error-spor|*Numeric value',
            'start.before' => 'error-start|*The effective start date must be a date before effective end date',
            'end.after' => 'error-end|*The effective end date must be a date after effective start date',
        ];

        $gross = array();
        $gross_id = Input::get('gross_id');
        $gross_year_id = Input::get('gross_year_id');
        $gross_value = Input::get('gross_value');

        // validate child rows
        if($gross_value) {
            foreach($gross_value as $key => $val) {
                if($gross_value[$key]) {
                    $subrules = array(
                      "gross_value.$key" => 'required|numeric',
                    );

                    $subfn = array(
                      "gross_value.$key" => 'gross_rev_value',
                    );

                    $submsg = array(
                      "gross_value.$key.numeric" => 'error-gross_value_' . $gross_year_id[$key] . '|*Numeric value',
                    );

                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $messages = array_merge($messages, $submsg);
                    $gross[] = $gross_value[$key];
                } else {
                    unset($gross_id[$key]);
                    unset($gross_year_id[$key]);
                    unset($gross_value[$key]);
                }
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules, $messages);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $log_provider_id            = "";
        $log_product_name           = "";
        $log_sequence_no            = "";
        $log_classification_id      = "";
        $log_code                   = "";
        $log_category_id            = "";
        $log_description            = "";
        $log_start                  = "";
        $log_end                    = "";
        $log_classification         = "";
        $log_renewal                = "";
        $log_details                = "";
        $log_remarks                = "";
        $log_term                   = "";
        $log_endterm                = "";
        $log_conversion_rate        = "";
        $log_rpbc_y1                = "";
        $log_rpbc_y2                = "";
        $log_rpbc_y3                = "";
        $log_rpbc_y4                = "";
        $log_rpbc_y5                = "";
        $log_rpbc_y6                = "";
        $log_rpbc_y99               = "";
        $log_spbc                   = "";
        $log_rpor_y1                = "";
        $log_rpor_y2                = "";
        $log_rpor_y3                = "";
        $log_rpor_y4                = "";
        $log_rpor_y5                = "";
        $log_rpor_y6                = "";
        $log_rpor_y99               = "";
        $log_spor                   = "";
        $conversion_year            = "";

        // create model if new
        if($new) {
            $row = new Product;
        } else {
          $log_provider_id            = $row->provider_id;           
          $log_product_name           = $row->product_name;
          $log_sequence_no            = $row->sequence_no;
          $log_classification_id      = $row->classification_id;
          $log_code                   = $row->code;
          $log_category_id            = $row->category_id;
          $log_description            = $row->description;
          $log_start = null;

          if ($row->start) {
            $log_start                  = date_format(date_create(substr($row->start, 0,10)),'m/d/Y');
          }

          $log_end = null;

          if ($row->end) {
            $log_end                  = date_format(date_create(substr($row->end, 0,10)),'m/d/Y');
          }
          // $log_end                    = date_format(date_create(substr($row->end, 0,10)),'m/d/Y');

          $log_classification         = $row->classification;
          $log_renewal                = $row->renewal;
          $log_details                = $row->details;
          $log_remarks                = $row->remarks;
          $log_term                   = $row->term;
          $log_endterm                = $row->endterm;
          $log_conversion_rate        = $row->conversion_rate;
          $log_conversion_year        = $row->conversion_year;
          $log_rpbc_y1                = $row->rpbc_y1;
          $log_rpbc_y2                = $row->rpbc_y2;
          $log_rpbc_y3                = $row->rpbc_y3;
          $log_rpbc_y4                = $row->rpbc_y4;
          $log_rpbc_y5                = $row->rpbc_y5;
          $log_rpbc_y6                = $row->rpbc_y6;
          $log_rpbc_y99               = $row->rpbc_y99;
          $log_spbc                   = $row->spbc;
          $log_rpor_y1                = $row->rpor_y1;
          $log_rpor_y2                = $row->rpor_y2;
          $log_rpor_y3                = $row->rpor_y3;
          $log_rpor_y4                = $row->rpor_y4;
          $log_rpor_y5                = $row->rpor_y5;
          $log_rpor_y6                = $row->rpor_y6;
          $log_rpor_y99               = $row->rpor_y99;
          $log_spor                   = $row->spor;
        }
          // save model
          $row->provider_id           = array_get($input, 'provider_id');
          $row->product_name          = array_get($input, 'product_name');
          $row->sequence_no           = array_get($input, 'sequence_no');
          $row->classification_id     = array_get($input, 'classification_id');
          $row->code                  = array_get($input, 'code');
          $row->category_id           = array_get($input, 'category_id');
          $row->main_id               = array_get($input, 'main_id');

          if (array_get($input, 'start')) {
            $row->start                 = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'start'). ' 00:00 AM');
          } else {
            $row->start = null;
          }

          if (array_get($input, 'end')) {
            $row->end                   = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'end'). ' 00:00 AM');
          } else {
            $row->end = null;
          }
          
          $row->endterm               = array_get($input, 'endterm');
          $row->renewal               = array_get($input, 'renewal');
          $row->details               = array_get($input, 'details');
          $row->remarks               = array_get($input, 'remarks');
          $row->term                  = array_get($input, 'term');
          $row->rpbc_y1               = array_get($input, 'rpbc_y1');
          $row->rpbc_y2               = array_get($input, 'rpbc_y2');
          $row->rpbc_y3               = array_get($input, 'rpbc_y3');
          $row->rpbc_y4               = array_get($input, 'rpbc_y4');
          $row->rpbc_y5               = array_get($input, 'rpbc_y5');
          $row->rpbc_y6               = array_get($input, 'rpbc_y6');
          $row->rpbc_y99              = array_get($input, 'rpbc_y99');
          $row->spbc                  = array_get($input, 'spbc');
          $row->rpor_y1               = array_get($input, 'rpor_y1');
          $row->rpor_y2               = array_get($input, 'rpor_y2');
          $row->rpor_y3               = array_get($input, 'rpor_y3');
          $row->rpor_y4               = array_get($input, 'rpor_y4');
          $row->rpor_y5               = array_get($input, 'rpor_y5');
          $row->rpor_y6               = array_get($input, 'rpor_y6');
          $row->rpor_y99              = array_get($input, 'rpbc_y99');
          $row->spor                  = array_get($input, 'spor');

          if (array_get($input, 'conversion_5') &&  !array_get($input, 'conversion_8')) {
            $row->conversion_year = '5';
            $conversion_year = '5';
            $row->conversion_rate = 1;
          } elseif (array_get($input, 'conversion_8') &&  !array_get($input, 'conversion_5')) {
            $row->conversion_year = '8';
            $conversion_year = '8';
            $row->conversion_rate = 1;
          } elseif (array_get($input, 'conversion_8') &&  array_get($input, 'conversion_5')) {
            $row->conversion_year = '5and8';
            $conversion_year = '5and8';
            $row->conversion_rate = 1;
          } elseif (!array_get($input, 'conversion_8') &&  !array_get($input, 'conversion_5')) {
            $row->conversion_year = 'none';
            $conversion_year = 'none';
            $row->conversion_rate = array_get($input, 'conversion_rate');
          }

          $row->save();
              
          // create model if new
          if($new) {
            $row->sequence_no = 'PROD-' . sprintf("%06d", $row->id);
            $row->save();
          }

        if(isset($row)) {
          $del = ProductGross::where('product_id', $row->id);
                               
          if(!empty($gross_id)) {
              $del->whereNotIn('id', $gross_id);
          }

          $drows = $del->delete();
        }
        
        // save rows
        if($gross_value) {
            foreach($gross_value as $key => $child) {
                if($gross_id[$key]) {
                    // this is an existing visa
                    $ginfo = ProductGross::find($gross_id[$key]);
                } else {
                    // this is a new visa
                    $ginfo = new ProductGross;
                    $ginfo->product_id = $row->id;
                }

                $ginfo->value = $gross_value[$key];
                $ginfo->year_id = $gross_year_id[$key];
                $ginfo->save();
            }
        }


          $updated = false;
        if ($new) {
          //create log
          LogProduct::create(['product_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Product Added']);
        } else {
          //create log
          if($log_provider_id != array_get($input, 'provider_id')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Provider Changed']);
              $updated = true;
            }
          if($log_product_name != array_get($input, 'product_name')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Product Name Changed']);
              $updated = true;
            }
          if($log_sequence_no != array_get($input, 'sequence_no')) {
              LogProduct::create(['sequence_no' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Sequence No. Changed']);
              $updated = true;
            }
          if($log_classification_id != array_get($input, 'classification_id')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Product Classification Changed']);
              $updated = true;
            }
          if($log_code != array_get($input, 'code')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Code Changed']);
              $updated = true;
            }
          if($log_category_id != array_get($input, 'category_id')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Product Category Changed']);
              $updated = true;
            }
          if($log_description != array_get($input, 'description')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Product Description Changed']);
              $updated = true;
            }
          if($log_start != array_get($input, 'start')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Effective Start Changed']);
              $updated = true;
            }
          if($log_end != array_get($input, 'end')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Effective End Changed']);
              $updated = true;
            }
          if($log_classification != array_get($input, 'classification')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Classification Changed']);
              $updated = true;
            }
          if($log_renewal != array_get($input, 'renewal')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Renewal Changed']);
              $updated = true;
            }
          if($log_details != array_get($input, 'details')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Details Changed']);
              $updated = true;
            }
          if($log_remarks != array_get($input, 'remarks')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Remarks Changed']);
              $updated = true;
            }
          if($log_term != array_get($input, 'term')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Term Changed']);
              $updated = true;
            }
          if($log_endterm != array_get($input, 'endterm')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'End Term Changed']);
              $updated = true;
            }
          if($log_conversion_rate != array_get($input, 'conversion_rate')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Conversion Rate Changed']);
              $updated = true;
            }
          if($log_conversion_year != $conversion_year) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Conversion Year Changed']);
              $updated = true;
            }
          if($log_rpbc_y1 != array_get($input, 'rpbc_y1')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPBC YEAR 1 Changed']);
              $updated = true;
            }
          if($log_rpbc_y2 != array_get($input, 'rpbc_y2')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPBC YEAR 2 Changed']);
              $updated = true;
            }
          if($log_rpbc_y3 != array_get($input, 'rpbc_y3')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPBC YEAR 3 Changed']);
              $updated = true;
            }
          if($log_rpbc_y4 != array_get($input, 'rpbc_y4')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPBC YEAR 4 Changed']);
              $updated = true;
            }
          if($log_rpbc_y5 != array_get($input, 'rpbc_y5')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPBC YEAR 5 Changed']);
              $updated = true;
            }
          if($log_rpbc_y6 != array_get($input, 'rpbc_y6')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPBC YEAR 6 Changed']);
              $updated = true;
            }
          if($log_rpbc_y99 != array_get($input, 'rpbc_y99')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPBC YEAR 99 Changed']);
              $updated = true;
            }
          if($log_spbc != array_get($input, 'spbc')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'SPBC Changed']);
              $updated = true;
            }
          if($log_rpor_y1 != array_get($input, 'rpor_y1')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPOR YEAR 1 Changed']);
              $updated = true;
            }
          if($log_rpor_y2 != array_get($input, 'rpor_y2')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPOR YEAR 2 Changed']);
              $updated = true;
            }
          if($log_rpor_y3 != array_get($input, 'rpor_y3')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPOR YEAR 3 Changed']);
              $updated = true;
            }
          if($log_rpor_y4 != array_get($input, 'rpor_y4')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPOR YEAR 4 Changed']);
              $updated = true;
            }
          if($log_rpor_y5 != array_get($input, 'rpor_y5')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPOR YEAR 5 Changed']);
              $updated = true;
            }
          if($log_rpor_y6 != array_get($input, 'rpor_y6')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPOR YEAR 6 Changed']);
              $updated = true;
            }
          if($log_rpor_y99 != array_get($input, 'rpor_y99')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'RPOR YEAR 99 Changed']);
              $updated = true;
            }
            if($log_spor != array_get($input, 'spor')) {
              LogProduct::create(['product_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'SPOR Changed']);
              $updated = true;
            }

          }

        if ($new) {
          // return
          return Response::json(['body' => 'Product successfully created.']);
        } else {
          return Response::json(['body' => 'Product successfully updated.']);
        }
    }


    public function caseSave() {
      $new = true;

      $input = Input::all();
      // dd($input);
      // check if an ID is passed
      if(array_get($input, 'id')) {

        // get the user info
        $row = ProductRider::find(array_get($input, 'id'));

        if(is_null($row)) {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }

        // this is an existing row
        $new = false;
      }
     
      $rules = [
        'provider_id' => 'required',
        'category_id' => 'required',
        'name' => 'required',
      ];
    
      $names = [

      ];

      $messages = [
        'provider_id.required' => 'error-provider_id|*Select value',
        'category_id.required' => 'error-category_id|*Select value',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules, $messages);
      $validator->setAttributeNames($names);

      // return errors
      if($validator->fails()) {
        return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      // create model if new
      if($new) {
        $row = new ProductRider;
        $row->provider_id = array_get($input, 'provider_id');
        $row->main_id = array_get($input, 'main_id');
      }

      $row->name = array_get($input, 'name');
      $row->category_id = array_get($input, 'category_id');
      $row->save();


      if ($new) {
        // return
        return Response::json(['body' => 'Case Submission Rider successfully created.']);
      } else {
        return Response::json(['body' => 'Case Submission Rider successfully updated.']);
      }
    }

    /**
     * Disable multiple user
     *
     */
    public function delete() {

      $row = Product::find(Request::input('id'));
      // check if user exists
      if(!is_null($row)) {
        //if users tries to disable itself
       
        $row->status = 2;
        $row->save();

        LogProduct::create(['product_id' => Request::input('id'), 'edit_id' => Auth::user()->id, 'activity' => 'Product Removed']);
        // return
        return Response::json(['body' => 'Product has been removed.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }


    /**
     * Disable multiple user
     *
     */
    public function deleteCase() {

      $row = ProductRider::find(Request::input('id'));
      // check if user exists
      if(!is_null($row)) {
        //if users tries to disable itself
       
        $row->status = 2;
        $row->save();

        LogProduct::create(['product_id' => Request::input('id'), 'edit_id' => Auth::user()->id, 'activity' => 'Case Submission Rider Removed']);
        // return
        return Response::json(['body' => 'Rider has been removed.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }


    /**
     * Deactivate the user
     *
     */
    public function disable() {
        $row = ProductMain::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
          //if users tries to disable itself
         
          $row->status = 2;
          $row->save();

          LogProduct::create(['product_id' => Request::input('id'), 'edit_id' => Auth::user()->id, 'activity' => 'Main Product Removed']);
          // return
          return Response::json(['body' => 'Main product has been removed.']);
        } else {
          // not found
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the user
     *
     */
    public function enable() {
        $row = Product::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
          //if users tries to disable itself
         
          $row->status = 1;
          $row->save();

          LogProduct::create(['product_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Product Enabled']);

          // return
          return Response::json(['body' => 'Product has been activated.']);
        } else {
          // not found
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Disable multiple user
     *
     */
    public function disableSelected() {
        $ids = Request::input('ids');
       
        if (count($ids) == 0) {
            return Response::json(['error' => 'Select products first.']); 
        } else {
           

            // process
            foreach ($ids as $id) {
                $row = Product::find($id);
                if(!is_null($row)) {
                    
                    $status = $row->status;
                    // disable only the active users.
                    if ($status == 1){
                      $row->status = 0;
                      $row->save();
                      LogProduct::create(['product_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Product/s Disabled']);
                     
                    }
                }
            }
            return Response::json(['body' => 'Selected products has been Deactivated.']);
        }
    }  

    /**
     * Remove multiple user
     *
     */
    public function removeSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select products first.']); 
        } else {

            // process
            foreach ($ids as $id) {
                $row = Product::find($id);
                if(!is_null($row)) {
                    $status = $row->status;
                    // you cannot remove user that already been remove
                    if ($status != 2){

                      $row->status = 2;
                      $row->save();
                       LogProduct::create(['product_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Product/s Deleted']);
                   
                    }
                    
                }
            }

            return Response::json(['body' => 'Selected products has been Removed.']);
        }
    }

    public function productCase() {

      $get_riders = ProductRider::with('getProvider')
                            ->with('getCategory')
                            ->where('main_id', Request::input('id'))
                            ->where('status', 1)
                            ->get();

      $rows = '<strong>Case Submission Riders</strong><div class="table-responsive">' .
              '<table class="table table-striped table-pagination" id="my-table">' .
              '<thead class="tbheader">' .
                  '<tr>' .
                    '<th data-sort="name"><i></i><small>PRODUCT</small></th>'. 
                    '<th data-sort="provider_name"><i></i><small>PROVIDER</small></th>' .
                    '<th data-sort="category_name"><i></i><small>PRODUCT<br>CATEGORY</small></th>' .
                    (Auth::user()->usertype_id != 8 ? '<th class="rightalign no-sort"><small>TOOLS</small></th>' : '') .
                  '</tr>' .
                '</thead>' .
                '<tbody id="rows">';


      foreach ($get_riders as $key => $field) {
        $rows .= '<tr data-id="' . $field->id . '">' .
                  '<td>' . $field->name . '</td>' .
                  '<td>' . $field->getProvider->name . '</td>' .
                  '<td>' . $field->getCategory->name . '</td>' .
                  (Auth::user()->usertype_id != 8 ?  '<td class="rightalign">' .
                    '<button type="button" class="btn btn-xs btn-table btn-edit-case" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-pencil"></i></button>' .
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete-case" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-trash"></i></button>' .
                  '</td>' : '')
                  .
                  '</tr>';
      }

      $rows .= '</tbody>' .
                '</table>' .
                '</div>';

      return Response::json($rows);
    }

    /**
     * Get Product Riders
     *
     */
    public function productRider() {

      $get_riders = Product::with('getProvider')
                            ->with('getCategory')
                            ->with('getClassification')
                            ->where('main_id', Request::input('id'))
                            ->where('status', 1)
                            ->get();

      $rows = '<strong>Riders</strong><div class="table-responsive">' .
              '<table class="table table-striped table-pagination" id="my-table">' .
              '<thead class="tbheader">' .
                  '<tr>' .
                    '<th data-sort="name"><i></i><small>PRODUCT</small></th>'. 
                    '<th data-sort="provider_name"><i></i><small>PROVIDER</small></th>' .
                    '<th data-sort="code"><i></i><small>CODE</small></th>'. 
                    '<th data-sort="sequence_no"><i></i><small>SEQUENCE NO</small></th>' .
                    '<th data-sort="category_name"><i></i><small>PRODUCT<br>CATEGORY</small></th>' .
                    '<th data-sort="classification_name"><i></i><small>PRODUCT<br>CLASSIFICATION</small></th>' .

                    (Auth::user()->usertype_id != 8 ? '<th class="rightalign no-sort"><small>TOOLS</small></th>' : '') .
                  '</tr>' .
                '</thead>' .
                '<tbody id="rows">';

      foreach ($get_riders as $key => $field) {
        $rows .= '<tr data-id="' . $field->id . '">' .
                  '<td>' . $field->product_name . '</td>' .
                  '<td>' . $field->getProvider->name . '</td>' .
                  '<td>' . $field->code . '</td>' .
                  '<td>' . $field->sequence_no . '</td>' .
                  '<td>' . $field->getCategory->name . '</td>' .
                  '<td>' . $field->getClassification->name . '</td>' .
                  (Auth::user()->usertype_id != 8 ? '<td class="rightalign">' .
                    '<button type="button" class="btn btn-xs btn-table btn-view" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-eye"></i></button>' .
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-pencil"></i></button>' .
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete" data-provider="' . $field->provider_id . '" data-id="' . $field->id . '"><i class="fa fa-trash"></i></button>' .
                  '</td>' : '') .
                  '</tr>';
      }


      $rows .= '</tbody>' .
                '</table>' .
                '</div>';

      return Response::json($rows);
    }

    /**
     * Get Product Rate
     *
     */
    public function productRate() {

        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $id = Request::input('id');

        $rows = ProductRate::where('product_id', '=', $id)->orderBy('created_at', 'desc')->get();
        //dd($rows);
        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
        
        return Response::json(['body' => 'success.']);

    }

    /**
     * Get Product Rate
     *
     */
    public function rate() {

        $id = Request::input('product_id');
        // dd($id);
        $check_if_product_rate = Product::find($id);

        $rules = [
            'rate' => 'required|numeric',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($check_if_product_rate) {
            $rate = new ProductRate;
            $rate->product_id = $id;

            $prev_rate = ProductRate::where('product_id', '=', $id)->orderBy('created_at', 'desc')->first();
            if ($prev_rate) {
              $rate->prev_rate = $prev_rate->rate;
            } else {
              $rate->prev_rate = $check_if_product_rate->conversion_rate;
            }
            $rate->rate = Request::input('rate');
            $rate->save();

            $check_if_product_rate->conversion_rate = Request::input('rate');
            $check_if_product_rate->save(); 
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }

        return Response::json(['body' => 'success.']);

    }

    public function export() {

   
     $file_name = rand(1111111, 9999999);

    if (Request::has('product-log')) {

       $rows = LogProduct::with('productLog')
                            ->with('editProductLog')
                            ->orderBy('created_at', 'desc')
                            ->where('edit_id','=',Request::input('designation_id'))
                            ->get();             
      if(count($rows)!= 0) {

         Excel::create($file_name, function($excel) {

        $excel->sheet('Product', function($sheet) {

          $rows = LogProduct::with('productLog')
                            ->with('editProductLog')
                            ->orderBy('created_at', 'desc')
                            ->where('edit_id','=',Request::input('designation_id'))
                            ->get();

          $sheet->appendRow(array(
              'PRODUCT CODE', 'PRODUCT ID', 'ACTIVITY', 'DATE', 'EDITED BY'
          ));

          foreach ($rows as $row) {
            $sheet->appendRow(array(
              $row->productLog->code, $row->productLog->sequence_no, $row->activity, $row->created_at, $row->editProductLog->name
            ));
          }

        });

      })->download('xls');
       return Response::json(['body' => 'No Data.']);   
      }else{

        return Response::json(['error' => ['No Data.']]);

      } 
     
     }

    if (Request::has('product-log-all')) {
     
      Excel::create($file_name, function($excel) {

        $excel->sheet('Product', function($sheet) {

          $rows = LogProduct::with('productLog')
                            ->with('editProductLog')
                            ->orderBy('created_at', 'desc')
                            ->where('created_at', '>=', Request::input('from'))
                            ->where('created_at', '<=', Request::input('to'))
                            ->get();

          $sheet->appendRow(array(
              'PRODUCT CODE', 'PRODUCT ID', 'ACTIVITY', 'DATE', 'EDITED BY'
          ));

          foreach ($rows as $row) {
            $sheet->appendRow(array(
              $row->productLog->code, $row->productLog->sequence_no, $row->activity, $row->created_at, $row->editProductLog->name
            ));
          }

        });

      })->download('xls');
     }
      //return Response::json(['body' => 'Success.']);
      
    }

    public function getProperty() {

      $rows = ProviderClassification::where('provider_id', Request::input('id'))->where('status', 1)->get();
      $rows_2 = ProviderCategory::where('provider_id', Request::input('id'))->where('status', 1)->get();

      $result['classifications'] = $rows->toArray();
      $result['categories'] = $rows_2->toArray();

      return Response::json($result);

    }

  public function notification($id){  
    $row = NotificationPerUser::where(function($query) use ($id) {
                                  $query->where('user_id', '=', Auth::user()->id)
                                          ->where('notification_id', '=', $id);

                              })->update(['is_read' => 1]);
    return Redirect::to('/policy/production/case-submission');
   }
}
