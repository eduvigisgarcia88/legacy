<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use Auth;
use Validator;
use DB;
use App\User;
use Response;
use App\Sales;
class SummaryController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    
    /**
     * Display a listing of the summaries.
     *
     * @return Response
     */
    public function index()
    {
        $this->data['title'] = "System Users";
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();


                                 
              $rows = User::join('sales','sales.user_id','=','users.id')
                              ->join('designations','designations.id','=','sales.designation_id')
                              ->leftJoin('sales_advisors','users.id', '=', 'sales_advisors.user_id')
                              ->leftJoin('sales_supervisors', 'sales_advisors.supervisor_id','=','sales_supervisors.id')
                              ->leftJoin('users as supervisors', 'sales_supervisors.user_id', '=',  'supervisors.id')
                              ->select('supervisors.name as supervisor_name', 'users.name','users.code','sales.sales_id','designations.designation','sales.user_id as sales_user','designations.rank')
                              ->where('users.usertype_id','=',8)
                              ->where('users.status','!=',2)
                              ->get();
                              

       $this->data['settings'] = Settings::first();                        
        return view('summary-and-reports.view-firm-revenue-summary.list', $this->data);
    }
    
}