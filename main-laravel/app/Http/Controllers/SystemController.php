<?php

namespace App\Http\Controllers;

use Request;
use Image;
use Response;
use Hash;
use Input;
use Validator;
use Paginator;
use PDF;
use Auth;
use Excel;
use Carbon;
use DB;
use Redirect;

use App\User;
use App\Sales;
use App\Permission;
use App\LogUser;
use App\Product;
use App\ProductGross;
use App\Settings;
use App\UserType;
use App\AssignedSales;
use App\Designation;
use App\Http\Requests;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;
use App\LogSales;

class SystemController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the system users.
     *
     * @return Response
     */
    public function index()
    {
      $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      // $this->data['sort'] = $result['sort'];
      // $this->data['order'] = $result['order'];

      $this->data['type_id'] = '';
      $this->data['refresh_route'] = url('users/refresh');
      $this->data['designations'] = Designation::all();

      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  

      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

      $this->data['title'] = "System Users";
      $this->data['types'] = UserType::where('id', '!=', 8)->get();
      $this->data['settings'] = Settings::first();
      $this->data['bread_crumbs'] = 'SYSTEM USERS';

      // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      return view('user-management.system-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
      
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
        

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      } else {
        $count = User::where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }


    /**
     * Display a listing of the ceo users.
     *
     * @return Response
     */
    public function indexCEO()
    {
      $result = $this->doListCEO();

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['sort'] = $result['sort'];
      $this->data['order'] = $result['order'];
      $this->data['type_id'] = 1;
      $this->data['refresh_route'] = url('users/refresh/ceo');

      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();

      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  

      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

           // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);

      $this->data['title'] = "System Users - CEO";
      $this->data['settings'] = Settings::first();
      $this->data['types'] = UserType::where('id', '!=', 8)->get();
      $this->data['bread_crumbs'] = 'SYSTEM USERS > CEO';
      return view('user-management.system-users.list', $this->data);
    }

    /**
     * Build the list for CEO
     *
     */
    public function doListCEO() {
        
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status)
                                {
                                    $query->where('users.usertype_id', '=', '1')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      } else {
        $count = User::where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '=', '1')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '=', '1')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Display a listing of the it users.
     *
     * @return Response
     */
    public function indexIT()
    {
        $result = $this->doListIT();

        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['type_id'] = 2;
        $this->data['refresh_route'] = url('users/refresh/it');
 
       $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();

     // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "System Users - IT";
        $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '!=', 8)->get();
        $this->data['bread_crumbs'] = 'SYSTEM USERS > IT USERS';
        return view('user-management.system-users.list', $this->data);
    }

    /**
     * Build the list for IT Users
     *
     */
    public function doListIT() {
        
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status)
                                {
                                    $query->where('users.usertype_id', '=', '2')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      } else {
        $count = User::where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '=', '2')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '=', '2')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Display a listing of the admin accounts.
     *
     * @return Response
     */
    public function indexAdmin()
    {
        $result = $this->doListAdmin();

        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['type_id'] = 3;
        $this->data['refresh_route'] = url('users/refresh/admin');
 
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
                                          
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();

     // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "System Users - Admin";
        $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '!=', 8)->get();
        $this->data['bread_crumbs'] = 'SYSTEM USERS > ADMIN ACCOUNTS';
        return view('user-management.system-users.list', $this->data);
    }

    /**
     * Build the list for Admin Accounts
     *
     */
    public function doListAdmin() {

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) {
                                  $query->where('users.usertype_id', '=', '3')
                                        ->orWhere('users.usertype_id', '=', '4');
                                })
                                ->where(function($query) use ($status) {
                                  $query->where('status', 'LIKE', '%' . $status . '%')
                                        ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      } else {
        $count = User::where(function($query) {
                                  $query->where('users.usertype_id', '=', '3')
                                        ->orWhere('users.usertype_id', '=', '4');
                                })
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) {
                                  $query->where('users.usertype_id', '=', '3')
                                        ->orWhere('users.usertype_id', '=', '4');
                                })
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Display a listing of the accounting users.
     *
     * @return Response
     */
    public function indexAccounting()
    {
        $result = $this->doListAccounting();

        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['type_id'] = 5;
        $this->data['refresh_route'] = url('users/refresh/accounting'); 

         $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
      // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "System Users - Accounting";
        $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '!=', 8)->get();
        $this->data['bread_crumbs'] = 'SYSTEM USERS > ACCOUNTING';
        return view('user-management.system-users.list', $this->data);
    }

    /**
     * Build the list for Accounting users
     *
     */
    public function doListAccounting() {
        
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) {
                                  $query->where('users.usertype_id', '=', '5')
                                        ->orWhere('users.usertype_id', '=', '6');
                                })
                                ->where(function($query) use ($status) {
                                  $query->where('status', 'LIKE', '%' . $status . '%')
                                        ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      } else {
        $count = User::where(function($query) {
                                  $query->where('users.usertype_id', '=', '5')
                                        ->orWhere('users.usertype_id', '=', '6');
                                })
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) {
                                  $query->where('users.usertype_id', '=', '5')
                                        ->orWhere('users.usertype_id', '=', '6');
                                })
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Display a listing of the ceo users.
     *
     * @return Response
     */
    public function indexCompliance()
    {
      $result = $this->doListCompliance();

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['sort'] = $result['sort'];
      $this->data['order'] = $result['order'];
      $this->data['type_id'] = 9;
      $this->data['refresh_route'] = url('users/refresh/compliance');

      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();

      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  

      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

           // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);

      $this->data['title'] = "System Users - Compliance";
      $this->data['settings'] = Settings::first();
      $this->data['types'] = UserType::where('id', '!=', 8)->get();
      $this->data['bread_crumbs'] = 'SYSTEM USERS > Compliance';
      return view('user-management.system-users.list', $this->data);
    }

    /**
     * Build the list for Compliance
     *
     */
    public function doListCompliance() {
        
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) {
                                  $query->where('users.usertype_id', '=', '9')
                                        ->orWhere('users.usertype_id', '=', '10');
                                })
                                ->where(function($query) use ($status)
                                {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      } else {
        $count = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) {
                                  $query->where('users.usertype_id', '=', '9')
                                        ->orWhere('users.usertype_id', '=', '10');
                                })
                                ->where(function($query) use ($status)
                                {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) {
                                  $query->where('users.usertype_id', '=', '9')
                                        ->orWhere('users.usertype_id', '=', '10');
                                })
                                ->where(function($query) use ($status)
                                {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }


    /**
     * Display a listing of the sales users.
     *
     * @return Response
     */
    public function indexSales() // Sales Assistant
    {

        $result = $this->doListSales();

        $this->data['rows'] = $result['rows'];
        $this->data['designations'] = Designation::all();
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();

     // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['type_id'] = 7;
        $this->data['refresh_route'] = url('users/refresh/sales-assistant');
        $this->data['title'] = "System Users - Sales Assistant";
        $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '!=', 8)->get();
        $this->data['bread_crumbs'] = 'SYSTEM USERS > SALES ASSISTANT';


        //dd(Permission::where('usertype_id', '=', Auth::user()->usertype_id)->get());

        //dd($this->data['permission']);
        return view('user-management.system-users.sales-assistant.list', $this->data);


    }

    /**
     * Build the list for Sales users
     *
     */
    public function doListSales() {
         
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status)
                                {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      } else {
        $count = User::where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                                        ->orWhere('mobile', 'LIKE', '%' . $search . '%')
                                        ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
    
    /**
     * Display a listing of the permissions.
     *
     * @return Response
     */
    public function indexPermission()
    {

        $result = $this->doListPermission();

        $this->data['rows'] = $result['rows'];
        //dd($this->data);
        $this->data['title'] = "Permissions";
        $this->data['types'] = UserType::where('id', '!=', 8)->get();
         $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
     // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('user-management.system-users.permissions.list', $this->data);
    }
    
    /**
     * Build the list for Permissions
     *
     */
    public function doListPermission() {

        $rows_sales = UserType::with('modules')
                            ->where('id', 8)
                            ->with('modules.permissionModules');

        $rows = UserType::with('modules')
                            ->where('id', '!=', 8)
                            ->with('modules.permissionModules')
                            ->union($rows_sales)
                            ->get();
                            
        //$rows = Permission::select('permissions.*', 'permission_modules.module')
        //                    ->join('permission_modules', 'permissions.module_id', '=', 'permission_modules.id')
        //                    ->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }


    /**
     * Save Permissions.
     *
     * @return Json
     */
    public function savePermission()
    {
    
        $rows = Request::input('rows');
  
        foreach ($rows as $row) {
            $permission = Permission::find([$row['id']])->first();

            //
            if ($permission->usertype_id == 3 || $permission->usertype_id == 5 || $permission->usertype_id == 7 || $permission->usertype_id == 9) {
                $pe = Permission::where('usertype_id', $permission->usertype_id + 1)
                                ->where('module_id', $permission->module_id)
                                ->update(['view' => ($row['view'] == 'true') ? 1 : 0, 'edit' => ($row['edit'] == 'true') ? 1 : 0, 'delete' => ($row['delete'] == 'true') ? 1 : 0]);
                                
            }

            $permission->view = ($row['view'] == 'true') ? 1 : 0;
            $permission->edit = ($row['edit'] == 'true') ? 1 : 0;
            $permission->delete = ($row['delete'] == 'true') ? 1 : 0;
            $permission->save();            
 
        }    

        return response()->json(['success' => 'Permissions has been updated.']);
    }

    /**
     * Display a listing of the logs.
     *
     * @return Response
     */
    public function indexLog()
    {
        $result = $this->doListLog();
         $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  

        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();

             // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        $this->data['settings'] = Settings::first();
        $this->data['noti_count'] = count($this->data['notification']);
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['title'] = "System User Log";

        // Exporting Logs
        // Check if file exists in app/storage/file folder
        // if(Request::input('export-destination')) {
        //   $file_path = storage_path() .'/exports/Users.xls';
        //   if (file_exists($file_path))
        //   {
        //       // Send Download
        //       return Response::download($file_path, 'Users.xls', [
        //           'Content-Length: '. filesize($file_path)
        //       ]);
        //   }

        // }
        

        return view('user-management.system-users.log.list', $this->data);
    }

    /**
     * Display a listing of the system users.
     *
     * @return Response
     */
    public function doListLog() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'log_users.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = LogUser::join('users', 'users.id', '=', 'log_users.user_id')
                        ->join('users as edit_users', 'edit_users.id', '=', 'log_users.edit_id')
                        ->select('log_users.*', 'edit_users.name as edit_name', 'users.name', 'users.code', 'users.system_id')
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

      } else {
        $count = LogUser::paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = LogUser::join('users', 'users.id', '=', 'log_users.user_id')
                        ->join('users as edit_users', 'edit_users.id', '=', 'log_users.edit_id')
                        ->select('log_users.*', 'edit_users.name as edit_name', 'users.name', 'users.code', 'users.system_id')
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;

          return $result;
      }
    }

    /**
     * Save model
     *
     */
    public function save() {

        $new = true;

        $input = Input::all();

        $pass_score = 0;
        if (Input::get('pass_score')) {
            $pass_score = Input::get('pass_score');
        }
        $password = Input::get('password');

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = User::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'photo' => $new ? 'image|max:3000' : 'image|max:3000',
            //'system_id' => $new ? 'required|min:2|max:100' : 'min:2|max:100',
            'code' => 'required|min:2|max:100',
            //'username' => 'required|min:2|max:15|alpha_dash|unique:users,username' . (!$new ? ',' . $row->id : ''),
            'password' => $new ? 'required|min:8|confirmed' : 'min:8|confirmed',
            'name' => 'required|min:2|max:100',
            'email'  => 'required|email|unique:users,email' . (!$new ? ',' . $row->id : ''),
            'mobile' => 'required|min:2|max:100',
        ];

        // do not require type if user is editing self
        if($new || ($new == false && $row->id != Auth::user()->id)) {
            $rules['usertype_id'] = 'required|exists:usertypes,id';
        }

        // field name overrides
        $names = [
        
        ];

        $messages = [
          'usertype_id.required' => 'error-usertype_id|*Select value',
        ];

        if ($new) {
          if ($pass_score <= 50) {

            return Response::json(['error' => ["error-pass_score|*Password must be at least <b><u>MEDIUM</u></b>"]]);
          } else {
            if(preg_match("/^(?=.*[A-Z])(?=.*\d)([0-9a-zA-Z]+)$/", $password) == 0 && preg_match('`[A-Z]`', $password) && preg_match('`[0-9]`', $password)) { 

            } else {
              return Response::json(['error' => ["error-pass_score|*Password must contain at least one uppercase, lowercase, numeric and special character."]]);
            }
          }
        } else {
          if ($pass_score <= 50 && (strlen($password) > 0)) {
            return Response::json(['error' => ["error-pass_score|*Password must be at least <b><u>MEDIUM</u></b>"]]);
          } else if (strlen($password) > 0) {
            if(preg_match("/^(?=.*[A-Z])(?=.*\d)([0-9a-zA-Z]+)$/", $password) == 0 && preg_match('`[A-Z]`', $password) && preg_match('`[0-9]`', $password)) { 

            } else { 
              return Response::json(['error' => ["error-pass_score|*Password must contain at least one uppercase, lowercase, numeric and special character."]]);
            }
          }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules, $messages);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $log_code = "";
        $log_name = "";
        $log_email = "";
        $log_mobile = "";
        $log_username = "";
        $log_password = "";
        $log_usertype_id = "";

        // create model if new
        if($new) {
          $row = new User;
          $row->code      = array_get($input, 'code');
          $row->name      = strip_tags(array_get($input, 'name'));
          $row->email     = array_get($input, 'email');
          $row->mobile    = array_get($input, 'mobile');
          $row->username  = array_get($input, 'email');
          $row->usertype_id  = array_get($input, 'usertype_id');
        } else {
          $log_code         = $row->code;
          $log_name         = $row->name;
          $log_email        = $row->email;
          $log_mobile       = $row->mobile;
          $log_username     = $row->username;
          $row->code      = array_get($input, 'code');
          $row->name      = strip_tags(array_get($input, 'name'));
          $row->email     = array_get($input, 'email');
          $row->mobile    = array_get($input, 'mobile');
          $row->username  = array_get($input, 'email');
          $row->usertype_id  = array_get($input, 'usertype_id');
          $row->save();
        }   

        // set type only if this isn't me
        if($new || ($new == false && $row->id != Auth::user()->id)) {
          $log_usertype_id = $row->usertype_id;
          $row->usertype_id = array_get($input, 'usertype_id');
        }

        // do not change password if old user and field is empty
        if($new || ($new == false && array_get($input, 'password'))) {
          $log_password = $row->password;
          $row->password = Hash::make(array_get($input, 'password'));
        }

        // save model
        $row->save();

        if($new) {
            // auto generate id
            $row->system_id = 'USER-' . sprintf("%04d", $row->id);
            $row->save();

        }

        if (Request::hasFile('photo')) {
            // Save the photo
            Image::make(Request::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
            $row->photo = $row->id . '.jpg';
            $row->save();
        } else {

          if(array_get($input, 'image_link') || array_get($input,'image_fb_link_edit_photo'))
          {
              //dd(array_get($input, 'image_link'));
              $img = file_get_contents(array_get($input, 'image_link'));
              Image::make($img)->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
              $row->photo = $row->id . '.jpg';
              $row->save();

          }else{
            if($new){
              $photo = Settings::first();
              $row->photo = $photo->default_photo;
              $row->save();
            }
          }           
        }
        if($new) {
          // create log
          LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Created']);
           } else {
          $updated = false;
          //update
          if($log_code != array_get($input, 'code')) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Code Changed']);
            $updated = true;
          }
          if($log_name != strip_tags(array_get($input, 'name'))) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Last Name Changed']);
            $updated = true;
          }
          if($log_email != array_get($input, 'email')) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Email Changed']);
            $updated = true;
          }
          if($log_mobile != array_get($input, 'mobile')) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Phone Number Changed']);
            $updated = true;
          }
          if($log_usertype_id != array_get($input, 'usertype_id')) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Designation Changed']);
            $updated = true;
          }
          if($log_username != array_get($input, 'email')) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Email Changed']);
            $updated = true;
          }
          if($log_password != Hash::make(array_get($input, 'password')) && !empty(array_get($input, 'password'))) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Password Changed']);
            $updated = true;
          }
          if(Request::hasFile('photo')) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Photo Changed']);
            $updated = true;
          }
          if ($updated) {
            LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Updated']);
          }
        }

        // return
        return Response::json(['body' => 'System user created successfully.']);
    }

     /**
     * View a User
     *
     * @return Response
     */
    public function view()
    {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = User::with('userType')->find($id);
        //dd($row);
        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Deactivate the user
     *
     */
    public function disable() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
            if ($row->id == Auth::user()->id) {
                return Response::json(['error' => "Access denied."]);
            } else {
                $row->status = 0;
                $row->save();

                //create log
                LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Disabled']);
            }
            

            // return
            return Response::json(['body' => 'User account has been deactivated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the user
     *
     */
    public function enable() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to enable itself
            if ($row->id == Auth::user()->id) {
                return Response::json(['error' => "Access denied."]);
            } else {
                $row->status = 1;
                $row->save();
                //create log
                LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Enabled']);
            }

          // return
          return Response::json(['body' => 'User account has been Activated.']);
        } else {
          // not found
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Disable multiple user
     *
     */
    public function disableSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select users first.']); 
        } else {
            // check if auth user is selected
            foreach ($ids as $id_check) {
                $row = User::find($id_check);
                if ($row->id == Auth::user()->id) {
                    return Response::json(['error' => "Access denied."]);
                } 
            }

            // process
            foreach ($ids as $id) {
                $row = User::find($id);
                if(!is_null($row)) {
                    
                    $status = $row->status;
                    // disable only the active users.
                    if ($status == 1){
                      $row->status = 0;
                      $row->save();
                      //create log
                      LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Disabled']);
                    }
                }
            }
            return Response::json(['body' => 'Selected users has been Deactivated.']);
        }
    }  

    /**
     * Remove multiple user
     *
     */
    public function removeSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select users first.']); 
        } else {

            // check if auth user is selected
            foreach ($ids as $id_check) {
                $row = User::find($id_check);
                if ($row->id == Auth::user()->id) {
                    return Response::json(['error' => "Access denied."]);
                } 
            }

            // process
            foreach ($ids as $id) {
                $row = User::find($id);
                if(!is_null($row)) {
                    $status = $row->status;
                    $cur_email = $row->email;
                    $cur_username = $row->username;
                    // you cannot remove user that already been remove
                    if ($status != 2){
                      $row->email = '(' . $cur_email . ')';
                      $row->username = '(' . $cur_username . ')';
                      $row->status = 2;
                      $row->save();
                      //create log
                      // LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Removed']);
                      LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Removed']);
                    }
                    
                }
            }

            return Response::json(['body' => 'Selected users has been Removed.']);
        }
    }  

    /**
     * Get Available Agents.
     *
     * @return Response
     */
    public function getAvailableAgents()
    {
        $designation_id = Request::input('designation_id');

        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $ids = AssignedSales::all()->lists('sales_id');

        // get all users by designation id
        if ($designation_id != 0) {
          $rows = User::select('users.*', 'sales.designation_id')
                          ->join('sales', 'users.id', '=', 'sales.user_id')
                          ->where('users.usertype_id', '=', '8')
                          ->where('users.status', '!=', '2')
                          ->where('sales.designation_id', '=', $designation_id)
                          ->whereNotIn('users.id', $ids)
                            ->orderBy($result['sort'], $result['order'])
                            ->get();                           
        } else {

          // get all available agents
          $rows = User::select('users.*')
                          ->where('users.usertype_id', '=', '8')
                          ->where('users.status', '!=', '2')
                          ->whereNotIn('users.id', $ids)
                            ->orderBy($result['sort'], $result['order'])
                            ->get();
        }
        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Get User Agents.
     *
     * @return Response
     */
    public function getAgents()
    {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        $id = Request::input('id');
        
        $rows = AssignedSales::select('users.*', 'assigned_sales.id as assigned_sales_id','designations.designation', 'sales.id as sales_id')
                            ->join('users', 'users.id', '=', 'assigned_sales.sales_id')
                            ->join('sales', 'sales.user_id', '=', 'assigned_sales.sales_id')
                            ->join('designations', 'designations.id', '=', 'sales.designation_id')
                            ->where('assigned_sales.assistant_id', '=', $id)
                            ->where('users.usertype_id', '=', '8')
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->get();

                            // dd($rows);

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Save available agents.
     *
     * @return Response
     */
    public function saveAgents()
    {
      // dd(Request::all());
        $rules = [
          'ids' => 'required'
        ];

        // field name overrides
        $names = [
          'ids' => 'agents',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }


      // $validator = Validator::make(Request::all(), [
      //       'ids' => 'required|array',
      //       'row' => 'required|integer',
      //   ]);

      //   if ($validator->fails()) {
      //       return Response::json(['type' => 'error', 'body' => 'failed']);
      //   }


      $ids = Request::input('ids');

      foreach ($ids as $id) {
        AssignedSales::create(['sales_id' => $id, 'assistant_id' =>  Request::input('id')]);
        LogUser::create(['user_id' => $id, 'edit_id' => Auth::user()->id, 'activity' => 'Agent Assigned']);
      }

      return Response::json(['type' => 'success', 'body' => 'success']);
    }

    /**
     * Remove Multiple agents.
     *
     * @return Response
     */
    public function removeAgents()
    {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['type' => 'error', 'body' => 'Access Denied.']);
        }

        foreach ($ids as $id) {
          $assistant_id = AssignedSales::find($id)->first()->assistant_id;

          AssignedSales::destroy($id);
          LogUser::create(['user_id' => $assistant_id, 'edit_id' => Auth::user()->id, 'activity' => 'Agent Remove']);
        }
        
        return Response::json(['type' => 'success', 'body' => 'Success']);
    }

    /**
     * Remove agent.
     *
     * @return Response
     */
    public function removeAgent()
    {
        $id = Request::input('id');


        $assistant_id = AssignedSales::find($id)->first()->assistant_id;

        AssignedSales::destroy($id);

        LogUser::create(['user_id' => $assistant_id, 'edit_id' => Auth::user()->id, 'activity' => 'Agent Remove']);
        
        return Response::json(['type' => 'success', 'body' => 'Success']);

    }

    /**
     * Remove agent.
     *
     * @return Response
     */
    public function exportAll()
    {

      // $data['rows'] = User::all();

      // $pdf = PDF::loadView('pdf.sample', $data);
      // return $pdf->setPaper('a4')->download('invoice.pdf');

      if (Request::has('export-destination')) {
        
       
           Excel::create('System-User-Logs', function($excel) {

          // // Set the title
          // $excel->setTitle('Our new awesome title');

          // // Chain the setters
          // $excel->setCreator('Maatwebsite')
          //       ->setCompany('Maatwebsite');

          // // Call them separately
          // $excel->setDescription('A demonstration to change the file properties');

          $excel->sheet('System-User-Logs', function($sheet) {

            // sort and order
            $result['sort'] = Request::input('s') ?: 'log_users.created_at';
            $result['order'] = Request::input('o') ?: 'desc';
           
            $rows = LogUser::with('userLog')
                            ->with('editUserLog')
                            ->where('edit_id','=',Request::input('destination'))
                            ->get();
                    
            $sheet->appendRow(array(
                'AGENT ID', 'AGENT CODE', 'AGENT NAME', 'ACTIVITY', 'DATE', 'EDITED BY'
            ));


            foreach ($rows as $row) {
              $sheet->appendRow(array(
                $row->userLog->system_id, $row->userLog->code, $row->userLog->name, $row->activity, $row->created_at->toDateTimeString(), $row->editUserLog->name
              ));
            }

          });


        })->download('xls');
        
         //return Response::json(['type' => 'success', 'body' => 'Success']);

      }

      // EXPORT ALL
      if (Request::has('export-all')) {
        Excel::create('System-User-Logs', function($excel) {

          // // Set the title
          // $excel->setTitle('Our new awesome title');

          // // Chain the setters
          // $excel->setCreator('Maatwebsite')
          //       ->setCompany('Maatwebsite');

          // // Call them separately
          // $excel->setDescription('A demonstration to change the file properties');

          $excel->sheet('System-User-Logs', function($sheet) {

            // sort and order
            $result['sort'] = Request::input('s') ?: 'log_users.created_at';
            $result['order'] = Request::input('o') ?: 'desc';

            $rows = LogUser::with('userLog')
                            ->with('editUserLog')
                            ->where('created_at', '>=', Request::input('from'))
                            ->where('created_at', '<=', Request::input('to'))
                            ->get();

            $sheet->appendRow(array(
                'AGENT ID', 'AGENT CODE', 'AGENT NAME', 'ACTIVITY', 'DATE', 'EDITED BY'
            ));


            foreach ($rows as $row) {
              $sheet->appendRow(array(
                $row->userLog->system_id, $row->userLog->code, $row->userLog->name, $row->activity, $row->created_at->toDateTimeString(), $row->editUserLog->name
              ));
            }

          });


        })->download('xls');
      } 
      // return Response::json(['type' => 'success', 'body' => 'Success']);
    }
        public function notification($id){  
 $row = NotificationPerUser::where(function($query) use ($id) {
                                $query->where('user_id', '=', Auth::user()->id)
                                        ->where('notification_id', '=', $id);

                            })->update(['is_read' => 1]);
return Redirect::to('/policy/production/case-submission');
   }

}