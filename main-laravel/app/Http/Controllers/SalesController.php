<?php

namespace App\Http\Controllers;
use App\Designation;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Crypt;
use Request;
use Image;
use Auth;
use Excel;
use Paginator;
use DateTime;

use App\Sales;
use App\CPDHours;
use App\Permission;
use App\GI;
// use App\SalesGI;
use App\User;
use App\UserType;
use App\SalesDesignation;
use App\SalesBonding;
use App\SalesGroup;
use App\SalesTeam;
use App\SalesNoTeam;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\SalesPartner;
use App\DesignationRates;
use App\LogSales;
use App\LogTeam;
use App\Team;
use App\Policy;
use App\Additionalinfo;
use App\TeamInfo;
use App\iServerInfo;
use App\MiscInfo;
use App\DialerInfo;
use App\SecurityInfo;
use App\CompanyInfo;
use App\SalesForceInfo;
use App\JumpsiteInfo;
use App\PrePayroll;
use App\Introducer;
use App\Group;
use App\Settings;
use App\Tier;
use App\Provider;
use App\ProviderClassification;
use App\ProviderCodes;
use App\SettingsCPD;
use App\BSCGrade;
use Input;
use Validator;
use DB;
use Carbon\Carbon;
use Redirect;
use App\Notification;
use App\NotificationPerUser;
use Response;
use Hash;
use App\SalesUser;
use Mail;

class SalesController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    /**
     * Display a listing of all sales users.
     *
     * @return Response
     */
    public function index() {

//   $ctr = 0;
//   $uctr = 0;

// Excel::load(storage_path('excel/exports/upload/bsc_grade.xlsx'), function($reader) use ($ctr, $uctr) {
//   $results = $reader->all();

//   foreach ($results as $key => $field) {

//     if ($field->sales_force_id != "0") {
//       $ctr++;
//       $user = SalesUser::where('salesforce_id', $field->sales_force_id)->first();

//       if ($user) {
//         $uctr++;
//         $row = new BSCGrade;
//         $row->user_id = $user->id;
//         $row->rnf_no  = $field->rnf_number;
//         $row->rnf_status = $field->rnf_status;
//         $row->q1_grade = $field->q1_grade;
//         $row->q1_supervisor_grade = $field->q1_supervisor_grade;
//         $row->q2_grade = $field->q2_grade;
//         $row->q2_supervisor_grade = $field->q2_supervisor_grade;
//         $row->q3_grade = $field->q3_grade;
//         $row->q3_supervisor_grade = $field->q3_supervisor_grade;
//         $row->q4_grade = $field->q4_grade;
//         $row->q4_supervisor_grade = $field->q4_supervisor_grade;
//         $row->save();
//       }
//     }
//   }
// });

// dd($ctr . '====' . $uctr);

      $result = $this->doList();

      $this->data['main_title'] = "SALES USERS";

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['get_provider_count'] = "0";
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['groups'] = Group::where('status', 1)->get();

      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

      // for notification
      if ( Auth::user()->usertype_id == '8') { //if sales user
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                  ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                  ->select('users.*','notifications_per_users.*','notifications.*')
                                  ->where(function($query) {
                                        $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                              ->where('notifications.usertype_id', '=', '1')
                                              ->where('notifications_per_users.is_read', '=', '0');
                                              
                                    })->get();
     
      } else {
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                  ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                  ->select('users.*','notifications_per_users.*','notifications.*')
                                  ->where(function($query) {
                                        $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                              ->where('notifications.usertype_id', '=', '2')
                                              ->where('notifications_per_users.is_read', '=', '0');
                                    })->get();
      }   

         // $user = User::where('status', '!=', 2)->where('usertype_id', 8)->lists('id');

         // foreach ($user as $key => $value) {
         // $check_advisor = SalesAdvisor::where('user_id', $value)->first();
         //   if ($check_advisor) {
         //    $get_supervisor = SalesSupervisor::find($check_advisor->supervisor_id);  
         //   } else {
         //    $get_supervisor = SalesSupervisor::where('user_id', $value)->first();  
         //   }

         //   $row = new SalesGroup;
         //   $row->user_id = $value;
         //   $row->supervisor_id = ($get_supervisor ? $get_supervisor->id : '');
         //   $row->group_id = ($get_supervisor ? $get_supervisor->group_id : '');
         //   $row->save();
         // }
        
      $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users";
      $this->data['designations'] = Designation::all();
      $this->data['designation_rates'] = DesignationRates::all();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      //$this->data['settings'] = Settings::first();
      $this->data['type_id'] = '0';
      $this->data['refresh_route'] = url('sales/refresh');
      $this->data['bread_crumbs'] = '';
      $this->data['groupy'] = Group::all();
      $this->data['settings'] = Settings::first();
      $this->data['tiers'] = Tier::all();
      $this->data['gis'] = GI::all();

      $this->data['providers'] = Provider::all();
      $this->data['get_rank'] = "";
      return view('user-management.sales-users.list', $this->data);
    }
    
    /**
     * Display a listing of all sales users.
     *
     * @return Response
     */
    public function doList() {


      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status']);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      } else {
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status']);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status']);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

      // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of FSC Elite users.
     *
     * @return Response
     */
    public function indexFSCElite(){

      $result = $this->doListFSCElite();
      
      $this->data['main_title'] = "SALES USERS (FSC - Elite)";

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

     // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
      $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - FSC - Elite";
      $this->data['designations'] = Designation::all();
      $this->data['groupy'] = Group::all();
      $this->data['gis'] = GI::all();

      $this->data['settings'] = Settings::first();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['type_id'] = 9;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/fsc-elite');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListFSCElite() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 9);
                                } else {
                                  $query->where('sales.designation_id', '=', 9);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);


      } else {
        
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 9);
                                } else {
                                  $query->where('sales.designation_id', '=', 9);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
       
        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 9);
                                } else {
                                  $query->where('sales.designation_id', '=', 9);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
            $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of SFSC users.
     *
     * @return Response
     */
    public function indexSFSC(){

      $result = $this->doListSFSC();

      $this->data['gis'] = GI::all();

      $this->data['main_title'] = "SALES USERS (SFSC)";

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

     // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - SFSC";
      $this->data['designations'] = Designation::all();
      $this->data['groupy'] = Group::all();
      $this->data['settings'] = Settings::first();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['type_id'] = 8;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/sfsc');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListSFSC() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

          
        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 8);
                                } else {
                                  $query->where('sales.designation_id', '=', 8);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      } else {
       
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 8);
                                } else {
                                  $query->where('sales.designation_id', '=', 8);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        
          $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 8);
                                } else {
                                  $query->where('sales.designation_id', '=', 8);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of PFSC users.
     *
     * @return Response
     */
    public function indexPFSC() {

      $result = $this->doListPFSC();
      
      $this->data['main_title'] = "SALES USERS (PFSC)";

      $this->data['gis'] = GI::all();

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
     // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - PFSC";
      $this->data['designations'] = Designation::all();
      $this->data['groupy'] = Group::all();
      $this->data['settings'] = Settings::first();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['type_id'] = 7;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/pfsc');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListPFSC() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        
          $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 7);
                                } else {
                                  $query->where('sales.designation_id', '=', 7);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      } else {
       
          $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 7);
                                } else {
                                  $query->where('sales.designation_id', '=', 7);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

       
          $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 7);
                                } else {
                                  $query->where('sales.designation_id', '=', 7);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of EFSC users.
     *
     * @return Response
     */
    public function indexEFSC() {

      $result = $this->doListEFSC();
      $this->data['gis'] = GI::all();

      $this->data['main_title'] = "SALES USERS (EFSC)";
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - EFSC";
      $this->data['designations'] = Designation::all();
      $this->data['groupy'] = Group::all();
      $this->data['settings'] = Settings::first();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['type_id'] = 6;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/efsc');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListEFSC() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

      $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 6);
                                } else {
                                  $query->where('sales.designation_id', '=', 6);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      } else {
      
          $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 6);
                                } else {
                                  $query->where('sales.designation_id', '=', 6);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        
          $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 6);
                                } else {
                                  $query->where('sales.designation_id', '=', 6);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of FSC users.
     *
     * @return Response
     */
    public function indexFSC() {

      $result = $this->doListFSC();
      
      $this->data['gis'] = GI::all();
      $this->data['main_title'] = "SALES USERS (FSC)";

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
      $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - FSC";
      $this->data['designations'] = Designation::all();
      $this->data['groupy'] = Group::all();
      $this->data['settings'] = Settings::first();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['type_id'] = 5;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/fsc');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListFSC() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

       
        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 5);
                                } else {
                                  $query->where('sales.designation_id', '=', 5);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      } else {
       
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 5);
                                } else {
                                  $query->where('sales.designation_id', '=', 5);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      
        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 5);
                                } else {
                                  $query->where('sales.designation_id', '=', 5);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of FSM users.
     *
     * @return Response
     */
    public function indexFSM()
    {

      $result = $this->doListFSM();
      
      $this->data['gis'] = GI::all();

      $this->data['main_title'] = "SALES USERS (FSM)";

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();

      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - FSM";
      $this->data['designations'] = Designation::all();
      $this->data['settings'] = Settings::first();
      $this->data['tiers'] = Tier::all();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['groupy'] = Group::all();
      $this->data['type_id'] = 4;
      $this->data['providers'] = Provider::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/fsm');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Display a listing of the managers.
     *
     * @return Response
     */
    public function doListFSM() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 4);
                                } else {
                                  $query->where('sales.designation_id', '=', 4);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      } else {
       
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 4);
                                } else {
                                  $query->where('sales.designation_id', '=', 4);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
       
        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 4);
                                } else {
                                  $query->where('sales.designation_id', '=', 4);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of AFSD users.
     *
     * @return Response
     */
    public function indexAFSD(){

      $result = $this->doListAFSD();
      
      $this->data['gis'] = GI::all();

      $this->data['main_title'] = "SALES USERS (AFSD)";
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - AFSD";
      $this->data['designations'] = Designation::all();
      $this->data['groupy'] = Group::all();
      $this->data['settings'] = Settings::first();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['type_id'] = 3;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/afsd');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
     public function doListAFSD() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 3);
                                } else {
                                  $query->where('sales.designation_id', '=', 3);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      } else {
       
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 3);
                                } else {
                                  $query->where('sales.designation_id', '=', 3);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        
        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 3);
                                } else {
                                  $query->where('sales.designation_id', '=', 3);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of FSD users.
     *
     * @return Response
     */
    public function indexFSD()
    {

      $result = $this->doListFSD();
      
      $this->data['gis'] = GI::all();

      $this->data['main_title'] = "SALES USERS (FSD)";

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - FSD";
      $this->data['designations'] = Designation::all();
      $this->data['settings'] = Settings::first();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['groupy'] = Group::all();
      $this->data['type_id'] = 2;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['refresh_route'] = url('sales/refresh/fsd');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListFSD() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 2);
                                } else {
                                  $query->where('sales.designation_id', '=', 2);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      } else {
       
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 2);
                                } else {
                                  $query->where('sales.designation_id', '=', 2);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 2);
                                } else {
                                  $query->where('sales.designation_id', '=', 2);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    /**
     * Display a listing of the partner.
     *
     * @return Response
     */
    public function indexPartner()
    {

      $result = $this->doListPartner();
      
      $this->data['main_title'] = "SALES USERS (PARTNER)";

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['groups'] = Group::where('status','!=',2)->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['gis'] = GI::all();
  

      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Sales Users - Partner";
      $this->data['designations'] = Designation::all();
      $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
      $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();
      $this->data['settings'] = Settings::first();
      $this->data['type_id'] = 1;
      $this->data['providers'] = Provider::all();
      $this->data['tiers'] = Tier::all();
      $this->data['get_providers'] = ProviderCodes::all();
      $this->data['groupy'] = Group::all();
      $this->data['refresh_route'] = url('sales/refresh/partner');
      return view('user-management.sales-users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListPartner() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'sales.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $result['search'] = Request::input('search');
      $result['status'] = Request::input('status');
      $per = Request::input('per') ?: '10';

      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        
        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 1);
                                } else {
                                  $query->where('sales.designation_id', '=', 1);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      } else {
       
        $count = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 1);
                                } else {
                                  $query->where('sales.designation_id', '=', 1);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.id','users.usertype_id','users.photo', 'users.status','users.email',
                             'users.name', 'users.preferred_name', 'users.code', 'users.mobile','position.rank as designation_rank',
                             'position.designation as position_name','sales_user_extension.nric','sales_user_extension.salesforce_id',
                             'sales_user_extension.salutation','sales_user_extension.date_of_birth','sales_user_extension.designation_updated',
                             'sales_user_extension.designation_old','sales_user_extension.home_address','sales_user_extension.postal_code',
                             'sales_user_extension.advisors_mobile','sales_user_extension.advisors_office_mobile','sales_user_extension.latest_ytd_production',
                             'sales_user_extension.smartermail_username','sales_user_extension.agent_serial_no','sales_user_extension.contracted_date',
                             'sales_user_extension.rnf_date','sales_user_extension.off_boarding_date','sales_user_extension.notes',
                             'group_super.unit_code as super_code', 'director_code.code as director_code')
                            ->join('sales', 'sales.user_id', '=', 'users.id')
                            ->leftjoin('sales_user_extension','sales_user_extension.user_id','=','users.id')
                            ->leftjoin('designations as position', 'position.id', '=', 'sales.designation_id')
                            ->leftJoin('sales_groups', 'sales_groups.user_id', '=', 'users.id')
                            ->leftJoin('groups as director_code', 'director_code.id', '=', 'sales_groups.group_id')
                            ->leftjoin('sales_supervisors as group_super','group_super.id','=','sales_groups.supervisor_id')
                            ->where(function($query) use ($result) {
                                $query->where('users.name', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('sales_user_extension.advisors_mobile', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.email', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('director_code.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('group_super.unit_code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('users.code', 'LIKE', '%' . $result['search'] . '%')
                                      ->orWhere('position.designation', 'LIKE', '%' . $result['search'] . '%');
                              })
                            ->where(function($query) use ($result) {
                                if ($result['status']) {
                                  $query->where('director_code.code', $result['status'])
                                        ->where('sales.designation_id', '=', 1);
                                } else {
                                  $query->where('sales.designation_id', '=', 1);
                                }
                              })
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

       // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }


    /**
     * Display a listing of the team management.
     *
     * @return Response
     */


     public function getCPDs() {

      $id = Request::input('id');
      //dd($id);
      $rows = '<div class="policies-table"><table class="table table-striped">' .
              '<thead>' . 
              '<tr><th class="tbheader center-align"><small>NEW OR<br>EXISTING</small></th>' .
              // '<th class="tbheader center-align"> <small>DATE JOINED<br>&nbsp;</small></th>' .
              '<th class="tbheader center-align"> <small>CPD PERIOD<br>&nbsp;</small></th>' .
              '<th class="tbheader center-align"> <small>CPD COURSE<br>&nbsp;</small></th>' .
              // '<th class="tbheader center-align"> <small>CPD TRACKING<br>PERIOD</small></th>' .
              // '<th class="tbheader center-align"> <small>CPD HOURS REQ<br>(STRUCTURED)</small></th>' .
              // '<th class="tbheader center-align"> <small>CPD HOURS REQ<br>(ETHICS)</small></th>' .
              // '<th class="tbheader center-align"> <small>CPD HOURS REQ<br>(RULES & REGULATIONS)</small></th>' .
              '<th class="tbheader center-align"> <small>TOTAL CPD HOURS<br>(STRUCTURED)</small></th>' .
              '<th class="tbheader center-align"> <small>TOTAL CPD HOURS<br>(ETHICS)</small></th>' .
              '<th class="tbheader center-align"> <small>TOTAL CPD HOURS<br>(RULES & REGULATIONS)</small></th>' .
              '<th class="tbheader center-align"> <small>TOTAL HOURS<br>(S,E & R)</small></th>'.
              '<th class="tbheader center-align"> <small>TOTAL CPD HOURS<br>(GI)</small></th>' .
              //'<th class="tbheader center-align"> <small>SHORTFALL/<br>EXCESS HOURS</small></th>'.
              '<th class="tbheader rightalign"> <small>TOOLS<br>&nbsp;</th></thead><tbody></tr>';
              //'<th class="tbheader rightalign"><i ></i> Tools</th>';

      $get_cpds = CPDHours::where('user_id','=',$id)->where('status','!=','2')->get();
      //dd($get_cpds);
      if(count($get_cpds) != 0){
           foreach ($get_cpds as $key => $field) {
            
              $rows .= '<tr data-cpd_id='.$field->id.' data-user_id='.$field->user_id.'>' .
                     '<td style="text-align:center;">' .  $field->cpd_status. '</td>' .
                     // '<td style="text-align:center;">' .  date_format(date_create(substr($field->date_joined, 0,10)),'m/d/Y') . '</td>' .
                     '<td style="text-align:center;">' .  $field->cpd_period . '</td>' .
                     '<td style="text-align:center;">' .  $field->course . '</td>' .
                     // '<td style="text-align:center;">' .  $field->cpd_tracking_period_from .' to '. $field->cpd_tracking_period_to.'</td>' .
                     // '<td style="text-align:center;">' .  number_format($field->cpd_hours_req_skills, 2) . '</td>' .
                     // '<td style="text-align:center;">' .  number_format($field->cpd_hours_req_knowledge, 2) . '</td>' .
                     // '<td style="text-align:center;">' .  number_format($field->cpd_hours_req_rules_and_regulations, 2) . '</td>' .
                     '<td style="text-align:center;">' .  number_format($field->total_cpd_hours_skills, 2) . '</td>' .
                     '<td style="text-align:center;">' .  number_format($field->total_cpd_hours_knowledge, 2) . '</td>' .
                     '<td style="text-align:center;">' .  number_format($field->total_cpd_hours_rules_and_regulations, 2) . '</td>' .
                     '<td style="text-align:center;">' .  number_format($field->total_cpd_hours_final, 2) . '</td>' .
                     '<td style="text-align:center;">' .  number_format($field->total_cpd_hours_gi, 2) . '</td>' .
                     //'<td style="text-align:center;">' .  number_format($field->shortfall, 2) . '</td>' .
                     // '<td style="text-align:center;">' .  $final_total_both . '</td>' .
                     // '<td style="text-align:center;">' .  $final_total_skills . '</td>' .
                     // '<td style="text-align:center;">' .  $final_total_knowledge . '</td>' .
                     '<td class="rightalign">' .
                          '<button type="button" class="btn-view btn btn-xs btn-default"><i class="fa fa-eye"></i></button>'.
                          '&nbsp;<button type="button" class="btn-edit btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>'.
                          '&nbsp;<button type="button" class="btn-delete btn btn-xs btn-default"><i class="fa fa-trash"></i></button>'.
                          //'&nbsp;<button type="button" class="btn-remove btn btn-xs btn-default"><i class="fa fa-times"></i></button>'.
                          '&nbsp<input id="checkbox" type="checkbox" value="'.$field->id.'">'.
                          // '<a href="" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>' .
                          //' <a href="" type="button" class="btn-edit btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>' .
                          // ' <button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModalmove"><i class="fa fa-exchange"></i></button>' .
                          // ' <button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>' .
                          // ' <input id="checkbox" type="checkbox" value="">' .
                        '</td>' .
                     '</tr>';
          }
      }else{
          $rows .= '<tr>' .
                     '<td colspan="10" style="text-align:center;">No Available Information</td>' .'</tr>';

      }
    

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);

    }
    public function indexCPDManagement(){
        $result = $this->doListCPDManagement();

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                                })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first(); 
         // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
      $this->data['noti_count'] = count($this->data['notification']);
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['sales_users'] = User::select('users.id', 'users.name', 'sales_user_extension.salesforce_id', 'users.code')
                                      ->leftjoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'users.id')
                                      ->where('status', 1)
                                      ->where('usertype_id','=',8)
                                      ->orderBy('salesforce_id', 'asc')
                                      ->get();

      $this->data['today_year'] = Carbon::now()->format('Y');
      $this->data['title'] = "CPD Management";
      $this->data['settings'] = Settings::first();
      $this->data['settings_cpd_structured'] = SettingsCPD::where('id','=',1)->first();
      $this->data['settings_cpd_ethics'] = SettingsCPD::where('id','=',2)->first();
      $this->data['settings_cpd_rules_and_regulation'] = SettingsCPD::where('id','=',3)->first();
      $this->data['settings_cpd_gi'] = SettingsCPD::where('id','=',4)->first();
      $this->data['refresh_route'] = url("sales/cpd/refresh");
      return view('user-management.sales-users.cpd-management.list', $this->data);  
    }

    public function CPDModify(){
      $row = CPDHours::get();
      foreach($row as $to_arrs)
      {
        $string_count = strlen($to_arrs->cpd_period);
        if($string_count == 4)
        {
          $to_arrs->cpd_starting_date = Carbon::createFromFormat('Y', trim(substr($to_arrs->cpd_period, 0, 4)))->format('Y'.'-01-01'.' 00:00:00');
        }
        else if($string_count == 23 || $string_count == 22 || $string_count == 24 || $string_count == 10)
        {
          $to_arrs->cpd_starting_date = Carbon::createFromFormat('d/m/Y', trim(substr($to_arrs->cpd_period, 0, 10)))->format('Y-m-d'.' 00:00:00');
        }
        else if($string_count == 21)
        {
          $to_arrs->cpd_starting_date = Carbon::createFromFormat('d/m/Y', trim(substr($to_arrs->cpd_period, 0, 9)))->format('Y-m-d'.' 00:00:00');
        }
        else if($string_count == 19)
        {
          $to_arrs->cpd_starting_date = Carbon::createFromFormat('d/m/Y', trim(substr($to_arrs->cpd_period, 0, 8)))->format('Y-m-d'.' 00:00:00');
        }
        else
        {
          $to_arrs->cpd_starting_date = Carbon::createFromFormat('d/m/Y', '19/09/2016')->format('Y-m-d'.' 00:00:00');
          $to_arrs->cpd_period = '19/09/2016';
        }
        $to_arrs->save();
      }
    }

    public function CPDEndModify()
    {
      $row = CPDHours::get();
      foreach($row as $end)
      {
        $end->cpd_ending_date = Carbon::createFromFormat('Y-m-d H:i:s', $end->cpd_starting_date)->format('Y-m-d'.' 23:59:59');
        $end->save();
      }
    }

    public function saveCPD() {
      // assume this is a new row
      $new = true;
      
      $input = Input::all();
      // dd($input);
      // check if an ID is passed
      if(array_get($input, 'id')) {
        // get the user info
        $row = CPDHours::find(array_get($input, 'id'));
        if(!$row) {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
        // this is an existing row
        $new = false;
      }

      $rules = [
        'sales_id' => array_get($input, 'user_id') ? '' : 'required',
        'cpd_status' => 'required',
        // 'cpd_period' => 'required',
        'course' => 'required',
        'cpd_hours_req_skills' => 'required|numeric',
        'cpd_hours_req_knowledge'  => 'required|numeric',
        'cpd_hours_req_rules_and_regulations'  => 'required|numeric',
        'cpd_hours_req_gi'  => 'required|numeric',
        'total_cpd_hours_rules_and_regulations'  => 'required|numeric',
        'total_cpd_hours_skills'  => 'required|numeric',
        'total_cpd_hours_knowledge' => 'required|numeric',
        'total_cpd_hours_gi' => 'required|numeric',
        'datefromcpd' => 'required|date',
        'datetocpd' => 'required|date'.(array_get($input, 'datetocpd') == array_get($input, 'datefromcpd') ? '' : '|after:datefromcpd' ),
      ];

      // field name overrides
      $names = [
        // 'sales_id' => 'Agents',
        // 'cpd_status' => 'New or Existing',
        // 'cpd_period' => 'CPD Period',
        // 'course' => 'Course',
        // 'cpd_tracking_period' => 'CPD Tracking Period',
        // 'cpd_hours_req_skills' => 'CPD Hours(Skills)',
        // 'cpd_hours_req_knowledge' => 'CPD Hours(Knowledge)',
        // 'total_cpd_hours_skills' => 'Total CPD Hours(Skills)',
        // 'total_cpd_hours_knowledge' => 'Total CPD Hours(Knowledge)',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      if (is_array(array_get($input, 'sales_id'))) {
        // create model if new
        $check_user = User::where('status', '!=', 1)->whereIn('id', array_get($input, 'sales_id'))->count();
      } else {
        // create model if new
        $check_user = User::where('status', '!=', 1)->where('id', array_get($input, 'sales_id'))->count();
      }
      
      if ($check_user > 0) {
        return Response::json(['error' => ['error-sales_id|*Disabled Agent']]);
      }
      
      if($new) {
        if (array_get($input, 'sales_id')) {
          foreach (array_get($input, 'sales_id') as $key => $value) {
            $row = new CPDHours;
            $row->user_id = $value;
            $row->cpd_status      = array_get($input, 'cpd_status');
            $row->date_joined = Carbon::now();
            $row->cpd_period = array_get($input, 'datefromcpd') . ' - ' . array_get($input, 'datetocpd');
            $row->cpd_starting_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'datefromcpd'))->format('Y-m-d'.' 00:00:00');
            $row->cpd_ending_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'datetocpd'))->format('Y-m-d'.' 23:59:59');
            $row->course = array_get($input, 'course');

            $row->cpd_hours_req_skills    = array_get($input, 'cpd_hours_req_skills');
            $row->cpd_hours_req_knowledge  = array_get($input, 'cpd_hours_req_knowledge');
            $row->cpd_hours_req_rules_and_regulations   = array_get($input, 'cpd_hours_req_rules_and_regulations');
            $row->cpd_hours_req_gi   = array_get($input, 'cpd_hours_req_gi');
            $row->total_cpd_hours_rules_and_regulations   = array_get($input, 'total_cpd_hours_rules_and_regulations');
            $row->total_cpd_hours_skills   = array_get($input, 'total_cpd_hours_skills');
            $row->total_cpd_hours_knowledge   = array_get($input, 'total_cpd_hours_knowledge');
            $row->total_cpd_hours_gi   = array_get($input, 'total_cpd_hours_gi');
            // $row->total_cpd_hours_final   = array_get($input, 'total_cpd_hours_final');
            $row->total_cpd_hours_final   = ((array_get($input, 'total_cpd_hours_skills') ? : 0) + (array_get($input, 'total_cpd_hours_knowledge') ? : 0) + (array_get($input, 'total_cpd_hours_rules_and_regulations') ? : 0));
            $row->shortfall   = array_get($input, 'short_fall');
            $row->save();
          }
        }

        if(array_get($input, 'user_id')) {
          $row = new CPDHours;
          $row->user_id = array_get($input, 'user_id');
          $row->cpd_status      = array_get($input, 'cpd_status');
          $row->date_joined = Carbon::now();
          $row->cpd_period = array_get($input, 'datefromcpd') . ' - ' . array_get($input, 'datetocpd');
          $row->cpd_starting_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'datefromcpd'))->format('Y-m-d'.' 00:00:00');
          $row->cpd_ending_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'datetocpd'))->format('Y-m-d'.' 23:59:59');
          $row->course = array_get($input, 'course');

          $row->cpd_hours_req_skills    = array_get($input, 'cpd_hours_req_skills');
          $row->cpd_hours_req_knowledge  = array_get($input, 'cpd_hours_req_knowledge');
          $row->cpd_hours_req_rules_and_regulations   = array_get($input, 'cpd_hours_req_rules_and_regulations');
          $row->cpd_hours_req_gi   = array_get($input, 'cpd_hours_req_gi');
          $row->total_cpd_hours_rules_and_regulations   = array_get($input, 'total_cpd_hours_rules_and_regulations');
          $row->total_cpd_hours_skills   = array_get($input, 'total_cpd_hours_skills');
          $row->total_cpd_hours_knowledge   = array_get($input, 'total_cpd_hours_knowledge');
          $row->total_cpd_hours_gi   = array_get($input, 'total_cpd_hours_gi');
          // $row->total_cpd_hours_final   = array_get($input, 'total_cpd_hours_final');
          $row->total_cpd_hours_final   = ((array_get($input, 'total_cpd_hours_skills') ? : 0) + (array_get($input, 'total_cpd_hours_knowledge') ? : 0) + (array_get($input, 'total_cpd_hours_rules_and_regulations') ? : 0));
          $row->shortfall   = array_get($input, 'short_fall');
          $row->save();
        }

      } else {
        $row->cpd_status      = array_get($input, 'cpd_status');
        $row->cpd_period = array_get($input, 'datefromcpd') . ' - ' . array_get($input, 'datetocpd');
        $row->cpd_starting_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'datefromcpd'))->format('Y-m-d'.' 00:00:00');
        $row->cpd_ending_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'datetocpd'))->format('Y-m-d'.' 23:59:59');
        $row->course = array_get($input, 'course');

        $row->cpd_hours_req_skills    = array_get($input, 'cpd_hours_req_skills');
        $row->cpd_hours_req_knowledge  = array_get($input, 'cpd_hours_req_knowledge');
        $row->cpd_hours_req_rules_and_regulations   = array_get($input, 'cpd_hours_req_rules_and_regulations');
        $row->cpd_hours_req_gi   = array_get($input, 'cpd_hours_req_gi');
        $row->total_cpd_hours_rules_and_regulations   = array_get($input, 'total_cpd_hours_rules_and_regulations');
        $row->total_cpd_hours_skills   = array_get($input, 'total_cpd_hours_skills');
        $row->total_cpd_hours_knowledge   = array_get($input, 'total_cpd_hours_knowledge');
        $row->total_cpd_hours_gi   = array_get($input, 'total_cpd_hours_gi');
        // $row->total_cpd_hours_final   = array_get($input, 'total_cpd_hours_final');
        $row->total_cpd_hours_final   = ((array_get($input, 'total_cpd_hours_skills') ? : 0) + (array_get($input, 'total_cpd_hours_knowledge') ? : 0) + (array_get($input, 'total_cpd_hours_rules_and_regulations') ? : 0));
        $row->shortfall   = array_get($input, 'short_fall');
        $row->save();
      }

      return Response::json(['body' => 'Changes have been saved.']);
    }

      public function viewCPD()
    {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = CPDHours::where('id','=',$id)->first();

        if($row) {
          // dd($row);
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }

         // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
            $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
    }

    public function deleteCPD() {
      $id = Request::input('id');

      if ($id) {
        $row = CPDHours::find($id);
        if ($row) {
          if ($row->status != 2) {

            $row->status = 2;
            $row->save();
            return Response::json(['body' => 'CPD successfully removed.']);

          } else {
            return Response::json(['error' => 'Invalid row specified.']); 
          }
        } else {
          return Response::json(['error' => 'Invalid row specified.']); 
        }
      } else {
        return Response::json(['error' => 'Invalid row specified.']); 
      }

    }

    public function deleteMultipleCPD(){

      $ids = Request::input('ids');

        if (count($ids) == 0) {
          return Response::json(['error' => 'Select cpds first.']); 
        } else {

            // check if auth user is selected
            // foreach ($ids as $id_check) {
            //     $row = User::find($id_check);
            //     if ($row->id == Auth::user()->id) {
            //         return Response::json(['error' => "Access denied."]);
            //     } 
            // }

            // process
          // /dd($ids);
            $deducted_cpd_hours_skills =0;
            $deducted_cpd_hours_knowledge=0;
            $deducted_cpd_both = 0;
            foreach ($ids as $id) {
                $row = CPDHours::find($id);
                // $row_user = User::where('id','=',$row->user_id)->first();
             
             
                //$row_user = User::where('user_id','='$row->user_id)->first();
                if(!is_null($row)) {
                    $status = $row->status;
                    // you cannot remove user that already been remove
                    if ($status != 2){
                      $row->status = 2;

                      $row->save();
                      // $deducted_cpd_hours_skills = ($row_user->cpd_hours_skills)-($row->total_cpd_hours_skills);
                      // $deducted_cpd_hours_knowledge = ($row_user->cpd_hours_knowledge)-($row->total_cpd_hours_knowledge);
                      // $deducted_cpd_both = ($row_user->cpd_hours_both)-($row->total_cpd_hours_final);
                      // $row_user->cpd_hours_skills = $deducted_cpd_hours_skills;
                      // $row_user->cpd_hours_knowledge = $deducted_cpd_hours_knowledge;
                      // $row_user->cpd_hours_both = $deducted_cpd_both;
                      // $row_user->save();

                      //create log
                      //LogUser::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Removed']);
                    }
                    
                }
            }

            return Response::json(['body' => 'Selected users has been Removed.']);
        }
    }


    public function doListCPDManagement(){

      $result['sort'] = Request::input('sort') ?: 'cpd_hours.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
    
      $per = Request::input('per') ?: 10;
     
      if (Request::input('page') != '»') {

        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::join('sales','sales.user_id','=','users.id')
                        ->join('designations','designations.id','=','sales.designation_id')
                        ->leftJoin('sales_advisors','users.id', '=', 'sales_advisors.user_id')
                        ->leftJoin('sales_supervisors', 'sales_advisors.supervisor_id','=','sales_supervisors.id')
                        ->leftJoin('users as supervisors', 'sales_supervisors.user_id', '=',  'supervisors.id')
                        ->leftjoin('cpd_hours','cpd_hours.user_id','=','users.id')
                        ->select('supervisors.name as supervisor_name','sales.user_id','users.name','users.code','sales.sales_id','designations.designation','sales.user_id as sales_user','designations.rank')
                        ->where('users.usertype_id','=',8)
                        ->where('users.status', 1)
                        ->where('cpd_hours.status','!=',2)
                        ->where(function($query) use ($search) {
                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                  ->orWhere('users.code', 'LIKE', '%' . $search . '%');
                            })
                        ->orderBy($result['sort'], $result['order'])
                        ->groupBy('cpd_hours.user_id')
                        ->selectRaw('sum(shortfall) as total_shortfall') 
                        ->selectRaw('sum(total_cpd_hours_skills) as cpd_hours_skills')
                        ->selectRaw('sum(total_cpd_hours_knowledge) as cpd_hours_knowledge')
                        ->selectRaw('sum(total_cpd_hours_rules_and_regulations) as cpd_hours_rules_and_regulations')
                        ->selectRaw('sum(total_cpd_hours_knowledge) + sum(total_cpd_hours_skills) + sum(total_cpd_hours_rules_and_regulations) as cpd_hours_both')
                        ->paginate($per);

      } else {
        $count = User::join('sales','sales.user_id','=','users.id')
                        ->join('designations','designations.id','=','sales.designation_id')
                        ->leftJoin('sales_advisors','users.id', '=', 'sales_advisors.user_id')
                        ->leftJoin('sales_supervisors', 'sales_advisors.supervisor_id','=','sales_supervisors.id')
                        ->leftJoin('users as supervisors', 'sales_supervisors.user_id', '=',  'supervisors.id')
                        ->leftjoin('cpd_hours','cpd_hours.user_id','=','users.id')
                        ->select('supervisors.name as supervisor_name','sales.user_id','users.name','users.code','sales.sales_id','designations.designation','sales.user_id as sales_user','designations.rank')
                        ->where('users.usertype_id','=',8)
                        ->where('users.status', 1)
                        ->where('cpd_hours.status','!=',2)
                        ->where(function($query) use ($search) {
                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                  ->orWhere('users.code', 'LIKE', '%' . $search . '%');
                            })
                        ->orderBy($result['sort'], $result['order'])
                        ->groupBy('cpd_hours.user_id')
                        ->selectRaw('sum(shortfall) as total_shortfall')
                        ->selectRaw('sum(total_cpd_hours_skills) as cpd_hours_skills')
                        ->selectRaw('sum(total_cpd_hours_knowledge) as cpd_hours_knowledge')
                        ->selectRaw('sum(total_cpd_hours_rules_and_regulations) as cpd_hours_rules_and_regulations')
                        ->selectRaw('sum(total_cpd_hours_knowledge) + sum(total_cpd_hours_skills) + sum(total_cpd_hours_rules_and_regulations) as cpd_hours_both')
                        ->paginate($per);
                      

        Paginator::currentPageResolver(function () use ($count) {
          return ceil($count->total() / $per);
        });

        $rows = User::join('sales','sales.user_id','=','users.id')
                        ->join('designations','designations.id','=','sales.designation_id')
                        ->leftJoin('sales_advisors','users.id', '=', 'sales_advisors.user_id')
                        ->leftJoin('sales_supervisors', 'sales_advisors.supervisor_id','=','sales_supervisors.id')
                        ->leftJoin('users as supervisors', 'sales_supervisors.user_id', '=',  'supervisors.id')
                        ->leftjoin('cpd_hours','cpd_hours.user_id','=','users.id')
                        ->select('supervisors.name as supervisor_name','sales.user_id','users.name','users.code','sales.sales_id','designations.designation','sales.user_id as sales_user','designations.rank')
                        ->where('users.usertype_id','=',8)
                        ->where('users.status', 1)
                        ->where('cpd_hours.status','!=',2)
                        ->where(function($query) use ($search) {
                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                  ->orWhere('users.code', 'LIKE', '%' . $search . '%');
                            })
                        ->orderBy($result['sort'], $result['order'])
                        ->groupBy('cpd_hours.user_id')
                        ->selectRaw('sum(shortfall) as total_shortfall')
                        ->selectRaw('sum(total_cpd_hours_skills) as cpd_hours_skills')
                        ->selectRaw('sum(total_cpd_hours_knowledge) as cpd_hours_knowledge')
                        ->selectRaw('sum(total_cpd_hours_rules_and_regulations) as cpd_hours_rules_and_regulations')
                        ->selectRaw('sum(total_cpd_hours_knowledge) + sum(total_cpd_hours_skills) + sum(total_cpd_hours_rules_and_regulations) as cpd_hours_both')
                        ->paginate($per); 
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
    
    public function indexTeamManagement()
    {



      // dd(SalesAdvisor::whereIn('user_id', SalesSupervisor::lists('user_id'))->get());
      $result = $this->doListTeamManagement();

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                                })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();  
      $this->data['rows'] = $result['rows'];
 // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
            $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
      $this->data['noti_count'] = count($this->data['notification']);
      $this->data['title'] = "Team Management";
      $this->data['settings'] = Settings::first();
      $this->data['groups'] = Group::where('status','!=',2)->get();
      return view('user-management.sales-users.team-management.list', $this->data);
    }

    public function doListTeamManagement() {
      // sort and order
      // $result['sort'] = Request::input('s') ?: 'sales.created_at';
      // $result['order'] = Request::input('o') ?: 'desc';

        $rows = Group::select('groups.*', 'users.name')->leftJoin('users', 'groups.user_id', '=', 'users.id')->with('salesSupervisor')->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    public function groupDelete() {

      $input = Request::all();

      if(array_get($input, 'id')) {

        // get the user info
        $row = Group::find(array_get($input, 'id'));

        if(is_null($row)) {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
        // this is an existing row
        $new = false;
        
        $current_group = $row->code;
        $current_user = $row->user_id;

        $rules = [
            'group_id' => 'required',
        ];

        // field name overrides
        $names = [

        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $check_supervisors = SalesSupervisor::where('group_id', array_get($input, 'id'))->count();

        if ($check_supervisors < 2) {
          $row->delete();
          $get_supervisor = SalesSupervisor::where('group_id', array_get($input, 'id'))->first();

          if ($get_supervisor) {
            $get_supervisor->group_id = array_get($input, 'group_id');
            $get_supervisor->save();

            $get_sales_groups_sup = SalesGroup::where('user_id', $get_supervisor->user_id)->first();

            if ($get_sales_groups_sup) {
              $get_sales_groups_sup->user_id = $current_user;
              $get_sales_groups_sup->supervisor_id = $get_supervisor->id;
              $get_sales_groups_sup->group_id = array_get($input, 'group_id');
              $get_sales_groups_sup->save();
              $get_sales_groups_sup = SalesGroup::where('supervisor_id', $get_supervisor->id)->update(['group_id' => array_get($input, 'group_id')]);
            } else {
              $get_sales_groups_sup = new SalesGroup;
              $get_sales_groups_sup->user_id = $current_user;
              $get_sales_groups_sup->supervisor_id = $get_supervisor->id;
              $get_sales_groups_sup->group_id = array_get($input, 'group_id');
              $get_sales_groups_sup->save();
              $get_sales_groups_sup = SalesGroup::where('supervisor_id', $get_supervisor->id)->update(['group_id' => array_get($input, 'group_id')]);
            }

            $check_group = Group::find(array_get($input, 'group_id'));
            $check_supervisor = SalesSupervisor::find($get_supervisor->id);

            $log_team = new LogTeam;
            $log_team->user_id = $current_user;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Deleted group (' . $current_group . ') and supervisor (' . ($check_supervisor ? $check_supervisor->unit_code : '') . ')  relocated to group (' . ($check_group ? $check_group->code : '') . ').';
            $log_team->save();

            // return
            return Response::json(['body' => 'Changes has been saved.']);
          } else {
            return Response::json(['error' => "The requested item was not found in the database."]);
          }

        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }

      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

    }

    public function getGroupDelete() {

      if (Request::input('id')) {
        
        $check_group = Group::find(Request::input('id'));
      
        $this->data['group_id'] = '<option value="" class="hide">Select</option>';
        $this->data['title'] = 'Group Deletion';
        $this->data['id'] = Request::input('id');

        if (!$check_group) {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }

        $this->data['title'] = 'Group Deletion (' . $check_group->code . ')';

        $get_user = User::find($check_group->user_id);
        $get_supervisor = SalesSupervisor::where('user_id', $check_group->user_id)->first();

        if ($get_user && $get_supervisor) {
          $this->data['title'] .= ' Relocation (' . $get_supervisor->unit_code . ') - ' . $get_user->name;
        }

        $get_groups = Group::where('id', '!=', Request::input('id'))->get();

        foreach ($get_groups as $ggkey => $ggfield) {
          $this->data['group_id'] .= '<option value="' . $ggfield->id . '">' . $ggfield->code . '</option>';
        }
        
        return Response::json($this->data);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function getDemoteManagerCode() {

      if (Request::input('id') && Request::input('super_id')) {
        
        $rows = '<option value="" class="hide">Select</option>';

        $get_supervisors = SalesSupervisor::where('group_id', Request::input('id'))->where('id', '!=', Request::input('super_id'))->get();

        foreach ($get_supervisors as $gskey => $gsfield) {
          $rows .= '<option value="' . $gsfield->id . '">' . $gsfield->unit_code . '</option>';
        }
        
        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

    }

    public function getDemoteCode() {

      if (Request::input('id')) {
        
        $this->data['designation'] = '<option value="" class="hide">Select</option>';
        $this->data['supervisor_id'] = '<option value="" class="hide">Select</option>';
        $this->data['group_id'] = '<option value="" class="hide">Select</option>';
        $this->data['id'] = '';
        $this->data['title'] = 'Demotion';

        $check_supervisor = SalesSupervisor::find(Request::input('id'));

        if ($check_supervisor) {
          $this->data['id'] = $check_supervisor->id;

          $check_user = User::find($check_supervisor->user_id);

          if ($check_user) {
            $this->data['title'] = 'Demotion and Deletion <b>(' . $check_supervisor->unit_code . ' - ' . $check_user->name . ')</b>';
          }

          $get_groups = Group::where('status', 1)->get();

          foreach ($get_groups as $ggkey => $ggfield) {
            $this->data['group_id'] .= '<option value="' . $ggfield->id . '" ' . ($ggfield->id == $check_supervisor->group_id ? 'selected' : '') . '>' . $ggfield->code . '</option>';
          }

          $get_supervisors = SalesSupervisor::where('group_id', $check_supervisor->group_id)->where('id', '!=', Request::input('id'))->get();

          foreach ($get_supervisors as $gskey => $gsfield) {
            $this->data['supervisor_id'] .= '<option value="' . $gsfield->id . '">' . $gsfield->unit_code . '</option>';
          }

          $get_designations = Designation::where('rank', 'Advisor')->get();

          foreach ($get_designations as $gdkey => $gdfield) {
            $this->data['designation'] .= '<option value="' . $gdfield->id . '">' . $gdfield->designation . '</option>';
          }
        }

        return Response::json($this->data);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function getGroupSupervisor() {
      
      $id = Request::input('id');

      $rows = '<div class="item-table nos"><table class="table table-striped nos">' .
              '<thead>' .
              '<th class="tbheader"><i ></i> Manager Code</th>' .
              '<th class="tbheader"><i ></i> Agent Serial No</th>' .
              '<th class="tbheader"><i ></i> Supervisor Name</th>' .
              '<th class="tbheader"><i ></i> Designation</th>' .
              '<th class="tbheader text-right"><i ></i> Tools</th>' .
              '</thead><tbody>';

      $get_group = Group::find($id);
      $group_users = [];

      if ($get_group) {
        $get_supervisor = SalesSupervisor::where('group_id', '=', $id)
                          ->where('user_id', '!=', $get_group->user_id)
                          ->orderBy('unit_code', 'asc')
                          ->lists('user_id');

        $group_users[0] = $get_group->user_id;
        $users = array_merge($group_users, $get_supervisor->toArray());

        foreach ($users as $key => $value) {

            $get_name = User::with('salesInfo.designations')->where('id', '=', $value)->first();

            $field = SalesSupervisor::where('user_id', $value)->first();

            $check_owner = Group::where('user_id', $value)->first();

            if ($field) {
            $check_advisors = SalesAdvisor::where('supervisor_id', $field->id)->count();

              $rows .= '<tr ' . ($value == $get_group->user_id ? 'class="info"' : '' ) . ' data-id="' . $field->id . '">' .

              '<td><a class="btn btn-table btn-xs btn-item-expand" data-user="'. $value . '"><i class="fa fa-plus"></i></a> ' . $field->unit_code . '</td>' .
              '<td>' .$get_name->code .  '</td>' .
              '<td>' . $get_name->name . '</td>' .
              '<td>' . $get_name->salesInfo->designations->designation . '</td>' .
              '<td class="text-right">'.
                  '<button class="btn btn-default borderzero btn-xs btn-table btn-edit_code" title="Edit Manager Code"><i class="fa fa-pencil"></i></button>'.
                  (count($get_supervisor) > 0 ?
                  (!$check_owner ? 
                    '&nbsp;<button class="btn btn-default borderzero btn-xs btn-table btn-relocate" title="Relocate"><i class="fa fa-retweet"></i></button>' . 
                      ($check_advisors == 0 ?
                        '&nbsp;<button class="btn btn-default borderzero btn-xs btn-table btn-demote" title="Demote and Delete"><i class="fa fa-arrow-down"></i></button>'
                      : '') 
                     : '')
                  : '') .
              '</td>' .
              '</tr>';
            }

        }
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getGroupAdvisor() {
      
      $id = Request::input('id');
      $user = Request::input('user');

      $rows = '<div class="item-table nos"><table class="table table-striped nos">' .
              '<thead>' .
              '<th class="tbheader"><i ></i> Agent Serial No</th>' .
              '<th class="tbheader"><i ></i> Agent Name</th>' .
              '<th class="tbheader"><i ></i> Designation</th>' .
              '<th class="tbheader text-right"><i ></i> Tools</th>' .
              '</thead><tbody>';

      $get_super = SalesSupervisor::find($id);

      if ($get_super) {

        $get_advisor = SalesAdvisor::where('supervisor_id', '=', $id)->get();

        foreach ($get_advisor as $key => $field) {
          $get_name = User::with('salesInfo.designations')->where('id', '=', $field->user_id)->first();
          $rows .= '<tr data-id = '.$field->id.'>' .
          '<td>' . $get_super->unit_code . '-' . $get_name->code .  '</td>' .
          '<td>' . $get_name->name . '</td>' .
          '<td>' . $get_name->salesInfo->designations->designation . '</td>' .
          '<td class="text-right">'.
              '<button class="btn btn-default borderzero btn-xs btn-table btn-promote-advisor" title="Promote Advisor"><i class="fa fa-arrow-up"></i></button>'.
              '&nbsp;<button class="btn btn-default borderzero btn-xs btn-table btn-relocate-advisor" title="Relocate Advisor"><i class="fa fa-retweet"></i></button>'.
          '</td>' .
          '</tr>';
        }
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function removeAdvisor(){
     
    }

    public function editAdvisor(){
      $id = Request::input('id');

      $row = SalesAdvisor::find($id);

      $this->data['row'] = null;
      $this->data['title'] = null;
      $this->data['groups'] = '<option class="hide" value="">Select</option>';
      $this->data['designation_id'] = '<option class="hide" value="">Select</option>';

      if($row) {
        $get_supervisor = SalesSupervisor::find($row->supervisor_id);

        $get_user = User::find($row->user_id);
        
        if ($get_user) {
          $this->data['title'] = 'Promotion <b>(' . $get_user->name . ')</b>';
        }

        if($get_supervisor) {

          $get_group = Group::where('status', 1)->get();

          foreach ($get_group as $key => $field) {
            $this->data['groups'] .= '<option value="' . $field->id . '"' . ( $field->id == $get_supervisor->group_id ? ' selected' : '') . '>' . $field->code . '</option>';
          }

          $get_designations = Designation::where('rank', 'Supervisor')->get();

          foreach ($get_designations as $gdkey => $gdfield) {
            $this->data['designation_id'] .= '<option value="' . $gdfield->id . '">' . $gdfield->designation . '</option>';
          }


          $this->data['row'] = $row;
          return Response::json($this->data);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
      } else {
          return Response::json(['error' => "Invalid row specified"]);
      }
      
    }

    public function editSupervisor() {
      $id = Request::input('id');

      $row = SalesSupervisor::find($id);
      // dd($row);
      if($row) {
          return Response::json($row);
      } else {
          return Response::json(['error' => "Invalid row specified"]);
      }
    }

    public function editDirectorCode(){

        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Group::select('groups.user_id as owner', 'groups.id', 'groups.code', 'sales_supervisors.unit_code as manager_code')
                      ->leftJoin('sales_supervisors', 'sales_supervisors.user_id', '=', 'groups.user_id')
                      ->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Display a listing of the incomplete.
     *
     * @return Response
     */

    
    public function indexIncomplete()
    {
        $this->data['title'] = "Incomplete";
        return view('user-management.sales-users.team-management.list', $this->data);
    }

    /**
     * Display a listing of the duplicate.
     *
     * @return Response
     */

    
    public function indexDuplicate()
    {
        $this->data['title'] = "Duplicate";
        return view('user-management.sales-users.team-management.list', $this->data);
    }
    
    /**
     * Display a listing of the logs.
     *
     * @return Response
     */

    
    public function indexLog()
    {
        $result = $this->doListLog();
        $this->data['pages'] = $result['pages'];
        $this->data['rows'] = $result['rows'];

        $this->data['refresh_route'] = url('sales/refresh/log');
        $this->data['settings'] = Settings::all();
        $this->data['designations'] = Designation::all();
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
         // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
            $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        $this->data['settings'] = Settings::first();
        $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Sales User Log";
        return view('user-management.sales-users.log.list', $this->data);
    }

    /**
     * Display a listing of the sales users.
     *
     * @return Response
     */
    public function doListLog() {
        // sort and order
        $result['sort'] = Request::input('sort') ?: 'log_sales.created_at';
        $result['order'] = Request::input('order') ?: 'desc';

        /*$rows = LogSales::select('log_sales.activity', 'log_sales.created_at', 'designations.*', 'users1.name as agent_name', 'users1.system_id as agent_id', 'users1.code as agent_code', 'users2.name as edited_by')
                    ->join('users as users1', 'users1.id', '=', 'log_sales.user_id')
                    ->join('users as users2', 'users2.id', '=', 'log_sales.edit_id')
                    ->join('designations', 'users.id', '=', 'designations.user_id')
                    ->where('users1.usertype_id', '=', '8')
                    ->orderBy($result['sort'], $result['order'])
                    ->get();*/

        // $rows = LogSales::with('salesLog')
        //                 ->with('editSalesLog')
        //                 ->with('salesLog.salesInfo')
        //                 ->orderBy($result['sort'], $result['order'])
        //                 ->get();
       

         $per = Request::input('per') ?: 10;
         
            if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
              });


              $rows = LogSales::join('users','users.id','=','log_sales.user_id')
                              ->join('users as editing_user','editing_user.id','=','log_sales.edit_id')
                              ->join('sales','sales.user_id','=','log_sales.user_id')
                              ->select('users.name','users.code','log_sales.activity','log_sales.created_at','log_sales.edit_id','sales.sales_id','editing_user.name as edit_name')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
                               //dd($rows);
            } else {     
              $count = LogSales::join('users','users.id','=','log_sales.user_id')
                              ->join('users as editing_user','editing_user.id','=','log_sales.edit_id')
                              ->join('sales','sales.user_id','=','log_sales.user_id')
                              ->select('users.name','users.code','log_sales.activity','log_sales.created_at','log_sales.edit_id','sales.sales_id','editing_user.name as edit_name')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);

                       Paginator::currentPageResolver(function () use ($count, $per) {
                       return ceil($count->total() / $per);
                       });

             $rows = LogSales::join('users','users.id','=','log_sales.user_id')
                              ->join('users as editing_user','editing_user.id','=','log_sales.edit_id')
                              ->join('sales','sales.user_id','=','log_sales.user_id')
                              ->select('users.name','users.code','log_sales.activity','log_sales.created_at','log_sales.edit_id','sales.sales_id','editing_user.name as edit_name')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
           }

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }


    public function getGroup() {
      $result['get_rank'] = Designation::find(Request::input('id'));
      $result['get_group'] = Group::where('status', '=', 1)->get();

      return Response::json($result);
    }

    public function getTierRate() {
      
      $result['get_tier'] = Tier::find(Request::input('id'));

      return Response::json($result);
    }

     public function getDesignationRate() {
      
      $result['get_rate'] = DesignationRates::find(Request::input('id'));
      return Response::json($result);
    }
    public function getUnit() {

      $get_units = SalesSupervisor::where('group_id', '=', Request::input('id'))->get();
      $rows = '<option class="hide" value="">Select:</option>';

      foreach ($get_units as $key => $field) {
        $rows .= '<option value="' . $field->id . '">' . $field->unit_code . '</option>';
      }
      return Response::json($rows);
    }


    /**
    * Save changes to a Sales User
    *
    * @return Response
    */
    public function save() {
        // assume this is a new row
        $new = true;

        $input = Input::all();

        $pass_score = 0;

        // dd($input);

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = User::where('status', '!=', 2)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error_prompt' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        if(!$new)
        {
          if(Input::get('password'))
          {
            if (Input::get('pass_score')) {
                $pass_score = Input::get('pass_score');
            }
            $password = Input::get('password');
                if(preg_match("/^(?=.*[A-Z])(?=.*\d)([0-9a-zA-Z]+)$/", $password) == 0 && preg_match('`[A-Z]`', $password) && preg_match('`[0-9]`', $password)) {

                } else {
                  return Response::json(['error' => ["error-pass_score|Password must contain atleast one uppercase, lowercase, numeric and special character."]]);
                }
          }
        }


        $rules = [
            'photo' => $new ? 'image|max:3000' : 'image|max:3000',
            'code'  =>  'required|unique:users,code' . (!$new ? ',' . $row->id : ''),
            'name' => 'required|max:100',
            'preferred_name' => 'required',
            'password' => $new ? '' : 'min:8|confirmed' . (array_get($input, 'password_confirmation') ? '|required' : ''),
            'email'  => 'required|email|unique:users,email' . (!$new ? ',' . $row->id : ''),
            'bonding_rate' => $new ? 'required|numeric' : '',
            'gender'  => 'required',
            //'unit_code' => $new ? 'unique:sales_supervisors,unit_code' : '',
            'date_of_birth' => 'date',
            'nric' => 'required',
            'salesforce_id' => 'required',
            'agent_serial_no' => 'required',
            'banding_rank' => 'required',
            // 'gi_banding_rank' => 'required',
            'banding_effective_date' => 'date',
            'designation_effective_date' => 'date',
        ];

        if ($new && array_get($input, 'group_code')) {
          $get_des = Designation::find(array_get($input, 'usertype_id'));
          if ($get_des->rank == "Supervisor") {
            $rules['unit_code'] = 'required|unique:sales_supervisors,unit_code';
          } else if ($get_des->rank == "Advisor") {
            $rules['unit_id'] = 'required';
          }
        } else if ($new && array_get($input, 'owner')) {
          $rules['group_code_owner'] = 'required|min:2|max:100';
          $rules['unit_code'] = 'required|min:2|max:100';
        }



        // if (array_get($input, 'unit_code')) {
        //   $check_unit_code = SalesSupervisor::lists('unit_code');

        //   foreach ($check_unit_code->toArray() as $key => $value) {
        //     if (array_get($input, 'unit_code') == $value) {
        //       $errors = ["The unit code field must be unique."];
        //     }
        //   }
        // }
        /*
        if ($new && array_get($input, 'usertype_id') != 6) {
            $rules['supervisor_id'] = 'required';
        }*/

        //dd($input);
        // if(strlen(array_get($input, 'mgt_email') ) > 0){
        //   ;

        // }
        //dd(strlen(array_get($input, 'google_account')));
        //do not require type if user is editing self
        if($new || ($new == false && $row->id != Auth::user()->id)) {
            $rules['usertype_id'] = 'required|exists:designations,id';
        }

        // field name overrides
        $names = [
            // 'name' => 'agent name',
            // 'usertype_id' => 'designation',
            // 'code' => 'agent code',
            // 'supervisor_id' => 'supervisor',
            // 'company_email_address_new' => 'new company email address',
            // 'mgt_email' => 'management email address',
            // 'uin_no' => 'UIN No',
            // 'house_no' => 'House No',
            // 'be_code' => 'BE Code',
            // 'days_since_contracted' => 'Days Since Contracted',
            // 'postal_code' => 'Postal Code',
            // 'nric' => 'NRIC',
            // 'salesforce_id' => 'Salesforce ID',
            // 'agent_serial_no' => 'Agent Serial No',
            // 'rnf_date' => 'RNF Date',
            // 'contracted_date' => 'Contracted Date',
            // 'off_boarding_date' => 'OFF Bording Date', 
        ];

        $product_id_nos = array();
        $product_id = Input::get('provider_id');
        $sum_assured = Input::get('provider_code');
        $classification_id = Input::get('classification_id');
     //dd($classification_id);
      //validate child rows
      // if($classification_id) {
      //     foreach($classification_id as $key => $val) {
      //         if($classification_id[$key]) {
      //             foreach($this->request->get('sum_assured') as $key => $val)
      //             {
      //               $subrules['sum_assured.'.$key] = 'unique:provider_codes,provider_code';
      //             }
                  

      //             $subfn = array(    
      //             );

      //             // if($sum_assured[$key]) {
      //             //     //$subrules["product_id.$key"] = 'exists:production_case_contacts,id';
      //             //     $subrules["sum_assured.$key"] = 'unique:provider_codes,provider_code';
      //             // }

      //             $rules = array_merge($rules, $subrules);
      //             $names = array_merge($names, $subfn);
      //             $product_id_nos[] = $classification_id[$key];
      //         } else {
      //             unset($product_id[$key]);
      //             unset($classification_id[$key]);
      //         }
      //     }
      // }

        if (!$new) {

          if(Input::get('password'))
          {
            if ($pass_score <= 50) {

              return Response::json(['error' => ["error-pass_score|*Password must be at least <b><u>MEDIUM</u></b>"]]);
            } else {
              if(preg_match("/^(?=.*[A-Z])(?=.*\d)([0-9a-zA-Z]+)$/", $password) == 0 && preg_match('`[A-Z]`', $password) && preg_match('`[0-9]`', $password)) { 

              } else {
                return Response::json(['error' => ["error-pass_score|*Password must contain at least one uppercase, lowercase, numeric and special character."]]);
              }
            }
          }
        }

        // else {
        //   if ($pass_score <= 50 && (strlen($password) > 0)) {
        //     return Response::json(['error' => ["error-pass_score|*Password must be at least <b><u>MEDIUM</u></b>"]]);
        //   } else if (strlen($password) > 0) {
        //     if(preg_match("/^(?=.*[A-Z])(?=.*\d)([0-9a-zA-Z]+)$/", $password) == 0 && preg_match('`[A-Z]`', $password) && preg_match('`[0-9]`', $password)) { 

        //     } else { 
        //       return Response::json(['error' => ["error-pass_score|*Password must contain at least one uppercase, lowercase, numeric and special character."]]);
        //     }
        //   }
        // }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $log_code = "";
        $log_name = "";
        $log_email = "";
        $log_mobile = "";
        $log_username = "";
        $log_password = "";


        // create model if new
        if($new) {
          $row = new User;
          $row->code      = array_get($input, 'code');
          $row->name      = strip_tags(array_get($input, 'name'));
          $row->preferred_name = array_get($input, 'preferred_name');
          $row->email     = array_get($input, 'email');
          $row->username  = array_get($input, 'email');
          $row->gender   = array_get($input, 'gender');
          $row->mobile   = array_get($input, 'mobile');
        } else {
          $log_code         = $row->code;
          $log_name         = $row->name;
          $log_email        = $row->email;
          $log_mobile       = $row->mobile;
          $log_username     = $row->username;
          $log_migrant      = $row->migrant;
          $log_gender       = $row->gender;
          $log_race         = $row->race;
          $log_nationality  = $row->nationality;
          $log_marital_status = $row->marital_status;
          $log_preferred_name = $row->preferred_name;
        
          $row->code         = array_get($input, 'code');
          $row->name         = strip_tags(array_get($input, 'name'));
          $row->email        = array_get($input, 'email');
          $row->username     = array_get($input, 'email');
          $row->preferred_name  = array_get($input, 'preferred_name');
          $row->mobile   = array_get($input, 'mobile');
          $row->gender   = array_get($input, 'gender');

          $row->save();
        }
        if($new) {
          $row->usertype_id = 8;
        }
         
        /* set type only if this isn't me
        if($new || ($new = false && $row->id != Auth::user()->id)) {
          $row->usertype_id = 7;
        }*/


        // do not change password if old user and field is empty
        if($new) {
          $nric_first = strtolower(substr(array_get($input, 'nric'), 0, 1));         
          $nric_middle = preg_replace('/[^0-9]/', '', array_get($input, 'nric'));
          $nric_last = strtoupper(substr(array_get($input, 'nric'), -1)).'!';
          $new_password = $nric_first.$nric_middle.$nric_last;
          $row->password = Hash::make($new_password);
        }
        else if($new == false && array_get($input, 'password'))
        {
          $row->password = Hash::make(array_get($input, 'password'));
        }
        else
        {
          // nahhhh
        }
        
        // save model
        $row->save();

        if (Request::hasFile('photo')) {
            // Save the photo
            Image::make(Request::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
            $row->photo = $row->id . '.jpg';
            $row->save();
        } else{
          //sets the default photo
          if(array_get($input, 'image_fb_link_edit_photo'))
          {
              $img = file_get_contents(array_get($input, 'image_fb_link_edit_photo'));
              Image::make($img)->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
              $row->photo = $row->id . '.jpg';
              $row->save();

          }else{
            if($new){
              $photo = Settings::first();
              $row->photo = $photo->default_photo;
              $row->save();
            }
          }
        }  


        // create agent info if new
        if($new) {
          $sales = new Sales;
          $sales->designation_id = array_get($input, 'usertype_id');
          $sales->user_id = $row->id;
          $sales->bonding_rate = array_get($input, 'bonding_rate');
          $sales->designation_rate = array_get($input, 'designation_rate');

          if (array_get($input, 'gi_banding_rank')) {
            $sales->gi_banding_rank = array_get($input, "gi_banding_rank");
          } else {
            $sales->gi_banding_rank = null;
          }

          // save model
          $sales->save();

          // create sales id
          $sales->sales_id = 'SALES-' . sprintf("%04d", $sales->id);
          $sales->save();

          $get_rank = Designation::find(array_get($input, 'usertype_id'));

          if (array_get($input, 'group_code') || array_get($input, 'owner')) {
            if ($get_rank->rank == "Supervisor") {
              $group_code = array_get($input, 'group_code');

              if (array_get($input, 'owner')) {
                $new_group = new Group;
                $new_group->code = array_get($input, 'group_code_owner');
                $new_group->user_id = $row->id;
                $new_group->save();
                $group_code = $new_group->id;
              }

              $group = new SalesSupervisor;
              $group->group_id = $group_code;
              $group->unit_code = array_get($input, 'unit_code');
              $group->user_id = $row->id;
              $group->save();

              $sales_group = new SalesGroup;
              $sales_group->user_id = $row->id;
              $sales_group->supervisor_id = $group->id;
              $sales_group->group_id = $group_code;
              $sales_group->save();

            } else if ($get_rank->rank == "Advisor") {
              $group = new SalesAdvisor;
              $group->supervisor_id = array_get($input, 'unit_id');
              $group->user_id = $row->id;
              $group->save();

              $new_sales = new SalesGroup;
              $new_sales->user_id = $row->id;
              $new_sales->supervisor_id = array_get($input, 'unit_id');
              $new_sales->group_id = array_get($input, 'group_code');
              $new_sales->save();
            }
          }
        
          $add_des = new SalesDesignation;
          $add_des->effective_date = $sales->created_at;
          $add_des->designation_id = $sales->designation_id;
          $add_des->prev_designation_id = $sales->designation_id;
          $add_des->sales_id = $sales->id;
          $add_des->rate = array_get($input,'designation_rate');
          $add_des->edit_id = Auth::user()->id;
          $add_des->save();

          $add_sales_user_info = new SalesUser;
          $add_sales_user_info->user_id = $row->id;
          $add_sales_user_info->nric = array_get($input, 'nric');
          $add_sales_user_info->banding_rank = array_get($input, 'banding_rank');
          $add_sales_user_info->salesforce_id = array_get($input, 'salesforce_id');
          $add_sales_user_info->salutation = array_get($input, 'salutation');
          $add_sales_user_info->home_address = array_get($input, 'home_address');

          if(array_get($input, 'date_of_birth')) {
            $add_sales_user_info->date_of_birth = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
          } else {
            $add_sales_user_info->date_of_birth = null;
          }

          $add_sales_user_info->postal_code = array_get($input, 'postal_code');  
          $add_sales_user_info->advisors_mobile = array_get($input, 'advisors_mobile');
          $add_sales_user_info->advisors_office_mobile = array_get($input, 'advisors_office_mobile');
          $add_sales_user_info->designation_updated = array_get($input, 'designation_updated');
          $add_sales_user_info->designation_old = array_get($input, 'designation_old');
          $add_sales_user_info->latest_ytd_production = array_get($input, 'latest_ytd_production');
          $add_sales_user_info->smartermail_username = array_get($input, 'smartermail_username');
          $add_sales_user_info->agent_serial_no = array_get($input, 'agent_serial_no');
          $add_sales_user_info->representative_type = array_get($input, 'representative_type');

          if(array_get($input, 'contracted_date')){
            $add_sales_user_info->contracted_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'contracted_date'));
          }else{
            $add_sales_user_info->contracted_date = null;
          }
          if(array_get($input, 'rnf_date')){
             $add_sales_user_info->rnf_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'rnf_date'));
          }else{
             $add_sales_user_info->rnf_date = null;
          }
          if(array_get($input, 'off_boarding_date')){
             $add_sales_user_info->off_boarding_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'off_boarding_date'));
          }else{
             $add_sales_user_info->off_boarding_date = null;
          }
         
          $add_sales_user_info->notes = array_get($input, 'notes');
          $add_sales_user_info->save();

          if(!is_null($classification_id)){
            $ids = [];

            foreach ($classification_id as $key => $value) {
              $row_provider = new ProviderCodes;
              $row_provider->provider_code = $sum_assured[$key];
              $row_provider->classification_id = $classification_id[$key];
              $row_provider->user_id = $row->id;
              $row_provider->provider_id = $product_id[$key];
              $row_provider->save();
              $ids[$row->id] = $row->id;
            }
         }
            
          $add_bond = new SalesBonding;
          $add_bond->bonding_rate = array_get($input, 'bonding_rate');
          $add_bond->prev_bonding_rate = array_get($input, 'bonding_rate');

          if (array_get($input,'banding_effective_date')) {
            $add_bond->effective_date = Carbon::createFromFormat('m/d/Y', array_get($input,'banding_effective_date'));
          } else {
            $add_bond->effective_date = null;
          }
          
          $add_bond->sales_id = $sales->id;
          $add_bond->edit_id = Auth::user()->id;
          $add_bond->save();

          //create log
          LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Sales User Created']);

        } else {

          $add_sales_user_info = SalesUser::where('user_id','=',array_get($input, 'id'))->first();
          
          if (!$add_sales_user_info) {
            $add_sales_user_info = new SalesUser;
          }

          $add_sales_user_info->user_id = $row->id;
          $add_sales_user_info->nric = array_get($input, 'nric');
          $add_sales_user_info->banding_rank = array_get($input, 'banding_rank');
          $add_sales_user_info->salutation = array_get($input, 'salutation');
          $add_sales_user_info->salesforce_id = array_get($input, 'salesforce_id');
          $add_sales_user_info->home_address = array_get($input, 'home_address');
          if(array_get($input, 'date_of_birth')) {
            $add_sales_user_info->date_of_birth = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
          } else {
            $add_sales_user_info->date_of_birth = null;
          }

          $add_sales_user_info->postal_code = array_get($input, 'postal_code');  
          $add_sales_user_info->advisors_mobile = array_get($input, 'advisors_mobile');
          $add_sales_user_info->advisors_office_mobile = array_get($input, 'advisors_office_mobile');
          $add_sales_user_info->designation_updated = array_get($input, 'designation_updated');
          $add_sales_user_info->designation_old = array_get($input, 'designation_old');
          $add_sales_user_info->latest_ytd_production = array_get($input, 'latest_ytd_production');
          $add_sales_user_info->smartermail_username = array_get($input, 'smartermail_username');
          $add_sales_user_info->agent_serial_no = array_get($input, 'agent_serial_no');
          $add_sales_user_info->representative_type = array_get($input, 'representative_type');

          if(array_get($input, 'contracted_date')){
            $add_sales_user_info->contracted_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'contracted_date'));
          }else{
            $add_sales_user_info->contracted_date = null;
          }
          if(array_get($input, 'rnf_date')){
             $add_sales_user_info->rnf_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'rnf_date'));
          }else{
             $add_sales_user_info->rnf_date = null;
          }
          if(array_get($input, 'off_boarding_date')){
             $add_sales_user_info->off_boarding_date = Carbon::createFromFormat('m/d/Y', array_get($input, 'off_boarding_date'));
          }else{
             $add_sales_user_info->off_boarding_date = null;
          }

          $add_sales_user_info->notes = array_get($input, 'notes');
          $add_sales_user_info->save();

       
        $get_designation = Sales::where('user_id','=',array_get($input, 'id'))->first();
  
         $get_rank = Designation::where('id','=',$get_designation->designation_id)->first();


         /*
          if (array_get($input, 'group_code')) {
            if ($get_rank->rank == "Supervisor") {

              $group = SalesSupervisor::where('user_id','=',array_get($input, 'id'))->first();
              
              if($group == null){
                  $group_supervisor = new SalesSupervisor;
                  $group_supervisor->group_id = array_get($input, 'group_code');
                  $group_supervisor->unit_code = array_get($input, 'unit_code');
                  $group_supervisor->user_id = $row->id;
                  $group_supervisor->save();
                  $get_owner = Group::where('id','=',$group_supervisor->group_id)->first();
                if(strtolower(array_get($input, 'unit_code')) == strtolower($get_owner->code)){
                  $get_owner->user_id = $row->id;
                  $get_owner->save();
                }
              }else{
                  $group->group_id = array_get($input, 'group_code');
                  $group->unit_code = array_get($input, 'unit_code');
                  $group->user_id = $row->id;
                  $group->save();
                  $get_owner = Group::where('id','=',$group->group_id)->first();
                if(strtolower(array_get($input, 'unit_code')) == strtolower($get_owner->code)){
                  $get_owner->user_id = $row->id;
                  $get_owner->save();
                }
              }
             
            } else if ($get_rank->rank == "Advisor") {
      

              $group = SalesAdvisor::where('user_id','=',array_get($input, 'id'))->first();
             
              if($group == null){

                 $group_advisor = new SalesAdvisor;   
                 $group_advisor->supervisor_id = array_get($input, 'unit_id');
                 $group_advisor->user_id = $row->id;
                 $group_advisor->save();

              }else{

                
                 //dd(array_get($input, 'unit_id'));
                 $group->supervisor_id = array_get($input, 'unit_id');
                 $group->user_id = $row->id;
                 $group->save();
              }
          


            }
          } */

     
      // $get_provider_codes = ProviderCodes::where('user_id','=',array_get($input, 'id'))->get();

      // if(is_null($get_provider_codes)){

      //       $get_provider = Provider::all();
      //       foreach($get_provider as $rows){

      //           $provider = new ProviderCodes;
      //           $provider->provider_code = array_get($input,'provider_code_'.strtolower($rows->name));
      //           $provider->user_id = $row->id;
      //           $provider->provider_id = array_get($input,'provider_id_'.strtolower($rows->name));
      //           $provider->save();
      //         }

      // }else{
      //  if(!is_null($product_id)){
      // $ids = [];
      //   foreach ($product_id as $key => $value) {
      //     $row_provider = ProviderCodes::find($key);
  
      //     if ($row_provider) {
      //       $row_provider->provider_code = $sum_assured[$key];
      //       $row_provider->user_id = array_get($input, 'id');
      //       $row_provider->provider_id = $product_id[$key];
      //       $row_provider->save();
      //     }
      //     $ids[$row_provider->id] = $row_provider->id;
      //   }
      //   }
      // }
         // dd($classification_id);
          if(!is_null($classification_id)){
               $ids = [];

            foreach ($classification_id as $key => $value) {
              $row_provider = ProviderCodes::where('classification_id','=',$classification_id[$key])->where('provider_id','=',$product_id[$key])->where('user_id','=',array_get($input,'id'))->first();

                if (is_null($row_provider)) {

                $row_provider = new ProviderCodes;
                $row_provider->user_id = array_get($input,'id');
                }
                $row_provider->classification_id = $classification_id[$key];
                $row_provider->provider_code = $sum_assured[$key];
                $row_provider->provider_id = $product_id[$key];
                $row_provider->save();  
              $ids[$row->id] = $row->id;
            }
          }
        if(array_get($input,'usertype_id')){ 
          $sales = Sales::where('user_id','=',array_get($input, 'id'))->first();
          $add_sales_desc = new SalesDesignation;
          $add_sales_desc->designation_id = array_get($input,'usertype_id');
          $add_sales_desc->sales_id = $sales->id;
          $add_sales_desc->prev_designation_id = $sales->designation_id;
          $add_sales_desc->rate = array_get($input,'designation_rate');

          if (array_get($input,'designation_effective_date')) {
            $add_sales_desc->effective_date = Carbon::createFromFormat('m/d/Y', array_get($input,'designation_effective_date'));
          } else {
            $add_sales_desc->effective_date = null;
          }

          $add_sales_desc->edit_id = Auth::user()->id;
          $add_sales_desc->save();
          $sales->designation_id = array_get($input,'usertype_id');
          $sales->designation_rate = array_get($input, 'designation_rate');

          $sales->save();
        }    

        if(array_get($input,'bonding_rate')){ 
          $sales = Sales::where('user_id','=',array_get($input, 'id'))->first();
          $add_bonding = new SalesBonding;
          $add_bonding->bonding_rate = array_get($input, 'bonding_rate');
          $add_bonding->sales_id = $sales->id;

          if (array_get($input,'banding_effective_date')) {
            $add_bonding->effective_date = Carbon::createFromFormat('m/d/Y', array_get($input,'banding_effective_date'));
          } else {
            $add_bonding->effective_date = null;
          }

          $add_bonding->prev_bonding_rate = $sales->bonding_rate;
          $add_bonding->edit_id = Auth::user()->id;
          $add_bonding->save();

          $sales->bonding_rate = array_get($input, 'bonding_rate');
          $sales->save();
        }


          $sales_save = Sales::where('user_id','=',array_get($input, 'id'))->first();

          if (array_get($input, 'gi_banding_rank')) {
            $sales_save->gi_banding_rank = array_get($input, "gi_banding_rank");
          } else {
            $sales_save->gi_banding_rank = null;
          }

          $sales_save->save();

     // if($product_id_nos) {
     //        foreach($product_id_nos as $key => $child) {

     //            if($product_id[$key]) {
     //                // this is an existing visa
     //                $pinfo = ProviderCodes::find($product_id[$key]);
     //            } else {
     //                // this is a new visa
     //                $pinfo = new ProviderCodes;
     //                $pinfo->user_id = $row->id;
     //            }

     //            $pinfo->provider_id = $product_id[$key];
     //            $pinfo->provider_code = $sum_assured[$key];
     //            $pinfo->save();
     //        }
     //    }

             // $get_provider = ProviderCodes::with('getproviderinfo')->where('user_id','=',array_get($input,'id'))->get();
              
             //  foreach ($get_provider as $key => $row) {
           

             //    $provider = ProviderCodes::where('id','=',$row->id)   
             //                              ->get();
                                  
             //    $provider->provider_code = array_get($input,'provider_code_'.strtolower($row->getproviderinfo->name));
                
             //    $provider->user_id = $row->user_id;
             //    $provider->provider_id = array_get($input,'provider_id_'.strtolower($row->getproviderinfo->name));
                
             //    $provider->save();
             //  }


          //  $get_provider = Provider::all();

          //     foreach ($get_provider as $key => $pvalue) {
          //     $provider_info = ProviderCodes::find('provider_id','=',array_get($input, 'provider_id_'.$pvalue->name));
          //     $provider_info->provider_id = array_get($input, 'provider_id_'.$pvalue->name);
          //     $provider_info->user_id = $row->id;
          //     $provider_info->provider_code = array_get($input, 'provider_code_'.$pvalue->name);

          //     $provider_info->save();

          // }
          // $get_usertype = Sales::where('user_id','=',array_get($input, 'id'))->first();
          // $get_rank = Designation::find($get_usertype->designation_id);

          // $group = SalesSupervisor::where('user_id','=',array_get($input, 'id'))->first();

          //   if(array_get($input, 'group_code') && array_get($input, 'unit_code')){

          //   if ($get_rank->rank == "Supervisor") {
              
          //       if($group == null){
          //         //dd($group);
          //         $group_new = new SalesSupervisor;
          //         $group_new->group_id = array_get($input, 'group_code');
          //         $group_new->unit_code = array_get($input, 'unit_code');
          //         $group_new->user_id = $row->id;
          //         $group_new->save();


          //         $team_info = TeamInfo::where('user_id', '=', array_get($input, 'id'))->first();
          //         $get_group_code = Group::where('id','=',$group_new->group_id)->first();   
          //         $team_info->group_id =  $get_group_code->code;                       
          //         $team_info->unit_codename =  $group_new->unit_code;
          //         $team_info->save();
                  

          //       }else{
          //          //dd(array_get($input, 'unit_code')."".array_get($input, 'group_code'));
          //         $group->group_id = array_get($input, 'group_code');
          //         $group->unit_code = array_get($input, 'unit_code');
          //         $group->user_id = $row->id;
          //         $group->save();
          //         $team_info = TeamInfo::where('user_id', '=', array_get($input, 'id'))->first();

              
          //         $get_group_code = Group::where('id','=',$group->group_id)->first();   
          //         $team_info->group_id =  $get_group_code->code;                   
          //         $team_info->unit_codename =  $group->unit_code;
          //         $team_info->save();

          //       }
              
          //   }
          //  }

          //  //dd($get_rank->rank);
          //  if(array_get($input, 'unit_id')){


          //     if ($get_rank->rank == "Advisor") {

          //       $group_advisor = SalesAdvisor::where('user_id','=',array_get($input, 'id'))->first();
          //      //dd($group_advisor);
          //       if($group_advisor == null){

          //          $group_new_advisor = new SalesAdvisor;
          //          $group_new_advisor->supervisor_id = array_get($input, 'unit_id');
          //          $group_new_advisor->user_id = $row->id;
          //          $group_new_advisor->save();

          //          $team_info = TeamInfo::where('user_id', '=', array_get($input, 'id'))->first();
          //          $get_supervisor_id = SalesAdvisor::with('getAdvisors')
          //                                       ->where('user_id','=',array_get($input, 'id'))
          //                                       ->first();
          //          $get_group_code = Group::where('id','=',$get_supervisor_id->getAdvisors->group_id)->first();   
          //          $team_info->group_id =  $get_group_code->code;                       
          //          $team_info->unit_codename =  $get_supervisor_id->getAdvisors->unit_code;
          //          $team_info->save();
                
          //       }else{
          //         //dd($row->id);
          //          $group_advisor->supervisor_id = array_get($input, 'unit_id');
          //          $group_advisor->user_id = $row->id;
          //          $group_advisor->save();

          //          $team_info = TeamInfo::where('user_id', '=', array_get($input, 'id'))->first();
          //          $get_supervisor_id = SalesAdvisor::with('getAdvisors')
          //                                       ->where('user_id','=',$row->id)
          //                                       ->first();

          //          $get_group_code = Group::where('id','=',$get_supervisor_id->getAdvisors->group_id)->first();  
          //          //dd($get_group_code); 
          //          $team_info->group_id =  $get_group_code->code;                       
          //          $team_info->unit_codename =  $get_supervisor_id->getAdvisors->unit_code;
          //          $team_info->save();
                  


          //       }         
          //    }
          //  }



          $updated = false;
          //update
          if($log_code != array_get($input, 'code')) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Code Changed']);
            $updated = true;
          }
          if($log_name != strip_tags(array_get($input, 'name'))) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Last Name Changed']);
            $updated = true;
          }
          if($log_email != array_get($input, 'email')) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Email Changed']);
            $updated = true;
          }
          if($log_mobile != array_get($input, 'mobile')) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Phone Number Changed']);
            $updated = true;
          }
          if($log_username != array_get($input, 'email')) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Email Changed']);
            $updated = true;
          }
          if($log_password != Hash::make(array_get($input, 'password')) && !empty(array_get($input, 'password'))) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Password Changed']);
            $updated = true;
          }
          if(Request::hasFile('photo')) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Photo Changed']);
            $updated = true;
          }
          if ($updated) {
            LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'System User Updated']);
          }
        
        }
      
        if($new)
        {
          $email_address =  array_get($input, 'email');
          $pref_name = (array_get($input, 'preferred_name') ? array_get($input, 'preferred_name') : 'Representative');
          Mail::send('email.account', ['email' => $email_address, 'name' => $pref_name], function($message) use ($email_address) {
            $message->from('techsupport@legacyfa-asia.com')->to($email_address)->subject('LegacyFA - Account Created');
          });
        }

        // return
        return Response::json(['body' => 'Changes have been saved.']);
    }
    
    public function view()
    {
        $this->data['title'] = "Sales Users";
        $this->data['designations'] = Designation::all();


        return view('user-management.sales-users.view', $this->data);
    }

    public function edit($id) {

        $result = $this->doEdit($id);
        
        $this->data['title'] = 'Sales User Profile';
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['row'] = $result['rows'];

        $this->data['sales_id'] = $result['rows']->id;
        $this->data['designations'] = Designation::all();
        $this->data['settings'] = Settings::first();
        $this->data['groupy'] = Group::all();
        $this->data['groups'] = Group::where('status','!=',2)->get();
        $this->data['tiers'] = Tier::all();
        $this->data['gis'] = GI::all();
        $this->data['providers'] = Provider::all();
        $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
        $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();  

        //$this->data['get_sup_info'] = $result['get_sup_info'];
        $this->data['get_members'] = $result['get_members'];
        $this->data['get_rank'] = $result['get_id'];

        //dd($this->data['get_rank']);
        $this->data['get_providers'] = $result['get_providers'];
        //dd($this->data['get_providers']);
        //dd($this->data['get_providers'] );
        //dd($this->data['get_rank']);
        //dd($this->data['get_rank']);
        //dd($result['get_members']);
        $this->data['check'] = $result['check'];
        // $this->data['add_info'] = $result['add_info'];
        $this->data['info_designations'] = $result['sales_designation'];
        //dd($result['sales_designation']);
        $this->data['info_bondings'] = $result['sales_bonding'];
        $this->data['info_teams'] = $result['sales_team'];
        $this->data['info_policies'] = $result['sales_policy'];
        $this->data['intro_info'] = $result['introducers_info'];
        $this->data['pre_payroll_info'] = $result['pre_payroll_info'];
        $this->data['get_provider_count'] = $result['get_provider_count'];
        // $this->data['company_info'] = $result['company_info'];
        // $this->data['dialer_info'] = $result['dialer_info'];
        // $this->data['misc_info'] = $result['misc_info'];
        // $this->data['jumpsite_info'] = $result['jumpsite_info'];
        // $this->data['iserver_info'] = $result['iserver_info'];
        // $this->data['security_info'] = $result['security_info'];
        // $this->data['sales_force_info'] = $result['sales_force_info'];
        // $this->data['team_info'] = $result['team_info'];
        $this->data['type_id'] = 'none';

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                                })->first();

        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                                })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        //dd($result['get_members']);
        //dd($result['add_info']);
        //dd($result['rows']);
        //dd($result['pre_payroll_info']);
         // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user

             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
              $this->data['noti_count'] = count($this->data['notification']);
    
        return view('user-management.sales-users.view', $this->data);
    }

    public function doEdit($id) {
      
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        //get sales information
       
        $get_providers = ProviderCodes::with('getproviderinfo')
                              ->where('user_id','=',$id)->get();

        $get_provider_count = ProviderCodes::with('getproviderinfo')
                              ->where('user_id','=',$id)->count();

        $pre_payroll_info = Prepayroll::with('getPrePayrollBatch')
                          ->where('user_id','=',$id)
                          ->get();
        $introducers_info = Introducer::where('user_id','=',$id)
                          ->get();
        //get sales id
    
        $get_id = Sales::with('designations')->where('user_id', '=', $id)->first();

         // dd($get_id->designations->rank);
        if ($get_id->designations->rank == "Supervisor") {

            $rows = User::with('salesInfo')
                    ->with('salesInfo.getDesignationRate')
                    ->with('salesInfo.designations')
                    ->with('salesInfo.getBandinginfo')
                    ->with('getSalesUserInfo')
                    ->with('getProviderCodes')
                    ->with('getSupervisorInfo.groupInfo')
                    //->with('getSupervisorInfo.groupUnitCode')
                   

                    ->find($id);
                $get_unit = SalesSupervisor::where('user_id','=',$id)
                                                ->first();
                if ($get_unit) {
                    $get_members = SalesAdvisor::with('advisorInfo')->with('advisorInfo.salesInfo.designations')->with('getAdvisors')->where('supervisor_id', '=', $get_unit->id)->get();
                }else{
                    $get_members = null;
                }
        } else if ($get_id->designations->rank == "Advisor") {
                  
            $rows = User::with('salesInfo')
                    ->with('salesInfo.getDesignationRate')
                    ->with('salesInfo.designations')
                    ->with('salesInfo.getBandinginfo')
                    ->with('getSalesUserInfo')
                    ->with('getProviderCodes')
                    ->with('getAdvisorInfo.getAdvisors')
                    ->with('getAdvisorInfo.getSupervisorInfo.groupInfo.salesSupervisor')
                  
                    ->find($id);

                $get_sup = SalesAdvisor::where('user_id','=',$id)->first();
                if ($get_sup) {
                    $get_members = SalesSupervisor::with('supervisorinfo')->where('id', '=', $get_sup->supervisor_id)->get();
                }else{
                    $get_members = null;
                }

        }

        // dd($rows);
          // dd($get_id);
        //dd($rows);
               // $get_members = SalesSupervisor::with('salesAdvisor')
            //                                 ->where('user_id','=',$id)
            //                                 ->get();               
        //$get_current_team = SalesSupervisor::find($id);

        //$get_members = TeamInfo::where('group_id','=',$get_current_team->group_id)->get();  


        //dd(SalesTeam::with('getSalesTeamCode')->where('sales_id', $get_id->id)->get());

        //get designation information
        $sales_designation = SalesDesignation::with('newDesignation')
                          ->with('prevDesignation')
                          ->with('modify')
                          ->where('sales_id', '=', $get_id->id)
                          ->orderBy('created_at', 'desc')
                          ->take(1)
                          ->get();

        
        //get bonding information
        $sales_bonding = SalesBonding::with('bondingModify')
                        ->where('sales_id', '=', $get_id->id)
                        ->orderBy('created_at', 'desc')
                        ->take(1)
                        ->get();

      
                        
        // $check_if_on_team = SalesNoTeam::where('sales_id', '=', $get_id->id)->first();
        // $check = '0';
        // $check_if_partner = Team::where('sales_id', '=', $get_id->id)->first();

        // if ($check_if_partner) {
        //   $check_if_sales_supervisor = SalesSupervisor::where('team_id', '=', $check_if_partner->id)->get();

        //   if(count($check_if_sales_supervisor) > 0) {
        //     $sales_team = SalesSupervisor::with('teamSales')
        //                   ->with('supervisor.users')
        //                   ->with('teamID')
        //                   ->with('teamSales.users')
        //                   ->with('teamSales.getAdvisor')
        //                   ->with('teamSales.getAdvisor.teamModified')
        //                   ->where('team_id', '=', $check_if_partner->id)
        //                   ->get();
        //     $check = '2';
        //     //dd($sales_team);
        //   } else {
        //     $sales_team = 'none';
        //     $check = '2';
        //   }
        //   //dd($sales_team);
        // } else {
        //   if ($check_if_on_team) {
        //     //get sales team info
        //     $sales_team = null;
        //   } else {

        //     $check_if_team_null = SalesTeam::where('sales_id', '=', $get_id->id)->first();

        //     if ($check_if_team_null->team_id) {
        //     //get sales team info
        //     $sales_team = SalesTeam::with('teamModified')
        //                   ->with('getSupervisor')
        //                   ->with('salesTeamInfo')
        //                   ->with('salesTeamInfo.sa`lesSupervisor')
        //                   ->with('salesTeamInfo.salesSupervisor.teamID')
        //                   ->with('salesTeamInfo.users')
        //                   ->where('sales_id', '=', $get_id->id)
        //                   ->get();
        //     $check = '1';
        //     } else {
        //     //get sales team info
        //     $sales_team = SalesTeam::with('teamModified')
        //                   ->with('getSupervisor')
        //                   ->with('salesTeamInfo')
        //                   ->with('salesTeamInfo.salesSupervisor')
        //                   ->with('salesTeamInfo.users')
        //                   ->where('sales_id', '=', $get_id->id)
        //                   ->get();
        //     $check = '0';
        //     }
        //   }
        // }

        $sales_team = null;
        $check = '0';
        
        $sales_policy = Policy::with('getPolicySales')
                      ->with('getPolicyCategoryInfo')
                      ->where('agent_code', '=', $rows->code)
                      ->get(); //edited

        // dd($sales_policy);
        // return response (format accordingly)
        if (Request::ajax()) {
            $result['rows'] = $rows->toArray();
            $result['sales_designation'] = $sales_designation->toArray();
            $result['sales_bonding'] = $sales_bonding->toArray();
            if ($sales_team) {
              $result['sales_team'] = $sales_team->toArray();
            } else {
              $result['sales_team'] = $sales_team;
            }
            $result['check'] = $check;
            $result['sales_policy'] = $sales_policy->toArray();
            // $result['add_info'] = $add_info->toArray();
            $result['introducers_info'] = $introducers_info->toArray();
            $result['get_providers']=$get_providers->toArray();
            // $result['add_info'] = $add_info->toArray();
            // $result['company_info'] = $company_info->toArray();
            // $result['dialer_info'] = $dialer_info->toArray();
            // $result['misc_info'] = $misc_info->toArray();
            // $result['jumpsite_info'] = $jumpsite_info->toArray();
            // $result['iserver_info'] = $iserver_info->toArray();
            // $result['team_info'] = $team_info->toArray();
            // $result['security_info'] = $security_info->toArray();
            // $result['sales_force_info'] = $sales_force_info->toArray();
            if($get_members){
                 $result['get_members'] = $get_members->toArray();
             }else{
                $result['get_members'] = $get_members;
             }
            
          
            //dd($add_info);
            return Response::json($result);
        } else {
            $result['rows'] = $rows;
            $result['sales_designation'] = $sales_designation;
            $result['sales_bonding'] = $sales_bonding;
            $result['sales_team'] = $sales_team;
            $result['sales_policy'] = $sales_policy;
            $result['check'] = $check;
            // $result['add_info'] = $add_info;
            $result['introducers_info'] = $introducers_info;
            $result['pre_payroll_info'] = $pre_payroll_info;
            $result['get_provider_count'] = $get_provider_count;
            // $result['add_info'] = $add_info;
            // $result['company_info'] = $company_info;
            // $result['dialer_info'] = $dialer_info;
            // $result['misc_info'] = $misc_info;
            // $result['jumpsite_info'] = $jumpsite_info;
            // $result['iserver_info'] = $iserver_info;
            // $result['team_info'] = $team_info;
            // $result['security_info'] = $security_info;
            // $result['sales_force_info'] = $sales_force_info;
            $result['get_id'] = $get_id;
            $result['get_members'] = $get_members;
            $result['get_providers']=$get_providers;
           
            //dd($add_info);
            return $result;
        }
    }
     public function editUser() {

        $result = $this->doEdit();
        
        $this->data['title'] = 'View';
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['row'] = $result['rows'];

        $this->data['sales_id'] = $result['rows']->id;
        $this->data['designations'] = Designation::all();
        $this->data['settings'] = Settings::first();
        $this->data['groupy'] = Group::all();
        $this->data['groups'] = Group::where('status','!=',2)->get();
        $this->data['tiers'] = Tier::all();
        $this->data['providers'] = Provider::all();
        $this->data['supervisors'] = Designation::where('rank', '=', 'Supervisor')->get();
        $this->data['advisors'] = Designation::where('rank', '=', 'Advisor')->get();  

        //$this->data['get_sup_info'] = $result['get_sup_info'];
        $this->data['get_members'] = $result['get_members'];
        $this->data['get_rank'] = $result['get_id'];
        //dd($this->data['get_rank']);
        $this->data['get_providers'] = $result['get_providers'];
        //dd($this->data['get_providers']);
        //dd($this->data['get_providers'] );
        //dd($this->data['get_rank']);
        //dd($this->data['get_rank']);
        //dd($result['get_members']);
        $this->data['check'] = $result['check'];
        // $this->data['add_info'] = $result['add_info'];
        $this->data['info_designations'] = $result['sales_designation'];
        //dd($result['sales_designation']);
        $this->data['info_bondings'] = $result['sales_bonding'];
        $this->data['info_teams'] = $result['sales_team'];
        $this->data['info_policies'] = $result['sales_policy'];
        $this->data['intro_info'] = $result['introducers_info'];
        $this->data['pre_payroll_info'] = $result['pre_payroll_info'];
        $this->data['get_provider_count'] = $result['get_provider_count'];
        // $this->data['company_info'] = $result['company_info'];
        // $this->data['dialer_info'] = $result['dialer_info'];
        // $this->data['misc_info'] = $result['misc_info'];
        // $this->data['jumpsite_info'] = $result['jumpsite_info'];
        // $this->data['iserver_info'] = $result['iserver_info'];
        // $this->data['security_info'] = $result['security_info'];
        // $this->data['sales_force_info'] = $result['sales_force_info'];
        // $this->data['team_info'] = $result['team_info'];
        $this->data['type_id'] = 'none';

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                                })->first();

        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                                })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        //dd($result['get_members']);
        //dd($result['add_info']);
        //dd($result['rows']);
        //dd($result['pre_payroll_info']);
         // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user

             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
        
              $this->data['noti_count'] = count($this->data['notification']);
    
        return view('user-management.sales-users.view', $this->data);
    }
     public function doEditUser() {

        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        //get sales information
        $id = Request::input('id');
        $get_providers = ProviderCodes::with('getproviderinfo')
                              ->where('user_id','=',$id)->get();

        $get_provider_count = ProviderCodes::with('getproviderinfo')
                              ->where('user_id','=',$id)->count();

        $pre_payroll_info = Prepayroll::with('getPrePayrollBatch')
                          ->where('user_id','=',$id)
                          ->get();
        $introducers_info = Introducer::where('user_id','=',$id)
                          ->get();
        //get sales id
    
        $get_id = Sales::with('designations')->where('user_id', '=', $id)->first();
         // dd($get_id->designations->rank);
        if ($get_id->designations->rank == "Supervisor") {

            $rows = User::with('salesInfo')
                    ->with('salesInfo.getDesignationRate')
                    ->with('salesInfo.designations')
                    ->with('salesInfo.getBandinginfo')
                    ->with('getSalesUserInfo')
                    ->with('getProviderCodes')
                    ->with('getSupervisorInfo.groupInfo')
                    //->with('getSupervisorInfo.groupUnitCode')
                    ->find($id);
                $get_unit = SalesSupervisor::where('user_id','=',$id)
                                                ->first();
                if ($get_unit) {
                    $get_members = SalesAdvisor::with('advisorInfo')->with('advisorInfo.salesInfo.designations')->with('getAdvisors')->where('supervisor_id', '=', $get_unit->id)->get();
                }else{
                    $get_members = null;
                }
        } else if ($get_id->designations->rank == "Advisor") {
                  
            $rows = User::with('salesInfo')
                    ->with('salesInfo.getDesignationRate')
                    ->with('salesInfo.designations')
                    ->with('salesInfo.getBandinginfo')
                    ->with('getSalesUserInfo')
                    ->with('getProviderCodes')
                    ->with('getAdvisorInfo.getAdvisors')
                    ->with('getAdvisorInfo.getSupervisorInfo.groupInfo.salesSupervisor')
                    ->find($id);

                $get_sup = SalesAdvisor::where('user_id','=',$id)->first();
                if ($get_sup) {
                    $get_members = SalesSupervisor::with('supervisorinfo')->where('id', '=', $get_sup->supervisor_id)->get();
                }else{
                    $get_members = null;
                }

        }

        //dd($rows);
          // dd($get_id);
        //dd($rows);
               // $get_members = SalesSupervisor::with('salesAdvisor')
            //                                 ->where('user_id','=',$id)
            //                                 ->get();               
        //$get_current_team = SalesSupervisor::find($id);

        //$get_members = TeamInfo::where('group_id','=',$get_current_team->group_id)->get();  


        //dd(SalesTeam::with('getSalesTeamCode')->where('sales_id', $get_id->id)->get());

        //get designation information
        $sales_designation = SalesDesignation::with('newDesignation')
                          ->with('prevDesignation')
                          ->with('modify')
                          ->where('sales_id', '=', $get_id->id)
                          ->orderBy('created_at', 'desc')
                          ->take(1)
                          ->get();

        
        //get bonding information
        $sales_bonding = SalesBonding::with('bondingModify')
                        ->where('sales_id', '=', $get_id->id)
                        ->orderBy('created_at', 'desc')
                        ->take(1)
                        ->get();

      
                        
        // $check_if_on_team = SalesNoTeam::where('sales_id', '=', $get_id->id)->first();
        // $check = '0';
        // $check_if_partner = Team::where('sales_id', '=', $get_id->id)->first();

        // if ($check_if_partner) {
        //   $check_if_sales_supervisor = SalesSupervisor::where('team_id', '=', $check_if_partner->id)->get();

        //   if(count($check_if_sales_supervisor) > 0) {
        //     $sales_team = SalesSupervisor::with('teamSales')
        //                   ->with('supervisor.users')
        //                   ->with('teamID')
        //                   ->with('teamSales.users')
        //                   ->with('teamSales.getAdvisor')
        //                   ->with('teamSales.getAdvisor.teamModified')
        //                   ->where('team_id', '=', $check_if_partner->id)
        //                   ->get();
        //     $check = '2';
        //     //dd($sales_team);
        //   } else {
        //     $sales_team = 'none';
        //     $check = '2';
        //   }
        //   //dd($sales_team);
        // } else {
        //   if ($check_if_on_team) {
        //     //get sales team info
        //     $sales_team = null;
        //   } else {

        //     $check_if_team_null = SalesTeam::where('sales_id', '=', $get_id->id)->first();

        //     if ($check_if_team_null->team_id) {
        //     //get sales team info
        //     $sales_team = SalesTeam::with('teamModified')
        //                   ->with('getSupervisor')
        //                   ->with('salesTeamInfo')
        //                   ->with('salesTeamInfo.sa`lesSupervisor')
        //                   ->with('salesTeamInfo.salesSupervisor.teamID')
        //                   ->with('salesTeamInfo.users')
        //                   ->where('sales_id', '=', $get_id->id)
        //                   ->get();
        //     $check = '1';
        //     } else {
        //     //get sales team info
        //     $sales_team = SalesTeam::with('teamModified')
        //                   ->with('getSupervisor')
        //                   ->with('salesTeamInfo')
        //                   ->with('salesTeamInfo.salesSupervisor')
        //                   ->with('salesTeamInfo.users')
        //                   ->where('sales_id', '=', $get_id->id)
        //                   ->get();
        //     $check = '0';
        //     }
        //   }
        // }

        $sales_team = null;
        $check = '0';
        
        $sales_policy = Policy::with('getPolicySales')
                      ->with('getPolicyCategoryInfo')
                      ->where('agent_code', '=', $rows->code)
                      ->get(); //edited

        // dd($sales_policy);
        // return response (format accordingly)
        if (Request::ajax()) {
            $result['rows'] = $rows->toArray();
            $result['sales_designation'] = $sales_designation->toArray();
            $result['sales_bonding'] = $sales_bonding->toArray();
            if ($sales_team) {
              $result['sales_team'] = $sales_team->toArray();
            } else {
              $result['sales_team'] = $sales_team;
            }
            $result['check'] = $check;
            $result['sales_policy'] = $sales_policy->toArray();
            // $result['add_info'] = $add_info->toArray();
            $result['introducers_info'] = $introducers_info->toArray();
            $result['get_providers']=$get_providers->toArray();
            // $result['add_info'] = $add_info->toArray();
            // $result['company_info'] = $company_info->toArray();
            // $result['dialer_info'] = $dialer_info->toArray();
            // $result['misc_info'] = $misc_info->toArray();
            // $result['jumpsite_info'] = $jumpsite_info->toArray();
            // $result['iserver_info'] = $iserver_info->toArray();
            // $result['team_info'] = $team_info->toArray();
            // $result['security_info'] = $security_info->toArray();
            // $result['sales_force_info'] = $sales_force_info->toArray();
            if($get_members){
                 $result['get_members'] = $get_members->toArray();
             }else{
                $result['get_members'] = $get_members;
             }
            
          
            //dd($add_info);
            return Response::json($result);
        } else {
            $result['rows'] = $rows;
            $result['sales_designation'] = $sales_designation;
            $result['sales_bonding'] = $sales_bonding;
            $result['sales_team'] = $sales_team;
            $result['sales_policy'] = $sales_policy;
            $result['check'] = $check;
            // $result['add_info'] = $add_info;
            $result['introducers_info'] = $introducers_info;
            $result['pre_payroll_info'] = $pre_payroll_info;
            $result['get_provider_count'] = $get_provider_count;
            // $result['add_info'] = $add_info;
            // $result['company_info'] = $company_info;
            // $result['dialer_info'] = $dialer_info;
            // $result['misc_info'] = $misc_info;
            // $result['jumpsite_info'] = $jumpsite_info;
            // $result['iserver_info'] = $iserver_info;
            // $result['team_info'] = $team_info;
            // $result['security_info'] = $security_info;
            // $result['sales_force_info'] = $sales_force_info;
            $result['get_id'] = $get_id;
            $result['get_members'] = $get_members;
            $result['get_providers']=$get_providers;
           
            //dd($add_info);
            // dd($rows);
            return $result;
        }
    }

    public function getOwnerSupervisor() {

      $this->data['rows'] = '<option class="hide" value="">Select</option>';
      $this->data['title'] = '';

      if (Request::input('id')) {
        $get_supervisor = SalesSupervisor::find(Request::input('id'));

        if ($get_supervisor) {
          $get_owner = User::find($get_supervisor->user_id);

          $this->data['title'] = 'Editing <b>(' .$get_supervisor->unit_code . '</b>)';

          $check_owner = Group::where('user_id', $get_supervisor->user_id)->first();
          if ($check_owner) {
            $this->data['title'] = 'Editing <b>(' .$get_supervisor->unit_code . '</b>) - GROUP OWNER';
          }

          if ($get_owner) {
            $this->data['rows'] .= '<optgroup label="Supervisor">';
            $this->data['rows'] .= '<option value="' . $get_owner->id . '">' . $get_owner->name . '</option>';
          }

          $get_adv_ids = SalesAdvisor::where('supervisor_id', $get_supervisor->id)->lists('user_id');

          $get_advisors = User::where('usertype_id', 8)
                          ->where('status', 1)
                          ->whereIn('users.id', $get_adv_ids)->get();

          if (count($get_advisors) > 0) {
            $this->data['rows'] .= '<optgroup label="Advisors">';
            foreach ($get_advisors as $gakey => $gafield) {
              $this->data['rows'] .= '<option value="' . $gafield->id . '">' . $gafield->name . '</option>';
            }
          }
        }

        return Response::json($this->data);
      }
    }

    public function getOwner() {


      $rows = '<option class="hide" value="">Select</option>';

      if (Request::input('id')) {
        $get_group = Group::find(Request::input('id'));
        if ($get_group) {
          $get_owner = User::find($get_group->user_id);

          if ($get_owner) {
            $rows .= '<optgroup label="Current Owner">';
            $rows .= '<option value="' . $get_owner->id . '">' . $get_owner->name . '</option>';
          }
        
          $get_ids = SalesSupervisor::where('group_id', Request::input('id'))->where('user_id', '!=', $get_group->user_id)->lists('user_id');

          $get_adv_ids = SalesSupervisor::where('group_id', Request::input('id'))->lists('id');
          $get_adv_ids = SalesAdvisor::whereIn('supervisor_id', $get_adv_ids)->where('user_id', '!=', $get_group->user_id)->lists('user_id');

          $get_supervisors = User::where('usertype_id', 8)
                        ->where('status', 1)
                        ->whereIn('users.id', $get_ids)->get();

          $get_advisors = User::where('usertype_id', 8)
                          ->where('status', 1)
                          ->whereIn('users.id', $get_adv_ids)->get();

          if (count($get_supervisors) > 0) {
            $rows .= '<optgroup label="Supervisors">';
            foreach ($get_supervisors as $gskey => $gsfield) {
              $rows .= '<option value="' . $gsfield->id . '">' . $gsfield->name . '</option>';
            }
          }

          if (count($get_advisors) > 0) {
            $rows .= '<optgroup label="Advisors">';
            foreach ($get_advisors as $gakey => $gafield) {
              $rows .= '<option value="' . $gafield->id . '">' . $gafield->name . '</option>';
            }
          }
        }
        return Response::json($rows);
      } else {
        $get_owners = Group::where('status', 1)->lists('user_id');

        $get_supervisors = SalesSupervisor::whereNotIn('user_id', $get_owners)->lists('user_id');

        $get_advisors = SalesAdvisor::whereNotIn('user_id', $get_owners)->lists('user_id');

        $get_supervisors = User::where('usertype_id', 8)
                          ->where('status', 1)
                          ->whereIn('users.id', $get_supervisors)
                          ->orderBy('name', 'asc')->get();

        $get_advisors = User::where('usertype_id', 8)
                          ->where('status', 1)
                          ->whereIn('users.id', $get_advisors)
                          ->orderBy('name', 'asc')->get();

        if (count($get_supervisors) > 0) {
          $rows .= '<optgroup label="Supervisors">';
          foreach ($get_supervisors as $gskey => $gsfield) {
            $rows .= '<option value="' . $gsfield->id . '">' . $gsfield->name . '</option>';
          }
        }

        if (count($get_advisors) > 0) {
          $rows .= '<optgroup label="Advisors">';
          foreach ($get_advisors as $gakey => $gafield) {
            $rows .= '<option value="' . $gafield->id . '">' . $gafield->name . '</option>';
          }
        }

        return Response::json($rows);
      }

    }

    public function getManagerCode() {
      $id = Request::input('id');
      
      $get_code = SalesSupervisor::where('user_id', $id)->first();
      $this->data['rows'] = '';
      $this->data['supervisor'] = false;

      if ($get_code) {
        $this->data['rows'] = $get_code->unit_code;
      } else {
        $this->data['supervisor'] = true;
        $get_designations = Designation::where('rank', 'Supervisor')->get();

        $this->data['rows'] = '<option class="hide" value="">Select</option>'; 

        foreach ($get_designations as $key => $field) {
          $this->data['rows'] .= '<option value="' . $field->id .'">' . $field->designation . '</option>';
        }
      }

      return Response::json($this->data);
    }

    //saving sales team

    public function team() {
      
      $input = Input::all();

      // check if an ID is passed
      if(array_get($input, 'id')) {

          // get the user info
          $row = Sales::where('user_id', '=', array_get($input, 'id'))->first();

          if(!$row) {
              return Response::json(['error' => ["The requested item was not found in the database."]]);
          }
      }

      $rules = [
          'supervisor_id' => 'required',
      ];

      // field name overrides
      $names = [
          'supervisor_id' => 'Supervisor',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      $salesTeam = SalesTeam::where('sales_id', '=', $row->id)->orderBy('created_at', 'desc')->first();

      $prev = 0;
      if($salesTeam) {
      $prev = $salesTeam->supervisor_id;
      }

      $get_team = SalesSupervisor::where('sales_id', '=', array_get($input, 'supervisor_id'))->first();


      $salesTeam = new SalesTeam;
      $salesTeam->prev_supervisor_id= $prev;
      $salesTeam->sales_id = $row->id;
      $salesTeam->edit_id = Auth::user()->id;
      $salesTeam->supervisor_id =array_get($input, 'supervisor_id');

      if ($get_team) {
        $salesTeam->team_id = $get_team->team_id;
      } else {
        $salesTeam->team_id = null;
      }

      $salesTeam->save();

      $sales_supervisor = SalesSupervisor::where('sales_id', '=', $row->id)->first();

      $sales_supervisor->supervisor_id = array_get($input, 'supervisor_id');

      if ($get_team) {
        $sales_supervisor->team_id = $get_team->team_id;
      } else {
        $sales_supervisor->team_id = null;
      }
        
      $sales_supervisor->save();

      LogSales::create(['user_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Sales Team Updated']);

      return Response::json(['body' => 'Changes has been saved.']);

    }


    public function editBanding() {

        $id = Request::input('id'); 
        // prevent non-admin from viewing deactivated row
        $row = SalesBonding::with('bondingModify')->find($id);

        if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

    }

    public function saveBanding() {

      $input = Input::all();
      $sales = null;
      // check if an ID is passed
     if(array_get($input, 'id')) {

          // get the user info
          $row = SalesBonding::where('id', '=', array_get($input, 'id'))->first();
         

          if(!$row) {
              return Response::json(['error' => ["The requested item was not found in the database."]]);
          }
      }
      
      $rules = [
          'bonding_rate' => 'required|numeric',
      ];

      // field name overrides
      $names = [
          'bonding_rate' => 'bonding rate',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }
      $sales = SalesBonding::where('sales_id', '=', $row->id)->orderBy('created_at', 'desc')->first();
      $sales->bonding_rate = array_get($input, 'bonding_rate');
      $sales->save();

      //create log
      LogSales::create(['user_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Bonding Updated']);

      return Response::json(['body' => 'Changes has been saved.']);
    }

    public function saveSales() {

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = User::find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => ["The requested item was not found in the database."]]);
            }

        }

        $rules = [
            'photo' => 'image|max:3000',
            'code'  =>  'required|min:2|max:100',
            'name' => 'required|min:2|max:100',
            'password' => 'min:6|confirmed',
            'email'  => 'required|email|unique:users,email,' . $row->id,
            'mobile'  => 'required|min:2|max:100',
        ];

        // field name overrides
        $names = [
            'name' => 'full name',
            'usertype_id' => 'designation',
            'system_id' => 'user id',
            'code' => 'user code',
        ];
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 


        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        //$row->system_id = array_get($input, 'system_id');
        $row->code      = array_get($input, 'code');
        $row->name      = strip_tags(array_get($input, 'name'));
        $row->email     = array_get($input, 'email');
        $row->mobile    = array_get($input, 'mobile');
        $row->username  = array_get($input, 'email');
        //$row->usertype_id = 8;

        /* set type only if this isn't me
        if($new || ($new = false && $row->id != Auth::user()->id)) {
          $row->usertype_id = 7;
        }*/

        // do not change password if old user and field is empty
        if(array_get($input, 'password')) {
          $row->password = Hash::make(array_get($input, 'password'));
        }

        // save model
        $row->save();

        if (Request::hasFile('photo')) {
            // Save the photo
            Image::make(Request::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
            $row->photo = $row->id . '.jpg';
            $row->save();
        }

        // create log
        LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Sales User Updated']);

        // return
        return Response::json(['body' => 'Changes have been saved.']);
    }

    /**
     * Deactivate the sales user
     *
     */
    public function disabled() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            if ($row->id == Auth::user()->id) {
                return Response::json(['error' => "Access denied."]);
            } else{
              $log_status = $row->status;
              if ($log_status != 2) {
                $row->deactivated_date = Carbon::now();
                $row->status = 0;
                $row->save();

                if ($log_status != 0) {
                  //create log
                  LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Sales User Disabled']);
                }
              } else {
                return Response::json(['error' => "Access denied."]);
              }
            }

            // return
            return Response::json(['body' => 'Sales user has been disabled.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the sales user
     *
     */
    public function enable() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            if ($row->id == Auth::user()->id) {
                return Response::json(['error' => "Access denied."]);
            } else {
              $log_status = $row->status;
              if ($log_status != 2) {
                $row->status = 1;
                $row->deactivated_date = null;
                $row->save();

                if ($log_status != 1) {
                  //create log
                  LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Sales User Enabled']);
                }
              } else {
                return Response::json(['error' => "Access denied."]);
              }
            }
            
            // return
            return Response::json(['body' => 'Sales user has been activated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Remove the sales user
     *
     */
    public function remove() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            if ($row->id == Auth::user()->id) {
                return Response::json(['error' => "Access denied."]);
            } else {
              $log_status = $row->status;
              if ($log_status != 2) {
                $row->status = 2;
                $row->email = '(' . $row->email . ')';
                $row->save();

                if ($log_status != 2) {
                  //create log
                  LogSales::create(['user_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Sales User Removed']);
                }
              } else {
                return Response::json(['error' => "Access denied."]);
              }
            }

            // return
            return Response::json(['body' => 'Sales user has been removed.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }


     /**
     * Disable multiple user
     *
     */
    public function disableSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select users first.']); 
        } else {
            // check if auth user is selected
            foreach ($ids as $id_check) {
                $row = User::find($id_check);
                if ($row->id == Auth::user()->id) {
                    return Response::json(['error' => "Access denied."]);
                } 
            }
            foreach ($ids as $id) {
                $row = User::find($id);
                if(!is_null($row)) {
                    $row->status = 0;
                    $row->save();
                }
            }
            return Response::json(['body' => 'Selected sales users has been Disabled.']);
        }
    }  

    /**
     * Remove multiple user
     *
     */
    public function removeSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select users first.']); 
        } else {
            // check if auth user is selected
            foreach ($ids as $id_check) {
                $row = User::find($id_check);
                if ($row->id == Auth::user()->id) {
                    return Response::json(['error' => "Access denied."]);
                } 
            }

            foreach ($ids as $id) {
                $row = User::find($id);
                if(!is_null($row)) {
                    $row->status = 2;
                    $row->email = '(' . $row->email . ')';
                    $row->username = '(' . $row->username . ')';
                    $row->save();
                }
            }
            return Response::json(['body' => 'Selected sales users has been Removed.']);
        }
    }

    public function designate() {

      $input = Input::all();

      // check if an ID is passed
      if(array_get($input, 'id')) {

          // get the user info
          $row = Sales::where('user_id', '=', array_get($input, 'id'))->first();

          if(!$row) {
              return Response::json(['error' => ["The requested item was not found in the database."]]);
          }
      }

      $rules = [
          'rate' => 'required',
          'designation_id' => 'required',
          'effective_date' => 'required|date'
      ];

      // field name overrides
      $names = [
          'rate' => 'rate',
          'designation_id' => 'designation',
      ];
      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }
      
      $sales = SalesDesignation::where('sales_id', '=', $row->id)->orderBy('created_at', 'desc')->first();

      if($sales) {
        $prev = $sales->designation_id;
        $sales = new SalesDesignation;
      } else {
        $prev = $row->designation_id;
        $sales = new SalesDesignation;
      }

      $sales->effective_date = Carbon::createFromFormat('m/d/Y', Request::input('effective_date'));
      $sales->designation_id = array_get($input, 'designation_id');
      $sales->prev_designation_id = $prev;
      $sales->sales_id = $row->id;
      $sales->rate = array_get($input, 'rate');
      $sales->edit_id = Auth::user()->id;
      $sales->save();


      $row->designation_id = array_get($input, 'designation_id');
      $row->save();
      
      //create log
      LogSales::create(['user_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Designation Updated']);

      return Response::json(['body' => 'Changes has been saved.']);

    }

    //saving banding settings
    public function bond() {

      $input = Input::all();
      $sales = null;
      // check if an ID is passed
     if(array_get($input, 'id')) {

          // get the user info
          $row = Sales::where('user_id', '=', array_get($input, 'id'))->first();
          $sales = SalesBonding::where('sales_id', '=', $row->id)->orderBy('created_at', 'desc')->first();

          if(!$row) {
              return Response::json(['error' => ["The requested item was not found in the database."]]);
          }
      }
      
      $rules = [
          'bonding_rate' => 'required|numeric',
      ];

      // field name overrides
      $names = [
          'bonding_rate' => 'bonding rate',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }
      

      if($sales) {
        $prev = $sales->bonding_rate;
        $sales = new SalesBonding;
      } else {
        $prev = 0;
        $sales = new SalesBonding;
      }

      $sales->bonding_rate = array_get($input, 'bonding_rate');
      $sales->prev_bonding_rate = $prev;
      $sales->sales_id = $row->id;
      $sales->effective_date = Carbon::createFromFormat('m/d/Y', Request::input('effective_date'));
      $sales->edit_id = Auth::user()->id;
      $sales->save();

      $change_band = Sales::find($row->id);
      $change_band->bonding_rate = array_get($input, 'bonding_rate');
      $change_band->save();

      //create log
      LogSales::create(['user_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Bonding Updated']);

      return Response::json(['body' => 'Changes has been saved.']);

    }

    public function getSupervisor() {

      $id = Request::input('id');

      $get_rank = Designation::find($id);

      $rank = $get_rank->rank;

      if ($rank == '1') {
        $result['rows'] = Designation::with('salesDesignations')->with('salesDesignations.users')->where('rank', '<', '4')->get();
      } else {
        $result['rows'] = Designation::with('salesDesignations')->with('salesDesignations.users')->where('rank', '>', $rank)->get();
      }

      return Response::json($result);
    }


    public function getSalesSupervisor() {

      $id = Request::input('id');

      $get_id = Sales::where('user_id', '=', $id)->first();

      $get_rank = Designation::find($get_id->designation_id);

      $rank = $get_rank->rank;

      if ($rank == '1') {
        $result['rows'] = Designation::with('salesDesignations')->with('salesDesignations.users')->where('rank', '<', '4')->get();
      } else {
        $result['rows'] = Designation::with('salesDesignations')->with('salesDesignations.users')->where('rank', '>', $rank)->get();
      }

      return Response::json($result);
    }

    //sales logs export
    public function export()
    {
      $name = 'SALES_USER-' . Carbon::now()->format('Y-m-d');

      Excel::create($name, function($excel) use ($name) {

        $rows = User::with('salesInfo')
                    ->with('salesInfo.getDesignationRate')
                    ->with('salesInfo.getDesignationRate.newDesignation')
                    ->with('salesInfo.getDesignationRate.prevDesignation')
                    ->with('salesInfo.designations')
                    ->with('salesInfo.getBandinginfo')
                    ->with('getSalesUserInfo')
                    ->with('getProviderCodes')
                    ->with('getSalesGroup')
                    ->with('getSalesGroup.getSupervisorInfo')
                    ->with('getSalesGroup.getGroupInfo')
                    ->with('getSupervisorInfo.groupInfo')
                    ->where('usertype_id', 8)
                    ->get();

        $excel->sheet($name, function($sheet) use ($rows) {

          $sheet->cell('A1', function($cell) {
            $cell->setValue('Basic Information');
            $cell->setFontWeight('bold');
          });

          $sheet->mergeCells('A1:I1');
          $sheet->cells('A1:I1', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
          });

          $sheet->cell('J1', function($cell) {
            $cell->setValue('Contact Information');
            $cell->setFontWeight('bold');
          });

          $sheet->mergeCells('J1:M1');
          $sheet->cells('J1:M1', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
          });

          $sheet->cell('N1', function($cell) {
            $cell->setValue('Company Information');
            $cell->setFontWeight('bold');
          });

          $sheet->mergeCells('N1:AB1');
          $sheet->cells('N1:AB1', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
          });

          $sheet->cell('AC1', function($cell) {
            $cell->setValue('Extra Information');
            $cell->setFontWeight('bold');
          });

          $sheet->mergeCells('AC1:AF1');
          $sheet->cells('AC1:AF1', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
          });

          $row_cell = 1;

          $char_key = 0;
          $ctr = 1;
          $all_categories = [];

          $get_providers = Provider::where('status', 1)->lists('id')->toArray();
          $categories = ProviderClassification::where('status', 1)->lists('id')->toArray();
          $chars = '';

          foreach (array_unique($get_providers) as $gpkey => $gpvalue) {
            
            if ($ctr == 1) {
              $start_key = chr(97) . chr(103);
              $sheet->cell($start_key . '1', function($cell) use ($gpvalue) {
                $get_provider = Provider::find($gpvalue);
                if ($get_provider) {
                  $cell->setValue($get_provider->name);
                  $cell->setFontWeight('bold');
                }
              });
            } else {
              $start_key = $char;
              $sheet->cell($start_key . '1', function($cell) use ($gpvalue) {
                $get_provider = Provider::find($gpvalue);
                if ($get_provider) {
                  $cell->setValue($get_provider->name);
                  $cell->setFontWeight('bold');
                }
              });
            }

            $count_categories = ProviderClassification::where('provider_id', $gpvalue)->whereIn('id', $categories)->count();
            $get_cat = ProviderClassification::where('provider_id', $gpvalue)->whereIn('id', $categories)->orderBy('provider_id', 'asc')->lists('id');
            $all_categories = array_merge($all_categories, $get_cat->toArray());

            // if ($ctr == 9) {
            //   $count_categories = 78;
            //   $char_key += $count_categories;
            // } else {
            //   $char_key += $count_categories;
            // }

            $char_key += $count_categories;

            if ($char_key > 18 && $char_key < 47) {
              $char = chr(97) . chr(97+($char_key-20));
              $char_last = chr(98) . chr(97+($char_key-21));
              if ($char_key == 19) {
                $char = chr(97) . chr(103+$char_key);
                $char_last = chr(97) . chr(103+$char_key-1);
              }
              if ($char_key-1 == 19) {
                $char = chr(98) . chr(97);
                $char_last = chr(97) . chr(122);
              }
            } else if ($char_key > 46 && $char_key < 73) {
              $char = chr(99) . chr(97+($char_key-50));
              $char_last = chr(99) . chr(97+($char_key-47));
            } else if ($char_key > 72 &&  $char_key < 99) {
              $char = chr(100) . chr(97+($char_key-72));
              $char_last = chr(100) . chr(97+($char_key-73));
            } else if ($char_key > 98) {
              dd("ERROR");
            } else {
              $char = chr(97) . chr(103+$char_key);
              $char_last = chr(97) . chr(103+$char_key-1);
            }

            $sheet->mergeCells($start_key . '1:' . $char_last . '1');
            $sheet->cells($start_key . '1:' . $char_last . '1', function($cells) {
              $cells->setValignment('center');
              $cells->setAlignment('center');
            });

            //TEST PURPOSES
            $chars .= ($ctr) . '-' . $start_key . '=' . $char_last . '*****';
            $ctr++;
          }

          $sheet->setAutoSize(true);
          $sheet->getStyle('A' . $row_cell . ':' . $char_last . $row_cell)->getAlignment()->setWrapText(true);
          $row_cell++;

          $sheet->appendRow();

          $sub_rows = [
                        'AGENT CODE',
                        'NRIC',
                        'SALESFORCE ID',
                        'SALUTATION',
                        'FULL NAME',
                        'PREFERRED NAME',
                        'GENDER',
                        'DATE OF BIRTH',
                        'AGENT STATUS',
                        'MOBILE NO',
                        'HOME ADDRESS',
                        'POSTAL CODE',
                        'ADVISORS MOBILE',
                        'ADVISORS OFFICE PHONE',
                        'DESIGNATION(EXISTING)',
                        'DESIGNATION(EXISTING) %',
                        'DESIGNATION(EXISTING) DATE',
                        'DESIGNATION(UPDATED)',
                        'DESIGNATION(OLD ENTRY)',
                        'BANDING RANK',
                        'BANDING RANK %',
                        'BANDING RANK DATE',
                        'GI BANDING RANK',
                        'LATEST YTD PRODUCTION',
                        'SMARTERMAIL USERNAME',
                        'AGENT SERIAL NO',
                        'DIRECTOR CODE',
                        'MANAGER CODE',
                        'RANKING',
                        'CONTRACTED DATE(OLD)',
                        'RNF DATE(UNDER LEGACYFA)',
                        'OFF-BOARDING DATE',
                        'NOTES',
                      ];

          $cat_ctr = count($sub_rows);
          $all_categories = [];

          foreach ($get_providers as $gpkey => $gpvalue) {

            $get_categories = ProviderClassification::where('provider_id', $gpvalue)->whereIn('id', $categories)->get();

            foreach ($get_categories as $gckey => $gcfield) {
              $sub_rows[$cat_ctr] = $gcfield->name;
              $all_categories[$cat_ctr] = $gcfield->id;
              $cat_ctr++;
            }
          }

          $sheet->appendRow($sub_rows);

          $sheet->cells('A' . $row_cell . ':' . $char_last . $row_cell, function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
            $cells->setFontWeight('bold');
          });

          $row_cell++;

          foreach ($rows as $key => $row) {

            $agent_code = $row->code;
            $full_name = $row->name;
            $preferred_name = $row->preferred_name;
            $gender = $row->gender;
            $mobile_no = $row->mobile;
            $agent_status = ($row->status == 1 ? 'Active' : 'Resigned');

            $gi_banding_rank = '';
            $banding_rank_per = '';
            $banding_rank_date = '';
            $ranking = '';
            $designation_existing = '';
            $designation_existing_per = '';
            $designation_existing_date = '';
            $designation_old = '';
            $designation_updated = '';

            // salesInfo
            if ($row->salesInfo) {
              $gi_banding_rank = $row->salesInfo->gi_banding_rank;
              $banding_rank_per = $row->salesInfo->bonding_rate;

              if ($row->salesInfo->getBandinginfo) {
                $banding_rank_date = $row->salesInfo->getBandinginfo->effective_date;
              }

              if ($row->salesInfo->designations) {
                $ranking = $row->salesInfo->designations->rank;
                $designation_existing = $row->salesInfo->designations->designation;
              }

              if ($row->salesInfo->getDesignationRate) {
                $designation_existing_per = $row->salesInfo->getDesignationRate->designation_rate;
                $designation_existing_date = $row->salesInfo->getDesignationRate->created_at;
              }

              if ($row->salesInfo->getDesignationRate->oldDesignation) {
                $designation_old= $row->salesInfo->getDesignationRate->oldDesignation->designation;
              }

              if ($row->salesInfo->getDesignationRate->newDesignation) {
                $designation_updated = $row->salesInfo->getDesignationRate->newDesignation->designation;
              }
            }

            $nric = '';
            $salesforce_id = '';
            $salutation = '';
            $date_of_birth = '';
            $home_address = '';
            $postal_code = '';
            $advisors_mobile = '';
            $advisors_office_phone = '';
            $banding_rank = '';
            $latest_ytd_production = '';
            $smartermail_username = '';
            $agent_serial_no = '';
            $contracted_date = '';
            $rnf_date = '';
            $off_boarding_date = '';
            $notes = '';

            // getSalesUserInfo
            if ($row->getSalesUserInfo) {
              $nric = $row->getSalesUserInfo->nric;
              $salesforce_id = $row->getSalesUserInfo->salesforce_id;
              $salutation = $row->getSalesUserInfo->salutation;
              $date_of_birth = $row->getSalesUserInfo->date_of_birth;
              $home_address = $row->getSalesUserInfo->home_address;
              $postal_code = $row->getSalesUserInfo->postal_code;
              $advisors_mobile = $row->getSalesUserInfo->advisors_mobile;
              $advisors_office_phone = $row->getSalesUserInfo->advisors_office_mobile;
              $banding_rank = $row->getSalesUserInfo->banding_rank;
              $latest_ytd_production = $row->getSalesUserInfo->latest_ytd_production;
              $smartermail_username = $row->getSalesUserInfo->smartermail_username;
              $agent_serial_no = $row->getSalesUserInfo->agent_serial_no;
              $contracted_date = $row->getSalesUserInfo->contracted_date;
              $rnf_date = $row->getSalesUserInfo->rnf_date;
              $off_boarding_date = $row->getSalesUserInfo->off_boarding_date;
              $notes = $row->getSalesUserInfo->notes;
            }

            $director_code = '';
            $manager_code = '';

            // getSalesGroup
            if ($row->getSalesGroup) {
              if ($row->getSalesGroup->getGroupInfo) {
                $director_code = $row->getSalesGroup->getGroupInfo->code;
                $manager_code = $row->getSalesGroup->getGroupInfo->unit_code;
              }
            }

            $sales_rows = [
                            $agent_code,
                            $nric,
                            $salesforce_id,
                            $salutation,
                            $full_name,
                            $preferred_name,
                            $gender,
                            $date_of_birth,
                            $agent_status,
                            $mobile_no,
                            $home_address,
                            $postal_code,
                            $advisors_mobile,
                            $advisors_office_phone,
                            $designation_existing,
                            $designation_existing_per,
                            $designation_existing_date,
                            $designation_updated,
                            $designation_old,
                            $banding_rank,
                            $banding_rank_per,
                            $banding_rank_date,
                            $gi_banding_rank,
                            $latest_ytd_production,
                            $smartermail_username,
                            $agent_serial_no,
                            $director_code,
                            $manager_code,
                            $ranking,
                            $contracted_date,
                            $rnf_date,
                            $off_boarding_date,
                            $notes,
                          ];

            $sr_ctr = count($sales_rows);

            foreach ($all_categories as $ackey => $acvalue) {
              $provider_code = '';

              if (count($row->getProviderCodes) > 0) {
                $provider_codes = $row->getProviderCodes->toArray();

                $search_provider_code = array_filter($provider_codes, function($pcvalue) use ($acvalue) {
                                                      return ($pcvalue['classification_id'] == $acvalue);
                                                    });

                if (count($search_provider_code) > 0){
                  $search_provider_code = head($search_provider_code);
                  $provider_code = $search_provider_code['provider_code'];
                }
              }

              $sales_rows[$sr_ctr] = $provider_code;
              $sr_ctr++;
            }

            $sheet->appendRow($sales_rows);
          }

        });

      })->download('xls');
    }
    
    //sales logs export
    public function exportAll()
    {

      // Export Destination
      if (Request::has('export-destination')) {
        
       
           Excel::create('Sales-User-Logs', function($excel) {

          // // Set the title
          // $excel->setTitle('Our new awesome title');

          // // Chain the setters
          // $excel->setCreator('Maatwebsite')
          //       ->setCompany('Maatwebsite');

          // // Call them separately
          // $excel->setDescription('A demonstration to change the file properties');

          $excel->sheet('Sales-User-Logs', function($sheet) {

            // sort and order
            $result['sort'] = Request::input('s') ?: 'log_sales.created_at';
            $result['order'] = Request::input('o') ?: 'desc';
           
            $rows = LogSales::with('salesLog')
                            ->with('editSalesLog')
                            ->where('edit_id','=',Request::input('destination'))
                            ->get();
                    
            $sheet->appendRow(array(
                'AGENT ID', 'AGENT CODE', 'AGENT NAME', 'ACTIVITY', 'DATE', 'EDITED BY'
            ));


            foreach ($rows as $row) {
              $sheet->appendRow(array(
                $row->salesLog->salesInfo->sales_id, $row->salesLog->code, $row->salesLog->name, $row->activity, $row->created_at->toDateTimeString(), $row->editSalesLog->name
              ));
            }

          });


        })->download('xls');
        
         //return Response::json(['type' => 'success', 'body' => 'Success']);

      }

      // EXPORT ALL
      if (Request::has('sales-export-all')) {
        Excel::create('Sales-User-Logs', function($excel) {

          // // Set the title
          // $excel->setTitle('Our new awesome title');

          // // Chain the setters
          // $excel->setCreator('Maatwebsite')
          //       ->setCompany('Maatwebsite');

          // // Call them separately
          // $excel->setDescription('A demonstration to change the file properties');

          $excel->sheet('Sales-User-Logs', function($sheet) {

            // sort and order
            $result['sort'] = Request::input('s') ?: 'log_sales.created_at';
            $result['order'] = Request::input('o') ?: 'desc';

            $rows = LogSales::with('salesLog')
                            ->with('editSalesLog')
                            ->where('created_at', '>=', Request::input('from'))
                            ->where('created_at', '<=', Request::input('to'))
                            ->get();

            $sheet->appendRow(array(
                'AGENT ID', 'AGENT CODE', 'AGENT NAME', 'ACTIVITY', 'DATE', 'EDITED BY'
            ));


            foreach ($rows as $row) {
              $sheet->appendRow(array(
                $row->salesLog->salesInfo->sales_id, $row->salesLog->code, $row->salesLog->name, $row->activity, $row->created_at->toDateTimeString(), $row->editSalesLog->name
              ));
            }

          });


        })->download('xls');
      } 
      // return Response::json(['type' => 'success', 'body' => 'Success']);
    }

    /**
     * Save model
     *
     */
    public function promoteSave() {

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

          // get the user info
          $row = SalesAdvisor::find(array_get($input, 'id'));

          if(is_null($row)) {
              return Response::json(['error' => "The requested item was not found in the database."]);
          }

          $current_user = $row->user_id;
          $current_group = '';

          $get_supervisor = SalesSupervisor::find($row->supervisor_id);

          if ($get_supervisor) {
            $current_group = $get_supervisor->group_id;
          }

          // this is an existing row
          $new = false;

          $rules = [
              'unit_code' => 'required',
              'group_id' => 'required',
              'designation_id' => 'required',
          ];

          // field name overrides
          $names = [

          ];

          // do validation
          $validator = Validator::make(Input::all(), $rules);
          $validator->setAttributeNames($names);

          // return errors
          if($validator->fails()) {
              return Response::json(['error' => array_unique($validator->errors()->all())]);
          }

          $row->delete();

          $supervisor = new SalesSupervisor;
          $supervisor->user_id = $current_user;
          $supervisor->group_id = array_get($input, 'group_id');
          $supervisor->unit_code = array_get($input, 'unit_code');
          $supervisor->save();

          $get_user = Sales::where('user_id', $current_user)->first();
          if ($get_user) {
            $get_user->designation_id = array_get($input, 'designation_id');
            $get_user->save();
          
            $log_sales_current = new LogSales;
            $log_sales_current->user_id = $current_user;
            $log_sales_current->edit_id = Auth::user()->id;
            $log_sales_current->activity = 'Designation Changed';
            $log_sales_current->save();
          }

          $get_sales_groups_sup = SalesGroup::where('user_id', $current_user)->first();
          if ($get_sales_groups_sup) {
            $get_sales_groups_sup->user_id = $current_user;
            $get_sales_groups_sup->supervisor_id = $supervisor->id;
            $get_sales_groups_sup->group_id = array_get($input, 'group_id');
            $get_sales_groups_sup->save();
          } else {
            $get_sales_groups_sup = new SalesGroup;
            $get_sales_groups_sup->user_id = $current_user;
            $get_sales_groups_sup->supervisor_id = $supervisor->id;
            $get_sales_groups_sup->group_id = array_get($input, 'group_id');
            $get_sales_groups_sup->save();
          }

          $check_group = Group::find(array_get($input, 'group_id'));

          if ($current_group != array_get($input, 'group_id')) {

            $log_team = new LogTeam;
            $log_team->user_id = $current_user;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Promoted to supervisor (' . array_get($input, 'unit_code') . ') and relocated to group (' . ($check_group ? $check_group->code : '') . ').';
            $log_team->save();
          } else {

            $log_team = new LogTeam;
            $log_team->user_id = $current_user;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Promoted to supervisor (' . array_get($input, 'unit_code') . ').';
            $log_team->save();
          }

          // return
          return Response::json(['body' => 'Changes has been saved.']);
        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }


    public function getRelocateCodeAdvisor() {

      if (Request::input('id')) {
        
        $this->data['supervisor_id'] = '<option value="" class="hide">Select</option>';
        $this->data['group_id'] = '<option value="" class="hide">Select</option>';
        $this->data['id'] = '';
        $this->data['title'] = 'Relocation';

        $check_advisor = SalesAdvisor::find(Request::input('id'));

        if ($check_advisor) {
          $this->data['id'] = $check_advisor->id;
          $check_supervisor = SalesSupervisor::find($check_advisor->supervisor_id);

          $check_user = User::find($check_advisor->user_id);

          if ($check_user) {
            $this->data['title'] = 'Relocation <b>(' . $check_user->name . ')</b>';
          }

          $get_groups = Group::where('status', 1)->get();

          foreach ($get_groups as $ggkey => $ggfield) {
            $this->data['group_id'] .= '<option value="' . $ggfield->id . '" ' . ($check_supervisor ? ($ggfield->id == $check_supervisor->group_id ? 'selected' : '') : '') . '>' . $ggfield->code . '</option>';
          }

          $get_supervisors = SalesSupervisor::where('group_id', ($check_supervisor ? $check_supervisor->group_id : ''))->where('id', '!=', $check_advisor->supervisor_id)->get();

          foreach ($get_supervisors as $gskey => $gsfield) {
            $this->data['supervisor_id'] .= '<option value="' . $gsfield->id . '">' . $gsfield->unit_code . '</option>';
          }
        }

        return Response::json($this->data);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

    }

    public function getRelocationCodeAdvisor() {

      $rows = '<option value="" class="hide">Select</option>';

      if (Request::input('id') && Request::input('super_id')) {
        $check_advisor = SalesAdvisor::find(Request::input('super_id'));       

        if ($check_advisor) {
        $check_supervisor = SalesSupervisor::find($check_advisor->supervisor_id);

          if ($check_supervisor) {
            $get_supervisors = SalesSupervisor::where('group_id', Request::input('id'))->where('id', '!=', $check_advisor->supervisor_id)->get();

            foreach ($get_supervisors as $gskey => $gsfield) {
              $rows .= '<option value="' . $gsfield->id . '">' . $gsfield->unit_code . '</option>';
            }
          }
        }

        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function relocationSave() {

        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

          // get the user info
          $row = SalesAdvisor::find(array_get($input, 'id'));

          if(is_null($row)) {
              return Response::json(['error' => "The requested item was not found in the database."]);
          }

          $current_user = $row->user_id;
          $current_group = '';

          $get_supervisor = SalesSupervisor::find($row->supervisor_id);

          if ($get_supervisor) {
            $current_group = $get_supervisor->group_id;
          }

          // this is an existing row
          $new = false;

          $rules = [
              'supervisor_id' => 'required',
              'group_id' => 'required',
          ];

          // field name overrides
          $names = [

          ];

          // do validation
          $validator = Validator::make(Input::all(), $rules);
          $validator->setAttributeNames($names);

          // return errors
          if($validator->fails()) {
              return Response::json(['error' => array_unique($validator->errors()->all())]);
          }

          $row->supervisor_id = array_get($input, 'supervisor_id');
          $row->save();

          $get_sales_groups_sup = SalesGroup::where('user_id', $current_user)->first();
          if ($get_sales_groups_sup) {
            $get_sales_groups_sup->user_id = $current_user;
            $get_sales_groups_sup->supervisor_id = array_get($input, 'supervisor_id');
            $get_sales_groups_sup->group_id = array_get($input, 'group_id');
            $get_sales_groups_sup->save();
          } else {
            $get_sales_groups_sup = new SalesGroup;
            $get_sales_groups_sup->user_id = $current_user;
            $get_sales_groups_sup->supervisor_id = array_get($input, 'supervisor_id');
            $get_sales_groups_sup->group_id = array_get($input, 'group_id');
            $get_sales_groups_sup->save();
          }

          $check_group = Group::find(array_get($input, 'group_id'));

          if ($current_group != array_get($input, 'group_id')) {
            $check_group = Group::find(array_get($input, 'group_id'));
            $check_supervisor = SalesSupervisor::find(array_get($input, 'supervisor_id'));

            $log_team = new LogTeam;
            $log_team->user_id = $current_user;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Relocated to group (' . ($check_group ? $check_group->code : '') . ') - supervisor (' . ($check_supervisor ? $check_supervisor->unit_code : '') . ').';
            $log_team->save();
          } else {
            $check_supervisor = SalesSupervisor::find(array_get($input, 'supervisor_id'));

            $log_team = new LogTeam;
            $log_team->user_id = $current_user;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Relocated to supervisor (' . ($check_supervisor ? $check_supervisor->unit_code : '') . ').';
            $log_team->save();
          }
          // return
          return Response::json(['body' => 'Changes has been saved.']);
        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }
    /**
     * Save model
     *
     */
    public function demoteSave() {
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

          // get the user info
          $row = SalesSupervisor::find(array_get($input, 'id'));

          if(is_null($row)) {
              return Response::json(['error' => "The requested item was not found in the database."]);
          }

          $check_advisors = SalesAdvisor::where('supervisor_id', $row->id)->count();

          if ($check_advisors > 0) {
              return Response::json(['error' => "Access denied."]);
          }

          $current_user = $row->user_id;
          $current_group = $row->group_id;
          $current_supervisor = $row->id;

          // this is an existing row
          $new = false;


          $rules = [
              'supervisor_id' => 'required',
              'group_id' => 'required',
              'designation' => 'required',
          ];

          // field name overrides
          $names = [

          ];

          // do validation
          $validator = Validator::make(Input::all(), $rules);
          $validator->setAttributeNames($names);

          // return errors
          if($validator->fails()) {
              return Response::json(['error' => array_unique($validator->errors()->all())]);
          }

          $row->delete();

          $advisor = new SalesAdvisor;
          $advisor->user_id = $current_user;
          $advisor->supervisor_id = array_get($input, 'supervisor_id');
          $advisor->save();

          $get_user = Sales::where('user_id', $current_user)->first();
          if ($get_user) {
            $get_user->designation_id = array_get($input, 'designation');
            $get_user->save();
          
            $log_sales_current = new LogSales;
            $log_sales_current->user_id = $current_user;
            $log_sales_current->edit_id = Auth::user()->id;
            $log_sales_current->activity = 'Designation Changed';
            $log_sales_current->save();
          }

          $get_sales_groups_sup = SalesGroup::where('user_id', $current_user)->first();

          if ($get_sales_groups_sup) {
            $get_sales_groups_sup->user_id = $current_user;
            $get_sales_groups_sup->supervisor_id = array_get($input, 'supervisor_id');
            $get_sales_groups_sup->group_id = array_get($input, 'group_id');
            $get_sales_groups_sup->save();
          } else {
            $get_sales_groups_sup = new SalesGroup;
            $get_sales_groups_sup->user_id = $current_user;
            $get_sales_groups_sup->supervisor_id = array_get($input, 'supervisor_id');
            $get_sales_groups_sup->group_id = array_get($input, 'group_id');
            $get_sales_groups_sup->save();
          }

          if ($current_group != array_get($input, 'group_id')) {
            $check_group = Group::find(array_get($input, 'group_id'));
            $check_supervisor = SalesSupervisor::find(array_get($input, 'supervisor_id'));

            $log_team = new LogTeam;
            $log_team->user_id = $row->user_id;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Demoted to advisor and relocated to group (' . ($check_group ? $check_group->code : '') . ') - supervisor (' . ($check_supervisor ? $check_supervisor->unit_code : '') . ').';
            $log_team->save();
          } else {
            $check_supervisor = SalesSupervisor::find(array_get($input, 'supervisor_id'));

            $log_team = new LogTeam;
            $log_team->user_id = $row->user_id;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Demoted to advisor and assigned to supervisor (' . ($check_supervisor ? $check_supervisor->unit_code : '') . ').';
            $log_team->save();
          }

          // return
          return Response::json(['body' => 'Changes has been saved.']);
        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    public function indexTeamLog() {

        $result = $this->doListTeamLog();
        $this->data['pages'] = $result['pages'];
        $this->data['rows'] = $result['rows'];

        $this->data['refresh_route'] = url('sales/team-management/refresh/log');

        $this->data['settings'] = Settings::all();
        $this->data['designations'] = Designation::all();
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();

         // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
            $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }

        $this->data['settings'] = Settings::first();
        $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Team Management Log";
        return view('user-management.sales-users.team-management.log.list', $this->data);
    }

    /**
     * Display a listing of the sales users.
     *
     * @return Response
     */
    public function doListTeamLog() {
        // sort and order
        $result['sort'] = Request::input('sort') ?: 'log_teams.created_at';
        $result['order'] = Request::input('order') ?: 'desc';


         $per = Request::input('per') ?: 10;
         
            if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
              });


              $rows = LogTeam::join('users','users.id','=','log_teams.user_id')
                              ->join('users as editing_user','editing_user.id','=','log_teams.edit_id')
                              ->join('sales','sales.user_id','=','log_teams.user_id')
                              ->select('users.name','users.code','log_teams.activity','log_teams.created_at','log_teams.edit_id','sales.sales_id','editing_user.name as edit_name')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);

                               //dd($rows);
            } else {     
              $count = LogTeam::join('users','users.id','=','log_teams.user_id')
                              ->join('users as editing_user','editing_user.id','=','log_teams.edit_id')
                              ->join('sales','sales.user_id','=','log_teams.user_id')
                              ->select('users.name','users.code','log_teams.activity','log_teams.created_at','log_teams.edit_id','sales.sales_id','editing_user.name as edit_name')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);

                       Paginator::currentPageResolver(function () use ($count, $per) {
                       return ceil($count->total() / $per);
                       });

             $rows = LogTeam::join('users','users.id','=','log_teams.user_id')
                              ->join('users as editing_user','editing_user.id','=','log_teams.edit_id')
                              ->join('sales','sales.user_id','=','log_teams.user_id')
                              ->select('users.name','users.code','log_teams.activity','log_teams.created_at','log_teams.edit_id','sales.sales_id','editing_user.name as edit_name')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
           }

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }


    /**
     * Save model
     *
     */
    public function relocateSave() {
        $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {

          // get the user info
          $row = SalesSupervisor::find(array_get($input, 'id'));

          if(is_null($row)) {
              return Response::json(['error' => "The requested item was not found in the database."]);
          }
          // this is an existing row
          $new = false;
          
          $rules = [
              'unit_code' => 'required|min:2|max:100',
              'group_id' => 'required',
          ];

          // field name overrides
          $names = [

          ];

          // do validation
          $validator = Validator::make(Input::all(), $rules);
          $validator->setAttributeNames($names);

          // return errors
          if($validator->fails()) {
              return Response::json(['error' => array_unique($validator->errors()->all())]);
          }

          $row->unit_code = array_get($input, 'unit_code');

          if ($row->group_id != array_get($input, 'group_id')) {
            $row->group_id = array_get($input, 'group_id');

            $get_sales_groups_sup = SalesGroup::where('user_id', $row->user_id)->first();

            if ($get_sales_groups_sup) {
              $get_sales_groups_sup->user_id = $row->user_id;
              $get_sales_groups_sup->supervisor_id = $row->id;
              $get_sales_groups_sup->group_id = array_get($input, 'group_id');
              $get_sales_groups_sup->save();

              $get_sales_groups_adv = SalesGroup::where('supervisor_id', $row->id)->update(['group_id' => array_get($input, 'group_id')]);
            } else {
              $get_sales_groups_sup = new SalesGroup;
              $get_sales_groups_sup->user_id = $row->user_id;
              $get_sales_groups_sup->supervisor_id = $row->id;
              $get_sales_groups_sup->group_id = array_get($input, 'group_id');
              $get_sales_groups_sup->save();

              $get_sales_groups_adv = SalesGroup::where('supervisor_id', $row->id)->update(['group_id' => array_get($input, 'group_id')]);
            }

            $check_group = Group::find(array_get($input, 'group_id'));

            $log_team = new LogTeam;
            $log_team->user_id = $row->user_id;
            $log_team->edit_id = Auth::user()->id;
            $log_team->activity = 'Relocated supervisor (' . array_get($input, 'unit_code') . ') to group (' . ($check_group ? $check_group->code : '') . ').';
            $log_team->save();
          }

          $row->save();

          // return
          return Response::json(['body' => 'Changes has been saved.']);
        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Save model
     *
     */
    public function supervisorSave() {

        $input = Input::all();
        // check if an ID is passed

        if(array_get($input, 'id')) {

          // get the user info
          $row = SalesSupervisor::find(array_get($input, 'id'));

          if(is_null($row)) {
              return Response::json(['error' => "The requested item was not found in the database."]);
          }

          $rules = [
              'unit_code' => 'required|min:2|max:100',
              'user_id' => 'required',
          ];

          // field name overrides
          $names = [

          ];

          if (array_get($input, 'user_id')) {
            $check_rules_advisor = SalesAdvisor::where('user_id', array_get($input, 'user_id'))->first();
            if ($check_rules_advisor) {
              $rules['designation'] = 'required';
            }
          }

          if (Request::input('current')) {
            $rules['supervisor_id'] = 'required';
            $rules['sup_designation'] = 'required';
          }

          // do validation
          $validator = Validator::make(Input::all(), $rules);
          $validator->setAttributeNames($names);

          // return errors
          if($validator->fails()) {
              return Response::json(['error' => array_unique($validator->errors()->all())]);
          }

          $get_sales_supervisor = SalesSupervisor::find(array_get($input, 'id'));

          if($get_sales_supervisor) {
            $current_supervisor = $get_sales_supervisor->user_id;
            $current_code = $get_sales_supervisor->unit_code;

            if ($current_supervisor != array_get($input, 'user_id')) {

              $get_sales_supervisor->user_id = array_get($input, 'user_id');
              $get_sales_supervisor->unit_code = array_get($input, 'unit_code');
              $get_sales_supervisor->save();

              $get_sup_user = Sales::where('user_id', array_get($input, 'user_id'))->first();
              if ($get_sup_user) {
                $get_sup_user->designation_id = array_get($input, 'designation');
                $get_sup_user->save();

                $log_sales = new LogSales;
                $log_sales->user_id = array_get($input, 'user_id');
                $log_sales->edit_id = Auth::user()->id;
                $log_sales->activity = 'Designation Changed';
                $log_sales->save();
              }

              $get_sales_groups_sup = SalesGroup::where('user_id', array_get($input, 'user_id'))->first();
              if ($get_sales_groups_sup) {
                $get_sales_groups_sup->user_id = array_get($input, 'user_id');
                $get_sales_groups_sup->supervisor_id = $get_sales_supervisor->id;
                $get_sales_groups_sup->group_id = $get_sales_supervisor->group_id;
                $get_sales_groups_sup->save();
              } else {
                $get_sales_groups_sup = new SalesGroup;
                $get_sales_groups_sup->user_id = array_get($input, 'user_id');
                $get_sales_groups_sup->supervisor_id = $get_sales_supervisor->id;
                $get_sales_groups_sup->group_id = $get_sales_supervisor->group_id;
                $get_sales_groups_sup->save();
              }

              $del_advisor = SalesAdvisor::where('user_id', array_get($input, 'user_id'))->delete();

              if (Request::input('current')) {
                $new_supervisor = new SalesAdvisor;
                $new_supervisor->user_id = $current_supervisor;
                $new_supervisor->supervisor_id = array_get($input, 'supervisor_id');
                $new_supervisor->save();

                $get_sales_groups = SalesGroup::where('user_id', $current_supervisor)->first();
                if ($get_sales_groups) {
                  $get_sales_groups->user_id = $current_supervisor;
                  $get_sales_groups->supervisor_id = array_get($input, 'supervisor_id');
                  $get_sales_groups->group_id = $get_sales_supervisor->group_id;
                  $get_sales_groups->save();
                } else {
                  $get_sales_groups = new SalesGroup;
                  $get_sales_groups->user_id = $current_supervisor;
                  $get_sales_groups->supervisor_id = array_get($input, 'supervisor_id');
                  $get_sales_groups->group_id = $get_sales_supervisor->group_id;
                  $get_sales_groups->save();
                }

                $get_rel_user = Sales::where('user_id', $current_supervisor)->first();
                if ($get_rel_user) {
                  $get_rel_user->designation_id = array_get($input, 'sup_designation');
                  $get_rel_user->save();
                
                  $log_sales_current = new LogSales;
                  $log_sales_current->user_id = $current_supervisor;
                  $log_sales_current->edit_id = Auth::user()->id;
                  $log_sales_current->activity = 'Designation Changed';
                  $log_sales_current->save();
                }
              } else {
                $new_supervisor = new SalesSupervisor;
                $new_supervisor->user_id = $current_supervisor;
                $new_supervisor->unit_code = $current_code;
                $new_supervisor->group_id = $get_sales_supervisor->group_id;
                $new_supervisor->save();

                $get_sales_groups = SalesGroup::where('user_id', $current_supervisor)->first();
                if ($get_sales_groups) {
                  $get_sales_groups->user_id = $current_supervisor;
                  $get_sales_groups->supervisor_id = $new_supervisor->id;
                  $get_sales_groups->group_id = $get_sales_supervisor->group_id;
                  $get_sales_groups->save();
                } else {
                  $get_sales_groups = new SalesGroup;
                  $get_sales_groups->user_id = $current_supervisor;
                  $get_sales_groups->supervisor_id = $new_supervisor->id;
                  $get_sales_groups->group_id = $get_sales_supervisor->group_id;
                  $get_sales_groups->save();
                }
              }

              $check_group_owner = Group::where('user_id', $current_supervisor)->first();

              if ($check_group_owner) {
                $check_group_owner->user_id = array_get($input, 'user_id');
                $check_group_owner->save();

                $log_team = new LogTeam;
                $log_team->user_id = array_get($input, 'user_id');
                $log_team->edit_id = Auth::user()->id;
                $log_team->activity = 'Promoted to supervisor (' . array_get($input, 'unit_code') . ') and assigned as group owner for (' . $check_group_owner->code . ').';
                $log_team->save();
              } else {
                $log_team = new LogTeam;
                $log_team->user_id = array_get($input, 'user_id');
                $log_team->edit_id = Auth::user()->id;
                $log_team->activity = 'Promoted to supervisor (' . array_get($input, 'unit_code') . ').';
                $log_team->save();
              }
            }
          }


          $row->unit_code = array_get($input, 'unit_code');
          $row->save();

          // return
          return Response::json(['body' => 'Changes has been saved.']);
        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }

      }

    public function getRelocateCode() {

      $this->data['rows'] = '';
      $this->data['unit_code'] = '';
      $this->data['id'] = '';
      $this->data['title'] = '';

      $rows = '<option value="" class="hide">Select</option>';

      if (Request::input('id')) {

        $get_supervisors = SalesSupervisor::find(Request::input('id'));

        if ($get_supervisors) {
          $this->data['unit_code'] = $get_supervisors->unit_code;
          $this->data['id'] = $get_supervisors->id;
          $get_groups = Group::where('status', 1)->get();

          foreach ($get_groups as $key => $field) {
            $rows .= '<option value="' . $field->id . '" ' . ($field->id == $get_supervisors->group_id ? 'selected' : '' ) . '>' . $field->code . '</option>';
          }
        }
      }
      $this->data['rows'] = $rows;

      return Response::json($this->data);
    }

    public function getSupervisorCode() {
      
      $id = Request::input('id');

      if ($id) {
        $get_supervisors = SalesSupervisor::find($id);

        if ($get_supervisors) {

          $get_designations = Designation::where('rank', 'Advisor')->get();

          $this->data['sup_designation'] = '<label for="sup_designation" class="col-sm-3 col-xs-5">NEW DESIGNATION</label>
                <div class="col-sm-9 col-xs-7 error-sup_designation">
                <select class="form-control" id="code-sup_designation" name="sup_designation">
                <option class="hide" value="">Select</option>';

          foreach ($get_designations as $gskey => $gsfield) {
            $this->data['sup_designation'] .= '<option value="' . $gsfield->id . '">' . $gsfield->designation . '</option>';
          }

          $this->data['sup_designation'] .= '<select><sup class="sup-errors"></sup></div>';

          $get_sales_supervisor = SalesSupervisor::where('group_id', $get_supervisors->group_id)->get();

          $this->data['rows'] = '<label for="supervisor_id" class="col-sm-3 col-xs-5">NEW MANAGER</label>
                <div class="col-sm-9 col-xs-7 error-supervisor_id">
                <select class="form-control" id="code-supervisor_id" name="supervisor_id">
                <option class="hide" value="">Select</option>';

          foreach ($get_sales_supervisor as $key => $field) {
            $this->data['rows'] .= '<option value="' . $field->id . '" ' . ($id == $field->id ? 'selected' : '') . '>' . $field->unit_code . '</option>';
          }

          $this->data['rows'] .= '<select><sup class="sup-errors"></sup></div>';

          return Response::json($this->data);
        }
      }

    }

    public function getSupervisorAdvisor() {

      $check_if_advisor = SalesAdvisor::where('user_id', Request::input('id'))->first();

      $check_if_sales_supervisor = SalesSupervisor::find(Request::input('super'));

      $name = null;

      if ($check_if_sales_supervisor) {
        $check_user = User::find($check_if_sales_supervisor->user_id);
        if ($check_user) {
          $name = $check_user->name;
        }
      }

      $this->data['rows'] = '';
      $this->data['designation'] = '';
      $this->data['code'] = '';

      if ($check_if_advisor) {
        $this->data['rows'] = '<label for="user_id" class="col-sm-3 col-xs-5"></label>
                <div class="col-sm-9 col-xs-7">
                <input name="current" id="code-current" type="checkbox" value="yes"> Make the <b>current supervisor (' . $name . ')</b> an <b>advisor</b>.
                </div>
                <script>
                  $("#code-current").on("change", function() {
                    $id = $("#code-id").val();
                    $("#code-codes").html("");
                    $("#code-sup_designation").html("");
                    if($(this).is(":checked")) {
                      $.post("' . url('sales/team-management/supervisor/get-code') . '", { id: $id, _token: $_token }, function(response) {
                        $("#code-codes").html(response.rows);
                        $("#code-sup_designation").html(response.sup_designation);
                      }, "json");
                    } else {
                      $("#code-codes").html("");
                    }
                  });
                </script>';


        $this->data['designation'] = '<label for="designation" class="col-sm-3 col-xs-5">NEW DESIGNATION</label>
                <div class="col-sm-9 col-xs-7 error-designation"><select class="form-control" id="code-designation" name="designation">
                <option class="hide" value="">Select</option>';

        $get_designations = Designation::where('rank', 'Supervisor')->get();

        foreach ($get_designations as $key => $field) {
          $this->data['designation'] .= '<option value="' . $field->id . '">' . $field->designation . '</option>';
        }

        $this->data['designation'] .= '<select><sup class="sup-errors"></sup></div>';

      } else {
        $get_code = SalesSupervisor::where('user_id', Request::input('id'))->first();
        if ($get_code) {
          $this->data['code'] = $get_code->unit_code;
        }
      }

      return Response::json($this->data);
    }

    /**
     * Save model
     *
     */
    public function groupSave() {

        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Group::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'code' => 'required|min:2|max:100',
            'manager_code' => 'required|min:2|max:100',
            'owner' => 'required',
        ];

        if (array_get($input, 'owner')) {
          $check_rules_advisor = SalesAdvisor::where('user_id', array_get($input, 'owner'))->first();
          if ($check_rules_advisor) {
            $rules['designation'] = 'required';
          }
        }

        // field name overrides
        $names = [
            'code' => 'group code',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $search_user = User::find(array_get($input, 'owner'));

        if ($search_user) {
          // create model if new

          $get_sales = Sales::where('user_id', $search_user->id)->first();
          if ($get_sales) {
            $check_supervisor = SalesSupervisor::where('user_id', $search_user->id)->first();
            $check_advisor = SalesAdvisor::where('user_id', $search_user->id)->first();
            
            if ($check_supervisor) {
              if($new) {
                $row = new Group;
                $row->code  = array_get($input, 'code');
                $row->save();

                $get_sales_supervisor = SalesSupervisor::where('user_id', array_get($input, 'owner'))->first();

                if ($get_sales_supervisor) {
                  $get_sales_supervisor->group_id = $row->id;
                  $get_sales_supervisor->unit_code = array_get($input, 'manager_code');
                  $get_sales_supervisor->save();
                } else {
                  $get_sales_supervisor = new SalesSupervisor;
                  $get_sales_supervisor->group_id = $row->id;
                  $get_sales_supervisor->unit_code = array_get($input, 'manager_code');
                  $get_sales_supervisor->save();
                }

                $put_group_id = Group::find($row->id);
                $put_group_id->group_id = 'GROUP-' . sprintf("%04d", $row->id);
                $put_group_id->user_id = array_get($input, 'owner');
                $put_group_id->save();

                $get_sales_groups = SalesGroup::where('user_id', array_get($input, 'owner'))->first();
                if ($get_sales_groups) {
                  $get_sales_groups->user_id = array_get($input, 'owner');
                  $get_sales_groups->supervisor_id = $get_sales_supervisor->id;
                  $get_sales_groups->group_id = $put_group_id->id;
                  $get_sales_groups->save();
                } else {
                  $get_sales_groups = new SalesGroup;
                  $get_sales_groups->user_id = array_get($input, 'owner');
                  $get_sales_groups->supervisor_id = $get_sales_supervisor->id;
                  $get_sales_groups->group_id = $put_group_id->id;
                  $get_sales_groups->save();
                }

                $log_team = new LogTeam;
                $log_team->user_id = array_get($input, 'owner');
                $log_team->edit_id = Auth::user()->id;
                $log_team->activity = 'Assigned as group owner for (' . array_get($input, 'code') . ').';
                $log_team->save();

              } else {

                $row->code  = array_get($input, 'code');
                $row->user_id = array_get($input, 'owner');
                $row->save();

                if($row->user_id != array_get($input, 'owner')) {
                  $log_team = new LogTeam;
                  $log_team->user_id = array_get($input, 'owner');
                  $log_team->edit_id = Auth::user()->id;
                  $log_team->activity = 'Assigned as group owner for (' . array_get($input, 'code') . ').';
                  $log_team->save();
                }
              }
            } else if  ($check_advisor) {

              if($new) {
                $row = new Group;
                $row->code  = array_get($input, 'code');

                $row->save();

                $get_sales = Sales::where('user_id', array_get($input, 'owner'))->first();

                if ($get_sales) {
                  $get_sales->designation_id = array_get($input, 'designation');
                  $get_sales->save();

                  $log_sales = new LogSales;
                  $log_sales->user_id = $get_sales->user_id;
                  $log_sales->edit_id = Auth::user()->id;
                  $log_sales->activity = 'Designation Changed';
                  $log_sales->save();
                }

                $get_sales_advisor = SalesAdvisor::where('user_id', array_get($input, 'owner'))->delete();

                $create_supervisor = new SalesSupervisor;
                $create_supervisor->unit_code = array_get($input, 'manager_code');
                $create_supervisor->user_id = array_get($input, 'owner');
                $create_supervisor->group_id = $row->id;
                $create_supervisor->save();

                $put_group_id = Group::find($row->id);
                $put_group_id->group_id = 'GROUP-' . sprintf("%04d", $row->id);
                $put_group_id->user_id = array_get($input, 'owner');
                $put_group_id->save();

                $get_sales_groups = SalesGroup::where('user_id', array_get($input, 'owner'))->first();
                if ($get_sales_groups) {
                  $get_sales_groups->user_id = array_get($input, 'owner');
                  $get_sales_groups->supervisor_id = $create_supervisor->id;
                  $get_sales_groups->group_id = $put_group_id->id;
                  $get_sales_groups->save();
                } else {
                  $get_sales_groups = new SalesGroup;
                  $get_sales_groups->user_id = array_get($input, 'owner');
                  $get_sales_groups->supervisor_id = $create_supervisor->id;
                  $get_sales_groups->group_id = $put_group_id->id;
                  $get_sales_groups->save();
                }

                $log_team = new LogTeam;
                $log_team->user_id = array_get($input, 'owner');
                $log_team->edit_id = Auth::user()->id;
                $log_team->activity = 'Assigned as group owner for (' . array_get($input, 'code') . ').';
                $log_team->save();

              } else {

                if($row->user_id != array_get($input, 'owner')) {

                  $get_sales = Sales::where('user_id', array_get($input, 'owner'))->first();

                  if ($get_sales) {
                    $get_sales->designation_id = array_get($input, 'designation');
                    $get_sales->save();

                    $log_sales = new LogSales;
                    $log_sales->user_id = $get_sales->user_id;
                    $log_sales->edit_id = Auth::user()->id;
                    $log_sales->activity = 'Designation Changed';
                    $log_sales->save();
                  }

                  $get_sales_advisor = SalesAdvisor::where('user_id', array_get($input, 'owner'))->delete();

                  $create_supervisor = new SalesSupervisor;
                  $create_supervisor->unit_code = array_get($input, 'manager_code');
                  $create_supervisor->user_id = array_get($input, 'owner');
                  $create_supervisor->group_id = $row->id;
                  $create_supervisor->save();

                  $row->code  = array_get($input, 'code');
                  $row->user_id = array_get($input, 'owner');
                  $row->save();

                  $get_sales_groups = SalesGroup::where('user_id', array_get($input, 'owner'))->first();
                  if ($get_sales_groups) {
                    $get_sales_groups->user_id = array_get($input, 'owner');
                    $get_sales_groups->supervisor_id = $create_supervisor->id;
                    $get_sales_groups->group_id = $row->id;
                    $get_sales_groups->save();
                  } else {
                    $get_sales_groups = new SalesGroup;
                    $get_sales_groups->user_id = array_get($input, 'owner');
                    $get_sales_groups->supervisor_id = $create_supervisor->id;
                    $get_sales_groups->group_id = $row->id;
                    $get_sales_groups->save();
                  }

                  $log_team = new LogTeam;
                  $log_team->user_id = array_get($input, 'owner');
                  $log_team->edit_id = Auth::user()->id;
                  $log_team->activity = 'Promoted to supervisor (' . array_get($input, 'manager_code') . ') and assigned as group owner for (' . array_get($input, 'code') . ').';
                  $log_team->save();

                }
              }
            }
          }

          // return
          return Response::json(['body' => 'Changes has been saved.']);
        }

    }

    public function groupView() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Group::find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }
    public function notification($id){  
       $row = NotificationPerUser::where(function($query) use ($id) {
                                      $query->where('user_id', '=', Auth::user()->id)
                                            ->where('notification_id', '=', $id);

                                  })->update(['is_read' => 1]);
      return Redirect::to('/policy/production/case-submission');
   }

}
