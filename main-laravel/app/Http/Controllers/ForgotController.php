<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Validator;
use Input;
use Request;
use Response;
use Hash;
use Mail;
use App\User;

class ForgotController extends Controller
{
    public function SendMail(){

        $system_users = User::where('usertype_id', '!=', 8)->where('status', '!=', '2')->lists('email')->toArray();
        $inside = in_array(Request::input('email'), $system_users);
        $messages = [
                        'email.required' => 'Input an email.',
                        'email.email' => 'The email address you entered is not valid.',
                        'nric.required' => 'NRIC is required.',
                        'code.required' => 'Representative code is required'
                    ];

        $validator = Validator::make(request()->all(), [
            'email' => 'required|email',
            'nric' => ($inside ? '' : 'required'),
            'code' => ($inside ? '' : 'required'),
        ], $messages);

        if($validator->fails())
        {
            $response['code'] = 0;
            $response['errors'] = $validator->errors()->first();
            return $response;
        }
        else
        {
            // techsupport@legacyfa-asia.com

            $agent_email = Request::input('email');
            $agent_nric = Request::input('nric');
            $agent_code = Request::input('code');
            $agent_check = User::leftjoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'users.id')
                            ->select('users.*', 'sales_user_extension.user_id', 'sales_user_extension.nric', 'sales_user_extension.salesforce_id')
                            ->where(function($query) use ($agent_email, $agent_nric, $agent_code, $inside){
                                if($inside)
                                {
                                    $query->where('users.email', $agent_email)
                                          ->where('users.status', '!=', 2)
                                          ->where('users.usertype_id', '!=', 8);
                                }
                                else
                                {
                                    $query->where('sales_user_extension.nric', $agent_nric)
                                          ->where('sales_user_extension.salesforce_id', $agent_code)
                                          ->where('users.email', $agent_email)
                                          ->where('users.status', 1)
                                          ->where('users.usertype_id', 8);
                                }
                            })
                            ->first();
          
            if($agent_check)
            {
                $nric_first = strtolower(substr($agent_check->nric, 0, 1));         
                $nric_middle = preg_replace('/[^0-9]/', '', $agent_check->nric);
                $nric_last = strtoupper(substr($agent_check->nric, -1)).'!';
                $new_password = $nric_first.$nric_middle.$nric_last;
                $agent_check->password = Hash::make($new_password);
                $agent_check->save();

                Mail::send('email.forgot', ['email' => $agent_email, 'name' => $agent_check->preferred_name], function ($m) use ($agent_check) {
                    $m->subject('LegacyFA - Password Reset');
                    $m->from('techsupport@legacyfa-asia.com', 'LegacyFA');
                    $m->to($agent_check->email);
                    $m->cc('techsupport@legacyfa-asia.com');
                });
              
                $response['code'] = 1;
                $response['message'] = 'Password has been reset. Check your email.';
                return $response;
            }
            else
            {
                $response['code'] = 0;
                $response['errors'] = 'Account not found.';
                return $response;  
            }

        }

    }
}