<?php

namespace App\Http\Controllers;

use Request;
use Excel;
use Response;
use App\UploadFeed;
use Auth;
use Carbon;
use View;
use Validator;
use Input;
use Config;
use Paginator;
use DB;

use App\User;
use App\GI;
use App\Permission;
use App\ProductionCases;
use App\Policy;
use App\PolicyContract;
use App\FeedType;
use App\ProviderCategory;
use App\ProviderCodes;
use App\ProviderClassification;
use App\ProviderFeed;
use App\SalesBonding;
use App\BatchPolicy;
use App\Nationality;
use App\PrePayroll;
use App\PrePayrollBSC;
use App\PrePayrollBSCUser;
use App\PrePayrollComputation;
use App\Introducer;
use App\AssignedPolicy;
use App\PayrollComputation;
use App\Batch;
use App\BatchMonth;
use App\BatchMonthUser;
use App\BatchMonthSupervisor;
use App\BatchMonthGroup;
use App\Group;
use App\Sales;
use App\SalesDesignation;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\Settings;
use App\GroupComputation;
use App\SalesSupervisorComputation;
use App\SalesAdvisorComputation;
use App\Product;
use App\ProductGross;
use App\Provider;
use App\DataFeed;
use App\Http\Requests;
use Redirect;
use Crypt;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;

class PayrollController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    /**
     * Display a listing of the uploaded data feeds.
     *
     * @return Response
     */
    public function index()
    {

      // $ids = [59, 60, 61, 62, 63, 64, 65, 66, 68];
      // $count = Policy::whereIn('upload_id', $ids)->whereNotNull('user_id')->lists('id');

      // dd($count);

      // $count_p = PayrollComputation::whereIn('policy_id', $count)->lists('policy_id');
      // dd($count_p);

      // $get_policy = Policy::whereNotNull('incept_date')->first();
      // dd($get_policy);

      // $get_payroll = PayrollComputation::find(59);
      // $get_payroll->incept_date = $get_policy->incept_date;
      // $get_payroll->save();
      // dd($get_payroll);
      // $get_users = BatchMonthUser::where('designation_id', '=', null)->get();;

      // foreach ($get_users as $key => $row) {
      //   $get_designation = Sales::where('user_id', $row->user_id)->first();
      //   if($get_designation) {
          
      //     $row->designation_id = $get_designation->designation_id;
      //     $row->save();
      //   }

      // }

      // dd($get_users);

      $result = $this->doList();

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];

      $this->data['title'] = "Payroll Management";

      $this->data['providers'] = Provider::where('status', '=', 1)->get();

      $this->data['feeds'] = FeedType::all();

      $this->data['permission_payroll'] = Permission::where(function($query) {
                                                          $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                                                ->where('module_id', '=', '2');
                                                      })->first();

      $this->data['permission'] = Permission::where(function($query) {
                                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                                        ->where('module_id', '=', '1');
                                              })->first();

      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                              ->select('sales.*','designations.*')
                                              ->where('sales.user_id','=',Auth::user()->id)
                                              ->first();

      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();

      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();

      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

      // for notification
      if ( Auth::user()->usertype_id == '8') { //if sales user
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                                ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                                ->select('users.*','notifications_per_users.*','notifications.*')
                                                ->where(function($query) {
                                                      $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                            ->where('notifications.usertype_id', '=', '1')
                                                            ->where('notifications_per_users.is_read', '=', '0');      
                                                  })->get();
      } else {
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                  ->select('users.*','notifications_per_users.*','notifications.*')
                                ->where(function($query) {
                                      $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                            ->where('notifications.usertype_id', '=', '2')
                                            ->where('notifications_per_users.is_read', '=', '0'); 
                                  })->get();
      } 

      $this->data['settings'] = Settings::first();
      $this->data['noti_count'] = count($this->data['notification']);

      return view('payroll-management.upload-data-feeds.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
        // sort and order
        $result['sort'] = Request::input('sort') ?: 'created_at';
        $result['order'] = Request::input('order') ?: 'desc';

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          $rows = UploadFeed::with('getFeedProvider')->with('getFeedType')
                                  ->select('upload_feeds.*', 'providers.name', 'provider_classifications.name as feed_type')
                                  ->leftJoin('providers', 'providers.id','=','upload_feeds.provider_id')
                                  ->leftJoin('provider_classifications', 'provider_classifications.id','=','upload_feeds.category_id')
                                  ->orderBy($result['sort'], $result['order'])
                                  ->paginate(10);
        } else {
          $count = UploadFeed::paginate(10);

          Paginator::currentPageResolver(function () use ($count) {
              return ceil($count->total() / 10);
          });

          $rows = UploadFeed::with('getFeedProvider')->with('getFeedType')
                                  ->select('upload_feeds.*', 'providers.name')
                                  ->join('providers', 'providers.id','=','upload_feeds.provider_id')
                                  ->orderBy($result['sort'], $result['order'])
                                  ->paginate(10);
        }

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            // dd($result);
            return $result;
        }
    }



    /**
     * Display a listing of the generated payrolls.
     *
     * @return Response
     */
    public function indexGenerate()
    {
        $result = $this->doListGenerate();
        

        // $get_payroll = PayrollComputation::where('pfr_check', null)->get();


        // foreach ($get_payroll as $key => $row) {

        // $get_main = Product::find($row->product_id);

        // if ($get_main) {

        //   $get_pcase = ProductionCases::where(function($query) use ($row) { 
        //                                   $query->where('fullname', $row->policy_holder)
        //                                         ->orWhere('fullname_first', $row->policy_holder);
        //                                 })
        //                               ->where('main_product_id', $get_main->main_id)
        //                               ->where('user_id', $row->user_id)
        //                               ->whereNotNull('upload')
        //                               ->first();

        // $get_pcase1 = ProductionCases::where(function($query) use ($row) { 
        //                                 $query->where('fullname', $row->policy_holder)
        //                                       ->orWhere('fullname_first', $row->policy_holder);
        //                               })
        //                             ->where('user_id', $row->user_id)
        //                             ->whereNotNull('upload')
        //                             ->first();

        //   $get_pcase2 = ProductionCases::where('main_product_id', $get_main->main_id)
        //                               ->where(function($query) use ($row) { 
        //                                   $query->where('fullname', $row->policy_holder)
        //                                         ->orWhere('fullname_first', $row->policy_holder);
        //                                 })
        //                               ->whereNotNull('upload')
        //                               ->first();
          
        //   if ($get_pcase) {
        //     $row->pfr_link = $get_pcase->upload;
        //     $row->save();
        //   } else if ($get_pcase1) {
        //     $row->pfr_link = $get_pcase1->upload;
        //     $row->save();
        //   } else if ($get_pcase2) {
        //     $row->pfr_link = $get_pcase2->upload;
        //     $row->save();
        //   }
        // }

        // $row->pfr_check = "Y";
        // $row->save();
        //   # code...
        // }


        // dd("Sampel");

        // $get_pcase = ProductionCases::whereNull('fullname_first')->get();

        // foreach ($get_pcase as $key => $row) {
          
        //   $row->fullname_first = $row->firstname . ' ' . $row->lastname;
        //   $row->save();
        // }

        // dd('tapos');

        // $get_users = User::with('salesInfo')
        //                   ->where(function($query) {
        //                     $query->where('status', 1)
        //                           ->where('usertype_id', 8);
        //                   })->lists('id');

        //                   // dd($get_users);
        // $get_sales = Sales::whereIn('user_id', $get_users)->get()->toArray();

        //                   dd($get_sales);
//         $get_payroll = PayrollComputation::where('batch_id', 1)->get()->take(10);

//         dd($get_payroll);
// foreach ($get_payroll as $key => $field) {

//                 $get_product = Product::where(function($query) use ($field) { 
//                                                         $query->where('code', 'LIKE', $field->compo_code)
//                                                               ->where('term', '<=', $field->premium_term)
//                                                               ->where('endterm', '>=', $field->premium_term)
//                                                               ->where('status', 1);
//                                                       })->first();
                
//                 $var_income = 0;                              
//                 if ($get_product) {
                  
//                   if ($first_year_com == 0) {
//                     $first_year_com = 1;
//                   }

//                   $frequency = 1;
//                   if (strtolower($field->billing_freq) === "yearly") {
//                     $frequency = 12;
//                   } else if (strtolower($field->billing_freq) === "monthly") {
//                     $frequency = 1;
//                   }

//                   $get_rev = ProductGross::where(function($query) use ($get_product, $first_year_com) {
//                                                     $query->where('product_id', '=', $get_product->id);
//                                                   })->lists('value');

//                   foreach ($get_rev as $grkey => $grvalue) {
//                     $var_income += ($field->net_prem_paid * $frequency) * ($grvalue / 100);
//                   }

//                   $check_rev_year = ProductGross::where(function($query) use ($get_product, $first_year_com) {
//                                                     $query->where('product_id', '=', $get_product->id)
//                                                           ->where('year_id', '=', floor($first_year_com));
//                                                   })->first();
//                   if ($check_rev_year) {
//                     $gross_prod_rev_per = $check_rev_year->value;
//                     $gross_prod_rev = $field->net_prem_paid * ($check_rev_year->value / 100);
//                   }

//                 }
// }


        // $get_pol = Policy::where('contract_no', '6033374')->where('premium_term', 68)->first();

        // $get_prod = Product::where(function($query) use ($get_pol) {
        //                             $query->where('code', $get_pol->compo_code)
        //                                   ->where('term', '<=', $get_pol->premium_term)
        //                                   ->where('endterm', '>=', $get_pol->premium_term);
        //                           })->get();

        // $check_rev_year = ProductGross::where(function($query) use ($get_prod) {
        //                                   $query->where('product_id', '=', $get_prod->id)
        //                                         ->where('year_id', '=', 1);
        //                                 })->first();

        // dd($check_rev_year);

        // $get_batch = PayrollComputation::where('batch_id', 8)->sum('agent_banding');
        // dd($get_batch);
        $this->data['rows'] = $result['rows'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Payroll Management";
        return view('payroll-management.generate-payroll.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListGenerate() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $rows = Batch::where('status', '!=', 2)->orderBy($result['sort'], $result['order'])->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    public function getBatch() {

        $rows = Batch::find(Request::input('id'));

        if ($rows) {
          return Response::json($rows);
        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
    
    }

    public function updateBatch() {

      $input = Request::all();

        // check if an ID is passed
        if(array_get($input, 'update_id')) {

            // get the user info
            $row = Batch::find(array_get($input, 'update_id'));

            if(!$row) {
                return Response::json(['error' => ["The requested item was not found in the database."]]);
            }

        }

        $rules = [
          'gross_revenue' => 'required|numeric',
          'firm_revenue' => 'required|numeric',
          'buyout_per' => 'required|numeric',
        ];

        // field name overrides
        $names = [
          'gross_revenue' => 'Gross Revenue Percentage',
          'firm_revenue' => 'Firm Revenue Percentage',
          'buyout_per' => 'Buyout Percentage',
          'notes' => 'Footer Notes',
        ];
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $row->gross_revenue     = array_get($input, 'gross_revenue');
        $row->firm_revenue      = array_get($input, 'firm_revenue');
        $row->buyout_per     = array_get($input, 'buyout_per');

        if (array_get($input, 'notes')) {
          $row->notes    = array_get($input, 'notes');
        }

        // save model
        $row->save();
        
        // return
        return Response::json(['body' => 'Batch updated successfully.']);
    
    }

    /**
     * Display a listing of the admin pre-payroll entries.
     *
     * @return Response
     */
    public function indexPrePayroll()
    {
        $result = $this->doListPrePayroll();

        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];

        $this->data['sales'] = $result['sales'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Payroll Management";
        return view('payroll-management.admin-pre-payroll-entries.list', $this->data);
    }
    
    /**
     * Build the list
     *
     */
    public function doListPrePayroll() {
        // sort and order
        $result['sort'] = Request::input('sort') ?: 'batches.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $per = Request::input('per') ?: 10;

        if (Auth::user()->usertype_id == 8) {
          $batch_ids = array_unique(PrePayroll::where('user_id', '=', Auth::user()->id)->lists('batch_id')->toArray());

          $rows = Batch::select('batches.*', 'pre_payrolls.cost')
                        ->leftJoin('pre_payrolls', function ($join) {
                            $join->on('batches.id', '=', 'pre_payrolls.batch_id');
                        })
                        ->selectRaw('sum(incentives) as total_incentives')
                        ->selectRaw('sum(deductions) as total_deductions')
                        ->where('batches.status', '!=', 2)
                        ->whereIn('batches.id', $batch_ids)
                        ->groupBy('batches.id')
                        ->orderBy($result['sort'], $result['order'])->paginate($per);
                        
        } else {
          if (Request::input('page') != '»') {
            Paginator::currentPageResolver(function () {
                return Request::input('page'); 
            });

              $rows = Batch::select('batches.*', 'pre_payrolls.cost')
                      ->leftJoin('pre_payrolls', function ($join) {
                          $join->on('batches.id', '=', 'pre_payrolls.batch_id');
                      })
                      ->selectRaw('sum(incentives) as total_incentives')
                      ->selectRaw('sum(deductions) as total_deductions')
                      ->where('batches.status', '!=', 2)
                      ->groupBy('batches.id')
                      ->orderBy($result['sort'], $result['order'])->paginate($per);

            $table = '';
            if(Request::ajax()) {
              foreach ($rows as $as => $row) {

                $table .= '<tr data-id="' . $row->id . '">
                  <td><a class="btn btn-table btn-xs btn-expand"><i class="fa fa-plus"></i></a> ' . $row->name . '</td>
                  <td>' . date_format(date_create(substr($row->start, 0,10)),'m/d/Y') . '</td>
                  <td>' . date_format(date_create(substr($row->end, 0,10)),'m/d/Y') . '</td>
                  <td>' . number_format($row->total_incentives, 2) . '</td>
                  <td>' . number_format($row->total_deductions, 2) . '</td>
                  <td class="rightalign">'
                  . ($row->status == 0 ? 'Unreleased' : 'Released' ) .
                  '</td>
                </tr>';

              }
            }
          } else {
            $count = Batch::select('batches.*', 'pre_payrolls.cost')
                            ->leftJoin('pre_payrolls', function ($join) {
                                $join->on('batches.id', '=', 'pre_payrolls.batch_id');
                              })
                            ->selectRaw('sum(incentives) as total_incentives')
                            ->selectRaw('sum(deductions) as total_deductions')
                            ->where('batches.status', '!=', 2)
                            ->groupBy('batches.id')
                            ->orderBy($result['sort'], $result['order'])->paginate($per);

            Paginator::currentPageResolver(function () use ($count, $per) {
                return ceil($count->total() / $per);
            });

            $rows = Batch::select('batches.*', 'pre_payrolls.cost')
                    ->leftJoin('pre_payrolls', function ($join) {
                        $join->on('batches.id', '=', 'pre_payrolls.batch_id');
                    })
                    ->selectRaw('sum(incentives) as total_incentives')
                    ->selectRaw('sum(deductions) as total_deductions')
                    ->where('batches.status', '!=', 2)
                    ->groupBy('batches.id')
                    ->orderBy($result['sort'], $result['order'])
                    ->paginate($per);

            $table = '';
            if(Request::ajax()) {
              foreach ($rows as $as => $row) {

                $table .= '<tr data-id="' . $row->id . '">
                  <td><a class="btn btn-table btn-xs btn-expand"><i class="fa fa-plus"></i></a> ' . $row->name . '</td>
                  <td>' . date_format(date_create(substr($row->start, 0,10)),'m/d/Y') . '</td>
                  <td>' . date_format(date_create(substr($row->end, 0,10)),'m/d/Y') . '</td>
                  <td>' . number_format($row->total_incentives, 2) . '</td>
                  <td>' . number_format($row->total_deductions, 2) . '</td>
                  <td class="rightalign">'
                  . ($row->status == 0 ? 'Unreleased' : 'Released' ) .
                  '</td>
                </tr>';

              }
            }

          }
        }

        //get sales information
        $sales = User::where(function($query) {
                            $query->where('usertype_id', '=', 8)
                                  ->where('status', '!=', 2);
                        })->with('salesInfo')->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $table;
            $result['sales'] = $sales->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());


            $result['rows'] = $rows;
            $result['sales'] = $sales;
            return $result;
        }
    }

    public function prePayrollView() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = PrePayroll::with('getPrePayrollPolicy')
                          ->with('getPrePayrollBatchPolicy.getBatchPolicyInfo')
                          ->with('getPrePayrollPolicy.getPolicySales')
                          ->with('getPrePayrollPolicy.getPolicySales.getUserPolicies')
                          ->with('getPrePayrollPolicy.getPolicySales.getUserPolicies.getPolicyProvider')
                          ->find($id);
        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }


    public function getPrePayrollBatch() {

      // $id = Request::input('id');

      // $batch_ids = [];
      // $bctr = 0;
      // $yes_batch = 'yes';
      // foreach (array_filter($id) as $bkey => $bvalue) {
      //   $batch_policy = PayrollComputation::where('user_id', $bvalue)->lists('batch_id');
      //   if (count($batch_policy) > 0) {
      //     array_push($batch_ids, $batch_policy);
      //     $bctr += 1;
      //   } else {
      //     $yes_batch = 'no';
      //   }
      // }

      // if ($yes_batch == 'yes' && count(array_filter($id)) > 0) {

      //   $get_batch = [];
      //   $ctr = 0;
      //   foreach ($batch_ids as $key => $value) {
      //     foreach (array_unique($value->toArray()) as $vkey => $vvalue) {
      //       $check_batch = Batch::where(function($query) use ($id, $vvalue) {
      //                                         $query->where('id', '=', $vvalue)
      //                                               ->where('status', '=', 0);
      //                                 })->first();

      //       if ($check_batch) {
      //         array_push($get_batch, $check_batch->id);
      //       }

      //       $ctr += 1;
      //     }
          
      //   }

      //   $batches = [];
      //   $batctr = 0;
      //   foreach (array_unique($get_batch) as $gbkey => $gbvalue) {

      //     $if_batch = Batch::where(function($query) use ($id, $gbvalue) {
      //                                       $query->where('id', '=', $gbvalue)
      //                                             ->where('status', '=', 0);
      //                               })->first();

      //     if ($if_batch) {
      //       $batches[$batctr] = $if_batch;
      //       $batctr += 1;
      //     }
      //   }
        

      //   $result['batches'] = $batches;
      //   return Response::json($result);
      // }

      $result['batches'] = Batch::where('status', '=', 0)->get();
      return Response::json($result);
    }

    public function getBatchPolicies() {

      $id = Request::input('id');
      $sales_id = Request::input('sales_id');
      // $batch_policy = BatchPolicy::with('getBatchPolicyInfo')
      //                             ->where(function($query) use ($id, $sales_id) {
      //                                     $query->where('batch_id', '=', $id)
      //                                           ->where('user_id', '=', $sales_id)
      //                                           ->where('status', '=', 0);
      //                             })->get();

      $batch_policy = PayrollComputation::where(function($query) use ($id, $sales_id) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('user_id', '=', $sales_id)
                                                ->where('status', '=', 0);
                                  })->get();

      $rows = '<option class="hide" value="">Select:</option>';

      foreach ($batch_policy as $key => $row) {
        $rows .= '<option value="' . $row->id . '">' . $row->policy_no . '</option>';
      }

      return Response::json($rows);
    }  

    /**
     * Display a listing of the payroll computations.
     *
     * @return Response
     */
    public function indexPayrollComputation()
    {
        $result = $this->doListPayrollComputation();

        $this->data['rows'] = $result['rows'];

        $this->data['title'] = "Payroll Management";

       
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('payroll-management.payroll-computation.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListPayrollComputation() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';

        if(Auth::user()->usertype_id == 8) {
          $batch_ids = array_unique(PayrollComputation::where('user_id', '=', Auth::user()->id)
                                                      ->lists('batch_id')->toArray());

          $rows = Batch::with('getBatchPolicies')
                          ->where(function($query) { 
                              $query->where('status', 1)
                                    ->orWhere('status', 0);
                            })
                          ->whereIn('id', $batch_ids)->get();
        } else {
          $rows = Batch::with('getBatchPolicies')->where(function($query) { 
                              $query->where('status', 1)
                                    ->orWhere('status', 0);
                            })->get();
        }

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    public function getComputationAgents() {

      $batch = Request::input('id');
      $get_status = Batch::find($batch);
      $get_user_id = PayrollComputation::where('batch_id', '=', $batch)
                                ->lists('user_id');

      if(Auth::user()->usertype_id == 8) {

        $rows = '<div class="policies-table"><table class="table table-striped">' .
                '<thead>' .
                '<th class="tbheader"> BREAKDOWN</th></thead><tbody>';

        $rows .= '<tr>' .
                 '<td><a data-status="' . $get_status->status . '" data-batch="' . $batch . '" data-type="prepayroll" class="btn btn-table btn-xs btn-agent-item-expand"><i class="fa fa-plus"></i></a> Incentives or Deductions</td></tr>' .
                 '<tr><td><a data-status="' . $get_status->status . '" data-batch="' . $batch . '" data-type="computation" class="btn btn-table btn-xs btn-agent-item-expand"><i class="fa fa-plus"></i></a> Payroll Computation</td>' .
                 '</tr>'; 

        $rows .= '</tbody></table><br></div>';
      } else {
        $rows = '<div class="policies-table"><table class="table table-striped">' .
                '<thead>' .
                '<th class="tbheader"> Agent Name</th>' .
                '<th class="tbheader"> Agent Code</th></thead><tbody>';

        foreach (array_unique($get_user_id->toArray()) as $key => $user_id) {
          $name = User::find($user_id);

          $rows .= '<tr><td><a data-status="' . $get_status->status . '" data-batch="' . $batch . '" data-user="' . $user_id . '" class="btn btn-table btn-xs btn-agent-expand"><i class="fa fa-plus"></i></a> ' . $name->name . '</td>' . 
                   '<td>' . $name->code . '</td></tr>';
        }

        // $rows .= '</tbody></table><hr><button type="button" class="btn btn-sm btn-danger borderzero btn-tool pull-right btn-agent-close"><i class="fa fa-close"></i> Close</button><br></div>';

        $rows .= '</tbody></table><br></div>';

      }

        return Response::json($rows);
    }

    public function getComputationAgentPayroll() {

      $batch = Request::input('id');
      $user = Request::input('user');
      $status = Request::input('status');

      $rows = '<div class="policies-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> BREAKDOWN</th></thead><tbody>';

      $rows .= '<tr>' .
               '<td><a data-status="' . $status . '" data-batch="' . $batch . '" data-user="' . $user . '" data-type="prepayroll" class="btn btn-table btn-xs btn-agent-item-expand"><i class="fa fa-plus"></i></a> Incentives or Deductions</td></tr>' .
               '<tr><td><a data-status="' . $status . '" data-batch="' . $batch . '" data-user="' . $user . '" data-type="computation" class="btn btn-table btn-xs btn-agent-item-expand"><i class="fa fa-plus"></i></a> Payroll Computation</td>' .
               '</tr>'; 

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getComputationAgentPrePayroll() {

      $batch = Request::input('id');
      $user = (Auth::user()->usertype_id == 8 ? Auth::user()->id : Request::input('user'));
      $status = Request::input('status');

      $get_pre_payroll = PrePayrollComputation::where(function($query) use ($user) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                          ->where('user_id', '=', $user)
                                                ->where('status', '=', 1);
                                  })->get();

      // dd($get_pre_payroll);
      $incentives = 0;
      $deductions = 0;
      foreach ($get_pre_payroll as $key => $field) {
        if ($field->type == 'Incentives') {
          $incentives += $field->cost;
        }
        if ($field->type == 'Deductions') {
          $deductions += $field->cost;
        }
      }

      $rows = '<div class="policies-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> PRE-PAYROLL TYPE</th>
              <th class="tbheader"> TOTAL</th></thead><tbody>';

      $rows .= '<tr>' .
               '<td><a data-status="' . $status . '"  data-batch="' . $batch . '" data-user="' . $user . '" data-type="incentives" class="btn btn-table btn-xs btn-agent-item-type-expand"><i class="fa fa-plus"></i></a> Incentives</td>' .
               '<td>' . number_format($incentives, 2) .'</td>' .
               '</tr>' .
               '<tr><td><a data-status="' . $status . '"  data-batch="' . $batch . '" data-user="' . $user . '" data-type="deductions" class="btn btn-table btn-xs btn-agent-item-type-expand"><i class="fa fa-plus"></i></a> Deductions</td>' .
               '<td>' . number_format($deductions, 2) .'</td>' .
               '</tr>'; 

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getComputationAgentIncentives() {

      $batch = Request::input('id');
      $user = (Auth::user()->usertype_id == 8 ? Auth::user()->id : Request::input('user'));
      $status = Request::input('status');

      $get_incentives = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('type', '=', 'Incentives')
                                                ->where('user_id', '=', Request::input('user'))
                                                ->where('status', '=', 1);
                                  })->get();

      $rows = '<div class="policies-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Remarks</th><th class="tbheader"> Cost</th></thead><tbody>';

      foreach ($get_incentives as $key => $field) {
        $rows .= '<tr data-id="' . $field->id . '"><td>' . $field->remarks . '</td><td>' . number_format($field->cost, 2) . '</td></tr>';  
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getComputationAgentDeductions() {
 
      $batch = Request::input('id');
      $user = (Auth::user()->usertype_id == 8 ? Auth::user()->id : Request::input('user'));
      $status = Request::input('status');

      $get_deductions = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('type', '=', 'Deductions')
                                                ->where('user_id', '=', Request::input('user'))
                                                ->where('status', '=', 1);
                                  })->get();

      $rows = '<div class="policies-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Remarks</th><th class="tbheader"> Cost</th></thead><tbody>';

      foreach ($get_deductions as $key => $field) {
        $rows .= '<tr data-id="' . $field->id . '"><td>' . $field->remarks . '</td><td>' . number_format($field->cost, 2) . '</td></tr>';  
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);

    }

    public function getComputationAgentPolicies() {

      $batch = Request::input('id');
      $user = (Auth::user()->usertype_id == 8 ? Auth::user()->id : Request::input('user'));
      $status = Request::input('status');

      // $get_batch_policies = BatchPolicy::with('getBatchPolicyInfo')
      //                   ->with('getBatchPayrollComputation')
      //                   ->with('getBatch')
      //                   ->with('getBatchPolicyInfo.getPolicySales')
      //                   ->with('getBatchPolicyInfo.getPolicySales.salesInfo')
      //                   ->with('getBatchPolicyInfo.getPolicySales.salesInfo.designations')
      //                   ->with('getBatchPolicyInfo.getPolicyProvider')
      //                   ->with('getBatchPolicyInfo.getPolicyCategoryInfo')
      //                   ->where(function($query) use ($batch, $user) {
      //                       $query->where('batch_id', '=', $batch)
      //                             ->where('user_id', '=', $user);
      //                     })
      //                   ->orderBy('created_at', 'desc')->get();

      $get_batch_policies = PayrollComputation::with('getPayrollComputationProvider')
                          ->with('getPayrollComputationUser')
                          ->with('getPayrollComputationCategory')
                          ->where(function($query) use ($batch, $user) {
                            $query->where('batch_id', '=', $batch)
                                  ->where('user_id', '=', $user);
                          })
                        ->orderBy('created_at', 'desc')->get();

      $rows = '<div class="Policies-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Policy Number</th>' .
              '<th class="tbheader"> Provider Name</th>' .
              '<th class="tbheader"> Agent Name</th>' .
              '<th class="tbheader"> Agent Code</th>' .
              '<th class="tbheader"> Product Code</th>' .
              '<th class="tbheader"> Category</th>' .
              '<th class="tbheader rightalign"> Status</th></thead><tbody>';

      foreach ($get_batch_policies as $key => $row) {
        $rows .= '<tr data-id="' . $row->id . '">';
        $rows .= '<td><a data-type="compute" class="btn btn-table btn-xs btn-agent-item-type-expand"><i class="fa fa-plus"></i></a> ' . $row->policy_no . '</td>';
        $rows .= '<td>' . $row->getPayrollComputationProvider->name . '</td>';
        $rows .= '<td>' . $row->getPayrollComputationUser->name . '</td>';
        $rows .= '<td>' . $row->getPayrollComputationUser->code . '</td>';
        $rows .= '<td>' . $row->comp_code . '</td>';
        $rows .= '<td>' . $row->getPayrollComputationCategory->name . '</td>';
        $rows .= '<td class="rightalign">' . ($row->status == '0' ? 'Unreleased' : 'Released' ) . '</td>';
        $rows .= '</tr>';
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getComputation() {
      //$get_computation = PayrollComputation::where('batch_policy_id', '=', Request::input('id'))->first();
      $get_computation = PayrollComputation::find(Request::input('id'));

      $rows =   '<div class=""><table class="table table-striped">' .
                          '<tbody>' .
                            '<tr>' .
                              '<th colspan="4" class="tbheader" scope="row">BREAKDOWN</th>' .
                            '</tr></tbody></table></div>' .
                '<div class="compute-table col-lg-6"><table class="table table-striped">' .
                          '<tbody>' .
                            '<tr>' .
                              '<th class="col-lg-3" scope="row">PREMIUM</th>' .
                              '<td class="col-lg-9">' . number_format($get_computation->premium, 2) . '</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">1ST YR</th>' .
                              '<td>' . $get_computation->first_year . '</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">RENEWAL YR</th>' .
                              '<td>' . $get_computation->renewal_year . '</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">GROSS REV %</th>' .
                              '<td>' . number_format($get_computation->gross_revenue_per, 2) . '%</td>' .
                            '</tr>' .
                            '</tr>' .
                              '<th scope="row">GROSS REV</th>' .
                              '<td>' . number_format($get_computation->gross_revenue, 2) . '</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">FIRM REV %</th>' .
                              '<td>' . number_format($get_computation->firm_revenue_per, 2) . '%</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">FIRM REV</th>' .
                              '<td>' . number_format($get_computation->firm_revenue, 2) . '</td>' .
                            '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">GST %</th>' .
                            //   '<td>' . number_format($get_computation->gst_per, 2) . '%</td>' .
                            // '</tr>' .
                            '</tbody></table></div>' .
                            '<div class="col-lg-6"><table class="table table-striped"><tbody>' .
                            '<tr>' .
                              '<th scope="row">NET SHARE</th>' .
                              '<td>' . number_format($get_computation->total_banding, 2) . '</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">AGENT BANDING</th>' .
                              '<td>' . number_format($get_computation->agent_banding_per , 2). '%</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">AGENT SHARE</th>' .
                              '<td>' . number_format($get_computation->agent_banding, 2) . '</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">MGT SHARE</th>' .
                              '<td>' . number_format($get_computation->manage_share , 2). '</td>' .
                            '</tr>';
                            // '<tr>' .
                            //   '<th scope="row">GROSS PRODUCT REV %</th>' .
                            //   '<td>' . number_format($get_computation->gross_product_revenue_per, 2) . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">GROSS PRODUCT REV</th>' .
                            //   '<td>' . number_format($get_computation->gross_product_revenue, 2) . '</td>' .
                            // '</tr>';
                            // '<tr>' .
                            //   '<th scope="row">DESIGNATION %</th>' .
                            //   '<td>' . number_format($get_computation->designation_rate, 2) . '%</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">DESIGNATION SHARE</th>' .
                            //   '<td>' . number_format($get_computation->designation, 2) . '</td>' .
                            // '</tr>' .


                            // '<tr>' .
                            //   '<th scope="row">TOTAL INCENTIVES</th>' .
                            //   '<td>' . number_format($get_computation->total_incentives, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">TOTAL DEDUCTIONS</th>' .
                            //   '<td>' . number_format($get_computation->total_deductions, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPBC YEAR 1</th>' .
                            //   '<td>' . number_format($get_computation->rpbc_y1, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPBC YEAR 2</th>' .
                            //   '<td>' . number_format($get_computation->rpbc_y2, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPBC YEAR 3</th>' .
                            //   '<td>' . number_format($get_computation->rpbc_y3, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPBC YEAR 4</th>' .
                            //   '<td>' . number_format($get_computation->rpbc_y4, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPBC YEAR 5</th>' .
                            //   '<td>' . number_format($get_computation->rpbc_y5, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPBC YEAR 6</th>' .
                            //   '<td>' . number_format($get_computation->rpbc_y6, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPBC YEAR 99</th>' .
                            //   '<td>' . number_format($get_computation->rpbc_y99, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">SPBC</th>' .
                            //   '<td>' . number_format($get_computation->spbc, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPOR YEAR 1</th>' .
                            //   '<td>' . number_format($get_computation->rpor_y1, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPOR YEAR 2</th>' .
                            //   '<td>' . number_format($get_computation->rpor_y2, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPOR YEAR 3</th>' .
                            //   '<td>' . number_format($get_computation->rpor_y3, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPOR YEAR 4</th>' .
                            //   '<td>' . number_format($get_computation->rpor_y4, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPOR YEAR 5</th>' .
                            //   '<td>' . number_format($get_computation->rpor_y5, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPOR YEAR 6</th>' .
                            //   '<td>' . number_format($get_computation->rpor_y6, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">RPOR YEAR 99</th>' .
                            //   '<td>' . number_format($get_computation->rpor_y99, 2, '.', '') . '</td>' .
                            // '</tr>' .
                            // '<tr>' .
                            //   '<th scope="row">SPOR</th>' .
                            //   '<td>' . number_format($get_computation->spor, 2, '.', '') . '</td>' .
                            // '</tr>' .

                            if ($get_computation->show_tier == 1) {
                              $rows .= '<tr>' .
                                        '<th scope="row">TIER2 SHARE</th>' .
                                        '<td>' . number_format($get_computation->tier2_share, 2) . '</td>' .
                                      '</tr>' .
                                      '<tr>' .
                                        '<th scope="row">TIER3 SHARE</th>' .
                                        '<td>' . number_format($get_computation->tier3_share, 2) . '</td>' .
                                      '</tr>';
                            }


                      $rows .=  '<tr>' .
                              '<th scope="row">INTRODUCER %</th>' .
                              '<td>' . number_format($get_computation->intro_com_per, 2) . '%</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">INTROD SHARE</th>' .
                              '<td>' . number_format($get_computation->intro_com, 2) . '</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">BUYOUT %</th>' .
                              '<td>' . number_format($get_computation->buyout_per, 2) . '%</td>' .
                            '</tr>' .
                            '<tr>' .
                              '<th scope="row">BUYOUT</th>' .
                              '<td>' . number_format($get_computation->buyout, 2) . '</td>' .
                            '</tr>' .
                            '<tbody></tbody>' .
                      '</table>' .
                      '<br></div>';

      if ($get_computation) {
        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    /**
     * Display a listing of the invoice summaries(advisor).
     *
     * @return Response
     */
    public function indexSummaryAdvisor()
    {
        $result = $this->doListSummaryAdvisor();

        $this->data['rows'] = $result['rows'];
        $this->data['tier'] = 0;
        $this->data['pages'] = $result['pages'];

        $this->data['rank'] = null;

        $get_rank = Sales::with('designations')->where('user_id', Auth::user()->id)->first();
        if ($get_rank) {
          $this->data['rank'] = $get_rank->designations->rank;
          $this->data['tier'] = $get_rank->designations->tier;
          $check_group = Group::where('user_id', Auth::user()->id)->first();
          if ($check_group) {
            $this->data['tier'] = 3;
          } else {
            $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();
            if ($check_supervisor) {
              $this->data['tier'] = 2;
            }
          }
        }

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Payroll Management";
        return view('payroll-management.payroll-summary-advisor.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doListSummaryAdvisor() {

      $check_rank = Sales::with('designations')->where('user_id', Auth::user()->id)->first();

      // sort and order
      $result['sort'] = Request::input('sort') ?: 'start';
      $result['order'] = Request::input('order') ?: 'desc';
      $per = Request::input('per') ?: 10;

      $tier = 0;
      if ($check_rank) {
        $tier = $check_rank->designations->tier;

        $check_group_1 = Group::where('user_id', '=', Auth::user()->id)->first();
        if ($check_group_1) {
          $tier = 3;
        }

        if (Auth::user()->usertype_id == 8 &&  ($tier == 1 || $tier == 2)) {

          $check_group = Group::where('user_id', '=', Auth::user()->id)->first();

          if ($check_group) {
            if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
              });
              $rows = BatchMonthGroup::where('user_id', Auth::user()->id)->where('status', 1)->orderBy($result['sort'], $result['order'])->paginate($per);

            } else {
              $count = BatchMonthGroup::where('user_id', Auth::user()->id)->where('status', 1)->orderBy($result['sort'], $result['order'])->paginate($per);
              Paginator::currentPageResolver(function () use ($count, $per) {
                  return ceil($count->total() / $per);
              });
               $rows = BatchMonthGroup::where('user_id', Auth::user()->id)->where('status', 1)->orderBy($result['sort'], $result['order'])->paginate($per);
            }
          }

          if ($tier == 1) {
            if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
              });

              $rows = BatchMonthUser::select('batch_month_users.*', 'batches.name')
                                      ->leftJoin('batches', 'batches.id', '=', 'batch_month_users.batch_id')
                                      ->where('user_id', Auth::user()->id)
                                      ->where('batch_month_users.status', 1)
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);

            } else {
              $count = BatchMonthUser::select('batch_month_users.*', 'batches.name')
                                      ->leftJoin('batches', 'batches.id', '=', 'batch_month_users.batch_id')
                                      ->where('user_id', Auth::user()->id)
                                      ->where('batch_month_users.status', 1)
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
              Paginator::currentPageResolver(function () use ($count, $per) {
                  return ceil($count->total() / $per);
              });

              $rows = BatchMonthUser::select('batch_month_users.*', 'batches.name')
                                      ->leftJoin('batches', 'batches.id', '=', 'batch_month_users.batch_id')
                                      ->where('user_id', Auth::user()->id)
                                      ->where('batch_month_users.status', 1)
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);

            }
          } else if ($tier == 2 && !$check_group) {
            if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
              });
              $rows = BatchMonthSupervisor::select('batch_month_supervisors.*', 'batches.name')
                                          ->leftJoin('batches', 'batches.id', '=', 'batch_month_supervisors.batch_id')
                                          ->where('user_id', Auth::user()->id)
                                          ->where('batch_month_supervisors.status', 1)
                                          ->orderBy($result['sort'], $result['order'])
                                          ->paginate($per);

            } else {
              $count = BatchMonthSupervisor::select('batch_month_supervisors.*', 'batches.name')
                                          ->leftJoin('batches', 'batches.id', '=', 'batch_month_supervisors.batch_id')
                                          ->where('user_id', Auth::user()->id)
                                          ->where('batch_month_supervisors.status', 1)
                                          ->orderBy($result['sort'], $result['order'])
                                          ->paginate($per);

              Paginator::currentPageResolver(function () use ($count, $per) {
                  return ceil($count->total() / $per);
              });
               $rows = BatchMonthSupervisor::select('batch_month_supervisors.*', 'batches.name')
                                          ->leftJoin('batches', 'batches.id', '=', 'batch_month_supervisors.batch_id')
                                          ->where('user_id', Auth::user()->id)
                                          ->where('batch_month_supervisors.status', 1)
                                          ->orderBy($result['sort'], $result['order'])
                                          ->paginate($per);
            }
          }

        //dd($rows);
          // return response (format accordingly)
          if(Request::ajax()) {
              $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
              $result['rows'] = $rows->toArray();
              $result['tier'] = $tier;
              return Response::json($result);
          } else {
              $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
              $result['rows'] = $rows;
              $result['tier'] = $tier;
              return $result;
          }
        }
      }

      // sort and order
      $result['sort'] = Request::input('s') ?: 'created_at';
      $result['order'] = Request::input('o') ?: 'desc';
      
      $rows = Batch::where('status', '=', 1)->orderBy('updated_at', 'desc')->get();

      // return response (format accordingly)
      if(Request::ajax()) {
          //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['pages'] = '';
          $result['rows'] = $rows->toArray();
          $result['tier'] = 0;
          return Response::json($result);
      } else {
          //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['pages'] = '';
          $result['rows'] = $rows;
          $result['tier'] = 0;
          return $result;
      }
    }

    public function getMonthsAdvisor() {

      $get_batch = Batch::find(Request::input('id'));

      $start = Carbon::createFromFormat('Y-m-d', substr($get_batch->start, 0, 10));
      $end = Carbon::createFromFormat('Y-m-d', substr($get_batch->end, 0, 10));

      $samp_start = $start->copy();
      $samp_end = $end->copy();

      $try_start = $samp_start->startOfMonth();
      $try_end = $samp_end->endOfMonth();
      $try_diff = $try_start->diffInMonths($try_end);

      $diff = $start->diffInMonths($end);

      $dates = [];
      $id = 0;

      // while ($start->lte($end)) {
      while ($id == 0) {

        $agent_commission = 0;
        $date_period = $start->copy();
        $format_end = $end->format('Y-m-d');
        $total_comm = 0;
        $total_incentives = 0;
        $total_deductions = 0;
        $total_overrides = 0;
        $total_firm = 0;
        $total_intro = 0;

        $diff = $id;

        if ($id == $diff) {

          if (Auth::user()->usertype_id == 8) {

            $agent_commission = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();
            $tier = $check_rank->designations->tier;
            $check_group = Group::where("user_id", Auth::user()->id)->first();
            if ($check_group) {
              $tier = 3;
            }

            if ($tier == 3) {

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;

                $get_group = GroupComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

                if ($get_group) {

                  $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                                    $query->where('group_id', '=', $get_group->id)
                                                          ->where('batch_id', '=', Request::input('id'));
                                              })->get();

                  foreach ($get_sups_id as $gpikey => $gpifield) {
                    
                    $ids[$ctr] = $gpifield->user_id;
                    $ctr += 1;

                    $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                                $query->where('supervisor_id', '=', $gpifield->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                    foreach ($get_advs_ids as $gaikey => $gaifield) {
                      $ids[$ctr] = $gaifield->user_id;
                      $ctr += 1;
                    }
                  }
                }

                $ids = array_unique($ids);

                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

            } else if ($check_rank->designations->rank == "Supervisor") {
              $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

              if ($get_sup_id) {
                $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;
                foreach ($get_adv_ids as $advkey => $advfield) {
                  $ids[$ctr] = $advfield->user_id;
                  $ctr += 1;
                }

                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

              } else {
                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');
                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('tier2_share');
                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
              }

            } else if ($check_rank->designations->rank == "Advisor") {
              $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

              $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

              $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
            }
          } else {
            $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');

            $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');

            $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('intro_com');

            $total_firm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('firm_revenue');
          }

        } else {

          if (Auth::user()->usertype_id == 8) {

            $agent_commission = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();
            $tier = $check_rank->designations->tier;
            $check_group = Group::where("user_id", Auth::user()->id)->first();
            if ($check_group) {
              $tier = 3;
            }
            
            if ($tier == 3) {

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;

                $get_group = GroupComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

                if ($get_group) {

                  $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                                    $query->where('group_id', '=', $get_group->id)
                                                          ->where('batch_id', '=', Request::input('id'));
                                              })->get();

                  foreach ($get_sups_id as $gpikey => $gpifield) {
                    
                    $ids[$ctr] = $gpifield->user_id;
                    $ctr += 1;

                    $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                                $query->where('supervisor_id', '=', $gpifield->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                    foreach ($get_advs_ids as $gaikey => $gaifield) {
                      $ids[$ctr] = $gaifield->user_id;
                      $ctr += 1;
                    }
                  }
                }

                $ids = array_unique($ids);
                
                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

            } else if ($check_rank->designations->rank == "Supervisor") {
              $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

              if ($get_sup_id) {
                $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;
                foreach ($get_adv_ids as $advkey => $advfield) {
                  $ids[$ctr] = $advfield->user_id;
                  $ctr += 1;
                }

                $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');
              } else {
                $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
              }

            } else if ($check_rank->designations->rank == "Advisor") {
              $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

              $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

              $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
            }
          } else {
            $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
            $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');
            $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('intro_com');

            $total_firm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('firm_revenue');

          }

          $format_end = $date_period->endOfMonth()->format('Y-m-d');
        }

        if (Auth::user()->usertype_id == 8) {
          $total_incentives = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('type', '=', 'Incentives');
                                    })->sum('cost');

          $total_deductions = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('type', '=', 'Deductions');
                                    })->sum('cost');
        } else {
          $total_incentives = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('type', '=', 'Incentives');
                                    })->sum('cost');

          $total_deductions = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('type', '=', 'Deductions');
                                    })->sum('cost');
        }

        if ($id == 0 && $diff == 0) {
          if ($try_diff == 0) {
            $format_end = $end->format('Y-m-d');
          } else {
            $format_end = $date_period->startOfMonth()->endOfMonth()->format('Y-m-d');
          }
        } else if ($id == 1 && $diff == 0) {
          $format_end = $end->format('Y-m-d');
        }

        $format_date = $start->copy()->format('Y-m-d');

        if (Auth::user()->usertype_id == 8) {
          $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

          if ($check_rank->designations->rank == "Advisor") { 
            $total_overrides = 0;
          }


          $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                          ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->sum('intro_com');

          // $total_comm = $agent_commission;
        }

        $dates[$id] = array_add(['total_comm' => number_format($total_comm, 2, '.', ''),
                                  'introducer' => number_format($total_intro, 2),
                                  'firm_revenue' => number_format($total_firm, 2),
                                  'total_revenue' => number_format($total_firm + ($total_intro + $total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2),
                                  'advisor' => number_format(($total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2),
                                  'gross' => number_format(($total_intro + $total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2),
                                  'incentives' => number_format($total_incentives, 2, '.', ''), 
                                  'deductions' => number_format($total_deductions, 2, '.', ''), 
                                  'date' => $format_date, 'end' => $end->format('Y-m-d'), 'name' => $get_batch->name], 'id', $id);

        // dd($dates);

        $start->startOfMonth();
        $start->addMonth();

        $id += 1;

      }

      $result['rows'] = $dates;
      $result['batch_id'] = Request::input('id');
      return Response::json($result);

    }

    public function getItemsAdvisor() {

      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');

      $commissions = 0;
      $overrides = 0;
      $incentives = 0;
      $deductions = 0;

      if (Auth::user()->usertype_id == 8) {
        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {
          $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();


          if ($get_sup_id) {
            $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

            $ids = [];
            $ids[0] = Auth::user()->id;
            $ctr = 1;
            foreach ($get_adv_ids as $advkey => $advfield) {
              $ids[$ctr] = $advfield->user_id;
              $ctr += 1;
            }

            // $tier3_share = PayrollComputation::where(function($query) use ($id, $start, $end, $get_sup_id) {
            //                         $query->where('batch_id', '=', Request::input('id'))
            //                               ->where('user_id', '=', Auth::user()->id)
            //                               ->where('upload_date', '>=', $start . " 00:00:00")
            //                               ->where('upload_date', '<=', $end . " 00:00:00")
            //                               ->where('status', '=', 1);
            //                         })->sum('tier3_share');

            $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('agent_banding');

            $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('tier2_share');

            // $overrides =  $overrides - $tier3_share;
          } else {
            $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->sum('agent_banding');

            $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->sum('manage_share');
          }

        } else if ($check_rank->designations->rank == "Advisor") { 
          $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('user_id', '=', Auth::user()->id)
                                        ->where('status', '=', 1);
                                  })->sum('agent_banding');

          $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('user_id', '=', Auth::user()->id)
                                        ->where('status', '=', 1);
                                  })->sum('manage_share');
        }

        $incentives = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Incentives")
                                                  ->where('user_id', '=', Auth::user()->id);
                                    })->sum('cost');

        $deductions = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Deductions")
                                                  ->where('user_id', '=', Auth::user()->id);
                                    })->sum('cost');

      } else {
        $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                $query->where('batch_id', '=', $id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('status', '=', 1);
                        })->sum('agent_banding');

        $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                $query->where('batch_id', '=', $id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('status', '=', 1);
                        })->sum('manage_share');

        $incentives = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Incentives");
                                    })->sum('cost');

        $deductions = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Deductions");
                                    })->sum('cost');
      }
      
      if (Auth::user()->usertype_id == 8) {
        $check_group = Group::where('user_id', '=', Auth::user()->id)->first();
        if ($check_group) {
          $check_month = BatchMonthGroup::where(function($query) use ($id, $start, $end) {
                                            $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('start', '=', $start . " 00:00:00")
                                            ->where('end', '=', $end . " 00:00:00");
                                    })->first();
          if ($check_month) {
            $commissions = $check_month->revenue;
            $overrides = $check_month->overrides;
            $incentives = $check_month->incentives;
            $deductions = $check_month->deductions;
          }
        }
      }

          
      $rows = '<table class="table table-striped">' .
                      '<thead class="tbheader">' .
                        '<tr>' .
                          '<th> MONTHLY BREAKDOWN</th>' .
                          '<th> AMOUNT</th>' .
                        '</tr>' .
                    '</thead>' .
                    '<tbody>' .
                  '<tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-expand="commissions" data-item="commissions" data-id="' . $start . '"><i class="fa fa-plus"></i></a> REVENUE</td>' .
                  '<td>'  . number_format($commissions, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-expand="overrides" data-item="overrides" data-id="' . $start . '"><i class="fa fa-plus"></i></a> OVERRIDES</td>' .
                  '<td>'  . number_format($overrides, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-expand="incentives" data-item="incentives" data-id="' . $start . '"><i class="fa fa-plus"></i></a> INCENTIVES</td>' .
                  '<td>'  . number_format($incentives, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-expand="deductions" data-item="deductions" data-id="' . $start . '"><i class="fa fa-plus"></i></a> DEDUCTIONS</td>' .
                  '<td>'  . number_format($deductions, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td>TOTAL</td>' .
                  '<td>' . number_format($commissions + $overrides + ($incentives - $deductions), 2) .  '</td></tr>' .
                  '</tbody>' .
                  '</table>';

        if (Auth::user()->usertype_id == 8) {
          $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

          if ($check_rank->designations->rank == "Advisor") { 
           $rows = '<table class="table table-striped">' .
                          '<thead class="tbheader">' .
                            '<tr>' .
                              '<th> MONTHLY BREAKDOWN</th>' .
                              '<th> AMOUNT</th>' .
                            '</tr>' .
                        '</thead>' .
                        '<tbody>' .
                      '<tr data-id="' . $id . '">' .
                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-expand="commissions" data-item="commissions" data-id="' . $start . '"><i class="fa fa-plus"></i></a> REVENUE</td>' .
                      '<td>'  . number_format($commissions, 2) .  '</td>' .
                      '</tr><tr data-id="' . $id . '">' .
                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-expand="incentives" data-item="incentives" data-id="' . $start . '"><i class="fa fa-plus"></i></a> INCENTIVES</td>' .
                      '<td>'  . number_format($incentives, 2) .  '</td>' .
                      '</tr><tr data-id="' . $id . '">' .
                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-expand="deductions" data-item="deductions" data-id="' . $start . '"><i class="fa fa-plus"></i></a> DEDUCTIONS</td>' .
                      '<td>'  . number_format($deductions, 2) .  '</td>' .
                      '</tr><tr data-id="' . $id . '">' .
                      '<td>TOTAL</td>' .
                      '<td>' . number_format($commissions + ($incentives - $deductions), 2) .  '</td></tr>' .
                      '</tbody>' .
                      '</table>';

            $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/view-batch-expand") . '">' . csrf_field() . '<input type="hidden" name="id" value="' . $id . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> View</button>' .
                '</form>';
          }

        }

      return Response::json($rows);

    }




    public function getIntroducerAdvisor() {
      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');

      if (Auth::user()->usertype_id == 8) {
        $rows = '<div class="item-table"><table class="table table-striped">' .
                      '<thead>' .
                      '<th class="tbheader"> Policy Number</th>' .
                      '<th class="tbheader"> Introducer Name</th>' .
                      '<th class="tbheader"> Introducer Revenue</th></thead><tbody>';

        $get_introducer = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('user_id', '=', Auth::user()->id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('introducer_id', '>', 0)
                                                    ->where('status', '=', 1);
                                      })->lists('policy_no');

        foreach (array_unique($get_introducer->toArray()) as $key => $field) {

          $get_total_policy = PayrollComputation::where(function($query) use ($id, $start, $end, $field) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('user_id', '=', Auth::user()->id)
                                                    ->where('policy_no', '=', $field)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('introducer_id', '>', 0)
                                                    ->where('status', '=', 1);
                                      })->get();

          $total_intro = 0;
          $get_intro_name = null;

          foreach ($get_total_policy as $ikey => $intro_com) {
            $total_intro += $intro_com->intro_com;
            $get_intro_name = Introducer::find($intro_com->introducer_id); 
          }

          $rows .= '<tr data-id="' . $id . '">' . 
                    '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-expand" data-item="introducer" data-provider="' . $field . '" data-user="' . Auth::user()->id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . $field . '</td>' .
                    '<td>' . $get_intro_name->name . '</td>' .
                    '<td>' . number_format($total_intro, 2) . '</td></tr>';

        }

        $rows .= '</tbody></table><br></div>';

        // dd(count($get_total_policy));
        return Response::json($rows);
      } else {
        $get_user_id = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('status', '=', 1);
                                    })->lists('user_id');

        $rows = '<div class="item-table"><table class="table table-striped">' .
                '<thead>' .
                '<th class="tbheader"> Agent Name</th>' .
                // '<th class="tbheader"> 1st Year Commission</th>' .
                // '<th class="tbheader"> Renewal Year Commission</th>' .
                '<th class="tbheader"> Introducer Revenue</th></thead><tbody>';

        $ctr = 0;
        foreach (array_unique($get_user_id->toArray()) as $key => $value) {

          $get_introducer = PayrollComputation::where(function($query) use ($id, $start, $end, $value) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('user_id', '=', $value)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('status', '=', 1);
                                    })->get();
          $introducer = 0;

          foreach ($get_introducer as $ikey => $intro) {
            $introducer += $intro->intro_com;
          }

          $get_user = User::find($value);

          if ($introducer > 0) {
            $rows .= '<tr data-id="' . $id . '">' .
                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-expand" data-id="' . $start . '" data-user="' . $get_user->id . '"data-expand="agent-' . $ctr . '" data-item="agent"><i class="fa fa-plus"></i></a> ' . $get_user->name . '</td>' . 
                      '<td>' . number_format($introducer, 2) . '</td></tr>';
          }

          $ctr += 1;
        }

        $rows .= '</tbody></table><br></div>';

        return Response::json($rows);
      }
    }

    public function getIntroducerPoliciesAdvisor() {
      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');
      $user = Request::input('user');

      $rows = '<div class="item-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Policy Number</th>' .
              '<th class="tbheader"> Introducer Name</th>' .
              '<th class="tbheader"> Introducer Revenue</th></thead><tbody>';

      $get_introducer = PayrollComputation::where(function($query) use ($id, $start, $end, $user) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('user_id', '=', $user)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('introducer_id', '>', 0)
                                                  ->where('status', '=', 1);
                                    })->lists('policy_no');


      foreach (array_unique($get_introducer->toArray()) as $key => $field) {

        $get_total_policy = PayrollComputation::where(function($query) use ($id, $start, $end, $user, $field) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('user_id', '=', $user)
                                                  ->where('policy_no', '=', $field)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('introducer_id', '>', 0)
                                                  ->where('status', '=', 1);
                                    })->get();

        $total_intro = 0;
        $get_intro_name = null;

        foreach ($get_total_policy as $ikey => $intro_com) {
          $total_intro += $intro_com->intro_com;
          $get_intro_name = Introducer::find($intro_com->introducer_id); 
        }

        $rows .= '<tr data-id="' . $id . '">' . 
                  '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-expand" data-item="introducer" data-provider="' . $field . '" data-user="' . $user . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . $field . '</td>' .
                  '<td>' . $get_intro_name->name . '</td>' .
                  '<td>' . number_format($total_intro, 2) . '</td></tr>';

      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getIntroducerSubPoliciesAdvisor() {
      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');
      $user = Request::input('user');
      $policy = Request::input('provider');

      $rows = '<div class="item-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Policy Number</th>' .
              '<th class="tbheader"> Component Code</th>' .
              '<th class="tbheader"> FYC</th>' .
              '<th class="tbheader"> Renewal</th>' .
              '<th class="tbheader"> Total Revenue</th>' .
              '<th class="tbheader"> Premium</th>' .
              '<th class="tbheader"> Introducer Revenue</th></thead><tbody>';

      $get_policies = PayrollComputation::where(function($query) use ($id, $start, $end, $user, $policy) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('user_id', '=', $user)
                                                  ->where('policy_no', '=', $policy)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('introducer_id', '>', 0)
                                                  ->where('status', '=', 1);
                                    })->get();

      foreach ($get_policies as $key => $field) {
        $rows .= '<tr>' . 
                  '<td>' . $field->policy_no . '</td>' .
                  '<td>' . $field->comp_code . '</td>' .
                  '<td>' . $field->first_year . '</td>' .
                  '<td>' . $field->renewal_year . '</td>' .
                  '<td>' . number_format($field->agent_banding, 2) . '</td>' .
                  '<td>' . number_format($field->premium, 2) . '</td>' .
                  '<td>' . number_format($field->intro_com, 2) . '</td></tr>';
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getCommissionAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;

      $get_provider = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('status', '=', 1);
                                  })->lists('provider_id');

      // if (Auth::user()->usertype_id == 8) {

      //   $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

      //   if ($check_rank->designations->rank == "Supervisor") {
      //     $get_sup_id = SalesSupervisorComputation::where(function($query) { 
      //                                           $query->where('user_id', '=', Auth::user()->id)
      //                                                 ->where('batch_id', '=', Request::input('id'));
      //                                     })->first();

      //     if ($get_sup_id) {
      //       $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
      //                                           $query->where('supervisor_id', '=', $get_sup_id->id)
      //                                                 ->where('batch_id', '=', Request::input('id'));
      //                                     })->get();

      //       $ids = [];
      //       $ids[0] = Auth::user()->id;
      //       $ctr = 1;
      //       foreach ($get_adv_ids as $advkey => $advfield) {
      //         $ids[$ctr] = $advfield->user_id;
      //         $ctr += 1;
      //       }

      //       $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
      //                               $query->where('batch_id', '=', Request::input('id'))
      //                                     ->where('upload_date', '>=', $start . " 00:00:00")
      //                                     ->where('upload_date', '<=', $end . " 00:00:00")
      //                                     ->where('status', '=', 1);
      //                               })->whereIn('user_id', $ids)->lists('provider_id');
      //     } else {
      //       $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
      //                               $query->where('batch_id', '=', Request::input('id'))
      //                                     ->where('upload_date', '>=', $start . " 00:00:00")
      //                                     ->where('upload_date', '<=', $end . " 00:00:00")
      //                                     ->where('user_id', '=', Auth::user()->id)
      //                                     ->where('status', '=', 1);
      //                               })->lists('provider_id');
      //     }

      //   } else if ($check_rank->designations->rank == "Advisor") {
      //     $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
      //                               $query->where('batch_id', '=', Request::input('id'))
      //                                     ->where('upload_date', '>=', $start . " 00:00:00")
      //                                     ->where('upload_date', '<=', $end . " 00:00:00")
      //                                     ->where('user_id', '=', Auth::user()->id)
      //                                     ->where('status', '=', 1);
      //                               })->lists('provider_id');
      //   }

      // } else {
      //   $get_provider = PayrollComputation::where(function($query) use ($id, $start, $end) {
      //                                       $query->where('batch_id', '=', $id)
      //                                             ->where('upload_date', '>=', $start . " 00:00:00")
      //                                             ->where('upload_date', '<=', $end . " 00:00:00")
      //                                             ->where('status', '=', 1);
      //                               })->lists('provider_id');
      // }
      

      $no_team = false;
      
      if (Auth::user()->usertype_id == 8) {
        $check_no_team = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();
        if (!$check_no_team) {
          $check_no_adv_team = SalesAdvisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if (!$check_no_adv_team) {
            $no_team = true;
          }
        }
      }

      $rows = '<div class="item-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Provider Name' . ( $no_team == true ? ' <small style="font-weight: normal;">(user has no team)</small>' : '' ) . '</th>' .
              '<th class="tbheader"> Total</th></thead><tbody>';

      foreach (array_unique($get_provider->toArray()) as $ikey => $field) {

        if (Auth::user()->usertype_id == 8) {

        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {

          $check_tier3 = Group::where('user_id', '=', Auth::user()->id)->first();

          if ($check_tier3) {

            $check_group = GroupComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', Auth::user()->id);
                                    })->get();
            $gids = [];
            $gctr = 0;

            foreach ($check_group as $cgkey => $cgfield) {

              $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $cgfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->get(); 
              
              foreach ($get_supervisors as $gskey => $gsfield) {
                $gids[$gctr] = $gsfield->user_id;
                $gctr += 1;

                $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $cgfield, $gsfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('supervisor_id', '=', $gsfield->id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->lists('user_id');

                foreach (array_unique($get_advisors->toArray()) as $gakey => $gafield) {
                  $gids[$gctr] = $gafield;
                  $gctr += 1;
                }
              }
            }

            $commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('agent_banding');
          } else {
            $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

            if ($get_sup_id) {

              $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

              $ids = [];
              $ids[0] = Auth::user()->id;
              $ctr = 1;
              foreach ($get_adv_ids as $advkey => $advfield) {
                $ids[$ctr] = $advfield->user_id;
                $ctr += 1;
              }

              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('agent_banding');
            } else {
              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
            }
          }

          } else if ($check_rank->designations->rank == "Advisor") {
            $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
          }

        } else {
          $commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('status', '=', 1);
                                  })->sum('agent_banding');
        }

        $get_name = Provider::find($field);

        $rows .= '<tr data-id="' . $id . '">' . 
                  '<td data-id="' . $end . '">' .
                  '<a class="btn btn-xs btn-table btn-advisor-item-sub-expand" data-item="' . $item . '" data-provider="' . $field . '" data-id="' . $start . '">' .
                  '<i class="fa fa-plus"></i></a> ' . $get_name->name . '</td>' .
                  '<td>' . number_format($commission, 2) . '</td>' .
                  '</tr>';

      }
      
      $rows .= '</tbody></table><br>';

      if (Auth::user()->usertype_id == 8) {
        $get_sales = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', Auth::user()->id)->first();

        $get_group = GroupComputation::where('batch_id', $id)->where('user_id', Auth::user()->id)->first();

        if ($get_sales || $get_group) {
          $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") .'">' 
                    . csrf_field() .  
                      '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-users"></i> Save Team PDF</button>' .
                    '</form>';

          // $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") .'">' 
          //           . csrf_field() .  
          //             '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="personal" value="personal"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-user"></i> Save Summary Report PDF</button>' .
          //           '</form>';
                    
        } else {
          $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") .'">' 
                    . csrf_field() .  
                      '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Summary</button>' .
                    '</form>';
        }
      } else {
        $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue") .'">' 
                  . csrf_field() .  
                    '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Save PDF</button>' .
                  '</form>';
      }

      if (Auth::user()->usertype_id == 1) {

        $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-summary-revenue") .'">' 
                  . csrf_field() .  
                    '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="personal" value="personal"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-user"></i> Save Summary Report PDF</button>' .
                  '</form>';

        $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue-or") .'">' 
                  . csrf_field() .  
                    '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Save PDF Revenue</button>' .
                  '</form>';
      }


      $rows .= '</div>';
      
      return Response::json($rows);
    }

    public function getIncentivesAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');

      if (Auth::user()->usertype_id == 8) {
        $get_pre_payroll = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('type', '=', 'Incentives')
                                                  ->where('user_id', '=', Auth::user()->id);
                                    })->lists('user_id');

      } else {
        $get_pre_payroll = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('type', '=', 'Incentives');
                                    })->lists('user_id');
      }

      $rows = '<div class="item-table"><table class="table table-striped">' .
              '<thead>' .
              (Auth::user()->usertype_id == 8 ? '<th class="tbheader"> Remarks</th>' : '<th class="tbheader"> Agent Name</th>' ) .
              '<th class="tbheader"> Cost</th></thead><tbody>';

      foreach (array_unique($get_pre_payroll->toArray()) as $key => $value) {

        $get_incentives = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) use ($value) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('type', '=', 'Incentives')
                                                ->where('user_id', '=', $value);
                                  })->get();

        $get_user = User::find($value);
        //$incentives = 0;

        foreach ($get_incentives as $ikey => $field) {
          $rows .= '<tr data-id="' . $field->id . '">' . 
            (Auth::user()->usertype_id == 8 ? '<td>' . $field->remarks . '</td>' : '<td>' . $get_user->name . '</td>' ) . 
            '<td>' . number_format($field->cost, 2) . '</td></tr>';
        }

      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);

    }

    public function getDeductionsAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');

      if (Auth::user()->usertype_id == 8) {
        $get_pre_payroll = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('type', '=', 'Deductions')
                                              ->where('user_id', '=', Auth::user()->id);
                                })->lists('user_id');
      } else {
        $get_pre_payroll = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('type', '=', 'Deductions');
                                })->lists('user_id');
      }

      $rows = '<div class="item-table"><table class="table table-striped">' .
              '<thead>' .
              (Auth::user()->usertype_id == 8 ? '<th class="tbheader"> Remarks</th>' : '<th class="tbheader"> Agent Name</th>' ) .
              '<th class="tbheader"> Cost</th></thead><tbody>';

      foreach (array_unique($get_pre_payroll->toArray()) as $key => $value) {

        $get_deductions = PrePayrollComputation::with('getUser')->with('getPrePayrollBatch')->where(function($query) use ($value) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('type', '=', 'Deductions')
                                                ->where('user_id', '=', $value);
                                  })->get();

        $get_user = User::find($value);

        foreach ($get_deductions as $ikey => $field) {

          $rows .= '<tr data-id="' . $field->id . '">' . 
            (Auth::user()->usertype_id == 8 ? '<td>' . $field->remarks . '</td>' : '<td>' . $get_user->name . '</td>' ) . 
            '<td>' . number_format($field->cost, 2) . '</td></tr>';
        }

      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    public function getOverrideAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');

      $provider = Provider::where('status', 1)->get();
      $override = 0;

      $get_provider = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('status', '=', 1);
                                  })->lists('provider_id');

      $no_team = false;
      
      if (Auth::user()->usertype_id == 8) {
        $check_no_team = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();
        if (!$check_no_team) {
          $check_no_adv_team = SalesAdvisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if (!$check_no_adv_team) {
            $no_team = true;
          }
        }
      }

      $rows = '<div class="item-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Provider Name' . ( $no_team == true ? ' <small style="font-weight: normal;">(user has no team)</small>' : '' ) . '</th>' .
              '<th class="tbheader"> Total</th></thead><tbody>';

      foreach (array_unique($get_provider->toArray()) as $ikey => $field) {

        if (Auth::user()->usertype_id == 8) {
          $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

          if ($check_rank->designations->rank == "Supervisor") {

          $check_tier3 = Group::where('user_id', '=', Auth::user()->id)->first();

          if ($check_tier3) {

            $check_group = GroupComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', Auth::user()->id);
                                    })->first();
            $gids = [];
            $gctr = 0;
            $get_advisors = null;

            if($check_group) {

              $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $check_group) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('group_id', '=', $check_group->id);
                                    })->get();
              
              foreach ($get_supervisors as $gskey => $gsfield) {
                $gids[$gctr] = $gsfield->user_id;
                $gctr += 1;

                $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $check_group, $gsfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('supervisor_id', '=', $gsfield->id)
                                            ->where('group_id', '=', $check_group->id);
                                    })->lists('user_id');

                foreach (array_unique($get_advisors->toArray()) as $gakey => $gafield) {
                  $gids[$gctr] = $gafield;
                  $gctr += 1;
                }
              }
            }

            $override = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('manage_share');
          } else {
              $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

              $ids = [];
              $ids[0] = Auth::user()->id;

              if ($get_sup_id) {
                $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                $ctr = 1;
                foreach ($get_adv_ids as $advkey => $advfield) {
                  $ids[$ctr] = $advfield->user_id;
                  $ctr += 1;
                }
              }

              $override = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                                $query->where('batch_id', '=', $id)
                                                      ->where('provider_id', '=', $field)
                                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                                      ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('tier2_share');

            }
          }
        } else {
          $override = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('provider_id', '=', $field)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('status', '=', 1);
                                    })->sum('manage_share');
        }

        $get_name = Provider::find($field);

        $rows .= '<tr data-id="' . $id . '">' . 
                  '<td data-id="' . $end . '">' .
                  '<a class="btn btn-xs btn-table btn-advisor-item-sub-expand" data-item="' . $item . '" data-provider="' . $field . '" data-id="' . $start . '">' .
                  '<i class="fa fa-plus"></i></a> ' . $get_name->name . '</td>' .
                  '<td>' . number_format($override, 2) . '</td>' .
                  '</tr>';

      }


      $rows .= '</tbody></table><br>';

      if (Auth::user()->usertype_id == 8) {
        $get_sales = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', Auth::user()->id)->first();

        $get_group = GroupComputation::where('batch_id', $id)->where('user_id', Auth::user()->id)->first();

        if ($get_sales || $get_group) {
          $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-override") .'">' 
                    . csrf_field() .  
                      '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-users"></i> Save Team PDF</button>' .
                    '</form>';

          // $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-override") .'">' 
          //           . csrf_field() .  
          //             '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="personal" value="personal"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-user"></i> Save Summary Report PDF</button>' .
          //           '</form>';
                    
        } else {
          $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-override") .'">' 
                    . csrf_field() .  
                      '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Summary</button>' .
                    '</form>';
        }
      } else {
        $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-override") .'">' 
                  . csrf_field() .  
                    '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Save PDF</button>' .
                  '</form>';
      }

      if (Auth::user()->usertype_id == 1) {

        $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-summary-override") .'">' 
                  . csrf_field() .  
                    '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="personal" value="personal"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-user"></i> Save Summary Report PDF</button>' .
                  '</form>';
                    

        $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-override-or") .'">' 
                  . csrf_field() .  
                    '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Save PDF Overrides</button>' .
                  '</form>';

        // $rows .= '<form role="form" method="post" action="' . url("payroll/payroll-summary-advisor/pdf-payroll-breakdown-override") .'">' 
        //           . csrf_field() .  
        //             '<input type="hidden" name="id" value="' . $id . '"><input type="hidden" name="start" value="' . $start . '"><input type="hidden" name="end" value="' . $end . '"><button type="submit" data-batch="' . $id . '" class="btn btn-sm btn-success borderzero pull-right btn-tool btn-pdf-batch-expand"><i class="fa fa-print"></i> Payroll Breakdown</button>' .
        //           '</form>';
      }

      $rows .= '</div>';

      return Response::json($rows);

    }

    public function getCommissionProviderAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');

      if (Auth::user()->usertype_id == 8) {
        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {
          $get_sup_id = SalesSupervisorComputation::where(function($query) use ($id) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

          $ids = [];
          $ids[0] = Auth::user()->id;
          if ($get_sup_id) {
            $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($id, $get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', $id);
                                          })->get();

            $ctr = 1;
            foreach ($get_adv_ids as $advkey => $advfield) {
              $ids[$ctr] = $advfield->user_id;
              $ctr += 1;
            }
          }

          $get_user_id = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $provider_id)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                              })->whereIn('user_id', $ids)->lists('user_id');
        } else if ($check_rank->designations->rank == "Advisor") {

          $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                                $query->where('batch_id', '=', $id)
                                      ->where('provider_id', '=', $provider_id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('user_id', '=', Auth::user()->id)
                                      ->where('status', '=', 1);
                        })->lists('category_id')->toArray());

          $rows = '';

          foreach ($get_cat_id as $ckey => $cat_id) {
            
            $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('category_id', '=', $cat_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('user_id', '=', Auth::user()->id)
                                        ->where('status', '=', 1);
                          })->get();

            // $total = 0;
            $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

            $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
                '<table class="table table-striped"><thead>' . 
                '<th class="tbheader">Policy Number</th>' .
                '<th class="tbheader">Component Code</th>' .
                '<th class="tbheader">FYC</th>' .
                '<th class="tbheader">Renewal</th>' .
                '<th class="tbheader">Premium</th>' .
                '<th class="tbheader">Total Revenue</th>' .
                '</thead><tbody>';

            foreach ($get_payroll_comp as $pkey => $pfield) {

              $rows .= '<tr>' .
                        '<td>' . $pfield->policy_no . '</td>' .
                        '<td>' . $pfield->comp_code . '</td>' .
                        '<td>' . $pfield->first_year . '</td>' .
                        '<td>' . $pfield->renewal_year . '</td>' .
                        '<td>' . number_format($pfield->premium, 2) . '</td>' .
                        '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                        '</tr>';
            }

            $rows .= '</tbody></table></div>';
          }

          return Response::json($rows);
        }
      } else {
        $get_user_id = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('provider_id', '=', $provider_id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                            })->lists('user_id');
      }

      $group = [];
      $ctr = 0;

      //dd(array_unique($get_user_id->toArray()));
      foreach (array_unique($get_user_id->toArray()) as $key => $value) {
        $get_group_sup = SalesSupervisorComputation::where(function($query) use ($id, $value) { 
                                                          $query->where('user_id', '=', $value)
                                                                ->where('batch_id', '=', $id);
                                                    })->first();
        // where('user_id', '=', $value)->first();
        if ($get_group_sup) {
          $group[$ctr] = $get_group_sup->group_id;
          $ctr += 1;
        } else if (!$get_group_sup) {
          $get_group_adv = SalesAdvisorComputation::where(function($query) use ($id, $value) { 
                                                          $query->where('user_id', '=', $value)
                                                                ->where('batch_id', '=', $id);
                                                    })->first();
          if ($get_group_adv) {
            $group[$ctr] = $get_group_adv->group_id;
            $ctr += 1;
          }
        }
      }

      $rows = '';
      $user_no_group = [];

      if (Auth::user()->usertype_id == 8) {

        $check_team = SalesSupervisorComputation::where(function($query) use ($id) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

        if ($check_team) {
          $id = Request::input('id');
          $start = Request::input('start');
          $end = Request::input('end');
          $provider_id = Request::input('provider');

          $get_group_comp = GroupComputation::where(function($query) use ($id, $check_team) { 
                                                $query->where('id', '=', $check_team->group_id)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

          // where('id', '=', $check_team->group_id)->first();

          $get_group = Group::find($get_group_comp->group_id);

          $group = $get_group_comp->group_id;

          $row_super = '';
          $row_adv = '';

          $rows = '<div class="provider-table">' .
              '<table class="table table-striped"><thead>' . 
              '<th class="tbheader">Team Total</th>' .
              '<th class="tbheader">Personal Revenue</th>' .
              '<th class="tbheader">Total</th>' .
              '</thead><tbody>';
        
          $get_group_comp = GroupComputation::where(function($query) use ($id, $group) { 
                                                    $query->where('group_id', '=', $group)
                                                          ->where('batch_id', '=', $id);
                                              })->first();

          if (Auth::user()->usertype_id == 8) {
            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

            if ($check_rank->designations->rank == "Supervisor") {

              $check_tier3 = Group::where('user_id', '=', Auth::user()->id)->first();
              $team_id = '';

              if ($check_tier3) {
                $check_group = GroupComputation::where(function($query) use ($id) {
                                                  $query->where('batch_id', '=', $id)
                                                        ->where('user_id', '=', Auth::user()->id);
                                                })->first();

                $sup_total = 0;
                $sup_per = 0;

                if($check_group) {

                  $team_id = $check_group->id;

                  $sup_per = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('provider_id', '=', $provider_id)
                                                    ->where('user_id', '=', Auth::user()->id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('status', '=', 1);
                                      })->sum('agent_banding');

                  $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $check_group) {
                                                          $query->where('batch_id', '=', $id)
                                                                ->where('user_id', '!=', Auth::user()->id)
                                                                ->where('group_id', '=', $check_group->id);
                                                        })->lists('user_id'); 

                  $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $check_group) {
                                                          $query->where('batch_id', '=', $id)
                                                                ->where('group_id', '=', $check_group->id);
                                                        })->lists('user_id'); 

                  $ga_ids = array_merge($get_supervisors->toArray(), $get_advisors->toArray());
                  
                  $sup_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('provider_id', '=', $provider_id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('status', '=', 1);
                                      })->whereIn('user_id', $ga_ids)->sum('agent_banding');
                }

                $rows .= $row_super .= '<tr data-id="' . $id . '">' .
                                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit-group-comm" data-unit="' . $team_id . '" data-item="unit-group-' . $team_id . '" data-supervisor="' . $team_id . '" data-user="' . Auth::user()->id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . number_format($sup_total, 2) . '</td>' .
                                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit-personal-comm" data-unit="' . $team_id . '" data-item="unit-personal-' . $team_id . '" data-supervisor="' . $team_id . '" data-user="' . Auth::user()->id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . number_format($sup_per, 2) . '</td>' . 
                                      '<td>' . number_format($sup_total + $sup_per, 2) . '</td></tr>';

                $rows .= '</tbody></table></div>';

                return Response::json($rows);

              } else {
                $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                            $query->where('group_id', '=', $get_group_comp->id)
                                                                  ->where('batch_id', '=', $id)
                                                                  ->where('user_id', '=', Auth::user()->id);
                                                            })->get();
              }
            }
          } else {
            $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('group_id', '=', $get_group_comp->id);
                                                        })->get();
          }


          foreach ($get_sup_user as $key => $field) {
            
            $agent_banding = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $field) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('user_id', '=', $field->user_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->sum('agent_banding');

            $get_adv_user = SalesAdvisorComputation::where(function($query) use ($id, $field) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('supervisor_id', '=', $field->id);
                                                        })->get();

            $sup_total = $agent_banding;
            $adv_total = 0;
            // foreach ($payroll_comp as $pkey => $value) {
            //   $agent_banding += $value;
            //   $sup_total += $value;
            // }

            foreach ($get_adv_user as $akey => $afield) {

              $adv_total += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $afield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('user_id', '=', $afield->user_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->sum('agent_banding');

              $agent_banding += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $afield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('user_id', '=', $afield->user_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->sum('agent_banding');
            }

            $get_name = User::find($field->user_id);

            $check_total = $adv_total + $sup_total;

            if ($check_total > 0) {
              $rows .= '<tr data-id="' . $id . '">' .
              '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit-comm" data-unit="' . $field->id. '" data-item="unit-comm-' . $field->id . '" data-supervisor="' . $field->id . '" data-user="' . $field->user_id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' /*. $get_group->code . ' - ' . $field->unit_code . ' - '*/ .  number_format($adv_total, 2) . '</td>' . 
              '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="supervisor-comm" data-item="user-comm-' . $field->id . '" data-supervisor="' . $field->id . '" data-user="' . $field->user_id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' .  number_format($sup_total, 2) . '</td>' . 
              '<td>' .  number_format($agent_banding, 2) . '</td>' . 
              '</tr>';
            }
          }

          $rows .= '</tbody></table></div>';

          return Response::json($rows);
        }
      } else {
        $rows = '<div class="provider-table">' .
                  '<table class="table table-striped"><thead>' . 
                  '<th class="tbheader">Group Code</th>' .
                  '<th class="tbheader">Group Owner</th>' .
                  '<th class="tbheader">Total</th>' .
                  '</thead><tbody>';

        $group_comp_total = 0;

        foreach (array_unique($group) as $gkey => $gvalue) {
          $total_group = 0;
          foreach (array_unique($get_user_id->toArray()) as $ukey => $uvalue) {
          $get_group_sup = SalesSupervisorComputation::where(function($query) use ($id, $uvalue, $gvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('group_id', '=', $gvalue)
                                                              ->where('user_id', '=', $uvalue);
                                                        })->first();

          $get_group_adv = SalesAdvisorComputation::where(function($query) use ($id, $uvalue, $gvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('group_id', '=', $gvalue)
                                                              ->where('user_id', '=', $uvalue);
                                                        })->first();

            if ($get_group_sup) {
              $total_group += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $uvalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $uvalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('agent_banding');

            } else if ($get_group_adv) {
              $total_group += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $uvalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $uvalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('agent_banding');
            } else {
              $user_no_group[$ukey] = $uvalue;
            }
          }

          $get_group_comp = GroupComputation::find($gvalue);
          $get_group = Group::find($get_group_comp->group_id);
          $get_name = null;

          if ($get_group_comp->user_id) {
            $get_owner = User::find($get_group_comp->user_id);

            if ($get_owner) {
              $get_name = $get_owner->name;
            }
          }
          $rows .= '<tr data-id="' . $id . '">' .
                    '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-expand" data-breakdown="team-commission" data-sub="commission-' . $get_group->id . '" data-group="' . $get_group->id . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' .  $get_group->code . '</td>' . 
                    '<td>' .  $get_name . '</td>' . 
                    '<td>' .  number_format($total_group, 2) . '</td>' . 
                    '</tr>';

          $group_comp_total += $total_group;
        }
      }

      $total_no_group = 0;
      foreach ($user_no_group as $ngkey => $ngvalue) {
        $total_no_group = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $ngvalue) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('user_id', '=', $ngvalue)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('status', '=', 1);
                })->sum('agent_banding');
      }

      if (Auth::user()->usertype_id != 8) {
        if ($total_no_group > 0) {

        $total_all_group = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('provider_id', '=', $provider_id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                            })->sum('agent_banding');

          $rows .= '<tr data-id="' . $id . '">' .
            '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-expand" data-breakdown="no_team-commission" data-sub="commission-nogroup" data-group="nogroup" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> No Team</td>' . 
            '<td></td>' . 
            '<td>' .  number_format($total_all_group - $group_comp_total, 2) . '</td>' . 
            '</tr>';
        }
      } else {

        $get_group_sup = SalesSupervisorComputation::where(function($query) use ($id) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('user_id', '=', Auth::user()->id);
                                                        })->first();

        $get_group_adv = SalesAdvisorComputation::where(function($query) use ($id) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('user_id', '=', Auth::user()->id);
                                                        })->first();

        if (!$get_group_sup && !$get_group_adv) {

          $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                                $query->where('batch_id', '=', $id)
                                      ->where('provider_id', '=', $provider_id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('user_id', '=', Auth::user()->id)
                                      ->where('status', '=', 1);
                        })->lists('category_id')->toArray());

          $rows = '';

          foreach ($get_cat_id as $ckey => $cat_id) {
            
            $payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id) {
                                                  $query->where('batch_id', '=', $id)
                                                        ->where('provider_id', '=', $provider_id)
                                                        ->where('category_id', '=', $cat_id)
                                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                                        ->where('user_id', '=', Auth::user()->id)
                                                        ->where('status', '=', 1);
                                                })->get();

            $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

            $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
                '<table class="table table-striped"><thead>' . 
                '<th class="tbheader">Policy Number</th>' .
                '<th class="tbheader">Component Code</th>' .
                '<th class="tbheader">FYC</th>' .
                '<th class="tbheader">Renewal</th>' .
                '<th class="tbheader">Premium</th>' .
                '<th class="tbheader">Total Revenue</th>' .
                '</thead><tbody>';

            foreach ($payroll_comp as $key => $field) {
              $rows .= '<tr>' .
                        '<td>' . $field->policy_no . '</td>' .
                        '<td>' . $field->comp_code . '</td>' .
                        '<td>' . $field->first_year . '</td>' .
                        '<td>' . $field->renewal_year . '</td>' .
                        '<td>' . number_format($field->premium, 2) . '</td>' .
                        '<td>' . number_format($field->agent_banding, 2) . '</td>' .
                        '</tr>';

            }

            $rows .= '</tbody></table></div>';
          }
        }
      }

      return Response::json($rows);
    }
    
    public function getCommissionProviderGroupPersonalAdvisor() {
      $id = Request::input('id');
      $start = Request::input('start');
      $unit = Request::input('unit');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('user_id', '=', Auth::user()->id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('user_id', '=', Auth::user()->id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">Agent Name</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            // '<th class="tbheader">Total Revenue</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Revenue</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    // '<td>' . number_format($pfield->agent_banding, 2, '.', '') . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '</tr>';
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    public function getCommissionProviderGroupAdvisor() {
      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' . 
          '<th class="tbheader">Unit Code - Total - Name</th>' .
          '<th class="tbheader">Personal Revenue</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';
      
      $get_group_comp = GroupComputation::where(function($query) use ($id, $group) { 
                                                $query->where('group_id', '=', $group)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

      if (Auth::user()->usertype_id == 8) {
        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {
          $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                      $query->where('group_id', '=', $get_group_comp->id)
                                                            ->where('batch_id', '=', $id)
                                                            ->where('user_id', '=', Auth::user()->id);
                                                      })->get();
        }
      } else {
        $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                      $query->where('group_id', '=', $get_group_comp->id)
                                                            ->where('batch_id', '=', $id); 
                                                    })->get();
      }


      foreach ($get_sup_user as $key => $field) {

        $agent_banding = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $field) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('user_id', '=', $field->user_id)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('status', '=', 1);
                })->sum('agent_banding');

        $get_adv_user = SalesAdvisorComputation::where('supervisor_id', '=', $field->id)->get();

        $sup_total = $agent_banding;
        $adv_total = 0;

        foreach ($get_adv_user as $akey => $afield) {

          $agent_banding += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $afield) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $afield->user_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('agent_banding');

          $adv_total += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $afield) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $afield->user_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('agent_banding');

        }

        $get_name = User::find($field->user_id);

        $info = 0;
        if ($field->user_id == $get_group_comp->user_id) {
          $info = 1;
        }

        $check_total = $adv_total + $sup_total;

        if ($check_total > 0) {
          $rows .= '<tr data-id="' . $id . '" ' . ($info == 1 ? 'class="info"' : '' ) . '>' .
          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit-comm" data-unit="' . $field->id. '" data-item="unit-comm-' . $field->id . '" data-supervisor="' . $field->id . '" data-user="' . $field->user_id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . $field->unit_code . ' - ' .  number_format($adv_total, 2) . ' - ' . $get_name->name . '</td>' . 
          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="supervisor-comm" data-item="user-comm-' . $field->id . '" data-supervisor="' . $field->id . '" data-user="' . $field->user_id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' .  number_format($sup_total, 2) . '</td>' . 
          '<td>' .  number_format($agent_banding, 2) . '</td>' . 
          '</tr>';
        }
      }

      $rows .= '</tbody></table></div>';

      return Response::json($rows);
    }

    public function getCommissionProviderNoGroupAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');

      $get_group_sup = SalesSupervisorComputation::where('batch_id', '=', $id)->lists('user_id');
      $get_group_adv = SalesAdvisorComputation::where('batch_id', '=', $id)->lists('user_id');

      $user_group = [];
      $ctr = 0;

      foreach ($get_group_sup as $skey => $svalue) {
        $user_group[$ctr] = $svalue;
        $ctr += 1;
      }

      foreach ($get_group_adv as $akey => $avalue) {
        $user_group[$ctr] = $avalue;
        $ctr += 1;
      }

      $payroll_comp = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->whereNotIn('user_id', $user_group)->lists('user_id')->toArray());

      $rows = '';

      // foreach ($payroll_comp as $key => $field) {

        $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->whereIn('user_id', $payroll_comp)->lists('category_id')->toArray());
        // dd($get_cat_id);
        foreach ($get_cat_id as $ckey => $cat_id) {
          
          $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id) {
                                $query->where('batch_id', '=', $id)
                                      ->where('provider_id', '=', $provider_id)
                                      ->where('category_id', '=', $cat_id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      // ->where('user_id', '=', $field)
                                      ->where('status', '=', 1);
                        })->whereIn('user_id', $payroll_comp)->get();

          $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

          $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
              '<table class="table table-striped"><thead>' . 
              '<th class="tbheader">Policy Number</th>' .
              '<th class="tbheader">Component Code</th>' .
              '<th class="tbheader">Agent Name</th>' .
              '<th class="tbheader">FYC</th>' .
              '<th class="tbheader">Renewal</th>' .
              // '<th class="tbheader">Total Revenue</th>' .
              '<th class="tbheader">Premium</th>' .
              '<th class="tbheader">Total Revenue</th>' .
              '</thead><tbody>';

          foreach ($get_payroll_comp as $pkey => $pfield) {

            $rows .= '<tr>' .
                      '<td>' . $pfield->policy_no . '</td>' .
                      '<td>' . $pfield->comp_code . '</td>' .
                      '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
                      '<td>' . $pfield->first_year . '</td>' .
                      '<td>' . $pfield->renewal_year . '</td>' .
                      // '<td>' . number_format($pfield->agent_banding, 2, '.', '') . '</td>' .
                      '<td>' . number_format($pfield->premium, 2) . '</td>' .
                      '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                      '</tr>';
          }

          $rows .= '</tbody></table></div>';
        }

      // }

      return Response::json($rows);
    }

    public function getCommissionProviderGroupSupervisorAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $user = Request::input('user');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('user_id', '=', $user)
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('user_id', '=', $user)
                                    ->where('status', '=', 1);
                      })->get();

        // $total = 0;
        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            // '<th class="tbheader">Total Revenue</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Revenue</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {
          // dd($pfield);
          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    // '<td>' . number_format($pfield->agent_banding, 2, '.', '') . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '</tr>';
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);

    }

    public function getCommissionProviderGroupUnitAdvisor() {

      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $unit = Request::input('unit');

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' .
          '<th class="tbheader">Advisor</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';

      $get_group_comp = GroupComputation::where(function($query) use ($id, $group) { 
                                                $query->where('group_id', '=', $group)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

      $get_sup_user = SalesAdvisorComputation::where(function($query) use ($unit, $get_group_comp, $id) {
                                              $query->where('supervisor_id', '=', $unit)
                                                    ->where('batch_id', '=', $id)
                                                    ->where('group_id', '=', $get_group_comp->id);
                                          })->lists('user_id');

      foreach ($get_sup_user as $skey => $svalue) {

        $agent_banding = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $svalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $svalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('agent_banding');

        if ($agent_banding > 0) {
          
          $get_name = User::find($svalue);
          $rows .= '<tr data-id="' . $id . '">' .
                    '<td data-id="' . $end . '">' .
                    '<a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-type="unit-commission" data-sub="commission-user-' . $svalue . '" data-user="' . $svalue . '" data-provider="' . $provider_id . '" data-id="' . $start . '">' .
                    '<i class="fa fa-plus"></i></a> ' . $get_name->name . '</td>' . 
                    '<td>' .  number_format($agent_banding, 2) . '</td>' . 
                    '</tr>';
        }
      }

      $rows .= '</tbody></table></div>';

      return Response::json($rows);

    }

    public function getCommissionProviderGroupUnitPersonalAdvisor() {

      $id = Request::input('id');
      $unit = Request::input('unit');
      $start = Request::input('start');
      $end = Request::input('end');
      $user = Request::input('user');
      $provider_id = Request::input('provider');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('user_id', '=', $user)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('user_id', '=', $user)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">Agent Name</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            // '<th class="tbheader">Total Revenue</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Revenue</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    // '<td>' . number_format($pfield->agent_banding, 2, '.', '') . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '</tr>';
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    public function getCommissionProviderGroupUnitAdvisorAdvisor() {

      $id = Request::input('id');
      $unit = Request::input('unit');
      $start = Request::input('start');
      $end = Request::input('end');
      $user = Request::input('user');
      $provider_id = Request::input('provider');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('user_id', '=', $user)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('user_id', '=', $user)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">Agent Name</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            // '<th class="tbheader">Total Revenue</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Revenue</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    // '<td>' . number_format($pfield->agent_banding, 2, '.', '') . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '</tr>';
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }


    public function getCommissionProviderGroupUnitSupervisorAdvisor() {

      $id = Request::input('id');
      $user = Request::input('user');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $supervisor = Request::input('supervisor');

      $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $supervisor) {
                            $query->where('batch_id', '=', $id)
                                  ->where('supervisor_id', '=', $supervisor);
                          })->lists('user_id');

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' .
          '<th class="tbheader">Advisor</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';
      
      foreach ($get_advisors as $skey => $svalue) {

        $agent_banding = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $svalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $svalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('agent_banding');

        if ($agent_banding > 0) {
          
          $get_name = User::find($svalue);
          $rows .= '<tr data-id="' . $id . '">' .
                    '<td data-id="' . $end . '">' .
                    '<a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-supervisor-advisor-expand" data-type="unit-commission-advisor" data-sub="advisor-commission-user-' . $svalue . '" data-user="' . $svalue . '" data-provider="' . $provider_id . '" data-id="' . $start . '">' .
                    '<i class="fa fa-plus"></i></a> ' . $get_name->name . '</td>' . 
                    '<td>' .  number_format($agent_banding, 2) . '</td>' . 
                    '</tr>';
        }
      }

      $rows .= '</tbody></table></div>';

      return Response::json($rows);

    }

    public function getCommissionProviderGroupUnitTeamAdvisor() {

      $id = Request::input('id');
      $unit = Request::input('unit');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');

      $get_group = GroupComputation::find($unit);

      $gids = [];
      $gctr = 0;
      $row_adv = '';
      $row_super = '';

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' . 
          '<th class="tbheader">Supervisors Total</th>' .
          '<th class="tbheader">Personal Supervisor Revenue</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';

      $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $get_group) {
                              $query->where('batch_id', '=', $id)
                                    ->where('group_id', '=', $get_group->id);
                            })->get(); 

      $get_sup_first = SalesSupervisorComputation::where(function($query) use ($id, $get_group) {
                              $query->where('batch_id', '=', $id)
                                    ->where('user_id', '=', Auth::user()->id)
                                    ->where('group_id', '=', $get_group->id);
                            })->first(); 

      $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $get_group, $get_sup_first) {
                            $query->where('batch_id', '=', $id)
                                  ->where('supervisor_id', '=', $get_sup_first->id)
                                  ->where('group_id', '=', $get_group->id);
                          })->get();
      
      foreach ($get_advisors as $gakey => $gafield) {
        $gids[$gctr] = $gafield->user_id;
        $gctr += 1;

        $adv_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $gafield) {
                                $query->where('batch_id', '=', $id)
                                      ->where('provider_id', '=', $provider_id)
                                      ->where('user_id', '=', $gafield->user_id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('status', '=', 1);
                        })->sum('agent_banding');

        $get_user = User::find($gafield->user_id);
        if ($adv_total > 0) {
          $row_adv .= '<tr data-id="' . $id . '"><td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-id="' . $start . '" data-provider="' . $provider_id . '" data-sub="comm-advisor-' . $gafield->id . '" data-user="' . $gafield->user_id . '" data-type="comm-advisor"><i class="fa fa-plus"></i></a> ' . $get_user->name . '</td><td></td><td>' . number_format($adv_total, 2) . '</td></tr>';
        }
      }

      foreach ($get_supervisors as $gskey => $gsfield) {
        if ($gsfield->user_id != Auth::user()->id) {
          $gids[$gctr] = $gsfield->user_id;
          $gctr += 1;
          
          $sup_ids = [];
          $sup_ctr = 0;

          $get_sup_advisors = SalesAdvisorComputation::where(function($query) use ($id, $get_group, $gsfield) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('supervisor_id', '=', $gsfield->id)
                                                            ->where('group_id', '=', $get_group->id);
                                                    })->lists('user_id');
          
          foreach ($get_sup_advisors as $gsakey => $gsavalue) {
            $sup_ids[$sup_ctr] = $gsavalue;
            $sup_ctr += 1;   
          }

          $sup_per = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $gsfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('user_id', '=', $gsfield->user_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->sum('agent_banding');

          $sup_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $gsfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->whereIn('user_id', $sup_ids)->sum('agent_banding');

          if ($sup_total > 0 || $sup_per > 0) {
            $get_name = User::find($gsfield->user_id);
            $row_super .= '<tr data-id="' . $id . '">' .
                          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-id="' . $start . '" data-provider="' . $provider_id . '" data-sub="comm-supervisor-' . $gsfield->id . '" data-supervisor="' . $gsfield->id . '" data-user="' . $gsfield->user_id .'" data-type="comm-supervisor"><i class="fa fa-plus"></i></a> ' . $gsfield->unit_code . ' - ' . number_format($sup_total, 2) . ' - ' . $get_name->name . '</td>' .
                          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-id="' . $start . '" data-provider="' . $provider_id . '" data-sub="comm-personal-' . $gsfield->id . '" data-user="' . $gsfield->user_id .'" data-type="comm-personal"><i class="fa fa-plus"></i></a> ' . number_format($sup_per, 2) . '</td>' . 
                          '<td>' . number_format($sup_total + $sup_per, 2) . '</td></tr>';
          }

        }
      }

      $rows .= $row_super . $row_adv . '</tbody></table></div>';

      return Response::json($rows);
    }

    public function getCommissionProviderGroupUnitUserSupervisorAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $user = Request::input('user');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('user_id', '=', $user)
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';
      $total = 0;

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('user_id', '=', $user)
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Revenue</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '</tr>';
          $total += $pfield->agent_banding;
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    public function getOverrideProviderGroupPersonalAdvisor() {
      $id = Request::input('id');
      $start = Request::input('start');
      $unit = Request::input('unit');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');

      $rows = '';
      $get_group = GroupComputation::where('user_id', Auth::user()->id)->where('batch_id', $id)->first();

      if ($get_group) {

        $ids = [];
        $ctr = 0;

        $total = 0;

        $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $get_group->id)->lists('user_id')->toArray();

        foreach ($get_sups_group as $gsgkey => $gsgvalue) {
          $ids[$ctr] = $gsgvalue;
          $ctr += 1;
        }

        $get_sups_group_id = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', '!=', Auth::user()->id)->where('group_id', $get_group->id)->get();

        foreach ($get_sups_group_id as $gsgikey => $gsgifield) {
          $get_adv_group = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', '=', $gsgifield->id)->lists('user_id');

          foreach ($get_adv_group as $gagkey => $gagvalue) {
            $ids[$ctr] = $gagvalue;
            $ctr += 1;
          }
        }

        $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->whereIn('user_id', $ids)->lists('category_id')->toArray());

        $total = 0;

        foreach ($get_cat_id as $ckey => $cat_id) {
          
          $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id) {
                                $query->where('batch_id', '=', $id)
                                      ->where('provider_id', '=', $provider_id)
                                      ->where('category_id', '=', $cat_id)
                                      // ->where('user_id', '=', Auth::user()->id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('status', '=', 1);
                        })->whereIn('user_id', $ids)->get();

          // dd($get_payroll_comp);
          $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

          $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
              '<table class="table table-striped"><thead>' . 
              '<th class="tbheader">Policy Number</th>' .
              '<th class="tbheader">Component Code</th>' .
              '<th class="tbheader">Agent Name</th>' .
              '<th class="tbheader">FYC</th>' .
              '<th class="tbheader">Renewal</th>' .
              '<th class="tbheader">Premium</th>' .
              '<th class="tbheader">Mgt Share</th>' .
              '<th class="tbheader">Tier3OR</th>' .
              '</thead><tbody>';

          foreach ($get_payroll_comp as $pkey => $pfield) {

            $rows .= '<tr>' .
                      '<td>' . $pfield->policy_no . '</td>' .
                      '<td>' . $pfield->comp_code . '</td>' .
                      '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
                      '<td>' . $pfield->first_year . '</td>' .
                      '<td>' . $pfield->renewal_year . '</td>' .
                      '<td>' . number_format($pfield->premium, 2) . '</td>' .
                      '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                      '<td>' . number_format($pfield->tier3_share, 2) . '</td>' .
                      '</tr>';
            
            $total += $pfield->tier3_share;
          }

          $rows .= '</tbody></table></div>';
        }
      }

      return Response::json($rows);
    }

    public function getOverrideProviderGroupUnitTeamAdvisor() {

      $id = Request::input('id');
      $unit = Request::input('unit');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');

      $get_group = GroupComputation::find($unit);

      $gids = [];
      $gctr = 0;
      $row_adv = '';
      $row_super = '';

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' . 
          '<th class="tbheader">Supervisors Total</th>' .
          '<th class="tbheader">Personal Supervisor Override</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';

      $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $get_group) {
                              $query->where('batch_id', '=', $id)
                                    ->where('group_id', '=', $get_group->id);
                            })->get();

      $get_sup_first = SalesSupervisorComputation::where(function($query) use ($id, $get_group) {
                              $query->where('batch_id', '=', $id)
                                    ->where('user_id', '=', Auth::user()->id)
                                    ->where('group_id', '=', $get_group->id);
                            })->first(); 

      $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $get_group, $get_sup_first) {
                            $query->where('batch_id', '=', $id)
                                  ->where('supervisor_id', '=', $get_sup_first->id)
                                  ->where('group_id', '=', $get_group->id);
                          })->get();
      
      foreach ($get_advisors as $gakey => $gafield) {
        $gids[$gctr] = $gafield->user_id;
        $gctr += 1;

        $adv_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $gafield) {
                                $query->where('batch_id', '=', $id)
                                      ->where('provider_id', '=', $provider_id)
                                      ->where('user_id', '=', $gafield->user_id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('status', '=', 1);
                        })->sum('manage_share');

        $get_user = User::find($gafield->user_id);
        if ($adv_total > 0) {
          $row_adv .= '<tr data-id="' . $id . '"><td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-id="' . $start . '" data-provider="' . $provider_id . '" data-sub="override-advisor-' . $gafield->id . '" data-user="' . $gafield->user_id .'" data-type="override-advisor"><i class="fa fa-plus"></i></a> ' . $get_user->name . '</td><td></td><td>' . number_format($adv_total, 2) . '</td></tr>';
        }
      }

      foreach ($get_supervisors as $gskey => $gsfield) {
        if ($gsfield->user_id != Auth::user()->id) {
          $gids[$gctr] = $gsfield->user_id;
          $gctr += 1;
          
          $sup_ids = [];
          $sup_ctr = 0;

          $get_sup_advisors = SalesAdvisorComputation::where(function($query) use ($id, $get_group, $gsfield) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('supervisor_id', '=', $gsfield->id)
                                                            ->where('group_id', '=', $get_group->id);
                                                    })->lists('user_id');
          
          foreach ($get_sup_advisors as $gsakey => $gsavalue) {
            $sup_ids[$sup_ctr] = $gsavalue;
            $sup_ctr += 1;   
          }

          $sup_per = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $gsfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('user_id', '=', $gsfield->user_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->sum('tier2_share');

          $sup_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $gsfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->whereIn('user_id', $sup_ids)->sum('tier2_share');

          if ($sup_total > 0 || $sup_per > 0) {
            $get_name = User::find($gsfield->user_id);
            $row_super .= '<tr data-id="' . $id . '">' .
                          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-id="' . $start . '" data-provider="' . $provider_id . '" data-sub="override-supervisor-' . $gsfield->id . '" data-supervisor="' . $gsfield->id . '" data-user="' . $gsfield->user_id .'" data-type="override-supervisor"><i class="fa fa-plus"></i></a> ' . $gsfield->unit_code . ' - ' . number_format($sup_total, 2) . ' - ' . $get_name->name . '</td>' .
                          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-id="' . $start . '" data-provider="' . $provider_id . '" data-sub="override-personal-' . $gsfield->id . '" data-user="' . $gsfield->user_id .'" data-type="override-personal"><i class="fa fa-plus"></i></a> ' . number_format($sup_per, 2) . '</td>' . 
                          '<td>' . number_format($sup_total + $sup_per, 2) . '</td></tr>';
          }

        }
      }

      $rows .= $row_super . $row_adv . '</tbody></table></div>';

      return Response::json($rows);
    }

    public function getOverrideProviderGroupUnitPersonalAdvisor() {

      $id = Request::input('id');
      $unit = Request::input('unit');
      $start = Request::input('start');
      $end = Request::input('end');
      $user = Request::input('user');
      $provider_id = Request::input('provider');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('user_id', '=', $user)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';
      $total = 0;

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('user_id', '=', $user)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">Agent Name</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Mgt Share</th>' .
            '<th class="tbheader">Tier2OR</th>' .
            '<th class="tbheader">Tier3OR</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                    '<td>' . number_format($pfield->tier2_share, 2) . '</td>' .
                    '<td>' . number_format($pfield->tier3_share, 2) . '</td>' .
                    '</tr>';

          $total += $pfield->manage_share;
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    public function getOverrideProviderGroupUnitSupervisorAdvisor() {

      $id = Request::input('id');
      $user = Request::input('user');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $supervisor = Request::input('supervisor');

      $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $supervisor) {
                            $query->where('batch_id', '=', $id)
                                  ->where('supervisor_id', '=', $supervisor);
                          })->lists('user_id');

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' .
          '<th class="tbheader">Advisor</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';
      
      foreach ($get_advisors as $skey => $svalue) {

        $manage_share = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $svalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $svalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('manage_share');

        if ($manage_share > 0) {

          $get_name = User::find($svalue);
          $rows .= '<tr data-id="' . $id . '">' .
                    '<td data-id="' . $end . '">' .
                    '<a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-supervisor-advisor-expand" data-type="unit-override-advisor" data-sub="advisor-override-user-' . $svalue . '" data-user="' . $svalue . '" data-provider="' . $provider_id . '" data-id="' . $start . '">' .
                    '<i class="fa fa-plus"></i></a> ' . $get_name->name . '</td>' . 
                    '<td>' .  number_format($manage_share, 2) . '</td>' . 
                    '</tr>';
        }
      }

      $rows .= '</tbody></table></div>';

      return Response::json($rows);

    }

    public function getOverrideProviderGroupUnitAdvisorAdvisor() {

      $id = Request::input('id');
      $unit = Request::input('unit');
      $start = Request::input('start');
      $end = Request::input('end');
      $user = Request::input('user');
      $provider_id = Request::input('provider');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('user_id', '=', $user)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';
      $total = 0;

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('user_id', '=', $user)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">Agent Name</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Override</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                    '</tr>';
          $total += $pfield->manage_share;
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    public function getOverrideProviderGroupUnitUserSupervisorAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $user = Request::input('user');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('user_id', '=', $user)
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';
      $total = 0;

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('user_id', '=', $user)
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Override</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                    '</tr>';
          $total += $pfield->manage_share;
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    public function getCommissionProviderGroupUnitUserAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $user = Request::input('user');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('user_id', '=', $user)
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';
      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('user_id', '=', $user)
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Total Revenue</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '</tr>';
        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }


    public function getOverrideProviderAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');

      if (Auth::user()->usertype_id == 8) {
        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {
          $get_sup_id = SalesSupervisorComputation::where(function($query) use ($id) {
                                                      $query->where('user_id', '=', Auth::user()->id)
                                                            ->where('batch_id', '=', $id); 
                                                    })->first();

          $ids = [];
          $ids[0] = Auth::user()->id;
          if ($get_sup_id) {
            $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($id, $get_sup_id) {
                                                      $query->where('supervisor_id', '=', $get_sup_id->id)
                                                            ->where('batch_id', '=', $id); 
                                                    })->get();

            $ctr = 1;
            foreach ($get_adv_ids as $advkey => $advfield) {
              $ids[$ctr] = $advfield->user_id;
              $ctr += 1;
            }
          }

          $get_user_id = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $provider_id)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                              })->whereIn('user_id', $ids)->lists('user_id');
        }
      } else {
        $get_user_id = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('provider_id', '=', $provider_id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                            })->lists('user_id');
      }

      $group = [];
      $ctr = 0;

      foreach (array_unique($get_user_id->toArray()) as $key => $value) {
        $get_group_sup = SalesSupervisorComputation::where(function($query) use ($id, $value) {
                                                      $query->where('user_id', '=', $value)
                                                            ->where('batch_id', '=', $id);
                                                      })->first();
        if ($get_group_sup) {
          $group[$ctr] = $get_group_sup->group_id;
          $ctr += 1;
        } else if (!$get_group_sup) {
          $get_group_adv = SalesAdvisorComputation::where(function($query) use ($id, $value) {
                                                      $query->where('user_id', '=', $value)
                                                            ->where('batch_id', '=', $id);
                                                      })->first();
          if ($get_group_adv) {
            $group[$ctr] = $get_group_adv->group_id;
            $ctr += 1;
          }
        }
      }

      $rows = '';
      $user_no_group = [];

      if (Auth::user()->usertype_id == 8) {

        $check_team = SalesSupervisorComputation::where(function($query) use ($id) {
                                                      $query->where('user_id', '=', Auth::user()->id)
                                                            ->where('batch_id', '=', $id);
                                                      })->first();

        if ($check_team) {
          $id = Request::input('id');
          $start = Request::input('start');
          $end = Request::input('end');
          $provider_id = Request::input('provider');

          $get_group_comp = GroupComputation::where(function($query) use ($id, $check_team) {
                                                      $query->where('id', '=', $check_team->group_id)
                                                            ->where('batch_id', '=', $id);
                                              })->first();

          $get_group = Group::find($get_group_comp->group_id);

          $group = $get_group_comp->group_id;
          $row_adv = '';
          $row_super = '';
          $rows = '<div class="provider-table">' .
              '<table class="table table-striped"><thead>' . 
              '<th class="tbheader">Team Total</th>' .
              '<th class="tbheader">Personal Override</th>' .
              '<th class="tbheader">Total</th>' .
              '</thead><tbody>';
          

          if (Auth::user()->usertype_id == 8) {

            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

            if ($check_rank->designations->rank == "Supervisor") {

              $check_tier3 = Group::where('user_id', '=', Auth::user()->id)->first();
              $team_id = '';

              if ($check_tier3) {
                $check_group = GroupComputation::where(function($query) use ($id) {
                                                  $query->where('batch_id', '=', $id)
                                                        ->where('user_id', '=', Auth::user()->id);
                                                })->first();

                $sup_total = 0;
                $sup_per = 0;

                if($check_group) {

                  $team_id = $check_group->id;

                  $ids = [];
                  $ctr = 0;

                  $total = 0;

                  $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $check_group->id)->lists('user_id')->toArray();

                  foreach ($get_sups_group as $gsgkey => $gsgvalue) {
                    $ids[$ctr] = $gsgvalue;
                    $ctr += 1;
                  }

                  $get_sups_group_id = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', '!=', Auth::user()->id)->where('group_id', $check_group->id)->get();

                  foreach ($get_sups_group_id as $gsgikey => $gsgifield) {
                    $get_adv_group = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', '=', $gsgifield->id)->lists('user_id');

                    foreach ($get_adv_group as $gagkey => $gagvalue) {
                      $ids[$ctr] = $gagvalue;
                      $ctr += 1;
                    }
                  }

                  $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $check_group) {
                                                          $query->where('batch_id', '=', $id)
                                                                ->where('group_id', '=', $check_group->id);
                                                        })->lists('user_id'); 

                  $sup_per = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('provider_id', '=', $provider_id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('tier3_share');

                  $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $check_group) {
                                                          $query->where('batch_id', '=', $id)
                                                                ->where('group_id', '=', $check_group->id);
                                                        })->lists('user_id'); 

                  $ga_ids = array_merge($get_supervisors->toArray(), $get_advisors->toArray());
                  
                  $sup_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('provider_id', '=', $provider_id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('status', '=', 1);
                                      })->whereIn('user_id', $ga_ids)->sum('manage_share');
                }

                $rows .= $row_super .= '<tr data-id="' . $id . '">' .
                                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit-group-override" data-unit="' . $team_id . '" data-item="unit-group-override-' . $team_id . '" data-supervisor="' . $team_id . '" data-user="' . Auth::user()->id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . number_format($sup_total - $sup_per, 2) . '</td>' .
                                      '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit-personal-override" data-unit="' . $team_id . '" data-item="unit-personal-override-' . $team_id . '" data-supervisor="' . $team_id . '" data-user="' . Auth::user()->id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . number_format($sup_per, 2) . '</td>' . 
                                      '<td>' . number_format($sup_total, 2) . '</td></tr>';

                $rows .= '</tbody></table></div>';

                return Response::json($rows);

              } else {
                $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                            $query->where('group_id', '=', $get_group_comp->id)
                                                                  ->where('batch_id', '=', $id)
                                                                  ->where('user_id', '=', Auth::user()->id);
                                                            })->get();
              }
            }

          } else {
            $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                        $query->where('group_id', '=', $get_group_comp->id)
                                                              ->where('batch_id', '=', $id);
                                                        })->get();
          }

          foreach ($get_sup_user as $key => $field) {

            $manage_share = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $field) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('user_id', '=', $field->user_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->sum('tier2_share');

            $get_adv_user = SalesAdvisorComputation::where(function($query) use ($id, $field) {
                                                        $query->where('supervisor_id', '=', $field->id)
                                                              ->where('batch_id', '=', $id);
                                                        })->get();

            $sup_total = $manage_share;
            $adv_total = 0;

            foreach ($get_adv_user as $akey => $afield) {

              $manage_share += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $afield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('user_id', '=', $afield->user_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->sum('tier2_share');

              $adv_total += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $afield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('user_id', '=', $afield->user_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('status', '=', 1);
                          })->sum('tier2_share');

            }

            $get_name = User::find($field->user_id);

            $check_total = $adv_total + $sup_total;

            if ($check_total > 0) {
              $rows .= '<tr data-id="' . $id . '">' .
              '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit" data-unit="' . $field->id. '" data-item="unit-' . $field->id . '" data-supervisor="' . $field->id . '" data-user="' . $field->user_id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' /*. $get_group->code . ' - ' . $field->unit_code . ' - '*/ .  number_format($adv_total, 2) . '</td>' . 
              '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="supervisor" data-item="user-' . $field->id . '" data-supervisor="' . $field->id . '" data-user="' . $field->user_id . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' .  number_format($sup_total, 2) . '</td>' . 
              '<td>' .  number_format($sup_total + $adv_total, 2) . '</td>' . 
              '</tr>';
            }
          }

          $rows .= '</tbody></table></div>';

          return Response::json($rows);
        }
      } else {
        $rows = '<div class="provider-table">' .
                  '<table class="table table-striped"><thead>' . 
                  '<th class="tbheader">Group Code</th>' .
                  '<th class="tbheader">Group Owner</th>' .
                  '<th class="tbheader">Total</th>' .
                  '</thead><tbody>';
        
        $group_comp_total = 0;

        foreach (array_unique($group) as $gkey => $gvalue) {
          $total_group = 0;
          foreach (array_unique($get_user_id->toArray()) as $ukey => $uvalue) {
          $get_group_sup = SalesSupervisorComputation::where(function($query) use ($id, $uvalue, $gvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('group_id', '=', $gvalue)
                                                              ->where('user_id', '=', $uvalue);
                                                        })->first();

          $get_group_adv = SalesAdvisorComputation::where(function($query) use ($id, $uvalue, $gvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('group_id', '=', $gvalue)
                                                              ->where('user_id', '=', $uvalue);
                                                        })->first();

            if ($get_group_sup) {
              $total_group += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $uvalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $uvalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('manage_share');

            } else if ($get_group_adv) {
              $total_group += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $uvalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $uvalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('manage_share');
            } else {
              $user_no_group[$ukey] = $uvalue;
            }
          }

          $get_group_comp = GroupComputation::find($gvalue);
          $get_group = Group::find($get_group_comp->group_id);

          $get_name = null;

          if ($get_group_comp->user_id) {
            $get_owner = User::find($get_group_comp->user_id);

            if ($get_owner) {
              $get_name = $get_owner->name;
            }
          }

          $rows .= '<tr data-id="' . $id . '">' .
                    '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-expand" data-breakdown="team-override" data-sub="override-' . $get_group->id . '" data-group="' . $get_group->id . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' .  $get_group->code . '</td>' . 
                    '<td>' .  $get_name . '</td>' . 
                    '<td>' .  number_format($total_group, 2) . '</td>' . 
                    '</tr>';

          $group_comp_total += $total_group;
        }
      }


      $total_no_group = 0;
      foreach ($user_no_group as $ngkey => $ngvalue) {
        $total_no_group = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $ngvalue) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('user_id', '=', $ngvalue)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('status', '=', 1);
                })->sum('manage_share');
      }

      if (Auth::user()->usertype_id != 8) {
        if ($total_no_group > 0) {

        $total_all_group = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('provider_id', '=', $provider_id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                            })->sum('manage_share');

          $rows .= '<tr data-id="' . $id . '">' .
            '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-expand" data-breakdown="no_team-override" data-sub="override-nogroup" data-group="nogroup" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> No Team</td>' . 
            '<td></td>' .
            '<td>' .  number_format($total_all_group - $group_comp_total, 2) . '</td>' . 
            '</tr>';
        }
      } else {

        $get_group_sup = SalesSupervisorComputation::where(function($query) use ($id) {
                                                        $query->where('user_id', '=', Auth::user()->id)
                                                              ->where('batch_id', '=', $id);
                                                    })->first();

        $get_group_adv = SalesAdvisorComputation::where(function($query) use ($id) {
                                                        $query->where('user_id', '=', Auth::user()->id)
                                                              ->where('batch_id', '=', $id);
                                                    })->first();

        if (!$get_group_sup && !$get_group_adv) {

          $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                                $query->where('batch_id', '=', $id)
                                      ->where('provider_id', '=', $provider_id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('user_id', '=', Auth::user()->id)
                                      ->where('status', '=', 1);
                        })->lists('category_id')->toArray());

          $rows = '';

          foreach ($get_cat_id as $ckey => $cat_id) {
            
            $payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('category_id', '=', $cat_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('user_id', '=', Auth::user()->id)
                                        ->where('status', '=', 1);
                          })->get();

            $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

            $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
                '<table class="table table-striped"><thead>' . 
                '<th class="tbheader">Policy Number</th>' .
                '<th class="tbheader">Component Code</th>' .
                '<th class="tbheader">FYC</th>' .
                '<th class="tbheader">Renewal</th>' .
                '<th class="tbheader">Total Override</th>' .
                '<th class="tbheader">Premium</th>' .
                '<th class="tbheader">Override</th>' .
                '</thead><tbody>';

            foreach ($payroll_comp as $key => $field) {
              $rows .= '<tr>' .
                        '<td>' . $field->policy_no . '</td>' .
                        '<td>' . $field->comp_code . '</td>' .
                        '<td>' . $field->first_year . '</td>' .
                        '<td>' . $field->renewal_year . '</td>' .
                        '<td>' . number_format($field->agent_banding, 2) . '</td>' .
                        '<td>' . number_format($field->premium, 2) . '</td>' .
                        '<td>' . number_format($field->manage_share, 2) . '</td>' .
                        '</tr>';

            }

            $rows .= '</tbody></table></div>';
          }
        }
      }

      return Response::json($rows);
    }

    public function getOverrideProviderGroupAdvisor() {
      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' . 
          '<th class="tbheader">Unit Code - Total</th>' .
          '<th class="tbheader">Personal Override</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';

      $get_group_comp = GroupComputation::where(function($query) use ($id, $group) { 
                                                $query->where('group_id', '=', $group)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

      if (Auth::user()->usertype_id == 8) {
        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {
          $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                      $query->where('group_id', '=', $get_group_comp->id)
                                                            ->where('batch_id', '=', $id)
                                                            ->where('user_id', '=', Auth::user()->id);
                                                      })->lists('user_id');
        }
      } else {
        $get_sup_user = SalesSupervisorComputation::where(function($query) use ($id, $get_group_comp) {
                                                      $query->where('group_id', '=', $get_group_comp->id)
                                                            ->where('batch_id', '=', $id); 
                                                    })->lists('user_id');
      }


      // dd($get_sup_user);

      foreach ($get_sup_user as $key => $field) {
        $ids = [];

        $if_group = GroupComputation::where('batch_id', $id)->where('user_id', $field)->first();

        if ($if_group) {
          $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $if_group->id)->lists('user_id');

          $get_sups_group_id = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', '!=', $field)->where('group_id', $if_group->id)->get();

          $personal_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $field) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('status', '=', 1);
                })->whereIn('user_id', $get_sups_group)->sum('tier3_share');

          foreach ($get_sups_group_id as $gsgikey => $gsgifield) {
            $get_adv_group = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', '=', $gsgifield->id)->lists('user_id');

            $personal_total += PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $field) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('status', '=', 1);
                })->whereIn('user_id', $get_adv_group)->sum('tier3_share');
          }


          $get_sup_info = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', $field)->first();

          if ($get_sup_info) {
            $get_adv_group = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', '=', $get_sup_info->id)->lists('user_id');
                    // dd($get_adv_group);
            $manage_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $field) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('status', '=', 1);
                })->whereIn('user_id', $get_adv_group)->sum('tier3_share');
          }

        } else {
         $if_sup = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', $field)->first();

          if ($if_sup) {

            $personal_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $field) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('user_id', $field)
                              ->where('status', '=', 1);
                })->sum('tier2_share');

            $get_adv_sups = SalesAdvisorComputation::where('batch_id', $id)->where('user_id', '!=', $field)->where('supervisor_id', $if_sup->id)->lists('user_id');

            $manage_total = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end) {
                        $query->where('batch_id', '=', $id)
                              ->where('provider_id', '=', $provider_id)
                              ->where('upload_date', '>=', $start . " 00:00:00")
                              ->where('upload_date', '<=', $end . " 00:00:00")
                              ->where('status', '=', 1);
                })->whereIn('user_id', $get_adv_sups)->sum('tier2_share');

          }
        }

        $get_sup_id = SalesSupervisorComputation::where(function($query) use ($id, $field) {
                                                      $query->where('user_id', '=', $field)
                                                            ->where('batch_id', '=', $id); 
                                                    })->first();
        $sup_id = null;
        $unit_code = null;
        $get_adv_user = null;
        $adv_total = 0;

        if ($get_sup_id) {
          $sup_id = $get_sup_id->id;
          $unit_code = $get_sup_id->unit_code;
        }

        $get_name = User::find($field);
        $check_group = GroupComputation::where('user_id', $field)->where('batch_id', $id)->first();

        $info = 0;
        if ($field == $get_group_comp->user_id) {
          $info = 1;
        }

        $check_total = $manage_total + $personal_total;

        if ($check_total > 0 || $check_group) {
          $rows .= '<tr data-id="' . $id . '"' . ($info == 1 ? 'class="info"' : '' ) . '>' .
          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="unit" data-unit="' . $sup_id . '" data-item="unit-' . $sup_id . '" data-supervisor="' . $sup_id . '" data-user="' . $field . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' . $unit_code . ' - ' .  number_format($manage_total, 2) . ' - ' . $get_name->name . '</td>' . 
          '<td data-id="' . $end . '"><a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-expand" data-type="supervisor" data-item="user-' . $sup_id . '" data-supervisor="' . $sup_id . '" data-user="' . $field . '" data-group="' .  $group . '" data-provider="' . $provider_id . '" ' . ($if_group ? 'data-owner="' . $field . '"' : '') . 'data-id="' . $start . '"><i class="fa fa-plus"></i></a> ' .  number_format($personal_total, 2) . '</td>' . 
          '<td>' .  number_format($personal_total + $manage_total, 2) . '</td>' . 
          '</tr>';
        }
      }

      $rows .= '</tbody></table></div>';

      return Response::json($rows);
    }

    public function getOverrideProviderGroupUnitAdvisor() {

      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $unit = Request::input('unit');
      $user = Request::input('user');


      $if_group = GroupComputation::where(function($query) use ($id, $group, $user) { 
                                                $query->where('group_id', '=', $group)
                                                      ->where('user_id', '=', $user)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

      $rows = '<div class="provider-table">' .
          '<table class="table table-striped"><thead>' .
          '<th class="tbheader">Agent Name</th>' .
          '<th class="tbheader">Total</th>' .
          '</thead><tbody>';
      

      $get_group_comp = GroupComputation::where(function($query) use ($id, $group) { 
                                                $query->where('group_id', '=', $group)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

      $get_sup_user = SalesAdvisorComputation::where(function($query) use ($unit, $get_group_comp, $id) {
                                              $query->where('supervisor_id', '=', $unit)
                                                    ->where('batch_id', '=', $id)
                                                    ->where('group_id', '=', $get_group_comp->id);
                                          })->lists('user_id');

      foreach ($get_sup_user as $skey => $svalue) {

        if ($if_group) {
        $manage_share = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $svalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $svalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('tier3_share');
      } else {
        $manage_share = PayrollComputation::where(function($query) use ($id, $provider_id, $start, $end, $svalue) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('user_id', '=', $svalue)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('status', '=', 1);
                      })->sum('tier2_share');
      }

        if ($manage_share > 0) {
          
          $get_name = User::find($svalue);
          $rows .= '<tr data-id="' . $id . '">' .
                    '<td data-id="' . $end . '">' .
                    '<a class="btn btn-xs btn-table btn-advisor-item-sub-group-unit-advisor-expand" data-type="unit-override" data-sub="override-user-' . $svalue . '" data-user="' . $svalue . '" data-owner="' . $user . '" data-provider="' . $provider_id . '" data-id="' . $start . '">' .
                    '<i class="fa fa-plus"></i></a> ' . $get_name->name . '</td>' . 
                    '<td>' .  number_format($manage_share, 2) . '</td>' . 
                    '</tr>';
        }
      }

      $rows .= '</tbody></table></div>';

      return Response::json($rows);

    }

    public function getOverrideProviderGroupUnitUserAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $user = Request::input('user');
      $owner = Request::input('owner');


      $if_group = GroupComputation::where(function($query) use ($id, $group, $owner) { 
                                                $query->where('user_id', '=', $owner)
                                                      ->where('batch_id', '=', $id);
                                          })->first();

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('user_id', '=', $user)
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('user_id', '=', $user)
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Agent Share</th>' .
            '<th class="tbheader">MgtShare</th>' .
            ($if_group ? '<th class="tbheader">Tier3 Share</th>' : '<th class="tbheader">Tier2 Share</th>' ) .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                    ($if_group ? '<td>' . number_format($pfield->tier3_share, 2) . '</td>' :
                      '<td>' . number_format($pfield->tier2_share, 2) . '</td>' ) .
                    '</tr>';

        }

        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    public function getOverrideProviderGroupSupervisorAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');
      $group = Request::input('group');
      $user = Request::input('user');
      $owner = Request::input('owner');

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('user_id', '=', $user)
                                  ->where('status', '=', 1);
                    })->lists('category_id')->toArray());

      $rows = '';
      $check_tier3 = GroupComputation::where('user_id', $user)->where('batch_id', $id)->first();

      if ($check_tier3) {

        $ids = [];
        $ctr = 0;

        $total = 0;

        $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $check_tier3->id)->lists('user_id')->toArray();

        foreach ($get_sups_group as $gsgkey => $gsgvalue) {
          $ids[$ctr] = $gsgvalue;
          $ctr += 1;
        }

        $get_sups_group_id = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', '!=', $user)->where('group_id', $check_tier3->id)->get();

        foreach ($get_sups_group_id as $gsgikey => $gsgifield) {
          $get_adv_group = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', '=', $gsgifield->id)->lists('user_id');

          foreach ($get_adv_group as $gagkey => $gagvalue) {
            $ids[$ctr] = $gagvalue;
            $ctr += 1;
          }
        }


        $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $user) {
                      $query->where('batch_id', '=', $id)
                            ->where('provider_id', '=', $provider_id)
                            ->where('upload_date', '>=', $start . " 00:00:00")
                            ->where('upload_date', '<=', $end . " 00:00:00")
                            ->where('status', '=', 1);
              })->whereIn('user_id', $ids)->lists('category_id')->toArray());

        foreach ($get_cat_id as $ckey => $cat_id) {

          $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

          $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
              '<table class="table table-striped"><thead>' . 
              '<th class="tbheader">Policy Number</th>' .
              '<th class="tbheader">Component Code</th>' .
              '<th class="tbheader">Agent Name</th>' .
              '<th class="tbheader">FYC</th>' .
              '<th class="tbheader">Renewal</th>' .
              '<th class="tbheader">Premium</th>' .
              '<th class="tbheader">Mgt Share</th>' .
              '<th class="tbheader">Tier3OR</th>' .
              '</thead><tbody>';
          foreach ($ids as $gskey => $gsvalue) {
            
            $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $gsvalue) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('provider_id', '=', $provider_id)
                                        ->where('category_id', '=', $cat_id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('user_id', '=', $gsvalue)
                                        ->where('status', '=', 1);
                          })->get();
            $name = null;

            $get_name = User::find($gsvalue);

            if ($get_name) {
              $name = $get_name->name;
            }

            foreach ($get_payroll_comp as $pkey => $pfield) {

                $rows .= '<tr>' .
                          '<td>' . $pfield->policy_no . '</td>' .
                          '<td>' . $pfield->comp_code . '</td>' .
                          '<td>' . $name . '</td>' .
                          '<td>' . $pfield->first_year . '</td>' .
                          '<td>' . $pfield->renewal_year . '</td>' .
                          '<td>' . number_format($pfield->premium, 2) . '</td>' .
                          '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                          '<td>' . number_format($pfield->tier3_share, 2) . '</td>' .
                          '</tr>';

                  $total += $pfield->tier3_share;
            }
          }

          $rows .= '</tbody></table></div>';
        }

        return Response::json($rows);
      }

      foreach ($get_cat_id as $ckey => $cat_id) {

        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id, $user) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    ->where('user_id', '=', $user)
                                    ->where('status', '=', 1);
                      })->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        $get_group = GroupComputation::where('user_id', $user)->where('batch_id', $id)->first();

        if ($get_group) {
          $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
              '<table class="table table-striped"><thead>' . 
              '<th class="tbheader">Policy Number</th>' .
              '<th class="tbheader">Component Code</th>' .
              '<th class="tbheader">FYC</th>' .
              '<th class="tbheader">Renewal</th>' .
              '<th class="tbheader">Premium</th>' .
              '<th class="tbheader">Agent Share</th>' .
              '<th class="tbheader">MgtOR</th>' .
              '</thead><tbody>';
        } else {
          $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
              '<table class="table table-striped"><thead>' . 
              '<th class="tbheader">Policy Number</th>' .
              '<th class="tbheader">Component Code</th>' .
              '<th class="tbheader">FYC</th>' .
              '<th class="tbheader">Renewal</th>' .
              '<th class="tbheader">Premium</th>' .
              '<th class="tbheader">Mgt Share</th>' .
              '<th class="tbheader">Tier2OR</th>' .
              '<th class="tbheader">Tier3OR</th>' .
              '</thead><tbody>';
        }

        foreach ($get_payroll_comp as $pkey => $pfield) {
          if ($pfield->show_tier == 1) {
            $rows .= '<tr>' .
                      '<td>' . $pfield->policy_no . '</td>' .
                      '<td>' . $pfield->comp_code . '</td>' .
                      '<td>' . $pfield->first_year . '</td>' .
                      '<td>' . $pfield->renewal_year . '</td>' .
                      '<td>' . number_format($pfield->premium, 2) . '</td>' .
                      '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                      '<td>' . number_format($pfield->tier2_share, 2) . '</td>' .
                      '<td>' . number_format($pfield->tier3_share, 2) . '</td>' .
                      '</tr>';
          } else {
            $rows .= '<tr>' .
                      '<td>' . $pfield->policy_no . '</td>' .
                      '<td>' . $pfield->comp_code . '</td>' .
                      '<td>' . $pfield->first_year . '</td>' .
                      '<td>' . $pfield->renewal_year . '</td>' .
                      '<td>' . number_format($pfield->premium, 2) . '</td>' .
                      '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                      '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                      '</tr>';
          }

        }

        $rows .= '</tbody></table></div>';
      }
      return Response::json($rows);

    }

    public function getOverrideProviderNoGroupAdvisor() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $provider_id = Request::input('provider');

      $get_group_sup = SalesSupervisorComputation::where('batch_id', '=', $id)->lists('user_id');
      $get_group_adv = SalesAdvisorComputation::where('batch_id', '=', $id)->lists('user_id');

      $user_group = [];
      $ctr = 0;

      foreach ($get_group_sup as $skey => $svalue) {
        $user_group[$ctr] = $svalue;
        $ctr += 1;
      }

      foreach ($get_group_adv as $akey => $avalue) {
        $user_group[$ctr] = $avalue;
        $ctr += 1;
      }

      $payroll_comp = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->whereNotIn('user_id', $user_group)->lists('user_id')->toArray());

      $rows = '';

      $get_cat_id = array_unique(PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end) {
                            $query->where('batch_id', '=', $id)
                                  ->where('provider_id', '=', $provider_id)
                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                  ->where('status', '=', 1);
                    })->whereIn('user_id', $payroll_comp)->lists('category_id')->toArray());

      foreach ($get_cat_id as $ckey => $cat_id) {
        
        $get_payroll_comp = PayrollComputation::with('getPayrollComputationUser')->where(function($query) use ($id, $provider_id, $start, $end, $cat_id) {
                              $query->where('batch_id', '=', $id)
                                    ->where('provider_id', '=', $provider_id)
                                    ->where('category_id', '=', $cat_id)
                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                    // ->where('user_id', '=', $field)
                                    ->where('status', '=', 1);
                      })->whereIn('user_id', $payroll_comp)->get();

        $get_cat_name = ProviderClassification::where('id', '=', $cat_id)->first();

        // $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
        //     '<table class="table table-striped"><thead>' . 
        //     '<th class="tbheader">Policy Number</th>' .
        //     '<th class="tbheader">Component Code</th>' .
        //     '<th class="tbheader">Agent Name</th>' .
        //     '<th class="tbheader">FYC</th>' .
        //     '<th class="tbheader">Renewal</th>' .
        //     '<th class="tbheader">Total Override</th>' .
        //     '<th class="tbheader">Premium</th>' .
        //     '<th class="tbheader">Override</th>' .
        //     '</thead><tbody>';

        // foreach ($get_payroll_comp as $pkey => $pfield) {
        //   $rows .= '<tr>' .
        //             '<td>' . $pfield->policy_no . '</td>' .
        //             '<td>' . $pfield->comp_code . '</td>' .
        //             '<td>' . $pfield->getPayrollComputationUser->name . '</td>' .
        //             '<td>' . $pfield->first_year . '</td>' .
        //             '<td>' . $pfield->renewal_year . '</td>' .
        //             '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
        //             '<td>' . number_format($pfield->premium, 2) . '</td>' .
        //             '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
        //             '</tr>';

        // }

        $rows .= '<big>' . $get_cat_name->name . '</big><div class="provider-table">' .
            '<table class="table table-striped"><thead>' . 
            '<th class="tbheader">Policy Number</th>' .
            '<th class="tbheader">Component Code</th>' .
            '<th class="tbheader">FYC</th>' .
            '<th class="tbheader">Renewal</th>' .
            '<th class="tbheader">Premium</th>' .
            '<th class="tbheader">Agent Share</th>' .
            '<th class="tbheader">MgtShare</th>' .
            '</thead><tbody>';

        foreach ($get_payroll_comp as $pkey => $pfield) {

          $rows .= '<tr>' .
                    '<td>' . $pfield->policy_no . '</td>' .
                    '<td>' . $pfield->comp_code . '</td>' .
                    '<td>' . $pfield->first_year . '</td>' .
                    '<td>' . $pfield->renewal_year . '</td>' .
                    '<td>' . number_format($pfield->premium, 2) . '</td>' .
                    '<td>' . number_format($pfield->agent_banding, 2) . '</td>' .
                    '<td>' . number_format($pfield->manage_share, 2) . '</td>' .
                    '</tr>';

        }
        
        $rows .= '</tbody></table></div>';
      }

      return Response::json($rows);
    }

    /**
     * Display a listing of the invoice summaries(supervisor).
     *
     * @return Response
     */
    public function indexSummarySupervisor()
    {
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
         $this->data['settings'] = Settings::first();                      })->first();
        $this->data['title'] = "Payroll Management";
        return view('payroll-management.payroll-summary-supervisor.list', $this->data);
    }


    public function getFeed() {
      $categories = ProviderClassification::where('provider_id', Request::input('id'))->get();
      $result['categories'] = $categories->toArray();

      return Response::json($result);
    }

    public function getParser() {
      $id =  Request::input('id');

      $parsers = '<option class="hide">Select:</option>' .
                 '<option value="Custom">Custom</option>';

      if ($id == 1 || $id == 2 || $id == 3 || $id == 4 || $id == 5 || $id == 6 || $id == 7 || $id == 8 || $id == 9 || $id == 11 || $id == 12 || $id == 20 || $id == 22) {
        $parsers .= '<option value="Preset">Preset</option>';
      }

      return Response::json($parsers);
    }

    /**
     * Upload Data Feed
     *
     * @return Response
     */
    public function upload()
    {
      $input = Request::all();
    
      $rules = [
          'category_id' => 'required',
          'provider_id' => 'required',
          'parser_type' => 'required',
          'upload' => 'required',
          'upload_date' => 'required|date',
          'agent_check' => !array_get($input, 'policy_check') && array_get($input, 'parser_type') == "Custom" ? 'required' : '',
          'policy_check' => !array_get($input, 'agent_check') && array_get($input, 'parser_type') == "Custom" ? 'required' : '',
      ];

      // field name overrides
      $names = [

      ];

      // field name overrides
      $messages = [
        'provider_id.required' => 'error-provider_id|*Select value',
        'category_id.required' => 'error-category_id|*Select value',
        'parser_type.required' => 'error-parser_type|*Select value',
        'agent_check.required' => 'error-tracking_type|*Select value',
        'policy_check.required' => 'error-tracking_type|*Select value',
        'upload.required' => 'error-upload|*No file is selected',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules, $messages);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

        $file = Request::file('upload');

        $upload = new UploadFeed();
        $upload->provider_id = Request::input('provider_id');
        $upload->category_id = Request::input('category_id');
        $upload->parser_type = Request::input('parser_type');

        if (array_get($input, 'agent_check')) {
          $upload->tracking_type = 'Agent';
        } 
        if (array_get($input, 'policy_check')) {
          $upload->tracking_type = 'Policy';
        }
        if ((array_get($input, 'agent_check')) && (array_get($input, 'policy_check'))) {
          $upload->tracking_type = 'AgentPolicy';
        } 

        // default
        if (!array_get($input, 'agent_check') && !array_get($input, 'policy_check')) {
          $upload->tracking_type = 'Agent';
        }

        if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 4 || Request::input('category_id') == 5 || Request::input('category_id') == 6 || Request::input('category_id') == 7 || Request::input('category_id') == 8 || Request::input('category_id') == 9 || Request::input('category_id') == 11 || Request::input('category_id') == 20 || Request::input('category_id') == 22)) {
          $upload->tracking_type = 'AgentPolicy';
        }

        if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 1 || Request::input('category_id') == 2 || Request::input('category_id') == 3)) {
          $upload->tracking_type = 'AgentPolicy';
        }

        $upload->save();

        $get_code_feed_type = ProviderClassification::find(Request::input('category_id'));
        $get_code_provider = Provider::find(Request::input('provider_id'));
        $get_code_parser = Request::input('parser_type');

        $name = $file->getClientOriginalName();
        $file_name = pathinfo($name, PATHINFO_FILENAME);
        $extension = pathinfo($name, PATHINFO_EXTENSION);

        $upload->display_name = strtoupper($file_name . '-' . substr($get_code_feed_type->name, 0, 1) . substr($get_code_provider->code, 0, 1) .
                                substr($get_code_parser, 0, 1) . sprintf("%04d", $upload->id)) . '.' . strtolower($extension);

        $file->move(storage_path('excel/exports/upload') . '/', $upload->display_name);
        $upload->file_name = $upload->display_name;
        $upload->orig_name = $upload->id . '.' . $file->getClientOriginalExtension();
        $upload->save();

        if (Request::input('parser_type') == "Preset") {

        // || Request::input('category_id') == 6

        if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 1 || Request::input('category_id') == 2 || Request::input('category_id') == 3 || Request::input('category_id') == 7 || Request::input('category_id') == 8 || Request::input('category_id') == 9)) {
          Config::set('excel.csv.delimiter', '|');
        }

        set_time_limit(900);

        Excel::load(storage_path('excel/exports/upload') . '/' . $upload->file_name, function($reader) use ($upload, $input) {
          $results = $reader->get();
          // dd($results->toArray());
          if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 1 || Request::input('category_id') == 2)) {

            foreach ($results as $key => $sheet) {

              $if_save_now = 1;
              $strlen_policy = 0;

              $rows = new DataFeed;
              $rows->data_id = $upload->id;

              $rows->provider_id = Request::input('provider_id');
              $rows->category_id = Request::input('category_id');

              $rows->agent_code = trim(preg_replace('/\s+/',' ', $sheet->agent_no));
              if ($sheet->policy_number) {
                $rows->policy_number = trim(preg_replace('/\s+/',' ', $sheet->policy_number));
                $strlen_policy = strlen(preg_replace('/\s+/',' ', $sheet->policy_number));
              } else {
                $rows->policy_number = trim(preg_replace('/\s+/',' ', $sheet->policy_num));
                $strlen_policy = strlen(preg_replace('/\s+/',' ', $sheet->policy_num));
              }

              $rows->policy_holder = trim(preg_replace('/\s+/',' ', $sheet->policy_holder));
              $rows->policy_type = trim(preg_replace('/\s+/',' ', $sheet->policy_type));

              if ($sheet->component_code) {
                $rows->component_code = trim(preg_replace('/\s+/',' ', $sheet->component_code));
              } else {
                $rows->component_code = trim(preg_replace('/\s+/',' ', $sheet->component));
              }

              if ($sheet->policy_term) {
                $rows->premium_term = trim(preg_replace('/\s+/',' ', $sheet->policy_term));
              } else {
                $rows->premium_term = trim(preg_replace('/\s+/',' ', $sheet->pol_term));
              }

              if ($sheet->contract_currency) {
                $rows->contract_currency = trim(preg_replace('/\s+/',' ', $sheet->contract_currency));
              } else {
                $rows->contract_currency = trim(preg_replace('/\s+/',' ', $sheet->cont_curr));
              }

              $rows->sum_insured = trim(preg_replace('/\s+/',' ', $sheet->sum_insured));

              if ($sheet->incept_date) {
                $date = $sheet->incept_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;
              } else {
                $date = $sheet->inception_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;
              }

              if ($sheet->billing_frequency) {
                $rows->billing_frequency = trim(preg_replace('/\s+/',' ', $sheet->billing_frequency));
              } else {
                $rows->billing_frequency = trim(preg_replace('/\s+/',' ', $sheet->bil_freq));
              }

              if ($sheet->nett_premium_paid) {
                $rows->net_premium_paid = trim(preg_replace('/\s+/',' ', $sheet->nett_premium_paid));
              } else {
                $rows->net_premium_paid = trim(preg_replace('/\s+/',' ', $sheet->premium_paid));
              }

              $rows->policy_nric = trim(preg_replace('/\s+/',' ', $sheet->policyholder_nric_number));
              $rows->installment_from_date = trim(preg_replace('/\s+/',' ', $sheet->installment_from_date));
              $rows->policy_expiry_date = trim(preg_replace('/\s+/',' ', $sheet->policy_expiry_date));
              $rows->gross_premium_paid = trim(preg_replace('/\s+/',' ', $sheet->gross_premium_paid));
              $rows->gross_premium_include_gst_amt = trim(preg_replace('/\s+/',' ', $sheet->gross_premium_include_gst_amt));
              $rows->nett_premium_include_gst_amt = trim(preg_replace('/\s+/',' ', $sheet->nett_premium_include_gst_amt));
              $rows->premium_without_commission = trim(preg_replace('/\s+/',' ', $sheet->premium_without_commission));
              $rows->premium_conversion_rate = trim(preg_replace('/\s+/',' ', $sheet->premium_comversion_rate));
              $rows->payment_currency = trim(preg_replace('/\s+/',' ', $sheet->payment_currency));
              $rows->commissionor = trim(preg_replace('/\s+/',' ', $sheet->commissionor));
              $rows->commission_adjustment_amt = trim(preg_replace('/\s+/',' ', $sheet->commission_adjustment_amt));
              $rows->commission_run_date = trim(preg_replace('/\s+/',' ', $sheet->commission_run_date));
              $rows->commission_conversion_rate = trim(preg_replace('/\s+/',' ', $sheet->commission_conversion_rate));
              // $rows->premium_term = trim(preg_replace('/\s+/',' ', $sheet->premium_term));
              $rows->issue_date = trim(preg_replace('/\s+/',' ', $sheet->date_of_issue));
              $rows->upload_date = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'upload_date') . ' 00:00 AM');

              if($sheet->reporting_to === "\x1A" || $strlen_policy < 1) {
                $if_save_now = 0;
              }

              if($if_save_now == 1) {
                $rows->save();
              }
            }

            $upload->status = 1;
          } else if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 7  || Request::input('category_id') == 8  || Request::input('category_id') == 9 || Request::input('category_id') == 3) || Request::input('category_id') == 22) {

            foreach ($results as $key => $sheet) {

              $if_save_now = 1;

              $rows = new DataFeed;
              $rows->data_id = $upload->id;

              $rows->provider_id = Request::input('provider_id');
              $rows->category_id = Request::input('category_id');

              $rows->agent_code = trim(preg_replace('/\s+/',' ', $sheet->agent_no));
              $rows->policy_number = trim(preg_replace('/\s+/',' ', $sheet->policy_num));
              $rows->premium_term = trim(preg_replace('/\s+/',' ', $sheet->pol_term));
              $rows->component_code = trim(preg_replace('/\s+/',' ', $sheet->component));

              if ($sheet->bil_freq) {
                $rows->billing_frequency = trim(preg_replace('/\s+/',' ', $sheet->bil_freq));
              }

              $rows->net_premium_paid = strval(trim(preg_replace('/\s+/',' ', $sheet->premium_paid)));
              $rows->policy_holder = trim(preg_replace('/\s+/',' ', $sheet->policy_holder));

              $rows->sum_insured = trim(preg_replace('/\s+/',' ', $sheet->sum_insured));

              if (Request::input('category_id') == 7) {

                $date = $sheet->inception_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;

                // $rows->incept_date = trim(preg_replace('/\s+/',' ', $sheet->inception_date));
                // $incept_date = explode('/',  trim(preg_replace('/\s+/',' ', $sheet->inception_date)));
                // $rows->incept_date = $incept_date[0] . '/' . $incept_date[1] . '/20' . $incept_date[2];
              } else if (Request::input('category_id') == 8) {

                $date = $sheet->inception_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;

              } else if (Request::input('category_id') == 22) {

                $date = $sheet->inception_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;

              } else if (Request::input('category_id') == 9) {

                $date = $sheet->inception_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;

              } else if (Request::input('category_id') == 3) {

                $date = '';
                if ($sheet->incept_date) {
                  $date = $sheet->incept_date;
                } else {
                  $date = $sheet->inception_date;
                }

                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;
              }

              $rows->upload_date = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'upload_date') . ' 00:00 AM');

              if($sheet->reporting_to === "\x1A" || strlen(trim(preg_replace('/\s+/',' ', $sheet->policy_num))) < 2) {
                $if_save_now = 0;
              }

              if($if_save_now == 1) {
                $rows->save();
              }
            }

            $upload->status = 1;
          } else if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 4 || Request::input('category_id') == 5 || Request::input('category_id') == 6)) {

            foreach ($results as $key => $sheet) {

              $if_save_now = 1;

              $rows = new DataFeed;
              $rows->data_id = $upload->id;

              $rows->provider_id = Request::input('provider_id');
              $rows->category_id = Request::input('category_id');

              $rows->agent_code = trim(preg_replace('/\s+/',' ', $sheet->agent_code));
              $rows->policy_number = trim(preg_replace('/\s+/',' ', $sheet->policy_number));
              $rows->premium_term = trim(preg_replace('/\s+/',' ', $sheet->policy_term));
              $rows->component_code = trim(preg_replace('/\s+/',' ', $sheet->plan_code));

              if ($sheet->payment_mode) {
                $rows->billing_frequency = trim(preg_replace('/\s+/',' ', $sheet->payment_mode));
              }
              
              $rows->net_premium_paid = trim(preg_replace('/\s+/',' ', $sheet->net_payable));
              $rows->policy_holder = trim(preg_replace('/\s+/',' ', $sheet->assured_name));

              if(Request::input('category_id') == 4 || Request::input('category_id') == 5 || Request::input('category_id') == 6) {

                $date = $sheet->policy_issue_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;

                // $rows->incept_date = trim(preg_replace('/\s+/',' ', $sheet->policy_issue_date));
              }
              
              $rows->upload_date = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'upload_date') . ' 00:00 AM');

              if($sheet->reporting_to === "\x1A" || strlen(trim(preg_replace('/\s+/',' ', $sheet->policy_number))) < 2) {
                $if_save_now = 0;
              }

              if($if_save_now == 1) {
                $rows->save();
              }
            }

            $upload->status = 1;
          } else if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 20)) {

            foreach ($results as $key => $sheet) {

              $if_save_now = 1;

              $rows = new DataFeed;
              $rows->data_id = $upload->id;

              $rows->provider_id = Request::input('provider_id');
              $rows->category_id = Request::input('category_id');

              $rows->agent_code = trim(preg_replace('/\s+/',' ', $sheet->agent_no));
              $rows->policy_number = trim(preg_replace('/\s+/',' ', $sheet->policy_num));
              $rows->premium_term = trim(preg_replace('/\s+/',' ', $sheet->pol_term));
              $rows->component_code = trim(preg_replace('/\s+/',' ', $sheet->component));

              if ($sheet->bil_freq) {
                $rows->billing_frequency = trim(preg_replace('/\s+/',' ', $sheet->bil_freq));
              }
              
              $rows->net_premium_paid = trim(preg_replace('/\s+/',' ', $sheet->premium_paid));
              $rows->policy_holder = trim(preg_replace('/\s+/',' ', $sheet->policy_holder));
              $rows->upload_date = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'upload_date') . ' 00:00 AM');

              if(Request::input('category_id') == 20) {

                $date = $sheet->inception_date;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;
              }

              if($sheet->reporting_to === "\x1A" || strlen(trim(preg_replace('/\s+/',' ', $sheet->policy_num))) < 2) {
                $if_save_now = 0;
              }

              if($if_save_now == 1) {
                $rows->save();
              }
            }

            $upload->status = 1;
          } else if (Request::input('parser_type') == "Preset" && (Request::input('category_id') == 11 || Request::input('category_id') == 12)) {

            foreach ($results as $key => $sheet) {

              $if_save_now = 1;

              $rows = new DataFeed;
              $rows->data_id = $upload->id;

              $rows->provider_id = Request::input('provider_id');
              $rows->category_id = Request::input('category_id');

              $rows->agent_code = trim(preg_replace('/\s+/',' ', $sheet->adviserno));
              $rows->policy_number = trim(preg_replace('/\s+/',' ', $sheet->reference));
              $rows->premium_term = trim(preg_replace('/\s+/',' ', $sheet->policyterm));
              $rows->component_code = trim(preg_replace('/\s+/',' ', $sheet->compcode));

              if ($sheet->billingfrequency) {
                $rows->billing_frequency = trim(preg_replace('/\s+/',' ', $sheet->billingfrequency));
              }
              
              $rows->net_premium_paid = trim(preg_replace('/\s+/',' ', $sheet->installmentpremium));
              $rows->policy_holder = trim(preg_replace('/\s+/',' ', $sheet->clientname));

              if (Request::input('category_id') == 11 || Request::input('category_id') == 12) {

                $date = $sheet->commencementdate;
                $conv = strval(substr(trim(preg_replace('/\s+/',' ', strval($date))), 0,10));
                $count_dash = substr_count($conv, '-');
                $count_backslash = substr_count($conv, '/');
                $incept_date = $conv;
                if ($count_dash == 2) {
                  $dates = explode('-', $conv);
                  $incept_date = $dates[2] . '/' . $dates[1] . '/' . $dates[0];
                } else if ($count_backslash == 2) {
                  $incept_date = $conv;
                }
                $rows->incept_date = $incept_date;
              }

              $rows->upload_date = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'upload_date') . ' 00:00 AM');

              if($sheet->adviserno === "\x1A" || strlen(trim(preg_replace('/\s+/',' ', $sheet->reference))) < 2 || strlen(trim(preg_replace('/\s+/',' ', $sheet->compcode))) < 1) {
                $if_save_now = 0;
              }

              if($if_save_now == 1) {
                $rows->save();
              }
            }

            $upload->status = 1;
          }
          // else if ($get_category->name == "Navigator") {
          //   foreach ($results as $key => $sheet) {

          //     $rows = new DataFeed;
          //     $rows->data_id = $upload->id;

          //     $rows->provider_id = Request::input('provider_id');
          //     $rows->category_id = Request::input('category_id');

          //     $rows->statement_from_date = trim(preg_replace('/\s+/',' ', $sheet->statement_from_date));
          //     $rows->statement_to_date = trim(preg_replace('/\s+/',' ', $sheet->statement_to_date));
          //     $rows->transaction_date = trim(preg_replace('/\s+/',' ', $sheet->transaction_date));
          //     $rows->agent_code = trim(preg_replace('/\s+/',' ', $sheet->agent_code));
          //     $rows->policy_number = trim(preg_replace('/\s+/',' ', $sheet->contract_number));
          //     $rows->policy_type = trim(preg_replace('/\s+/',' ', $sheet->contract_type));
          //     $rows->payment_currency = trim(preg_replace('/\s+/',' ', $sheet->payment_currency));
          //     $rows->contract_currency = trim(preg_replace('/\s+/',' ', $sheet->contract_currency));
              
          //     $rows->net_premium_paid = 0;
          //     if ($sheet->gross_transaction_amount) {
          //       $rows->net_premium_paid = trim(preg_replace('/\s+/',' ', $sheet->gross_transaction_amount));
          //     }

          //     if ($sheet->gross_fee_amount) {
          //       $rows->gross_fee_amount = trim(preg_replace('/\s+/',' ', $sheet->gross_fee_amount));
          //     }
              
          //     if ($sheet->net_fee_amount) {
          //       $rows->net_fee_amount = trim(preg_replace('/\s+/',' ', $sheet->net_fee_amount));
          //     }
              
          //     $rows->fee_amount_conversion_rate = trim(preg_replace('/\s+/',' ', $sheet->amount_comversion_rate));
          //     $rows->adjustment_amount = trim(preg_replace('/\s+/',' ', $sheet->adjustment_amount));
          //     $rows->issue_date = trim(preg_replace('/\s+/',' ', $sheet->issue_date));
          //     $rows->incept_date = trim(preg_replace('/\s+/',' ', $sheet->inception_date));

          //     if ($sheet->fund_code) {
          //       $rows->fund_code = trim(preg_replace('/\s+/',' ', $sheet->fund_code));
          //     }

          //     if ($sheet->trailer_fee_amount) {
          //       $rows->trailer_fee_amount = trim(preg_replace('/\s+/',' ', $sheet->trailer_fee_amount));
          //     }

          //     $rows->upload_date = Carbon::createFromFormat('m/d/Y h:i A', $upload->created_at->format('m/d/Y') . ' 00:00 AM');

          //     if($sheet->statement_from_date != "\x1A") {
          //       $rows->save();
          //     }
          //   }
          //   $upload->status = 1;
          // }
        
        }, 'UTF-8')->store('xls', storage_path('excel/exports/feed'));
        } else {

          $heading = Excel::selectSheetsByIndex(0)->load(storage_path('excel/exports/upload') . '/' . $upload->display_name, function($reader) {
              $reader->noHeading();
          })->first();

          $header = '';
          $temp_header = '';

          foreach ($heading->toArray() as $key => $value) {
            if ($key == 0) {
              $header = $value;
            }
          }

          $headers = explode('|', $header);

          $array_header = [];
          foreach ($headers as $key => $value) {
            if ($value != '') {
              $rem_space = trim(preg_replace('/\s+/',' ', $value));
              $temp_header .= trim(preg_replace('/\s+/',' ', str_replace('/', '', $value))) . '|';
              $array_header[$key] = str_replace(' ', '_', strtolower(str_replace('/', '', $rem_space)));
            }
          }

          $header = '';
          foreach ($array_header as $key => $value) {
            if ($value != '') {
              $header .= $value . '|';
            }
          }

          $upload->temp_heading = $temp_header;
          $upload->heading = $header;

        }

        $upload->file_name = strtoupper($file_name . '-' . substr($get_code_feed_type->name, 0, 1) . substr($get_code_provider->code, 0, 1) . substr($get_code_parser, 0, 1) . sprintf("%04d", $upload->id)) . '.xls';

        $upload->upload_date = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'upload_date') . ' 00:00 AM');
        $upload->save();

        return Response::json(['body' => 'Data feed successfully uploaded']);

    }

    /**
     * Show map
     *
     * @return Response
     */
    public function showMap() {

      $id = Request::input('id');

      $upload = UploadFeed::find($id);

      $temp_id = 1;
      $temp_heading = explode('|', $upload->temp_heading);
      $heading = explode('|', $upload->heading);

      foreach ($heading as $key => $value) {
        if ($value != '') {
          $header[$temp_id] = array_add(['temp_heading' => $temp_heading[$key], 'heading' => $value], 'id', $temp_id);
          $temp_id += 1;
        }
      }

      return Response::json($header);
    }

    /**
     * Parse
     *
     * @return Response
     */
    public function parse() {

      $get_file = UploadFeed::find(Request::input('parse_id'));
      $parse = Request::input('parse');
      $parse_head = Request::input('parse_head');

      if (!array_filter($parse)) {
              return Response::json(['error' => ['System Map field is required.']]);
      }

      if ($get_file->category_id == 1 || $get_file->category_id == 2 || $get_file->category_id == 3 || $get_file->category_id == 7) {
        Config::set('excel.csv.delimiter', '|');
      }

      set_time_limit(900);

      Excel::load(storage_path('excel/exports/upload') . '/' . $get_file->display_name, function($reader) use ($get_file, $parse_head, $parse) {
        $results = $reader->all();
        // dd($results);
        foreach ($results as $key => $sheet) {
          $if_save = 1;
          $rows = new DataFeed;
          // dd($rows);
          foreach ($parse_head as $pkey => $pvalue) {
            if ($parse[$pkey] != '') {
              $rows[$parse[$pkey]] = trim(preg_replace('/\s+/',' ', $sheet[$parse_head[$pkey]]));
            }

            if ($sheet[$parse_head[$pkey]] === "\x1A") {
              // dd($parse_head[$pkey]);
              $if_save = 0;
            }
          }

          $rows->data_id = $get_file->id;
          $rows->provider_id = $get_file->provider_id;
          $rows->category_id = $get_file->category_id;
          $rows->upload_date = Carbon::createFromFormat('m/d/Y h:i A', $get_file->created_at->format('m/d/Y') . ' 00:00 AM');
          // dd($if_save);
          if ($if_save == 1) {
            $rows->save();
          }
        }
      });

      $get_file->status = 1;
      $get_file->save();

      return Response::json(['body' => 'Mapped Feed successfully allocated.']);
    }

    public function allocateCheck() {
      $get_data_feeds = DataFeed::where('data_id', Request::input('id'))->count();

      $this->data['loop'] = ceil($get_data_feeds / 100);
      $this->data['id'] = Request::input('id');
      $this->data['url'] = url('payroll/allocate');
      $this->data['url_total'] = url('payroll/allocate-total');

      Policy::where('upload_id', Request::input('id'))->delete();

      return Response::json($this->data);

    }

    public function allocate() {

      $get_upload = UploadFeed::find(Request::input('id'));
      $get_data = '';
      $check_data = Policy::where('upload_id', Request::input('id'))->lists('data_id');

      if (count($check_data) > 0) {
        $get_data_merge = DataFeed::where('data_id', Request::input('id'))->whereNotIn('id', $check_data)->lists('id');
        $get_data = DataFeed::where('data_id', Request::input('id'))->whereIn('id', $get_data_merge)->get()->take(100);
      } else {
        $get_data = DataFeed::where('data_id', '=', Request::input('id'))->get()->take(100);
      }

      $policy_no = [];
      $check_policy = [];
      $ctr = 0;
      $pctr = 0;

      set_time_limit(900);

      foreach ($get_data as $key => $sheet) {

        $rows = new Policy;

        $rows->provider_id = $sheet->provider_id;
        $rows->data_id = $sheet->id;

        if (strlen($sheet->category_id) > 0) {
          $rows->category_id = $sheet->category_id;
        } else {
          $get_upload->status = 9;
          $get_upload->save();
          return Response::json(['error' => ['Invalid Document.1']]);
        }

        if (strlen($sheet->statement_from_date) > 0) {
          if (is_numeric($sheet->statement_from_date)) {
            $date_year = substr($sheet->statement_from_date, 0, 4);
            $date_month = substr($sheet->statement_from_date, 4, 2);
            $date_day = substr($sheet->statement_from_date, 6, 2);

            if (checkdate($date_month, $date_day, $date_year)) {
              $rows->state_from_date = Carbon::createFromFormat('m/d/Y h:i A', $date_month . '/' . $date_day . '/' . $date_year . ' 00:00 AM');
            } else {
              // $get_upload->status = 9;
              // $get_upload->save();
              // return Response::json(['error' => ['Invalid Document.2']]);
            }
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.3']]);
          }
        }

        if (strlen($sheet->statement_to_date) > 0) {
          if (is_numeric($sheet->statement_to_date)) {
            $date_year = substr($sheet->statement_to_date, 0, 4);
            $date_month = substr($sheet->statement_to_date, 4, 2);
            $date_day = substr($sheet->statement_to_date, 6, 2);

            if (checkdate($date_month, $date_day, $date_year)) {
              $rows->state_to_date = Carbon::createFromFormat('m/d/Y h:i A', $date_month . '/' . $date_day . '/' . $date_year . ' 00:00 AM');
            } else {
              // $get_upload->status = 9;
              // $get_upload->save();
              // return Response::json(['error' => ['Invalid Document.4']]);
            }
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.5']]);
          }
        }

        if (strlen($sheet->transaction_date) > 0) {
          if (is_numeric($sheet->transaction_date)) {
            $date_year = substr($sheet->transaction_date, 0, 4);
            $date_month = substr($sheet->transaction_date, 4, 2);
            $date_day = substr($sheet->transaction_date, 6, 2);

            if (checkdate($date_month, $date_day, $date_year)) {
              $rows->transaction_date = Carbon::createFromFormat('m/d/Y h:i A', $date_month . '/' . $date_day . '/' . $date_year . ' 00:00 AM');
            } else {
              // $get_upload->status = 9;
              // $get_upload->save();
              // return Response::json(['error' => ['Invalid Document.6']]);
            }
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.7']]);
          }
        }

        if (strlen($sheet->policy_number) > 0) {
          $rows->contract_no = $sheet->policy_number;
        } else {
          $get_upload->status = 9;
          $get_upload->save();
          return Response::json(['error' => ['Invalid Document.8']]);
        }

        if ($sheet->policy_nric) {
          $rows->policy_nric = $sheet->policy_nric;
        }

        if (strlen($sheet->agent_code) > 0) {
          $rows->agent_code = $sheet->agent_code;
          if ($get_upload->tracking_type == "Agent") {
            $get_user_id = User::where('code', '=', $sheet->agent_code)->first();
          }
          if ($get_upload->tracking_type == "Policy") {
            $get_user_id = ProviderCodes::where(function($query) use ($sheet, $get_upload) {
                                                $query->where('provider_code', '=', $sheet->agent_code)
                                                      ->where('classification_id', '=', $get_upload->category_id)
                                                      ->where('provider_id', '=', $get_upload->provider_id);
                                          })->first();
          }
          if ($get_upload->tracking_type == "AgentPolicy") {
            $get_agent_policy = ProviderCodes::where(function($query) use ($sheet, $get_upload) {
                                                $query->where('provider_code', '=', $sheet->agent_code)
                                                      ->where('classification_id', '=', $get_upload->category_id)
                                                      ->where('provider_id', '=', $get_upload->provider_id);
                                          })->first();
            if (!$get_agent_policy) {
              $get_user_id = User::where('code', '=', $sheet->agent_code)->first();
            } else {
              $get_user_id = User::where('id', '=', $get_agent_policy->user_id)->first();
            }
          }
          if ($get_user_id) {
            $rows->user_id = $get_user_id->id;
            if ($sheet->policy_number) {
              $policy_no[$ctr] = $sheet->policy_number;
              $ctr += 1;
            }
          }
        } else {
          $get_upload->status = 9;
          $get_upload->save();
          return Response::json(['error' => ['Invalid Document.9']]);
        }

        if (strlen($sheet->policy_type) > 0) {
          $rows->policy_type = $sheet->policy_type;
        }

        if (strlen($sheet->component_code) > 0) {
          $rows->compo_code = $sheet->component_code;
        } else {
          $get_upload->status = 9;
          $get_upload->save();
          return Response::json(['error' => ['Invalid Document.10']]);
        }

        if (strlen($sheet->policy_term) > 0) {
          $rows->policy_term = $sheet->policy_term;
        }

        if (strlen($sheet->policy_holder) > 0) {
          $rows->policy_holder = $sheet->policy_holder;
        }

        if (strlen($sheet->contract_currency) > 0) {
          $rows->contract_currency = $sheet->contract_currency;
        }

        if (strlen($sheet->sum_insured) > 0) {
          if (is_numeric($sheet->sum_insured)) {
            $rows->sum_insured = $sheet->sum_insured;
          } else if ($sheet->sum_insured === "-") {
            $rows->sum_insured = null;
          } else {
            $get_upload->status = 9;
            $get_upload->save();
            return Response::json(['error' => ['Invalid Document.11']]);
          }
        }

        if (strlen($sheet->incept_date) > 0) {

          $incept_date = $sheet->incept_date;
          $incept_date = explode('/', $incept_date);

          if (count($incept_date) == 3) {
            if (is_numeric($incept_date[0]) && is_numeric($incept_date[1]) && is_numeric($incept_date[2])) {
              
              $date_year = $incept_date[2];
              $date_month = $incept_date[1];
              $date_day = $incept_date[0];

              if (checkdate($date_month, $date_day, $date_year)) {
                $rows->incept_date = Carbon::createFromFormat('m/d/Y h:i A', $date_month . '/' . $date_day . '/' . $date_year . ' 00:00 AM');
              } else {
                $get_upload->status = 9;
                $get_upload->save();
                return Response::json(['error' => ['Invalid Document.13.1']]);
              }
            } else {
              $get_upload->status = 9;
              $get_upload->save();
              return Response::json(['error' => ['Invalid Document.13.2']]);
            }
          } else {
            $get_upload->status = 9;
            $get_upload->save();
            return Response::json(['error' => ['Invalid Document.13.3']]);
          }

          // if (is_numeric($sheet->incept_date)) {
          //   $date_year = substr($sheet->incept_date, 0, 4);
          //   $date_month = substr($sheet->incept_date, 4, 2);
          //   $date_day = substr($sheet->incept_date, 6, 2);

          //   if (checkdate($date_month, $date_day, $date_year)) {
          //     $rows->incept_date = Carbon::createFromFormat('m/d/Y h:i A', $date_month . '/' . $date_day . '/' . $date_year . ' 00:00 AM');
          //   } else {
          //     // $get_upload->status = 9;
          //     // $get_upload->save();
          //     // return Response::json(['error' => ['Invalid Document.12']]);
          //   }
          // } else {
          //   $get_upload->status = 9;
          //   $get_upload->save();
          //   return Response::json(['error' => ['Invalid Document.13']]);
          // }
        } else {
          $get_upload->status = 9;
          $get_upload->save();
          return Response::json(['error' => ['Invalid Document.13.4']]);
        }
        // else {
        //   $get_upload->status = 9;
        //   $get_upload->save();
        //   return Response::json(['error' => ['Invalid Document.14']]);
        // }

        if (strlen($sheet->transaction_code) > 0) {
          $rows->transaction_code = $sheet->transaction_code;
        }
        
        if (strlen($sheet->policy_term) > 0) {
          if (is_numeric($sheet->policy_term)) {
            $rows->policy_term = $sheet->policy_term;
          } else {
            $get_upload->status = 9;
            $get_upload->save();
            return Response::json(['error' => ['Invalid Document.15']]);
          }
        }

        if (strlen($sheet->installment_from_date) > 0) {
          if (is_numeric($sheet->installment_from_date)) {
            $date_year = substr($sheet->installment_from_date, 0, 4);
            $date_month = substr($sheet->installment_from_date, 4, 2);
            $date_day = substr($sheet->installment_from_date, 6, 2);

            if (checkdate($date_month, $date_day, $date_year)) {
              $rows->inst_from_date = Carbon::createFromFormat('m/d/Y h:i A', $date_month . '/' . $date_day . '/' . $date_year . ' 00:00 AM');
            } else {
              // $get_upload->status = 9;
              // $get_upload->save();
              // return Response::json(['error' => ['Invalid Document.16']]);
            }
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.17']]);
          }
        }

        if (strlen($sheet->billing_frequency) > 0) {
          $rows->billing_freq = $sheet->billing_frequency;
        }

        if (strlen($sheet->policy_expiry_date) > 0) {
          if (is_numeric($sheet->policy_expiry_date)) {
            $date_year = substr($sheet->policy_expiry_date, 0, 4);
            $date_month = substr($sheet->policy_expiry_date, 4, 2);
            $date_day = substr($sheet->policy_expiry_date, 6, 2);

            if (checkdate($date_month, $date_day, $date_year)) {
              $rows->policy_exp_date = Carbon::createFromFormat('m/d/Y h:i A', $date_month . '/' . $date_day . '/' . $date_year . ' 00:00 AM');
            } else {
              // $get_upload->status = 9;
              // $get_upload->save();
              // return Response::json(['error' => ['Invalid Document.18']]);
            }
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.19']]);
          }
        }

        if (strlen($sheet->gross_premium_paid) > 0) {
          if (is_numeric($sheet->gross_premium_paid)) {
            $rows->gross_prem_paid = $sheet->gross_premium_paid;
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.20']]);
          }
          // $rows->gross_prem_paid = $sheet->gross_premium_paid;
        }

        if (strlen($sheet->gross_fee_amount) > 0) {
          if (is_numeric($sheet->gross_fee_amount)) {
            $rows->gross_fee_amt = $sheet->gross_fee_amount;
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.21']]);
          }
        }

        if (strlen($sheet->net_premium_paid)  > 0) {
          if (is_numeric($sheet->net_premium_paid)) {
            $rows->net_prem_paid = $sheet->net_premium_paid;
          } else {
            $get_upload->status = 9;
            $get_upload->save();
            return Response::json(['error' => ['Invalid Document.22']]);
          }
        } else {
            $get_upload->status = 9;
            $get_upload->save();
            return Response::json(['error' => ['Invalid Document.23']]);
        }

        if (strlen($sheet->net_fee_amount) > 0) {
          if (is_numeric($sheet->net_fee_amount)) {
            $rows->net_fee_amount = $sheet->net_fee_amount;
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document.24']]);
          }
        }

        if (strlen($sheet->gross_premium_include_gst_amt) > 0) {
          if (is_numeric($sheet->gross_premium_include_gst_amt)) {
            $rows->gross_prem_gst_amt = $sheet->gross_premium_include_gst_amt;
          } else {
            // $get_upload->status = 9;
            // $get_upload->save();
            // return Response::json(['error' => ['Invalid Document25.']]