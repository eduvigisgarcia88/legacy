<?php

namespace App\Http\Controllers;

use Request;
use Excel;
use Response;
use App\UploadFeed;
use Auth;
use Carbon;
use View;
use Validator;
use Input;
use Config;
use Paginator;
use DB;

use App\User;
use App\Permission;
use App\Policy;
use App\PolicyContract;
use App\FeedType;
use App\ProviderCategory;
use App\ProviderCodes;
use App\ProviderClassification;
use App\ProviderFeed;
use App\SalesBonding;
use App\SalesUser;
use App\BatchPolicy;
use App\PrePayroll;
use App\PrePayrollComputation;
use App\Introducer;
use App\AssignedPolicy;
use App\PayrollComputation;
use App\Batch;
use App\BatchMonth;
use App\BatchMonthUser;
use App\BatchMonthSupervisor;
use App\BatchMonthGroup;
use App\Group;
use App\Sales;
use App\SalesDesignation;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\Settings;
use App\GroupComputation;
use App\SalesSupervisorComputation;
use App\SalesAdvisorComputation;
use App\Product;
use App\ProductGross;
use App\Provider;
use App\DataFeed;
use App\Http\Requests;
use Redirect;
use Crypt;
use PDF;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;

class PayrollPDFController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    

    public function payrollBreakdownRevenue() {
      $id = Request::input('id');
      $get_batch = Batch::find(Request::input('id'));

      $get_users = PayrollComputation::where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('status', '=', 1);
                                  })->lists('user_id')->toArray();

      $get_providers = array_unique(PayrollComputation::where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('status', '=', 1);
                                  })->lists('provider_id')->toArray());

      $get_categories = PayrollComputation::where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('status', '=', 1);
                                  })->lists('category_id')->toArray();

      $get_active_categories = ProviderClassification::where('status', 1)->lists('id')->toArray();

      $categories = array_unique(array_merge($get_categories, $get_active_categories));

      $get_users = User::select('users.id', 'users.name', 'sales_user_extension.salesforce_id')
                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'users.id')
                        ->whereIn('users.id', array_unique($get_users))
                        ->orderBy('name', 'asc')->get();

      $batch_name = "download";
      if ($get_batch) {
        $batch_name = $get_batch->name;
      }

      Excel::create($batch_name, function($excel) use ($get_users, $batch_name, $get_providers, $categories) {

        $excel->sheet($batch_name, function($sheet) use ($get_users, $get_providers, $categories) {

          $sheet->cell('A1', function($cell) {
              $cell->setValue('SalesForceID');
          });

          $sheet->cell('B1', function($cell) {
              $cell->setValue('Agent Name');
          });

          $sheet->mergeCells('A1:A2');
          $sheet->mergeCells('B1:B2');

          $sheet->cells('A1:A2', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
            $cells->setFontWeight('bold');
          });

          $sheet->setSize(array(
            'A1:A2' => array(
                'width'     => 30,
                'height'     => 15
            )
          ));


          $sheet->cells('B1:B2', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
            $cells->setFontWeight('bold');
          });

          $sheet->setSize(array(
              'B1:B2' => array(
                  'width'     => 50,
                  'height'     => 15
              )
          ));

          $char_key = 0;
          $ctr = 1;
          $all_categories = [];

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            if ($ctr == 1) {
              $start_key = chr(99);
              $sheet->cell(chr(99) . '1', function($cell) use ($gpvalue) {
                $get_provider = Provider::find($gpvalue);
                if ($get_provider) {
                  $cell->setValue($get_provider->name);
                  $cell->setFontWeight('bold');
                }
              });
            } else {
              $start_key = $char;
              $sheet->cell($start_key . '1', function($cell) use ($gpvalue) {
                $get_provider = Provider::find($gpvalue);
                if ($get_provider) {
                  $cell->setValue($get_provider->name);
                  $cell->setFontWeight('bold');
                }
              });
            }

            $count_categories = ProviderClassification::where('provider_id', $gpvalue)->whereIn('id', $categories)->count();
            $get_cat = ProviderClassification::where('provider_id', $gpvalue)->whereIn('id', $categories)->orderBy('provider_id', 'asc')->lists('id');
            $all_categories = array_merge($all_categories, $get_cat->toArray());

            $char_key += $count_categories;

            if ($char_key > 25) {
              $char = chr(99) . chr(99+($char_key-26));
              if ($char_key-1 == 25) {
                $char_last = chr(122);
              }
            } else if ($char_key > 50) {
              $char = chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 50) {
                $char_last = chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 100) {
              $char = chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 100) {
                $char_last = chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 150) {
              $char = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 150) {
                $char_last = chr(99) . chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 200) {
              $char = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 200) {
                $char_last = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 250) {
              $char = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 250) {
                $char_last = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else {
              $char = chr(99+$char_key);
              $char_last = chr(99+$char_key-1);
            }

            $sheet->mergeCells($start_key . '1:' . $char_last . '1');

            $sheet->cells($start_key . '1:' . $char_last . '1', function($cells) {
              $cells->setValignment('center');
              $cells->setAlignment('center');
            });

            $ctr++;
          }

          
          $sheet->mergeCells($char . '1:' . $char . '2');

          $sheet->cell($char . '1', function($cell) {
              $cell->setValue('Total');
          });

          $sheet->cells($char . '1:' . $char . '2', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
            $cells->setFontWeight('bold');
          });

          $sheet->setSize(array(
            $char . '1:' . $char . '2' => array(
                'width'     => 15,
                'height'     => 15
            )
          ));

          $achar_key = 1;
          $actr = 1;

          foreach ($all_categories as $ackey => $acvalue) {

            if ($actr == 1) {
              $start_key = chr(99);
              $sheet->cell(chr(99) . '2', function($cell) use ($acvalue) {
                $get_class = ProviderClassification::find($acvalue);
                if ($get_class) {
                  $cell->setValue($get_class->name);
                  $cell->setAlignment('center');
                  $cell->setFontWeight('bold');
                }
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));

            } else {
              $start_key = $achar;
              $sheet->cell($start_key . '2', function($cell) use ($acvalue) {
                $get_class = ProviderClassification::find($acvalue);
                if ($get_class) {
                  $cell->setValue($get_class->name);
                  $cell->setAlignment('center');
                  $cell->setFontWeight('bold');
                }
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));
            }

            if ($achar_key > 25) {
              $achar = chr(99) . chr(99+($achar_key-26));
            } else if ($achar_key > 50) {
              $achar = chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($char_key > 100) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 150) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 200) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 250) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else {
              $achar = chr(99+$achar_key);
            }

            $achar_key++;
            $actr++;
          }

          $user_ctr = 3;

          foreach ($get_users as $gukey => $gufield) {

            $sheet->cell('A' . $user_ctr, function($cell) use ($gufield) {
                $cell->setValue($gufield->salesforce_id);
            });

            $sheet->cell('B' . $user_ctr, function($cell) use ($gufield) {
                $cell->setValue($gufield->name);
            });

            $achar_key = 1;
            $actr = 1;
            $achar = '';

            foreach ($all_categories as $ackey => $acvalue) {

              if ($actr == 1) {
                $start_key = chr(99);
                $sheet->cell(chr(99) . $user_ctr, function($cell)  use ($acvalue, $gufield) {

                  $get_total_cat = PayrollComputation::where(function($query) use ($acvalue, $gufield) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('user_id', '=', $gufield->id)
                                        ->where('category_id', '=', $acvalue)
                                        ->where('status', '=', 1);
                                  })->sum('agent_banding');

                  if ($get_total_cat) {
                    if ($get_total_cat < 0) {
                      $cell->setValue('('. number_format(abs($get_total_cat), 2) . ')');
                    } else {
                      $cell->setValue(number_format($get_total_cat, 2));
                    }
                  } else {
                    $cell->setValue('-');
                  }
                  $cell->setAlignment('center');
                });

                $sheet->setSize(array(
                  $start_key . '1' => array(
                      'width'     => 25,
                      'height'     => 15
                  )
                ));

              } else {
                $start_key = $achar;
                $sheet->cell($start_key .  $user_ctr, function($cell)  use ($acvalue, $gufield) {

                  $get_total_cat = PayrollComputation::where(function($query) use ($acvalue, $gufield) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('user_id', '=', $gufield->id)
                                        ->where('category_id', '=', $acvalue)
                                        ->where('status', '=', 1);
                                  })->sum('agent_banding');
                  
                  if ($get_total_cat) {
                    if ($get_total_cat < 0) {
                      $cell->setValue('('. number_format(abs($get_total_cat), 2) . ')');
                    } else {
                      $cell->setValue(number_format($get_total_cat, 2));
                    }
                  } else {
                    $cell->setValue('-');
                  }
                  $cell->setAlignment('center');
                });

                $sheet->setSize(array(
                  $start_key . '1' => array(
                      'width'     => 25,
                      'height'     => 15
                  )
                ));
              }

              if ($achar_key > 25) {
                $achar = chr(99) . chr(99+($achar_key-26));
              } else if ($achar_key > 50) {
                $achar = chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($char_key > 100) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($achar_key > 150) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($achar_key > 200) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($achar_key > 250) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else {
                $achar = chr(99+$achar_key);
              }

              $achar_key++;
              $actr++;
            }

            $sheet->cell($achar .  $user_ctr, function($cell) use ($acvalue, $gufield) {

              $get_total_cat_user = PayrollComputation::where(function($query) use ($acvalue, $gufield) {
                              $query->where('batch_id', '=', Request::input('id'))
                                    ->where('user_id', '=', $gufield->id)
                                    ->where('status', '=', 1);
                              })->sum('agent_banding');
              
              if ($get_total_cat_user) {
                if ($get_total_cat_user < 0) {
                  $cell->setValue('('. number_format(abs($get_total_cat_user), 2) . ')');
                } else {
                  $cell->setValue(number_format($get_total_cat_user, 2));
                }
              } else {
                $cell->setValue('-');
              }
              $cell->setAlignment('center');
            });

            $user_ctr++;
          }

          $achar_key = 1;
          $actr = 1;
          $achar = '';

          foreach ($all_categories as $ackey => $acvalue) {

            if ($actr == 1) {
              $start_key = chr(99);
              $sheet->cell(chr(99) . $user_ctr, function($cell)  use ($acvalue) {

                $get_total_cat_all = PayrollComputation::where(function($query) use ($acvalue) {
                                $query->where('batch_id', '=', Request::input('id'))
                                      ->where('category_id', '=', $acvalue)
                                      ->where('status', '=', 1);
                                })->sum('agent_banding');

                if ($get_total_cat_all) {
                  if ($get_total_cat_all < 0) {
                    $cell->setValue('('. number_format(abs($get_total_cat_all), 2) . ')');
                  } else {
                    $cell->setValue(number_format($get_total_cat_all, 2));
                  }
                } else {
                  $cell->setValue('-');
                }
                $cell->setAlignment('center');
                $cell->setFontWeight('bold');
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));

            } else {
              $start_key = $achar;
              $sheet->cell($start_key .  $user_ctr, function($cell)  use ($acvalue) {

                $get_total_cat_all = PayrollComputation::where(function($query) use ($acvalue) {
                                $query->where('batch_id', '=', Request::input('id'))
                                      ->where('category_id', '=', $acvalue)
                                      ->where('status', '=', 1);
                                })->sum('agent_banding');
                
                if ($get_total_cat_all) {
                  if ($get_total_cat_all < 0) {
                    $cell->setValue('('. number_format(abs($get_total_cat_all), 2) . ')');
                  } else {
                    $cell->setValue(number_format($get_total_cat_all, 2));
                  }
                } else {
                  $cell->setValue('-');
                }
                $cell->setAlignment('center');
                $cell->setFontWeight('bold');
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));
            }

            if ($achar_key > 25) {
              $achar = chr(99) . chr(99+($achar_key-26));
            } else if ($achar_key > 50) {
              $achar = chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($char_key > 100) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 150) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 200) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 250) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else {
              $achar = chr(99+$achar_key);
            }

            $achar_key++;
            $actr++;
          }

          $sheet->cell($achar .  $user_ctr, function($cell) {

            $get_main_total = PayrollComputation::where(function($query) {
                            $query->where('batch_id', '=', Request::input('id'))
                                  ->where('status', '=', 1);
                            })->sum('agent_banding');
            
            if ($get_main_total) {
              if ($get_main_total < 0) {
                $cell->setValue('('. number_format(abs($get_main_total), 2) . ')');
              } else {
                $cell->setValue(number_format($get_main_total, 2));
              }
            } else {
              $cell->setValue('-');
            }
            $cell->setAlignment('center');
            $cell->setFontWeight('bold');
          });



        });

      })->download('xls');
    }

    public function payrollBreakdownOverride() {
      // dd("error");
      
      $id = Request::input('id');
      $get_batch = Batch::find(Request::input('id'));

      $get_owners = GroupComputation::where('batch_id', '=', Request::input('id'))->lists('user_id');
      $get_users = SalesSupervisorComputation::where('batch_id', '=', Request::input('id'))->orderBy('group_id', 'asc')->lists('user_id');

      $get_users = array_unique(array_merge($get_users->toArray(), $get_owners->toArray()));

      // $get_users = PayrollComputation::where(function($query) {
      //                             $query->where('batch_id', '=', Request::input('id'))
      //                                   ->where('status', '=', 1);
      //                             })->lists('user_id')->toArray();

      $get_providers = array_unique(PayrollComputation::where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('status', '=', 1);
                                  })->lists('provider_id')->toArray());

      $get_categories = PayrollComputation::where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('status', '=', 1);
                                  })->lists('category_id')->toArray();

      $get_active_categories = ProviderClassification::where('status', 1)->lists('id')->toArray();

      $categories = array_unique(array_merge($get_categories, $get_active_categories));

      $get_users = User::select('users.id', 'users.name', 'sales_user_extension.salesforce_id')
                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'users.id')
                        ->whereIn('users.id', $get_users)
                        ->orderBy('name', 'asc')->get();

      $batch_name = "download-Override";
      if ($get_batch) {
        $batch_name = $get_batch->name . '-Override';
      }

      Excel::create($batch_name, function($excel) use ($get_users, $batch_name, $get_providers, $categories) {

        $excel->sheet($batch_name, function($sheet) use ($get_users, $get_providers, $categories) {

          $sheet->cell('A1', function($cell) {
              $cell->setValue('SalesForceID');
          });

          $sheet->cell('B1', function($cell) {
              $cell->setValue('Agent Name');
          });

          $sheet->mergeCells('A1:A2');
          $sheet->mergeCells('B1:B2');

          $sheet->cells('A1:A2', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
            $cells->setFontWeight('bold');
          });

          $sheet->setSize(array(
            'A1:A2' => array(
                'width'     => 30,
                'height'     => 15
            )
          ));


          $sheet->cells('B1:B2', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
            $cells->setFontWeight('bold');
          });

          $sheet->setSize(array(
              'B1:B2' => array(
                  'width'     => 50,
                  'height'     => 15
              )
          ));

          $char_key = 0;
          $ctr = 1;
          $all_categories = [];

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            if ($ctr == 1) {
              $start_key = chr(99);
              $sheet->cell(chr(99) . '1', function($cell) use ($gpvalue) {
                $get_provider = Provider::find($gpvalue);
                if ($get_provider) {
                  $cell->setValue($get_provider->name);
                  $cell->setFontWeight('bold');
                }
              });
            } else {
              $start_key = $char;
              $sheet->cell($start_key . '1', function($cell) use ($gpvalue) {
                $get_provider = Provider::find($gpvalue);
                if ($get_provider) {
                  $cell->setValue($get_provider->name);
                  $cell->setFontWeight('bold');
                }
              });
            }

            $count_categories = ProviderClassification::where('provider_id', $gpvalue)->whereIn('id', $categories)->count();
            $get_cat = ProviderClassification::where('provider_id', $gpvalue)->whereIn('id', $categories)->orderBy('provider_id', 'asc')->lists('id');
            $all_categories = array_merge($all_categories, $get_cat->toArray());

            $char_key += $count_categories;

            if ($char_key > 25) {
              $char = chr(99) . chr(99+($char_key-26));
              if ($char_key-1 == 25) {
                $char_last = chr(122);
              }
            } else if ($char_key > 50) {
              $char = chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 50) {
                $char_last = chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 100) {
              $char = chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 100) {
                $char_last = chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 150) {
              $char = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 150) {
                $char_last = chr(99) . chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 200) {
              $char = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 200) {
                $char_last = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else if ($char_key > 250) {
              $char = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-51));
              if ($char_key-1 == 250) {
                $char_last = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($char_key-26-1));
              }
            } else {
              $char = chr(99+$char_key);
              $char_last = chr(99+$char_key-1);
            }

            $sheet->mergeCells($start_key . '1:' . $char_last . '1');

            $sheet->cells($start_key . '1:' . $char_last . '1', function($cells) {
              $cells->setValignment('center');
              $cells->setAlignment('center');
            });

            $ctr++;
          }

          
          $sheet->mergeCells($char . '1:' . $char . '2');

          $sheet->cell($char . '1', function($cell) {
              $cell->setValue('Total');
          });

          $sheet->cells($char . '1:' . $char . '2', function($cells) {
            $cells->setValignment('center');
            $cells->setAlignment('center');
            $cells->setFontWeight('bold');
          });

          $sheet->setSize(array(
            $char . '1:' . $char . '2' => array(
                'width'     => 15,
                'height'     => 15
            )
          ));

          $achar_key = 1;
          $actr = 1;

          foreach ($all_categories as $ackey => $acvalue) {

            if ($actr == 1) {
              $start_key = chr(99);
              $sheet->cell(chr(99) . '2', function($cell) use ($acvalue) {
                $get_class = ProviderClassification::find($acvalue);
                if ($get_class) {
                  $cell->setValue($get_class->name);
                  $cell->setAlignment('center');
                  $cell->setFontWeight('bold');
                }
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));

            } else {
              $start_key = $achar;
              $sheet->cell($start_key . '2', function($cell) use ($acvalue) {
                $get_class = ProviderClassification::find($acvalue);
                if ($get_class) {
                  $cell->setValue($get_class->name);
                  $cell->setAlignment('center');
                  $cell->setFontWeight('bold');
                }
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));
            }

            if ($achar_key > 25) {
              $achar = chr(99) . chr(99+($achar_key-26));
            } else if ($achar_key > 50) {
              $achar = chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($char_key > 100) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 150) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 200) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 250) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else {
              $achar = chr(99+$achar_key);
            }

            $achar_key++;
            $actr++;
          }

          $user_ctr = 3;

          foreach ($get_users as $gukey => $gufield) {


            $sheet->cell('A' . $user_ctr, function($cell) use ($gufield) {
                $cell->setValue($gufield->salesforce_id);
            });

            $sheet->cell('B' . $user_ctr, function($cell) use ($gufield) {
                $cell->setValue($gufield->name);
            });

            $achar_key = 1;
            $actr = 1;
            $achar = '';
            $total_cat_all = 0;

            foreach ($all_categories as $ackey => $acvalue) {

              if ($actr == 1) {
                $start_key = chr(99);
                $sheet->cell(chr(99) . $user_ctr, function($cell)  use ($acvalue, $gufield, $total_cat_all) {

                  $check_group_owner = GroupComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->first();
                  if ($check_group_owner) {
                    $get_owner = GroupComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
                    $get_supervisors = SalesSupervisorComputation::where('group_id', $check_group_owner->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
  
                    $goids = array_merge($get_owner, $get_supervisors);
                    $get_advisors = SalesAdvisorComputation::where('group_id', $check_group_owner->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
                    $goids = array_merge($goids, $get_advisors);

                    $get_total_cat = PayrollComputation::where(function($query) use ($acvalue, $gufield) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('category_id', '=', $acvalue)
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $goids)->sum('tier3_share');
                  } else {
                    $check_supervisor = SalesSupervisorComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->first();
                    $guids[0] = $gufield->id;
                    $guctr = 1;
                    if ($check_supervisor) {
                      $get_advisors = SalesAdvisorComputation::where('supervisor_id', $check_supervisor->id)->where('batch_id', '=', Request::input('id'))->lists('user_id');

                      foreach ($get_advisors as $gakey => $gavalue) {
                        $guids[$guctr] = $gavalue;
                        $guctr++;
                      }
                    }

                    $get_total_cat = PayrollComputation::where(function($query) use ($acvalue, $gufield) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('category_id', '=', $acvalue)
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $guids)->sum('tier2_share');
                  }

                  $total_cat_all += $get_total_cat;

                  if ($get_total_cat) {
                    if ($get_total_cat < 0) {
                      $cell->setValue('('. number_format(abs($get_total_cat), 2) . ')');
                    } else {
                      $cell->setValue(number_format($get_total_cat, 2));
                    }
                  } else {
                    $cell->setValue('-');
                  }
                  $cell->setAlignment('center');
                });

                $sheet->setSize(array(
                  $start_key . '1' => array(
                      'width'     => 25,
                      'height'     => 15
                  )
                ));

              } else {
                $start_key = $achar;
                $sheet->cell($start_key .  $user_ctr, function($cell)  use ($acvalue, $gufield, $total_cat_all) {

                  $check_group_owner = GroupComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->first();
                  if ($check_group_owner) {
                    $get_owner = GroupComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
                    $get_supervisors = SalesSupervisorComputation::where('group_id', $check_group_owner->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
  
                    $goids = array_merge($get_owner, $get_supervisors);
                    $get_advisors = SalesAdvisorComputation::where('group_id', $check_group_owner->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
                    $goids = array_merge($goids, $get_advisors);

                    $get_total_cat = PayrollComputation::where(function($query) use ($acvalue, $gufield) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('category_id', '=', $acvalue)
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $goids)->sum('tier3_share');
                  } else {
                    $check_supervisor = SalesSupervisorComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->first();
                    $guids[0] = $gufield->id;
                    $guctr = 1;
                    if ($check_supervisor) {
                      $get_advisors = SalesAdvisorComputation::where('supervisor_id', $check_supervisor->id)->where('batch_id', '=', Request::input('id'))->lists('user_id');

                      foreach ($get_advisors as $gakey => $gavalue) {
                        $guids[$guctr] = $gavalue;
                        $guctr++;
                      }
                    }

                    $get_total_cat = PayrollComputation::where(function($query) use ($acvalue, $gufield) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('category_id', '=', $acvalue)
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $guids)->sum('tier2_share');
                  }
                  
                  $total_cat_all += $get_total_cat;

                  if ($get_total_cat) {
                    if ($get_total_cat < 0) {
                      $cell->setValue('('. number_format(abs($get_total_cat), 2) . ')');
                    } else {
                      $cell->setValue(number_format($get_total_cat, 2));
                    }
                  } else {
                    $cell->setValue('-');
                  }
                  $cell->setAlignment('center');
                });

                $sheet->setSize(array(
                  $start_key . '1' => array(
                      'width'     => 25,
                      'height'     => 15
                  )
                ));
              }

              if ($achar_key > 25) {
                $achar = chr(99) . chr(99+($achar_key-26));
              } else if ($achar_key > 50) {
                $achar = chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($char_key > 100) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($achar_key > 150) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($achar_key > 200) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else if ($achar_key > 250) {
                $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
              } else {
                $achar = chr(99+$achar_key);
              }

              $achar_key++;
              $actr++;
            }

            $sheet->cell($achar .  $user_ctr, function($cell) use ($acvalue, $gufield, $total_cat_all) {

              $check_group_owner = GroupComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->first();
              if ($check_group_owner) {
                $get_owner = GroupComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
                $get_supervisors = SalesSupervisorComputation::where('group_id', $check_group_owner->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();

                $goids = array_merge($get_owner, $get_supervisors);
                $get_advisors = SalesAdvisorComputation::where('group_id', $check_group_owner->id)->where('batch_id', '=', Request::input('id'))->lists('user_id')->toArray();
                $goids = array_merge($goids, $get_advisors);

                $get_total_cat_user = PayrollComputation::where(function($query) use ($gufield) {
                                $query->where('batch_id', '=', Request::input('id'))
                                      ->where('status', '=', 1);
                                })->whereIn('user_id', $goids)->sum('tier3_share');
              } else {
                $check_supervisor = SalesSupervisorComputation::where('user_id', $gufield->id)->where('batch_id', '=', Request::input('id'))->first();
                $guids[0] = $gufield->id;
                $guctr = 1;
                if ($check_supervisor) {
                  $get_advisors = SalesAdvisorComputation::where('supervisor_id', $check_supervisor->id)->where('batch_id', '=', Request::input('id'))->lists('user_id');

                  foreach ($get_advisors as $gakey => $gavalue) {
                    $guids[$guctr] = $gavalue;
                    $guctr++;
                  }
                }

                $get_total_cat_user = PayrollComputation::where(function($query) use ($gufield) {
                                $query->where('batch_id', '=', Request::input('id'))
                                      ->where('status', '=', 1);
                                })->whereIn('user_id', $guids)->sum('tier2_share');
              }


              if ($get_total_cat_user) {
                if ($get_total_cat_user < 0) {
                  $cell->setValue('('. number_format(abs($get_total_cat_user), 2) . ')');
                } else {
                  $cell->setValue(number_format($get_total_cat_user, 2));
                }
              } else {
                $cell->setValue('-');
              }
              $cell->setAlignment('center');
            });

            $user_ctr++;
          }

          $achar_key = 1;
          $actr = 1;
          $achar = '';

          foreach ($all_categories as $ackey => $acvalue) {

            if ($actr == 1) {
              $start_key = chr(99);
              $sheet->cell(chr(99) . $user_ctr, function($cell)  use ($acvalue) {
                // $get_total_cat_all = $total_cat_all;
                $get_total_cat_all = PayrollComputation::where(function($query) use ($acvalue) {
                                $query->where('batch_id', '=', Request::input('id'))
                                      ->where('category_id', '=', $acvalue)
                                      ->where('status', '=', 1);
                                })->sum('manage_share');

                if ($get_total_cat_all) {
                  if ($get_total_cat_all < 0) {
                    $cell->setValue('('. number_format(abs($get_total_cat_all), 2) . ')');
                  } else {
                    $cell->setValue(number_format($get_total_cat_all, 2));
                  }
                } else {
                  $cell->setValue('-');
                }
                $cell->setAlignment('center');
                $cell->setFontWeight('bold');
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));

            } else {
              $start_key = $achar;
              $sheet->cell($start_key .  $user_ctr, function($cell)  use ($acvalue) {

                // $get_total_cat_all = $total_cat_all;

                $get_total_cat_all = PayrollComputation::where(function($query) use ($acvalue) {
                                $query->where('batch_id', '=', Request::input('id'))
                                      ->where('category_id', '=', $acvalue)
                                      ->where('status', '=', 1);
                                })->sum('manage_share');
                
                if ($get_total_cat_all) {
                  if ($get_total_cat_all < 0) {
                    $cell->setValue('('. number_format(abs($get_total_cat_all), 2) . ')');
                  } else {
                    $cell->setValue(number_format($get_total_cat_all, 2));
                  }
                } else {
                  $cell->setValue('-');
                }
                $cell->setAlignment('center');
                $cell->setFontWeight('bold');
              });

              $sheet->setSize(array(
                $start_key . '1' => array(
                    'width'     => 25,
                    'height'     => 15
                )
              ));
            }

            if ($achar_key > 25) {
              $achar = chr(99) . chr(99+($achar_key-26));
            } else if ($achar_key > 50) {
              $achar = chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($char_key > 100) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 150) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 200) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else if ($achar_key > 250) {
              $achar = chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99) . chr(99+($achar_key-51));
            } else {
              $achar = chr(99+$achar_key);
            }

            $achar_key++;
            $actr++;
          }

          $sheet->cell($achar .  $user_ctr, function($cell) {

            $get_main_total = PayrollComputation::where(function($query) {
                            $query->where('batch_id', '=', Request::input('id'))
                                  ->where('status', '=', 1);
                            })->sum('manage_share');
            
            if ($get_main_total) {
              if ($get_main_total < 0) {
                $cell->setValue('('. number_format(abs($get_main_total), 2) . ')');
              } else {
                $cell->setValue(number_format($get_main_total, 2));
              }
            } else {
              $cell->setValue('-');
            }
            $cell->setAlignment('center');
            $cell->setFontWeight('bold');
          });



        });

      })->download('xls');
    }

    public function viewBatchExpand() {

      // return Response::json(['error' => "The requested item was not found in the database."]);

      $get_batch = Batch::find(Request::input('id'));
      $personal = Request::input('personal');
      // dd($get_batch);
      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }
      
      $id = Request::input('id');

      $get_providers = array_unique(PayrollComputation::where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('status', '=', 1);
                                  })->lists('provider_id')->toArray());
      
      $get_payroll_comp = PayrollComputation::select('payroll_computations.*', 'users.name', 'policies.inst_from_date',
                                  'policies.billing_freq', 'policies.contract_currency', 'policies.sum_insured', 'policies.agent_code')
                                  ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                  ->leftJoin('policies', 'policies.id', '=', 'payroll_computations.policy_id')
                                  ->where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('payroll_computations.status', '=', 1);
                                  })->orderBy('name', 'asc')->get();

      $rows = '';
      $rows .= '<table class="table-main">
                <tbody class="tbody-table">';

      $group = false;
      $tier = false;
      $sup_group = false;

      if (Auth::user()->usertype_id == 8) {
        $group = true;
        $if_group = Group::where("user_id", Auth::user()->id)->first();
        $sup_group = Group::where("user_id", Auth::user()->id)->first();

        if ($if_group) {
          $tier = true;
          $ids = [];
          $ids[0] = Auth::user()->id;
          $ctr = 1;

          $get_group = GroupComputation::where(function($query) { 
                                          $query->where('user_id', '=', Auth::user()->id)
                                                ->where('batch_id', '=', Request::input('id'));
                                    })->first();

          if ($get_group) {

            $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                              $query->where('group_id', '=', $get_group->id)
                                                    ->where('batch_id', '=', Request::input('id'));
                                        })->get();

            foreach ($get_sups_id as $gpikey => $gpifield) {
              
              $ids[$ctr] = $gpifield->user_id;
              $ctr += 1;

              $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                          $query->where('supervisor_id', '=', $gpifield->id)
                                                ->where('batch_id', '=', Request::input('id'));
                                    })->get();

              foreach ($get_advs_ids as $gaikey => $gaifield) {
                $ids[$ctr] = $gaifield->user_id;
                $ctr += 1;
              }
            }
          }
        } else {
          $if_group = SalesSupervisor::where("user_id", Auth::user()->id)->first();

          if ($if_group) {
            $tier = true;
          }

          $ids = [];
          $ids[0] = Auth::user()->id;
          $ctr = 1;

          $check_supervisor = SalesSupervisorComputation::where('user_id', Auth::user()->id)->where('batch_id', $id)->first();

          if ($check_supervisor) {
            $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($check_supervisor) { 
                                        $query->where('supervisor_id', '=', $check_supervisor->id)
                                              ->where('batch_id', '=', Request::input('id'));
                                  })->get();

            foreach ($get_advs_ids as $gaikey => $gaifield) {
              $ids[$ctr] = $gaifield->user_id;
              $ctr += 1;
            }
          }
        }
      }

      if ($personal) {
        $ids = [];
        $ids[0] = Auth::user()->id;
      }

      if ($group) {
        $get_payroll_comp = PayrollComputation::select('payroll_computations.*', 'users.name', 'policies.inst_from_date',
                                'policies.billing_freq', 'policies.contract_currency', 'policies.sum_insured', 'policies.agent_code', 'providers.name as provider_name', 'provider_classifications.name as policy_category', 'policies.policy_holder')
                                ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                ->leftJoin('policies', 'policies.id', '=', 'payroll_computations.policy_id')
                                ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                ->where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('payroll_computations.status', '=', 1);
                                  })
                                ->orderBy('name', 'asc')
                                ->whereIn('payroll_computations.user_id', $ids)
                                ->get();

        $total_premuim = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('premium');

        $total_agent_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('agent_banding');

        $total_mgt_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('manage_share');

        $total_tier2_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('tier2_share');
      } else {
        $get_payroll_comp = PayrollComputation::select('payroll_computations.*', 'users.name', 'policies.inst_from_date',
                                'policies.billing_freq', 'policies.contract_currency', 'policies.sum_insured', 'policies.agent_code', 'providers.name as provider_name', 'provider_classifications.name as policy_category', 'policies.policy_holder')
                                ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                ->leftJoin('policies', 'policies.id', '=', 'payroll_computations.policy_id')
                                ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                ->where(function($query) {
                                  $query->where('batch_id', '=', Request::input('id'))
                                        ->where('payroll_computations.status', '=', 1);
                                  })
                                ->orderBy('name', 'asc')
                                ->get();

        $total_premuim = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->lists('premium');
        $total_premuim = array_sum($total_premuim->toArray());

        $total_firm_revenue = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->lists('firm_revenue');
        $total_firm_revenue = array_sum($total_firm_revenue->toArray());

        $total_agent_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->lists('agent_banding');
        $total_agent_share = array_sum($total_agent_share->toArray());

        $total_mgt_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->lists('manage_share');
        $total_mgt_share = array_sum($total_mgt_share->toArray());

        $total_gross_revenue = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->sum('gross_revenue');

        $total_firm_revenue = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->sum('firm_revenue');

        $total_net_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->sum('total_banding');

        $total_tier2_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->sum('tier2_share');

        $total_tier3_share = PayrollComputation::where(function($query) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('status', '=', 1);
                                    })->sum('tier3_share');
      }

        if ($group) {

          $rows .= '<tr>
                    <td style="text-align: center;"><strong>Agent<br>Code</strong></td>
                    <td style="text-align: center;"><strong>Agent Name</strong></td>
                    <td style="text-align: center;"><strong>Policy<br>Holder</strong></td>
                    <td style="text-align: center;"><strong>Provider</strong></td>
                    <td style="text-align: center;"><strong>Policy<br>Number</strong></td>
                    <td style="text-align: center;"><strong>Policy<br>Category</strong></td>
                    <td style="text-align: center;"><strong>Product<br>Code</strong></td>
                    <td style="text-align: center;"><strong>Install<br>From Date</strong></td>
                    <td style="text-align: center;"><strong>Bill Freq</strong></td>
                    <td style="text-align: center;"><strong>Cont Curr</strong></td>
                    <td style="text-align: center;"><strong>Sum Insured</strong></td>
                    <td style="text-align: center;"><strong>Premium</strong></td>
                    <td style="text-align: center;"><strong>Basic/Rider Comm</strong></td>';

          if ($sup_group) {
            $rows .= '<td style="text-align: center;"><strong>Overrides</strong></td>';
          } else if ($tier) {
            $rows .= '<td style="text-align: center;"><strong>Tier2 Share</strong></td>';
          }

          $rows .= '</tr>';
        } else {


          if (Auth::user()->usertype_id == 1) {
            $rows .= '<tr>
                      <td style="text-align: center;"><strong>Agent<br>Code</strong></td>
                      <td style="text-align: center;"><strong>Agent Name</strong></td>
                      <td style="text-align: center;"><strong>Policy<br>Holder</strong></td>
                      <td style="text-align: center;"><strong>Provider</strong></td>
                      <td style="text-align: center;"><strong>Policy<br>Number</strong></td>
                      <td style="text-align: center;"><strong>Policy<br>Category</strong></td>
                      <td style="text-align: center;"><strong>Product<br>Code</strong></td>
                      <td style="text-align: center;"><strong>Install<br>From Date</strong></td>
                      <td style="text-align: center;"><strong>Bill Freq</strong></td>
                      <td style="text-align: center;"><strong>Cont Curr</strong></td>
                      <td style="text-align: center;"><strong>Sum Insured</strong></td>
                      <td style="text-align: center;"><strong>Premium</strong></td>
                      <td style="text-align: center;"><strong>Gross Rev %</strong></td>
                      <td style="text-align: center;"><strong>Gross Rev</strong></td>
                      <td style="text-align: center;"><strong>Firm Rev %</strong></td>
                      <td style="text-align: center;"><strong>Firm Rev<strong></td>
                      <td style="text-align: center;"><strong>Net Share</strong></td>
                      <td style="text-align: center;"><strong>Agent Banding</strong></td>
                      <td style="text-align: center;"><strong>Agent Share</strong></td>
                      <td style="text-align: center;"><strong>Mgt Share</strong></td>
                      <td style="text-align: center;"><strong>Tier2 Share</strong></td>
                      <td style="text-align: center;"><strong>Tier3 Share</strong></td>
                      </tr>';
          } else {
            $rows .= '<tr>
                      <td style="text-align: center;"><strong>Agent<br>Code</strong></td>
                      <td style="text-align: center;"><strong>Agent Name</strong></td>
                      <td style="text-align: center;"><strong>Policy<br>Holder</strong></td>
                      <td style="text-align: center;"><strong>Provider</strong></td>
                      <td style="text-align: center;"><strong>Policy<br>Number</strong></td>
                      <td style="text-align: center;"><strong>Policy<br>Category</strong></td>
                      <td style="text-align: center;"><strong>Product<br>Code</strong></td>
                      <td style="text-align: center;"><strong>Install<br>From Date</strong></td>
                      <td style="text-align: center;"><strong>Bill Freq</strong></td>
                      <td style="text-align: center;"><strong>Cont Curr</strong></td>
                      <td style="text-align: center;"><strong>Sum Insured</strong></td>
                      <td style="text-align: center;"><strong>Premium</strong></td>
                      <td style="text-align: center;"><strong>Basic/Rider Comm</strong></td>
                      <td style="text-align: center;"><strong>Overrides</strong></td>
                      </tr>';
          }
        }

        foreach ($get_payroll_comp as $gpkey => $gpfield) {
        
          $get_user = User::find($gpfield->user_id);
          $name = null;
          $code = null;
          if ($get_user) {
            $name = $get_user->name;
            $code = $get_user->code;
          }

          $get_category = ProviderClassification::find($gpfield->category_id);
          $category = null;
          if ($get_category) {
            $category = $get_category->name;
          }

          if ($group) {
            $rows .= '<tr>
                    <td style="text-align: center;">' . $gpfield->agent_code . '</td>
                    <td style="text-align: center;">' . $gpfield->name . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_holder . '</td>
                    <td style="text-align: center;">' . $gpfield->provider_name . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_no . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_category . '</td>
                    <td style="text-align: center;">' . $gpfield->comp_code . '</td>
                    <td style="text-align: center;">' . $gpfield->inst_from_date . '</td>
                    <td style="text-align: center;">' . $gpfield->billing_freq . '</td>
                    <td style="text-align: center;">' . $gpfield->contract_currency . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->sum_insured, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->premium, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->agent_banding, 2). '</td>';

            if ($sup_group) {
              $rows .= '<td style="text-align: center;">' . number_format($gpfield->manage_share, 2). '</td>';
            } else if ($tier) {
              $rows .= '<td style="text-align: center;">' . number_format($gpfield->tier2_share, 2). '</td>';
            }

            $rows .= '</tr>';

          } else {
            if (Auth::user()->usertype_id == 1) {
              $rows .= '<tr>
                    <td style="text-align: center;">' . $gpfield->agent_code . '</td>
                    <td style="text-align: center;">' . $gpfield->name . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_holder . '</td>
                    <td style="text-align: center;">' . $gpfield->provider_name . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_no . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_category . '</td>
                    <td style="text-align: center;">' . $gpfield->comp_code . '</td>
                    <td style="text-align: center;">' . $gpfield->inst_from_date . '</td>
                    <td style="text-align: center;">' . $gpfield->billing_freq . '</td>
                    <td style="text-align: center;">' . $gpfield->contract_currency . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->sum_insured, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->premium, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->gross_revenue_per, 2). '%</td>
                    <td style="text-align: center;">' . number_format($gpfield->gross_revenue, 2). '</td>
                    <td style="text-align: center;">' . number_format($gpfield->firm_revenue_per, 2) . '%</td>
                    <td style="text-align: center;">' . number_format($gpfield->firm_revenue, 2). '</td>
                    <td style="text-align: center;">' . number_format($gpfield->total_banding, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->agent_banding_per, 2). '%</td>
                    <td style="text-align: center;">' . number_format($gpfield->agent_banding, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->manage_share, 2). '</td>
                    <td style="text-align: center;">' . number_format($gpfield->tier2_share, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->tier3_share, 2). '</td>';
            } else {
              $rows .= '<tr>
                    <td style="text-align: center;">' . $gpfield->agent_code . '</td>
                    <td style="text-align: center;">' . $gpfield->name . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_holder . '</td>
                    <td style="text-align: center;">' . $gpfield->provider_name . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_no . '</td>
                    <td style="text-align: center;">' . $gpfield->policy_category . '</td>
                    <td style="text-align: center;">' . $gpfield->comp_code . '</td>
                    <td style="text-align: center;">' . $gpfield->inst_from_date . '</td>
                    <td style="text-align: center;">' . $gpfield->billing_freq . '</td>
                    <td style="text-align: center;">' . $gpfield->contract_currency . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->sum_insured, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->premium, 2) . '</td>
                    <td style="text-align: center;">' . number_format($gpfield->agent_banding, 2). '</td>
                    <td style="text-align: center;">' . number_format($gpfield->manage_share, 2). '</td>';
            }
            $rows .= '</tr>';
          }
        }

        if ($group) {
          $rows .= '<tr>
                    <td><strong>TOTAL</strong></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"><strong></strong></td>
                    <td style="text-align: center;"><strong style="display: none;">' . number_format($total_premuim, 2). '</strong></td>
                    <td style="text-align: center;"><strong>' . number_format($total_agent_share, 2). '</strong></td>';

          if ($sup_group) {
            $rows .= '<td style="text-align: center;"><strong>' . number_format($total_mgt_share, 2). '</strong></td>';
          } else if ($tier) {
            $rows .= '<td style="text-align: center;"><strong>' . number_format($total_tier2_share, 2). '</strong></td>';
          }

          $rows .= '</tr>';

        } else {

          if (Auth::user()->usertype_id == 1) {
            $rows .= '<tr>
                      <td><strong>TOTAL</strong></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"><strong></strong></td>
                      <td style="text-align: center;"><strong style="display: none;">' . number_format($total_premuim, 2). '</strong></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"><strong>' . number_format($total_gross_revenue, 2). '</strong></td>
                      <td style="text-align: center;"></strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_firm_revenue, 2). '</strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_net_share, 2). '</strong></td>
                      <td style="text-align: center;"><strong></strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_agent_share, 2). '</strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_mgt_share, 2). '</strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_tier2_share, 2). '</strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_tier3_share, 2). '</strong></td>
                      </tr>';

          } else {
            $rows .= '<tr>
                      <td><strong>TOTAL</strong></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"></td>
                      <td style="text-align: center;"><strong></strong></td>
                      <td style="text-align: center;"><strong style="display: none;">' . number_format($total_premuim, 2). '</strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_agent_share, 2). '</strong></td>
                      <td style="text-align: center;"><strong>' . number_format($total_mgt_share, 2). '</strong></td>
                      </tr>';
          }
        }
      

      $rows .= '</tbody></table>';

      $result['rows'] = $rows;

      $view = view('payroll-management.xls.view-batch-expand', $result)->render();

      // load view
      return Response::json(['report' => $view]);
      // return view('payroll-management.xls.view-batch-expand', $result);
    }


    public function xlsBatchExpand() {

      // header('Location: about:blank');

      $id = Request::input('id');
      $get_batch = Batch::find(Request::input('id'));

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      $batch_name = "download";
      if ($get_batch) {
        $batch_name = $get_batch->name;
      }

      $ids = [];
      $tier = false;
      $group = false;

      if (Auth::user()->usertype_id == 8) {
        $group = true;
        $if_group = Group::where("user_id", Auth::user()->id)->first();
        if ($if_group) {
          $tier = true;
          $ids[0] = Auth::user()->id;
          $ctr = 1;

          $get_group = GroupComputation::where(function($query) { 
                                          $query->where('user_id', '=', Auth::user()->id)
                                                ->where('batch_id', '=', Request::input('id'));
                                    })->first();

          if ($get_group) {

            $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                              $query->where('group_id', '=', $get_group->id)
                                                    ->where('batch_id', '=', Request::input('id'));
                                        })->get();

            foreach ($get_sups_id as $gpikey => $gpifield) {
              
              $ids[$ctr] = $gpifield->user_id;
              $ctr += 1;

              $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                          $query->where('supervisor_id', '=', $gpifield->id)
                                                ->where('batch_id', '=', Request::input('id'));
                                    })->get();

              foreach ($get_advs_ids as $gaikey => $gaifield) {
                $ids[$ctr] = $gaifield->user_id;
                $ctr += 1;
              }
            }
          }
        } else {
          $if_group = SalesSupervisor::where("user_id", Auth::user()->id)->first();

          if ($if_group) {
            $tier = true;
          }

          $ids = [];
          $ids[0] = Auth::user()->id;
          $ctr = 1;
          $check_supervisor = SalesSupervisorComputation::where('user_id', Auth::user()->id)->where('batch_id', $id)->first();

          if ($check_supervisor) {
            $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($check_supervisor) { 
                                        $query->where('supervisor_id', '=', $check_supervisor->id)
                                              ->where('batch_id', '=', Request::input('id'));
                                  })->get();

            foreach ($get_advs_ids as $gaikey => $gaifield) {
              $ids[$ctr] = $gaifield->user_id;
              $ctr += 1;
            }
          }
        }
      }


      Excel::create($batch_name, function($excel) use ($batch_name, $group, $ids, $tier) {

        $excel->sheet($batch_name, function($sheet) use ($group, $ids, $tier) {
          
          if ($group) {
            $get_payroll_comp = PayrollComputation::select('payroll_computations.*', 'users.name', 'policies.inst_from_date',
                                    'policies.billing_freq', 'policies.contract_currency', 'policies.sum_insured', 'policies.agent_code', 'providers.name as provider_name', 'provider_classifications.name as policy_category', 'policies.policy_holder')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('policies', 'policies.id', '=', 'payroll_computations.policy_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('payroll_computations.status', '=', 1);
                                      })
                                    ->orderBy('name', 'asc')
                                    ->whereIn('payroll_computations.user_id', $ids)
                                    ->get();

            $total_premuim = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('premium');

            $total_agent_share = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

            $total_mgt_share = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

          } else {
            $get_payroll_comp = PayrollComputation::select('payroll_computations.*', 'users.name', 'policies.inst_from_date',
                                    'policies.billing_freq', 'policies.contract_currency', 'policies.sum_insured', 'policies.agent_code', 'providers.name as provider_name', 'provider_classifications.name as policy_category', 'policies.policy_holder')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('policies', 'policies.id', '=', 'payroll_computations.policy_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('payroll_computations.status', '=', 1);
                                      })
                                    ->orderBy('name', 'asc')
                                    ->get();


            $total_premuim = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->lists('premium');
            $total_premuim = array_sum($total_premuim->toArray());

            $total_agent_share = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->lists('agent_banding');
            $total_agent_share = array_sum($total_agent_share->toArray());

            $total_mgt_share = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->lists('manage_share');
            $total_mgt_share = array_sum($total_mgt_share->toArray());

            $total_gross_revenue = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->sum('gross_revenue');

            $total_firm_revenue = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->sum('firm_revenue');

            $total_net_share = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->sum('total_banding');

            $total_tier2_share = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->sum('tier2_share');

            $total_tier3_share = PayrollComputation::where(function($query) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('status', '=', 1);
                                        })->sum('tier3_share');
          }
          
          $row_cell = 0;

          // if(Auth::user()->usertype_id == 1) {
          //     $sheet->cell('A' . $row_cell . ':V' . $row_cell, function($cells) {
          //       $cells->setBackground('#5b9bd5');

          //       $cells->setFont(array(
          //           'bold'       =>  true,
          //       ));

          //       $cells->setAlignment('center');
          //       $cells->setValignment('middle');
          //     });
          // } else {
          //   if($group && !$tier) {
          //     $sheet->cell('A' . $row_cell . ':M' . $row_cell, function($cells) {
          //       $cells->setBackground('#5b9bd5');

          //       $cells->setFont(array(
          //           'bold'       =>  true,
          //       ));

          //       $cells->setAlignment('center');
          //       $cells->setValignment('middle');
          //     });
          //   } else {
          //     $sheet->cell('A' . $row_cell . ':N' . $row_cell, function($cells) {
          //       $cells->setBackground('#5b9bd5');

          //       $cells->setFont(array(
          //           'bold'       =>  true,
          //       ));

          //       $cells->setAlignment('center');
          //       $cells->setValignment('middle');
          //     });
          //   }
          // }

          $row_cell += 1;
          if(Auth::user()->usertype_id == 1) {
              $sheet->appendRow(array(
                  'Agent Code', 'Agent Name', 'Policy Holder', 'Provider', 'Policy Number', 'Policy Category', 'Product Code', 'Install From Date', 'Bill Freq', 'Cont Curr', 'Sum Insured', 'Premium', 'Gross Rev %', 'Gross Rev', 'Firm Rev %', 'Firm Rev', 'Net Share', 'Agent Banding', 'Agent Share', 'Mgt Share', 'Tier2 Share', 'Tier3 Share',
              ));
          } else {
            if ($group && !$tier) {
              $sheet->appendRow(array(
                  'Agent Code', 'Agent Name', 'Policy Holder', 'Provider', 'Policy Number', 'Policy Category', 'Product Code', 'Install From Date', 'Bill Freq', 'Cont Curr', 'Sum Insured', 'Premium', 'Basic/Rider Comm'
              ));
            } else {
              $sheet->appendRow(array(
                  'Agent Code', 'Agent Name', 'Policy Holder', 'Provider', 'Policy Number', 'Policy Category', 'Product Code', 'Install From Date', 'Bill Freq', 'Cont Curr', 'Sum Insured', 'Premium', 'Basic/Rider Comm', 'Overrides'
              ));
            }
          }
          
          $sheet->cell('A' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('B' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('C' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('D' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('E' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('F' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('G' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('H' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('I' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('J' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

          $sheet->cell('K' . $row_cell, function($cells) {
            // Set font
            $cells->setFont(array(
                'bold'       =>  true,
            ));

            $cells->setAlignment('center');
            $cells->setValignment('middle');
          });

            $sheet->cell('L' . $row_cell, function($cells) {
              // Set font
              $cells->setFont(array(
                  'bold'       =>  true,
              ));

              $cells->setAlignment('center');
              $cells->setValignment('middle');
            });

            $sheet->cell('M' . $row_cell, function($cells) {
              // Set font
              $cells->setFont(array(
                  'bold'       =>  true,
              ));

              $cells->setAlignment('center');
              $cells->setValignment('middle');
            });

          if (!$group || $tier) {
            $sheet->cell('N' . $row_cell, function($cells) {
              // Set font
              $cells->setFont(array(
                  'bold'       =>  true,
              ));

              $cells->setAlignment('center');
              $cells->setValignment('middle');
            });
            if (Auth::user()->usertype_id == 1) {
              $sheet->cell('O' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

              $sheet->cell('P' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

              $sheet->cell('Q' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

              $sheet->cell('R' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

              $sheet->cell('S' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

              $sheet->cell('T' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

              $sheet->cell('U' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

              $sheet->cell('V' . $row_cell, function($cells) {
                // Set font
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            }
          }

          foreach ($get_payroll_comp as $gpkey => $gpfield) {
            $row_cell += 1;
            if (Auth::user()->usertype_id == 1) {
              $sheet->appendRow(array(
                    $gpfield->agent_code,
                    $gpfield->name,
                    $gpfield->policy_holder,
                    $gpfield->provider_name,
                    $gpfield->policy_no,
                    $gpfield->policy_category,
                    $gpfield->comp_code,
                    $gpfield->inst_from_date,
                    $gpfield->billing_freq,
                    $gpfield->contract_currency,
                    number_format($gpfield->sum_insured, 2),
                    ($gpfield->premium == 0 ? '-' : number_format($gpfield->premium, 2)),
                    number_format($gpfield->gross_revenue_per, 2) . '%',
                    ($gpfield->gross_revenue == 0 ? '-' : number_format($gpfield->gross_revenue, 2)),
                    number_format($gpfield->firm_revenue_per, 2) . '%',
                    ($gpfield->firm_revenue == 0 ? '-' : number_format($gpfield->firm_revenue, 2)),
                    ($gpfield->total_banding == 0 ? '-' : number_format($gpfield->total_banding, 2)),
                    number_format($gpfield->agent_banding_per, 2) . '%',
                    ($gpfield->agent_banding == 0 ? '-' : number_format($gpfield->agent_banding, 2)),
                    ($gpfield->manage_share == 0 ? '-' : number_format($gpfield->manage_share, 2)),
                    ($gpfield->tier2_share == 0 ? '-' : number_format($gpfield->tier2_share, 2)),
                    ($gpfield->tier3_share == 0 ? '-' : number_format($gpfield->tier3_share, 2)),
              ));
            } else {
              if ($group && !$tier) {
                $sheet->appendRow(array(
                      $gpfield->agent_code,
                      $gpfield->name,
                      $gpfield->policy_holder,
                      $gpfield->provider_name,
                      $gpfield->policy_no,
                      $gpfield->policy_category,
                      $gpfield->comp_code,
                      $gpfield->inst_from_date,
                      $gpfield->billing_freq,
                      $gpfield->contract_currency,
                      number_format($gpfield->sum_insured, 2),
                      ($gpfield->premium == 0 ? '-' : number_format($gpfield->premium, 2)),
                      ($gpfield->agent_banding == 0 ? '-' : number_format($gpfield->agent_banding, 2)),
                ));
              } else {
                $sheet->appendRow(array(
                      $gpfield->agent_code,
                      $gpfield->name,
                      $gpfield->policy_holder,
                      $gpfield->provider_name,
                      $gpfield->policy_no,
                      $gpfield->policy_category,
                      $gpfield->comp_code,
                      $gpfield->inst_from_date,
                      $gpfield->billing_freq,
                      $gpfield->contract_currency,
                      number_format($gpfield->sum_insured, 2),
                      ($gpfield->premium == 0 ? '-' : number_format($gpfield->premium, 2)),
                      ($gpfield->agent_banding == 0 ? '-' : number_format($gpfield->agent_banding, 2)),
                      ($gpfield->manage_share == 0 ? '-' : number_format($gpfield->manage_share, 2)),
                ));
              }
            }
            if (Auth::user()->usertype_id == 1) {
              $sheet->cell('A' . $row_cell . ':H' . $row_cell, function($cells) {
                $cells->setAlignment('left');
                $cells->setValignment('middle');
              });
              $sheet->cell('I' . $row_cell, function($cells) {
                $cells->setAlignment('right');
                $cells->setValignment('middle');
              });
              $sheet->cell('J' . $row_cell, function($cells) {
                $cells->setAlignment('left');
                $cells->setValignment('middle');
              });
              $sheet->cell('K' . $row_cell . ':V' . $row_cell, function($cells) {
                $cells->setAlignment('right');
                $cells->setValignment('middle');
              });
            } else {
              if ($group && !$tier) {
                $sheet->cell('A' . $row_cell . ':H' . $row_cell, function($cells) {
                  $cells->setAlignment('left');
                  $cells->setValignment('middle');
                });
                $sheet->cell('I' . $row_cell, function($cells) {
                  $cells->setAlignment('right');
                  $cells->setValignment('middle');
                });
                $sheet->cell('J' . $row_cell, function($cells) {
                  $cells->setAlignment('left');
                  $cells->setValignment('middle');
                });
                $sheet->cell('K' . $row_cell . ':M' . $row_cell, function($cells) {
                  $cells->setAlignment('right');
                  $cells->setValignment('middle');
                });
              } else {
                $sheet->cell('A' . $row_cell . ':H' . $row_cell, function($cells) {
                  $cells->setAlignment('left');
                  $cells->setValignment('middle');
                });
                $sheet->cell('I' . $row_cell, function($cells) {
                  $cells->setAlignment('right');
                  $cells->setValignment('middle');
                });
                $sheet->cell('J' . $row_cell, function($cells) {
                  $cells->setAlignment('left');
                  $cells->setValignment('middle');
                });
                $sheet->cell('K' . $row_cell . ':N' . $row_cell, function($cells) {
                  $cells->setAlignment('right');
                  $cells->setValignment('middle');
                });
              }
            }
          }

          $row_cell += 1;
          if (Auth::user()->usertype_id == 1) {
              $sheet->appendRow(array(
                'TOTAL',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                // ($total_premuim == 0 ? '-' : number_format($total_premuim, 2)),
                '',
                '',
                ($total_gross_revenue == 0 ? '-' : number_format($total_gross_revenue, 2)),
                '',
                ($total_firm_revenue == 0 ? '-' : number_format($total_firm_revenue, 2)),
                ($total_net_share == 0 ? '-' : number_format($total_net_share, 2)),
                '',
                ($total_agent_share == 0 ? '-' : number_format($total_agent_share, 2)),
                ($total_mgt_share == 0 ? '-' : number_format($total_mgt_share, 2)),
                ($total_tier2_share == 0 ? '-' : number_format($total_tier2_share, 2)),
                ($total_tier3_share == 0 ? '-' : number_format($total_tier3_share, 2)),
              ));
          } else {
            if ($group && !$tier) {
              $sheet->appendRow(array(
                'TOTAL',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                // ($total_premuim == 0 ? '-' : number_format($total_premuim, 2)),
                '',
                ($total_agent_share == 0 ? '-' : number_format($total_agent_share, 2)),
              ));
            } else {
              $sheet->appendRow(array(
                'TOTAL',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                // ($total_premuim == 0 ? '-' : number_format($total_premuim, 2)),
                '',
                ($total_agent_share == 0 ? '-' : number_format($total_agent_share, 2)),
                ($total_mgt_share == 0 ? '-' : number_format($total_mgt_share, 2)),
              ));
            }
          }

          if (Auth::user()->usertype_id == 1) {
              $sheet->cell('A' . $row_cell . ':V' . $row_cell, function($cells) {
                $cells->setBackground('#d3d3d3');
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('right');
                $cells->setValignment('middle');
              });
          } else {
            if ($group) {
              $sheet->cell('A' . $row_cell . ':M' . $row_cell, function($cells) {
                $cells->setBackground('#d3d3d3');
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('right');
                $cells->setValignment('middle');
              });
            } else {
              $sheet->cell('A' . $row_cell . ':N' . $row_cell, function($cells) {
                $cells->setBackground('#d3d3d3');
                $cells->setFont(array(
                    'bold'       =>  true,
                ));

                $cells->setAlignment('right');
                $cells->setValignment('middle');
              });
            }
          }

          $row_cell += 1;
          $sheet->appendRow(array(
              '', '',
          ));

        });

      })->download('xls');

    }

    /**
     * Build the list
     *
     */
    public function batchExpand() {

      $get_batch = Batch::find(Request::input('id'));

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      $start = Carbon::createFromFormat('Y-m-d', substr($get_batch->start, 0, 10));
      $end = Carbon::createFromFormat('Y-m-d', substr($get_batch->end, 0, 10));

      $result['start'] = $start->format('M Y');
      $result['end'] = $end->format('M Y');

      $samp_start = $start->copy();
      $samp_end = $end->copy();

      $try_start = $samp_start->startOfMonth();
      $try_end = $samp_end->endOfMonth();
      $try_diff = $try_start->diffInMonths($try_end);

      $diff = $start->diffInMonths($end);

      $dates = [];
      $id = 0;
      $table = '';
      $result['title'] = 'Payroll Summary';
      $result['agent_name'] = '';
      $result['agent_rank'] = '';
      $total_advisor = 0;
      $total_introducer = 0;
      $total_gross_revenue = 0;

      while ($start->lte($end)) {

        $agent_commission = 0;
        $date_period = $start->copy();
        $format_end = $end->format('Y-m-d');
        $total_comm = 0;
        $total_incentives = 0;
        $total_deductions = 0;
        $total_overrides = 0;
        $total_intro = 0;

        if ($id == $diff) {

          if (Auth::user()->usertype_id == 8) {

            $agent_commission = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();
            $tier = $check_rank->designations->tier;

            $check_group = Group::where("user_id", Auth::user()->id)->first();
            $check_supervisor = SalesSupervisor::where("user_id", Auth::user()->id)->first();
            $check_advisor = SalesAdvisor::where("user_id", Auth::user()->id)->first();

            if ($check_group) {

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;

                $get_group = GroupComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

                if ($get_group) {

                  $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                                    $query->where('group_id', '=', $get_group->id)
                                                          ->where('batch_id', '=', Request::input('id'));
                                              })->get();

                  foreach ($get_sups_id as $gpikey => $gpifield) {
                    
                    $ids[$ctr] = $gpifield->user_id;
                    $ctr += 1;

                    $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                                $query->where('supervisor_id', '=', $gpifield->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                    foreach ($get_advs_ids as $gaikey => $gaifield) {
                      $ids[$ctr] = $gaifield->user_id;
                      $ctr += 1;
                    }
                  }
                }

                $ids = array_unique($ids);

                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

            } else if ($check_supervisor) {
              $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

              if ($get_sup_id) {
                $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;
                foreach ($get_adv_ids as $advkey => $advfield) {
                  $ids[$ctr] = $advfield->user_id;
                  $ctr += 1;
                }

                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

              } else {
                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');
                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');
                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
              }

            } else if ($check_advisor) {
              $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

              $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

              $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
            }
          } else {
            $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');

            $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');

            $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('intro_com');

          }

        } else {

          if (Auth::user()->usertype_id == 8) {

            $agent_commission = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();
            $tier = $check_rank->designations->tier;

            $check_group = Group::where("user_id", Auth::user()->id)->first();
            $check_supervisor = SalesSupervisor::where("user_id", Auth::user()->id)->first();
            $check_advisor = SalesAdvisor::where("user_id", Auth::user()->id)->first();

            if ($check_group) {

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;

                $get_group = GroupComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

                if ($get_group) {

                  $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                                    $query->where('group_id', '=', $get_group->id)
                                                          ->where('batch_id', '=', Request::input('id'));
                                              })->get();

                  foreach ($get_sups_id as $gpikey => $gpifield) {
                    
                    $ids[$ctr] = $gpifield->user_id;
                    $ctr += 1;

                    $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                                $query->where('supervisor_id', '=', $gpifield->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                    foreach ($get_advs_ids as $gaikey => $gaifield) {
                      $ids[$ctr] = $gaifield->user_id;
                      $ctr += 1;
                    }
                  }
                }

                $ids = array_unique($ids);
                
                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

            } else if ($check_supervisor) {
              $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

              if ($get_sup_id) {
                $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;
                foreach ($get_adv_ids as $advkey => $advfield) {
                  $ids[$ctr] = $advfield->user_id;
                  $ctr += 1;
                }

                $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');
              } else {
                $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
              }

            } else if ($check_advisor) {
              $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

              $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

              $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
            }
          } else {
            $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
            $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');
            $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('intro_com');
          }

          $format_end = $date_period->endOfMonth()->format('Y-m-d');
        }

        if (Auth::user()->usertype_id == 8) {
          $total_incentives = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('type', '=', 'Incentives');
                                    })->sum('cost');

          $total_deductions = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('type', '=', 'Deductions');
                                    })->sum('cost');
        } else {
          $total_incentives = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('type', '=', 'Incentives');
                                    })->sum('cost');

          $total_deductions = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('type', '=', 'Deductions');
                                    })->sum('cost');
        }

        if ($id == 0 && $diff == 0) {
          if ($try_diff == 0) {
            $format_end = $end->format('Y-m-d');
          } else {
            $format_end = $date_period->startOfMonth()->endOfMonth()->format('Y-m-d');
          }
        } else if ($id == 1 && $diff == 0) {
          $format_end = $end->format('Y-m-d');
        }

        $format_date = $start->copy()->format('Y-m-d');

        if (Auth::user()->usertype_id == 8) {
          $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

          $check_advisor = SalesAdvisor::where("user_id", Auth::user()->id)->first();

          if ($check_advisor) { 
            $total_overrides = 0;
            $result['title'] = 'Advisor Payroll Summary';
          }


          $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                          ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->sum('intro_com');
        }

        $dates[$id] = array_add(['total_comm' => number_format($total_comm, 2, '.', ''),
                                  'introducer' => number_format($total_intro, 2),
                                  'advisor' => number_format(($total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2),
                                  'gross' => number_format(($total_intro + $total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2),
                                  'incentives' => number_format($total_incentives, 2, '.', ''), 
                                  'deductions' => number_format($total_deductions, 2, '.', ''), 
                                  'date' => $format_date, 'end' => $format_end], 'id', $id);

        $total_advisor += ($total_comm + ($total_incentives - $total_deductions) + $total_overrides);
        $total_introducer += $total_intro;
        $total_gross_revenue = ($total_intro + $total_comm + ($total_incentives - $total_deductions) + $total_overrides);

        $table .= '<tr>' .
                    '<td>' . date_format(date_create(substr($format_date, 0,10)),'M Y') . '</td>' .
                    '<td style="text-align: center;">' . number_format(($total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2) . '</td>' .
                    '<td style="text-align: center;">' . number_format($total_intro, 2) . '</td>' .
                    '<td style="text-align: center;">' . number_format(($total_intro + $total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2) . '</td>' .
                  '</tr>';


        $start->startOfMonth();
        $start->addMonth();

        $id += 1;

      }

      $file_name = $get_batch->name;
      $check_rank_pdf = Sales::with('designations')->with('users')->where('user_id', '=', Auth::user()->id)->first();
      if (Auth::user()->usertype_id == 8) {
        $result['agent_name'] = '<b>Agent Name: </b>' . $check_rank_pdf->users->name;
        $result['agent_rank'] = '<b>Agent Designation: </b>' . $check_rank_pdf->designations->name;
      }

      $result['rows'] = $table;
      $result['advisor'] = number_format($total_advisor, 2);
      $result['introducer'] = number_format($total_introducer, 2);
      $result['gross_revenue'] = number_format($total_gross_revenue, 2);

      $pdf = PDF::loadView('payroll-management.pdf.view-batch-expand', $result);
      return $pdf->setPaper('A4')->setOrientation('landscape')->download($file_name . '.pdf');
    }


    public function advisorExpand() {

      // $get_del = User::where('status', 2)->lists('id');

      // dd($get_del);
      // SalesSupervisorComputation::whereIn('user_id', $get_del)->delete();
      // SalesAdvisorComputation::whereIn('user_id', $get_del)->delete();
      // BatchMonthUser::whereIn('user_id', $get_del)->delete();

      $id = Request::input('id');
      $get_batch = Batch::find($id);
      $rows = '';
      $get_group = null;
      if ($get_batch) {
        $get_group = GroupComputation::where('batch_id', $get_batch->id)->get();
        if (Auth::user()->usertype_id == 8) {
          $check_tier3 = Group::where('user_id', Auth::user()->id)->first();
          if ($check_tier3) {
            $get_group = GroupComputation::where('batch_id', $get_batch->id)->where('user_id', Auth::user()->id)->get();
          } else {
            $get_supervisors = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', Auth::user()->id)->get();

            $result['batch_name'] = null;
            if ($get_batch) {
              $result['batch_name'] = $get_batch->name;
            }

            $ids = [];
            $ctr = 0;
            $main_ctr = 0;

            $name = null;
            $code = null;
            $rank = null;

            $user = User::find(Auth::user()->id);

            if ($user) {
              $name = $user->name;

              $get_designation = BatchMonthUser::with('designations')->where('user_id', Auth::user()->id)->where('batch_id', '=', $id)->first();
              if ($get_designation) {
                $rank = $get_designation->designations->designation;
              }
            }

            $rows .= '<center>
                        <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                      </center>
                      <strong><u>Team Payroll Summary</u></strong><br>
                      <table>
                      <tr>
                      <td><strong>Supervisor: ' . $name . '</strong></td>
                      <td style="text-align:right;"><strong>Period From:</strong> ' . $result['batch_name'] . '</td>
                      </tr>
                      <tr>
                      <td><strong>Designation: ' . $rank . '</strong></td>
                      <td></td>
                      </tr>
                      </table>
                      <br>
                      <table class="table-main">
                        <thead class="thead-table">
                          <tr>
                            <th>Agent Name</th>
                            <th>Designation</th>
                            <th>Comms</th>
                            <th>Overrides</th>
                            <th>Incentives</th>
                            <th>Deductions</th>
                            <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody class="tbody-table">';

            foreach ($get_supervisors as $gskey => $gsfield) {

                if ($gsfield->user_id) {
                  $gsuser = User::with('salesInfo')->with('salesInfo.designations')->find($gsfield->user_id);
                  $ids[$ctr] = $gsfield->user_id;
                  $ctr += 1;
                  $main_ids[$main_ctr] = $gsfield->user_id;
                  $main_ctr += 1;

                  $gsname = null;
                  $gscode = null;
                  $gsrank = null;
                  if ($gsuser) {
                    $gsname = $gsuser->name;

                    $get_designation = BatchMonthUser::with('designations')->where('user_id', $gsfield->user_id)->where('batch_id', '=', $id)->first();
                    if ($get_designation) {
                      $gsrank = $get_designation->designations->designation;
                    }
                  }

                  $gsget_advisors = SalesAdvisorComputation::where('batch_id', $get_batch->id)->where('supervisor_id', $gsfield->id)->lists('user_id');

                  $gsoverrides = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', $gsget_advisors)->sum('tier2_share');

                  $gscomms = PayrollComputation::where(function($query) use ($gsfield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gsfield->user_id)
                                          ->where('status', 1);
                                  })->sum('agent_banding');

                  $gsincentives = PrePayrollComputation::where(function($query) use ($gsfield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gsfield->user_id)
                                          ->where('type', '=', 'Incentives')
                                          ->where('status', 1);
                                  })->sum('cost');

                  $gsdeductions = PrePayrollComputation::where(function($query) use ($gsfield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gsfield->user_id)
                                          ->where('type', '=', 'Deductions')
                                          ->where('status', 1);
                                  })->sum('cost');

                  $gsamount = $gscomms + $gsoverrides + ($gsincentives - $gsdeductions);

                  if ($gscomms) {
                    if ($gscomms < 0) {
                      $gscomms = '(' . number_format(abs($gscomms), 2) . ')';
                    } else {
                      $gscomms = number_format($gscomms, 2);
                    }
                  } else {
                    $gscomms = '-';
                  }

                  if ($gsoverrides) {
                    if ($gsoverrides < 0) {
                      $gsoverrides = '(' . number_format(abs($gsoverrides), 2) . ')';
                    } else {
                      $gsoverrides = number_format($gsoverrides, 2);
                    }
                  } else {
                    $gsoverrides = '-';
                  }

                  if ($gsincentives) {
                    if ($gsincentives < 0) {
                      $gsincentives = '(' . number_format(abs($gsincentives), 2) . ')';
                    } else {
                      $gsincentives = number_format($gsincentives, 2);
                    }
                  } else {
                    $gsincentives = '-';
                  }

                  if ($gsdeductions) {
                    if ($gsdeductions < 0) {
                      $gsdeductions = '(' . number_format(abs($gsdeductions), 2) . ')';
                    } else {
                      $gsdeductions = number_format($gsdeductions, 2);
                    }
                  } else {
                    $gsdeductions = '-';
                  }

                  if ($gsamount) {
                    if ($gsamount < 0) {
                      $gsamount = '(' . number_format(abs($gsamount), 2) . ')';
                    } else {
                      $gsamount = number_format($gsamount, 2);
                    }
                  } else {
                    $gsamount = '-';
                  }

                }

                $rows .= '<tr>
                        <td>' . $gsname . '</td>
                        <td>' . $gsrank . '</td>
                        <td style="text-align: center;">' . $gscomms . '</td>
                        <td style="text-align: center;">' . $gsoverrides . '</td>
                        <td style="text-align: center;">' . $gsincentives . '</td>
                        <td style="text-align: center;">' . $gsdeductions . '</td>
                        <td style="text-align: center;">' . $gsamount . '</td>
                      </tr>';
                

                $get_advisors = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', $gsfield->id)->get();

                foreach ($get_advisors as $gakey => $gafield) {

                  if ($gafield->user_id) {
                    $gauser = User::with('salesInfo')->with('salesInfo.designations')->find($gafield->user_id);
                    $ids[$ctr] = $gafield->user_id;
                    $ctr += 1;
                    $main_ids[$main_ctr] = $gafield->user_id;
                    $main_ctr += 1;

                    $ganame = null;
                    $gacode = null;
                    $garank = null;
                    if ($gauser) {
                      $ganame = $gauser->name;

                      $get_designation = BatchMonthUser::with('designations')->where('user_id', $gafield->user_id)->where('batch_id', '=', $id)->first();
                      if ($get_designation) {
                        $garank = $get_designation->designations->designation;
                      }
                    }

                    $gacomms = PayrollComputation::where(function($query) use ($gafield, $id) {
                                      $query->where('batch_id', $id)
                                            ->where('user_id', $gafield->user_id)
                                            ->where('status', 1);
                                    })->sum('agent_banding');


                    $gaincentives = PrePayrollComputation::where(function($query) use ($gafield, $id) {
                                      $query->where('batch_id', $id)
                                            ->where('user_id', $gafield->user_id)
                                            ->where('type', '=', 'Incentives')
                                            ->where('status', 1);
                                    })->sum('cost');

                    $gadeductions = PrePayrollComputation::where(function($query) use ($gafield, $id) {
                                      $query->where('batch_id', $id)
                                            ->where('user_id', $gafield->user_id)
                                            ->where('type', '=', 'Deductions')
                                            ->where('status', 1);
                                    })->sum('cost');

                    $gaamount = $gacomms + ($gaincentives - $gadeductions);

                    if ($gacomms) {
                      if ($gacomms < 0) {
                        $gacomms = '(' . number_format(abs($gacomms), 2) . ')';
                      } else {
                        $gacomms = number_format($gacomms, 2);
                      }
                    } else {
                      $gacomms = '-';
                    }

                    if ($gaincentives) {
                      if ($gaincentives < 0) {
                        $gaincentives = '(' . number_format(abs($gaincentives), 2) . ')';
                      } else {
                        $gaincentives = number_format($gaincentives, 2);
                      }
                    } else {
                      $gaincentives = '-';
                    }

                    if ($gadeductions) {
                      if ($gadeductions < 0) {
                        $gadeductions = '(' . number_format(abs($gadeductions), 2) . ')';
                      } else {
                        $gadeductions = number_format($gadeductions, 2);
                      }
                    } else {
                      $gadeductions = '-';
                    }

                    if ($gaamount) {
                      if ($gaamount < 0) {
                        $gaamount = '(' . number_format(abs($gaamount), 2) . ')';
                      } else {
                        $gaamount = number_format($gaamount, 2);
                      }
                    } else {
                      $gaamount = '-';
                    }

                  }

                  $rows .= '<tr>
                          <td>' . $ganame . '</td>
                          <td>' . $garank . '</td>
                          <td style="text-align: center;">' . $gacomms . '</td>
                          <td style="text-align: center;">-</td>
                          <td style="text-align: center;">' . $gaincentives . '</td>
                          <td style="text-align: center;">' . $gadeductions . '</td>
                          <td style="text-align: center;">' . $gaamount . '</td>
                        </tr>';
                }
              
            }


            $get_total_comms = PayrollComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', $id)
                                            ->where('status', 1);
                                    })->whereIn('user_id', array_unique($ids))->sum('agent_banding');

            $get_total_tier2 = PayrollComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', $id)
                                            ->where('status', 1);
                                    })->whereIn('user_id', array_unique($ids))->sum('tier2_share');

            $get_total_tier3 = PayrollComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', $id)
                                            ->where('status', 1);
                                    })->whereIn('user_id', array_unique($ids))->sum('tier3_share');

            $get_total_overrides = $get_total_tier2 + $get_total_tier3;

            $get_total_incentives = PrePayrollComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', $id)
                                            ->where('type', '=', 'Incentives')
                                            ->where('status', 1);
                                    })->whereIn('user_id', array_unique($ids))->sum('cost');

            $get_total_deductions = PrePayrollComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', $id)
                                            ->where('type', '=', 'Deductions')
                                            ->where('status', 1);
                                    })->whereIn('user_id', array_unique($ids))->sum('cost');

            $get_total_amount = $get_total_comms + $get_total_overrides + ($get_total_incentives - $get_total_deductions);

            if ($get_total_comms) {
              if ($get_total_comms < 0) {
                $get_total_comms = '(' . number_format(abs($get_total_comms), 2) . ')';
              } else {
                $get_total_comms = number_format($get_total_comms, 2);
              }
            } else {
              $get_total_comms = '-';
            }

            if ($get_total_incentives) {
              if ($get_total_incentives < 0) {
                $get_total_incentives = '(' . number_format(abs($get_total_incentives), 2) . ')';
              } else {
                $get_total_incentives = number_format($get_total_incentives, 2);
              }
            } else {
              $get_total_incentives = '-';
            }

            if ($get_total_overrides) {
              if ($get_total_overrides < 0) {
                $get_total_overrides = '(' . number_format(abs($get_total_overrides), 2) . ')';
              } else {
                $get_total_overrides = number_format($get_total_overrides, 2);
              }
            } else {
              $get_total_overrides = '-';
            }

            if ($get_total_deductions) {
              if ($get_total_deductions < 0) {
                $get_total_deductions = '(' . number_format(abs($get_total_deductions), 2) . ')';
              } else {
                $get_total_deductions = number_format($get_total_deductions, 2);
              }
            } else {
              $get_total_deductions = '-';
            }

            if ($get_total_amount) {
              if ($get_total_amount < 0) {
                $get_total_amount = '(' . number_format(abs($get_total_amount), 2) . ')';
              } else {
                $get_total_amount = number_format($get_total_amount, 2);
              }
            } else {
              $get_total_amount = '-';
            }

            $rows .= '<tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="color: #ffffff;">-</td>
                  </tr>';

            $rows .= '<tr>
                    <td><strong>TOTAL</strong></td>
                    <td></td>
                    <td style="text-align: center;">' . $get_total_comms . '</td>
                    <td style="text-align: center;">' . $get_total_overrides . '</td>
                    <td style="text-align: center;">' . $get_total_incentives . '</td>
                    <td style="text-align: center;">' . $get_total_deductions . '</td>
                    <td style="text-align: center;">' . $get_total_amount . '</td>
                  </tr>';

            $rows .= '</tbody></table><br>';

            $result['rows'] = $rows;
            // $result['total'] = number_format($commissions + $overrides + ($incentives - $deductions), 2);
            $view = view('payroll-management.pdf.view-firm-revenue', $result)->render();

            return Response::json(['report' => $view]);

            // return view('payroll-management.pdf.view-firm-revenue', $result);

            // $pdf = PDF::loadView('payroll-management.pdf.view-advisor-expand', $result);
          // return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');

          }
        }
      }

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      if ($get_group) {
        $rows = '';
        $main_ids = [];
        $main_ctr = 0;

        foreach ($get_group as $key => $field) {

          $ids = [];
          $ctr = 0;
          if ($field->user_id) {
            $user = User::find($field->user_id);

            $ids[$ctr] = $field->user_id;
            $ctr += 1;
            $main_ids[$main_ctr] = $field->user_id;
            $main_ctr += 1;

            $name = null;
            $rank = null;

            $get_supervisors = SalesSupervisorComputation::where('batch_id', $get_batch->id)->where('group_id', $field->id)->lists('user_id');
            $get_advisors = SalesAdvisorComputation::where('batch_id', $get_batch->id)->where('group_id', $field->id)->lists('user_id');
            $users = array_merge($get_supervisors->toArray(), $get_advisors->toArray());

            $overrides = PayrollComputation::where(function($query) use ($field, $id) {
                              $query->where('batch_id', $id)
                                    ->where('status', 1);
                            })->whereIn('user_id', $users)->sum('tier3_share');

            if ($user) {
              $name = $user->name;

              $get_designation = BatchMonthUser::with('designations')->where('user_id', $field->user_id)->where('batch_id', '=', $id)->first();
              if ($get_designation) {
                $rank = $get_designation->designations->designation;
              }
            }


            $comms = PayrollComputation::where(function($query) use ($field, $id) {
                              $query->where('batch_id', $id)
                                    ->where('user_id', $field->user_id)
                                    ->where('status', 1);
                            })->sum('agent_banding');

            $incentives = PrePayrollComputation::where(function($query) use ($field, $id) {
                              $query->where('batch_id', $id)
                                    ->where('user_id', $field->user_id)
                                    ->where('type', '=', 'Incentives')
                                    ->where('status', 1);
                            })->sum('cost');

            $deductions = PrePayrollComputation::where(function($query) use ($field, $id) {
                              $query->where('batch_id', $id)
                                    ->where('user_id', $field->user_id)
                                    ->where('type', '=', 'Deductions')
                                    ->where('status', 1);
                            })->sum('cost');

            $amount = $comms + $overrides + ($incentives - $deductions);

            if ($comms) {
              if ($comms < 0) {
                $comms = '(' . number_format(abs($comms), 2) . ')';
              } else {
                $comms = number_format($comms, 2);
              }
            } else {
              $comms = '-';
            }

            if ($overrides) {
              if ($overrides < 0) {
                $overrides = '(' . number_format(abs($overrides), 2) . ')';
              } else {
                $overrides = number_format($overrides, 2);
              }
            } else {
              $overrides = '-';
            }

            if ($incentives) {
              if ($incentives < 0) {
                $incentives = '(' . number_format(abs($incentives), 2) . ')';
              } else {
                $incentives = number_format($incentives, 2);
              }
            } else {
              $incentives = '-';
            }

            if ($deductions) {
              if ($deductions < 0) {
                $deductions = '(' . number_format(abs($deductions), 2) . ')';
              } else {
                $deductions = number_format($deductions, 2);
              }
            } else {
              $deductions = '-';
            }

            if ($amount) {
              if ($amount < 0) {
                $amount = '(' . number_format(abs($amount), 2) . ')';
              } else {
                $amount = number_format($amount, 2);
              }
            } else {
              $amount = '-';
            }
            
          }
          
          if ($key != 0) {
            $rows .= '<div class="page-break">
                      </div>';
          }

          $rows .= '<center>
                      <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                    </center>
                    <strong><u>Team Payroll Summary</u></strong><br>
                    <table>
                    <tr>
                    <td><strong>Supervisor: ' . $name . '</strong></td>
                    <td style="text-align:right;"><strong>Period From:</strong> ' . $result['batch_name'] . '</td>
                    </tr>
                    <tr>
                    <td><strong>Designation: ' . $rank . '</strong></td>
                    <td></td>
                    </tr>
                    </table>
                    <br>
                    <table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th>Agent Name</th>
                          <th>Designation</th>
                          <th>Comms</th>
                          <th>Overrides</th>
                          <th>Incentives</th>
                          <th>Deductions</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          $rows .= '<tr>
                      <td>' . $name . '</td>
                      <td>' . $rank . '</td>
                      <td style="text-align: center;">' . $comms . '</td>
                      <td style="text-align: center;">' . $overrides . '</td>
                      <td style="text-align: center;">' . $incentives . '</td>
                      <td style="text-align: center;">' . $deductions . '</td>
                      <td style="text-align: center;">' . $amount . '</td>
                    </tr>';

          $get_supervisors = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $field->id)->get();

          foreach ($get_supervisors as $gskey => $gsfield) {

              if ($gsfield->user_id) {
                $gsuser = User::with('salesInfo')->with('salesInfo.designations')->find($gsfield->user_id);
                $ids[$ctr] = $gsfield->user_id;
                $ctr += 1;
                $main_ids[$main_ctr] = $gsfield->user_id;
                $main_ctr += 1;

                $gsname = null;
                $gscode = null;
                $gsrank = null;
                if ($gsuser) {
                  $gsname = $gsuser->name;

                  $get_designation = BatchMonthUser::with('designations')->where('user_id', $gsfield->user_id)->where('batch_id', '=', $id)->first();
                  if ($get_designation) {
                    $gsrank = $get_designation->designations->designation;
                  }
                }

                $gsget_advisors = SalesAdvisorComputation::where('batch_id', $get_batch->id)->where('supervisor_id', $gsfield->id)->lists('user_id');

                $gsoverrides = PayrollComputation::where(function($query) use ($field, $id) {
                                  $query->where('batch_id', $id)
                                        ->where('status', 1);
                                })->whereIn('user_id', $gsget_advisors)->sum('tier2_share');

                $gscomms = PayrollComputation::where(function($query) use ($gsfield, $id) {
                                  $query->where('batch_id', $id)
                                        ->where('user_id', $gsfield->user_id)
                                        ->where('status', 1);
                                })->sum('agent_banding');

                $gsincentives = PrePayrollComputation::where(function($query) use ($gsfield, $id) {
                                  $query->where('batch_id', $id)
                                        ->where('user_id', $gsfield->user_id)
                                        ->where('type', '=', 'Incentives')
                                        ->where('status', 1);
                                })->sum('cost');

                $gsdeductions = PrePayrollComputation::where(function($query) use ($gsfield, $id) {
                                  $query->where('batch_id', $id)
                                        ->where('user_id', $gsfield->user_id)
                                        ->where('type', '=', 'Deductions')
                                        ->where('status', 1);
                                })->sum('cost');

                $gsamount = $gscomms + $gsoverrides + ($gsincentives - $gsdeductions);

                if ($gscomms) {
                  if ($gscomms < 0) {
                    $gscomms = '(' . number_format(abs($gscomms), 2) . ')';
                  } else {
                    $gscomms = number_format($gscomms, 2);
                  }
                } else {
                  $gscomms = '-';
                }

                if ($gsoverrides) {
                  if ($gsoverrides < 0) {
                    $gsoverrides = '(' . number_format(abs($gsoverrides), 2) . ')';
                  } else {
                    $gsoverrides = number_format($gsoverrides, 2);
                  }
                } else {
                  $gsoverrides = '-';
                }

                if ($gsincentives) {
                  if ($gsincentives < 0) {
                    $gsincentives = '(' . number_format(abs($gsincentives), 2) . ')';
                  } else {
                    $gsincentives = number_format($gsincentives, 2);
                  }
                } else {
                  $gsincentives = '-';
                }

                if ($gsdeductions) {
                  if ($gsdeductions < 0) {
                    $gsdeductions = '(' . number_format(abs($gsdeductions), 2) . ')';
                  } else {
                    $gsdeductions = number_format($gsdeductions, 2);
                  }
                } else {
                  $gsdeductions = '-';
                }

                if ($gsamount) {
                  if ($gsamount < 0) {
                    $gsamount = '(' . number_format(abs($gsamount), 2) . ')';
                  } else {
                    $gsamount = number_format($gsamount, 2);
                  }
                } else {
                  $gsamount = '-';
                }

              }

              if ($gsfield->user_id != $field->user_id) {
                $rows .= '<tr>
                        <td>' . $gsname . '</td>
                        <td>' . $gsrank . '</td>
                        <td style="text-align: center;">' . $gscomms . '</td>
                        <td style="text-align: center;">' . $gsoverrides . '</td>
                        <td style="text-align: center;">' . $gsincentives . '</td>
                        <td style="text-align: center;">' . $gsdeductions . '</td>
                        <td style="text-align: center;">' . $gsamount . '</td>
                      </tr>';
              }

              $get_advisors = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', $gsfield->id)->get();

              foreach ($get_advisors as $gakey => $gafield) {

                if ($gafield->user_id) {
                  $gauser = User::with('salesInfo')->with('salesInfo.designations')->find($gafield->user_id);
                  $ids[$ctr] = $gafield->user_id;
                  $ctr += 1;
                  $main_ids[$main_ctr] = $gafield->user_id;
                  $main_ctr += 1;

                  $ganame = null;
                  $gacode = null;
                  $garank = null;
                  if ($gauser) {
                    $ganame = $gauser->name;

                    $get_designation = BatchMonthUser::with('designations')->where('user_id', $gafield->user_id)->where('batch_id', '=', $id)->first();
                    if ($get_designation) {
                      $garank = $get_designation->designations->designation;
                    }
                  }

                  $gacomms = PayrollComputation::where(function($query) use ($gafield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gafield->user_id)
                                          ->where('status', 1);
                                  })->sum('agent_banding');


                  $gaincentives = PrePayrollComputation::where(function($query) use ($gafield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gafield->user_id)
                                          ->where('type', '=', 'Incentives')
                                          ->where('status', 1);
                                  })->sum('cost');

                  $gadeductions = PrePayrollComputation::where(function($query) use ($gafield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gafield->user_id)
                                          ->where('type', '=', 'Deductions')
                                          ->where('status', 1);
                                  })->sum('cost');

                  $gaamount = $gacomms + ($gaincentives - $gadeductions);

                  if ($gacomms) {
                    if ($gacomms < 0) {
                      $gacomms = '(' . number_format(abs($gacomms), 2) . ')';
                    } else {
                      $gacomms = number_format($gacomms, 2);
                    }
                  } else {
                    $gacomms = '-';
                  }

                  if ($gaincentives) {
                    if ($gaincentives < 0) {
                      $gaincentives = '(' . number_format(abs($gaincentives), 2) . ')';
                    } else {
                      $gaincentives = number_format($gaincentives, 2);
                    }
                  } else {
                    $gaincentives = '-';
                  }

                  if ($gadeductions) {
                    if ($gadeductions < 0) {
                      $gadeductions = '(' . number_format(abs($gadeductions), 2) . ')';
                    } else {
                      $gadeductions = number_format($gadeductions, 2);
                    }
                  } else {
                    $gadeductions = '-';
                  }

                  if ($gaamount) {
                    if ($gaamount < 0) {
                      $gaamount = '(' . number_format(abs($gaamount), 2) . ')';
                    } else {
                      $gaamount = number_format($gaamount, 2);
                    }
                  } else {
                    $gaamount = '-';
                  }

                }

                $rows .= '<tr>
                        <td>' . $ganame . '</td>
                        <td>' . $garank . '</td>
                        <td style="text-align: center;">' . $gacomms . '</td>
                        <td style="text-align: center;">-</td>
                        <td style="text-align: center;">' . $gaincentives . '</td>
                        <td style="text-align: center;">' . $gadeductions . '</td>
                        <td style="text-align: center;">' . $gaamount . '</td>
                      </tr>';
              }
            
          }

          $get_total_comms = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($ids))->sum('agent_banding');

          $get_total_tier2 = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($ids))->sum('tier2_share');

          $get_total_tier3 = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($ids))->sum('tier3_share');

          $get_total_overrides = $get_total_tier2 + $get_total_tier3;

          $get_total_incentives = PrePayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('type', '=', 'Incentives')
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($ids))->sum('cost');

          $get_total_deductions = PrePayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('type', '=', 'Deductions')
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($ids))->sum('cost');

          $get_total_amount = $get_total_comms + $get_total_overrides + ($get_total_incentives - $get_total_deductions);

          if ($get_total_comms) {
            if ($get_total_comms < 0) {
              $get_total_comms = '(' . number_format(abs($get_total_comms), 2) . ')';
            } else {
              $get_total_comms = number_format($get_total_comms, 2);
            }
          } else {
            $get_total_comms = '-';
          }

          if ($get_total_incentives) {
            if ($get_total_incentives < 0) {
              $get_total_incentives = '(' . number_format(abs($get_total_incentives), 2) . ')';
            } else {
              $get_total_incentives = number_format($get_total_incentives, 2);
            }
          } else {
            $get_total_incentives = '-';
          }

          if ($get_total_overrides) {
            if ($get_total_overrides < 0) {
              $get_total_overrides = '(' . number_format(abs($get_total_overrides), 2) . ')';
            } else {
              $get_total_overrides = number_format($get_total_overrides, 2);
            }
          } else {
            $get_total_overrides = '-';
          }

          if ($get_total_deductions) {
            if ($get_total_deductions < 0) {
              $get_total_deductions = '(' . number_format(abs($get_total_deductions), 2) . ')';
            } else {
              $get_total_deductions = number_format($get_total_deductions, 2);
            }
          } else {
            $get_total_deductions = '-';
          }

          if ($get_total_amount) {
            if ($get_total_amount < 0) {
              $get_total_amount = '(' . number_format(abs($get_total_amount), 2) . ')';
            } else {
              $get_total_amount = number_format($get_total_amount, 2);
            }
          } else {
            $get_total_amount = '-';
          }

          $rows .= '<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="color: #ffffff;">-</td>
                </tr>';

          $rows .= '<tr>
                  <td><strong>TOTAL</strong></td>
                  <td></td>
                  <td style="text-align: center;">' . $get_total_comms . '</td>
                  <td style="text-align: center;">' . $get_total_overrides . '</td>
                  <td style="text-align: center;">' . $get_total_incentives . '</td>
                  <td style="text-align: center;">' . $get_total_deductions . '</td>
                  <td style="text-align: center;">' . $get_total_amount . '</td>
                </tr>';

          $rows .= '</tbody></table><br>';

        }

        if (Auth::user()->usertype_id == 8) {
          $result['rows'] = $rows;
          // return view('payroll-management.pdf.view-firm-revenue', $result);

          $view = view('payroll-management.pdf.view-firm-revenue', $result)->render();
          
          return Response::json(['report' => $view]);

      
          // $pdf = PDF::loadView('payroll-management.pdf.view-advisor-expand', $result);
          // return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');
        }

        $get_no_group = PayrollComputation::where(function($query) use ($id) {
                                  $query->where('batch_id', $id)
                                        ->where('status', 1);
                                })->whereNotIn('user_id', array_unique($main_ids))->lists('user_id');


        if (count($get_no_group) > 0) {

          $rows .= '<center>
                      <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                    </center>
                    <strong>No Team</strong>
                    <table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th>Agent Code</th>
                          <th>Agent Name</th>
                          <th>Firm Revenue</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          foreach ($get_no_group as $gnkey => $gnfield) {
          
            if ($gnfield->user_id) {
              $gnuser = User::find($gnfield->user_id);

                  $gnname = null;
                  $gncode = null;
                  $gnrank = null;
                  if ($gnuser) {
                    $gnname = $gnuser->name;

                    $get_designation = BatchMonthUser::with('designations')->where('user_id', $gnfield->user_id)->where('batch_id', '=', $id)->first();
                    if ($get_designation) {
                      $gnrank = $get_designation->designations->designation;
                    }
                  }

                  $gncomms = PayrollComputation::where(function($query) use ($gnfield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gnfield->user_id)
                                          ->where('status', 1);
                                  })->sum('agent_banding');


                  $gnoverrides = PayrollComputation::where(function($query) use ($gnfield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gnfield->user_id)
                                          ->where('status', 1);
                                  })->sum('manage_share');

                  $gnincentives = PrePayrollComputation::where(function($query) use ($gnfield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gnfield->user_id)
                                          ->where('type', '=', 'Incentives')
                                          ->where('status', 1);
                                  })->sum('cost');

                  $gndeductions = PrePayrollComputation::where(function($query) use ($gnfield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gnfield->user_id)
                                          ->where('type', '=', 'Deductions')
                                          ->where('status', 1);
                                  })->sum('cost');

                  $gnamount = $gncomms + ($gnincentives - $gndeductions);

                  if ($gncomms) {
                    if ($gncomms < 0) {
                      $gncomms = '(' . number_format(abs($gncomms), 2) . ')';
                    } else {
                      $gncomms = number_format($gncomms, 2);
                    }
                  } else {
                    $gncomms = '-';
                  }

                  if ($gnincentives) {
                    if ($gnincentives < 0) {
                      $gnincentives = '(' . number_format(abs($gnincentives), 2) . ')';
                    } else {
                      $gnincentives = number_format($gnincentives, 2);
                    }
                  } else {
                    $gnincentives = '-';
                  }

                  if ($gnoverrides) {
                    if ($gnoverrides < 0) {
                      $gnoverrides = '(' . number_format(abs($gnoverrides), 2) . ')';
                    } else {
                      $gnoverrides = number_format($gnoverrides, 2);
                    }
                  } else {
                    $gnoverrides = '-';
                  }

                  if ($gndeductions) {
                    if ($gndeductions < 0) {
                      $gndeductions = '(' . number_format(abs($gndeductions), 2) . ')';
                    } else {
                      $gndeductions = number_format($gndeductions, 2);
                    }
                  } else {
                    $gndeductions = '-';
                  }

                  if ($gnamount) {
                    if ($gnamount < 0) {
                      $gnamount = '(' . number_format(abs($gnamount), 2) . ')';
                    } else {
                      $gnamount = number_format($gnamount, 2);
                    }
                  } else {
                    $gnamount = '-';
                  }
            }

            $rows .= '<tr>
                    <td>' . $gnname . '</td>
                    <td>' . $gnrank . '</td>
                    <td style="text-align: center;">' . $gncomms . '</td>
                    <td style="text-align: center;">' . $gnoverrides . '</td>
                    <td style="text-align: center;">' . $gnincentives . '</td>
                    <td style="text-align: center;">' . $gndeductions . '</td>
                    <td style="text-align: center;">' . $gnamount . '</td>
                  </tr>';
          }

          $get_gn_total_comms = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($get_no_group))->sum('agent_banding');

          $get_gn_total_overrides = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($get_no_group))->sum('manage_share');

          $get_gn_total_incentives = PrePayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('type', '=', 'Incentives')
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($get_no_group))->sum('cost');

          $get_gn_total_deductions = PrePayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('type', '=', 'Deductions')
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($get_no_group))->sum('cost');

          $get_gn_total_amount = $get_gn_total_comms + $get_gn_total_overrides + ($get_gn_total_incentives - $get_gn_total_deductions);

          if ($get_gn_total_comms) {
            if ($get_gn_total_comms < 0) {
              $get_gn_total_comms = '(' . number_format(abs($get_gn_total_comms), 2) . ')';
            } else {
              $get_gn_total_comms = number_format($get_gn_total_comms, 2);
            }
          } else {
            $get_gn_total_comms = '-';
          }

          if ($get_gn_total_incentives) {
            if ($get_gn_total_incentives < 0) {
              $get_gn_total_incentives = '(' . number_format(abs($get_gn_total_incentives), 2) . ')';
            } else {
              $get_gn_total_incentives = number_format($get_gn_total_incentives, 2);
            }
          } else {
            $get_gn_total_incentives = '-';
          }

          if ($get_gn_total_overrides) {
            if ($get_gn_total_overrides < 0) {
              $get_gn_total_overrides = '(' . number_format(abs($get_gn_total_overrides), 2) . ')';
            } else {
              $get_gn_total_overrides = number_format($get_gn_total_overrides, 2);
            }
          } else {
            $get_gn_total_overrides = '-';
          }

          if ($get_gn_total_deductions) {
            if ($get_gn_total_deductions < 0) {
              $get_gn_total_deductions = '(' . number_format(abs($get_gn_total_deductions), 2) . ')';
            } else {
              $get_gn_total_deductions = number_format($get_gn_total_deductions, 2);
            }
          } else {
            $get_gn_total_deductions = '-';
          }

          if ($get_gn_total_amount) {
            if ($get_gn_total_amount < 0) {
              $get_gn_total_amount = '(' . number_format(abs($get_gn_total_amount), 2) . ')';
            } else {
              $get_gn_total_amount = number_format($get_gn_total_amount, 2);
            }
          } else {
            $get_gn_total_amount = '-';
          }

          $rows .= '<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="color: #ffffff;">-</td>
                </tr>';

          $rows .= '<tr>
                  <td><strong>TOTAL</strong></td>
                  <td></td>
                  <td style="text-align: center;">' . $get_gn_total_comms . '</td>
                  <td style="text-align: center;">' . $get_gn_total_overrides . '</td>
                  <td style="text-align: center;">' . $get_gn_total_incentives . '</td>
                  <td style="text-align: center;">' . $get_gn_total_deductions . '</td>
                  <td style="text-align: center;">' . $get_gn_total_amount . '</td>
                </tr>';

          $rows .= '</tbody></table><br>';
        }
      }

      $result['rows'] = $rows;
      // $result['total'] = number_format($commissions + $overrides + ($incentives - $deductions), 2);

      $view = view('payroll-management.pdf.view-firm-revenue', $result)->render();
      
      return Response::json(['report' => $view]);
            
      // return view('payroll-management.pdf.view-firm-revenue', $result);

      // $pdf = PDF::loadView('payroll-management.pdf.view-advisor-expand', $result);
      // return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');

    }


    public function advisorSummaryRevenue() {

      $id = Request::input('id');
      $get_batch = Batch::find($id);
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;
      $first_commission = 0;
      $renewal_commission = 0;
      $total_first_all = 0;
      $total_renewal_all = 0;

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      $get_users = array_unique(PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->lists('user_id')->toArray());

      $settings = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
      $result['date'] = Carbon::now()->format('d/m/Y');


      $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
      $result['type'] = "COMM";
      $result['summary'] = false;
      $all_first = '';
      $ctr = 0;

      ini_set('max_execution_time', 36000);

      foreach ($get_users as $key => $field) {

      $source_name = User::find($field)->name;
      $salesforce = SalesUser::where('user_id', $field)->first();
      $source_code = null;
      if ($salesforce) {
        $source_code = $salesforce->salesforce_id;
      }

      if ($ctr != 0) {
        $all_first .= '<div class="page-break"></div>';
      }

      $ctr += 1;
        $all_first .= '<table>
                      <tr>
                        <td>
                          <img src="' . url('images/logo') . '/legacy-main-logo.png' .'" style="height: 75px; padding-bottom: 10px;">
                        </td>
                        <td style="line-height: 1; text-align#: right;">
                          <small><small>' . $settings->address . '</small></small><br>
                          <small><small>' . $settings->address_2 . '</small></small><br>
                          <small><small>' . $settings->address_3 . '</small></small><br><br>
                        </td>
                      </tr>
                    </table>
                    <br>SOURCE NAME: ' . $source_name . ' <br>
                    SOURCE CODE: ' . $source_code . ' <br><br>
                    <big><big><strong>SUMMARY STATEMENT OF ACCOUNT</strong></big></big><br>
                    LFA ONGOING COMM FOR <strong>' . $result['batch_name'] . '</strong>
                    <div>
                    <br>';

          $all_first .= '<table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th style="background-color: #ffffff; color: #000000;">Principal</th>
                          <th style="background-color: #ffffff; color: #000000;">First Yr Comms</th>
                          <th style="background-color: #ffffff; color: #000000;">Renewal Comms</th>
                          <th style="background-color: #ffffff; color: #000000;">Amount</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';


          $get_providers = array_unique(PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->orderBy('provider_id', 'asc')->lists('provider_id')->toArray());
          $total = 0;
          $first_comms = 0;
          $renewal_comms = 0;

          sort($get_providers);
        
//           $payroll_name = Batch::where('')
//         dd($id);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            $get_provider_info = Provider::find($gpvalue);

            $get_class = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('status', '=', 1);
                                            })->orderBy('category_id', 'asc')->lists('category_id')->toArray());

            $all_first .= '<tr style="background-color: #d9edf7;">
                            <td colspan="4">' . $get_provider_info->name . '</td></tr>';
            sort($get_class);
            // dd($start);
            foreach ($get_class as $gckey => $gcvalue) {
              $year_start = intval(Carbon::createFromFormat('Y-m-d', $start)->subMonth()->format('Y'));
              $get_class_info = ProviderClassification::find($gcvalue);

              

              $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue, $field) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', $field)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');

              $renewal_start = Carbon::createFromFormat('Y-m-d', $start)->subMonth()->startOfMonth()->format('Y-m-d');
              $renewal_end = Carbon::createFromFormat('Y-m-d', $start)->subMonth()->endOfMonth()->format('Y-m-d');

              
              $first_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue, $field) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', $field)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })
                                            ->where(function($date) use ($year_start){
                                              if($year_start > 2016)
                                              {
                                                $date->whereYear('incept_date', '>', '2016');
                                              }
                                            })
                                            ->sum('agent_banding');

              $renewal_new = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue, $field, $renewal_start, $renewal_end) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->whereYear('incept_date', '=', '2016')
                                                  ->where('user_id', $field)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');
              
//               $year_start = intval(substr($start, 0, 4));
              



              $all_first .= '<tr>
                              <td>' . $get_class_info->name . '</td>
                              <td style="text-align: center;">' . ($first_year > 0 ? number_format($first_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . ($year_start <= 2016 ? '-' : ($renewal_new > 0 ? number_format($renewal_new, 2) : '-')) . '</td>
                              <td style="text-align: center;">' . (($first_year + ($year_start <= 2016 ? $renewal_year : $renewal_new)) > 0 ? number_format($first_year + ($year_start <= 2016 ? $renewal_year : $renewal_new), 2) : '-') . '</td>
                             </tr>';
              

//               $all_first .= '<tr>
//                               <td>' . $get_class_info->name . '</td>
//                               <td style="text-align: center;">' . ($first_year > 0 ? number_format($first_year, 2) : '-') . '</td>
//                               <td style="text-align: center;">' . ($renewal_year > 0 ? number_format($renewal_year, 2) : '-') . '</td>
//                               <td style="text-align: center;">' . (($first_year + $renewal_year) > 0 ? number_format($first_year + $renewal_year, 2) : '-') . '</td>
//                              </tr>';

              $total += $first_year;
              $total += ($year_start <= 2016 ? $renewal_year : $renewal_new);
              $first_comms += $first_year;
              $renewal_comms += ($year_start <= 2016 ? 0 : $renewal_new);

            }

              $all_first .= '<tr>
                              <td>&nbsp;</td>
                              <td></td>
                              <td></td>
                              <td style="text-align: center; color: #ffffff;"></td>
                            </tr>';
          }


          $all_first .= '<tr>
                          <td>TOTAL COMMS</td>
                          <td style="text-align: center;">' . ($first_comms > 0 ? number_format($first_comms, 2) : '-') . '</td>
                          <td style="text-align: center;">' . ($renewal_comms > 0 ? number_format($renewal_comms, 2) : '-') . '</td>
                          <td style="text-align: center;">' . ($total != 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';


          $all_first .= '<tr>
                          <td colspan="3">&nbsp;</td>
                          <td></td>
                         </tr>';

          $gpifield = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', $field)
                      ->where('type', '=', 'Incentives')
                      ->first();

          $total_pre_inc = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', $field)
                      ->where('type', '=', 'Incentives')
                      ->sum('cost');

          if ($gpifield) {
              $all_first .= '<tr>' .
                              '<td>Incentives</td>' .
                              '<td colspan=2>' . $gpifield->remarks . '</td>' .
                              '<td style="text-align: center;">' . number_format($total_pre_inc, 2) . '</td>' .
                            '</tr>';
          } else {
            $all_first .= '<tr>' .
                '<td>Incentives</td>' .
                '<td colspan=2><center>-</center></td>' .
                '<td style="text-align: center;">-</td>' .
              '</tr>';
          }


          $gpdfield = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', $field)
                      ->where('type', '=', 'Deductions')
                      ->first();

          $total_pre_ded = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', $field)
                      ->where('type', '=', 'Deductions')
                      ->sum('cost');

          if ($gpdfield) {
            $all_first .= '<tr>' .
                            '<td>Deductions</td>' .
                            '<td colspan=2>' . $gpdfield->remarks . '</td>' .
                            '<td style="text-align: center;">' . number_format($gpdfield->cost, 2) . '</td>' .
                          '</tr>';
          } else {
            $all_first .= '<tr>' .
                '<td>Deductions</td>' .
                '<td colspan=2><center>-</center></td>' .
                '<td style="text-align: center;">-</td>' .
              '</tr>';
          }

          $all_first .= '<tr>
                          <td colspan="3">&nbsp;</td>
                          <td></td>
                         </tr>';

          $total = $total + $total_pre_inc - $total_pre_ded;

          $all_first .= '<tr>
                          <td colspan="3">NET AMOUNT PAID</td>
                          <td style="text-align: center;">' . ($total != 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '</tbody></table>';

          $result['all_first'] = $all_first;

        }

        // dd($all_first);

      $view =  view('payroll-management.pdf.view-advisor-item-summary-revenue', $result)->render();

      return Response::json(['report' => $view]);

      // $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-summary-revenue', $result);
      // return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');
        
    }


    public function advisorSummaryOverride() {

      $id = Request::input('id');
      $get_batch = Batch::find($id);
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;
      $first_commission = 0;
      $renewal_commission = 0;
      $total_first_all = 0;
      $total_renewal_all = 0;

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      $settings = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
      $result['date'] = Carbon::now()->format('d/m/Y');

      $get_group = GroupComputation::where('batch_id', $id)->where('user_id', '>', 0)->lists('user_id')->toArray();
      $get_sups =  SalesSupervisorComputation::where('batch_id', $id)->lists('user_id')->toArray();

      $get_users = array_unique(array_merge($get_group, $get_sups));

      $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');

      $result['type'] = "OR";
      $result['summary'] = false;
      $all_first = '';
      $ctr = 0;

      #kardo
      $get_group = GroupComputation::where('batch_id', $id)->where('user_id', '>', 0)->lists('user_id')->toArray();
      $get_sups =  SalesSupervisorComputation::where('batch_id', $id)->lists('user_id')->toArray();
      $get_advs =  SalesAdvisorComputation::where('batch_id', $id)->lists('user_id')->toArray();

      $get_group = array_merge($get_group, $get_sups);

      $get_all_group = array_merge($get_group, $get_advs);

      $get_all = PayrollComputation::where('batch_id', $id)->lists('user_id');

      $get_all_users = PayrollComputation::where('batch_id', $id)->whereNotIn('user_id', $get_all_group)->lists('user_id')->toArray();

      $get_group = array_merge($get_group, $get_all_users);

      $get_users = User::whereIn('id', array_unique($get_group))->orderBy('name', 'asc')->lists('id');

      set_time_limit(9000);

      foreach ($get_users as $key => $field) {

        $get_group_id = GroupComputation::where('batch_id', $id)->where('user_id', '=', $field)->first();

        $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', $field)->first();
        if ($get_group_id) {

          $get_sups_user = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $get_group_id->id)->lists('user_id');

          $get_adv = SalesAdvisorComputation::where('batch_id', $id)->where('group_id', $get_group_id->id)->lists('user_id');
          
          $get_all_users = array_merge($get_sups_user->toArray(), $get_adv->toArray());

        } else if ($get_sups_group) {

          $get_sups_user = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', $field)->lists('user_id');

          $get_adv = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', $get_sups_group->id)->lists('user_id');

          $get_all_users = array_merge($get_sups_user->toArray(), $get_adv->toArray());

        } else {
          $get_all_users[0] = $field;
        }

        $source_name = User::find($field)->name;
        $salesforce = SalesUser::where('user_id', $field)->first();
        $source_code = null;
        if ($salesforce) {
          $source_code = $salesforce->salesforce_id;
        } 

        if ($ctr != 0) {
          $all_first .= '<div class="page-break"></div>';
        }

        $ctr += 1;

        $all_first .= '<table>
                      <tr>
                        <td>
                          <img src="' . url('images/logo') . '/legacy-main-logo.png' .'" style="height: 75px; padding-bottom: 10px;">
                        </td>
                        <td style="line-height: 1; text-align: right;">
                          <small><small>' . $settings->address . '</small></small><br>
                          <small><small>' . $settings->address_2 . '</small></small><br>
                          <small><small>' . $settings->address_3 . '</small></small><br><br>
                        </td>
                      </tr>
                    </table>
                    <br>SOURCE NAME: ' . $source_name . ' <br>
                    SOURCE CODE: ' . $source_code . ' <br><br>
                    <big><big><strong>SUMMARY STATEMENT OF ACCOUNT</strong></big></big><br>
                    LFA ONGOING OR FOR <strong>' . $result['batch_name'] . '</strong>
                    <div>
                    <br>';

          $all_first .= '<table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th style="background-color: #ffffff; color: #000000;">Principal</th>
                          <th style="background-color: #ffffff; color: #000000;">First Yr Overrides</th>
                          <th style="background-color: #ffffff; color: #000000;">Renewal Overrides</th>
                          <th style="background-color: #ffffff; color: #000000;">Amount</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';


          $get_providers = array_unique(PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->orderBy('provider_id', 'asc')->lists('provider_id')->toArray());
          $total = 0;

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            $get_provider_info = Provider::find($gpvalue);

            $get_class = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('status', '=', 1);
                                            })->orderBy('category_id', 'asc')->lists('category_id')->toArray());

            $all_first .= '<tr style="background-color: #d9edf7;">
                            <td colspan="4">' . $get_provider_info->name . '</td></tr>';
            sort($get_class);
            foreach ($get_class as $gckey => $gcvalue) {
              
              $get_class_info = ProviderClassification::find($gcvalue);
              $name = null;

              if ($get_group_id) {
                $first_year = PayrollComputation::where(function($query) use ($start, $end, $field, $gcvalue, $gpvalue) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('category_id', $gcvalue)
                                                  ->where('status', '=', 1);
                                            })->whereIn('user_id', $get_all_users)->sum('tier3_share');

                $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $field, $gcvalue, $gpvalue) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('category_id', $gcvalue)
                                                  ->where('status', '=', 1);
                                            })->whereIn('user_id', $get_all_users)->sum('tier3_share');

              } else if ($get_sups_group) {

                $first_year = PayrollComputation::where(function($query) use ($start, $end, $field, $gcvalue, $gpvalue) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('category_id', $gcvalue)
                                                  ->where('status', '=', 1);
                                            })->whereIn('user_id', $get_all_users)->sum('tier2_share');

                $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $field, $gcvalue, $gpvalue) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('category_id', $gcvalue)
                                                  ->where('status', '=', 1);
                                            })->whereIn('user_id', $get_all_users)->sum('tier2_share');
              } else {
                $first_year = PayrollComputation::where(function($query) use ($start, $end, $field, $gcvalue, $gpvalue) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('category_id', $gcvalue)
                                                  ->where('status', '=', 1);
                                            })->whereIn('user_id', $get_all_users)->sum('manage_share');

                $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $field, $gcvalue, $gpvalue) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('category_id', $gcvalue)
                                                  ->where('status', '=', 1);
                                            })->whereIn('user_id', $get_all_users)->sum('manage_share');
              }

              $all_first .= '<tr>
                              <td>' . $get_class_info->name . '</td>
                              <td style="text-align: center;">' . ($first_year > 0 ? number_format($first_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . ($renewal_year > 0 ? number_format($renewal_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . (($first_year + $renewal_year) > 0 ? number_format($first_year + $renewal_year, 2) : '-') . '</td>
                             </tr>';

              $total += $first_year;
              $total += $renewal_year;

            }

              $all_first .= '<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td style="text-align: center; color: #ffffff;">-</td>
                            </tr>';
          }

          $all_first .= '<tr>
                          <td colspan="3">NET AMOUNT PAID</td>
                          <td style="text-align: center;">' . ($total > 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '</tbody></table>';

          $result['all_first'] = $all_first;

        }


      $view = view('payroll-management.pdf.view-advisor-item-summary-revenue', $result)->render();
        
      return Response::json(['report' => $view]);

      // $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-summary-revenue', $result);
      // return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');
        
    }


    public function advisorItemSubExpandRevenue() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;
      $first_commission = 0;
      $renewal_commission = 0;
      $total_first_all = 0;
      $total_renewal_all = 0;

      $result['system_user'] = "YES";
      $rows = '';
      $get_batch = Batch::find($id);
      $result['title'] = 'Payroll Summary';
      $result['agent_name'] = '';
      $result['agent_rank'] = '';
      $result['start'] = date_format(date_create(substr($start, 0,10)),'M Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'M Y');

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      $result['type'] = "COMM";
      $result['summary'] = false;

      if (Auth::user()->usertype_id == 8) {

        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        $check_supervisor = SalesSupervisor::where("user_id", Auth::user()->id)->first();
        $check_advisor = SalesAdvisor::where("user_id", Auth::user()->id)->first();

        if ($check_supervisor) {
          $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if ($get_sup_id) {
            $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

            $ids = [];
            $ids[0] = Auth::user()->id;
            $ctr = 1;
            foreach ($get_adv_ids as $advkey => $advfield) {
              $ids[$ctr] = $advfield->user_id;
              $ctr += 1;
            }

            $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->lists('provider_id');
          } else {
            $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          // ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id');
          }

        } else if ($check_advisor) {
          $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          // ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id');
        }

      } else {
        $get_provider = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('status', '=', 1);
                                    })->lists('provider_id');
      }

      $no_team = false;
      
      if (Auth::user()->usertype_id == 8) {
        $check_no_team = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();
        if (!$check_no_team) {
          $check_no_adv_team = SalesAdvisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if (!$check_no_adv_team) {
            $no_team = true;
          }
        }
      }

      $get_provider = array_unique($get_provider->toArray());
      sort($get_provider);
      
      foreach ($get_provider as $ikey => $field) {

        if (Auth::user()->usertype_id == 8) {

        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        $check_supervisor = SalesSupervisor::where("user_id", Auth::user()->id)->first();
        $check_advisor = SalesAdvisor::where("user_id", Auth::user()->id)->first();

        if ($check_supervisor) {

          $check_tier3 = Group::where('user_id', '=', Auth::user()->id)->first();

          if ($check_tier3) {

            $check_group = GroupComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', Auth::user()->id);
                                    })->get();
            $gids = [];
            $gctr = 0;

            foreach ($check_group as $cgkey => $cgfield) {

              $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $cgfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->get(); 
              
              foreach ($get_supervisors as $gskey => $gsfield) {
                $gids[$gctr] = $gsfield->user_id;
                $gctr += 1;

                $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $cgfield, $gsfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('supervisor_id', '=', $gsfield->id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->lists('user_id');

                foreach (array_unique($get_advisors->toArray()) as $gakey => $gafield) {
                  $gids[$gctr] = $gafield;
                  $gctr += 1;
                }
              }
            }

            $commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('agent_banding');

            $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('agent_banding');

            $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('agent_banding');

          } else {
            $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

            if ($get_sup_id) {

              $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

              $ids = [];
              $ids[0] = Auth::user()->id;
              $ctr = 1;
              foreach ($get_adv_ids as $advkey => $advfield) {
                $ids[$ctr] = $advfield->user_id;
                $ctr += 1;
              }

              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('agent_banding');

              $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('agent_banding');

              $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('agent_banding');


            } else {
              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');

              $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');

              $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
            }
          }

          } else if ($check_advisor) {
            $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');

            $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');

            $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
          }

        } else {
          $commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('status', '=', 1);
                                  })->sum('agent_banding');

          $first_commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                  })->sum('agent_banding');

          $renewal_commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                  })->sum('agent_banding');
        }

        $get_name = Provider::find($field);

        $rows .= '<tr>' . 
                  '<td>' . $get_name->name . '</td>' .
                  '<td style="text-align: center;">' . number_format($first_commission, 2) . '</td>' .
                  '<td style="text-align: center;">' . number_format($renewal_commission, 2) . '</td>' .
                  '<td style="text-align: center;">' . number_format($renewal_commission + $first_commission, 2) . '</td>' .
                  '</tr>';

        $total_first_all += $first_commission;
        $total_renewal_all += $renewal_commission;

      }

      $all_first = '';


      if (Auth::user()->usertype_id == 8) {

        if (Request::input('personal')) {

          $all_first = '<table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th style="background-color: #ffffff; color: #000000;">Principal</th>
                          <th style="background-color: #ffffff; color: #000000;">First Yr Comms</th>
                          <th style="background-color: #ffffff; color: #000000;">Renewal Comms</th>
                          <th style="background-color: #ffffff; color: #000000;">Amount</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          $result['settings'] = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
          $result['date'] = Carbon::now()->format('d/m/Y');

          $result['source_name'] = User::find(Auth::user()->id)->name;
          $salesforce = SalesUser::where('user_id', Auth::user()->id)->first();
          $result['source_code'] = null;
          if ($salesforce) {
            $result['source_code'] = $salesforce->salesforce_id;
          }


          $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
          $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');

          $get_providers = array_unique(PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id')->toArray());

          $total = 0;

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            $get_provider_info = Provider::find($gpvalue);

            $get_class = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  // ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('status', '=', 1);
                                            })->lists('category_id')->toArray());

            $all_first .= '<tr style="background-color: #d9edf7;">
                            <td colspan="4">' . $get_provider_info->name . '</td></tr>';
            sort($get_class);
            foreach ($get_class as $gckey => $gcvalue) {
              
              $get_class_info = ProviderClassification::find($gcvalue);

              $first_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');

              $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');

              $all_first .= '<tr>
                              <td>' . $get_class_info->name . '</td>
                              <td style="text-align: center;">' . ($first_year > 0 ? number_format($first_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . ($renewal_year > 0 ? number_format($renewal_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . (($first_year + $renewal_year) > 0 ? number_format($first_year + $renewal_year, 2) : '-') . '</td>
                             </tr>';

              $total += $first_year;
              $total += $renewal_year;

            }

              $all_first .= '<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td style="text-align: center; color: #ffffff;">-</td>
                            </tr>';
          }



          $all_first .= '<tr>
                          <td colspan="3">TOTAL COMMS</td>
                          <td style="text-align: center;">' . ($total != 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '<tr>
                          <td colspan="3">&nbsp;</td>
                          <td></td>
                         </tr>';

          $get_pre_inc = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', Auth::user()->id)
                      ->where('type', '=', 'Incentives')
                      ->take(1)->get();

          $total_pre_inc = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', Auth::user()->id)
                      ->where('type', '=', 'Incentives')
                      ->sum('cost');

          if (count($get_pre_inc) > 0) {
            foreach ($get_pre_inc as $gpikey => $gpifield) {
              $all_first .= '<tr>' .
                              '<td>Incentives</td>' .
                              '<td colspan=2>' . $gpifield->remarks . '</td>' .
                              '<td style="text-align: center;">' . number_format($total_pre_inc, 2) . '</td>' .
                            '</tr>';
            }
          } else {
            $all_first .= '<tr>' .
                '<td>Incentives</td>' .
                '<td colspan=2><center>-</center></td>' .
                '<td style="text-align: center;">-</td>' .
              '</tr>';
          }


          $get_pre_ded = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', Auth::user()->id)
                      ->where('type', '=', 'Deductions')
                      ->take(1)->get();

          $total_pre_ded = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', Auth::user()->id)
                      ->where('type', '=', 'Deductions')
                      ->sum('cost');

          if (count($get_pre_ded) > 0) {
            foreach ($get_pre_ded as $gpdkey => $gpdfield) {
              $all_first .= '<tr>' .
                              '<td>Deductions</td>' .
                              '<td colspan=2>' . $gpdfield->remarks . '</td>' .
                              '<td style="text-align: center;">' . number_format($gpdfield->cost, 2) . '</td>' .
                            '</tr>';
            }
          } else {
            $all_first .= '<tr>' .
                '<td>Deductions</td>' .
                '<td colspan=2><center>-</center></td>' .
                '<td style="text-align: center;">-</td>' .
              '</tr>';
          }

          $all_first .= '<tr>
                          <td colspan="3">&nbsp;</td>
                          <td></td>
                         </tr>';

          $total = $total + $total_pre_inc - $total_pre_ded;

          $all_first .= '<tr>
                          <td colspan="3">NET AMOUNT PAID</td>
                          <td style="text-align: center;">' . ($total != 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '</tbody></table>';

          $result['all_first'] = $all_first;


          // #hello
          // $all_first .= '<tr>
          //                 <td>NET AMOUNT PAID</td>
          //                 <td></td>
          //                 <td></td>
          //                 <td style="text-align: center;">' . ($total > 0 ? number_format($total, 2) : '-') . '</td>
          //                </tr>';


          // $all_first .= '</tbody></table>';

          // $result['all_first'] = $all_first;

          // return view('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);
          $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);
          return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');
        }
      }

      if (Auth::user()->usertype_id == 8) {

        $get_providers = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('status', '=', 1);
                                                        })->lists('provider_id')->toArray());
        $name = '';
        $rank = '';
        $total_first = 0;
        $total_renewal = 0;
        $total_amount = 0;
        $result['system_user'] = null;

        $auth_info = User::with('salesInfo')->with('salesInfo.designations')->find(Auth::user()->id);

        $check_group = Group::where('user_id', '=', Auth::user()->id)->first();

        if ($check_group) {
          $get_groups = GroupComputation::where(function($query) use ($start, $end, $id) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('user_id', '=', Auth::user()->id);
                                    })->get();
        } else if ($auth_info->salesInfo->designations->rank == "Supervisor") {

          $get_sups = SalesSupervisorComputation::where(function($query) use ($start, $end, $id) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('user_id', '=', Auth::user()->id);
                                    })->first();
          $user_ids = [];

          if ($auth_info) {
            $name = $auth_info->name;

            $get_designation = BatchMonthUser::with('designations')->where('user_id', Auth::user()->id)->where('batch_id', '=', $id)->first();
            if ($get_designation) {
              $rank = $get_designation->designations->designation;
            }
          }

          $all_first .= '<center>
                            <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                          </center>';

          $all_first .= '<table>
                          <tr>
                            <td style="text-align: left; text-decoration: underline; font-weight: bold;">Payroll Breakdown</td>
                            <td style="text-align: right;"><b>Provider:</b> All Providers</td>
                          </tr>
                          <tr>
                            <td><b>Agent Name:</b> ' . $name . '</td>
                            <td style="text-align: right;"><b>Period From:</b>' . $result['start'] . '</td>
                          </tr>
                          <tr>
                            <td><b>Rank:</b> ' . $rank . '</td>
                            <td style="text-align: right;"><b>Period To:</b>' . $result['end'] . '</td>
                          </tr>
                        </table>
                        <br><br><b>Revenue</b>';

          $all_first .= '<table class="table-main">
                        <thead class="thead-table">
                          <tr>
                            <th>Team Members</th>
                            <th>Rank</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (first)</th>';
          }

          $all_first .= '<th>First</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (renewal)</th>';
          }

          $all_first .= '<th>Renewal</th>
                          <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody class="tbody-table">';

        // $all_first .= '<table class="table-main">
        //               <thead class="thead-table">
        //                 <tr>
        //                   <th>Team Members</th>
        //                   <th>Rank</th>
        //                   <th>First</th>
        //                   <th>Renewal</th>
        //                   <th>Amount</th>
        //                 </tr>
        //               </thead>
        //               <tbody class="tbody-table">';

          if($get_sups->user_id) {

            $user_ids[0] = $get_sups->user_id;

            $info = User::with('salesInfo')->with('salesInfo.designations')->find($get_sups->user_id);

            $first = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $get_sups->user_id)
                                                            ->where('first_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $renewal = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $get_sups->user_id)
                                                            ->where('renewal_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $all_first .= '<tr>
                            <td>' . $info->name . '</td>
                            <td>' . $info->salesInfo->designations->designation . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $get_sups->user_id)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');

              $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($first, 2) . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $get_sups->user_id)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');

              $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($renewal, 2) . '</td>
                            <td style="text-align: center;">' . number_format($first + $renewal, 2) . '</td>
                          </tr>';

            $total_first += $first;
            $total_renewal += $renewal;
            $total_amount += $renewal + $first;
          }
          
          $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $get_sups) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('supervisor_id', '=', $get_sups->id);
                                  })->lists('user_id');

          $user_ids = array_merge($get_advisors->toArray(), $user_ids);

          foreach ($get_advisors as $gakey => $gavalue) {

            $info = User::with('salesInfo')->with('salesInfo.designations')->find($gavalue);

            $first = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $gavalue)
                                                            ->where('first_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $renewal = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $gavalue)
                                                            ->where('renewal_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $all_first .= '<tr>
                            <td>' . $info->name . '</td>
                            <td>' . $info->salesInfo->designations->designation . '</td>';
            
            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $gavalue)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');

              $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($first, 2) . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $gavalue)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');

              $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
            }

            $all_first .='<td style="text-align: center;">' . number_format($renewal, 2) . '</td>
                            <td style="text-align: center;">' . number_format($first + $renewal, 2) . '</td>
                          </tr>';

            $total_first += $first;
            $total_renewal += $renewal;
            $total_amount += $renewal + $first;
          }

          $all_first .= '<tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';

          $all_first .= '<tr>
                          <td>PROVIDER TOTAL</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {

            $provider_info = Provider::find($gpvalue);

            $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('first_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('agent_banding');


            $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);

            $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('renewal_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('agent_banding');


            $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';


          $all_first .= '<tr>
                          <td><b>TOTAL</b></td>
                          <td></td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }
          
          $all_first .= '<td style="text-align: center;">' . number_format($total_first, 2) . '</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }     

          $all_first .= '<td style="text-align: center;">' . number_format($total_renewal, 2) . '</td>
                          <td style="text-align: center;">' . number_format($total_amount, 2) . '</td>
                        </tr>';

          $all_first .= '</tbody></table>';

          $all_first .= '</div>';

          $result['all_first'] = $all_first;

          $view = view('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result)->render();
            
          return Response::json(['report' => $view]);

          // $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result);
          // return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');

        } else if ($auth_info->salesInfo->designations->rank == "Advisor") {

          $all_first = '<table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th style="background-color: #ffffff; color: #000000;">Principal</th>
                          <th style="background-color: #ffffff; color: #000000;">First Yr Comms</th>
                          <th style="background-color: #ffffff; color: #000000;">Renewal Comms</th>
                          <th style="background-color: #ffffff; color: #000000;">Amount</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          $result['settings'] = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
          $result['date'] = Carbon::now()->format('d/m/Y');

          $result['source_name'] = User::find(Auth::user()->id)->name;
          $salesforce = SalesUser::where('user_id', Auth::user()->id)->first();
          $result['source_code'] = null;
          if ($salesforce) {
            $result['source_code'] = $salesforce->salesforce_id;
          }


          $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
          $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');

          $get_providers = array_unique(PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          // ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id')->toArray());
          $total = 0;

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            $get_provider_info = Provider::find($gpvalue);

            $get_class = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  // ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('status', '=', 1);
                                            })->lists('category_id')->toArray());

            $all_first .= '<tr style="background-color: #d9edf7;">
                            <td colspan="4">' . $get_provider_info->name . '</td></tr>';
            sort($get_class);

            foreach ($get_class as $gckey => $gcvalue) {
              
              $get_class_info = ProviderClassification::find($gcvalue);

              $first_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');

              $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');
              $all_first .= '<tr>
                              <td>' . $get_class_info->name . '</td>
                              <td style="text-align: center;">' . ($first_year > 0 ? number_format($first_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . ($renewal_year > 0 ? number_format($renewal_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . (($first_year + $renewal_year) > 0 ? number_format($first_year + $renewal_year, 2) : '-') . '</td>
                             </tr>';

              $total += $first_year;
              $total += $renewal_year;

            }

              $all_first .= '<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td style="text-align: center; color: #ffffff;">-</td>
                            </tr>';
          }


          $all_first .= '<tr>
                          <td colspan="3">TOTAL COMMS</td>
                          <td style="text-align: center;">' . ($total != 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '<tr>
                          <td colspan="3">&nbsp;</td>
                          <td></td>
                         </tr>';

          $get_pre_inc = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', Auth::user()->id)
                      ->where('type', '=', 'Incentives')
                      ->take(1)->get();

          $total_pre_inc = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id', Auth::user()->id)
                      ->where('type', '=', 'Incentives')
                      ->sum('cost');

          if (count($get_pre_inc) > 0) {
            foreach ($get_pre_inc as $gpikey => $gpifield) {
              $all_first .= '<tr>' .
                              '<td>Incentives</td>' .
                              '<td colspan=2>' . $gpifield->remarks . '</td>' .
                              '<td style="text-align: center;">' . number_format($total_pre_inc, 2) . '</td>' .
                            '</tr>';
            }
          } else {
            $all_first .= '<tr>' .
                '<td>Incentives</td>' .
                '<td colspan=2><center>-</center></td>' .
                '<td style="text-align: center;">-</td>' .
              '</tr>';
          }


          $get_pre_ded = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id',  Auth::user()->id)
                      ->where('type', '=', 'Deductions')
                      ->take(1)->get();

          $total_pre_ded = PrePayrollComputation::where('batch_id', $id)
                      ->where('user_id',  Auth::user()->id)
                      ->where('type', '=', 'Deductions')
                      ->sum('cost');

          if (count($get_pre_ded) > 0) {
            foreach ($get_pre_ded as $gpdkey => $gpdfield) {
              $all_first .= '<tr>' .
                              '<td>Deductions</td>' .
                              '<td colspan=2>' . $gpdfield->remarks . '</td>' .
                              '<td style="text-align: center;">' . number_format($gpdfield->cost, 2) . '</td>' .
                            '</tr>';
            }
          } else {
            $all_first .= '<tr>' .
                '<td>Deductions</td>' .
                '<td colspan=2><center>-</center></td>' .
                '<td style="text-align: center;">-</td>' .
              '</tr>';
          }

          $all_first .= '<tr>
                          <td colspan="3">&nbsp;</td>
                          <td></td>
                         </tr>';

          $total = $total + $total_pre_inc - $total_pre_ded;

          $all_first .= '<tr>
                          <td colspan="3">NET AMOUNT PAID</td>
                          <td style="text-align: center;">' . ($total != 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '</tbody></table>';

          $result['all_first'] = $all_first;

          $result['all_first'] = $all_first;

          // return view('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);

          $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);
          return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');

        }
      } else {
        $get_groups = GroupComputation::where(function($query) use ($start, $end, $id) {
                                  $query->where('batch_id', '=', $id);
                                  })->get();
      }


      foreach ($get_groups as $ggkey => $ggfield) {
        
        $total_first_main = 0;
        $total_renewal_main = 0;
        $total_amount_main = 0;
        $user_ids = [];

        $get_providers = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('status', '=', 1);
                                                        })->lists('provider_id')->toArray());
          $name = '';
          $rank = '';
          $total_first = 0;
          $total_renewal = 0;
          $total_amount = 0;

          if ($ggfield->user_id) {
            $user_info = User::with('salesInfo')->with('salesInfo.designations')->find($ggfield->user_id);

            if ($user_info) {
              $name = $user_info->name;

              $get_designation = BatchMonthUser::with('designations')->where('user_id', $ggfield->user_id)->where('batch_id', '=', $id)->first();
              if ($get_designation) {
                $rank = $get_designation->designations->designation;
              }
            }

          } else {
            $name = "N/A";
            $rank = "N/A";
          }

          if ($result['system_user']) {
            $all_first .= '<br><br><div class="page-break"></div>';
          }

          $all_first .= '<center>
                            <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                          </center>';

          $all_first .= '<table>
                          <tr>
                            <td style="text-align: left; text-decoration: underline; font-weight: bold;">Payroll Breakdown</td>
                            <td style="text-align: right;"><b>Provider:</b> All Providers</td>
                          </tr>
                          <tr>
                            <td><b>Agent Name:</b> ' . $name . '</td>
                            <td style="text-align: right;"><b>Period From:</b>' . $result['start'] . '</td>
                          </tr>
                          <tr>
                            <td><b>Rank:</b> ' . $rank . '</td>
                            <td style="text-align: right;"><b>Period To:</b>' . $result['end'] . '</td>
                          </tr>
                        </table>
                        <br><b>Revenue</b>';

          $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $ggfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('group_id', '=', $ggfield->id);
                                  })->lists('user_id');

          $user_ids = array_merge($user_ids, $get_supervisors->toArray());

          $all_first .= '<table class="table-main">
                        <thead class="thead-table">
                          <tr>
                            <th>Team Members</th>
                            <th>Rank</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (first)</th>';
          }

          $all_first .= '<th>First</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (renewal)</th>';
          }

          $all_first .= '<th>Renewal</th>
                          <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody class="tbody-table">';
                          // <tr>
                          //   <th colspan="5">' . $provider_info->name . '</th>
                          // </tr>';

          if ($ggfield->user_id) {
            $info = User::with('salesInfo')->with('salesInfo.designations')->find($ggfield->user_id);

            $first_group = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $ggfield->user_id)
                                                            ->where('first_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $renewal_group = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $ggfield->user_id)
                                                            ->where('renewal_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $all_first .= '<tr>
                            <td>' . $info->name . '</td>
                            <td>' . $info->salesInfo->designations->designation . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $ggfield->user_id)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');


              $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($first_group, 2) . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $ggfield->user_id)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');


              $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($renewal_group, 2) . '</td>
                            <td style="text-align: center;">' . number_format($first_group + $renewal_group, 2) . '</td>
                          </tr>';

            $total_first += $first_group;
            $total_renewal += $renewal_group;
            $total_amount += $renewal_group + $first_group;

            // $total_first_main += $first_group;
            // $total_renewal_main += $renewal_group;
            // $total_amount_main += $renewal_group + $first_group;
          }

          foreach ($get_supervisors as $gskey => $gsvalue) {

            if ($ggfield->user_id != $gsvalue && ($ggfield->user_id)) {

              $info = User::with('salesInfo')->with('salesInfo.designations')->find($gsvalue);

              $first = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('user_id', '=', $gsvalue)
                                                              ->where('first_year', '=', "YES")
                                                              ->where('status', '=', 1);
                                                        })->sum('agent_banding');

              $renewal = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('user_id', '=', $gsvalue)
                                                              ->where('renewal_year', '=', "YES")
                                                              ->where('status', '=', 1);
                                                        })->sum('agent_banding');

              $all_first .= '<tr>
                              <td>' . $info->name . '</td>
                              <td>' . $info->salesInfo->designations->designation . '</td>
                              <td style="text-align: center;">' . number_format($first, 2) . '</td>';

              sort($get_providers);
              foreach ($get_providers as $gpkey => $gpvalue) {
                $provider_info = Provider::find($gpvalue);

                $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', '=', $gsvalue)
                                                  ->where('provider_id', '=', $gpvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');


                $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
              }

              $all_first .= '<td style="text-align: center;">' . number_format($renewal, 2) . '</td>';

              sort($get_providers);
              foreach ($get_providers as $gpkey => $gpvalue) {
                $provider_info = Provider::find($gpvalue);

                $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', '=', $gsvalue)
                                                  ->where('provider_id', '=', $gpvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');


                $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
              }

              $all_first .= '<td style="text-align: center;">' . number_format($first + $renewal, 2) . '</td>
                              </tr>';

              $total_first += $first;
              $total_renewal += $renewal;
              $total_amount += $renewal + $first;

              // $total_first_main += $first;
              // $total_renewal_main += $renewal;
              // $total_amount_main += $renewal + $first;
            }
          } 

          $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $ggfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('group_id', '=', $ggfield->id);
                                  })->lists('user_id');


          $user_ids = array_merge($user_ids, $get_advisors->toArray());

          foreach ($get_advisors as $gakey => $gavalue) {
        
            $info = User::with('salesInfo')->with('salesInfo.designations')->find($gavalue);

            $first = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $gavalue)
                                                            ->where('first_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $renewal = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $gavalue)
                                                            ->where('renewal_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('agent_banding');

            $all_first .= '<tr>
                            <td>' . $info->name . '</td>
                            <td>' . $info->salesInfo->designations->designation . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $gavalue)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');


              $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($first, 2) . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $gavalue)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('agent_banding');


              $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($renewal, 2) . '</td>
                            <td style="text-align: center;">' . number_format($first + $renewal, 2) . '</td>
                          </tr>';

            $total_first += $first;
            $total_renewal += $renewal;
            $total_amount += $renewal + $first;

        }

          $all_first .= '<tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';

          $all_first .= '<tr>
                          <td>PROVIDER TOTAL</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {

            $provider_info = Provider::find($gpvalue);

            $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('first_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('agent_banding');


            $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);

            $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('renewal_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('agent_banding');


            $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';


        $all_first .= '<tr>
                          <td><b>TOTAL</b></td>
                          <td></td>';

        sort($get_providers);
        foreach ($get_providers as $gpkey => $gpvalue) {
          $all_first .= '<td>&nbsp;</td>';
        }

        $all_first .= '<td style="text-align: center;">' . number_format($total_first, 2) . '</td>';

        sort($get_providers);
        foreach ($get_providers as $gpkey => $gpvalue) {
          $all_first .= '<td>&nbsp;</td>';
        }

        $all_first .= '<td style="text-align: center;">' . number_format($total_renewal, 2) . '</td>
                          <td style="text-align: center;">' . number_format($total_first + $total_renewal, 2) . '</td>
                        </tr>';

        $all_first .= '</tbody></table>';

      }

      $all_first .= '</div>';

      $result['rows'] = $rows;
      $result['total'] = number_format($total_first_all + $total_renewal_all, 2);
      $result['total_first'] = number_format($total_first_all, 2);
      $result['total_renewal'] = number_format($total_renewal_all, 2);
      $result['all_first'] = $all_first;

      // return view('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result);

      $view = view('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result)->render();
        
      return Response::json(['report' => $view]);

      // $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result);
      // return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');
    }



    public function advisorItemSubExpandOverrideOR() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;
      $first_commission = 0;
      $renewal_commission = 0;
      $total_first_all = 0;
      $total_renewal_all = 0;

      $result['system_user'] = "YES";
      $rows = '';
      $get_batch = Batch::find($id);
      $result['title'] = 'Payroll Summary';
      $result['agent_name'] = '';
      $result['agent_rank'] = '';
      $result['start'] = date_format(date_create(substr($start, 0,10)),'M Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'M Y');
      $result['settings'] = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
      $result['date'] = Carbon::now()->format('d/m/Y');

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      $result['source_name'] = $get_batch->name;
      $salesforce = SalesUser::where('user_id', Auth::user()->id)->first();
      $result['source_code'] = null;
      if ($salesforce) {
        $result['source_code'] = $salesforce->salesforce_id;
      }


      $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');

      $result['summary'] = true;

      $get_providers = array_unique(PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->orderBy('provider_id', 'asc')->lists('provider_id')->toArray());
      $rows = null;
      $ctr = 0;

      sort($get_providers);
      foreach ($get_providers as $key => $field) {
        
        $get_provider = Provider::find($field);
        if ($ctr > 0) {
          $rows .= '<br><br><div class="page-break"></div>';
        }

        $rows .= '<strong>Provider: ' . $get_provider->name . '</strong><br>';

        $rows  .= '<table class="table-main">
            <thead class="thead-table">
              <tr>
                <th style="background-color: #ffffff; color: #000000;">Agent Name</th>
                <th style="background-color: #ffffff; color: #000000;">Amount</th>
              </tr>
            </thead>
            <tbody class="tbody-table">';

            $get_group = GroupComputation::where('batch_id', $id)->where('user_id', '>', 0)->lists('user_id')->toArray();
            $get_sups =  SalesSupervisorComputation::where('batch_id', $id)->lists('user_id')->toArray();
            $get_advs =  SalesAdvisorComputation::where('batch_id', $id)->lists('user_id')->toArray();

            $get_group = array_merge($get_group, $get_sups);

            $get_all_group = array_merge($get_group, $get_advs);

            $get_all = PayrollComputation::where('batch_id', $id)->lists('user_id');

            $get_all_users = PayrollComputation::where('batch_id', $id)->whereNotIn('user_id', $get_all_group)->lists('user_id')->toArray();

            $get_group = array_merge($get_group, $get_all_users);

            $get_group = User::whereIn('id', array_unique($get_group))->orderBy('name', 'asc')->lists('id');

            $amount = 0;
            foreach ($get_group as $key => $ggfield) {

              $get_group_id = GroupComputation::where('batch_id', $id)->where('user_id', '=', $ggfield)->first();

              $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', $ggfield)->first();
              if ($get_group_id) {

                $get_sups_user = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $get_group_id->id)->lists('user_id');

                $get_adv = SalesAdvisorComputation::where('batch_id', $id)->where('group_id', $get_group_id->id)->lists('user_id');
                
                $get_all_users = array_merge($get_sups_user->toArray(), $get_adv->toArray());

              } else if ($get_sups_group) {

                $get_sups_user = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', $ggfield)->lists('user_id');

                $get_adv = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', $get_sups_group->id)->lists('user_id');

                $get_all_users = array_merge($get_sups_user->toArray(), $get_adv->toArray());

              } else {
                $get_all_users[0] = $ggfield;
              }


              $name = null;

              if ($get_group_id) {

                $get_override = PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('provider_id', $field)
                                                ->where('status', '=', 1);
                                          })->whereIn('user_id', $get_all_users)->sum('tier3_share');

              } else if ($get_sups_group) {

                $get_override = PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('provider_id', $field)
                                                ->where('status', '=', 1);
                                          })->whereIn('user_id', $get_all_users)->sum('tier2_share');

              } else {

                $get_override = PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('provider_id', $field)
                                                ->where('user_id', $ggfield)
                                                ->where('status', '=', 1);
                                          })->whereIn('user_id', $get_all_users)->sum('manage_share');
              }

              $get_name = User::find($ggfield);

              if ($get_name) {
                $name = $get_name->name;
              }

              if ($get_override != 0) {
                $rows .= '<tr>' .
                          '<td>' . $name . '</td>' .
                          '<td style="text-align: center;">' . number_format($get_override, 2) . '</td>'.
                        '</tr>';
              }

            }

              $amount = PayrollComputation::where(function($query) use ($start, $end, $field) {
                                          $query->where('batch_id', '=', Request::input('id'))
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('provider_id', $field)
                                                ->where('status', '=', 1);
                                          })->whereIn('user_id', $get_all)->sum('manage_share');

              $rows .= '<tr>' .
                        '<td>&nbsp;</td>' .
                        '<td></td>'.
                      '</tr>';
              $rows .= '<tr>' .
                        '<td><strong>TOTAL</strong></td>' .
                        '<td style="text-align: center;"><strong>' . number_format($amount, 2) . '</strong></td>'.
                      '</tr>';

        $rows .= '</tbody></table>';
        $ctr += 1;
      }

      $result['all_first'] = $rows;
      $result['type'] = 'OR';

      // return view('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);

      $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);
      return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');
    }


    public function advisorItemSubExpandRevenueOR() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;
      $first_commission = 0;
      $renewal_commission = 0;
      $total_first_all = 0; 
      $total_renewal_all = 0;

      $result['system_user'] = "YES";
      $rows = '';
      $get_batch = Batch::find($id);
      $result['title'] = 'Payroll Summary';
      $result['agent_name'] = '';
      $result['agent_rank'] = '';
      $result['start'] = date_format(date_create(substr($start, 0,10)),'M Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'M Y');
      $result['settings'] = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
      $result['date'] = Carbon::now()->format('d/m/Y');

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      $result['source_name'] = $get_batch->name;
      $salesforce = SalesUser::where('user_id', Auth::user()->id)->first();
      $result['source_code'] = null;
      if ($salesforce) {
        $result['source_code'] = $salesforce->salesforce_id;
      }

      $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');

      $result['summary'] = true;

      $get_providers = array_unique(PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->orderBy('provider_id', 'asc')->lists('provider_id')->toArray());
      // dd($get_providers);
      $rows = null;
      $ctr = 0;

      sort($get_providers);
      foreach ($get_providers as $key => $field) {
        
        $get_provider = Provider::find($field);
        if ($ctr > 0) {
          $rows .= '<div class="page-break"></div>';
        }

        $rows .= '<strong>Provider: ' . $get_provider->name . '</strong><br>';

        $rows  .= '<table class="table-main">
            <thead class="thead-table">
              <tr>
                <th style="background-color: #ffffff; color: #000000;">Agent Name</th>
                <th style="background-color: #ffffff; color: #000000;">Amount</th>
              </tr>
            </thead>
            <tbody class="tbody-table">';

            // $get_group = GroupComputation::where('batch_id', $id)->where('user_id', '>', 0)->lists('user_id')->toArray();
            // $get_sups =  SalesSupervisorComputation::where('batch_id', $id)->lists('user_id')->toArray();

            // $get_group = array_merge($get_group, $get_sups);

            // $get_group = PayrollComputation::where(function($query) use ($start, $end, $field) {
            //                             $query->where('batch_id', '=', Request::input('id'))
            //                                   ->where('upload_date', '>=', $start . " 00:00:00")
            //                                   ->where('upload_date', '<=', $end . " 00:00:00")
            //                                   ->where('provider_id', $field)
            //                                   ->where('status', '=', 1);
            //                             })->lists('user_id');

            // $get_group = User::whereIn('id', array_unique($get_group))->lists('id');

            $get_user = PayrollComputation::where(function($query) use ($start, $end, $field) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', $field)
                                              ->where('status', '=', 1);
                                        })->lists('user_id');
            
            $get_user = User::whereIn('id', $get_user)->orderBy('name', 'asc')->lists('id');

            $get_group = array_unique($get_user->toArray());

            $amount = 0;
            foreach ($get_group as $key => $ggfield) {

              $name = null;

              // $get_group_id = GroupComputation::where('batch_id', $id)->where('user_id', '=', $ggfield)->first();

              // $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', $ggfield)->first();
              // $get_adv = [];

              // if ($get_sups_group) {
              //   $get_adv = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', $get_sups_group->id)->lists('user_id');
              // }

              // if ($get_group_id) {
              //   $get_override = PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
              //                               $query->where('batch_id', '=', Request::input('id'))
              //                                     ->where('upload_date', '>=', $start . " 00:00:00")
              //                                     ->where('upload_date', '<=', $end . " 00:00:00")
              //                                     ->where('provider_id', $field)
              //                                     ->where('user_id', $ggfield)
              //                                     ->where('status', '=', 1);
              //                               })->sum('agent_banding');

              //   $get_sups = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $get_group_id->id)->lists('user_id');

              //   // $get_override += PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
              //   //                             $query->where('batch_id', '=', Request::input('id'))
              //   //                                   ->where('upload_date', '>=', $start . " 00:00:00")
              //   //                                   ->where('upload_date', '<=', $end . " 00:00:00")
              //   //                                   ->where('provider_id', $field)
              //   //                                   ->where('status', '=', 1);
              //   //                             })->whereIn('user_id', $get_sups)->sum('agent_banding');

              // } else {
              //   $get_override = PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
              //                               $query->where('batch_id', '=', Request::input('id'))
              //                                     ->where('upload_date', '>=', $start . " 00:00:00")
              //                                     ->where('upload_date', '<=', $end . " 00:00:00")
              //                                     ->where('provider_id', $field)
              //                                     ->where('user_id', $ggfield)
              //                                     ->where('status', '=', 1);
              //                               })->sum('agent_banding');
              // }

              // $get_override += PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
              //                             $query->where('batch_id', '=', Request::input('id'))
              //                                   ->where('upload_date', '>=', $start . " 00:00:00")
              //                                   ->where('upload_date', '<=', $end . " 00:00:00")
              //                                   ->where('provider_id', $field)
              //                                   ->where('status', '=', 1);
              //                             })->whereIn('user_id', $get_adv)->sum('agent_banding');

                $get_override = PayrollComputation::where(function($query) use ($start, $end, $field, $ggfield) {
                                            $query->where('batch_id', '=', Request::input('id'))
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', $field)
                                                  ->where('user_id', $ggfield)
                                                  ->where('status', '=', 1);
                                            })->sum('agent_banding');
                
              $get_name = User::find($ggfield);

              if ($get_name) {
                $name = $get_name->name;
              }

              if ($get_override != 0) {
                $rows .= '<tr>' .
                          '<td>' . $name . '</td>' .
                          '<td style="text-align: center;">' . number_format($get_override, 2) . '</td>'.
                        '</tr>';

                $amount += $get_override;
              }

            }

              $rows .= '<tr>' .
                        '<td>&nbsp;</td>' .
                        '<td></td>'.
                      '</tr>';
              $rows .= '<tr>' .
                        '<td><strong>TOTAL</strong></td>' .
                        '<td style="text-align: center;"><strong>' . number_format($amount, 2) . '</strong></td>'.
                      '</tr>';

        $rows .= '</tbody></table>';
        $ctr += 1;
      }

      $result['all_first'] = $rows;
      $result['type'] = 'COMM';
      dd($rows);

      // return view('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);

      $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary-pdf', $result);
      return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');
    }


    public function advisorItemSubExpandOverride() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;
      $first_commission = 0;
      $renewal_commission = 0;
      $total_first_all = 0;
      $total_renewal_all = 0;

      $result['system_user'] = "YES";
      $rows = '';
      $get_batch = Batch::find($id);
      $result['title'] = 'Payroll Summary';
      $result['agent_name'] = '';
      $result['agent_rank'] = '';
      $result['start'] = date_format(date_create(substr($start, 0,10)),'M Y');
      $result['end'] = date_format(date_create(substr($start, 0,10)),'M Y');
      $result['summary'] = false;
      $result['type'] = "OR";

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      if (Auth::user()->usertype_id == 8) {

        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        $check_supervisor = SalesSupervisor::where("user_id", Auth::user()->id)->first();
        $check_advisor = SalesAdvisor::where("user_id", Auth::user()->id)->first();

        if ($check_supervisor) {
          $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if ($get_sup_id) {
            $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

            $ids = [];
            $ids[0] = Auth::user()->id;
            $ctr = 1;
            foreach ($get_adv_ids as $advkey => $advfield) {
              $ids[$ctr] = $advfield->user_id;
              $ctr += 1;
            }

            $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->lists('provider_id');
          } else {
            $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          // ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id');
          }

        } else if ($check_advisor) {
          $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          // ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id');
        }

      } else {
        $get_provider = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('status', '=', 1);
                                    })->lists('provider_id');
      }

      $no_team = false;
      
      if (Auth::user()->usertype_id == 8) {
        $check_no_team = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();
        if (!$check_no_team) {
          $check_no_adv_team = SalesAdvisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if (!$check_no_adv_team) {
            $no_team = true;
          }
        }
      }

      foreach (array_unique($get_provider->toArray()) as $ikey => $field) {

        if (Auth::user()->usertype_id == 8) {

        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        $check_supervisor = SalesSupervisor::where("user_id", Auth::user()->id)->first();
        $check_advisor = SalesAdvisor::where("user_id", Auth::user()->id)->first();

        if ($check_supervisor) {

          $check_tier3 = Group::where('user_id', '=', Auth::user()->id)->first();

          if ($check_tier3) {

            $check_group = GroupComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', Auth::user()->id);
                                    })->get();
            $gids = [];
            $gctr = 0;

            foreach ($check_group as $cgkey => $cgfield) {

              $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $cgfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->get(); 
              
              foreach ($get_supervisors as $gskey => $gsfield) {
                $gids[$gctr] = $gsfield->user_id;
                $gctr += 1;

                $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $cgfield, $gsfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('supervisor_id', '=', $gsfield->id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->lists('user_id');

                foreach (array_unique($get_advisors->toArray()) as $gakey => $gafield) {
                  $gids[$gctr] = $gafield;
                  $gctr += 1;
                }
              }
            }

            $commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('manage_share');

            $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('manage_share');

            $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('manage_share');

          } else {
            $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

            if ($get_sup_id) {

              $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

              $ids = [];
              $ids[0] = Auth::user()->id;
              $ctr = 1;
              foreach ($get_adv_ids as $advkey => $advfield) {
                $ids[$ctr] = $advfield->user_id;
                $ctr += 1;
              }

              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('manage_share');

              $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('manage_share');

              $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('manage_share');


            } else {
              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');

              $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');

              $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');
            }
          }

          } else if ($check_advisor) {
            $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');

            $first_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('first_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');

            $renewal_commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('renewal_year', '=', "YES")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');
          }

        } else {
          $commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('status', '=', 1);
                                  })->sum('manage_share');

          $first_commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                  })->sum('manage_share');

          $renewal_commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                  })->sum('manage_share');
        }

        $get_name = Provider::find($field);

        $rows .= '<tr>' . 
                  '<td>' . $get_name->name . '</td>' .
                  '<td style="text-align: center;">' . number_format($first_commission, 2) . '</td>' .
                  '<td style="text-align: center;">' . number_format($renewal_commission, 2) . '</td>' .
                  '<td style="text-align: center;">' . number_format($renewal_commission + $first_commission, 2) . '</td>' .
                  '</tr>';

        $total_first_all += $first_commission;
        $total_renewal_all += $renewal_commission;

      }

      $all_first = '';


      if (Auth::user()->usertype_id == 8) {

        if (Request::input('personal')) {

          $all_first = '<table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th style="background-color: #ffffff; color: #000000;">Principal</th>
                          <th style="background-color: #ffffff; color: #000000;">First Yr Overrides</th>
                          <th style="background-color: #ffffff; color: #000000;">Renewal Overrides</th>
                          <th style="background-color: #ffffff; color: #000000;">Amount</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          $result['settings'] = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
          $result['date'] = Carbon::now()->format('d/m/Y');

          $result['source_name'] = User::find(Auth::user()->id)->name;
          $salesforce = SalesUser::where('user_id', Auth::user()->id)->first();
          $result['source_code'] = null;
          if ($salesforce) {
            $result['source_code'] = $salesforce->salesforce_id;
          }


          $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
          $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');

          $get_providers = array_unique(PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          // ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id')->toArray());
          $total = 0;

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            $get_provider_info = Provider::find($gpvalue);

            $get_class = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  // ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('status', '=', 1);
                                            })->orderBy('category_id', 'asc')->lists('category_id')->toArray());

            $all_first .= '<tr style="background-color: #d9edf7;">
                            <td colspan="4">' . $get_provider_info->name . '</td></tr>';

            $get_group_id = GroupComputation::where('batch_id', $id)->where('user_id', '=', Auth::user()->id)->first();

            $get_sups_group = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', Auth::user()->id)->first();
            if ($get_group_id) {

              $get_sups_user = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $get_group_id->id)->lists('user_id');

              $get_adv = SalesAdvisorComputation::where('batch_id', $id)->where('group_id', $get_group_id->id)->lists('user_id');
              
              $get_all_users = array_merge($get_sups_user->toArray(), $get_adv->toArray());

            } else if ($get_sups_group) {

              $get_sups_user = SalesSupervisorComputation::where('batch_id', $id)->where('user_id', Auth::user()->id)->lists('user_id');

              $get_adv = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', $get_sups_group->id)->lists('user_id');

              $get_all_users = array_merge($get_sups_user->toArray(), $get_adv->toArray());

            } else {
              $get_all_users[0] = Auth::user()->id;
            }

            sort($get_class);

            foreach ($get_class as $gckey => $gcvalue) {
              
              $get_class_info = ProviderClassification::find($gcvalue);

              if ($get_group_id) {
                $first_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('provider_id', $gpvalue)
                                                    ->where('category_id', $gcvalue)
                                                    ->where('first_year', '=', "YES")
                                                    ->where('status', '=', 1);
                                              })->whereIn('user_id', $get_all_users)->sum('tier3_share');

                $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('provider_id', $gpvalue)
                                                    ->where('category_id', $gcvalue)
                                                    ->where('renewal_year', '=', "YES")
                                                    ->where('status', '=', 1);
                                              })->whereIn('user_id', $get_all_users)->sum('tier3_share');
              } else if ($get_sups_group) {
                $first_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('provider_id', $gpvalue)
                                                    ->where('category_id', $gcvalue)
                                                    ->where('first_year', '=', "YES")
                                                    ->where('status', '=', 1);
                                              })->whereIn('user_id', $get_all_users)->sum('tier2_share');

                $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('provider_id', $gpvalue)
                                                    ->where('category_id', $gcvalue)
                                                    ->where('renewal_year', '=', "YES")
                                                    ->where('status', '=', 1);
                                              })->whereIn('user_id', $get_all_users)->sum('tier2_share');
              } else {
                $first_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('provider_id', $gpvalue)
                                                    ->where('category_id', $gcvalue)
                                                    ->where('first_year', '=', "YES")
                                                    ->where('status', '=', 1);
                                              })->whereIn('user_id', $get_all_users)->sum('manage_share');

                $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                              $query->where('batch_id', '=', $id)
                                                    ->where('upload_date', '>=', $start . " 00:00:00")
                                                    ->where('upload_date', '<=', $end . " 00:00:00")
                                                    ->where('provider_id', $gpvalue)
                                                    ->where('category_id', $gcvalue)
                                                    ->where('renewal_year', '=', "YES")
                                                    ->where('status', '=', 1);
                                              })->whereIn('user_id', $get_all_users)->sum('manage_share');
              }

              $all_first .= '<tr>
                              <td>' . $get_class_info->name . '</td>
                              <td style="text-align: center;">' . ($first_year > 0 ? number_format($first_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . ($renewal_year > 0 ? number_format($renewal_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . (($first_year + $renewal_year) > 0 ? number_format($first_year + $renewal_year, 2) : '-') . '</td>
                             </tr>';

              $total += $first_year;
              $total += $renewal_year;

            }

              $all_first .= '<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td style="text-align: center; color: #ffffff;">-</td>
                            </tr>';
          }

          $all_first .= '<tr>
                          <td colspan="3">NET AMOUNT PAID</td>
                          <td style="text-align: center;">' . ($total > 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '</tbody></table>';

          $result['all_first'] = $all_first;

          $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);
          return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');
        }
      }

      if (Auth::user()->usertype_id == 8) {

        $get_providers = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('status', '=', 1);
                                                        })->lists('provider_id')->toArray());
        $name = '';
        $rank = '';
        $total_first = 0;
        $total_renewal = 0;
        $total_amount = 0;
        $result['system_user'] = null;

        $auth_info = User::with('salesInfo')->with('salesInfo.designations')->find(Auth::user()->id);

        $check_group = Group::where('user_id', '=', Auth::user()->id)->first();

        if ($check_group) {
          $get_groups = GroupComputation::where(function($query) use ($start, $end, $id) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('user_id', '=', Auth::user()->id);
                                    })->get();
        } else if ($auth_info->salesInfo->designations->rank == "Supervisor") {

          $get_sups = SalesSupervisorComputation::where(function($query) use ($start, $end, $id) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('user_id', '=', Auth::user()->id);
                                    })->first();
          $user_ids = [];

          if ($auth_info) {
            $name = $auth_info->name;

            $get_designation = BatchMonthUser::with('designations')->where('user_id', Auth::user()->id)->where('batch_id', '=', $id)->first();
            if ($get_designation) {
              $rank = $get_designation->designations->designation;
            }
          }

          $all_first .= '<center>
                            <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                          </center>';

          $all_first .= '<table>
                          <tr>
                            <td style="text-align: left; text-decoration: underline; font-weight: bold;">Payroll Breakdown</td>
                            <td style="text-align: right;"><b>Provider:</b> All Providers</td>
                          </tr>
                          <tr>
                            <td><b>Agent Name:</b> ' . $name . '</td>
                            <td style="text-align: right;"><b>Period From:</b>' . $result['start'] . '</td>
                          </tr>
                          <tr>
                            <td><b>Rank:</b> ' . $rank . '</td>
                            <td style="text-align: right;"><b>Period To:</b>' . $result['end'] . '</td>
                          </tr>
                        </table>
                        <br><br><b>Override</b>';

          $all_first .= '<table class="table-main">
                        <thead class="thead-table">
                          <tr>
                            <th>Team Members</th>
                            <th>Rank</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (first)</th>';
          }

          $all_first .= '<th>First</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (renewal)</th>';
          }

          $all_first .= '<th>Renewal</th>
                          <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody class="tbody-table">';

          if($get_sups->user_id) {

            $user_ids[0] = $get_sups->user_id;

            $info = User::with('salesInfo')->with('salesInfo.designations')->find($get_sups->user_id);

            $first = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $get_sups->user_id)
                                                            ->where('first_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('tier2_share');

            $renewal = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $get_sups->user_id)
                                                            ->where('renewal_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('tier2_share');

            $all_first .= '<tr>
                            <td>' . $info->name . '</td>
                            <td>' . $info->salesInfo->designations->designation . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $get_sups->user_id)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('tier2_share');

              $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($first, 2) . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $get_sups, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $get_sups->user_id)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('tier2_share');

              $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($renewal, 2) . '</td>
                            <td style="text-align: center;">' . number_format($first + $renewal, 2) . '</td>
                          </tr>';

            $total_first += $first;
            $total_renewal += $renewal;
            $total_amount += $renewal + $first;
          }
          
          $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $get_sups) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('supervisor_id', '=', $get_sups->id);
                                  })->lists('user_id');

          $user_ids = array_merge($get_advisors->toArray(), $user_ids);

          foreach ($get_advisors as $gakey => $gavalue) {

            $info = User::with('salesInfo')->with('salesInfo.designations')->find($gavalue);

            $first = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $gavalue)
                                                            ->where('first_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('manage_share');

            $renewal = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('user_id', '=', $gavalue)
                                                            ->where('renewal_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->sum('manage_share');

            $all_first .= '<tr>
                            <td>' . $info->name . '</td>
                            <td>' . $info->salesInfo->designations->designation . '</td>';
            
            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $gavalue)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('manage_share');

              $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($first, 2) . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gavalue, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('user_id', '=', $gavalue)
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->sum('manage_share');

              $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
            }

            $all_first .='<td style="text-align: center;">' . number_format($renewal, 2) . '</td>
                            <td style="text-align: center;">' . number_format($first + $renewal, 2) . '</td>
                          </tr>';

            $total_first += $first;
            $total_renewal += $renewal;
            $total_amount += $renewal + $first;
          }

          $all_first .= '<tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';

          $all_first .= '<tr>
                          <td>PROVIDER TOTAL</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {

            $provider_info = Provider::find($gpvalue);

            $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('first_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('manage_share');


            $tier3_share = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('first_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('tier3_share');

            $all_first .= '<td style="text-align: center;">' . number_format($first_provider - $tier3_share, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);

            $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('renewal_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('manage_share');


            $tier3_share = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('renewal_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('tier3_share');

            $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider - $tier3_share, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';


          $all_first .= '<tr>
                          <td><b>TOTAL</b></td>
                          <td></td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }
          
          $all_first .= '<td style="text-align: center;">' . number_format($total_first, 2) . '</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }     

          $all_first .= '<td style="text-align: center;">' . number_format($total_renewal, 2) . '</td>
                          <td style="text-align: center;">' . number_format($total_amount, 2) . '</td>
                        </tr>';

          $all_first .= '</tbody></table>';

          $all_first .= '</div>';

          $result['all_first'] = $all_first;

          $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result);
          return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');

        } else if ($auth_info->salesInfo->designations->rank == "Advisor") {

          $all_first = '<table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th style="background-color: #ffffff; color: #000000;">Principal</th>
                          <th style="background-color: #ffffff; color: #000000;">First Yr Overrides</th>
                          <th style="background-color: #ffffff; color: #000000;">Renewal Overrides</th>
                          <th style="background-color: #ffffff; color: #000000;">Amount</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          $result['settings'] = Settings::select('settings.address', 'settings.address_2', 'settings.address_3', 'settings.tel_no')->first();
          $result['date'] = Carbon::now()->format('d/m/Y');

          $result['source_name'] = User::find(Auth::user()->id)->name;
          $salesforce = SalesUser::where('user_id', Auth::user()->id)->first();
          $result['source_code'] = null;
          if ($salesforce) {
            $result['source_code'] = $salesforce->salesforce_id;
          }


          $result['start'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');
          $result['end'] = date_format(date_create(substr($start, 0,10)),'d/m/Y');

          $get_providers = array_unique(PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', $id)
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          // ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id')->toArray());
          $total = 0;

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            
            $get_provider_info = Provider::find($gpvalue);

            $get_class = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  // ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('status', '=', 1);
                                            })->lists('category_id')->toArray());

            $all_first .= '<tr style="background-color: #d9edf7;">
                            <td colspan="4">' . $get_provider_info->name . '</td></tr>';

            sort($get_class);

            foreach ($get_class as $gckey => $gcvalue) {
              
              $get_class_info = ProviderClassification::find($gcvalue);

              $first_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('manage_share');

              $renewal_year = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue, $gcvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('user_id', Auth::user()->id)
                                                  ->where('provider_id', $gpvalue)
                                                  ->where('category_id', $gcvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                            })->sum('manage_share');
              $all_first .= '<tr>
                              <td>' . $get_class_info->name . '</td>
                              <td style="text-align: center;">' . ($first_year > 0 ? number_format($first_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . ($renewal_year > 0 ? number_format($renewal_year, 2) : '-') . '</td>
                              <td style="text-align: center;">' . (($first_year + $renewal_year) > 0 ? number_format($first_year + $renewal_year, 2) : '-') . '</td>
                             </tr>';

              $total += $first_year;
              $total += $renewal_year;

            }

              $all_first .= '<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td style="text-align: center; color: #ffffff;">-</td>
                            </tr>';
          }

          $all_first .= '<tr>
                          <td colspan="3">NET AMOUNT PAID</td>
                          <td style="text-align: center;">' . ($total > 0 ? number_format($total, 2) : '-') . '</td>
                         </tr>';

          $all_first .= '</tbody></table>';

          $result['all_first'] = $all_first;

          // $result['all_first'] = $all_first;

          // return view('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);

          $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue-summary', $result);
          return $pdf->setPaper('A4')->setOrientation('portrait')->download($get_batch->name . '.pdf');

        }
      } else {
        $get_groups = GroupComputation::where(function($query) use ($start, $end, $id) {
                                  $query->where('batch_id', '=', $id);
                                  })->get();
      }

      foreach ($get_groups as $ggkey => $ggfield) {
        
        $total_first_main = 0;
        $total_renewal_main = 0;
        $total_amount_main = 0;
        $user_ids = [];

        $get_providers = array_unique(PayrollComputation::where(function($query) use ($start, $end, $id) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('status', '=', 1);
                                                        })->lists('provider_id')->toArray());
          $name = '';
          $rank = '';
          $total_first = 0;
          $total_renewal = 0;
          $total_amount = 0;

          if ($ggfield->user_id) {
            $user_info = User::with('salesInfo')->with('salesInfo.designations')->find($ggfield->user_id);

            if ($user_info) {
              $name = $user_info->name;

              $get_designation = BatchMonthUser::with('designations')->where('user_id', $ggfield->user_id)->where('batch_id', '=', $id)->first();
              if ($get_designation) {
                $rank = $get_designation->designations->designation;
              }
            }
          } else {
            $name = "N/A";
            $rank = "N/A";
          }

          if ($result['system_user']) {
            $all_first .= '<br><br><div class="page-break"></div>';
          }

          $all_first .= '<center>
                            <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                          </center>';

          $all_first .= '<table>
                          <tr>
                            <td style="text-align: left; text-decoration: underline; font-weight: bold;">Payroll Breakdown</td>
                            <td style="text-align: right;"><b>Provider:</b> All Providers</td>
                          </tr>
                          <tr>
                            <td><b>Agent Name:</b> ' . $name . '</td>
                            <td style="text-align: right;"><b>Period From:</b>' . $result['start'] . '</td>
                          </tr>
                          <tr>
                            <td><b>Rank:</b> ' . $rank . '</td>
                            <td style="text-align: right;"><b>Period To:</b>' . $result['end'] . '</td>
                          </tr>
                        </table>
                        <br><b>Override</b>';

          $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $ggfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('group_id', '=', $ggfield->id);
                                  })->lists('user_id');

          $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $ggfield) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('group_id', '=', $ggfield->id);
                                  })->lists('user_id');

          $user_ids = array_merge($user_ids, $get_supervisors->toArray(), $get_advisors->toArray());

          $all_first .= '<table class="table-main">
                        <thead class="thead-table">
                          <tr>
                            <th>Team Members</th>
                            <th>Rank</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (first)</th>';
          }

          $all_first .= '<th>First</th>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);
            $all_first .= '<th>' . $provider_info->name . ' (renewal)</th>';
          }

          $all_first .= '<th>Renewal</th>
                          <th>Amount</th>
                          </tr>
                        </thead>
                        <tbody class="tbody-table">';
                          // <tr>
                          //   <th colspan="5">' . $provider_info->name . '</th>
                          // </tr>';

          if ($ggfield->user_id) {
            $info = User::with('salesInfo')->with('salesInfo.designations')->find($ggfield->user_id);


            $get_supervisors_group = SalesSupervisorComputation::where('batch_id', $id)
                                                        ->where('group_id', $ggfield->id)
                                                        ->lists('user_id')->toArray();

            $get_supervisors_id = SalesSupervisorComputation::where('batch_id', $id)
                                                        ->where('group_id', $ggfield->id)
                                                        ->lists('id');

            $get_advisors_group = SalesAdvisorComputation::where('batch_id', $id)
                                                        ->where('group_id', $ggfield->id)
                                                        ->whereIn('supervisor_id', $get_supervisors_id)
                                                        ->lists('user_id')->toArray();

            $get_ids = array_merge($get_supervisors_group, $get_advisors_group);

            $first_group = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('first_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->whereIn('user_id', $get_ids)->sum('tier3_share');

            $renewal_group = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield) {
                                                      $query->where('batch_id', '=', $id)
                                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                                            ->where('renewal_year', '=', "YES")
                                                            ->where('status', '=', 1);
                                                      })->whereIn('user_id', $get_ids)->sum('tier3_share');

            $all_first .= '<tr>
                            <td>' . ($info ? $info->name : '') . '</td>
                            <td>' . ($info ? $info->salesInfo->designations->designation : '') . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('first_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->whereIn('user_id', $get_ids)->sum('tier3_share');


              $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($first_group, 2) . '</td>';

            sort($get_providers);
            foreach ($get_providers as $gpkey => $gpvalue) {
              $provider_info = Provider::find($gpvalue);

              $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $ggfield, $gpvalue) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('provider_id', '=', $gpvalue)
                                                ->where('renewal_year', '=', "YES")
                                                ->where('status', '=', 1);
                                          })->whereIn('user_id', $get_ids)->sum('tier3_share');


              $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
            }

            $all_first .= '<td style="text-align: center;">' . number_format($renewal_group, 2) . '</td>
                            <td style="text-align: center;">' . number_format($first_group + $renewal_group, 2) . '</td>
                          </tr>';

            $total_first += $first_group;
            $total_renewal += $renewal_group;
            $total_amount += $renewal_group + $first_group;
          }

          foreach ($get_supervisors as $gskey => $gsvalue) {

            if ($ggfield->user_id != $gsvalue && ($ggfield->user_id)) {

              $get_supervisor_id = SalesSupervisorComputation::where(function($query) use ($id, $ggfield, $gsvalue) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', $gsvalue)
                                            ->where('group_id', '=', $ggfield->id);
                                      })->first();

              $get_advisors_group = [];

              if ($get_supervisor_id) {
                $get_advisors_group = SalesAdvisorComputation::where('batch_id', $id)
                                                          ->where('supervisor_id', $get_supervisor_id->id)
                                                          ->lists('user_id');
              }

              $g_ids[0] = $gsvalue;
              $ctr = 1;

              foreach ($get_advisors_group as $gagkey => $gagvalue) {
                $g_ids[$ctr] = $gagvalue;
                $ctr += 1;
              }


              $info = User::with('salesInfo')->with('salesInfo.designations')->find($gsvalue);

              $first = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('first_year', '=', "YES")
                                                              ->where('status', '=', 1);
                                                        })->whereIn('user_id', $g_ids)->sum('tier2_share');

              $renewal = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue) {
                                                        $query->where('batch_id', '=', $id)
                                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                                              ->where('renewal_year', '=', "YES")
                                                              ->where('status', '=', 1);
                                                        })->whereIn('user_id', $g_ids)->sum('tier2_share');

              $all_first .= '<tr>
                              <td>' . ($info ? $info->name : '') . '</td>
                              <td>' . ($info ? $info->salesInfo->designations->designation : '') . '</td>';

              sort($get_providers);
              foreach ($get_providers as $gpkey => $gpvalue) {
                $provider_info = Provider::find($gpvalue);

                $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', '=', $gpvalue)
                                                  ->where('first_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                                })->whereIn('user_id', $g_ids)->sum('tier2_share');

                $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
              }

              $all_first .= '<td style="text-align: center;">' . number_format($first, 2) . '</td>';

              sort($get_providers);
              foreach ($get_providers as $gpkey => $gpvalue) {
                $provider_info = Provider::find($gpvalue);

                $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gsvalue, $gpvalue) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('provider_id', '=', $gpvalue)
                                                  ->where('renewal_year', '=', "YES")
                                                  ->where('status', '=', 1);
                                                })->whereIn('user_id', $g_ids)->sum('tier2_share');

                $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
              }

              $all_first .= '<td style="text-align: center;">' . number_format($renewal, 2) . '</td>';

              $all_first .= '<td style="text-align: center;">' . number_format($first + $renewal, 2) . '</td>
                              </tr>';

              $total_first += $first;
              $total_renewal += $renewal;
              $total_amount += $renewal + $first;
            }
          } 

          $all_first .= '<tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $all_first .= '<td>&nbsp;</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';

          $all_first .= '<tr>
                          <td>PROVIDER TOTAL</td>
                          <td>&nbsp;</td>';

          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {

            $provider_info = Provider::find($gpvalue);

            $first_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('first_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('manage_share');

            // $tier3_share = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
            //                             $query->where('batch_id', '=', $id)
            //                                   ->where('upload_date', '>=', $start . " 00:00:00")
            //                                   ->where('upload_date', '<=', $end . " 00:00:00")
            //                                   ->where('provider_id', '=', $gpvalue)
            //                                   ->where('first_year', '=', "YES")
            //                                   ->where('status', '=', 1);
            //                             })->whereIn('user_id', $user_ids)->sum('tier3_share');


            $all_first .= '<td style="text-align: center;">' . number_format($first_provider, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>';
          
          sort($get_providers);
          foreach ($get_providers as $gpkey => $gpvalue) {
            $provider_info = Provider::find($gpvalue);

            $renewal_provider = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
                                        $query->where('batch_id', '=', $id)
                                              ->where('upload_date', '>=', $start . " 00:00:00")
                                              ->where('upload_date', '<=', $end . " 00:00:00")
                                              ->where('provider_id', '=', $gpvalue)
                                              ->where('renewal_year', '=', "YES")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $user_ids)->sum('manage_share');

            // $tier3_share = PayrollComputation::where(function($query) use ($start, $end, $id, $gpvalue) {
            //                             $query->where('batch_id', '=', $id)
            //                                   ->where('upload_date', '>=', $start . " 00:00:00")
            //                                   ->where('upload_date', '<=', $end . " 00:00:00")
            //                                   ->where('provider_id', '=', $gpvalue)
            //                                   ->where('renewal_year', '=', "YES")
            //                                   ->where('status', '=', 1);
            //                             })->whereIn('user_id', $user_ids)->sum('tier3_share');

            $all_first .= '<td style="text-align: center;">' . number_format($renewal_provider, 2) . '</td>';
          }

          $all_first .= '<td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>';


        $all_first .= '<tr>
                          <td><b>TOTAL</b></td>
                          <td></td>';

        sort($get_providers);
        foreach ($get_providers as $gpkey => $gpvalue) {
          $all_first .= '<td>&nbsp;</td>';
        }

        $all_first .= '<td style="text-align: center;">' . number_format($total_first, 2) . '</td>';

        sort($get_providers);
        foreach ($get_providers as $gpkey => $gpvalue) {
          $all_first .= '<td>&nbsp;</td>';
        }

        $all_first .= '<td style="text-align: center;">' . number_format($total_renewal, 2) . '</td>
                          <td style="text-align: center;">' . number_format($total_first + $total_renewal, 2) . '</td>
                        </tr>';

        $all_first .= '</tbody></table>';

      }

      $all_first .= '</div>';

      $result['rows'] = $rows;
      $result['total'] = number_format($total_first_all + $total_renewal_all, 2);
      $result['total_first'] = number_format($total_first_all, 2);
      $result['total_renewal'] = number_format($total_renewal_all, 2);
      $result['all_first'] = $all_first;

      // return view('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result);

      $pdf = PDF::loadView('payroll-management.pdf.view-advisor-item-sub-expand-revenue', $result);
      return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');
    }

    public function pdfFirmRevenue() {

      $id = Request::input('id');
      $get_batch = Batch::find($id);
      $rows = 'N/A';
      $get_group = null;
      if ($get_batch) {
        $get_group = GroupComputation::where('batch_id', $get_batch->id)->get();
      }

      $result['batch_name'] = null;
      if ($get_batch) {
        $result['batch_name'] = $get_batch->name;
      }

      if ($get_group) {
        $rows = '';
        $main_ids = [];
        $main_ctr = 0;

        foreach ($get_group as $key => $field) {

          $ids = [];
          $ctr = 0;
          if ($field->user_id) {
            $user = User::find($field->user_id);
            $ids[$ctr] = $field->user_id;
            $ctr += 1;
            $main_ids[$main_ctr] = $field->user_id;
            $main_ctr += 1;

            $name = null;
            $code = null;
            if ($user) {
              $name = $user->name;
              $code = $user->code;
            }

            $firm_revenue = PayrollComputation::where(function($query) use ($field, $id) {
                              $query->where('batch_id', $id)
                                    ->where('user_id', $field->user_id)
                                    ->where('status', 1);
                            })->sum('firm_revenue');
          }
          
          if ($key != 0) {
            $rows .= '<div class="page-break">
                      </div>';
          }

          $rows .= '<center>
                      <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                    </center>
                    <strong>Group Code:</strong> ' . $field->code . '<br>
                    <strong>Group Owner:</strong> ' . $name . '<br><br>
                    <table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th>Agent Code</th>
                          <th>Agent Name</th>
                          <th>Rank</th>
                          <th>Firm Revenue</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          $rows .= '<tr>
                      <td>' . $code . '</td>
                      <td>' . $name . '</td>
                      <td>Owner</td>
                      <td style="text-align: center;">' . number_format($firm_revenue, 2) . '</td>
                    </tr>';

          $get_supervisors = SalesSupervisorComputation::where('batch_id', $id)->where('group_id', $field->id)->get();

          foreach ($get_supervisors as $gskey => $gsfield) {

              if ($gsfield->user_id) {
                $gsuser = User::with('salesInfo')->with('salesInfo.designations')->find($gsfield->user_id);
                $ids[$ctr] = $gsfield->user_id;
                $ctr += 1;
                $main_ids[$main_ctr] = $gsfield->user_id;
                $main_ctr += 1;

                $gsname = null;
                $gscode = null;
                $gsrank = null;
                if ($gsuser) {
                  $gsname = $gsuser->name;
                  $gscode = $gsuser->code;
                }

                if ($gsuser->salesInfo->designations->rank) {
                  $gsrank = $gsuser->salesInfo->designations->rank;
                }

                $gsfirm_revenue = PayrollComputation::where(function($query) use ($gsfield, $id) {
                                  $query->where('batch_id', $id)
                                        ->where('user_id', $gsfield->user_id)
                                        ->where('status', 1);
                                })->sum('firm_revenue');
              }

              if ($gsfield->user_id != $field->user_id) {
                $rows .= '<tr>
                        <td>' . $gscode . '</td>
                        <td>' . $gsname . '</td>
                        <td>' . $gsrank . '</td>
                        <td style="text-align: center;">' . number_format($gsfirm_revenue, 2) . '</td>
                      </tr>';
              }

              $get_advisors = SalesAdvisorComputation::where('batch_id', $id)->where('supervisor_id', $gsfield->id)->get();

              foreach ($get_advisors as $gakey => $gafield) {

                if ($gafield->user_id) {
                  $gauser = User::with('salesInfo')->with('salesInfo.designations')->find($gafield->user_id);
                  $ids[$ctr] = $gafield->user_id;
                  $ctr += 1;
                  $main_ids[$main_ctr] = $gafield->user_id;
                  $main_ctr += 1;

                  $ganame = null;
                  $gacode = null;
                  $garank = null;
                  if ($user) {
                    $ganame = $gauser->name;
                    $gacode = $gauser->code;
                  }

                  if ($user->salesInfo->designations->rank) {
                    $garank = $gauser->salesInfo->designations->rank;
                  }

                  $gafirm_revenue = PayrollComputation::where(function($query) use ($gafield, $id) {
                                    $query->where('batch_id', $id)
                                          ->where('user_id', $gafield->user_id)
                                          ->where('status', 1);
                                  })->sum('firm_revenue');
                }

                $rows .= '<tr>
                        <td>' . $gacode . '</td>
                        <td>' . $ganame . '</td>
                        <td>' . $garank . '</td>
                        <td style="text-align: center;">' . number_format($gafirm_revenue, 2) . '</td>
                      </tr>';
              }
            
          }

          $get_total = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($ids))->sum('firm_revenue');

          $rows .= '<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="color: #ffffff;">-</td>
                </tr>';

          $rows .= '<tr>
                  <td><strong>TOTAL</strong></td>
                  <td></td>
                  <td></td>
                  <td style="text-align: center;"><strong>' . number_format($get_total, 2) . '</strong></td>
                </tr>';

          $rows .= '</tbody></table><br>';

        }

        $get_no_group = PayrollComputation::where(function($query) use ($id) {
                                  $query->where('batch_id', $id)
                                        ->where('status', 1);
                                })->whereNotIn('user_id', array_unique($main_ids))->lists('user_id');


        if (count($get_no_group) > 0) {

          $rows .= '<center>
                      <img src="' . url('images/logo') . '/legacy-main-logo.png' . '" style="height: 75px; padding-bottom: 10px;">
                    </center>
                    <strong>No Team</strong>
                    <table class="table-main">
                      <thead class="thead-table">
                        <tr>
                          <th>Agent Code</th>
                          <th>Agent Name</th>
                          <th>Firm Revenue</th>
                        </tr>
                      </thead>
                      <tbody class="tbody-table">';

          foreach ($get_no_group as $gnkey => $gnfield) {
          
            if ($gnfield->user_id) {
              $gnuser = User::find($gnfield->user_id);

              $gnname = null;
              $gncode = null;

              if ($user) {
                $gnname = $gnuser->name;
                $gncode = $gnuser->code;
              }

              $gnfirm_revenue = PayrollComputation::where(function($query) use ($gnfield, $id) {
                                $query->where('batch_id', $id)
                                      ->where('user_id', $gnfield->user_id)
                                      ->where('status', 1);
                              })->sum('firm_revenue');
            }

            $rows .= '<tr>
                    <td>' . $gncode . '</td>
                    <td>' . $gnname . '</td>
                    <td style="text-align: center;">' . number_format($gnfirm_revenue, 2) . '</td>
                  </tr>';
          }

          $get_total = PayrollComputation::where(function($query) use ($id) {
                                    $query->where('batch_id', $id)
                                          ->where('status', 1);
                                  })->whereIn('user_id', array_unique($get_no_group))->sum('firm_revenue');

          $rows .= '<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td style="color: #ffffff;">-</td>
                </tr>';

          $rows .= '<tr>
                  <td><strong>TOTAL</strong></td>
                  <td></td>
                  <td></td>
                  <td style="text-align: center;"><strong>' . number_format($get_total, 2) . '</strong></td>
                </tr>';

          $rows .= '</tbody></table><br>';
        }
      }


      $result['rows'] = $rows;

      // return view('payroll-management.pdf.view-firm-revenue', $result);

      $pdf = PDF::loadView('payroll-management.pdf.view-firm-revenue', $result);
      return $pdf->setPaper('A4')->setOrientation('landscape')->download($get_batch->name . '.pdf');

    }

}