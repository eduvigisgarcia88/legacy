<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use Request;
use Session;
use Input;
use Validator;
use Response;
use Image;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Settings;

class ProfileController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    
    /**
     * Update model
     *
     */
    public function save() {   

        $input = Input::all();
        $pass_score = 0;
        if (Input::get('pass_score')) {
            $pass_score = Input::get('pass_score');
        }
        $password = Input::get('password');

        // get the user info
        $row = User::where('status', 1)->find(Auth::user()->id);

        if(!$row) {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }

        $rules = [
            'photo' => 'image|max:3000',
            'code' => 'required|min:2|max:100',
            'password' => 'min:8|confirmed',
            'name' => 'required|min:2|max:100',
            'email' => 'required|email|unique:users,email,' . $row->id,
            'mobile' => 'required|min:2|max:100',
        ];

        // field name overrides
        $names = [
            'name' => 'real name',
            'usertype_id' => 'designation',
            'system_id' => 'user id',
            'code' => 'user code',
        ];

        if ($pass_score <= 50 && (strlen($password) > 0)) {
            return Response::json(['error' => ["error-pass_score|*Password must be at least <b><u>MEDIUM</u></b>"]]);
        } else if (strlen($password) > 0) {
            if(preg_match("/^(?=.*[A-Z])(?=.*\d)([0-9a-zA-Z]+)$/", $password) == 0 && preg_match('`[A-Z]`', $password) && preg_match('`[0-9]`', $password)) { 

            } else {
              return Response::json(['error' => ["error-pass_score|*Password must contain at least one uppercase, lowercase, numeric and special character."]]);
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $row->code      = array_get($input, 'code');
        $row->name      = strip_tags(array_get($input, 'name'));
        $row->email     = array_get($input, 'email');
        $row->mobile    = array_get($input, 'mobile');
        $row->username  = array_get($input, 'email');

        // do not change password if old user and field is empty
        if(!empty(array_get($input, 'password'))) {
            $row->password = Hash::make(array_get($input, 'password'));
        }
        // save model
        $row->save();
       
        // Save the photo if exists
        if (Request::hasFile('photo')) {
            Image::make(Request::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
            $row->photo = $row->id . '.jpg';
            $row->save();
        }else{
            if(array_get($input, 'image_link') != "")
          {

              $img = file_get_contents(array_get($input, 'image_link'));
              Image::make($img)->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
              $row->photo = $row->id . '.jpg';
              $row->save();

          }
        }  

        // return
        return Response::json(['body' => 'Profile successfully updated.', 'photo' => $row->photo, 'username' => $row->username]);
    }
	

	/**
	* Saving the changes.
	*
	* @return Response
	*/
	public function info() {
        $user = Auth::user();
        return response()->json($user);
	}

    /**
    * Refresh navbar.
    *
    * @return Response
    */
    public function doInfo() {
        
        $rows = User::find(Auth::user()->id);

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

}