<?php

namespace App\Http\Controllers;

use Request;
use Excel;
use Response;
use App\UploadFeed;
use Auth;
use Carbon;
use View;
use Validator;
use Input;
use Config;
use Paginator;
use DB;

use App\User;
use App\Permission;
use App\Policy;
use App\PolicyContract;
use App\FeedType;
use App\ProviderCategory;
use App\ProviderCodes;
use App\ProviderClassification;
use App\ProviderFeed;
use App\SalesBonding;
use App\BatchPolicy;
use App\PrePayroll;
use App\PrePayrollComputation;
use App\Introducer;
use App\AssignedPolicy;
use App\PayrollComputation;
use App\Batch;
use App\BatchMonth;
use App\BatchMonthUser;
use App\BatchMonthSupervisor;
use App\BatchMonthGroup;
use App\Group;
use App\Sales;
use App\SalesDesignation;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\Settings;
use App\GroupComputation;
use App\SalesSupervisorComputation;
use App\SalesAdvisorComputation;
use App\Product;
use App\ProductGross;
use App\Provider;
use App\DataFeed;
use App\Http\Requests;
use Redirect;
use Crypt;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;

class PrintPreviewController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    /**
     * Build the list
     *
     */
    public function viewBatchExpand() {

      $this->data['batch'] = Request::input('batch');
      $this->data['table'] = Request::input('table');

      $get_batch = Batch::find(Request::input('id'));

      $start = Carbon::createFromFormat('Y-m-d', substr($get_batch->start, 0, 10));
      $end = Carbon::createFromFormat('Y-m-d', substr($get_batch->end, 0, 10));

      $samp_start = $start->copy();
      $samp_end = $end->copy();

      $try_start = $samp_start->startOfMonth();
      $try_end = $samp_end->endOfMonth();
      $try_diff = $try_start->diffInMonths($try_end);

      $diff = $start->diffInMonths($end);

      $dates = [];
      $id = 0;
      $table = '';

      while ($start->lte($end)) {

        $agent_commission = 0;
        $date_period = $start->copy();
        $format_end = $end->format('Y-m-d');
        $total_comm = 0;
        $total_incentives = 0;
        $total_deductions = 0;
        $total_overrides = 0;
        $total_intro = 0;

        if ($id == $diff) {

          if (Auth::user()->usertype_id == 8) {

            $agent_commission = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();
            if ($check_rank->designations->tier == 3) {

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;

                $get_group = GroupComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

                if ($get_group) {

                  $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                                    $query->where('group_id', '=', $get_group->id)
                                                          ->where('batch_id', '=', Request::input('id'));
                                              })->get();

                  foreach ($get_sups_id as $gpikey => $gpifield) {
                    
                    $ids[$ctr] = $gpifield->user_id;
                    $ctr += 1;

                    $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                                $query->where('supervisor_id', '=', $gpifield->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                    foreach ($get_advs_ids as $gaikey => $gaifield) {
                      $ids[$ctr] = $gaifield->user_id;
                      $ctr += 1;
                    }
                  }
                }

                $ids = array_unique($ids);

                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

            } else if ($check_rank->designations->rank == "Supervisor") {
              $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

              if ($get_sup_id) {
                $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;
                foreach ($get_adv_ids as $advkey => $advfield) {
                  $ids[$ctr] = $advfield->user_id;
                  $ctr += 1;
                }

                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

              } else {
                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');
                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');
                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
              }

            } else if ($check_rank->designations->rank == "Advisor") {
              $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

              $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

              $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
            }
          } else {
            $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');

            $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');

            $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('intro_com');

          }

        } else {

          if (Auth::user()->usertype_id == 8) {

            $agent_commission = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

            $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();
            if ($check_rank->designations->tier == 3) {

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;

                $get_group = GroupComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

                if ($get_group) {

                  $get_sups_id = SalesSupervisorComputation::where(function($query) use ($get_group) { 
                                                    $query->where('group_id', '=', $get_group->id)
                                                          ->where('batch_id', '=', Request::input('id'));
                                              })->get();

                  foreach ($get_sups_id as $gpikey => $gpifield) {
                    
                    $ids[$ctr] = $gpifield->user_id;
                    $ctr += 1;

                    $get_advs_ids = SalesAdvisorComputation::where(function($query) use ($gpifield) { 
                                                $query->where('supervisor_id', '=', $gpifield->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                    foreach ($get_advs_ids as $gaikey => $gaifield) {
                      $ids[$ctr] = $gaifield->user_id;
                      $ctr += 1;
                    }
                  }
                }

                $ids = array_unique($ids);
                
                $total_comm = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');

            } else if ($check_rank->designations->rank == "Supervisor") {
              $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

              if ($get_sup_id) {
                $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

                $ids = [];
                $ids[0] = Auth::user()->id;
                $ctr = 1;
                foreach ($get_adv_ids as $advkey => $advfield) {
                  $ids[$ctr] = $advfield->user_id;
                  $ctr += 1;
                }

                $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('status', '=', 1);
                                        })->whereIn('user_id', $ids)->sum('intro_com');
              } else {
                $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

                $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

                $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
              }

            } else if ($check_rank->designations->rank == "Advisor") {
              $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('agent_banding');

              $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('manage_share');

              $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                        $query->where('batch_id', '=', Request::input('id'))
                                              ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                              ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                              ->where('user_id', '=', Auth::user()->id)
                                              ->where('status', '=', 1);
                                        })->sum('intro_com');
            }
          } else {
            $total_comm = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
            $total_overrides = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('manage_share');
            $total_intro = PayrollComputation::where(function($query) use ($date_period) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                            ->where('upload_date', '<=', $date_period->endOfMonth() . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->sum('intro_com');
          }

          $format_end = $date_period->endOfMonth()->format('Y-m-d');
        }

        if (Auth::user()->usertype_id == 8) {
          $total_incentives = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('type', '=', 'Incentives');
                                    })->sum('cost');

          $total_deductions = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('user_id', '=', Auth::user()->id)
                                                  ->where('type', '=', 'Deductions');
                                    })->sum('cost');
        } else {
          $total_incentives = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('type', '=', 'Incentives');
                                    })->sum('cost');

          $total_deductions = PrePayrollComputation::where(function($query) use ($get_batch) {
                                            $query->where('batch_id', '=', $get_batch->id)
                                                  ->where('type', '=', 'Deductions');
                                    })->sum('cost');
        }

        if ($id == 0 && $diff == 0) {
          if ($try_diff == 0) {
            $format_end = $end->format('Y-m-d');
          } else {
            $format_end = $date_period->startOfMonth()->endOfMonth()->format('Y-m-d');
          }
        } else if ($id == 1 && $diff == 0) {
          $format_end = $end->format('Y-m-d');
        }

        $format_date = $start->copy()->format('Y-m-d');

        if (Auth::user()->usertype_id == 8) {
          $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

          if ($check_rank->designations->rank == "Advisor") { 
            $total_overrides = 0;
          }


          $total_intro = PayrollComputation::where(function($query) use ($date_period, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $date_period->format('Y-m-d') . " 00:00:00")
                                          ->where('upload_date', '<=', $end->format('Y-m-d') . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->sum('intro_com');

          // $total_comm = $agent_commission;
        }

        $dates[$id] = array_add(['total_comm' => number_format($total_comm, 2, '.', ''),
                                  'introducer' => number_format($total_intro, 2),
                                  'advisor' => number_format(($total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2),
                                  'gross' => number_format(($total_intro + $total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2),
                                  'incentives' => number_format($total_incentives, 2, '.', ''), 
                                  'deductions' => number_format($total_deductions, 2, '.', ''), 
                                  'date' => $format_date, 'end' => $format_end], 'id', $id);

        $table .= '<tr>' .
                    '<td>' . date_format(date_create(substr($format_date, 0,10)),'M Y') . '</td>' .
                    '<td>' . number_format(($total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2) . '</td>' .
                    '<td>' . number_format($total_intro, 2) . '</td>' .
                    '<td>' . number_format(($total_intro + $total_comm + ($total_incentives - $total_deductions) + $total_overrides), 2) . '</td>' .
                  '</tr>';


        $start->startOfMonth();
        $start->addMonth();

        $id += 1;

      }

      // $table .= '</tbody>' .
      //           '</table><br></div>';

      $result['rows'] = $table;

      //$result['advisor'] = $advisor;
      $result['batch_id'] = Request::input('id');

      return View::make('payroll-management.pdf.view-batch-expand', $result)->render();

    }


    public function viewAdvisorItemExpand() {

      $id = Request::input('id');
      $start = Request::input('start');
      $end = Request::input('end');

      $commissions = 0;
      $overrides = 0;
      $incentives = 0;
      $deductions = 0;

      if (Auth::user()->usertype_id == 8) {
        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {
          $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if ($get_sup_id) {
            $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

            $ids = [];
            $ids[0] = Auth::user()->id;
            $ctr = 1;
            foreach ($get_adv_ids as $advkey => $advfield) {
              $ids[$ctr] = $advfield->user_id;
              $ctr += 1;
            }

            $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('agent_banding');

            $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->sum('manage_share');

          } else {
            $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->sum('agent_banding');

            $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->sum('manage_share');
          }

        } else if ($check_rank->designations->rank == "Advisor") { 
          $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('user_id', '=', Auth::user()->id)
                                        ->where('status', '=', 1);
                                  })->sum('agent_banding');

          $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                  $query->where('batch_id', '=', $id)
                                        ->where('upload_date', '>=', $start . " 00:00:00")
                                        ->where('upload_date', '<=', $end . " 00:00:00")
                                        ->where('user_id', '=', Auth::user()->id)
                                        ->where('status', '=', 1);
                                  })->sum('manage_share');
        }

        $incentives = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Incentives")
                                                  ->where('user_id', '=', Auth::user()->id);
                                    })->sum('cost');

        $deductions = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Deductions")
                                                  ->where('user_id', '=', Auth::user()->id);
                                    })->sum('cost');

      } else {
        $commissions = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                $query->where('batch_id', '=', $id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('status', '=', 1);
                        })->sum('agent_banding');

        $overrides = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                $query->where('batch_id', '=', $id)
                                      ->where('upload_date', '>=', $start . " 00:00:00")
                                      ->where('upload_date', '<=', $end . " 00:00:00")
                                      ->where('status', '=', 1);
                        })->sum('manage_share');

        $incentives = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Incentives");
                                    })->sum('cost');

        $deductions = PrePayrollComputation::where(function($query) use ($id) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('type', '=', "Deductions");
                                    })->sum('cost');
      }
      
      if (Auth::user()->usertype_id == 8) {
        $check_group = Group::where('user_id', '=', Auth::user()->id)->first();
        if ($check_group) {
          $check_month = BatchMonthGroup::where(function($query) use ($id, $start, $end) {
                                            $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('start', '=', $start . " 00:00:00")
                                            ->where('end', '=', $end . " 00:00:00");
                                    })->first();
          if ($check_month) {
            $commissions = $check_month->revenue;
            $overrides = $check_month->overrides;
            $incentives = $check_month->incentives;
            $deductions = $check_month->deductions;
          }
        }
      }

      $rows = '<table class="table table-bordered table-condensed table-striped">' .
                      '<thead class="tbheader">' .
                        '<tr>' .
                          '<th> MONTHLY BREAKDOWN</th>' .
                          '<th> AMOUNT</th>' .
                        '</tr>' .
                    '</thead>' .
                    '<tbody>' .
                  '<tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '">REVENUE</td>' .
                  '<td>'  . number_format($commissions, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '">OVERRIDES</td>' .
                  '<td>'  . number_format($overrides, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '">INCENTIVES</td>' .
                  '<td>'  . number_format($incentives, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td data-id="' . $end . '">DEDUCTIONS</td>' .
                  '<td>'  . number_format($deductions, 2) .  '</td>' .
                  '</tr><tr data-id="' . $id . '">' .
                  '<td>TOTAL</td>' .
                  '<td>' . number_format($commissions + $overrides + ($incentives - $deductions), 2) .  '</td></tr>' .
                  '</tbody>' .
                  '</table>';

        if (Auth::user()->usertype_id == 8) {
          $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

          if ($check_rank->designations->rank == "Advisor") { 
           $rows = '<table class="table table-bordered table-condensed table-striped">' .
                          '<thead class="tbheader">' .
                            '<tr>' .
                              '<th> MONTHLY BREAKDOWN</th>' .
                              '<th> AMOUNT</th>' .
                            '</tr>' .
                        '</thead>' .
                        '<tbody>' .
                      '<tr data-id="' . $id . '">' .
                      '<td data-id="' . $end . '">REVENUE</td>' .
                      '<td>'  . number_format($commissions, 2) .  '</td>' .
                      '</tr><tr data-id="' . $id . '">' .
                      '<td data-id="' . $end . '">INCENTIVES</td>' .
                      '<td>'  . number_format($incentives, 2) .  '</td>' .
                      '</tr><tr data-id="' . $id . '">' .
                      '<td data-id="' . $end . '">DEDUCTIONS</td>' .
                      '<td>'  . number_format($deductions, 2) .  '</td>' .
                      '</tr><tr data-id="' . $id . '">' .
                      '<td>TOTAL</td>' .
                      '<td>' . number_format($commissions + ($incentives - $deductions), 2) .  '</td></tr>' .
                      '</tbody>' .
                      '</table>';
          }

        }

      $result['rows'] = $rows;

      //$result['advisor'] = $advisor;
      $result['batch_id'] = Request::input('id');

      return View::make('payroll-management.pdf.view-advisor-item-expand', $result)->render();
    }


    public function viewAdvisorItemSubExpandRevenue() {

      $id = Request::input('id');
      $item = Request::input('item');
      $start = Request::input('start');
      $end = Request::input('end');
      $commission = 0;

      if (Auth::user()->usertype_id == 8) {

        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {
          $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if ($get_sup_id) {
            $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

            $ids = [];
            $ids[0] = Auth::user()->id;
            $ctr = 1;
            foreach ($get_adv_ids as $advkey => $advfield) {
              $ids[$ctr] = $advfield->user_id;
              $ctr += 1;
            }

            $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('status', '=', 1);
                                    })->whereIn('user_id', $ids)->lists('provider_id');
          } else {
            $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id');
          }

        } else if ($check_rank->designations->rank == "Advisor") {
          $get_provider = PayrollComputation::where(function($query) use ($start, $end) {
                                    $query->where('batch_id', '=', Request::input('id'))
                                          ->where('upload_date', '>=', $start . " 00:00:00")
                                          ->where('upload_date', '<=', $end . " 00:00:00")
                                          ->where('user_id', '=', Auth::user()->id)
                                          ->where('status', '=', 1);
                                    })->lists('provider_id');
        }

      } else {
        $get_provider = PayrollComputation::where(function($query) use ($id, $start, $end) {
                                            $query->where('batch_id', '=', $id)
                                                  ->where('upload_date', '>=', $start . " 00:00:00")
                                                  ->where('upload_date', '<=', $end . " 00:00:00")
                                                  ->where('status', '=', 1);
                                    })->lists('provider_id');
      }
      

      $no_team = false;
      
      if (Auth::user()->usertype_id == 8) {
        $check_no_team = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();
        if (!$check_no_team) {
          $check_no_adv_team = SalesAdvisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

          if (!$check_no_adv_team) {
            $no_team = true;
          }
        }
      }

      $rows = '<div class="item-table"><table class="table table-bordered table-condensed table-striped">' .
              '<thead>' .
              '<th class="tbheader"> Provider Name' . ( $no_team == true ? ' <small style="font-weight: normal;">(user has no team)</small>' : '' ) . '</th>' .
              '<th class="tbheader"> Total</th></thead><tbody>';

      foreach (array_unique($get_provider->toArray()) as $ikey => $field) {

        if (Auth::user()->usertype_id == 8) {

        $check_rank = Sales::with('designations')->where('user_id', '=', Auth::user()->id)->first();

        if ($check_rank->designations->rank == "Supervisor") {

          $check_tier3 = Group::where('user_id', '=', Auth::user()->id)->first();

          if ($check_tier3) {

            $check_group = GroupComputation::where(function($query) use ($id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('user_id', '=', Auth::user()->id);
                                    })->get();
            $gids = [];
            $gctr = 0;

            foreach ($check_group as $cgkey => $cgfield) {

              $get_supervisors = SalesSupervisorComputation::where(function($query) use ($id, $cgfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->get(); 
              
              foreach ($get_supervisors as $gskey => $gsfield) {
                $gids[$gctr] = $gsfield->user_id;
                $gctr += 1;

                $get_advisors = SalesAdvisorComputation::where(function($query) use ($id, $cgfield, $gsfield) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('supervisor_id', '=', $gsfield->id)
                                            ->where('group_id', '=', $cgfield->id);
                                    })->lists('user_id');

                foreach (array_unique($get_advisors->toArray()) as $gakey => $gafield) {
                  $gids[$gctr] = $gafield;
                  $gctr += 1;
                }
              }
            }

            $commission = PayrollComputation::where(function($query) use ($field, $start, $end, $id) {
                                      $query->where('batch_id', '=', $id)
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $gids)->sum('agent_banding');
          } else {
            $get_sup_id = SalesSupervisorComputation::where(function($query) { 
                                                $query->where('user_id', '=', Auth::user()->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->first();

            if ($get_sup_id) {

              $get_adv_ids = SalesAdvisorComputation::where(function($query) use ($get_sup_id) { 
                                                $query->where('supervisor_id', '=', $get_sup_id->id)
                                                      ->where('batch_id', '=', Request::input('id'));
                                          })->get();

              $ids = [];
              $ids[0] = Auth::user()->id;
              $ctr = 1;
              foreach ($get_adv_ids as $advkey => $advfield) {
                $ids[$ctr] = $advfield->user_id;
                $ctr += 1;
              }

              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('status', '=', 1);
                                      })->whereIn('user_id', $ids)->sum('agent_banding');
            } else {
              $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
            }
          }

          } else if ($check_rank->designations->rank == "Advisor") {
            $commission = PayrollComputation::where(function($query) use ($field, $start, $end) {
                                      $query->where('batch_id', '=', Request::input('id'))
                                            ->where('provider_id', '=', $field)
                                            ->where('upload_date', '>=', $start . " 00:00:00")
                                            ->where('upload_date', '<=', $end . " 00:00:00")
                                            ->where('user_id', '=', Auth::user()->id)
                                            ->where('status', '=', 1);
                                      })->sum('agent_banding');
          }

        } else {
          $commission = PayrollComputation::where(function($query) use ($id, $field, $start, $end) {
                                          $query->where('batch_id', '=', $id)
                                                ->where('provider_id', '=', $field)
                                                ->where('upload_date', '>=', $start . " 00:00:00")
                                                ->where('upload_date', '<=', $end . " 00:00:00")
                                                ->where('status', '=', 1);
                                  })->sum('agent_banding');
        }

        $get_name = Provider::find($field);

        $rows .= '<tr data-id="' . $id . '">' . 
                  '<td data-id="' . $end . '">' . $get_name->name . '</td>' .
                  '<td>' . number_format($commission, 2) . '</td>' .
                  '</tr>';

      }
      
      $rows .= '</tbody></table><br></div>';

      $result['rows'] = $rows;

      //$result['advisor'] = $advisor;
      $result['batch_id'] = Request::input('id');

      return View::make('payroll-management.pdf.view-advisor-item-expand', $result)->render();
    }

}