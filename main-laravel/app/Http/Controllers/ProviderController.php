<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Validator;
use Input;
use Carbon;
use App\LogProvider;
use Auth;
use Paginator;
use App\Permission;
use App\Provider;
use App\PrePayroll;
use App\ProviderCategory;
use App\ProviderProperty;
use App\ProviderClassification;
use App\ProviderFeed;
use App\Http\Requests;
use App\Sales;
use Redirect;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;
use App\Settings;

class ProviderController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    
    /**
     * Display a listing of the provider.
     *
     * @return Response
     */
    public function index() {
      $result = $this->doList();

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['title'] = "Provider List";
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                              ->select('sales.*','designations.*')
                                              ->where('sales.user_id','=',Auth::user()->id)
                                              ->first();
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

      // for notification
     if ( Auth::user()->usertype_id == '8') {
      $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                                ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                                ->select('users.*','notifications_per_users.*','notifications.*')
                                                ->where(function($query) {
                                                      $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                            ->where('notifications.usertype_id', '=', '1')
                                                            ->where('notifications_per_users.is_read', '=', '0');
                                                })->get();
      } else {
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                                  ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                                  ->select('users.*','notifications_per_users.*','notifications.*')
                                                  ->where(function($query) {
                                                        $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                              ->where('notifications.usertype_id', '=', '2')
                                                              ->where('notifications_per_users.is_read', '=', '0');
                                                  })->get();
       }   

      $provider  = Provider::orderBy('id', 'desc')->first();
      $this->data['providerclassification'] = ProviderProperty::join('products', 'provider_property.provider_id', '=', 'products.id')
                                        ->select('products.*','provider_property.*')
                                      ->where(function($query) {
                                            $query->where('provider_property.provider_id', '==', '$provider->id');
                                       })->count();

      $this->data['settings'] = Settings::first();
      $this->data['noti_count'] = count($this->data['notification']);
      return view('provider-product-management.provider.list', $this->data);
    }

    public function doList() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'asc';
      $search = Request::input('search');
      $per = Request::input('per') ?: 10;
      
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Provider::where(function($query) {
                            $query->where('status', '!=', '2');
                          })
                        ->where(function($query) use ($search) {
                          $query->where('code', 'LIKE', '%' . $search . '%')
                                ->orWhere('name', 'LIKE', '%' . $search . '%');
                          })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      } else {
        $count = Provider::where(function($query) {
                            $query->where('status', '!=', '2');
                          })
                        ->where(function($query) use ($search) {
                          $query->where('code', 'LIKE', '%' . $search . '%')
                                ->orWhere('name', 'LIKE', '%' . $search . '%');
                          })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Provider::where(function($query) {
                            $query->where('status', '!=', '2');
                          })
                        ->where(function($query) use ($search) {
                          $query->where('code', 'LIKE', '%' . $search . '%')
                                ->orWhere('name', 'LIKE', '%' . $search . '%');
                          })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    public function view() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Provider::find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Save model
     *
     */
    public function save() {

      $new = true;

      $input = Input::all();

      // check if an ID is passed
      if(array_get($input, 'id')) {

          // get the user info
          $row = Provider::find(array_get($input, 'id'));

          if(is_null($row)) {
              return Response::json(['error' => "The requested item was not found in the database."]);
          }
          // this is an existing row
          $new = false;
      }
      $rules = [
          'code' => 'required|max:100',
          'name' => 'required|min:2|max:100',
          'description' => 'required|min:2|max:1000',
      ];

      // field name overrides
      $names = [
          // 'name' => 'provider name',
          // 'code' => 'user code',
          // 'description' => 'description',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names);

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      // create model if new
      if($new) {
        $row = new Provider;
      }

      $row->code      = array_get($input, 'code');
      $row->name      = array_get($input, 'name');
      $row->description    = array_get($input, 'description');

      // save model
      $row->save();
      
      // $life = new ProviderCategory;
      // $life->provider_id = $row->id;
      // $life->name = "Life";
      // $life->status = 0;
      // // $life->code = "L";

      // $life->save();

      // // $health = new ProviderCategory;
      // // $health->provider_id = $row->id;
      // // $health->name = "Health";
      // // $health->code = "H";
      // // $health->save();



      // if ($row->id == 1) {
      //   $feeds = new ProviderFeed;
      //   $feeds->provider_id = $row->id;
      //   $feeds->feed_id = 1;
      //   $feeds->save();
      // }
      
      // $feed = new ProviderFeed;
      // $feed->provider_id = $row->id;
      // $feed->feed_id = 2;
      // $feed->save();

      // return
      if ($new) {
        return Response::json(['body' => 'Provider created successfully.']);
      } else {
        return Response::json(['body' => 'Provider updated successfully.']);
      }
    }

    /**
     * Deactivate the user
     *
     */
    public function disable() {
      $row = Provider::find(Request::input('id'));

      // check if user exists
      if(!is_null($row)) {
        //if users tries to disable itself
       
        $row->status = 0;
        $row->save();

        // return
        return Response::json(['body' => 'Provider has been deactivated.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    /**
     * Activate the user
     *
     */
    public function enable() {
      $row = Provider::find(Request::input('id'));

      // check if user exists
      if(!is_null($row)) {
          //if users tries to disable itself
         
              $row->status = 1;
              $row->save();

          // return
          return Response::json(['body' => 'Provider has been activated.']);
      } else {
          // not found
          return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    /**
     * Disable multiple user
     *
     */
    public function disableSelected() {
      $ids = Request::input('ids');
     
      if (count($ids) == 0) {
        return Response::json(['error' => 'Select providers first.']); 
      } else {
        foreach ($ids as $id) {
            $row = Provider::find($id);
            if(!is_null($row)) {
                
                $status = $row->status;
                // disable only the active users.
                if ($status == 1){
                  $row->status = 0;
                  $row->save();
                 
                }
            }
        }
        return Response::json(['body' => 'Selected providers has been Deactivated.']);
      }
    }  

    /**
     * Remove multiple user
     *
     */
    public function remove() {

      $ids = Request::input('id');
      $providerclassification = ProviderProperty::join('products', 'provider_property.provider_id', '=', 'products.id')
                                        ->select('products.*','provider_property.*')
                                      ->where(function($query) {
                                            $query->where('provider_property.status', '=', '1')
                                            ->where('provider_property.property_id', '=', Request::input('id'));
                                        })->get();

      if (!(count($providerclassification) == 0)) {
        return Response::json(['error' => 'Unable to remove the provider contains product.']); 
      } else {
        // process
        $row = Provider::find($ids);
        if(!is_null($row)) {
            $status = $row->status;
            // you cannot remove user that already been remove
            if ($status != 2){
              $row->status = 2;
              $row->save();
            } 
        }

        return Response::json(['body' => 'Selected providers has been Removed.']);
      }
    }


    public function getProviderProperty() {
      
      $providercategory = null;

      if (Auth::user()->usertype_id == 8) {
        $providercategory = ProviderCategory::join('products', 'provider_categories.provider_id', '=', 'products.id')
                                        ->select('products.*','provider_categories.*')
                                        ->where(function($query) {
                                            $query->where('provider_categories.provider_id', '=', Request::input('id'));
                                        })->get();
      } else {
        $providercategory = ProviderCategory::join('products', 'provider_categories.provider_id', '=', 'products.id')
                                        ->select('products.*','provider_categories.*')
                                        ->where(function($query) {
                                            $query->where('provider_categories.provider_id', '=', Request::input('id'));
                                        })->get();
      }

      $incentives = 0;
      $deductions = 0;

      // foreach ($providercategory as  $field) {

        $rows =  '<div class="type-table"><table class="table table-striped">' .
                  '<tbody>' .
                    '<tr>' .
                      '<td><a class="btn btn-table btn-xs btn-type-expand" data-property_id="1" data-id="' . Request::input('id') .  '"><i class="fa fa-plus"></i></a> Product Classification</td>' .
                      '<td>' . '</td>' .
                    '</tr>' .
                    '<tr>' .
                      '<td><a class="btn btn-table btn-xs btn-type-expand" data-property_id="2" data-id="' . Request::input('id') .  '"><i class="fa fa-plus"></i></a> Product Category </td>' .
                    '<td></td>' .
                    '</tr>' . 
                  '</tbody></table></div>';
      // }
          

      if (Request::input('id')) {
        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }
   
    public function getProviderProduct() {

      $property = Request::input('property_id');
        // dd(ProviderClassification::all());
      if ($property == 1) {
         $providerclassification = ProviderClassification::select('provider_classifications.*')
                                      ->where(function($query) {
                                          $query->where('provider_classifications.provider_id', '=', Request::input('id'))
                                                ->where('provider_classifications.status', '=', '1');
                                      })->get();
      }
      if ($property == 2) {
        $providerclassification = ProviderCategory::select('provider_categories.*')
                                      ->where(function($query) {
                                          $query->where('provider_categories.provider_id', '=', Request::input('id'))
                                                ->where('provider_categories.status', '=', '1');
                                      })->get();                            
      }

      $rows = '<div class="type-table"><table class="table table-striped">' .  
              '<thead class="tbheader">
                  <tr>
                    <!--<th><i class="fa fa-sort"></i> </th>-->
                    <th colspan="4"><i class="fa fa-sort"></i> TITLE</th>
                    <th><i class="fa fa-sort"></i> UPDATED AT</th>
                    <th><i class="fa fa-sort"></i> CREATED AT</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>'.  
              '<tbody>';

      foreach ($providerclassification as $row) {

        $rows .= '<tr data-id="' . $row->id . '">' . 
                  ( Auth::user()->usertype_id != 8 ? 
                  '<td colspan="4">' .$row->name. '</td>' . 
                  '<td>' .$row->updated_at. '</td>' . '<td>'  .$row->created_at. '</td>' .
                  '<td class="rightalign">' . 
                    '<button type="button" class="btn btn-xs btn-table btn-property-edit"><i class="fa fa-pencil"></i></button>&nbsp;'.  
                    ( $property == 1 ? '<button type="button" class="btn btn-xs btn-table btn-delete-classification"><i class="fa fa-trash"></i></button>' : '') .
                    ( $property == 2 ? '<button type="button" class="btn btn-xs btn-table btn-delete-category"><i class="fa fa-trash"></i></button>' : '' ) .
                  '</td>' : '' ) .
                  '</tr>'; 
      }

      $rows .= '</tbody></table></div>';

      if (Request::input('id')) {
        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function deleteClassification() {

      $id = Request::input('id');
      $row = ProviderClassification::find(Request::input('id'));

      if(!is_null($row)) {
            $row->status = 0;
            $row->save();
            // return
        return Response::json(['body' => 'Provider has been deleted.']);
      } else {
          // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function deleteCategory() {

      $id = Request::input('id');
      $row = ProviderCategory::find(Request::input('id'));

      if(!is_null($row)) {
        $row->status = 0;
        $row->save();

        // return
        return Response::json(['body' => 'Provider has been deleted.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function viewProperty() {

      $id = Request::input('id');
      $property_id = Request::input('property_id');

      if($property_id==1)
      {
        $row = ProviderClassification::find($id);  
      }
      if($property_id==2)
      {
        $row = ProviderCategory::find($id); 
      }

      if($row) {
          return Response::json($row);
      } else {
          return Response::json(['error' => "Invalid row specified"]);
      }
    }

    public function saveProperty() {

      $new = true;
      $input = Input::all();

      $property = array_get($input, 'property_id');

      if ($property == 1) {
        if (array_get($input, 'id')) {
          $row = ProviderClassification::find(array_get($input, 'id'));
          
          if(is_null($row)) {
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
          $new = false;
        }
      }

      if ($property==2) {
        if(array_get($input, 'id')) {
          $row = ProviderCategory::find(array_get($input, 'id'));
          
          if(is_null($row)) {
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
          $new = false;
        }
      }

      $rules = [
        'name' => 'required',
        'property_id' => 'required',
      ];

      // field name overrides
      $names = [

      ];

      $messages = [
        'property_id.required' => 'error-property_id|*Select value'
      ];  

      // do validation
      $validator = Validator::make(Input::all(), $rules, $messages);
      $validator->setAttributeNames($names);

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      if ($property == 1) {
        if ($new) {
          $row = new ProviderClassification;
          $row->provider_id = array_get($input, 'provider_id');
        }
        $row->name = array_get($input, 'name');

        if (array_get($input, 'name') === "General Insurance") {
          $row->is_gi = 1;
        } else {
          $row->is_gi = 0;
        }

        $row->save();
      }

      if ($property == 2) {
        if($new) {
          $row = new ProviderCategory;
          $row->provider_id  = array_get($input, 'provider_id');
        }
        $row->name = array_get($input, 'name');
        $row->save();
      }

      // return
      if ($new) {
      return Response::json(['body' => 'Provider created successfully.']);
       }
      else{
      return Response::json(['body' => 'Provider updated successfully.']);
         }
    }

    public function notification($id) {  
      $row = NotificationPerUser::where(function($query) use ($id) {
                                      $query->where('user_id', '=', Auth::user()->id)
                                            ->where('notification_id', '=', $id);
                                  })->update(['is_read' => 1]);

      return Redirect::to('/policy/production/case-submission');
    }

}
