<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Input;
use View;
use Excel;
use Validator;
use Carbon;
use Auth;
use Paginator;
use Storage;
use DB;

use App\Permission;
use App\User;
use App\Sales;
use App\Group;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\BatchMonthUser;
use App\GroupComputation;
use App\SalesAdvisorComputation;
use App\SalesSupervisorComputation;
use App\Designation;
use App\Nationality;
use App\Policy;
use App\Batch;
use App\PayrollComputation;
use App\Product;
use App\ProductGross;
use App\ProductRider;
use App\ProductMain;
use App\ProductionCaseContact;
use App\ProductionCaseRider;
use App\PolicyContract;
use App\Provider;
use App\ProviderCategory;
use App\AssignedPolicy;
use App\Introducer;
use App\LogPolicy;
use App\ProductionCases;
use App\GIProductionCases;
use App\UploadFeed;
use App\SettingsRate;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;
use App\Settings;
use Redirect;
use App\Inception;


class PolicyController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }


    public function checkBSC() {

      // dd(Request::input('id'));
      if (Request::input('id')) {
        $get_comp = PayrollComputation::where('status', 1)->where('batch_id', Request::input('id'))->count();
      } else {
        $get_comp = PayrollComputation::where('status', 1)->count();
      }

      if ($get_comp > 0) {

        if (Request::input('id')) {
          $get_first = PayrollComputation::where('status', 1)->where('batch_id', Request::input('id'))->orderBy('id', 'asc')->first();
        } else {
          $get_first = PayrollComputation::where('status', 1)->orderBy('id', 'asc')->first();
        }

        $this->data['count'] = $get_comp;
        $this->data['loop'] = ceil($get_comp / 100);
        $this->data['url'] = url('policy/bsc/save');
        $this->data['url_total'] = url('policy/bsc/done');
        $this->data['first'] = $get_first->id;

        $this->data['batch_id'] = Request::input('id');

        return Response::json($this->data);

      } else {
        return Response::json(['nopolicy' => ['Access Denied.']]);
      }

    }

    public function saveBSC() {

      $loop = Request::input('loop');

      if ($loop > 1) {
        if (Request::input('batch_id')) {

          $get_off_comp = PayrollComputation::where('status', 1)
                                        ->where('batch_id', Request::input('batch_id'))
                                        ->orderBy('id', 'asc')
                                        ->lists('id')->take(100 * ($loop - 1));

          $get_comp = PayrollComputation::where('status', 1)
                                        ->where('batch_id', Request::input('batch_id'))
                                        ->orderBy('id', 'asc')
                                        ->whereNotIn('id', $get_off_comp)
                                        ->get()->take(100);
        } else {
          $get_off_comp = PayrollComputation::where('status', 1)
                                        ->orderBy('id', 'asc')
                                        ->lists('id')->take(100 * ($loop - 1));

          $get_comp = PayrollComputation::where('status', 1)
                                        ->orderBy('id', 'asc')
                                        ->whereNotIn('id', $get_off_comp)
                                        ->get()->take(100);
        }
      } else {
        if (Request::input('batch_id')) {
          $get_comp = PayrollComputation::where('status', 1)
                                        ->where('batch_id', Request::input('batch_id'))
                                        ->orderBy('id', 'asc')
                                        ->get()->take(100);
        } else {
          $get_comp = PayrollComputation::where('status', 1)
                                        ->orderBy('id', 'asc')
                                        ->get()->take(100);

        }
      }

      foreach ($get_comp as $key => $row) {
        $get_main = Product::find($row->product_id);
        
        // $get_rev = ProductGross::where(function($query) use ($row) {
        //                                   $query->where('product_id', '=', $row->product_id);
        //                                 })->lists('value');

        // $frequency = 0;
        // if ($row->provider_id == 2) {
        //   if (strtolower($row->billing_freq) === "a") {
        //     $frequency = 1;
        //   } else if (strtolower($row->billing_freq) === "m") {
        //     $frequency = 12;
        //   } else if (strtolower($row->billing_freq) === "q") {
        //     $frequency = 3;
        //   }
        // } else {
        //   if (strtolower($row->billing_freq) === "yearly") {
        //     $frequency = 1;
        //   } else if (strtolower($row->billing_freq) === "monthly") {
        //     $frequency = 12;
        //   }
        // }


        // $var_income = 0;
        // foreach ($get_rev as $grkey => $grvalue) {
        //   $var_income += ($row->premium * $frequency) * ($grvalue / 100);
        // }

        // $row->variable_income = $var_income;
        // $row->spec_variable_income = $var_income * .60;
        // $row->save();

        // if ($row->category_id != 7 && $row->category_id != 8 && $row->category_id != 9) {
        //   $row->svi = $var_income * ($row->tier_per / 100);
        //   $row->save();
        // }

        // dd('code');

        if ($get_main) {

          $get_pcase = ProductionCases::where(function($query) use ($row) { 
                                          $query->where('fullname', $row->policy_holder)
                                                ->orWhere('fullname_first', $row->policy_holder);
                                        })
                                      ->where('main_product_id', $get_main->main_id)
                                      ->where('user_id', $row->user_id)
                                      ->whereNotNull('upload')
                                      ->first();

          $get_pcase1 = ProductionCases::where(function($query) use ($row) { 
                                          $query->where('fullname', $row->policy_holder)
                                                ->orWhere('fullname_first', $row->policy_holder);
                                        })
                                      ->where('user_id', $row->user_id)
                                      ->whereNotNull('upload')
                                      ->first();

          $get_pcase2 = ProductionCases::where('main_product_id', $get_main->main_id)
                                      ->where(function($query) use ($row) { 
                                          $query->where('fullname', $row->policy_holder)
                                                ->orWhere('fullname_first', $row->policy_holder);
                                        })
                                      ->whereNotNull('upload')
                                      ->first();
          
          if ($get_pcase) {
            $row->pfr_link = $get_pcase->upload;
            $row->save();
          } else if ($get_pcase1) {
            $row->pfr_link = $get_pcase1->upload;
            $row->save();
          } else if ($get_pcase2) {
            $row->pfr_link = $get_pcase2->upload;
            $row->save();
          }
        }
      }

      return Response::json("success");
    }

    public function doneBSC() {

      return Response::json("success");
    }

    public function exportOnGoing() {

      if (Request::input('fees') == 'ONGOING FEES') {
        $title = "ONGOING";
      } else if (Request::input('fees') == 'INITIAL FEES') {
        $title = "INITIAL";
      }

      $name = $title . '-' . Carbon::now()->format('Y-m-d');

      Excel::create($name, function($excel) use ($name) {
        $status = Request::input('export');

        $import = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('updated_at', 'desc')->first();

        $status = Request::input('status') ?: '';
        if ($status) {
          $ids = PayrollComputation::where('batch_id', $status)->lists('policy_id');
        } else {
          $ids = PayrollComputation::lists('policy_id');
        }

        if (Auth::user()->usertype_id == 8) {

            $rows = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->where(function($query) use ($import) {
                                            $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                          })
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy('agent_name', 'asc')
                                        ->get();

        } else {
            $rows = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy('agent_name', 'asc')
                                        ->get();
        }

        $excel->sheet($name, function($sheet) use ($rows) {

            $row_cell = 1;
              $sheet->appendRow(array(
                'SALESFORCE ID',
                'AGENT NAME',
                'POLICY OWNER',
                'PREMIUM',
                'REVENUE',
                'MONTH',
                'INCEPTION DATE',
              ));

              $sheet->cell('A' . $row_cell . ':G' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            foreach ($rows as $key => $row) {
              
              $row_cell += 1;

              $sheet->appendRow(array(
                  strtoupper($row->salesforceid),
                  strtoupper($row->agent_name),
                  strtoupper($row->policy_owner),
                  number_format($row->premium, 2),
                  number_format($row->revenue, 2),
                  ($row->month ? $row->month : ''),
                  ($row->inception_date ? date_format(date_create(substr($row->inception_date, 0,10)),'d/m/Y') : ''),
              ));

              $sheet->cell('A' . $row_cell . ':G' . $row_cell, function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            }

        });

      })->download('xls');

    }


    public function indexOngoing() {

        $result = $this->doListOngoing();
        $this->data['title'] = "Ongoing Fees Report";

        $url = Request::url();
        if (strpos($url, 'ongoing')) {
          $this->data['breadcrumb'] = "ONGOING FEES REPORT";
        } else if (strpos($url, 'initial')) {
          $this->data['breadcrumb'] = "INITIAL FEES REPORT";
        }

        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['batches'] = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('updated_at', 'desc')->get();

        $import = Batch::where(function($query) {
                              $query->where('status', 1)
                                    ->orWhere('status', 0);
                          })->orderBy('updated_at', 'desc')->first();

        $this->data['export'] = null;
        if ($import) {
          $this->data['export'] = $import->id;
        }

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
         $this->data['noti_count'] = count($this->data['notification']);
           $this->data['settings'] = Settings::first();
        return view('summary-and-reports.ongoing-fees-report.list', $this->data);

    }

    public function doListOngoing() {

      // sort and order
      $result['sort'] = Request::input('sort') ?: 'payroll_computations.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $per = Request::input('per') ?: 10;
      $status = Request::input('status');

      $import = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('updated_at', 'desc')->first();

        $status = Request::input('status') ?: '';
        if ($status) {
          $ids = PayrollComputation::where('batch_id', $status)->lists('policy_id');
        } else {
          $ids = PayrollComputation::lists('policy_id');
        }

        $paginate = '';

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          if (Auth::user()->usertype_id == 8) {

            $rows = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->where(function($query) use ($search) {
                                            $query->where('sales_user_extension.salesforce_id', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%');
                                          })
                                        ->where(function($query) use ($import) {
                                            $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                          })
                                        ->where(function($query) use ($status) {
                                            if ($status) {
                                              $query->where('payroll_computations.batch_id', $status);
                                            }
                                          })
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);

            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr><td>' . strtoupper($row->salesforceid) . '</td>' .
                    '<td>' . strtoupper($row->agent_name) . '</td>' .
                    '<td>' . strtoupper($row->policy_owner) . '</td>' .
                    '<td class="rightalign">' . number_format($row->premium, 2) . '</td>' .
                    '<td class="rightalign">' . number_format($row->revenue, 2) . '</td>' .
                    '<td>' . ($row->month ? $row->month : '') . '</td>' .
                    '<td>' . ($row->inception_date ? date_format(date_create(substr($row->inception_date, 0,10)),'d/m/Y') : '') . '</td></tr>';
              }
            }

          } else {

            $rows = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->where(function($query) use ($search) {
                                            $query->where('sales_user_extension.salesforce_id', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%');
                                          })
                                        ->where(function($query) use ($status) {
                                            if ($status) {
                                              $query->where('payroll_computations.batch_id', $status);
                                            }
                                          })
                                        ->selectRaw('sum(agent_banding + premium) as overall_total')
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);

            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr><td>' . strtoupper($row->salesforceid) . '</td>' .
                    '<td>' . strtoupper($row->agent_name) . '</td>' .
                    '<td>' . strtoupper($row->policy_owner) . '</td>' .
                    '<td class="rightalign">' . number_format($row->premium, 2) . '</td>' .
                    '<td class="rightalign">' . number_format($row->revenue, 2) . '</td>' .
                    '<td>' . ($row->month ? $row->month : '') . '</td>' .
                    '<td>' . ($row->inception_date ? date_format(date_create(substr($row->inception_date, 0,10)),'d/m/Y') : '') . '</td></tr>';
              }
            }
          }
        } else {
          if (Auth::user()->usertype_id == 8) {

            $count = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->where(function($query) use ($search) {
                                            $query->where('sales_user_extension.salesforce_id', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%');
                                          })
                                        ->where(function($query) use ($import) {
                                            $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                          })
                                        ->where(function($query) use ($status) {
                                            if ($status) {
                                              $query->where('payroll_computations.batch_id', $status);
                                            }
                                          })
                                        ->selectRaw('sum(agent_banding + premium) as overall_total')
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);
          } else {
            
            $count = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->where(function($query) use ($search) {
                                            $query->where('sales_user_extension.salesforce_id', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%');
                                          })
                                        ->where(function($query) use ($status) {
                                            if ($status) {
                                              $query->where('payroll_computations.batch_id', $status);
                                            }
                                          })
                                        ->selectRaw('sum(agent_banding + premium) as overall_total')
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);

          }

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          if (Auth::user()->usertype_id == 8) {

            $rows = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->where(function($query) use ($search) {
                                            $query->where('sales_user_extension.salesforce_id', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%');
                                          })
                                        ->where(function($query) use ($import) {
                                            $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                          })
                                        ->where(function($query) use ($status) {
                                            if ($status) {
                                              $query->where('payroll_computations.batch_id', $status);
                                            }
                                          })
                                        ->selectRaw('sum(agent_banding + premium) as overall_total')
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);

            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr><td>' . strtoupper($row->salesforceid) . '</td>' .
                    '<td>' . strtoupper($row->agent_name) . '</td>' .
                    '<td>' . strtoupper($row->policy_owner) . '</td>' .
                    '<td class="rightalign">' . number_format($row->premium, 2) . '</td>' .
                    '<td class="rightalign">' . number_format($row->revenue, 2) . '</td>' .
                    '<td>' . ($row->month ? $row->month : '') . '</td>' .
                    '<td>' . ($row->inception_date ? date_format(date_create(substr($row->inception_date, 0,10)),'d/m/Y') : '') . '</td></tr>';
              }
            }
            
          } else {
            
            
            $rows = PayrollComputation::select( 'payroll_computations.id', 'sales_user_extension.salesforce_id as salesforceid', 'users.name as agent_name',
                                        'payroll_computations.policy_holder as policy_owner', 'payroll_computations.premium', 'payroll_computations.agent_banding as revenue',
                                        'batches.name as month', 'batches.id as month_id', 'payroll_computations.incept_date as inception_date')
                                        ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                        ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                        ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                        ->where(function($query) use ($search) {
                                            $query->where('sales_user_extension.salesforce_id', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%');
                                          })
                                        ->where(function($query) use ($status) {
                                            if ($status) {
                                              $query->where('payroll_computations.batch_id', $status);
                                            }
                                          })
                                        ->selectRaw('sum(agent_banding + premium) as overall_total')
                                        ->groupBy('payroll_computations.id')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);

            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr><td>' . strtoupper($row->salesforceid) . '</td>' .
                    '<td>' . strtoupper($row->agent_name) . '</td>' .
                    '<td>' . strtoupper($row->policy_owner) . '</td>' .
                    '<td class="rightalign">' . number_format($row->premium, 2) . '</td>' .
                    '<td class="rightalign">' . number_format($row->revenue, 2) . '</td>' .
                    '<td>' . ($row->month ? $row->month : '') . '</td>' .
                    '<td>' . ($row->inception_date ? date_format(date_create(substr($row->inception_date, 0,10)),'d/m/Y') : '') . '</td></tr>';
              }
            }

          }
        }

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          $result['paginate'] = $paginate;
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['paginate'] = $paginate;
          return $result;
        }
    }


    public function export() {
      $name = 'POLICY-' . Carbon::now()->format('Y-m-d');

      Excel::create($name, function($excel) use ($name) {
        $status = Request::input('export');

        $import = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('updated_at', 'desc')->first();
        if ($import) {

        $status = Request::input('status') ?: '';
        if ($status) {
          $ids = PayrollComputation::where('batch_id', $status)->lists('policy_id');
        } else {
          $ids = PayrollComputation::lists('policy_id');
        }

        if (Auth::user()->usertype_id == 8) {

          $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
                                      'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                      ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                      ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                      ->where(function($query) {
                                          $query->where('policies.user_id', '=', Auth::user()->id);
                                        })
                                      ->orderBy('payroll_computations.updated_at', 'desc')
                                      ->get();
        } else {

            $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
                                    'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->orderBy('payroll_computations.updated_at', 'desc')
                                    ->get();
        }

        $excel->sheet($name, function($sheet) use ($rows) {

            $row_cell = 1;
              $sheet->appendRow(array(
                'POLICY NO',
                'POLICY OWNER NAME',
                'PRODUCT CODE',
                'PRODUCT NAME',
                'POLICY TYPE',
                'PREMIUM',
                'GROSS REV AMT',
                'AGENT SHARE',
                'MGT SHARE',
              ));

              $sheet->cell('A' . $row_cell . ':I' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            foreach ($rows as $key => $field) {
              
              $row_cell += 1;

              $sheet->appendRow(array(
                $field->policy_no,
                strtoupper($field->policy_holder),
                $field->compo_code,
                $field->product_name,
                $field->cat_name,
                number_format($field->net_prem_paid, 2),
                number_format($field->gross_revenue, 2),
                number_format($field->agent_banding, 2),
                number_format($field->manage_share, 2),
              ));

              $sheet->cell('A' . $row_cell . ':I' . $row_cell, function($cells) {
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            }

        });

        }

      })->download('xls');


    }

    public function viewBSC() {

      $id = Request::input('id');

      if ($id) {
        $row = PayrollComputation::find($id);

        if ($row) {

          $row = PayrollComputation::select(
                                      'products.product_name',
                                      'sales_user_extension.salesforce_id',
                                      'supervisor_user.name as supervisor_name',
                                      'users.name as agent_name',
                                      'providers.name as provider_name',
                                      'payroll_computations.policy_no',
                                      'payroll_computations.agent_banding',
                                      'payroll_computations.manage_share',
                                      'payroll_computations.comp_code as compo_code',
                                      'payroll_computations.id',
                                      'payroll_computations.product_id',
                                      'payroll_computations.agent_code',
                                      'payroll_computations.gross_revenue',
                                      'payroll_computations.premium_term', 
                                      'payroll_computations.billing_freq',
                                      'payroll_computations.policy_holder',
                                      'payroll_computations.premium_freq_mtd as premium',
                                      'payroll_computations.upload_date',
                                      'provider_classifications.name as cat_name',
                                      'payroll_computations.incept_date',
                                      'payroll_computations.variable',
                                      'payroll_computations.variable_income',
                                      'payroll_computations.spec_variable_income',
                                      'payroll_computations.svi',
                                      'payroll_computations.bsc_or_svi',
                                      // 'payroll_computations.total_override',
                                      'payroll_computations.variable_or',
                                      'payroll_computations.variable_or_tier2',
                                      'payroll_computations.variable_or_tier3',
                                      'payroll_computations.s_or',
                                      'payroll_computations.s_or_tier2',
                                      'payroll_computations.s_or_tier3'
                                    )
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->leftJoin('batch_month_users', function ($join) {
                                          $join->on('batch_month_users.user_id', '=', 'payroll_computations.user_id')
                                               ->on('batch_month_users.batch_id', '=', 'payroll_computations.batch_id');
                                      })
                                    ->leftJoin('users as supervisor_user', 'supervisor_user.id', '=', 'batch_month_users.supervisor_user_id')
                                    ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'batch_month_users.supervisor_user_id')
                                    ->where('payroll_computations.id', $id)
                                    ->first()->toArray();

          $row['incept_date'] = date_format(date_create(substr($row['incept_date'], 0,10)),'d/m/Y');
          $row['upload_date'] = date_format(date_create(substr($row['upload_date'], 0,10)),'d/m/Y');

          return Response::json($row);
        } else {
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function saveBSCReport() {

      $input = Input::all();

      $row = PayrollComputation::find(Input::get('id'));

      if (!$row) {
        return Response::json(['error_prompt' => "The requested item was not found in the database."]);
      }

      $rules = [
        'variable' => 'required|numeric',
        'variable_income' =>  'required|numeric',
        'spec_variable_income' => 'required|numeric',
        'svi' =>  'required|numeric',
        'bsc_or_svi' =>  'required|numeric',
        // 'total_override' => 'required|numeric',
        'variable_or' =>  'required|numeric',
        'variable_or_tier2' => 'required|numeric',
        'variable_or_tier3' =>  'required|numeric',
        's_or' => 'required|numeric',
        's_or_tier2' => 'required|numeric',
        's_or_tier3' => 'required|numeric',
      ];

      // field name overrides
      $names = [

      ];

      $messages = [

      ];

      // do validation
      $validator = Validator::make($input, $rules, $messages);
      $validator->setAttributeNames($names);

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      $row->variable = array_get($input, 'variable');
      $row->variable_income = array_get($input, 'variable_income');
      $row->spec_variable_income = array_get($input, 'spec_variable_income');
      $row->svi = array_get($input, 'svi');
      $row->bsc_or_svi = array_get($input, 'bsc_or_svi');
      // $row->total_override = array_get($input, 'total_override');
      $row->variable_or = array_get($input, 'variable_or');
      $row->variable_or_tier2 = array_get($input, 'variable_or_tier2');
      $row->variable_or_tier3 = array_get($input, 'variable_or_tier3');
      $row->s_or = array_get($input, 's_or');
      $row->s_or_tier2 = array_get($input, 's_or_tier2');
      $row->s_or_tier3 = array_get($input, 's_or_tier3');
      $row->save();

      return Response::json(['body' => "BSC successfully updated."]);
    }

    public function exportBSC() {
      // dd(Request::all());
      ini_set('memory_limit', '-1');
      $name = 'BSC-' . Carbon::now()->format('Y-m-d');      
      $batch_id = Request::input('batch_id');

      // dd($batch_id);

      if ($batch_id) {
        $import = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->where('id', $batch_id)->orderBy('batch_date', 'desc')->first();

        if ($import) {
          $name = 'BSC-' . strtoupper(date_format(date_create(substr($import->batch_date, 0,10)),'M_Y')) . '-' . Carbon::now()->format('d-m-Y');
        }
      }

      Excel::create($name, function($excel) use ($name, $batch_id) {
        $input_year = Request::input('year');
        $status = Request::input('export');
        $import = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('batch_date', 'desc')->first();

        if ($import) {

          $status = Request::input('status') ?: '';
          if ($status) {
            $ids = PayrollComputation::where('batch_id', $status)->lists('policy_id');
          } else {
            $ids = PayrollComputation::lists('policy_id');
          }

          $status = Request::input('quarter') ?: '';
          $start = null;
          $end = null;

          if ($status) {

            $dates = explode("|", $status);
            if (count($dates) > 1) {
              // $today_year = Carbon::now()->format('Y');

              // $today_year = PayrollComputation::orderBy('created_at','desc')->pluck('created_at')->format('Y');
              $today_year = Request::input('year');

              $start_month = $dates[0];
              $end_month = $dates[1];

              $start = Carbon::create($today_year, $start_month, 1, 0)->format('Y-m-d') . ' 00:00:00';
              $end = Carbon::create($today_year, $end_month, 1, 0)->endOfMonth()->format('Y-m-d') . ' 00:00:00';
            } else {
              $status = null;
            }
          }



          if (Auth::user()->usertype_id == 8) {

              $rows = PayrollComputation::select(
                                        'payroll_computations.policy_holder as policy_owner',
                                        'payroll_computations.policy_no',
                                        'provider_classifications.name as product_type',
                                        'payroll_computations.comp_code as product_code',
                                        'products.product_name',
                                        'providers.name as company_name',
                                        'payroll_computations.premium',
                                        'payroll_computations.variable_income',
                                        'payroll_computations.spec_variable_income',
                                        'payroll_computations.bsc_or_svi',
                                        'payroll_computations.variable_or',
                                        'payroll_computations.variable_or_tier2',
                                        'payroll_computations.variable_or_tier3',
                                        'payroll_computations.s_or',
                                        'payroll_computations.s_or_tier2',
                                        'payroll_computations.s_or_tier3',
                                        'payroll_computations.gross_revenue_per',
                                        'payroll_computations.manage_share',
                                        'payroll_computations.upload_date as date_issue',
                                        'sales_user_extension.salesforce_id as agent_code',
                                        'users.name as agent_name',
                                        'payroll_computations.user_id',
                                        'payroll_computations.batch_id',
                                        'payroll_computations.incept_date',
                                        'payroll_computations.pfr_link',
                                        'payroll_computations.agent_banding',
                                        'payroll_computations.manage_share',
                                        'payroll_computations.product_id',
                                        'payroll_computations.gross_revenue',
                                        'payroll_computations.batch_date',
                                        'payroll_computations.premium_term',
                                        'payroll_computations.billing_freq',
                                        'payroll_computations.provider_id',
                                        'batches.name as batch_name')
                                      ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                      ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                      ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                      ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                      ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                      ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                      ->where(function($query) use ($import) {
                                          $query->where('policies.user_id', '=', Auth::user()->id);
                                        })
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('payroll_computations.incept_date', '>=', $start)
                                                  ->where('payroll_computations.incept_date', '<=', $end);
                                          }
                                        })
                                      ->whereYear('payroll_computations.incept_date', '=', $input_year)
                                      ->orderBy('payroll_computations.created_at', 'desc')
                                      ->get();

          $list_check = $rows->lists('policy_no')->toArray();

          } else {
              $rows =  PayrollComputation::select(
                                        'payroll_computations.policy_holder as policy_owner',
                                        'payroll_computations.policy_no',
                                        'provider_classifications.name as product_type',
                                        'payroll_computations.comp_code as product_code',
                                        'products.product_name',
                                        'providers.name as company_name',
                                        'payroll_computations.premium',
                                        'payroll_computations.variable_income',
                                        'payroll_computations.spec_variable_income',
                                        'payroll_computations.bsc_or_svi',
                                        'payroll_computations.variable_or',
                                        'payroll_computations.variable_or_tier2',
                                        'payroll_computations.variable_or_tier3',
                                        'payroll_computations.s_or',
                                        'payroll_computations.s_or_tier2',
                                        'payroll_computations.s_or_tier3',
                                        'payroll_computations.gross_revenue_per',
                                        'payroll_computations.manage_share',
                                        'payroll_computations.upload_date as date_issue',
                                        'sales_user_extension.salesforce_id as agent_code',
                                        'users.name as agent_name',
                                        'payroll_computations.user_id',
                                        'payroll_computations.batch_id',
                                        'payroll_computations.incept_date',
                                        'payroll_computations.pfr_link',
                                        'payroll_computations.agent_banding',
                                        'payroll_computations.manage_share',
                                        'payroll_computations.product_id',
                                        'payroll_computations.gross_revenue',
                                        'payroll_computations.batch_date',
                                        'payroll_computations.premium_term',
                                        'payroll_computations.billing_freq',
                                        'payroll_computations.provider_id',
                                        'batches.name as batch_name')
                                      ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                      ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                      ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'payroll_computations.user_id')
                                      ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                      ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                      ->leftJoin('batches', 'batches.id', '=', 'payroll_computations.batch_id')
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('payroll_computations.incept_date', '>=', $start)
                                                  ->where('payroll_computations.incept_date', '<=', $end);
                                                  
                                          }
                                        })
                                      ->whereYear('payroll_computations.incept_date', '=', $input_year)
                                      ->orderBy('payroll_computations.created_at', 'desc')
                                      ->get();

          }

          $list_check = $rows->lists('policy_no')->toArray();
            $policy_list = [];
            $array_count = array_count_values($list_check);
            foreach ($array_count as $value => $count)
            {
              if ($count > 1)
              {
                 $policy_list[] = $value;
              }
            }

          $excel->sheet($name, function($sheet) use ($rows, $policy_list) {

              $row_cell = 1;
                $sheet->appendRow(array(
                  'POLICY OWNER',
                  'POLICY NO',
                  'PRODUCT TYPE',
                  'PRODUCT CATEGORY',
                  'PRODUCT CODE',
                  'PRODUCT NAME',
                  'COMPANY NAME',
                  'POLICY STATUS',
                  'POLICY TERM',
                  'PREMIUM',
                  'VARIABLE INCOME',
                  'SPECIFIED VARIABLE INCOME',
                  'TOTAL OVERRIDE',
                  'VARIABLE OR TIER2',
                  'VARIABLE OR TIER3',
                  'S OR TIER2',
                  'S OR TIER3',
                  'PAYROLL MONTH',
                  'DATE LOGGED',
                  'DATE SUBMISSION',
                  'DATE ISSUE',
                  'DATE INCEPTED',
                  'DATE EFFECTIVE',
                  'DATE TRANSFERRED',
                  'DATE BACKDATED',
                  'PFR LINK',
                  'AGENT CODE',
                  'AGENT NAME',
                  'UNIT CODE',
                  'SUPERVISOR CODE',
                  'REPORTING NAME',
                  'CASE SUBMISSION PREMIUM'
                ));

                $sheet->cell('A' . $row_cell . ':AE' . $row_cell, function($cells) {
                  $cells->setFont(array(
                      'bold' => true,
                  ));
                  $cells->setAlignment('center');
                  $cells->setValignment('middle');
                });

              foreach ($rows as $key => $row) {
                $unit_code = '';
                $reporting_to = '';
                $supervisor_code = '';

                $check_advisor = SalesAdvisorComputation::where('user_id', $row->user_id)->where('batch_id', $row->batch_id)->first();
                if ($check_advisor) {
                  $get_supervisor = SalesSupervisorComputation::find($check_advisor->supervisor_id);
                  $get_group = GroupComputation::find($check_advisor->group_id);
                  if ($get_supervisor) {
                    $get_supervisor_info = User::select('users.name', 'sales_user_extension.salesforce_id')
                                                ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'users.id')
                                                ->find($get_supervisor->user_id);
                    if ($get_supervisor_info) {
                      $reporting_to = $get_supervisor_info->name;
                      $supervisor_code = $get_supervisor_info->salesforce_id;
                    }
                  }
                  if ($get_group) {
                    $unit_code = $get_group->code;
                  }
                } else {
                  $check_supervisor = SalesSupervisorComputation::where('user_id', $row->user_id)->where('batch_id', $row->batch_id)->first();
                  if ($check_supervisor) {
                    $get_supervisor_info = User::select('users.name', 'sales_user_extension.salesforce_id')
                              ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'users.id')
                              ->find($check_supervisor->user_id);
                    if ($get_supervisor_info) {
                      $reporting_to = $get_supervisor_info->name;
                      $supervisor_code = $get_supervisor_info->salesforce_id;
                    }
                    $get_group = GroupComputation::find($check_supervisor->group_id);
                    if ($get_group) {
                      $unit_code = $get_group->code;
                    }
                  }
                }

                $spec_variable_income = 0;
                if ($row->variable_income > 0 && $row->spec_variable_income) {
                  $spec_variable_income = $row->variable_income * .60;
                } else {
                  $spec_variable_income = $row->spec_variable_income;
                }


                if(!in_array($row->policy_no, $policy_list))
                {
                  $row_cell += 1;
                  $sheet->appendRow(array(
                    strtoupper($row->policy_owner),
                    strtoupper($row->policy_no),
                    strtoupper($row->product_type),
                    '',
                    strtoupper($row->product_code),
                    $row->product_name,
                    strtoupper($row->company_name),
                    '',
                    $row->premium_term,
                    number_format($row->premium, 2),
                    number_format($row->variable_income, 2),
                    number_format($spec_variable_income, 2),
                    number_format($row->variable_or, 2),
                    number_format($row->variable_or_tier2, 2),
                    number_format($row->variable_or_tier3, 2),
                    number_format($row->s_or_tier2, 2),
                    number_format($row->s_or_tier3, 2),
                    $row->batch_name,
                    '',
                    '',
                    ($row->date_issue ? date_format(date_create(substr($row->date_issue, 0,10)),'d/m/Y') : '' ),
                    ($row->incept_date ? date_format(date_create(substr($row->incept_date, 0,10)),'d/m/Y') : '' ),
                    '',
                    '',
                    '',
                    ($row->pfr_link ? url("policy/production/download") . '/' . $row->pfr_link : ''),
                    strtoupper($row->agent_code),
                    strtoupper($row->agent_name),
                    strtoupper($unit_code),
                    strtoupper($supervisor_code),
                    strtoupper($reporting_to),
                    '-'
                  ));
                }
                else
                {
                  $user_id = User::where('code', $row->agent_code)->pluck('id');
                  $cross_premium = ProductionCases::where('policy_term', $row->policy_term)
                                                  ->where('provider_list', $row->provider_id)
                                                  ->where('currency_value2', $row->premium)
                                                  ->where(function($query) use ($user_id){
                                                    if($user_id)
                                                    {
                                                      $query->where('user_id', $user_id);
                                                    }
                                                  })
                                                  // ->where('incept_date', $row->incept_date)
                                                  ->pluck('currency_value2');
                  $row_cell += 1;
                  $sheet->appendRow(array(
                    strtoupper($row->policy_owner),
                    strtoupper($row->policy_no),
                    strtoupper($row->product_type),
                    '',
                    strtoupper($row->product_code),
                    $row->product_name,
                    strtoupper($row->company_name),
                    '',
                    $row->premium_term,
                    number_format($row->premium, 2),
                    number_format($row->variable_income, 2),
                    number_format($spec_variable_income, 2),
                    number_format($row->variable_or, 2),
                    number_format($row->variable_or_tier2, 2),
                    number_format($row->variable_or_tier3, 2),
                    number_format($row->s_or_tier2, 2),
                    number_format($row->s_or_tier3, 2),
                    $row->batch_name,
                    '',
                    '',
                    ($row->date_issue ? date_format(date_create(substr($row->date_issue, 0,10)),'d/m/Y') : '' ),
                    ($row->incept_date ? date_format(date_create(substr($row->incept_date, 0,10)),'d/m/Y') : '' ),
                    '',
                    '',
                    '',
                    ($row->pfr_link ? url("policy/production/download") . '/' . $row->pfr_link : ''),
                    strtoupper($row->agent_code),
                    strtoupper($row->agent_name),
                    strtoupper($unit_code),
                    strtoupper($supervisor_code),
                    strtoupper($reporting_to),
                    ($cross_premium ? cross_premium : '-')
                  ));
                }


                $sheet->cell('A' . $row_cell . ':AF' . $row_cell, function($cells) {
                  $cells->setAlignment('center');
                  $cells->setValignment('middle');
                });

              }

          });

        }

      })->download('xls');


    }


    public function exportCase() {

      // $name = 'CASE-' . Carbon::now()->format('Y-m-d');

      // Excel::create($name, function($excel) use ($name) {
      //     $rows = ProductionCases::select('nationalities.nationality', 'production_cases.id','production_cases.user_id', 'production_cases.life_id', 'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date', 'review_id.name as review_by',
      //                                       'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.created_at', 'production_cases.source','production_cases.annual_currency_value2',
      //                                       'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
      //                                       'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency','production_cases.currency_id',
      //                                       'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
      //                                       'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','nationalities.nationality', 'production_cases.case',
      //             'production_cases.residency_status', 'production_cases.residency_others', 'providers.name as provider_name')                                 
      //                               ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
      //                               ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
      //                               ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
      //                               ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
      //                               ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
      //                               ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
      //                               ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
      //                               ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
      //                               ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
      //                               ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
      //                               ->where(function($query) {
      //                                   $query->where('production_cases.case', 'LIKE', '%%');
      //                                 })
      //                               ->where('production_cases.status', '=', 1)
      //                               ->orderBy('firstname', 'asc')

      //                               ->get();

      //   // $rows = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.firstname', 'production_cases.lastname', 
      //   //                                 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 
      //   //                                 'assess_id.name as assess_by', 'production_cases.life_id', 'production_cases.owner_id', 
      //   //                                 'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.created_at', 'production_cases.source','production_cases.annual_currency_value2',
      //   //                                 'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
      //   //                                 'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency',
      //   //                                 'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
      //   //                                 'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case')                                  
      //   //                                 ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
      //   //                                 ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
      //   //                                 ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
      //   //                                 ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
      //   //                                 ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
      //   //                                 ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
      //   //                                 ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
      //   //                                 ->where(function($query) {
      //   //                                     if (Auth::user()->usertype_id == 9) {
      //   //                                       $query->where('production_cases.case', '=', 'rejected');
      //   //                                     } else {
      //   //                                       $query->where('production_cases.case', 'LIKE', '%%');
      //   //                                     }
      //   //                                   })
      //   //                                 ->orderBy('case', 'asc')
      //   //                                 ->get();

      //   $excel->sheet($name, function($sheet) use ($rows) {

      //       $row_cell = 1;
      //       if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
      //         $sheet->appendRow(array(
      //           'POLICY OWNER NAME',
      //           'POLICY OWNER ID',
      //           'POLICY OWNER NATIONALITY',
      //           'POLICY OWNER RESIDENCY STATUS',
      //           'POLICY OWNER DATE OF BIRTH',
      //           'PROVIDER',
      //           'MAIN PLAN',
      //           'CURRENCY CODE',
      //           'SUM ASSURED',
      //           'PREMIUM AMOUNT',
      //           'POLICY TERM',
      //           'MODE OF PAYMENT',
      //           'ANNUALIZED PREMIUM',
      //           'AGENT CODE',
      //           'DATE SUBMITTED',
      //           'SOURCE',
      //           'SCREENED STATUS',
      //           'SCREENED DATE',
      //           'SCREENED BY',
      //           'ASSESSED OUTCOME',
      //           'ASSESSED DATE',
      //           'ASSESSED BY',
      //           'REVIEWED OUTCOME',
      //           'REVIEWED DATE',
      //           'REVIEWED BY',
      //         ));

      //         $sheet->cell('A' . $row_cell . ':Y' . $row_cell, function($cells) {
      //           $cells->setFont(array(
      //               'bold' => true,
      //           ));
      //           $cells->setAlignment('center');
      //           $cells->setValignment('middle');
      //         });

      //       } else if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {
      //         $sheet->appendRow(array(
      //           'POLICY OWNER NAME',
      //           'POLICY OWNER ID',
      //           'POLICY OWNER NATIONALITY',
      //           'POLICY OWNER RESIDENCY STATUS',
      //           'POLICY OWNER DATE OF BIRTH',
      //           'PROVIDER',
      //           'MAIN PLAN',
      //           'CURRENCY CODE',
      //           'SUM ASSURED',
      //           'PREMIUM AMOUNT',
      //           'POLICY TERM',
      //           'MODE OF PAYMENT',
      //           'APE',
      //           'AGENT CODE',
      //           'DATE SUBMITTED',
      //           'SOURCE',
      //           'SCREENED STATUS',
      //           'SCREENED DATE',
      //           'SCREENED BY',
      //           'ASSESSED OUTCOME',
      //           'ASSESSED DATE',
      //           'ASSESSED BY',
      //           'REVIEWED OUTCOME',
      //           'REVIEWED DATE',
      //           'REVIEWED BY',
      //           'SUBMITTED TO HO',
      //           'SUBMITTED TO HO DATE',
      //           'SUBMITTED TO HO BY',
      //         ));

      //         $sheet->cell('A' . $row_cell . ':AB' . $row_cell, function($cells) {
      //           $cells->setFont(array(
      //               'bold' => true,
      //           ));
      //           $cells->setAlignment('center');
      //           $cells->setValignment('middle');
      //         });
      //       }

      //       foreach ($rows as $key => $row) {

      //         $curr = explode(" ", $row->currency);
      //         $currency = "N/A";
      //         // if (count($curr) > 0) {
      //         //   $currency = $curr[1];
      //         // }

      //         $row_cell += 1;
      //         $residency = '';
      //         if($row->residency_status == "Others") {
      //           $residency = $row->residency_others;
      //         }
      //         else {
      //           $residency = $row->residency_status;
      //         }

      //         if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
      //           $sheet->appendRow(array(
      //               $row->firstname . ' ' . $row->lastname,
      //               $row->owner_id,
      //               $row->nationality,
      //               $residency,
      //               ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : ''),
      //               $row->provider_name,
      //               $row->product_name,
      //               $row->getSettingsRate->code,
      //               $row->main_sum_assured,
      //               $row->currency_value2,
      //               $row->policy_term,
      //               $row->mode_of_payment,
      //               $row->ape,
      //               $row->agent_code,
      //               ($row->created_at ? date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') : ''),
      //               $row->source,
      //               $row->case,
      //               ($row->screen_date ? date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') : ''),
      //               $row->screened_by,
      //               $row->assess_outcome,
      //               ($row->assess_date ? date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') : ''),
      //               $row->assess_by,
      //               $row->review_outcome,
      //               ($row->review_date ? date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') : ''),
      //               $row->review_by,
      //           ));

      //           $sheet->setColumnFormat(array(
      //               'J' => '#,##0.00',
      //               'M' => '#,##0.00',
      //           ));

      //           $sheet->cell('A' . $row_cell . ':Y' . $row_cell, function($cells) {
      //             $cells->setAlignment('center');
      //             $cells->setValignment('middle');
      //           });

      //         } else {
      //           // dd($row);
      //           $sheet->appendRow(array(
      //               $row->firstname . ' ' . $row->lastname,
      //               $row->owner_id,
      //               $row->nationality,
      //               $residency,
      //               ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : ''),
      //               $row->provider_name,
      //               $row->product_name,
      //               $row->getSettingsRate->code,
      //               $row->main_sum_assured,
      //               $row->currency_value2,
      //               $row->policy_term,
      //               $row->mode_of_payment,
      //               $row->ape,
      //               $row->agent_code,
      //               ($row->created_at ? date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') : ''),
      //               $row->source,
      //               $row->case,
      //               ($row->screen_date ? date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') : ''),
      //               $row->screened_by,
      //               $row->assess_outcome,
      //               ($row->assess_date ? date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') : ''),
      //               $row->assess_by,
      //               $row->review_outcome,
      //               ($row->review_date ? date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') : ''),
      //               $row->review_by,
      //               $row->submitted_ho,
      //               ($row->submitted_date ? date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') : ''),
      //               $row->submitted_by,
      //           ));

      //           $sheet->setColumnFormat(array(
      //               'J' => '#,##0.00',
      //               'M' => '#,##0.00',
      //           ));

      //           $sheet->cell('A' . $row_cell . ':AB' . $row_cell, function($cells) {
      //             $cells->setAlignment('center');
      //             $cells->setValignment('middle');
      //           });
      //         }
      //       }


      // });

      // })->download('xls');
    

      
      $selected_clients = Request::input('selected_client_on');
      $name = 'CASE-' . Carbon::now()->format('Y-m-d') . ($selected_clients == 1 ? '-SC' : '');
      Excel::create($name, function($excel) use ($name, $selected_clients) {
          $rows = ProductionCases::select('nationalities.nationality', 'production_cases.id','production_cases.user_id', 'production_cases.life_id', 'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date', 'review_id.name as review_by','production_cases.submission_date',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.created_at', 'production_cases.source','production_cases.annual_currency_value2',
                                            'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                            'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency','production_cases.currency_id',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation','production_cases.main_sum_assured','production_cases.address',
                                            'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','nationalities.nationality', 'production_cases.case',
                  'production_cases.residency_status', 'production_cases.residency_others', 'providers.name as provider_name', 'production_cases.selected_client')                             
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->where(function($query) {
                                        $query->where('production_cases.case', 'LIKE', '%%');
                                      // $query->where('production_cases.submission_date', '>=', '2016-11-01 00:00:00')
                                      //       ->where('production_cases.submission_date', '<=', '2016-11-30 23:59:59');
                                      })
                                    ->where(function($query) use ($selected_clients){
                                      if($selected_clients)
                                      {
                                        $query->where('production_cases.selected_client', 1);
                                      }
                                    })
                                    ->where('production_cases.status', '=', 1)
                                    ->orderBy('firstname', 'asc')

                                    ->get();

        // $rows = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.firstname', 'production_cases.lastname', 
        //                                 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 
        //                                 'assess_id.name as assess_by', 'production_cases.life_id', 'production_cases.owner_id', 
        //                                 'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.created_at', 'production_cases.source','production_cases.annual_currency_value2',
        //                                 'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
        //                                 'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency',
        //                                 'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
        //                                 'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case')                                  
        //                                 ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
        //                                 ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
        //                                 ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
        //                                 ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
        //                                 ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
        //                                 ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
        //                                 ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
        //                                 ->where(function($query) {
        //                                     if (Auth::user()->usertype_id == 9) {
        //                                       $query->where('production_cases.case', '=', 'rejected');
        //                                     } else {
        //                                       $query->where('production_cases.case', 'LIKE', '%%');
        //                                     }
        //                                   })
        //                                 ->orderBy('case', 'asc')
        //                                 ->get();

        $excel->sheet($name, function($sheet) use ($rows) {

            $row_cell = 1;
            if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              $sheet->appendRow(array(
                'ID',
                'POLICY OWNER ID',
                'POLICY OWNER NAME',
                'POLICY OWNER NATIONALITY',
                'POLICY OWNER RESIDENCY STATUS',
                'POLICY OWNER DATE OF BIRTH',
                'ADDRESS',
                'PROVIDER',
                'MAIN PLAN',
                'CURRENCY CODE',
                'SUM ASSURED',
                'PREMIUM AMOUNT',
                'POLICY TERM',
                'MODE OF PAYMENT',
                'ANNUALIZED PREMIUM',
                'SOURCE',
                'AGENT CODE',
                'DATE SUBMITTED',
                'SUBMISSION STATUS',
                'SCREENED DATE',
                'SCREENED STATUS',
                'SCREENED BY',
                'ASSESSED DATE',
                'ASSESSED OUTCOME',
                'ASSESSED BY',
                'REVIEWED DATE',
                'REVIEWED OUTCOME',
                'REVIEWED BY',
                'SELECTED CLIENT',
              ));

              $sheet->cell('A' . $row_cell . ':Y' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            } else if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {
              $sheet->appendRow(array(
                'ID',
                'POLICY OWNER ID',
                'POLICY OWNER NAME',
                'POLICY OWNER NATIONALITY',
                'POLICY OWNER RESIDENCY STATUS',
                'POLICY OWNER DATE OF BIRTH',
                'ADDRESS',
                'PROVIDER',
                'MAIN PLAN',
                'CURRENCY CODE',
                'SUM ASSURED',
                'PREMIUM AMOUNT',
                'POLICY TERM',
                'MODE OF PAYMENT',
                'APE',
                'SOURCE',
                'AGENT CODE',
                'DATE SUBMITTED',
                'SUBMISSION STATUS',
                'SCREENED DATE',
                'SCREENED STATUS',
                'SCREENED BY',
                'ASSESSED DATE',
                'ASSESSED OUTCOME',
                'ASSESSED BY',
                'REVIEWED DATE',
                'REVIEWED OUTCOME',
                'REVIEWED BY',
                'SUBMITTED TO HO',
                'SUBMITTED TO HO DATE',
                'SUBMITTED TO HO BY',
                'SELECTED CLIENT'
              ));

              $sheet->cell('A' . $row_cell . ':AF' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });
            }

            foreach ($rows as $key => $row) {

              $curr = explode(" ", $row->currency);
              $currency = "N/A";
              // if (count($curr) > 0) {
              //   $currency = $curr[1];
              // }

              $row_cell += 1;
              $residency = '';
              if($row->residency_status == "Others") {
                $residency = $row->residency_others;
              }
              else {
                $residency = $row->residency_status;
              }

              if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
                $sheet->appendRow(array(
                    $row->id,
                    $row->owner_id,
                    $row->firstname . ' ' . $row->lastname,
                    $row->nationality,
                    $residency,
                    ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : ''),
                    $row->address,
                    $row->provider_name,
                    $row->product_name,
                    $row->getSettingsRate->code,
                    $row->main_sum_assured,
                    $row->currency_value2,
                    $row->policy_term,
                    $row->mode_of_payment,
                    $row->ape,
                    $row->source,
                    $row->agent_code,
                    $row->submission_date,
                    $row->case,
                    ($row->screen_date ? date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') : ''),
                    $row->screened_status,
                    $row->screened_by,
                    ($row->assess_date ? date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') : ''),
                    $row->assess_outcome,
                    $row->assess_by,
                    ($row->review_date ? date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') : ''),
                    $row->review_outcome,
                    $row->review_by,
                    ($row->selected_client == 1 ? 'YES' : 'NO')
                ));

                // $sheet->setColumnFormat(array(
                //     'J' => '#,##0.00',
                //     'M' => '#,##0.00',
                // ));

                $sheet->cell('A' . $row_cell . ':Y' . $row_cell, function($cells) {
                  $cells->setAlignment('center');
                  $cells->setValignment('middle');
                });

              } else {
                // dd($row);
                $sheet->appendRow(array(
                    $row->id,
                    $row->owner_id,
                    $row->firstname . ' ' . $row->lastname,
                    $row->nationality,
                    $residency,
                    ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : ''),
                    $row->address,
                    $row->provider_name,
                    $row->product_name,
                    $row->getSettingsRate->code,
                    $row->main_sum_assured,
                    $row->currency_value2,
                    $row->policy_term,
                    $row->mode_of_payment,
                    $row->ape,
                    $row->source,
                    $row->agent_code,
                    ($row->created_at ? date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') : ''),
                    $row->case,
                    ($row->screen_date ? date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') : ''),
                    $row->screened_status,
                    $row->screened_by,
                    ($row->assess_date ? date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') : ''),
                    $row->assess_outcome,
                    $row->assess_by,
                    ($row->review_date ? date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') : ''),
                    $row->review_outcome,
                    $row->review_by,
                    $row->submitted_ho,
                    ($row->submitted_date ? date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') : ''),
                    $row->submitted_by,
                    ($row->selected_client == 1 ? 'YES' : 'NO')
                ));

                // $sheet->setColumnFormat(array(
                //     'J' => '#,##0.00',
                //     'M' => '#,##0.00',
                // ));

                $sheet->cell('A' . $row_cell . ':AB' . $row_cell, function($cells) {
                  $cells->setAlignment('center');
                  $cells->setValignment('middle');
                });
              }
            }


      });

      })->download('xls');
    }
    
    public function indexBSC()
    {   

      // $sales = BatchMonthUser::all();

      // foreach ($sales as $key => $row) {

      //   if(!$row->supervisor_user_id) {
      //     $check_advisor = SalesAdvisorComputation::where('user_id', $row->user_id)->where('batch_id', $row->batch_id)->first();
      //     if ($check_advisor) {
      //       $get_supervisor = SalesSupervisorComputation::find($check_advisor->supervisor_id);
      //       $row->supervisor_user_id = $get_supervisor->user_id;
      //       $row->save();
      //     } else {
      //       $check_supervisor = SalesSupervisorComputation::where('user_id', $row->user_id)->where('batch_id', $row->batch_id)->first();
      //       if ($check_supervisor) {
      //         $row->supervisor_user_id = $check_supervisor->user_id;
      //         $row->save();
      //       }
      //     }
      //   }
      // }

      // dd($sales);

        $result = $this->doListBSC();
        $this->data['title'] = "BSC Report";
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['batches'] = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('batch_date', 'desc')->get();

        $import = Batch::where(function($query) {
                              $query->where('status', 1)
                                    ->orWhere('status', 0);
                          })->orderBy('batch_date', 'desc')->first();

        $this->data['export'] = null;
        if ($import) {
          $this->data['export'] = $import->id;
        }

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
           $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
         $this->data['today_year'] = Carbon::now()->format('Y');
      $this->data['today_year_index'] = Carbon::now()->format('Y');
        return view('policy-management.bsc.list', $this->data);

    }
    
    public function doListBSC()
    {   
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'payroll_computations.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $per = Request::input('per') ?: 10;
      $status = Request::input('status');

      $import = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('updated_at', 'desc')->first();

      // if (!$import) {
      //   $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
      //                               ->join('users', 'users.id', '=', 'policies.user_id')
      //                               ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
      //                               ->where(function($query) {
      //                                 $query->where('policies.status', '=', 1)
      //                                       ->where('policies.user_id', '>', 0);
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                 $query->where('contract_no', 'LIKE', '%' . $search . '%')
      //                                       ->orWhere('users.name', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->selectRaw('sum(count) as rider')
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->groupBy('policies.contract_no')
      //                               ->paginate($per);

      //   // return response (format accordingly)
      //   if(Request::ajax()) {
      //     $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
      //     $result['rows'] = $rows->toArray();
      //     return Response::json($result);
      //   } else {
      //     $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
      //     $result['rows'] = $rows;
      //     return $result;
      //   }
      // }

        $status = Request::input('status') ?: '';
        $start = null;
        $end = null;

        if ($status) {
          $dates = explode("|", $status);
          if (count($dates) > 1) {
            $today_year = Request::input('year') ? Request::input('year') : Carbon::now()->format('Y');

            $start_month = $dates[0];
            $end_month = $dates[1];

            $start = Carbon::create($today_year, $start_month, 1, 0)->format('Y-m-d') . ' 00:00:00';
            $end = Carbon::create($today_year, $end_month, 1, 0)->endOfMonth()->format('Y-m-d') . ' 00:00:00';
          } else {
            $status = null;
          }
        }

        if ($status) {
          $ids = PayrollComputation::where('batch_id', $status)->lists('policy_id');
        } else {
          $ids = PayrollComputation::lists('policy_id');
        }

        $paginate = '';

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          if (Auth::user()->usertype_id == 8) {

            $rows = PayrollComputation::select('products.product_name', 'sales_user_extension.salesforce_id', 'supervisor_user.name as supervisor_name', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code','payroll_computations.id',
                                    'payroll_computations.product_id', 'payroll_computations.agent_code', 'payroll_computations.gross_revenue', 'payroll_computations.premium_term', 
                                    'payroll_computations.billing_freq', 'payroll_computations.policy_holder', 'payroll_computations.variable_income','payroll_computations.premium as net_prem_paid','payroll_computations.upload_date', 'payroll_computations.bsc_or_svi', 'payroll_computations.spec_variable_income',
                                    'provider_classifications.name as cat_name', 'payroll_computations.incept_date', 'payroll_computations.s_or_tier2', 'payroll_computations.s_or_tier3')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                      })
                                    ->leftJoin('batch_month_users', function ($join) {
                                          $join->on('batch_month_users.user_id', '=', 'payroll_computations.user_id')
                                               ->on('batch_month_users.batch_id', '=', 'payroll_computations.batch_id');
                                      })
                                    ->leftJoin('users as supervisor_user', 'supervisor_user.id', '=', 'batch_month_users.supervisor_user_id')
                                    ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'batch_month_users.supervisor_user_id')
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('incept_date', '>=', $start)
                                                ->where('incept_date', '<=', $end);
                                        }
                                      })
                                    ->where('payroll_computations.premium', '>=', 0)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr data-id="' . $row->id . '">' .  
                      '<td>' . $row->agent_code . '</td>' .
                      '<td>' . strtoupper($row->agent_name) . '</td>' .
                      '<td>' . strtoupper($row->salesforce_id) . '</td>' .
                      '<td>' . strtoupper($row->supervisor_name) . '</td>' .
                      '<td>' . strtoupper($row->policy_holder) . '</td>' .
                      '<td>' . strtoupper($row->provider_name) . '</td>' .
                      '<td>' . strtoupper($row->compo_code) . '</td>' .
                      '<td>' . ($row->upload_date ? date_format(date_create(substr($row->upload_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . ($row->incept_date ? date_format(date_create(substr($row->incept_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td class="rightalign">' . $row->premium_term . '</td>' .
                      '<td class="rightalign">' . $row->billing_freq . '</td>' .
                      '<td class="rightalign">' . number_format($row->net_prem_paid, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->spec_variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->bsc_or_svi, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier2, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier3, 2) . '</td></tr>';
              }
            }

          } else {

            $rows = PayrollComputation::select('products.product_name', 'sales_user_extension.salesforce_id', 'supervisor_user.name as supervisor_name', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code','payroll_computations.id',
                                    'payroll_computations.product_id', 'payroll_computations.agent_code', 'payroll_computations.gross_revenue', 'payroll_computations.premium_term', 
                                    'payroll_computations.billing_freq', 'payroll_computations.policy_holder', 'payroll_computations.variable_income','payroll_computations.premium as net_prem_paid','payroll_computations.upload_date', 'payroll_computations.bsc_or_svi', 'payroll_computations.spec_variable_income',
                                    'provider_classifications.name as cat_name', 'payroll_computations.incept_date', 'payroll_computations.s_or_tier2', 'payroll_computations.s_or_tier3')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->leftJoin('batch_month_users', function ($join) {
                                          $join->on('batch_month_users.user_id', '=', 'payroll_computations.user_id')
                                               ->on('batch_month_users.batch_id', '=', 'payroll_computations.batch_id');
                                      })
                                    ->leftJoin('users as supervisor_user', 'supervisor_user.id', '=', 'batch_month_users.supervisor_user_id')
                                    ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'batch_month_users.supervisor_user_id')
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('incept_date', '>=', $start)
                                                ->where('incept_date', '<=', $end);
                                        }
                                      })
                                    ->where('payroll_computations.premium', '>=', 0)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);


            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr data-id="' . $row->id . '">' .  
                      '<td>' . $row->agent_code . '</td>' .
                      '<td>' . strtoupper($row->agent_name) . '</td>' .
                      '<td>' . strtoupper($row->salesforce_id) . '</td>' .
                      '<td>' . strtoupper($row->supervisor_name) . '</td>' .
                      '<td>' . strtoupper($row->policy_holder) . '</td>' .
                      '<td>' . strtoupper($row->provider_name) . '</td>' .
                      '<td>' . strtoupper($row->compo_code) . '</td>' .
                      '<td>' . ($row->upload_date ? date_format(date_create(substr($row->upload_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . ($row->incept_date ? date_format(date_create(substr($row->incept_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td class="rightalign">' . $row->premium_term . '</td>' .
                      '<td class="rightalign">' . $row->billing_freq . '</td>' .
                      '<td class="rightalign">' . number_format($row->net_prem_paid, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->spec_variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->bsc_or_svi, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier2, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier3, 2) . '</td>';

                if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2) {
                   $paginate .= '<td class="rightalign">
                                  <button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                                </td>';
                }

                $paginate .= '</tr>'; 
              }
            }
          }
        } else {
          if (Auth::user()->usertype_id == 8) {

            $count = PayrollComputation::select('products.product_name', 'sales_user_extension.salesforce_id', 'supervisor_user.name as supervisor_name', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code','payroll_computations.id',
                                    'payroll_computations.product_id', 'payroll_computations.agent_code', 'payroll_computations.gross_revenue', 'payroll_computations.premium_term', 
                                    'payroll_computations.billing_freq', 'payroll_computations.policy_holder', 'payroll_computations.variable_income','payroll_computations.premium as net_prem_paid','payroll_computations.upload_date', 'payroll_computations.bsc_or_svi', 'payroll_computations.spec_variable_income',
                                    'provider_classifications.name as cat_name', 'payroll_computations.incept_date', 'payroll_computations.s_or_tier2', 'payroll_computations.s_or_tier3')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                      })
                                    ->leftJoin('batch_month_users', function ($join) {
                                          $join->on('batch_month_users.user_id', '=', 'payroll_computations.user_id')
                                               ->on('batch_month_users.batch_id', '=', 'payroll_computations.batch_id');
                                      })
                                    ->leftJoin('users as supervisor_user', 'supervisor_user.id', '=', 'batch_month_users.supervisor_user_id')
                                    ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'batch_month_users.supervisor_user_id')
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('incept_date', '>=', $start)
                                                ->where('incept_date', '<=', $end);
                                        }
                                      })
                                    ->where('payroll_computations.premium', '>=', 0)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
          } else {
            
            $count = PayrollComputation::select('products.product_name', 'sales_user_extension.salesforce_id', 'supervisor_user.name as supervisor_name', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code','payroll_computations.id',
                                    'payroll_computations.product_id', 'payroll_computations.agent_code', 'payroll_computations.gross_revenue', 'payroll_computations.premium_term', 
                                    'payroll_computations.billing_freq', 'payroll_computations.policy_holder', 'payroll_computations.variable_income','payroll_computations.premium as net_prem_paid','payroll_computations.upload_date', 'payroll_computations.bsc_or_svi', 'payroll_computations.spec_variable_income',
                                    'provider_classifications.name as cat_name', 'payroll_computations.incept_date', 'payroll_computations.s_or_tier2', 'payroll_computations.s_or_tier3')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->leftJoin('batch_month_users', function ($join) {
                                          $join->on('batch_month_users.user_id', '=', 'payroll_computations.user_id')
                                               ->on('batch_month_users.batch_id', '=', 'payroll_computations.batch_id');
                                      })
                                    ->leftJoin('users as supervisor_user', 'supervisor_user.id', '=', 'batch_month_users.supervisor_user_id')
                                    ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'batch_month_users.supervisor_user_id')
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status) {
                                        if ($status) {
                                          $query->where('incept_date', '>=', $start)
                                                ->where('incept_date', '<=', $end);
                                        }
                                      })
                                    ->where('payroll_computations.premium', '>=', 0)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

          }

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          if (Auth::user()->usertype_id == 8) {

            $rows = PayrollComputation::select('products.product_name', 'sales_user_extension.salesforce_id', 'supervisor_user.name as supervisor_name', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code','payroll_computations.id',
                                    'payroll_computations.product_id', 'payroll_computations.agent_code', 'payroll_computations.gross_revenue', 'payroll_computations.premium_term', 
                                    'payroll_computations.billing_freq', 'payroll_computations.policy_holder', 'payroll_computations.variable_income','payroll_computations.premium as net_prem_paid','payroll_computations.upload_date', 'payroll_computations.bsc_or_svi', 'payroll_computations.spec_variable_income',
                                    'provider_classifications.name as cat_name', 'payroll_computations.incept_date', 'payroll_computations.s_or_tier2', 'payroll_computations.s_or_tier3')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                      })
                                    ->leftJoin('batch_month_users', function ($join) {
                                          $join->on('batch_month_users.user_id', '=', 'payroll_computations.user_id')
                                               ->on('batch_month_users.batch_id', '=', 'payroll_computations.batch_id');
                                      })
                                    ->leftJoin('users as supervisor_user', 'supervisor_user.id', '=', 'batch_month_users.supervisor_user_id')
                                    ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'batch_month_users.supervisor_user_id')
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('incept_date', '>=', $start)
                                                ->where('incept_date', '<=', $end);
                                        }
                                      })
                                    ->where('payroll_computations.premium', '>=', 0)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr data-id="' . $row->id . '">' .  
                      '<td>' . $row->agent_code . '</td>' .
                      '<td>' . strtoupper($row->agent_name) . '</td>' .
                      '<td>' . strtoupper($row->salesforce_id) . '</td>' .
                      '<td>' . strtoupper($row->supervisor_name) . '</td>' .
                      '<td>' . strtoupper($row->policy_holder) . '</td>' .
                      '<td>' . strtoupper($row->provider_name) . '</td>' .
                      '<td>' . strtoupper($row->compo_code) . '</td>' .
                      '<td>' . ($row->upload_date ? date_format(date_create(substr($row->upload_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . ($row->incept_date ? date_format(date_create(substr($row->incept_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td class="rightalign">' . $row->premium_term . '</td>' .
                      '<td class="rightalign">' . $row->billing_freq . '</td>' .
                      '<td class="rightalign">' . number_format($row->net_prem_paid, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->spec_variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->bsc_or_svi, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier2, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier3, 2) . '</td></tr>';
              }
            }
          } else {
            
            
            $rows = PayrollComputation::select('products.product_name', 'sales_user_extension.salesforce_id', 'supervisor_user.name as supervisor_name', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code','payroll_computations.id',
                                    'payroll_computations.product_id', 'payroll_computations.agent_code', 'payroll_computations.gross_revenue', 'payroll_computations.premium_term', 
                                    'payroll_computations.billing_freq', 'payroll_computations.policy_holder', 'payroll_computations.variable_income','payroll_computations.premium as net_prem_paid','payroll_computations.upload_date', 'payroll_computations.bsc_or_svi', 'payroll_computations.spec_variable_income',
                                    'provider_classifications.name as cat_name', 'payroll_computations.incept_date', 'payroll_computations.s_or_tier2', 'payroll_computations.s_or_tier3')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->leftJoin('batch_month_users', function ($join) {
                                          $join->on('batch_month_users.user_id', '=', 'payroll_computations.user_id')
                                               ->on('batch_month_users.batch_id', '=', 'payroll_computations.batch_id');
                                      })
                                    ->leftJoin('users as supervisor_user', 'supervisor_user.id', '=', 'batch_month_users.supervisor_user_id')
                                    ->leftJoin('sales_user_extension', 'sales_user_extension.user_id', '=', 'batch_month_users.supervisor_user_id')
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('incept_date', '>=', $start)
                                                ->where('incept_date', '<=', $end);
                                        }
                                      })
                                    ->where('payroll_computations.premium', '>=', 0)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            if (Request::ajax()) {
              foreach ($rows as $key => $row) {
                $paginate .= '<tr data-id="' . $row->id . '">' .  
                      '<td>' . $row->agent_code . '</td>' .
                      '<td>' . strtoupper($row->agent_name) . '</td>' .
                      '<td>' . strtoupper($row->salesforce_id) . '</td>' .
                      '<td>' . strtoupper($row->supervisor_name) . '</td>' .
                      '<td>' . strtoupper($row->policy_holder) . '</td>' .
                      '<td>' . strtoupper($row->provider_name) . '</td>' .
                      '<td>' . strtoupper($row->compo_code) . '</td>' .
                      '<td>' . ($row->upload_date ? date_format(date_create(substr($row->upload_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . ($row->incept_date ? date_format(date_create(substr($row->incept_date, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td></td>' .
                      '<td class="rightalign">' . $row->premium_term . '</td>' .
                      '<td class="rightalign">' . $row->billing_freq . '</td>' .
                      '<td class="rightalign">' . number_format($row->net_prem_paid, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->spec_variable_income, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->bsc_or_svi, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier2, 2) . '</td>' .
                      '<td class="rightalign">' . number_format($row->s_or_tier3, 2) . '</td>';

                if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2) {
                   $paginate .= '<td class="rightalign">
                                  <button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                                </td>';
                }

                $paginate .= '</tr>'; 
              }
            }

          }
        }

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          $result['paginate'] = $paginate;
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['paginate'] = $paginate;
          return $result;
        }
    }

    /**
     * Display a listing of the policy.
     *
     * @return Response
     */
    public function index()
    {   
        $result = $this->doList();
        $this->data['title'] = "Policies";
        $this->data['rows'] = $result['rows'];
        $this->data['paginate'] = $result['paginate'];
        $this->data['pages'] = $result['pages'];
        $this->data['batches'] = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->where('is_sales', 1)->orderBy('updated_at', 'desc')->get();

        $import = Batch::where(function($query) {
                              $query->where('status', 1)
                                    ->orWhere('status', 0);
                          })->orderBy('updated_at', 'desc')->first();

        $this->data['export'] = null;
        if ($import) {
          $this->data['export'] = $import->id;
        }

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();
        $this->data['supervisor'] = "system";

        if (Auth::user()->usertype_id == '8') { //if sales user

          $check_supervisor = Sales::with('designations')->where('user_id', Auth::user()->id)->first();

          $this->data['supervisor'] = "no";
          if ($check_supervisor->designations->rank == "Supervisor") {
            $this->data['supervisor'] = "yes";
          }

          $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                        ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                        ->where(function($query) {
                                              $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                    ->where('notifications.usertype_id', '=', '1')
                                                    ->where('notifications_per_users.is_read', '=', '0');                             
                                          })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
           $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.list', $this->data);
    }
    
    /**
     * Build the list
     *
     */
    public function doList() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'name';
      $result['order'] = Request::input('order') ?: 'asc';
      $search = Request::input('search');
      $per = Request::input('per') ?: 20;
      $status = Request::input('status');

      $import = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('updated_at', 'desc')->first();

      $paginate = '';

      if (Auth::user()->usertype_id != 8) {
      $result['sort'] = Request::input('sort') ?: 'agent_name';
        
        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

            $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code', 'payroll_computations.spec_variable_income', 'payroll_computations.billing_freq',
                                    'payroll_computations.product_id', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->where(function($query) use ($status) {
                                        if ($status) {
                                          $query->where('payroll_computations.batch_id', $status);
                                        }
                                      })
                                    ->where(function($query) use ($search) {
                                        $query->where('payroll_computations.policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('products.product_name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('provider_classifications.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('payroll_computations.billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            foreach ($rows as $key => $field) {
              $paginate .= '<tr data-id="' . $field->id . '">' . 
                '<td>' . strtoupper($field->agent_name) . '</td>' .
                '<td>' . $field->policy_no . '</td>' .
                '<td>' . strtoupper($field->policy_holder) . '</td>' .
                '<td>' . $field->provider_name . '</td>' .
                '<td>' . $field->compo_code . '</td>' .
                '<td>' . $field->product_name . '</td>' .
                '<td>' . $field->cat_name . '</td>' .
                '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
                '<td>' . ucwords($field->billing_freq) . '</td>' .
                '<td class="rightalign">' . number_format($field->spec_variable_income, 2) . '</td>' .
                '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
                '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td></tr>';
            }


          // $rows = User::where('usertype_id', 8)
          //             ->where('status', 1)
          //             ->where(function($query) use ($search) {
          //                 $query->where('name', 'LIKE', '%' . $search . '%');
          //               })
          //             ->orderBy($result['sort'], $result['order'])
          //             ->paginate($per);

          // foreach ($rows as $key => $field) {
          //   $paginate .= '<tr data-id="' . $field->id . '"><td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' . $field->name . '</td></tr>';
          // }
        } else {

          $count = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code', 'payroll_computations.spec_variable_income', 'payroll_computations.billing_freq',
                                  'payroll_computations.product_id', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                  ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                  ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                  ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                  ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                  ->where(function($query) use ($status) {
                                      if ($status) {
                                        $query->where('payroll_computations.batch_id', $status);
                                      }
                                    })
                                  ->where(function($query) use ($search) {
                                      $query->where('payroll_computations.policy_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('products.product_name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('provider_classifications.name', 'LIKE', '%' . $search . '%')
                                            ->orWhere('payroll_computations.billing_freq', 'LIKE', '%' . $search . '%');
                                    })
                                  ->orderBy($result['sort'], $result['order'])
                                  ->paginate($per);

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding',
                                 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
                                 'payroll_computations.spec_variable_income',
                                  'payroll_computations.billing_freq','payroll_computations.product_id', 'users.name as agent_name', 'providers.name as provider_name', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'payroll_computations.provider_id')
                                    ->leftJoin('users', 'users.id', '=', 'payroll_computations.user_id')
                                    ->where(function($query) use ($status) {
                                        if ($status) {
                                          $query->where('payroll_computations.batch_id', $status);
                                        }
                                      })
                                    ->where(function($query) use ($search) {
                                        $query->where('payroll_computations.policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('payroll_computations.policy_holder', 'LIKE', '%' . $search . '%')
                                              ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('products.product_name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('provider_classifications.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('payroll_computations.billing_freq', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            foreach ($rows as $key => $field) {
              $paginate .= '<tr data-id="' . $field->id . '">' . 
                '<td>' . strtoupper($field->agent_name) . '</td>' .
                '<td>' . $field->policy_no . '</td>' .
                '<td>' . strtoupper($field->policy_holder) . '</td>' .
                '<td>' . $field->provider_name . '</td>' .
                '<td>' . $field->compo_code . '</td>' .
                '<td>' . $field->product_name . '</td>' .
                '<td>' . $field->cat_name . '</td>' .
                '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
                '<td>' . ucwords($field->billing_freq) . '</td>' .
                '<td class="rightalign">' . number_format($field->spec_variable_income, 2) . '</td>' .
                '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
                '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td></tr>';
            }

        }

        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          $result['paginate'] = $paginate;
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['paginate'] = $paginate;
          return $result;
        }

      } else {
        $check_owner = Group::where('user_id', Auth::user()->id)->first();

        $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

        if ($check_owner) {

          $rows = User::where('usertype_id', 8)
                      ->where('status', 9)
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate($per);

          $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

          if ($check_supervisor) {
            $paginate .= '<tr><td class="td-hover" style="border-bottom: 1px solid #cccccc;" data-toggle="collapse" data-target="#expand-' . $check_supervisor->id . '"><a style="color: #000000;"><u>' . $check_supervisor->unit_code . '</u></a> </td></tr>';

            $users[0] = Auth::user()->id;
            $get_own_advisors = SalesAdvisor::where('supervisor_id', $check_supervisor->id)->lists('user_id');

            $users = array_merge($users, $get_own_advisors->toArray());

            $get_users = User::where('usertype_id', 8)
                        ->where('status', 1)
                        ->whereIn('id', $users)
                        ->get();

            $paginate .= '<tr>
                            <td class="expand-td" colspan="1" style="padding: 0;">
                              <div class="container-fluid">
                                <div class="accordian-body collapse" id="expand-' . $check_supervisor->id . '" style="margin-bottom: 0px;" aria-expanded="true">
                                  <table class="table table-striped">';

            foreach ($get_users as $key => $field) {
              $paginate .= '<tr class="' . (Auth::user()->id == $field->id ? 'info' : '') . '" data-id="' . $field->id . '"><td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a>  ' . $field->name . (Auth::user()->id == $field->id ? ' <i class="fa fa-user"></i>' : '') . '</td></tr>';
            }

            $paginate .= '</table>
                          </div>
                          </div>
                          </td>
                          </tr>';


            $get_supervisors = SalesSupervisor::where('group_id', $check_owner->id)->where('id', '!=', $check_supervisor->id)->get();

            foreach ($get_supervisors as $gskey => $gsfield) {
              $gsusers[0] = $gsfield->user_id;
              $get_advisors = SalesAdvisor::where('supervisor_id', $gsfield->id)->lists('user_id');
              $gsusers = array_merge($gsusers, $get_advisors->toArray());

              $get_gsusers = User::where('usertype_id', 8)
                          ->where('status', 1)
                          ->whereIn('id', $gsusers)
                          ->get();

              $paginate .= '<tr><td class="td-hover" style="border-bottom: 1px solid #cccccc;" data-toggle="collapse" data-target="#expand-' . $gsfield->id . '"><a style="color: #000000;"><u>' . $gsfield->unit_code . '</u></a></td></tr>';

              $paginate .= '<tr>
                              <td class="expand-td" colspan="1" style="padding: 0;">
                                <div class="container-fluid">
                                  <div class="accordian-body collapse" id="expand-' . $gsfield->id . '" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="table table-striped">';

              foreach ($get_gsusers as $key => $field) {
                $paginate .= '<tr class="' . (Auth::user()->id == $field->id ? 'info' : '') . '" data-id="' . $field->id . '"><td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' . $field->name . (Auth::user()->id == $field->id ? ' <i class="fa fa-user"></i>' : '') . '</td></tr>';
              }

              $paginate .= '</table>
                            </div>
                            </div>
                            </td>
                            </tr>';
            }
          } else {

            $get_supervisors = SalesSupervisor::where('group_id', $check_owner->id)->where('id', '!=', $check_supervisor)->get();

            foreach ($get_supervisors as $gskey => $gsfield) {
              $paginate = '<td>' . $gsfield->unit_code . '</td>';
            }
          }
          // $paginate .= '</tr>';

        } else if ($check_supervisor) {
          $users[0] = Auth::user()->id;
          $get_advisors = SalesAdvisor::where('supervisor_id', $check_supervisor->id)->lists('user_id');

          $users = array_merge($users, $get_advisors->toArray());

          if (Request::input('page') != '»') {
            Paginator::currentPageResolver(function () {
                return Request::input('page'); 
            });

            $rows = User::where('usertype_id', 8)
                        ->where('status', 1)
                        ->where(function($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%');
                          })
                        ->whereIn('id', $users)
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

            foreach ($rows as $key => $field) {
              $paginate .= '<tr class="' . (Auth::user()->id == $field->id ? 'info' : '') . '" data-id="' . $field->id . '"><td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' . $field->name . (Auth::user()->id == $field->id ? ' <i class="fa fa-user"></i>' : '') . '</td></tr>';
            }

          } else {

            $count = User::where('usertype_id', 8)
                        ->where('status', 1)
                        ->where(function($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%');
                          })
                        ->whereIn('id', $users)
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

            Paginator::currentPageResolver(function () use ($count, $per) {
                return ceil($count->total() / $per);
            });

            $rows = User::where('usertype_id', 8)
                        ->where('status', 1)
                        ->where(function($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%');
                          })
                        ->whereIn('id', $users)
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

            foreach ($rows as $key => $field) {
              $paginate .= '<tr data-id="' . $field->id . '"><td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a> ' . $field->name . '</td></tr>';
            }
          }
        } else {
          if (Request::input('page') != '»') {
            Paginator::currentPageResolver(function () {
              return Request::input('page'); 
            });

            $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code', 'payroll_computations.spec_variable_income', 'payroll_computations.billing_freq',
                                    'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) use ($status) {
                                        if ($status) {
                                          $query->where('payroll_computations.batch_id', $status);
                                        }
                                      })
                                    ->where(function($query) use ($import) {
                                        $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            $total_premium = 0;
            $total_gross = 0;
            $total_agent = 0;
            $total_mgt = 0;
            $total_svi = 0;

            foreach ($rows as $key => $field) {
            $paginate .= '<tr data-id="' . $field->id . '"><td>' . $field->policy_no . '</td>' .
              '<td>' . strtoupper($field->policy_holder) . '</td>' .
              '<td>' . $field->product_name . '</td>' .
              '<td>' . $field->cat_name . '</td>' .
              '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
              '<td>' . ucwords($field->billing_freq) . '</td>' .
              '<td class="rightalign">' . number_format($field->spec_variable_income, 2) . '</td>' .
              '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
              '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td></tr>';

              $total_premium += $field->net_prem_paid;
              $total_gross += $field->gross_revenue;
              $total_agent += $field->agent_banding;
              $total_mgt += $field->manage_share;
              $total_svi += $field->spec_variable_income;
            }

            $paginate .= '<tr style="border-top: 2px solid #555B63;" class="hide">' .
              '<td><strong>TOTAL</strong></td>' .
              '<td></td>' .
              '<td></td>' .
              '<td></td>' .
              '<td class="rightalign"><strong>' . number_format($total_premium, 2) . '</strong></td>' .
              '<td></td>' .
              '<td class="rightalign"><strong>' . number_format($total_svi, 2) . '</strong></td>' .
              '<td class="rightalign"><strong>' . number_format($total_gross, 2) . '</strong></td>' .
              '<td class="rightalign"><strong>' . number_format($total_agent, 2) . '</strong></td>' .
              '</tr>';

          } else {

            $count = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
                                    'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) use ($status) {
                                        if ($status) {
                                          $query->where('payroll_computations.batch_id', $status);
                                        }
                                      })
                                    ->where(function($query) use ($import) {
                                        $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            Paginator::currentPageResolver(function () use ($count, $per) {
                return ceil($count->total() / $per);
            });

            $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code', 'payroll_computations.spec_variable_income', 'payroll_computations.billing_freq',
                                    'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                    ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                    ->where(function($query) use ($status) {
                                        if ($status) {
                                          $query->where('payroll_computations.batch_id', $status);
                                        }
                                      })
                                    ->where(function($query) use ($import) {
                                        $query->where('payroll_computations.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                        $query->where('policy_no', 'LIKE', '%' . $search . '%')
                                              ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

            $total_premium = 0;
            $total_gross = 0;
            $total_agent = 0;
            $total_mgt = 0;
            $total_svi = 0;

            foreach ($rows as $key => $field) {
            $paginate .= '<tr data-id="' . $field->id . '"><td>' . $field->policy_no . '</td>' .
              '<td>' . strtoupper($field->policy_holder) . '</td>' .
              '<td>' . $field->product_name . '</td>' .
              '<td>' . $field->cat_name . '</td>' .
              '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
              '<td>' . ucwords($field->billing_freq) . '</td>' .
              '<td class="rightalign">' . number_format($field->spec_variable_income, 2) . '</td>' .
              '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
              '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td></tr>';

              $total_premium += $field->net_prem_paid;
              $total_gross += $field->gross_revenue;
              $total_agent += $field->agent_banding;
              $total_mgt += $field->manage_share;
              $total_svi += $field->spec_variable_income;
            }

            $paginate .= '<tr style="border-top: 2px solid #555B63;" class="hide">' .
              '<td><strong>TOTAL</strong></td>' .
              '<td></td>' .
              '<td></td>' .
              '<td></td>' .
              '<td class="rightalign"><strong>' . number_format($total_premium, 2) . '</strong></td>' .
              '<td></td>' .
              '<td class="rightalign"><strong>' . number_format($total_svi, 2) . '</strong></td>' .
              '<td class="rightalign"><strong>' . number_format($total_gross, 2) . '</strong></td>' .
              '<td class="rightalign"><strong>' . number_format($total_agent, 2) . '</strong></td>' .
              '</tr>';

          }
        }

        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          $result['paginate'] = $paginate;
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['paginate'] = $paginate;
          return $result;
        }
      }

      // if (!$import) {
      //   $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
      //                               ->join('users', 'users.id', '=', 'policies.user_id')
      //                               ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
      //                               ->where(function($query) {
      //                                 $query->where('policies.status', '=', 1)
      //                                       ->where('policies.user_id', '>', 0);
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                 $query->where('contract_no', 'LIKE', '%' . $search . '%')
      //                                       ->orWhere('users.name', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->selectRaw('sum(count) as rider')
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->groupBy('policies.contract_no')
      //                               ->paginate($per);

      //   // return response (format accordingly)
      //   if(Request::ajax()) {
      //     $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
      //     $result['rows'] = $rows->toArray();
      //     return Response::json($result);
      //   } else {
      //     $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
      //     $result['rows'] = $rows;
      //     return $result;
      //   }
      // }

      //   $status = Request::input('status') ?: '';
      //   if ($status) {
      //     $ids = PayrollComputation::where('batch_id', $status)->lists('policy_id');
      //   } else {
      //     $ids = PayrollComputation::lists('policy_id');
      //   }

      //   $paginate = '';

      //   if (Request::input('page') != '»') {
      //     Paginator::currentPageResolver(function () {
      //         return Request::input('page'); 
      //     });

      //     if (Auth::user()->usertype_id == 8) {

      //       $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
      //                               'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
      //                               ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
      //                               ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
      //                               ->where(function($query) use ($status) {
      //                                   if ($status) {
      //                                     $query->where('payroll_computations.batch_id', $status);
      //                                   }
      //                                 })
      //                               ->where(function($query) use ($import) {
      //                                   $query->where('payroll_computations.user_id', '=', Auth::user()->id);
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                   $query->where('policy_no', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->paginate($per);

      //       $check_supervisor = Sales::with('designations')->where('user_id', Auth::user()->id)->first();

      //       $supervisor = false;
      //       if ($check_supervisor->designations->rank == "Supervisor") {
      //         $supervisor = true;
      //       }

      //       $total_premium = 0;
      //       $total_gross = 0;
      //       $total_agent = 0;
      //       $total_mgt = 0;
      //       foreach ($rows as $key => $field) {
      //         $paginate .= '<tr data-id="' . $field->id . '"><td>' . $field->policy_no . '</td>' .
      //           '<td>' . strtoupper($field->policy_holder) . '</td>' .
      //           '<td>' . $field->compo_code . '</td>' .
      //           '<td>' . $field->product_name . '</td>' .
      //           '<td>' . $field->cat_name . '</td>' .
      //           '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
      //           '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
      //           '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td>' .
      //           ($supervisor ? '<td class="rightalign">' . number_format($field->manage_share, 2) . '</td>' : '')
      //           .'</tr>';
      //         $total_premium += $field->net_prem_paid;
      //         $total_gross += $field->gross_revenue;
      //         $total_agent += $field->agent_banding;
      //         $total_mgt += $field->manage_share;
      //       }

      //       $paginate .= '<tr style="border-top: 2px solid #555B63;">' .
      //         '<td><strong>TOTAL</strong></td>' .
      //         '<td></td>' .
      //         '<td></td>' .
      //         '<td></td>' .
      //         '<td></td>' .
      //         '<td class="rightalign"><strong>' . number_format($total_premium, 2) . '</strong></td>' .
      //         '<td class="rightalign"><strong>' . number_format($total_gross, 2) . '</strong></td>' .
      //         '<td class="rightalign"><strong>' . number_format($total_agent, 2) . '</strong></td>' .
      //         ($supervisor ? '<td class="rightalign"><strong>' . number_format($total_mgt, 2) . '</strong></td>' : '')
      //         .'</tr>';

            
      //     } else {

      //       $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
      //                               'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
      //                               ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
      //                               ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
      //                               ->where(function($query) use ($status) {
      //                                   if ($status) {
      //                                     $query->where('payroll_computations.batch_id', $status);
      //                                   }
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                   $query->where('policy_no', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->paginate($per);

      //         $total_premium = 0;
      //         $total_gross = 0;
      //         $total_agent = 0;
      //         $total_mgt = 0;
      //         foreach ($rows as $key => $field) {
      //           $paginate .= '<tr data-id="' . $field->id . '"><td>' . $field->policy_no . '</td>' .
      //             '<td>' . strtoupper($field->policy_holder) . '</td>' .
      //             '<td>' . $field->compo_code . '</td>' .
      //             '<td>' . $field->product_name . '</td>' .
      //             '<td>' . $field->cat_name . '</td>' .
      //             '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
      //             '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
      //             '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td>' .
      //             '<td class="rightalign">' . number_format($field->manage_share, 2) . '</td></tr>';
      //           $total_premium += $field->net_prem_paid;
      //           $total_gross += $field->gross_revenue;
      //           $total_agent += $field->agent_banding;
      //           $total_mgt += $field->manage_share;
      //         }
      //         $paginate .= '<tr style="border-top: 2px solid #555B63;">' .
      //           '<td><strong>TOTAL</strong></td>' .
      //           '<td></td>' .
      //           '<td></td>' .
      //           '<td></td>' .
      //           '<td></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_premium, 2) . '</strong></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_gross, 2) . '</strong></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_agent, 2) . '</strong></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_mgt, 2) . '</strong></td>' .
      //           '</tr>';
            
      //     }
      //   } else {
      //     if (Auth::user()->usertype_id == 8) {

      //       $count = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
      //                               'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
      //                               ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
      //                               ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
      //                               ->where(function($query) use ($status) {
      //                                   if ($status) {
      //                                     $query->where('payroll_computations.batch_id', $status);
      //                                   }
      //                                 })
      //                               ->where(function($query) use ($import) {
      //                                   $query->where('payroll_computations.user_id', '=', Auth::user()->id);
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                   $query->where('policy_no', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->paginate($per);

      //     } else {

      //       $count = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
      //                               'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
      //                               ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
      //                               ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
      //                               ->where(function($query) use ($status) {
      //                                   if ($status) {
      //                                     $query->where('payroll_computations.batch_id', $status);
      //                                   }
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                   $query->where('policy_no', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->paginate($per);

      //     }

      //     Paginator::currentPageResolver(function () use ($count, $per) {
      //         return ceil($count->total() / $per);
      //     });

      //     if (Auth::user()->usertype_id == 8) {

      //       $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
      //                               'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
      //                               ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
      //                               ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
      //                               ->where(function($query) use ($status) {
      //                                   if ($status) {
      //                                     $query->where('payroll_computations.batch_id', $status);
      //                                   }
      //                                 })
      //                               ->where(function($query) use ($import) {
      //                                   $query->where('payroll_computations.user_id', '=', Auth::user()->id);
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                   $query->where('policy_no', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->paginate($per);
                                    
      //       $check_supervisor = Sales::with('designations')->where('user_id', Auth::user()->id)->first();

      //       $supervisor = false;
      //       if ($check_supervisor->designations->rank == "Supervisor") {
      //         $supervisor = true;
      //       }

      //       $total_premium = 0;
      //       $total_gross = 0;
      //       $total_agent = 0;
      //       $total_mgt = 0;
      //       foreach ($rows as $key => $field) {
      //         $paginate .= '<tr data-id="' . $field->id . '"><td>' . $field->policy_no . '</td>' .
      //           '<td>' . strtoupper($field->policy_holder) . '</td>' .
      //           '<td>' . $field->compo_code . '</td>' .
      //           '<td>' . $field->product_name . '</td>' .
      //           '<td>' . $field->cat_name . '</td>' .
      //           '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
      //           '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
      //           '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td>' .
      //           ($supervisor ? '<td class="rightalign">' . number_format($field->manage_share, 2) . '</td>' : '')
      //           .'</tr>';
      //         $total_premium += $field->net_prem_paid;
      //         $total_gross += $field->gross_revenue;
      //         $total_agent += $field->agent_banding;
      //         $total_mgt += $field->manage_share;
      //       }
            
      //       $paginate .= '<tr style="border-top: 2px solid #555B63;">' .
      //         '<td><strong>TOTAL</strong></td>' .
      //         '<td></td>' .
      //         '<td></td>' .
      //         '<td></td>' .
      //         '<td></td>' .
      //         '<td class="rightalign"><strong>' . number_format($total_premium, 2) . '</strong></td>' .
      //         '<td class="rightalign"><strong>' . number_format($total_gross, 2) . '</strong></td>' .
      //         '<td class="rightalign"><strong>' . number_format($total_agent, 2) . '</strong></td>' .
      //         ($supervisor ? '<td class="rightalign"><strong>' . number_format($total_mgt, 2) . '</strong></td>' : '')
      //         .'</tr>';

      //     } else {

      //       $rows = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding', 'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code',
      //                               'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
      //                               ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
      //                               ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
      //                               ->where(function($query) use ($status) {
      //                                   if ($status) {
      //                                     $query->where('payroll_computations.batch_id', $status);
      //                                   }
      //                                 })
      //                               ->where(function($query) use ($search) {
      //                                   $query->where('policy_no', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('policy_holder', 'LIKE', '%' . $search . '%');
      //                                 })
      //                               ->orderBy($result['sort'], $result['order'])
      //                               ->paginate($per);
            
      //         $total_premium = 0;
      //         $total_gross = 0;
      //         $total_agent = 0;
      //         $total_mgt = 0;
      //         foreach ($rows as $key => $field) {
      //           $paginate .= '<tr data-id="' . $field->id . '"><td>' . $field->policy_no . '</td>' .
      //             '<td>' . strtoupper($field->policy_holder) . '</td>' .
      //             '<td>' . $field->compo_code . '</td>' .
      //             '<td>' . $field->product_name . '</td>' .
      //             '<td>' . $field->cat_name . '</td>' .
      //             '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
      //             '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
      //             '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td>' .
      //             '<td class="rightalign">' . number_format($field->manage_share, 2) . '</td></tr>';
      //           $total_premium += $field->net_prem_paid;
      //           $total_gross += $field->gross_revenue;
      //           $total_agent += $field->agent_banding;
      //           $total_mgt += $field->manage_share;
      //         }
      //         $paginate .= '<tr style="border-top: 2px solid #555B63;">' .
      //           '<td><strong>TOTAL</strong></td>' .
      //           '<td></td>' .
      //           '<td></td>' .
      //           '<td></td>' .
      //           '<td></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_premium, 2) . '</strong></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_gross, 2) . '</strong></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_agent, 2) . '</strong></td>' .
      //           '<td class="rightalign"><strong>' . number_format($total_mgt, 2) . '</strong></td>' .
      //           '</tr>';
            
      //     }
      //   }

      //   // return response (format accordingly)
      //   if(Request::ajax()) {
      //     $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
      //     $result['rows'] = $rows->toArray();
      //     $result['paginate'] = $paginate;
      //     return Response::json($result);
      //   } else {
      //     $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
      //     $result['rows'] = $rows;
      //     $result['paginate'] = $paginate;
      //     return $result;
      //   }
    }

    public function getUserPolicies() {

      // dd(Request::all());
      // $import = Batch::where(function($query) {
      //                       $query->where('status', 1)
      //                             ->orWhere('status', 0);
      //                   })->orderBy('created_at', 'desc')->first();

      // $batches = Batch::where(function($query) {
      //                     $query->where('status', 1)
      //                           ->orWhere('status', 0);
      //                 })->orderBy('created_at', 'desc')->get();

      // $this->data['export'] = null;
      // if ($import) {
      //   $this->data['export'] = $import->id;
      // }


      if (Request::input('date')) {
        $today_month = Request::input('date');
        
        $start = Carbon::createFromFormat('M Y', Request::input('date'))->startOfMonth()->format('Y-m-d');
        $end = Carbon::createFromFormat('M Y', Request::input('date'))->endOfMonth()->format('Y-m-d');
      } else {

        $batches = Batch::where(function($query) {
                            $query->where('status', 1)
                                  ->orWhere('status', 0);
                        })->orderBy('created_at', 'desc')->first();

        if ($batches) {
          $today_month = Carbon::createFromFormat('Y-m-d H:i:s', $batches->batch_date)->format('M Y');

          $start = Carbon::createFromFormat('Y-m-d H:i:s', $batches->batch_date)->startOfMonth()->format('Y-m-d');
          $end = Carbon::createFromFormat('Y-m-d H:i:s', $batches->batch_date)->endOfMonth()->format('Y-m-d');
        } else {
          $today_month = Carbon::now()->format('M Y');

          $start = Carbon::now()->startOfMonth()->format('Y-m-d');
          $end = Carbon::now()->endOfMonth()->format('Y-m-d');
        }

      }

      // dd($today_month);
      $rows = '<div class="policies-table"><table class="table table-striped">
              <input type="hidden" id="search-id" value="' . Request::input('id') . '">
              <div class="form-group">
                  <label for="" class="col-lg-1 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left; padding: 0;"><p style="padding: 6px;" class="p"><small>DATE RANGE</small></p></label>
                  <div class="col-lg-3 col-md-6 col-sm-8 col-xs-6" style="padding-left:0; padding-bottom: 20px;">
                    <div>
                        <div class="input-group date filter_status">
                          <input class="form-control input-xs" readonly="readonly" style="background:white;" name="filter_status" id="row-filter_status" type="text" value="' . $today_month . '">
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                    </div>
                </div>
              </div>' .
              '<thead>' . 
                '<th class="tbheader" nowrap data-sort="policy_no"><i></i> POLICY NO</th>' .
                '<th class="tbheader" nowrap data-sort="policy_holder"><i></i> POLICY OWNER NAME</th>' .
                '<th class="tbheader" nowrap data-sort="product_name"><i></i> PRODUCT NAME</th>' .
                '<th class="tbheader" nowrap data-sort="cat_name"><i></i> POLICY<br>TYPE</th>' .
                '<th class="tbheader" nowrap data-sort="billing_freq"><i></i> PAYMENT<br>FREQUENCY</th>' .
                '<th class="tbheader" nowrap data-sort="premium_freq_mtd"><i></i> APE</th>' .
                '<th class="tbheader" nowrap data-sort="net_prem_paid"><i></i> PREMIUM</th>' .
                '<th class="tbheader" nowrap data-sort="gross_revenue"><i></i> GROSS REV AMT</th>' .
                '<th class="tbheader" nowrap data-sort="agent_banding"><i></i> AGENT SHARE</th>' .
                '<th class="tbheader" nowrap data-sort="manage_share"><i></i> MGT SHARE</th>' .
              '</thead><tbody>';

      if (Request::input('id')) {

        $total_premium = 0;
        $total_gross = 0;
        $total_agent = 0;
        $total_mgt = 0;
        $total_svi = 0;

        $check_owner = Group::where('user_id', Request::input('id'))->where('status', 1)->first();
        $check_supervisor = SalesSupervisor::where('user_id', Request::input('id'))->first();

        $rows = '<div class="policies-table"><table class="table table-striped">
                <input type="hidden" id="search-id" value="' . Request::input('id') . '">
                <div class="form-group">
                    <label for="" class="col-lg-1 col-md-6 col-sm-4 col-xs-6 control-label" style="text-align:left; padding: 0;"><p style="padding: 6px;" class="p"><small>DATE RANGE</small></p></label>
                    <div class="col-lg-3 col-md-6 col-sm-8 col-xs-6" style="padding-left:0; padding-bottom: 20px;">
                      <div>
                          <div class="input-group date filter_status">
                            <input class="form-control input-xs" readonly="readonly" style="background:white;" name="filter_status" id="row-filter_status" type="text" value="' . $today_month . '">
                            <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                          </div>
                      </div>
                  </div>
                </div>' .
                '<thead>' . 
                  '<th class="tbheader" nowrap data-sort="policy_no"><i></i> POLICY NUMBER</th>' .
                  '<th class="tbheader" nowrap data-sort="policy_holder"><i></i> POLICY OWNER NAME</th>' .
                  '<th class="tbheader" nowrap data-sort="product_name"><i></i> PRODUCT NAME</th>' .
                  '<th class="tbheader" nowrap data-sort="cat_name"><i></i> POLICY<br>TYPE</th>' .
                  '<th class="tbheader" nowrap data-sort="net_prem_paid"><i></i> PREMIUM</th>' .
                  '<th class="tbheader" nowrap data-sort="billing_freq"><i></i> PAYMENT<br>FREQUENCY</th>' .
                  '<th class="tbheader" nowrap data-sort="premium_freq_mtd"><i></i> APE</th>' .
                  '<th class="tbheader" nowrap data-sort="gross_revenue"><i></i> GROSS REV AMT</th>' .
                  '<th class="tbheader" nowrap data-sort="agent_banding"><i></i> AGENT SHARE</th>';
        if ($check_owner || $check_supervisor) {
          $rows .= '<th class="tbheader" nowrap data-sort="manage_share"><i></i> MGT SHARE</th>';
        }

        $rows .= '</thead><tbody>';

        $comp = PayrollComputation::select('products.product_name', 'payroll_computations.policy_no',  'payroll_computations.agent_banding',
                                'payroll_computations.manage_share', 'payroll_computations.comp_code as compo_code', 'payroll_computations.premium_freq_mtd',
                                'payroll_computations.billing_freq', 'payroll_computations.tier2_share', 'payroll_computations.tier3_share', 
                                'payroll_computations.product_id', 'payroll_computations.gross_revenue', 'payroll_computations.policy_holder', 'payroll_computations.premium as net_prem_paid','payroll_computations.variable_income','provider_classifications.name as cat_name')
                                ->leftJoin('products', 'products.id', '=', 'payroll_computations.product_id')
                                ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'payroll_computations.category_id')
                                ->where('user_id', Request::input('id'))
                                ->where(function ($query) use ($start, $end) {
                                    $query->where('batch_date', '>=', $start . ' 00:00:00')
                                          ->where('batch_date', '<=', $end . ' 00:00:00');
                                  })
                                ->orderBy('policy_no', 'asc')
                                ->get();

        foreach ($comp as $key => $field) {
          $rows .= '<tr data-id="' . $field->id . '"><td>' . $field->policy_no . '</td>' .
            '<td>' . strtoupper($field->policy_holder) . '</td>' .
            '<td>' . $field->product_name . '</td>' .
            '<td>' . $field->cat_name . '</td>' .
            '<td class="rightalign">' . number_format($field->net_prem_paid, 2) . '</td>' .
            '<td>' . ucwords($field->billing_freq) . '</td>' .
            '<td class="rightalign">' . number_format($field->premium_freq_mtd, 2) . '</td>' .
            '<td class="rightalign">' . number_format($field->gross_revenue, 2) . '</td>' .
            '<td class="rightalign">' . number_format($field->agent_banding, 2) . '</td>';

          if ($check_owner) {
            $rows .= '<td class="rightalign">' . number_format($field->tier3_share, 2) . '</td>';
            $total_mgt += $field->tier3_share;
          } else if ($check_supervisor) {
            $rows .= '<td class="rightalign">' . number_format($field->tier2_share, 2) . '</td>';
            $total_mgt += $field->tier2_share;
          }

          $total_premium += $field->net_prem_paid;
          $total_gross += $field->gross_revenue;
          $total_agent += $field->agent_banding;

          $total_svi += $field->premium_freq_mtd;

          $rows .= '</tr>';
        }

        if (count($comp) > 0) {
          $rows .= '<tr style="border-top: 2px solid #555B63;" class="hide">' .
            '<td><strong>TOTAL</strong></td>' .
            '<td></td>' .
            '<td></td>' .
            '<td></td>' .
            '<td class="rightalign"><strong>' . number_format($total_premium, 2) . '</strong></td>' .
            '<td></td>' .
            '<td class="rightalign"><strong>' . number_format($total_svi, 2) . '</strong></td>' .
            '<td class="rightalign"><strong>' . number_format($total_gross, 2) . '</strong></td>' .
            '<td class="rightalign"><strong>' . number_format($total_agent, 2) . '</strong></td>';

          if ($check_owner || $check_supervisor) {
            $rows .= '<td class="rightalign"><strong>' . number_format($total_mgt, 2) . '</strong></td>';
          }

          $rows .= '</tr>';
        }
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    /**
     * Posts
     *
     * @return void
     */
    public function getPolicyPolicies() {

      $policy = Request::input('policy');
      $compo_code = Request::input('compo_code') ?: '';
      // dd(Request::all());
      $rows = '<div class="policies-table"><table class="table table-striped">' .
              '<thead>' . 
              '<th class="th-sort tbheader" data-sort="policy_no"><i></i> POLICY NO</th>' .
              '<th class="th-sort tbheader" data-sort="name"><i></i> POLICY HOLDER</th>' .
              '<th class="th-sort tbheader" data-sort="compo_code"><i></i> COMPONENT CODE</th>' .
              '<th class="th-sort tbheader" data-sort="cat_name"><i></i> POLICY TYPE</th>' .
              '<th class="th-sort tbheader" data-sort="incept_date"><i></i> UPLOAD DATE</th>' .
              '<th class="th-sort tbheader" data-sort="net_prem_paid"><i></i> PREMIUM</th>' .
              '<th class="tbheader"data-sort="fyc"><i></i> FYC</th>' .
              '<th class="tbheader"data-sort="renewal"><i></i> RENEWAL</th>' .
              '<th class="th-sort tbheader" data-sort="name"><i></i> AGENT NAME</th>' .
              '<th class="th-sort tbheader" data-sort="consent_mail"><i></i> MAIL</th>' .
              '<th class="th-sort tbheader" data-sort="consent_telephone"><i></i> TELEPHONE</th>' .
              '<th class="th-sort tbheader" data-sort="consent_sms"><i></i> SMS</th>' .
              '<th class="tbheader rightalign">TOOLS</th>' .
              '</thead><tbody>';

      // $get_policy = Policy::with('getPolicySales')->with('getPolicyCategoryInfo')
      //                     ->select('policies.*', 'production_cases.consent_mail', 'production_cases.consent_sms', 'production_cases.consent_telephone')
      //                     ->leftJoin('production_cases', function ($join) {
      //                           $join->on('policies.policy_nric', '=', 'production_cases.owner_id')
      //                                ->where('production_cases.case', '=', 'verified');
      //                       })
      //                     ->where(function($query) use ($policy, $compo_code) {
      //                       $query->where('contract_no', '=', $policy)
      //                             ->where('status', '=', 1)
      //                             ->where('upload_id', '=', Request::input('upload'))
      //                             ->where('compo_code', 'LIKE', '%' . $compo_code . '%');
      //                   })->orderBy('created_at', 'desc')->get();

      $get_policy = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_classifications.name as cat_name',
                                    'production_cases.consent_mail', 'production_cases.consent_sms', 'production_cases.consent_telephone')
                                    ->leftJoin('users', 'users.id', '=', 'policies.user_id')
                                    ->leftJoin('provider_classifications', 'provider_classifications.id', '=', 'policies.category_id')
                                    ->leftJoin('production_cases', function ($join) {
                                          $join->on('policies.policy_nric', '=', 'production_cases.owner_id')
                                               ->where('production_cases.case', '=', 'verified');
                                      })  
                                    ->where(function($query) use ($policy, $compo_code) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.upload_id', '=', Request::input('upload'))
                                            ->where('policies.compo_code', 'LIKE', '%' . $compo_code . '%')
                                            ->where('policies.contract_no', '=', $policy);
                                      })->orderBy('created_at', 'desc')->get();

      foreach ($get_policy as $key => $field) {
        $rows .= '<tr>' .
                 '<td>' .  $field->contract_no . '</td>' .
                 '<td>' .  $field->name . '</td>' .
                 '<td>' .  $field->compo_code . '</td>' .
                 '<td>' .  $field->getPolicyCategoryInfo->name . '</td>' .
                 '<td>' .  date_format(date_create(substr($field->upload_date, 0,10)),'d/m/Y') . '</td>' .
                 '<td>' .  number_format($field->net_prem_paid, 2) . '</td>';

        $rows .= '<td>';
                      $date_year = substr($field->upload_date, 0, 4);
                      $first_year_com = Carbon::now()->format('Y') - $date_year;

                      if ($first_year_com <= 1) {
                                $rows .= 'YES';
                      } else if ($first_year_com >= 2) {
                                $rows .= 'NO';
                      } else {
                                $rows .= 'NO';
                      }
        $rows .= '</td>';

        $rows .= '<td>';

                      $date_year = substr($field->upload_date, 0, 4);
                      $first_year_com = Carbon::now()->format('Y') - $date_year;

                      if ($first_year_com <= 1) {
                                $rows .= 'YES';
                      } else if ($first_year_com >= 2) {
                                $rows .= 'NO';
                      } else {
                                $rows .= 'NO';
                      }

        $rows .= '</td>' .
                  '<td>' .  $field->name . '</td>' .
                  '<td>' . ($field->consent_mail == 1 ? ' <i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>' ) . '</td>' .
                  '<td>' . ($field->consent_sms == 1 ? ' <i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>' ) . '</td>' .
                  '<td>' . ($field->consent_telephone == 1 ? ' <i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>' ) . '</td>';

        $rows .= '<td class="rightalign">' .
                      '<a href="' . url('policy/' . $field->id . '/view') . '" type="button" class="btn-table btn btn-xs btn-default"><i class="fa fa-eye"></i></a>' .
                      ' <button type="button" class="btn btn-xs btn-table" data-toggle="modal" data-target="#myModaldeactivate"><i class="fa fa-adjust"></i></button>' .
                      ' <input id="checkbox" type="checkbox" value="">' .
                    '</td>' .
                 '</tr>';
      }

      $rows .= '</tbody></table><br></div>';

      return Response::json($rows);
    }

    /**
     * Viewing a policy.
     *
     * @return Response
     */
    public function view($id)
    {
        $result = $this->doView($id);
        
        $this->data['policy'] = $result['policy'];
        $this->data['row'] = $result['row'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Policy Management";
        $this->data['settings'] = Settings::first();
        return view('policy-management.view', $this->data);
    }

    public function doView($id) {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $policy = Policy::find($id);
        $row = null;

        //dd($policy->id);
        if ($policy->user_id) {
            //get sales information
            $row = User::with('salesInfo')->with('salesInfo.designations')->find($policy->user_id);
        }

        //dd($policy);
        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $policy->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['policy'] = $policy->toArray();
            $result['row'] = $row->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $policy->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['policy'] = $policy;
            $result['row'] = $row;
            return $result;
        }
    }

    /**
     * Edit a policy.
     *
     * @return Response
     */
    public function edit()
    {
    

        $this->data['title'] = "Policy Management";
        return view('policy-management.edit', $this->data);
    }

    /**
     * Display a listing of the policy.
     *
     * @return Response
     */
    public function indexRider()
    {   
        $result = $this->doListRider();

        $this->data['title'] = "Policies - Riders";
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.riders.list', $this->data);
    }
    
    /**
     * Build the list
     *
     */
    public function doListRider() {
        $result['sort'] = Request::input('sort') ?: 'policies.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $per = Request::input('per') ?: 10;
        
        $import = UploadFeed::orderBy('created_at', 'desc')->first();

        if (!$import) {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) {
                                      $query->where('policies.status', '=', 1);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);


          // return response (format accordingly)
          if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
          } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
          }
        }

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          if (Auth::user()->usertype_id == 8) {

            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);

          } else {

            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);

                                    // $rows = Policy::groupBy('policies.contract_no')->havingRaw('sum(count) > 1')->selectRaw('sum(count) as rider')->take(10)->get();
                                    // dd($rows);
            // dd($rows);
          }
        } else {
          if (Auth::user()->usertype_id == 8) {
            $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          } else {
            $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          }

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          if (Auth::user()->usertype_id == 8) {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          } else {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          }
        }

                                    // dd($rows);
        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
        }
    }


    /**
     * Viewing a rider.
     *
     * @return Response
     */
    public function viewRider()
    {
        $this->data['title'] = "Policy Management";
        return view('policy-management.riders.view', $this->data);
    }

    /**
     * Edit a rider.
     *
     * @return Response
     */
    public function editRider()
    {
        $this->data['title'] = "Policy Management";
        return view('policy-management.riders.edit', $this->data);
    }

    /**
     * Display a listing of the policy introducers.
     *
     * @return Response
     */
    public function indexIntroducer()
    {
        $result = $this->doListIntroducer();
        $this->data['rows'] = $result['rows'];
        $this->data['sales'] = $result['sales'];
        $this->data['title'] = "Introducers";

        $get_policy = Policy::where(function($query) {
                              $query->where('user_id', '=', Auth::user()->id)
                                    ->where('status', '=', 1);
                          })->lists('provider_id');

        $this->data['provider_user'] = Provider::whereIn('id', $get_policy)->get();
        
      
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.introducers.list', $this->data);
    }

    public function doListIntroducer() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $rows = Introducer::with('getUser')
                            ->where('status', '!=', 2)
                            ->orderBy($result['sort'], $result['order'])
                            ->get();

        $sales = User::where(function($query) {
                            $query->where('usertype_id', '=', 8)
                                  ->where('status', '=', 1);
                        })->with('salesInfo')->get();

        $ids = AssignedPolicy::all()->lists('policy_id');

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            $result['sales'] = $sales->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            $result['sales'] = $sales;
            return $result;
        }
    }

    /**
     * Get Available Policies.
     *
     * @return Response
     */
    public function getPolicy()
    {
        $designation_id = Request::input('designation_id');

        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $ids = AssignedPolicy::all()->lists('policy_id');

        // get all available agents
        $rows = Policy::with('getPolicyProvider')->where('user_id', '>', 0)->whereNotIn('id', $ids)->get();
        
        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Get Available Policies.
     *
     * @return Response
     */
    public function getProviders()
    {
      if (Request::input('id')) {
      $get_user = Introducer::find(Request::input('id'));
      $get_policy = Policy::where(function($query) use ($get_user) {
                            $query->where('user_id', '=', $get_user->user_id)
                                  ->where('status', '=', 1);
                        })->lists('provider_id');
      } else if  (Request::input('user')) {
      $get_policy = Policy::where(function($query) {
                            $query->where('user_id', '=', Request::input('user'))
                                  ->where('status', '=', 1);
                        })->lists('provider_id');

      }
      $ctr = 0;
      $provider_id = [];

      foreach (array_unique($get_policy->toArray()) as $key => $value) {
        $provider_id[$ctr] = $value;
        $ctr += 1;  
      }

      $pctr = 0;
      $provider = [];
      foreach ($provider_id as $key => $prov_id) {
        
        $provider[$pctr] = Provider::where(function($query) use ($prov_id) {
                            $query->where('id', '=', $prov_id)
                                  ->where('status', '=', 1);
                        })->first();
        $pctr += 1;
      }

      $rows = '<option class="hide" value="">Select:</option>';
      foreach ($provider as $key => $field) {
        $rows .= '<option value="' . $field->id . '">' . $field->name . '</option>';
      }
      
      return Response::json($rows);
    }

    /**
     * Get Available Policies.
     *
     * @return Response
     */
    public function getPolicies()
    {
      //dd(Request::all());
        $user = 0;
      if (Request::input('id')) {
         $get_user = Introducer::find(Request::input('id'));
         $user = $get_user->user_id;
      } else if  (Request::input('user')) {
         $user = Request::input('user');
      }

      $provider = Request::input('provider');

      $get_used_policy = AssignedPolicy::lists('policy_no');

      $get_policy = Policy::where(function($query) use ($user, $provider) {
                      $query->where('user_id', '=', $user)
                            ->where('provider_id', '=', $provider)
                            ->where('status', '=', 1);
                  })->whereNotIn('contract_no', $get_used_policy)->lists('contract_no');

      $rows = '<option class="hide" value="">Select:</option>';
      foreach (array_unique($get_policy->toArray()) as $key => $field) {
        $rows .= '<option value="' . $field . '">' . $field . '</option>';
      }

      return Response::json($rows);
    }

    /**
     * Viewing an introducer.
     *
     * @return Response
     */
    public function viewIntroducer()
    {
        $this->data['title'] = "Policy Management";
        return view('policy-management.introducers.view', $this->data);
    }

    /**
     * Edit an introducer.
     *
     * @return Response
     */
    public function editIntroducer()
    {
        $this->data['title'] = "Policy Management";
        return view('policy-management.introducers.edit', $this->data);
    }

    /**
     * Display a listing of the policy history.
     *
     * @return Response
     */
    public function indexHistory()
    {   

        $result= $this->doListPolicyLogs();
        $this->data['rows'] = $result['rows'];

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Policy Log";
        return view('policy-management.policy-history.list', $this->data);
    }
    /**
     * Fetch the policy logs
     *
     * @return Response
     */
    public function doListPolicyLogs() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $rows = LogPolicy::with('policyLog')
                         ->with('policyInfo')
                         ->get();

        //dd($rows);
        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }
    /**
     * Display a listing of the policy orphans.
     *
     * @return Response
     */
    public function indexOrphan()
    {
        $result = $this->doListOrphan();
        
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['rows'] = $result['rows'];
        $this->data['refresh_route'] = url('policy/orphan-policies/refresh');

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        $this->data['title'] = "Orphan Policy Service";
        return view('policy-management.orphan-policies.list', $this->data);
    }

    public function doListOrphan() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'policies.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $per = Request::input('per') ?: 10;

      $import = UploadFeed::orderBy('created_at', 'desc')->where('status', 2)->first();

      if (!$import) {
        $rows = Policy::select('policies.id','policies.contract_no','providers.name','policies.incept_date')
                      ->join('providers','providers.id','=','policies.provider_id')
                      ->where('user_id', '=', null)
                      ->where(function($query) use ($search) {
                          $query->where('contract_no', 'LIKE', '%' . $search . '%');
                        })
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate($per);

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
        }
      }

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Policy::select('policies.id','policies.contract_no','providers.name','policies.incept_date')
                      ->join('providers','providers.id','=','policies.provider_id')
                      ->where('user_id', '=', null)
                      ->where(function($query) use ($search, $import) {
                          $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                ->where('upload_id', '=', $import->id);
                        })
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate($per);
      } else {
        $count = Policy::select('policies.id','policies.contract_no','providers.name','policies.incept_date')
                      ->join('providers','providers.id','=','policies.provider_id')
                      ->where('user_id', '=', null)
                      ->where(function($query) use ($search, $import) {
                          $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                ->where('upload_id', '=', $import->id);
                        })
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
          return ceil($count->total() / $per);
        });
          
        $rows = Policy::select('policies.id','policies.contract_no','providers.name','policies.incept_date')
                      ->join('providers','providers.id','=','policies.provider_id')
                      ->where('user_id', '=', null)
                      ->where(function($query) use ($search, $import) {
                          $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                ->where('upload_id', '=', $import->id);
                        })
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
        $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
        $result['rows'] = $rows->toArray();
        return Response::json($result);
      } else {
        $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
        $result['rows'] = $rows;
        return $result;
      }
    }

    /**
     * Viewing an orphan policy.
     *
     * @return Response
     */
    public function viewOrphan()
    {    $this->data['refresh_route'] = url('policy/orphan-policies/refresh');
        $this->data['title'] = "Policy Management";
        return view('policy-management.orphan-policies.view', $this->data);
    }

    /**
     * Editing an orphan policy.
     *
     * @return Response
     */
    public function editOrphan()
    {   
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['title'] = "Policy Management";

                 if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.orphan-policies.edit', $this->data);
    }
    public function moveOrphan()
    {
           $result = $this->doListOrphan();

         $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['rows'] = $result['rows'];
        $this->data['refresh_route'] = url('policy/orphan-policies/refresh');

        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();

           //trigger notification
        $row_noti = new Notification;
        $row_noti->user_id =  Auth::user()->id;
        $row_noti->activity=  " Moved orphan policy!";
        $row_noti->controller =  "PolicyController@notiOrphan";
         

        if (Auth::user()->usertype_id == '8') {//if sales user
         $row_noti->usertype_id = "2"; //sales user
        }
        else{
          $row_noti->usertype_id = "1"; //system user
        }
        $row_noti->save();
        //save to per user
        $row_noti  = Notification::orderBy('id', 'desc')->first();
        if (Auth::user()->usertype_id == '8') {
        $row_user = User::where(function($query) {
                                 $query->where('status', '=', '1')
                                 ->where('usertype_id', '<=', '7');
                                })->get();
       }
        else
       {
        $row_user = User::where(function($query) {
                                  $query->where('status', '=', '1')
                                 ->where('usertype_id', '=', '8');
                                 })->get();
       }
        foreach ($row_user as $key => $field) {      
                  $rows = new NotificationPerUser;
                  $rows->user_id = $field->id;
                  $rows->notification_id = $row_noti->id;
                  $rows->save();
                 }          


        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
        
         $this->data['noti_count'] = count($this->data['notification']);
          $this->data['title'] = "Policy Management";
          return view('policy-management.orphan-policies.list', $this->data);
    }
    /**
     * Display a listing of the policy.
     *
     * @return Response
     */
    public function indexDuplicate()
    {   
        $result = $this->doListDuplicate();
        $this->data['title'] = "Duplicates";
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.duplicates.list', $this->data);
    }
    
    /**
     * Build the list
     *
     */
    public function doListDuplicate() {
        // sort and order
        $result['sort'] = Request::input('sort') ?: 'policies.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $per = Request::input('per') ?: 10;

        $import = UploadFeed::orderBy('created_at', 'desc')->first();

        if (!$import) {
          $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no')
                                ->join('users', 'users.id', '=', 'policies.user_id')
                                ->groupBy('contract_no')
                                ->where(function($query) {
                                  $query->where('duplicate', '=', '1')
                                        ->where('policies.status', '=', '1');
                                  })
                                ->where(function($query) use ($search) {
                                    $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                          ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('compo_code')
                                ->paginate($per);


          // return response (format accordingly)
          if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
          } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
          }
        }

        if (Request::input('page') != '»') {

          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no')
                                ->join('users', 'users.id', '=', 'policies.user_id')
                                ->groupBy('contract_no')
                                ->where(function($query) use ($import) {
                                  $query->where('duplicate', '=', '1')
                                        ->where('policies.status', '=', '1')
                                        ->where('policies.upload_id', '=', $import->id);
                                  })
                                ->where(function($query) use ($search) {
                                    $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                          ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('compo_code')
                                ->paginate($per);
                                  // dd($rows);
        } else {
          $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no')
                                ->join('users', 'users.id', '=', 'policies.user_id')
                                ->groupBy('contract_no')
                                ->where(function($query) use ($import) {
                                  $query->where('duplicate', '=', '1')
                                        ->where('policies.status', '=', '1')
                                        ->where('policies.upload_id', '=', $import->id);
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('compo_code')
                                ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no')
                                ->join('users', 'users.id', '=', 'policies.user_id')
                                ->groupBy('contract_no')
                                ->where(function($query) use ($import) {
                                  $query->where('duplicate', '=', '1')
                                        ->where('policies.status', '=', '1')
                                        ->where('policies.upload_id', '=', $import->id);
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('compo_code')
                                ->paginate($per);
        }

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
        }
    }
    /**
     * Viewing a duplicate policy.
     *
     * @return Response
     */
    public function viewDuplicate()
    {
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
      $this->data['title'] = "Policy Management";
      return view('policy-management.duplicates.view', $this->data);
  }

    /**
     * Edit a duplicate policy.
     *
     * @return Response
     */
    public function editDuplicate()
    {    
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
      $this->data['title'] = "Policy Management";
      return view('policy-management.duplicates.edit', $this->data);
    }
    
    /**
     * Display a listing of the case submision.
     *
     * @return Response
     */
    public function indexCaseSubmission()
    {
      $result = $this->doListCaseSubmission();
      $this->data['rows'] = $result['rows'];

      $this->data['now'] = strval(Carbon::now()->format('Y-m-d'));

      // Excel::load(storage_path('case/book.xlsx'), function($reader) {
      //   $results = $reader->all();
      //   dd($results);
      // });
      // dd(
      // ProductionCases::where('created_at', '>=', '2016-01-01 00:00:00')
      //                 ->where('created_at', '<=', '2016-07-31 00:00:00')
      //                 ->get()->take(20)
      //   );

      // Excel::load(storage_path('case/AVIVAAngela2.xlsx'), function($reader) {
      //   $results = $reader->all();
      //   // dd($results);

      //   foreach ($results as $key => $field) {
      //     $row = new ProductionCases;
      //     $row->life_firstname = trim(preg_replace('/\s+/',' ', $field->life_assured_gname));
      //     $row->life_lastname = trim(preg_replace('/\s+/',' ', $field->life_assured_fname));
      //     $row->life_id = trim(preg_replace('/\s+/',' ', $field->life_assured_ic));
      //     $row->life_occupation = trim(preg_replace('/\s+/',' ', $field->life_assured_occupation));
      //     $row->life_dob = trim(preg_replace('/\s+/',' ', $field->life_assured_dob));
      //     $row->owner_dob = trim(preg_replace('/\s+/',' ', $field->dob));
      //     $row->life_age = trim(preg_replace('/\s+/',' ', $field->life_assured_anb));
      //     $row->life_gender = trim(preg_replace('/\s+/',' ', $field->life_assured_gender));

      //     $row->firstname = trim(preg_replace('/\s+/',' ', $field->policyowner_gname));
      //     $row->lastname = trim(preg_replace('/\s+/',' ', $field->policyowner_fname));
      //     $row->owner_id = trim(preg_replace('/\s+/',' ', $field->nric));
      //     $row->owner_occupation = trim(preg_replace('/\s+/',' ', $field->occupation));
      //     $row->life_dob = trim(preg_replace('/\s+/',' ', $field->life_assured_dob));
      //     $row->owner_age = trim(preg_replace('/\s+/',' ', $field->age_next_bday));
      //     $row->owner_gender = trim(preg_replace('/\s+/',' ', $field->gender));

      //     if ($field->income_range) {
      //       $row->annual_income_range = $field->income_range;
      //     }

      //     $row->postal_code = trim(preg_replace('/\s+/',' ', $field->postalcode));
      //     $row->address = trim(preg_replace('/\s+/',' ', $field->address));
      //     $row->email = trim(preg_replace('/\s+/',' ', $field->email_address));
      //     $row->source = trim(preg_replace('/\s+/',' ', $field->source));

      //     $get_main = ProductMain::where('name', '=', trim(preg_replace('/\s+/',' ', $field->plan_name)))->where('provider_id', 1)->first();
      //     if ($get_main) {
      //       $row->main_product_id = $get_main->id;
      //       $row->currency_value = trim(preg_replace('/\s+/',' ', $field->premium_amount));
      //       $row->currency_value2 = trim(preg_replace('/\s+/',' ', $field->premium_amount));
      //       $row->annual_currency_value2 = trim(preg_replace('/\s+/',' ', $field->premium_amount)) * 12;
      //       $row->conv_total = trim(preg_replace('/\s+/',' ', $field->premium_amount));
      //       $row->life_total_amount = trim(preg_replace('/\s+/',' ', $field->premium_amount));
      //       $row->ape = trim(preg_replace('/\s+/',' ', $field->annualized_premium));
      //     }

      //     $row->main_sum_assured = trim(preg_replace('/\s+/',' ', $field->sum_assured));
      //     $row->provider_list = 1;
      //     $row->currency = "127 SGD";
      //     $row->currency2 = "127 SGD";
      //     $row->conversion_rate = 1;
      //     $row->conversion_year = "none";

      //     if (is_numeric($field->policy_term)) {
      //       $row->policy_term = trim(preg_replace('/\s+/',' ', $field->policy_term));
      //     }

      //     $get_user = User::where('code', '=', $field->agent_no)->first();
      //     if (!$get_user) {
      //       dd($field->agent_no);
      //     }
      //     $row->user_id = $get_user->id;


      //     if ($field->submitted_to_ho != "NULL") {
      //       $row->submitted_date = trim(preg_replace('/\s+/',' ', $field->submitted_to_ho));
      //       $row->submitted_ho = "Yes";
      //       $row->submitted_auth_id = 149;
      //     }

      //     $row->created_at = trim(preg_replace('/\s+/',' ', $field->submitted_date));
      //     $row->save();

      //     $tels = explode(',', trim(preg_replace('/\s+/',' ', $field->tel_no)));
      //     foreach ($tels as $tkey => $tvalue) {
      //       $tel = new ProductionCaseContact;
      //       $tel->production_id = $row->id;
      //       $tel->contact = $tvalue;
      //       $tel->save();
      //     }

      //   }
      // });

      // dd("success");
      // dd(ProductMain::where('name', '=', "Death")->where('provider_id', 2)->first());
        // Excel::load(storage_path('case/AXAAngela.xlsx'), function($reader) {
        // $results = $reader->all();
        // // dd($results);
        // foreach ($results as $key => $field) {
        //   $row = new ProductionCases;
        //   $row->firstname = trim(preg_replace('/\s+/',' ', $field->name));
        //   $row->owner_id = trim(preg_replace('/\s+/',' ', $field->nricpassport));
        //   $row->owner_occupation = trim(preg_replace('/\s+/',' ', $field->occupation));
        //   $row->owner_dob = trim(preg_replace('/\s+/',' ', $field->dob));
        //   $row->owner_gender = trim(preg_replace('/\s+/',' ', $field->gender));
        //   $row->address = trim(preg_replace('/\s+/',' ', $field->residential_add));
        //   $row->email = trim(preg_replace('/\s+/',' ', $field->email_add));

        //   if ($field->annual_income_range) {
        //     $row->annual_income_range = $field->annual_income_range;
        //   }

        //   $get_user = User::where('code', '=', $field->agent_code)->first();
        //   if (!$get_user) {
        //     dd($field->agent_code);
        //   }
        //   $row->user_id = $get_user->id;
        //   $row->save();

        //   $row->main_sum_assured = trim(preg_replace('/\s+/',' ', $field->sum_assured));
        //   $row->provider_list = 2;
        //   $row->currency = "127 SGD";
        //   $row->currency2 = "127 SGD";
        //   $row->conversion_rate = 1;
        //   $row->conversion_year = "none";
        //   $row->policy_term = trim(preg_replace('/\s+/',' ', $field->policy_term));

        //   $get_main = ProductMain::where('name', '=', trim(preg_replace('/\s+/',' ', $field->plans_recommended)))->where('provider_id', 2)->first();
        //   if ($get_main) {
        //     $row->main_product_id = $get_main->id;
        //     $row->currency_value = trim(preg_replace('/\s+/',' ', $field->premium));
        //     $row->currency_value2 = trim(preg_replace('/\s+/',' ', $field->premium));
        //     $row->annual_currency_value2 = trim(preg_replace('/\s+/',' ', $field->premium)) * 12;
        //     $row->conv_total = trim(preg_replace('/\s+/',' ', $field->premium));
        //     $row->life_total_amount = trim(preg_replace('/\s+/',' ', $field->premium));

        //     $row->ape = trim(preg_replace('/\s+/',' ', $field->premium));
        //   }


        //   $get_nat = Nationality::where('nationality', '=', trim(preg_replace('/\s+/',' ', $field->nationality)))->first();
        //   $row->nationality_id = $get_nat->id;

        //   $row->payment_frequency = trim(preg_replace('/\s+/',' ', $field->payment_frequency));
        //   $row->created_at = trim(preg_replace('/\s+/',' ', $field->date_submitted));
        //   $row->save();

        //   $tels = explode('/', trim(preg_replace('/\s+/',' ', $field->contact_details)));
        //   foreach ($tels as $tkey => $tvalue) {
        //     $tel = new ProductionCaseContact;
        //     $tel->production_id = $row->id;
        //     $tel->contact = $tvalue;
        //     $tel->save();
        //   }
        // }

        // });

      if(Auth::user()->usertype_id == 8){
        $this->data['sales_id'] = Auth::user()->id;

        $check_supervisor =SalesSupervisor::where('user_id', Auth::user()->id)->first();
        $this->data['supervisor'] = false;
        
        if ($check_supervisor) {
          $this->data['supervisor'] = true;
        }

      }else{
         $this->data['sales'] = $result['sales'];
      }
      
      $this->data['nations'] = Nationality::orderBy('country', 'asc')->get();
      $this->data['providers'] = Provider::where('status', 1)->get();

      $this->data['sgd_value'] = SettingsRate::where('code','=','SGD')->first();
      $this->data['pages'] = $result['pages'];
      $this->data['case'] = 'pending';
      $this->data['currencies'] = SettingsRate::where('status','=','2')->get();


      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
      $this->data['title'] = "Life Submission";

      if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
          $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
      return view('policy-management.production.case-submission.list', $this->data);
    }

    public function doListCaseSubmission()
    {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'production_cases.submission_date';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $start = null;
      $end = null;

      if ($status) {
        $start = Carbon::createFromFormat('M Y', $status)->startOfMonth();
        $end = Carbon::createFromFormat('M Y', $status)->endOfMonth();
      }
      // dd($status);
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        if (Auth::user()->usertype_id == 8) {

          $check_group = Group::where('user_id', Auth::user()->id)->first();
          if ($check_group) {
            $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

            $ids = [];
            // $ids[0] = Auth::user()->id;

            $get_supervisors_id = SalesSupervisor::where('group_id', $check_group->id)->lists('id');
            $get_supervisors = SalesSupervisor::where('group_id', $check_group->id)->where('user_id', '!=', Auth::user()->id)->lists('user_id');

            $get_advisors = SalesAdvisor::whereIn('supervisor_id', $get_supervisors_id)->lists('user_id');

            $ids = array_merge($ids, $get_supervisors->toArray());
            $ids = array_merge($ids, $get_advisors->toArray());
            // dd($ids);
          } else {
            $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

            $ids = [];
            $ctr = 0;
            // $ids[0] = Auth::user()->id;

            if ($check_supervisor) {
              $get_advisors = SalesAdvisor::where('supervisor_id', $check_supervisor->id)->lists('user_id');

              foreach ($get_advisors as $key => $value) {
                $ids[$ctr] = $value;
                $ctr += 1;
              }
            }
          }
        }

        if (Auth::user()->usertype_id == 8) {
          if ($check_supervisor) {

            $rows = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.life_id',
                    'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname',
                    'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date',
                    'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date',
                    'review_id.name as review_by', 'nationalities.nationality', 'production_cases.policy_term',
                    'production_cases.mode_of_payment', 'production_cases.consent_telephone','production_cases.case',
                    'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 
                    'production_cases.source','production_cases.annual_currency_value2', 'production_cases.owner_dob', 
                    'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date',
                    'users.code as agent_code','production_cases.submitted_ho', 'production_cases.submitted_date', 
                    'submitted_id.name as submitted_by', 'production_cases.currency_value2', 'production_cases.consent_marketing',
                    'production_cases.consent_servicing', 'auth_id.name as screened_by', 'production_cases.currency',
                    'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name',
                    'designations.designation', 'production_cases.consent_mail', 'production_cases.consent_sms', 'users.id as user_group',
                    'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.payment_frequency', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                      ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                      ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                      ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                      ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                      ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                      ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                      ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                      ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                      ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                      ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->with('getSettingsRate')
                                      ->where(function($query) {
                                          $query->where('production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search, $check_supervisor) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                        })
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                  ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                          }
                                        })
                                      ->where('production_cases.user_id', Auth::user()->id)
                                      ->orderBy('users.name', 'asc')
                                      ->orderBy($result['sort'], $result['order'])
                                      ->get();

            $body = '';
            $curr_user = "NA";

            foreach ($rows as $key => $row) {
              $body .= '<tr ' . ($row->user_group != $curr_user ? 'style="border-top: 2px solid #60be54;"' : '') . ' class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                          '<td>' . ($row->user_group != $curr_user ? $row->name : '') . ($row->user_id == Auth::user()->id && $row->user_group != $curr_user ? ' <i class="fa fa-user"></i>' : '') . '</td>' .
                          '<td>' . ucwords(strtolower($row->lastname)) . '</td>' .
                          '<td>' . $row->provider_name . '</td>' .
                          '<td>' . $row->product_name . '</td>' .
                          '<td>' . $row->main_sum_assured . '</td>' .
                          '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                          '<td>' . $row->payment_frequency . '</td>' .
                          '<td class="rightalign">' . number_format($row->ape, 2) . '</td>' .
                          '<td class="rightalign">' . ($row->selected_client == 1 ? 'YES' : 'NO') . '</td>' .
                          // '<td>' . $row->case . '</td>' .
                          '<td class="rightalign">' .
                            '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                            (Auth::user()->id == $row->user_id ? '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' : '') .
                          '</td>' .
                        '</tr>';

              $curr_user = $row->user_group;
            }

            $rows = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.life_id',
                    'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname',
                    'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date',
                    'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date',
                    'review_id.name as review_by', 'nationalities.nationality', 'production_cases.policy_term',
                    'production_cases.mode_of_payment', 'production_cases.consent_telephone','production_cases.case',
                    'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 
                    'production_cases.source','production_cases.annual_currency_value2', 'production_cases.owner_dob', 
                    'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date',
                    'users.code as agent_code','production_cases.submitted_ho', 'production_cases.submitted_date', 
                    'submitted_id.name as submitted_by', 'production_cases.currency_value2', 'production_cases.consent_marketing',
                    'production_cases.consent_servicing', 'auth_id.name as screened_by', 'production_cases.currency',
                    'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name',
                    'designations.designation', 'production_cases.consent_mail', 'production_cases.consent_sms', 'users.id as user_group',
                    'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.payment_frequency', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                      ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                      ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                      ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                      ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                      ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                      ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                      ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                      ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                      ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                      ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->with('getSettingsRate')
                                      ->where(function($query) {
                                          $query->where('production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search, $check_supervisor) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                        })
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                  ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                          }
                                        })
                                      ->whereIn('production_cases.user_id', $ids)
                                      ->orderBy('users.name', 'asc')
                                      ->orderBy($result['sort'], $result['order'])
                                      ->get();

            // $body = '';
            // $curr_user = "NA";

            foreach ($rows as $key => $row) {
              $body .= '<tr ' . ($row->user_group != $curr_user ? 'style="border-top: 2px solid #60be54;"' : '') . ' class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                          '<td>' . ($row->user_group != $curr_user ? $row->name : '') . ($row->user_id == Auth::user()->id && $row->user_group != $curr_user ? ' <i class="fa fa-user"></i>' : '') . '</td>' .
                          '<td>' . ucwords(strtolower($row->lastname)) . '</td>' .
                          '<td>' . $row->provider_name . '</td>' .
                          '<td>' . $row->product_name . '</td>' .
                          '<td>' . $row->main_sum_assured . '</td>' .
                          '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                          '<td>' . $row->payment_frequency . '</td>' .
                          '<td class="rightalign">' . number_format($row->ape, 2) . '</td>' .
                          '<td class="rightalign">' . ($row->selected_client == 1 ? 'YES' : 'NO') . '</td>' .
                          // '<td>' . $row->case . '</td>' .
                          '<td class="rightalign">' .
                            '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                            (Auth::user()->id == $row->user_id ? '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' : '') .
                          '</td>' .
                        '</tr>';

              $curr_user = $row->user_group;
            }

            $result['pages'] = '';
            $result['rows'] = $body;
            $result['sales'] = null;

            if(Request::ajax()) {
              return Response::json($result);
            } else {
              return $result;
            }

          } else {
            $rows = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.life_id',
                    'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname',
                    'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date',
                    'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date',
                    'review_id.name as review_by', 'nationalities.nationality', 'production_cases.policy_term',
                    'production_cases.mode_of_payment', 'production_cases.consent_telephone','production_cases.case',
                    'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 
                    'production_cases.source','production_cases.annual_currency_value2', 'production_cases.owner_dob', 
                    'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date',
                    'users.code as agent_code','production_cases.submitted_ho', 'production_cases.submitted_date', 
                    'submitted_id.name as submitted_by', 'production_cases.currency_value2', 'production_cases.consent_marketing',
                    'production_cases.consent_servicing', 'auth_id.name as screened_by', 'production_cases.currency',
                    'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name',
                    'designations.designation', 'production_cases.consent_mail', 'production_cases.consent_sms',
                    'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                      ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                      ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                      ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                      ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                      ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                      ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                      ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                      ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                      ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                      ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->with('getSettingsRate')
                                      ->where(function($query) {
                                          $query->where('production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search, $check_supervisor) {
                                          if ($check_supervisor) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");

                                          } else {
                                            $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                          }
                                        })
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                  ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                          }
                                        })
                                      ->where('production_cases.user_id', Auth::user()->id)
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);

            $body = '';
            if(Request::ajax()) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">';

                if($check_supervisor) {
                  $body .= '<td>' . $row->name . '</td>';
                }

                $body .= '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                        '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status === "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                        '<td>' . $row->provider_name . '</td>' .
                        '<td>' . $row->product_name . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>';
                        // '<td>' . $row->case . '</td>';

                if ($row->selected_client == 1) {
                  $body .= '<td>YES</td>';
                } else {
                    $body .= '<td>NO</td>';
                }

                $body .= '<td class="rightalign">' .
                        '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                      '</td>' .
                    '</tr>';
              }
            }
          }

        } else {
          $rows = ProductionCases::select('nationalities.nationality', 'production_cases.id','production_cases.screened_status','production_cases.user_id', 'production_cases.life_id', 'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date', 'review_id.name as review_by',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 'production_cases.source','production_cases.annual_currency_value2',
                                            'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                            'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
                                            'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','nationalities.nationality', 'production_cases.case',
                  'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                    ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->with('getSettingsRate')
                                    ->where(function($query) {
                                        $query->where('production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhere('currency', 'LIKE', '%' . $search . '%')
                                                ->orWhere('mode_of_payment', 'LIKE', '%' . $search . '%')
                                                ->orWhere('source', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
                                    
          $body = '';
          if(Request::ajax()) {
            if (Auth::user()->usertype_id == 1) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td>' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td>' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td>' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td>' . $row->review_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td>' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td>' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }

                if ($row->selected_client == 1) {
                  $body .= '<td>YES</td>';
                } else {
                  $body .= '<td>NO</td>';
                }

                $body .= '<td class="rightalign">';
                if($row->assess_outcome && $row->review_outcome) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                }
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No" || $row->case == "verified") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }

                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                      '</td>' .
                    '</tr>';
              }
            } else if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td>' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td>' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td>' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td>' . $row->review_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }

                $body .= '<td class="rightalign">';
                if (Auth::user()->usertype_id == 9) {
                  if($row->assess_outcome && !$row->review_outcome) {
                    $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                  }
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '</td>' .
                    '</tr>';
              }
            } else if(Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                // if ($row->screened_by) {
                //   $body .= '<td>' . $row->screened_by . '</td>';
                // } else {
                //   $body .= '<td>N/A</td>';
                // }
                // if ($row->submitted_ho) {
                //   $body .= '<td>' . $row->submitted_ho . '</td>';
                // } else {
                //   $body .= '<td>N/A</td>';
                // }
                if ($row->submitted_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                // if ($row->submitted_by) {
                //   $body .= '<td>' . $row->submitted_by . '</td>';
                // } else {
                //   $body .= '<td>N/A</td>';
                // }

                $body .= '<td class="rightalign">';
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No" || $row->case == "verified") {
                  $body .= '<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                        '</td>' .
                    '</tr>';
              }
            } else {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if (Auth::user()->usertype_id == 2) {
                  if ($row->screened_status) {
                    $body .= '<td>' . $row->screened_status . '</td>';
                  } else {
                    $body .= '<td>N/A</td>';
                  }
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td>' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td>' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td>' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td>' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }

                $body .= '<td class="rightalign">';
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>';

                if (Auth::user()->usertype_id == 2) {
                  if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No" || $row->case == "verified") {
                    $body .= '<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                  }
                }

                if (Auth::user()->usertype_id == 2) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                          '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">';
                }
                $body .= '</td>' .
                    '</tr>';
              }
            }
          }

        }
      } else {
        if (Auth::user()->usertype_id == 8) {
          $count = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.life_id', 'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date', 'review_id.name as review_by',
                                    'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 'production_cases.source','production_cases.annual_currency_value2',
                                    'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                    'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency',
                                    'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
                                    'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','nationalities.nationality', 'production_cases.case',
                                    'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                    ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->with('getSettingsRate')
                                    ->where(function($query) {
                                        $query->where('production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search, $check_supervisor) {
                                        if ($check_supervisor) {
                                          $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");

                                        } else {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                        }
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->whereIn('production_cases.user_id', $ids)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
        } else {
          $count = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.life_id', 'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date', 'review_id.name as review_by',
                                'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 'production_cases.source','production_cases.annual_currency_value2',
                                'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency',
                                'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
                                'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','nationalities.nationality', 'production_cases.case',
                                  'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                    ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->with('getSettingsRate')
                                    ->where(function($query) {
                                        $query->where('production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhere('currency', 'LIKE', '%' . $search . '%')
                                                ->orWhere('mode_of_payment', 'LIKE', '%' . $search . '%')
                                                ->orWhere('source', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
        }
                                      
        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        if (Auth::user()->usertype_id == 8) {

          $rows = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.life_id', 'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date', 'review_id.name as review_by',
                                          'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 'production_cases.source','production_cases.annual_currency_value2',
                                          'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                          'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency',
                                          'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
                                          'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','nationalities.nationality', 'production_cases.case',
                                  'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                          ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                          ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                          ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                          ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                          ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                          ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                          ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                          ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                          ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                          ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                          ->with('getSettingsRate')
                                          ->where(function($query) {
                                              $query->where('production_cases.status', '!=', 2);
                                            })
                                          ->where(function($query) use ($search, $check_supervisor) {
                                              if ($check_supervisor) {
                                                $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                      ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                      ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");

                                              } else {
                                                $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                      ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                      ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                              }
                                            })
                                          ->where(function($query) use ($status, $start, $end) {
                                              if ($status) {
                                                $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                      ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                              }
                                            })
                                          ->whereIn('production_cases.user_id', $ids)
                                          ->orderBy($result['sort'], $result['order'])
                                          ->paginate($per);
                                          
          $body = '';
          if(Request::ajax()) {
            foreach ($rows as $key => $row) {
              $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">';

              if($check_supervisor) {
                $body .= '<td>' . $row->name . '</td>';
              }

              $body .= '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

              if ($row->residency_status === "Others") {
                $body .= '<td>' . $row->residency_others . '</td>';
              } else {
                $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
              }

              $body .= '<td>' . ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';
              $body .= '<td class="rightalign">' . 
                      $row->main_sum_assured .
                      '</td>' .
                      '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                      '<td>' . $row->policy_term . '</td>' . 
                      '<td>' . $row->case . '</td>';

              if ($row->selected_client == 1) {
                  $body .= '<td>YES</td>';
              } else {
                  $body .= '<td>NO</td>';
              }

              $body .= '<td class="rightalign">' .
                      '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                      '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                    '</td>' .
                  '</tr>';
            }
          }

        } else {

          $rows = ProductionCases::select('production_cases.id','production_cases.user_id', 'production_cases.life_id', 'production_cases.owner_id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','production_cases.assess_outcome','production_cases.assess_date', 'assess_id.name as assess_by','production_cases.review_outcome','production_cases.review_date', 'review_id.name as review_by','production_cases.screened_status', 
                                            'production_cases.policy_term', 'production_cases.mode_of_payment', 'product_mains.name as product_name', 'production_cases.ape', 'production_cases.submission_date', 'production_cases.source','production_cases.annual_currency_value2',
                                            'production_cases.owner_dob', 'production_cases.life_dob', 'production_cases.main_sum_assured', 'production_cases.screen_date','users.code as agent_code','production_cases.submitted_ho','production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                            'production_cases.currency_value2', 'production_cases.consent_marketing', 'production_cases.consent_servicing', 'auth_id.name as screened_by','production_cases.currency',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation',
                                            'production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','nationalities.nationality', 'production_cases.case',
                                  'production_cases.residency_status', 'production_cases.residency_others', 'production_cases.currency_id',
                    'production_cases.currency_value', 'providers.name as provider_name', 'production_cases.selected_client')
                                    ->leftJoin('providers', 'providers.id', '=', 'production_cases.provider_list')
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'production_cases.screen_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'production_cases.assess_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'production_cases.review_auth_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'production_cases.submitted_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'production_cases.nationality_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->where(function($query) {
                                        $query->where('production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhere('currency', 'LIKE', '%' . $search . '%')
                                                ->orWhere('mode_of_payment', 'LIKE', '%' . $search . '%')
                                                ->orWhere('source', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ");
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('production_cases.submission_date', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('production_cases.submission_date', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

          $body = '';
          if(Request::ajax()) {
            if (Auth::user()->usertype_id == 1) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td>' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td>' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td>' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td>' . $row->review_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td>' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td>' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                $body .= '<td class="rightalign">';
                if($row->assess_outcome && $row->review_outcome) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                }
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }

                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                      '</td>' .
                    '</tr>';
              }
            } else if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td>' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td>' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td>' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td>' . $row->review_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }

                $body .= '<td class="rightalign">';
                if (Auth::user()->usertype_id == 9) {
                  if($row->assess_outcome && !$row->review_outcome) {
                    $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                  }
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '</td>' .
                    '</tr>';
              }
            } else if(Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                // if ($row->screened_by) {
                //   $body .= '<td>' . $row->screened_by . '</td>';
                // } else {
                //   $body .= '<td>N/A</td>';
                // }
                // if ($row->submitted_ho) {
                //   $body .= '<td>' . $row->submitted_ho . '</td>';
                // } else {
                //   $body .= '<td>N/A</td>';
                // }
                if ($row->submitted_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                // if ($row->submitted_by) {
                //   $body .= '<td>' . $row->submitted_by . '</td>';
                // } else {
                //   $body .= '<td>N/A</td>';
                // }

                $body .= '<td class="rightalign">';
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No") {
                  $body .= '<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                        '</td>' .
                    '</tr>';
              }
            } else {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . ucwords($row->firstname . ' ' . strtolower(($row->lastname ? $row->lastname : ''))) . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>' .
                      '<td>' . $row->provider_name . '</td>' .
                      '<td>' . $row->product_name . '</td>';

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->submission_date, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if (Auth::user()->usertype_id == 2) {
                  if ($row->screened_status) {
                    $body .= '<td>' . $row->screened_status . '</td>';
                  } else {
                    $body .= '<td>N/A</td>';
                  }
                }
                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td>' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td>' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td>' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td>' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }

                $body .= '<td class="rightalign">';
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>';

                if (Auth::user()->usertype_id == 2) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                          '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">';
                }
                $body .= '</td>' .
                    '</tr>';
              }
            }
          }

        }
      }

      $sales = User::where(function($query) {
                          $query->where('usertype_id', '=', '8')
                                ->where('status', '=', 1);
                      })->with('salesInfo')->get();

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $body;
          $result['sales'] = $sales->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['sales'] = $sales;
          return $result;
      }
    }

     /**
     * Display a listing of the case submision.
     *
     * @return Response
     */
    public function indexPendingCases()
    {
      $result = $this->doListPendingCases();
      $this->data['rows'] = $result['rows'];
      $this->data['paginate'] = $result['paginate'];
      $this->data['sales'] = $result['sales'];
      $this->data['pages'] = $result['pages'];
      $this->data['case'] = 'pending';
      
      $this->data['nations'] = Nationality::all();
      $this->data['refresh_route'] = url('policy/production/case-submission/pending-cases/refresh');
      $this->data['plans'] = Product::where(function($query) {
                                  $query->where('provider_id', '=', '1')
                                        ->where('status', '=', '1');
                            })->get();
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
      $this->data['providers'] = Provider::where('status', 1)->get();
      $this->data['sgd_value'] = SettingsRate::where('code','=','SGD')->first();
      $this->data['currencies'] = SettingsRate::where('status','=','2')->get();
      // $this->data['plans'] = Product::where(function($query) {
      //                             $query->where('classification', '=', 'Rider');
      //                       })->get();
      $this->data['title'] = "Pending Cases";

      if ( Auth::user()->usertype_id == '8') { //if sales user
              $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');
                                                  
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
      return view('policy-management.production.case-pending.list', $this->data);
    }
    //displays a list of pending cases
     public function doListPendingCases()
    {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'production_cases.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status') ?: 'life';
      $per = Request::input('per') ?: 10;

      $paginate = '';
      $paginate_headers = '';

      if (Request::input('page') != '»') {

        if ($status == "life") {

          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          if (Auth::user()->usertype_id == 8) {

            $get_sales_designation = Sales::where('user_id', '=', Auth::user()->id)->first();
            $get_rank = Designation::find($get_sales_designation->designation_id);
            //dd(Auth::user()->id);
            if ($get_rank->rank == "Supervisor") {
              $ids = [];
              $ids[0] = Auth::user()->id;
              $check_if_supervisor = SalesSupervisor::where('user_id', '=', Auth::user()->id)->first();
              if ($check_if_supervisor) {
                $ctr = 1;
                $get_advisors = SalesAdvisor::where('supervisor_id', '=', $check_if_supervisor->id)->lists('user_id');
                foreach ($get_advisors as $key => $value) {
                  $ids[$ctr] = $value;
                  $ctr += 1;
                }
              }
              $rows = ProductionCases::select('production_cases.id', 'production_cases.fullname_first', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                              'production_cases.policy_term', 'production_cases.mode_of_payment',
                                              'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                        ->join('users', 'users.id', '=', 'production_cases.user_id')
                                        ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                        ->join('sales', 'sales.user_id', '=', 'users.id')
                                        ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                        ->where(function($query) {
                                              $query->where('production_cases.status', '!=', 2);
                                        })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                          })
                                        ->whereNull('screened_status')
                                        ->whereIn('production_cases.user_id', $ids)
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);

            } else if ($get_rank->rank == "Advisor") {

              $rows = ProductionCases::select('production_cases.id','production_cases.user_id','production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                              'production_cases.policy_term', 'production_cases.mode_of_payment',
                                               'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                        // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                        ->join('users', 'users.id', '=', 'production_cases.user_id')
                                        ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                        ->join('sales', 'sales.user_id', '=', 'users.id')
                                        ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                        ->where(function($query) {
                                              $query->where('production_cases.status', '!=', 2)
                                                    ->where('production_cases.user_id', '=', Auth::user()->id);
                                          })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                          })
                                        ->whereNull('screened_status')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);
                       //dd($rows);                
            } 
          } else {
            $rows = ProductionCases::select('production_cases.id', 'production_cases.fullname_first', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment','users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                      // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                      ->join('users', 'users.id', '=', 'production_cases.user_id')
                                      ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where('production_cases.status', '!=', 2)
                                      ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                        })
                                      ->whereNull('screened_status')
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
          }

          $paginate_headers = Request::input('headers');
          $paginate = '';

          foreach ($rows as $key => $row) {
            $paginate .= '<tr data-id="' . $row->id . '">
                          <td>' .$row->agent_name . '</td>
                          <td>' .$row->agent_code . '</td>
                          <td>' .$row->provider_name . '</td>
                          <td>' .$row->firstname . ' ' .$row->lastname . '</td>
                          <td>' .$row->annual_income_range . '</td>
                          <td>' .$row->life_firstname . ' ' .$row->life_lastname . '</td>
                          <td>' .$row->policy_term . '</td>
                          <td>' .$row->mode_of_payment . '</td>
                          <td>' .$row->designation . '</td>
                          <td>' .$row->case . '</td>';

            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              $paginate .= '<td>' . ($row->screened_status ? $row->screened_status : 'N/A') . '</td>';
            }
            
            if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              if ($row->case === "pending") {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-flag"></i></button>';
              } else {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view btn-table"><i class="fa fa-flag-checkered"></i></button>';
              }
            } else {
              $paginate .= '<td class="rightalign">
                            <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-eye"></i></button>';
            }
            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2) {
              $paginate .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete"><i class="fa fa-trash"></i></button>';
            }

            $paginate .= '</td>
                  </tr>';
          }

        } else if ($status == "gi") {

          $rows = GIProductionCases::select('gi_production_cases.id', 'gi_production_cases.fullname_first', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range',
                                            'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                            'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname','gi_production_cases.company_name',  'gi_production_cases.status', 'users.name', 'designations.designation','gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','gi_production_cases.case','gi_production_cases.screened_status')
                                      ->join('users', 'users.id', '=', 'gi_production_cases.user_id')
                                      ->join('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                          $query->where('gi_production_cases.status', '!=', 2);
                                        })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                        })
                                        ->whereNull('screened_status')
                                        ->paginate($per);

          $paginate_headers = Request::input('headers');
          $paginate = '';

          foreach ($rows as $key => $row) {
            $paginate .= '<tr data-id="' . $row->id . '">
                          <td>' .$row->agent_name . '</td>
                          <td>' .$row->agent_code . '</td>
                          <td>' .$row->provider_name . '</td>
                          <td>' .$row->company_name . '</td>
                          <td>' .$row->mode_of_payment . '</td>
                          <td>' .$row->designation . '</td>
                          <td>' .$row->case . '</td>';

            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              $paginate .= '<td>' . ($row->screened_status ? $row->screened_status : 'N/A') . '</td>';
            }
            
            if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              if ($row->case === "pending") {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view-gi btn-table"><i class="fa fa-flag"></i></button>';
              } else {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view-gi btn-table"><i class="fa fa-flag-checkered"></i></button>';
              }
            } else {
              $paginate .= '<td class="rightalign">
                            <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-eye"></i></button>';
            }

            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2) {
              $paginate .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete-gi"><i class="fa fa-trash"></i></button>';
            }

            $paginate .= '</td>
                  </tr>';
          }

        }

      } else {

        if ($status == "life") {

          if (Auth::user()->usertype_id == 8) {
            $get_sales_designation = Sales::where('user_id', '=', Auth::user()->id)->first();
            $get_rank = Designation::find($get_sales_designation->designation_id);
            //dd(Auth::user()->id);
            if ($get_rank->rank == "Supervisor") {
              $ids = [];
              $ids[0] = Auth::user()->id;
              $check_if_supervisor = SalesSupervisor::where('user_id', '=', Auth::user()->id)->first();
              if ($check_if_supervisor) {
                $ctr = 1;
                $get_advisors = SalesAdvisor::where('supervisor_id', '=', $check_if_supervisor->id)->lists('user_id');
                foreach ($get_advisors as $key => $value) {
                  $ids[$ctr] = $value;
                  $ctr += 1;
                }
              }
              $count = ProductionCases::select('production_cases.id', 'production_cases.fullname_first', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                              'production_cases.policy_term', 'production_cases.mode_of_payment',
                                              'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                        // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                        ->join('users', 'users.id', '=', 'production_cases.user_id')
                                        ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                        ->join('sales', 'sales.user_id', '=', 'users.id')
                                        ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                        ->where(function($query) {
                                              $query->where('production_cases.status', '!=', 2);
                                          })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                          })
                                        ->whereNull('screened_status')
                                        ->whereIn('production_cases.user_id', $ids)
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);

            } else if ($get_rank->rank == "Advisor") {

              $count = ProductionCases::select('production_cases.id','production_cases.user_id','production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range','users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                              'production_cases.policy_term', 'production_cases.mode_of_payment',
                                               'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                        // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                        ->join('users', 'users.id', '=', 'production_cases.user_id')
                                        ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                        ->join('sales', 'sales.user_id', '=', 'users.id')
                                        ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                        ->where(function($query) {
                                              $query->where('production_cases.status', '!=', 2)
                                                    ->where('production_cases.user_id', '=', Auth::user()->id);
                                          })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                          })
                                        ->whereNull('screened_status')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);
            }
          } else {

            $count = ProductionCases::select('production_cases.id', 'production_cases.fullname_first', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment','users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                      ->join('users', 'users.id', '=', 'production_cases.user_id')
                                      ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')->where('production_cases.status', '!=', 2)
                                      ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                        })
                                      ->whereNull('screened_status')
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
          }

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          if (Auth::user()->usertype_id == 8) {
            
            $get_sales_designation = Sales::where('user_id', '=', Auth::user()->id)->first();
            $get_rank = Designation::find($get_sales_designation->designation_id);

            if ($get_rank->rank == "Supervisor") {
              $ids = [];
              $ids[0] = Auth::user()->id;
              $check_if_supervisor = SalesSupervisor::where('user_id', '=', Auth::user()->id)->first();
              if ($check_if_supervisor) {
                $ctr = 1;
                $get_advisors = SalesAdvisor::where('supervisor_id', '=', $check_if_supervisor->id)->lists('user_id');
                foreach ($get_advisors as $key => $value) {
                  $ids[$ctr] = $value;
                  $ctr += 1;
                }
              }
              $rows = ProductionCases::select('production_cases.id', 'production_cases.fullname_first', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                              'production_cases.policy_term', 'production_cases.mode_of_payment', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                              'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                        ->join('users', 'users.id', '=', 'production_cases.user_id')
                                        ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                        ->join('sales', 'sales.user_id', '=', 'users.id')
                                        ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                        ->where(function($query) {
                                              $query->where('production_cases.status', '!=', 2);
                                          })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                          })
                                        ->whereNull('screened_status')
                                        ->whereIn('production_cases.user_id', $ids)
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);
            } else if ($get_rank->rank == "Advisor") {
              $rows = ProductionCases::select('production_cases.id', 'production_cases.fullname_first', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                              'production_cases.policy_term', 'production_cases.mode_of_payment', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                               'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                        ->join('users', 'users.id', '=', 'production_cases.user_id')
                                        ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                        ->join('sales', 'sales.user_id', '=', 'users.id')
                                        ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                        ->where(function($query) {
                                              $query->where('production_cases.status', '!=', 2)
                                                    ->where('production_cases.user_id', '=', Auth::user()->id);
                                          })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                          })
                                        ->whereNull('screened_status')
                                        ->orderBy($result['sort'], $result['order'])
                                        ->paginate($per);
            }
          } else {
            $rows = ProductionCases::select('production_cases.id', 'production_cases.fullname_first', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                             'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'designations.designation','production_cases.consent_mail','production_cases.consent_sms','production_cases.consent_telephone','production_cases.case','production_cases.screened_status')
                                      // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                      ->join('users', 'users.id', '=', 'production_cases.user_id')
                                      ->join('providers', 'providers.id', '=', 'production_cases.provider_list')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                            $query->where('production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                        })
                                      ->whereNull('screened_status')
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
          }

          $paginate_headers = Request::input('headers');
          $paginate = '';

          foreach ($rows as $key => $row) {
            $paginate .= '<tr data-id="' . $row->id . '">
                          <td>' .$row->agent_name . '</td>
                          <td>' .$row->agent_code . '</td>
                          <td>' .$row->provider_name . '</td>
                          <td>' .$row->firstname . ' ' .$row->lastname . '</td>
                          <td>' .$row->annual_income_range . '</td>
                          <td>' .$row->life_firstname . ' ' .$row->life_lastname . '</td>
                          <td>' .$row->policy_term . '</td>
                          <td>' .$row->mode_of_payment . '</td>
                          <td>' .$row->designation . '</td>
                          <td>' .$row->case . '</td>';

            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              $paginate .= '<td>' . ($row->screened_status ? $row->screened_status : 'N/A') . '</td>';
            }
            
            if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              if ($row->case === "pending") {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-flag"></i></button>';
              } else {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view btn-table"><i class="fa fa-flag-checkered"></i></button>';
              }
            } else {
              $paginate .= '<td class="rightalign">
                            <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-eye"></i></button>';
            }
            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2) {
              $paginate .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete"><i class="fa fa-trash"></i></button>';
            }

            $paginate .= '</td>
                  </tr>';
          }

        } else if ($status == 'gi') {

          $count = GIProductionCases::select('gi_production_cases.id', 'gi_production_cases.fullname_first', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range',
                                            'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                            'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname','gi_production_cases.company_name',  'gi_production_cases.status', 'users.name', 'designations.designation','gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','gi_production_cases.case','gi_production_cases.screened_status')
                                      ->join('users', 'users.id', '=', 'gi_production_cases.user_id')
                                      ->join('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                          $query->where('gi_production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search) {
                                          $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                ->orWhere('gi_production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                ->orWhere('gi_production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('gi_production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('gi_production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                          })
                                        ->whereNull('screened_status')
                                        ->paginate($per);

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          $rows = GIProductionCases::select('gi_production_cases.id', 'gi_production_cases.fullname_first', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range',
                                            'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'users.name as agent_name', 'users.code as agent_code', 'providers.name as provider_name',
                                            'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname','gi_production_cases.company_name',  'gi_production_cases.status', 'users.name', 'designations.designation','gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','gi_production_cases.case','gi_production_cases.screened_status')
                                      ->join('users', 'users.id', '=', 'gi_production_cases.user_id')
                                      ->join('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                          $query->where('gi_production_cases.status', '!=', 2);
                                        })
                                        ->where(function($query) use ($search) {
                                              $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('users.code', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.fullname_first', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.life_lastname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.life_firstname', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('gi_production_cases.mode_of_payment', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('designations.designation', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('providers.name', 'LIKE', '%' . $search . '%')
                                                    ->orWhereRaw("concat(life_firstname, ' ', life_lastname) like '%$search%' ")
                                                    ->orWhereRaw("concat(life_lastname, ' ', life_firstname) like '%$search%' ");
                                        })
                                        ->whereNull('screened_status')
                                        ->paginate($per);

          $paginate_headers = Request::input('headers');
          $paginate = '';

          foreach ($rows as $key => $row) {
            $paginate .= '<tr data-id="' . $row->id . '">
                          <td>' .$row->agent_name . '</td>
                          <td>' .$row->agent_code . '</td>
                          <td>' .$row->provider_name . '</td>
                          <td>' .$row->company_name . '</td>
                          <td>' .$row->mode_of_payment . '</td>
                          <td>' .$row->designation . '</td>
                          <td>' .$row->case . '</td>';

            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              $paginate .= '<td>' . ($row->screened_status ? $row->screened_status : 'N/A') . '</td>';
            }
            
            if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 ||
              Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4 ||
              Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              if ($row->case === "pending") {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-flag"></i></button>';
              } else {
                $paginate .= '<td class="rightalign">
                              <button type="button" class="btn btn-xs btn-view btn-table"><i class="fa fa-flag-checkered"></i></button>';
              }
            } else {
              $paginate .= '<td class="rightalign">
                            <button type="button" class="btn btn-xs btn-view btn-table btn-view"><i class="fa fa-eye"></i></button>';
            }
            if(Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2) {
              $paginate .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete"><i class="fa fa-trash"></i></button>';
            }

            $paginate .= '</td>
                  </tr>';
          }
        }
      }

      $sales = User::where(function($query) {
                          $query->where('usertype_id', '=', '8')
                                ->where('status', '=', 1);
                      })->with('salesInfo')->get();

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          $result['sales'] = $sales->toArray();
          $result['paginate'] = $paginate;
          $result['paginate_headers'] = $paginate_headers;
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['sales'] = $sales;
          $result['paginate'] = $paginate;
          return $result;
      }
    }
    /**
     * Display a listing of the case inception advisor.
     *
     * @return Response
     */


    public function indexCaseInception()
    {
        $result = $this->doListCaseInception();
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['title'] = "Policy Management";
        $this->data['case'] = 'approved';
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        // for notification
       if ( Auth::user()->usertype_id == '8') { //if sales user
    $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0'); 
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.production.case-inception.list', $this->data);
    }

     public function doListCaseInception()
    {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'production_cases.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        if (Auth::user()->usertype_id == 8) {
          $get_sales_designation = Sales::where('user_id', '=', Auth::user()->id)->first();
          $get_rank = Designation::find($get_sales_designation->designation_id);
          if ($get_rank->rank == "Supervisor") {
            $ids = [];
            $ids[0] = Auth::user()->id;
            $check_if_supervisor = SalesSupervisor::where('user_id', '=', Auth::user()->id)->first();
            if ($check_if_supervisor) {
              $ctr = 1;
              $get_advisors = SalesAdvisor::where('supervisor_id', '=', $check_if_supervisor->id)->lists('user_id');
              foreach ($get_advisors as $key => $value) {
                $ids[$ctr] = $value;
                $ctr += 1;
              }
            }
            $rows = ProductionCases::select('production_cases.id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation')
                                      // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                      ->join('users', 'users.id', '=', 'production_cases.user_id')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                            $query->where('production_cases.status', '!=', 2)
                                                  ->where('case', '=', 'pending');
                                        })
                                      ->where(function($query) use ($search) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%');
                                        })
                                      ->whereIn('production_cases.user_id', $ids)
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
          } else if ($get_rank->rank == "Advisor") {
            $rows = ProductionCases::select('production_cases.id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation')
                                      // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                      ->join('users', 'users.id', '=', 'production_cases.user_id')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                            $query->where('production_cases.status', '!=', 2)
                                                  ->where('production_cases.user_id', '=', Auth::user()->id)
                                                  ->where('case', '=', 'pending');
                                        })
                                      ->where(function($query) use ($search) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%');
                                        })
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
          }
        } else {
          $rows = ProductionCases::select('production_cases.id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                          'production_cases.policy_term', 'production_cases.mode_of_payment',
                                          'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation')
                                    // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                    ->join('users', 'users.id', '=', 'production_cases.user_id')
                                    ->join('sales', 'sales.user_id', '=', 'users.id')
                                    ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->where(function($query) {
                                          $query->where('production_cases.status', '!=', 2)
                                                ->where('case', '=', 'pending');
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
        }
      } else {
        $count = ProductionCases::select('production_cases.id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                          'production_cases.policy_term', 'production_cases.mode_of_payment',
                                          'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation')
                                    // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                    ->join('users', 'users.id', '=', 'production_cases.user_id')
                                    ->join('sales', 'sales.user_id', '=', 'users.id')
                                    ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->where(function($query) {
                                          $query->where('production_cases.status', '!=', 2)
                                                ->where('case', '=', 'pending');
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        if (Auth::user()->usertype_id == 8) {
          
          $get_sales_designation = Sales::where('user_id', '=', Auth::user()->id)->first();
          $get_rank = Designation::find($get_sales_designation->designation_id);

          if ($get_rank->rank == "Supervisor") {
            $ids = [];
            $ids[0] = Auth::user()->id;
            $check_if_supervisor = SalesSupervisor::where('user_id', '=', Auth::user()->id)->first();
            if ($check_if_supervisor) {
              $ctr = 1;
              $get_advisors = SalesAdvisor::where('supervisor_id', '=', $check_if_supervisor->id)->lists('user_id');
              foreach ($get_advisors as $key => $value) {
                $ids[$ctr] = $value;
                $ctr += 1;
              }
            }
            $rows = ProductionCases::select('production_cases.id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation')
                                      // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                      ->join('users', 'users.id', '=', 'production_cases.user_id')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                            $query->where('production_cases.status', '!=', 2)
                                                  ->where('case', '=', 'pending');
                                        })
                                      ->where(function($query) use ($search) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%');
                                        })
                                      ->whereIn('production_cases.user_id', $ids)
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
          } else if ($get_rank->rank == "Advisor") {
            $rows = ProductionCases::select('production_cases.id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                            'production_cases.policy_term', 'production_cases.mode_of_payment',
                                            'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation')
                                      // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                      ->join('users', 'users.id', '=', 'production_cases.user_id')
                                      ->join('sales', 'sales.user_id', '=', 'users.id')
                                      ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->where(function($query) {
                                            $query->where('production_cases.status', '!=', 2)
                                                  ->where('production_cases.user_id', '=', Auth::user()->id)
                                                  ->where('case', '=', 'pending');
                                        })
                                      ->where(function($query) use ($search) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%');
                                        })
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);
          }

        } else {
          $rows = ProductionCases::select('production_cases.id', 'production_cases.firstname', 'production_cases.lastname', 'production_cases.annual_income_range',
                                          'production_cases.policy_term', 'production_cases.mode_of_payment',
                                          'production_cases.life_firstname', 'production_cases.life_lastname', 'production_cases.status', 'users.name', 'designations.designation')
                                    // ->join('products', 'products.id', '=', 'production_cases.product_id')
                                    ->join('users', 'users.id', '=', 'production_cases.user_id')
                                    ->join('sales', 'sales.user_id', '=', 'users.id')
                                    ->join('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->where(function($query) {
                                          $query->where('production_cases.status', '!=', 2)
                                                ->where('case', '=', 'pending');
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
        }
      }

      $sales = User::where(function($query) {
                          $query->where('usertype_id', '=', '8')
                                ->where('status', '=', 1);
                      })->with('salesInfo')->get();

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          $result['sales'] = $sales->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['sales'] = $sales;
          return $result;
      }
    }


    // /**
    //  * Display a listing of the case submision advisor.
    //  *
    //  * @return Response
    //  */
    // public function indexCaseSubAdvisor()
    // {
    //     $result = $this->doListCaseSubAdvisor();
    //     $this->data['rows'] = $result['rows'];
    //     $this->data['sales'] = $result['sales'];
    //     $this->data['permission'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '1');
    //                           })->first();
    //     $this->data['permission_payroll'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '2');
    //                           })->first();
    //     $this->data['permission_policy'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '3');
    //                           })->first();
    //     $this->data['permission_provider'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '4');
    //                           })->first();
    //     $this->data['permission_reports'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '5');
    //                           })->first();
    //     $this->data['title'] = "Policy Management";
    //     return view('policy-management.production.case-submission-advisor.list', $this->data);
    // }

    // public function doListCaseSubAdvisor()
    // {
    //      // sort and order
    //     $result['sort'] = Request::input('s') ?: 'created_at';
    //     $result['order'] = Request::input('o') ?: 'desc';
        
    //     $rows = ProductionCases::with('getUserSubmission')
    //                         ->where('status', '!=', 2)
    //                         ->where('visor', '=', 'Advisor')
    //                         ->where('case', '=', 'submission')
    //                         ->orderBy($result['sort'], $result['order'])
    //                         ->get();

    //     $sales = User::where(function($query) {
    //                         $query->where('system_id', '=', null)
    //                               ->where('status', '=', 1);
    //                     })->with('salesInfo')->get();

    //     // return response (format accordingly)
    //     if(Request::ajax()) {
    //         //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows->toArray();
    //         $result['sales'] = $sales->toArray();
    //         return Response::json($result);
    //     } else {
    //         //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows;
    //         $result['sales'] = $sales;
    //         return $result;
    //     }
    // }

    // *
    //  * Display a listing of the case submission supervisor.
    //  *
    //  * @return Response
     
    // public function indexCaseSubSupervisor()
    // {
    //     $result = $this->doListCaseSubSupervisor();
    //     $this->data['rows'] = $result['rows'];
    //     $this->data['sales'] = $result['sales'];
    //     $this->data['permission'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '1');
    //                           })->first();
    //     $this->data['permission_payroll'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '2');
    //                           })->first();
    //     $this->data['permission_policy'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '3');
    //                           })->first();
    //     $this->data['permission_provider'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '4');
    //                           })->first();
    //     $this->data['permission_reports'] = Permission::where(function($query) {
    //                                 $query->where('usertype_id', '=', Auth::user()->usertype_id)
    //                                       ->where('module_id', '=', '5');
    //                           })->first();
    //     $this->data['title'] = "Policy Management";
    //     return view('policy-management.production.case-submission-supervisor.list', $this->data);
    // }

    // public function doListCaseSubSupervisor()
    // {
    //      // sort and order
    //     $result['sort'] = Request::input('s') ?: 'created_at';
    //     $result['order'] = Request::input('o') ?: 'desc';
        
    //     $rows = ProductionCases::with('getUserSubmission')
    //                         ->where('status', '!=', 2)
    //                         ->where('visor', '=', 'Supervisor')
    //                         ->where('case', '=', 'submission')
    //                         ->orderBy($result['sort'], $result['order'])
    //                         ->get();

    //     $sales = User::where(function($query) {
    //                         $query->where('system_id', '=', null)
    //                               ->where('status', '=', 1);

    //                     })->with('salesInfo')->get();

    //     // return response (format accordingly)
    //     if(Request::ajax()) {
    //         //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows->toArray();
    //         $result['sales'] = $sales->toArray();
    //         return Response::json($result);
    //     } else {
    //         //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows;
    //         $result['sales'] = $sales;
    //         return $result;
    //     }
    // }

    /**
     * Incept case
     *
     */

    public function incept() {
        $row = ProductionCases::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->case = 'pending';
                $row->save();

            // return
            return Response::json(['body' => 'Case has been incepted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }
     /**
     * reject case
     *
     */
    public function assessment() {
      // get the user info
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        if (!$row->assess_outcome) {
          $row->assess_outcome = Request::input('assessment_outcome');
          $row->assess_date = Carbon::now();
          $row->assess_auth_id = Auth::user()->id;
          $row->save();
          
          return Response::json(['body' => 'Case has been assessed.']);
        } else {
          return Response::json(['error' => "Unauthorized Access."]);
        }
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }

    public function review() {
      // get the user info
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->review_outcome = Request::input('review_outcome');
        $row->review_date = Carbon::now();
        $row->review_auth_id = Auth::user()->id;
        $row->save();
        
        return Response::json(['body' => 'Case has been reviewed.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }


   public function undeterminedGI() {
      // get the user info
      $row = GIProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        if ($row->screened_status == 'Undetermined') {
          return Response::json(['error' => 'GI Submission screened status is already set to Undertermined.']);
        } else {
          $row->screened_status = 'Undetermined';
          $row->screened_date = Carbon::now();
          $row->screened_auth_by = Auth::user()->id;
          $row->save();
          
          return Response::json(['body' => 'GI Submission screen status has been set to Undertermined.']);
        }
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }

    public function negativeGI() {
      // get the user info
      $row = GIProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        if ($row->screened_status == 'Negative') {
          return Response::json(['error' => 'GI Submission screened status is already set to Negative.']);
        } else {
          $row->screened_status = 'Negative';
          $row->screened_date = Carbon::now();
          $row->screened_auth_by = Auth::user()->id;
          $row->save();
        }
        return Response::json(['body' => 'GI Submission screen status has been set to Negative.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }

    public function undeterminedLife() {
      // get the user info
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        if ($row->screened_status == 'Undetermined') {
          return Response::json(['error' => 'Life Submission screened status is already set to Undertermined.']);
        } else {
          $row->screened_status = 'Undetermined';
          $row->screened_date = Carbon::now();
          $row->screened_auth_by = Auth::user()->id;
          $row->save();
          
          return Response::json(['body' => 'Life Submission screen status has been set to Undertermined.']);
        }
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }

    public function negativeLife() {
      // get the user info
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        if ($row->screened_status == 'Negative') {
          return Response::json(['error' => 'Life Submission screened status is already set to Negative.']);
        } else {
          $row->screened_status = 'Negative';
          $row->screened_date = Carbon::now();
          $row->screened_auth_by = Auth::user()->id;
          $row->save();
        }
        return Response::json(['body' => 'Life Submission screen status has been set to Negative.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }


     /**
     * reject case
     *
     */
    public function submitted() {
      // get the user info
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->submitted_ho = Request::input('submitted_outcome');
        $row->submitted_date = Carbon::now();
        $row->submitted_auth_id = Auth::user()->id;
        $row->save();
        
        if (Request::input('submitted_outcome') == "Yes") {
          return Response::json(['body' => 'Case has been submitted to HO.']);
        } else {
          // not found
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }


     /**
     * reject case
     *
     */
    public function reject() {
      // get the user info
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->reject_reason = Request::input('reason');
        $row->case = "rejected";
        $row->screen_date = Carbon::now();
        $row->screen_auth_id = Auth::user()->id;
        $row->save();
        
        return Response::json(['body' => 'Case has been rejected.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }
    /**
     * verify case
     *
     */
    public function verified() {
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->reject_reason = Request::input('reason');
        $row->case = "verified";
        $row->screen_date = Carbon::now();
        $row->screen_auth_id = Auth::user()->id;
        $row->save();

        return Response::json(['body' => 'Case has been verfied.']);
      } else {
          // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }
    /**
     * Incept case
     *
     */
    public function approved() {
      $row = ProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->reject_reason = Request::input('reason');
        $row->case = "approved";
        $row->save();
        
        return Response::json(['body' => 'Case has been approved.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    /**
     * Decept case
     *
     */
    public function decept() {
        $row = ProductionCases::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
            $row->case = 'approved';
            $row->save();

            // return
            return Response::json(['body' => 'Case has been incepted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Deactivate the cases
     *
     */
    public function disable() {
        $row = ProductionCases::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->status = 0;
                $row->save();

            // return
            return Response::json(['body' => 'Case has been deactivated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the cases
     *
     */
    public function enable() {
        $row = ProductionCases::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->status = 1;
                $row->save();

            // return
            return Response::json(['body' => 'Case has been activated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

     /**
     * Incept multiple case
     *
     */
    public function inceptSelected() {
        $ids = Request::input('ids');
       
        if (count($ids) == 0) {
            return Response::json(['error' => 'Select providers first.']); 
        } else {
           

            // process
            foreach ($ids as $id) {
                $row = ProductionCases::find($id);
                if(!is_null($row)) {
                    
                    $case = $row->case;
                    // disable only the active users.
                    if ($case == 'submission'){
                      $row->case = 'inception';
                      $row->save();
                     
                    }
                }
            }
            return Response::json(['body' => 'Selected providers has been Deactivated.']);
        }
    } 

    /**
     * Decept multiple case
     *
     */
    public function deceptSelected() {
        $ids = Request::input('ids');
       
        if (count($ids) == 0) {
            return Response::json(['error' => 'Select providers first.']); 
        } else {
           

            // process
            foreach ($ids as $id) {
                $row = ProductionCases::find($id);
                if(!is_null($row)) {
                    
                    $case = $row->case;
                    // disable only the active users.
                    if ($case == 'inception'){
                      $row->case = 'submission';
                      $row->save();
                     
                    }
                }
            }
            return Response::json(['body' => 'Selected providers has been Deactivated.']);
        }
    } 

    /**
     * Display a listing of the case inception advisor.
     *
     * @return Response
     */
    public function indexCaseIncAdvisor()
    {
        $result = $this->doListCaseIncAdvisor();
        $this->data['rows'] = $result['rows'];
        $this->data['title'] = "Policy Management";
        $this->data['settings'] = Settings::first();
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        return view('policy-management.production.case-inception-advisor.list', $this->data);
    }

     public function doListCaseIncAdvisor()
    {
         // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $rows = ProductionCases::with('getUserSubmission')
                            ->where('status', '!=', 2)
                            ->where('visor', '=', 'Advisor')
                            ->where('case', '=', 'inception')
                            ->orderBy($result['sort'], $result['order'])
                            ->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    
    /**
     * Display a listing of the case ineption supervisor.
     *
     * @return Response
     */
    public function indexCaseIncSupervisor()
    {
        $result = $this->doListCaseIncSupervisor();
        $this->data['rows'] = $result['rows'];
        $this->data['settings'] = Settings::first();
        $this->data['title'] = "Policy Management";
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();

        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();

        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        return view('policy-management.production.case-inception-supervisor.list', $this->data);
    }

     public function doListCaseIncSupervisor()
    {
         // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $rows = ProductionCases::with('getUserSubmission')
                            ->where('status', '!=', 2)
                            ->where('visor', '=', 'Supervisor')
                            ->where('case', '=', 'inception')
                            ->orderBy($result['sort'], $result['order'])
                            ->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    public function getCaseCurrency() {

      $row = SettingsRate::where('status', '=', '1')
                          ->get();
      $col = Request::input('col');
      return Response::json($row->$col);
    }

    public function getCaseCurrencyRate() {

      $row = SettingsRate::find(Request::input('id'));

      return Response::json($row->rate);
    }

    public function caseView() {
        $id = Request::input('id');
        $result['same'] = false;
        $result['backdate'] = false;
        $result['owner_backdate'] = '';
        $result['conversion'] = '';
        $result['premium_paying_term'] = '';
        $result['maturity_term'] = '';

        // prevent non-admin from viewing deactivated row
        $result['row'] = ProductionCases::with('getContact')
                              ->with('getSum')
                              ->with('getProvider')
                              ->with('getProvider.getProducts')
                              ->with('getProvider.getProducts.getCaseRiders')
                              ->with('getMainRiders')
                              ->with('getSum.getCaseRider')->find($id);

        if ($result['row']) {
          $row = $result['row'];

          $result['owner_backdate'] = $row->owner_age;
          if ($row->firstname === $row->life_firstname && $row->lastname === $row->life_lastname
            && $row->owner_id === $row->life_id && $row->owner_occupation === $row->life_occupation
            && $row->owner_gender === $row->life_gender && $row->owner_dob === $row->life_dob 
            && $row->owner_age === $row->life_age && $row->owner_bday === $row->life_bday) {

            $result['same'] = true;
          }

          if ($row->owner_age && $row->life_age) {
            $result['backdate'] = true;
          }

          $getSum = ProductionCaseRider::where('production_id', $row->id)->get();
          // dd($getSum);

          $main = ProductMain::find($row->main_product_id);

          if ($main) {
            $result['conversion'] = $main->conversion;
            $result['premium_paying_term'] = $main->premium_paying_term;
            $result['maturity_term'] = $main->maturity_term;
            $result['conversion_rate'] = $main->conversion_rate;
          }

        }

        if($result['row']) {
            return Response::json($result);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function caseViewAll() {
        $id = Request::input('id');

      // $result['plans'] = Product::where(function($query) {
      //                             $query->where('provider_id', '=', '1')
      //                                   ->where('status', '=', '1');
      //                       })->get();

        // prevent non-admin from viewing deactivated row
        $result['row'] = ProductionCases::with('getContact')
                              ->with('getSum')
                              ->with('getProvider')
                              ->with('getProvider.getProducts')
                              ->with('getProvider.getProducts.getCaseRiders')
                              ->with('getMainRiders')
                              ->with('getSum.getCaseRider')->find($id);

        if($result['row']) {
            return Response::json($result);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }



    // /**
    //  * Save model
    //  *
    //  */
    // public function caseSubmissionAdvisorSave() {
    
    //   $new = true;

    //   $input = Input::all();

    //   // check if an ID is passed
    //   if(array_get($input, 'id')) {

    //     // get the user info
    //     $row = ProductionCases::find(array_get($input, 'id'));

    //     if(is_null($row)) {
    //         return Response::json(['error' => "The requested item was not found in the database."]);
    //     }
    //     // this is an existing row
    //     $new = false;
    //   }
        
    //     $rules = [
    //         'firstname' => 'required',
    //         'lastname' => 'required',
    //         'annual_income_range' => 'required|min:2|max:100',
    //         'life_firstname' => 'required|min:2|max:100',
    //         'life_lastname' => 'required|min:2|max:1000',
    //         'owner_id' => 'required|min:2|max:1000',
    //         'life_id' => 'required|min:2|max:1000',
    //         'owner_occupation' => 'required|min:2|max:1000',
    //         'life_occupation' => 'required|min:2|max:1000',
    //         'owner_gender' => 'required|min:2|max:1000',
    //         'life_gender' => 'required|min:2|max:1000',
    //         'owner_dob' => 'required|min:2|max:1000',
    //         'life_dob' => 'required|min:2|max:1000',
    //         'owner_age' => 'required|min:2|max:1000',
    //         'life_age' => 'required|min:2|max:1000',
    //         'contact' => 'required|min:2|max:1000',
    //         'address' => 'required|min:2|max:1000',
    //         'postal_code' => 'required|min:2|max:1000',
    //         'email' => 'required|min:2|max:1000',
    //         'product_id' => 'required|min:2|max:1000',
    //         'sum_assured' => 'required|min:2|max:1000',
    //         'amount' => 'required|min:2|max:1000',
    //         'currency' => 'required|min:2|max:1000',
    //         'prem_currency' => 'required|min:2|max:1000',
    //         'prem_account' => 'required|min:2|max:1000',
    //         'policy_term' => 'required|min:2|max:1000',
    //         'payment_frequency' => 'required|min:2|max:1000',
    //         'mode_of_payment' => 'required|min:2|max:1000',
    //         'ape' => 'required|min:2|max:1000',
    //         'source' => 'required|min:1|max:1000',
    //     ];

    //     // field name overrides
    //     $names = [
    //         'firstname' => 'firstname',
    //         'lastname' => 'lastname',
    //         'annual_income_range' => 'annual income range',
    //         'life_firstname' => 'life firstname',
    //         'life_lastname' => 'life lastname',
    //         'owner_id' => 'owner id',
    //         'life_id' => 'life id',
    //         'owner_occupation' => 'owner occupation',
    //         'life_occupation' => 'life occupation',
    //         'owner_gender' => 'gender',
    //         'life_gender' => 'gender',
    //         'owner_dob' => 'date of birth',
    //         'life_dob' => 'date of birth',
    //         'owner_age' => 'age',
    //         'life_age' => 'age',
    //         'contact' => 'contact',
    //         'address' => 'address',
    //         'postal_code' => 'postal code',
    //         'email' => 'email',
    //         'type_of_plan' => 'type of plan',
    //         'sum_assured' => 'sum assured',
    //         'amount' => 'amount',
    //         'currency' => 'currency',
    //         'prem_currency' => 'premium currency',
    //         'prem_account' => 'premium term',
    //         'policy_term' => 'policy term',
    //         'payment_frequency' => 'payment frequency',
    //         'mode_of_payment' => 'mode of payment',
    //         'ape' => 'ape',
    //         'cka' => 'cka',
    //         'source' => 'source',
    //     ];

    //     // do validation
    //     $validator = Validator::make(Input::all(), $rules);
    //     $validator->setAttributeNames($names);

    //     // return errors
    //     if($validator->fails()) {
    //         return Response::json(['error' => array_unique($validator->errors()->all())]);
    //     }
    //     $log_firstname = "";
    //     $log_lastname = "";
    //     $log_annual_income_range = "";
    //     $log_life_firstname = "";
    //     $log_life_lastname = "";
    //     $log_owner_id = "";
    //     $log_life_id = "";
    //     $log_owner_occupation = "";
    //     $log_life_occupation = "";
    //     $log_owner_gender = "";
    //     $log_life_gender = "";
    //     $log_owner_dob = "";
    //     $log_life_dob = "";
    //     $log_owner_age = "";
    //     $log_life_age = "";
    //     $log_contact = "";
    //     $log_address = "";
    //     $log_postal_code = "";
    //     $log_email = "";
    //     $log_type_of_plan = "";
    //     $log_sum_assured = "";
    //     $log_amount = "";
    //     $log_currency = "";
    //     $log_prem_currency = "";
    //     $log_prem_account = "";
    //     $log_policy_term = "";
    //     $log_payment_frequency = "";
    //     $log_mode_of_payment = "";
    //     $log_ape = "";
    //     $log_cka = "";
    //     $log_source = "";

    //     // create model if new
    //     if($new) {
    //         $row = new ProductionCases;
    //         $row->user_id = array_get($input, 'user_sales_id');
    //         $row->firstname = array_get($input, 'firstname');
    //         $row->lastname = array_get($input, 'lastname');
    //         $row->annual_income_range = array_get($input, 'annual_income_range');
    //         $row->life_firstname = array_get($input, 'life_firstname');
    //         $row->life_lastname = array_get($input, 'life_lastname');
    //         $row->owner_id = array_get($input, 'owner_id');
    //         $row->life_id = array_get($input, 'life_id');
    //         $row->owner_occupation = array_get($input, 'owner_occupation');
    //         $row->life_occupation = array_get($input, 'life_occupation');
    //         $row->owner_gender = array_get($input, 'owner_gender');
    //         $row->life_gender = array_get($input, 'life_gender');
    //         $row->owner_dob = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'owner_dob'). '00:00 AM');
    //         $row->life_dob = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'life_dob'). '00:00 AM');
    //         $row->owner_age = array_get($input, 'owner_age');
    //         $row->life_age = array_get($input, 'life_age');
    //         $row->contact = array_get($input, 'contact');
    //         $row->address = array_get($input, 'address');
    //         $row->postal_code = array_get($input, 'postal_code');
    //         $row->email = array_get($input, 'email');
    //         $row->type_of_plan = array_get($input, 'type_of_plan');
    //         $row->sum_assured = array_get($input, 'sum_assured');
    //         $row->amount = array_get($input, 'amount');
    //         $row->currency =  array_get($input, 'currency');
    //         $row->prem_currency = array_get($input, 'prem_currency');
    //         $row->prem_account = array_get($input, 'prem_account');
    //         $row->policy_term = array_get($input, 'policy_term');
    //         $row->payment_frequency = array_get($input, 'payment_frequency');
    //         $row->mode_of_payment = array_get($input, 'mode_of_payment');
    //         $row->ape = array_get($input, 'ape');
    //         $row->cka = array_get($input, 'cka');
    //         $row->source = array_get($input, 'source');
    //         $row->visor = 'advisor';
    //     } else {
    //         $log_firstname              = $row->firstname;
    //         $log_lastname               = $row->lastname;
    //         $log_annual_income_range   = $row->annual_income_range;
    //         $log_life_firstname        = $row->life_firstname;
    //         $log_life_lastname         = $row->life_lastname;
    //         $log_owner_id              = $row->owner_id;
    //         $log_life_id               = $row->life_id;
    //         $log_owner_occupation      = $row->owner_occupation;
    //         $log_life_occupation       = $row->life_occupation;
    //         $log_owner_gender          = $row->owner_gender;
    //         $log_life_gender           = $row->life_gender;
    //         $log_owner_dob             = $row->owner_dob;
    //         $log_life_dob              = $row->life_dob;
    //         $log_owner_age             = $row->owner_age;
    //         $log_life_age              = $row->life_age;
    //         $log_contact               = $row->contact;
    //         $log_address               = $row->address;
    //         $log_postal_code           = $row->postal_code;
    //         $log_email                 = $row->email;
    //         $log_type_of_plan          = $row->type_of_plan;
    //         $log_sum_assured            = $row->sum_assured;
    //         $log_amount                = $row->amount;
    //         $log_currency             = $row->currency;
    //         $log_prem_currency        = $row->prem_currency;
    //         $log_prem_account         = $row->prem_account;
    //         $log_policy_term          = $row->policy_term;
    //         $log_payment_frequency   = $row->payment_frequency;
    //         $log_mode_of_payment       = $row->mode_of_payment;
    //         $log_ape                   = $row->ape;
    //         $log_cka                   = $row->cka;
    //         $log_source                = $row->source;

    //         $row = new ProductionCases;
    //         $row->user_id = array_get($input, 'user_sales_id');
    //         $row->firstname = array_get($input, 'firstname');
    //         $row->lastname = array_get($input, 'lastname');
    //         $row->annual_income_range = array_get($input, 'annual_income_range');
    //         $row->life_firstname = array_get($input, 'life_firstname');
    //         $row->life_lastname = array_get($input, 'life_lastname');
    //         $row->owner_id = array_get($input, 'owner_id');
    //         $row->life_id = array_get($input, 'life_id');
    //         $row->owner_occupation = array_get($input, 'owner_occupation');
    //         $row->life_occupation = array_get($input, 'life_occupation');
    //         $row->owner_gender = array_get($input, 'owner_gender');
    //         $row->life_gender = array_get($input, 'life_gender');
    //         $row->owner_dob = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'owner_dob'). '00:00 AM');
    //         $row->life_dob = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'life_dob'). '00:00 AM');
    //         $row->owner_age = array_get($input, 'owner_age');
    //         $row->life_age = array_get($input, 'life_age');
    //         $row->contact = array_get($input, 'contact');
    //         $row->address = array_get($input, 'address');
    //         $row->postal_code = array_get($input, 'postal_code');
    //         $row->email = array_get($input, 'email');
    //         $row->type_of_plan = array_get($input, 'type_of_plan');
    //         $row->sum_assured = array_get($input, 'sum_assured');
    //         $row->amount = array_get($input, 'amount');
    //         $row->currency =  array_get($input, 'currency');
    //         $row->prem_currency = array_get($input, 'prem_currency');
    //         $row->prem_account = array_get($input, 'prem_account');
    //         $row->policy_term = array_get($input, 'policy_term');
    //         $row->payment_frequency = array_get($input, 'payment_frequency');
    //         $row->mode_of_payment = array_get($input, 'mode_of_payment');
    //         $row->ape = array_get($input, 'ape');
    //         $row->cka = array_get($input, 'cka');
    //         $row->source = array_get($input, 'source');
    //         $row->visor = 'Advisor';
    //     }

    //     // save model
    //     $row->save();

    //     // if ($new) {

    //     //     $case = new ProductionIncept;
    //     //     $case->info = array_get($input, 'info');
    //     //     $case->code = array_get($input, 'code');
    //     //     $case->name = array_get($input, 'name');
    //     //     $case->case_policy_id = $row->id;
    //     //     $case->visor = 'advisor';
    //     //     $case->case = 'submission';
    //     //     $case->edit_id = Auth::user()->id;
    //     //     $case->save();

    //     //     $case->case_id = 'CASE-' . sprintf("%04d", $case->id);
    //     //     $case->save();
    //     // } else {
    //     //     $case->info = array_get($input, 'info');
    //     //     $case->code = array_get($input, 'code');
    //     //     $case->name = array_get($input, 'name');
    //     //     $case->edit_id = Auth::user()->id;
    //     //     $case->save();
    //     // }

    //     // if ($new) {
    //     //     //create log
    //     //     LogPolicy::create(['policy_id' => $row->id, 'edit_id' => Auth::user()->id, 'activity' => 'Policy Case Added']);
    //     // } else {
    //     //     $updated = false;
    //     //     //create log
    //     //     if($log_policy_no != array_get($input, 'policy_id')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Policy id Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_policy_no != array_get($input, 'policy_no')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Policy number Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_policy_type!= array_get($input, 'policy_type')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Policy type Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_comp_code != array_get($input, 'comp_code')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Component code Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_policy_term != array_get($input, 'policy_term')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Policy term Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_contract_curr != array_get($input, 'contract_curr')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Contract currency Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_sum_insured != array_get($input, 'sum_insured')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Sum insured Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_incept_date != array_get($input, 'incept_date')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Incept date Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_install_form_date != array_get($input, 'install_form_date')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Installment form date Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_billing_freq != array_get($input, 'billing_freq')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Billing frequency Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_policy_exp_date != array_get($input, 'policy_exp_date')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Policy expiry date Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_gross_prem_paid != array_get($input, 'gross_prem_paid')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Gross premium paid Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_net_prem_paid != array_get($input, 'net_prem_paid')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Net premium paid Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_gross_prem_inc_gst != array_get($input, 'gross_prem_inc_gst')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Gross premium inc gst Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_net_prem_inc_gst != array_get($input, 'net_prem_inc_gst')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Net premium inc gst Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_prem_wo_com != array_get($input, 'prem_wo_com')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Net premium w/o com Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_prem_conv_rate != array_get($input, 'prem_conv_rate')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Premium conv rate Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_payment_curr != array_get($input, 'payment_curr')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Payment currency Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_com_or != array_get($input, 'com_or')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Commission OR Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_com_run_date != array_get($input, 'com_run_date')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Commission run date Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_com_conv_rate != array_get($input, 'com_conv_rate')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Commission conv rate Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_trans_code != array_get($input, 'trans_code')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Transaction code Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_prem_term != array_get($input, 'prem_term')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Premium term Changed']);
    //     //         $updated = true;
    //     //     }
    //     //     if($log_date_issue != array_get($input, 'date_issue')) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Date issue Changed']);
    //     //         $updated = true;
    //     //     
    //     //     if ($updated) {
    //     //         LogPolicy::create(['policy_id' => array_get($input, 'id'), 'edit_id' => Auth::user()->id, 'activity' => 'Policy Case Updated']);
    //     //     }
    //     // }





    //      // return
    //     return Response::json(['body' => 'Case created successfully.']);
    // }

    // public function caseSubmissionSupervisorView() {
    //     $id = Request::input('id');

    //     // prevent non-admin from viewing deactivated row
    //     $row = ProductionIncept::with('getCasePolicy')->find($id);

    //     if($row) {
    //         return Response::json($row);
    //     } else {
    //         return Response::json(['error' => "Invalid row specified"]);
    //     }
    // }

        /**
     * Delete Pending
     *
     */
    public function deletePending() {
        $row = ProductionCases::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->status = 2;
                $row->save();

            // return
            return Response::json(['body' => 'Case has been deleted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    public function getMainPlan() {

      // $get_plan = ProductRider::select('product_mains.provider_id', 'product_riders.id', 'product_mains.name', 'product_riders.status')
      //                           ->leftJoin('product_mains', 'product_mains.id', '=', 'product_riders.main_id')
      //                           ->where('product_mains.provider_id', '=', Request::input('id'))
      //                           ->where('product_riders.status', '=', 1)
      //                           ->groupBy('product_mains.id')
      //                           ->get();

      $get_plan = ProductMain::with('getCaseRiders')
                              ->where('provider_id', '=', Request::input('id'))
                              ->orderBy('name', 'asc')
                              ->get();
                                // dd($get_plan->take(10));

      $rows = '';

      if ($get_plan->count() > 0) {
        $ctr = 0;
        $rows = '<option class="hide" value="">Select Plan:</option>';
        foreach ($get_plan as $key => $field) {
          if ($field->getCaseRiders->count() > 0) {
            $ctr += 1;
            $rows .= '<option value="' . $field->id . '">' . $field->name . '</option>';
          }
        }
        if ($ctr == 0) {
          $rows = '<option class="hide" value="">0 Plan/s Found.</option>';
        }

      } else {
        $rows = '<option class="hide" value="">0 Plan/s Found.</option>';
      }

      return Response::json($rows);

    }

    public function getRiderPlan() {

      $rows = Product::where('provider_id', Request::input('id'))
                      ->where('classification', '=', 'Rider')
                      ->get();


      $result['lists'] = $rows->toArray();

      return Response::json($result);

    }

    public function getRate() {

      $rows = ProductMain::find(Request::input('id'));

      $aviva = ProductMain::where(function($query) {
                                  $query->where('name', 'LIKE', '%MyShield Plan 1%')
                                        ->orWhere('name', 'LIKE', '%MyShield Plan 2%')
                                        ->orWhere('name', 'LIKE', '%MyShield Plan 3%')
                                        ->orWhere('name', 'LIKE', '%MyShield Standard Plan%')
                                        ->orWhere('name', 'LIKE', '%MyHealth Plus Option A%')
                                        ->orWhere('name', 'LIKE', '%MyHealth Plus Option C%')
                                        ->orWhere('name', 'LIKE', 'MyCare')
                                        ->orWhere('name', 'LIKE', 'MyCarePlus');
                                  })
                          ->where('provider_id', 1)
                          ->lists('id');

      $axa = ProductMain::where(function($query) {
                                  $query->where('name', 'LIKE', '%AXA Shield Plan A%')
                                          ->orWhere('name', 'LIKE', '%AXA Shield Plan B%')
                                          ->orWhere('name', 'LIKE', '%AXA Shield Standard Plan%');
                                  })
                          ->where('provider_id', 2)
                          ->lists('id');

      $ntuc = ProductMain::where(function($query) {
                                  $query->where('name', 'LIKE', '%PrimeShield%');
                                  })
                          ->where('provider_id', 8)
                          ->lists('id');

      $ids = array_merge($aviva->toArray(), $axa->toArray());
      $ids = array_merge($ids, $ntuc->toArray());

      $check = ProductMain::whereIn('id', $ids)->where('id', $rows->id)->first();

      $gst = 1;

      if ($check) {
        $gst = 1.07;
      }
      
      $result['list'] = $rows->toArray();
      $result['gst'] = $gst;

      return Response::json($result);

    }


    public function getRider() {

      $rows = ProductRider::where('main_id', '=', Request::input('id'))->orderBy('name', 'asc')->get();
      

      $row = '<option value="" class="hide">Select Plan</option>';

      foreach ($rows as $key => $field) {
        $row .= '<option value="' . $field->id . '">' . $field->name . '</option>';
      }

      return Response::json($row);
    }
    
    /**
     * Save model
     *
     */
    public function caseSave() {
      // dd(Request::all());
      $new = true;

      $input = Input::all();
      // check if an ID is passed
    
      if(array_get($input, 'id')) {

        // get the user info
        $row = ProductionCases::find(array_get($input, 'id'));

        if(is_null($row)) {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
        // this is an existing row
        $new = false;
      }
      
      $rules = [
          'user_sales_id' => (Auth::user()->usertype_id == 8 ? '' : 'required'),
          'firstname' => 'required',
          'lastname' => 'required',
          'annual_income_range' => 'required',
          'owner_id' => 'required',
          // 'life_id' => 'required',
          'address' => 'required',
          'postal_code' => 'required',
          // 'production_email' => 'required',
          'provider_list' => 'required',
          'main_product_id' => 'required',
          'main_product_id' => 'required',
          // 'main_sum_assured' => 'numeric',
          'currency_value' => 'required',
          'currency_id' => 'required',
          'currency2_id' => 'required',
          'mode_others' => (array_get($input, 'mode_of_payment') == "Others" ? 'required' : ''),
          'policy_term' => 'required',
          'payment_frequency' => 'required',
          'mode_of_payment' => 'required',
          'nationality_id' => 'required',
          'residency_status' => 'required',
          'residency_others' => (array_get($input, 'residency_status') == "Others" ? 'required' : ''),
          'nationality_id' => 'required',
          'source' => 'required',
      ];

      if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {

      } else {
        if (array_get($input, 'upload_2')) {
          $rules['upload_name_2'] = 'required';
        }

        if (array_get($input, 'upload_4')) {
          $rules['upload_name_4'] = 'required';
        }

        if (array_get($input, 'upload_5')) {
          $rules['upload_name_5'] = 'required';
        }
      }

      if ($new) {
        if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {

        } else {
          $rules['upload'] = 'required';
          $rules['upload_name'] = 'required';
          $rules['upload_3'] = 'required';
          $rules['upload_name_3'] = 'required';
          $rules['upload_6'] = 'required';
          $rules['upload_name_6'] = 'required';
        }
      } else {
        if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {

        } else {
          if (!$row->upload) {
            $rules['upload'] = 'required';
            $rules['upload_name'] = 'required';
          } else if (array_get($input, 'upload_remove')) {
            $rules['upload'] = 'required';
            $rules['upload_name'] = 'required';
          }
          if (!$row->upload_3) {
            $rules['upload_3'] = 'required';
            $rules['upload_name_3'] = 'required';
          } else if (array_get($input, 'upload_remove_3')) {
            $rules['upload_3'] = 'required';
            $rules['upload_name_3'] = 'required';
          }
          if (!$row->upload_6) {
            $rules['upload_6'] = 'required';
            $rules['upload_name_6'] = 'required';
          } else if (array_get($input, 'upload_remove_6')) {
            $rules['upload_6'] = 'required';
            $rules['upload_name_6'] = 'required';
          }
        }

      }

      // field name overrides
      $names = [

      ];

      $messages = [
          'provider_list' => 'errors-provider_list|*Select value.',
          'main_product_id' => 'errors-main_product_id|*Select value.',
      ];
        
      $contact_nos = array();
      $contact_id = Input::get('contact_id');
      $contact = Input::get('contact');

      // validate child rows
      if($contact) {
          foreach($contact as $key => $val) {
              if($contact[$key]) {
                  $subrules = array(
                      "contact.$key" => 'required',
                  );

                  $subfn = array(
                      "contact.$key" => 'contact',
                  );

                  if($contact_id[$key]) {
                      $subrules["contact_id.$key"] = 'exists:production_case_contacts,id';
                  }

                  $rules = array_merge($rules, $subrules);
                  $names = array_merge($names, $subfn);
                  $contact_nos[] = $contact[$key];
              } else {
                  unset($contact_id[$key]);
                  unset($contact[$key]);
              }
          }
      }
      // dd($input);
      $product_id_nos = array();
      $riders_id = Input::get('riders_id');
      $rider_product_id = Input::get('rider_product_id');
      $rider_sum_assured = Input::get('rider_sum_assured');
      $riders_sub_id = Input::get('riders_sub_id');

      // validate child rows
      if($rider_product_id) {
          foreach($rider_product_id as $key => $val) {
              if($rider_product_id[$key]) {
                  $subrules = array(
                      "rider_product_id.$key" => 'required',
                      "rider_sum_assured.$key" => 'required|numeric',
                  );

                  $subfn = array(

                  );

                  $submsg = array(
                    "rider_sum_assured.$key.numeric" => 'error-rider_sum_assured_' . $riders_sub_id[$key] . '|*Numeric value',
                    "rider_sum_assured.$key.required" => 'error-rider_sum_assured_' . $riders_sub_id[$key] . '|*Field required',
                    "rider_product_id.$key.required" => 'error-rider_product_id_' . $riders_sub_id[$key] . '|*Select value',
                  );

                  // if($riders_id[$key]) {
                  //     $subrules["rider_product_id.$key"] = 'exists:production_case_riders,id';
                  // }

                  $rules = array_merge($rules, $subrules);
                  $names = array_merge($names, $subfn);
                  $messages = array_merge($messages, $submsg);
                  $product_id_nos[] = $rider_product_id[$key];
              } else {
                  unset($riders_id[$key]);
                  unset($rider_product_id[$key]);
                  unset($rider_sum_assured[$key]);
              }
          }
      }


      // do validation
      $validator = Validator::make(Input::all(), $rules, $messages);
      $validator->setAttributeNames($names);
      
      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      // create model if new
      if($new) {
        $row = new ProductionCases;

        $row->submission_date = Carbon::createFromFormat('d/m/Y H:i:s',  Carbon::now()->format('d/m/Y') . ' 00:00:00');

        $row->user_id = (Auth::user()->usertype_id == 8 ? Auth::user()->id : array_get($input, 'user_sales_id'));
        $row->firstname = array_get($input, 'firstname');
        $row->lastname = array_get($input, 'lastname');

        if (array_get($input, 'firstname') && array_get($input, 'lastname')) {
          $row->fullname = array_get($input, 'lastname') . ' ' . array_get($input, 'firstname');
          $row->fullname_first = array_get($input, 'firstname') . ' ' . array_get($input, 'lastname');
        } else if (array_get($input, 'firstname') && !array_get($input, 'lastname')) {
          $row->fullname = array_get($input, 'firstname');
          $row->fullname_first = array_get($input, 'firstname');
        } else if (!array_get($input, 'firstname') && array_get($input, 'lastname')) {
          $row->fullname = array_get($input, 'lastname');
          $row->fullname_first = array_get($input, 'lastname');
        } else {
          $row->fullname = null;
          $row->fullname_first = null;
        }

        $row->annual_income_range = array_get($input, 'annual_income_range');
        $row->life_firstname = array_get($input, 'life_firstname');
        $row->life_lastname = array_get($input, 'life_lastname');
        $row->owner_id = array_get($input, 'owner_id');
        $row->life_id = array_get($input, 'life_id');
        $row->owner_occupation = array_get($input, 'owner_occupation');

        $row->life_occupation = array_get($input, 'life_occupation');

        if (array_get($input, 'owner_age')) {
          $row->owner_age = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_age'));
        } else {
          $row->owner_age = null;
        }

        $row->owner_bday = array_get($input, 'owner_bday');
        $row->owner_gender = array_get($input, 'owner_gender');

        if (array_get($input, 'same')) {
          $row->life_gender = array_get($input, 'owner_gender');
          if (array_get($input, 'owner_age')) {
            $row->life_age = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_age'));
          } else {
            $row->life_age = null;
          }
          $row->life_bday = array_get($input, 'owner_bday');
        } else {
          $row->life_gender = array_get($input, 'life_gender');
          if (array_get($input, 'life_age')) {
            $row->life_age = Carbon::createFromFormat('d/m/Y', array_get($input, 'life_age'));
          } else {
            $row->life_age = null;
          }
          $row->life_bday = array_get($input, 'life_bday');
        }

        if (array_get($input, 'owner_dob')) {
          $row->owner_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_dob'));
        } else {
          $row->owner_dob = null;
        }  
        
        if (array_get($input, 'life_dob')) {
          $row->life_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'life_dob'));
        } else {
          $row->life_dob = null;
        }

        $row->address = array_get($input, 'address');
        $row->postal_code = array_get($input, 'postal_code');
        $row->email = array_get($input, 'production_email');
        $row->provider_list = array_get($input, 'provider_list');
        $row->main_product_id = array_get($input, 'main_product_id');
        $row->main_sum_assured = array_get($input, 'main_sum_assured');
        $row->conversion_rate = array_get($input, 'conversion_rate');
        $row->conversion_year = array_get($input, 'conversion_year');
        $row->conversion_gst = array_get($input, 'conversion_gst');
        $row->currency = array_get($input, 'currency');
        $row->currency2 = array_get($input, 'currency2');
        $row->currency_value = array_get($input, 'currency_value');
        $row->currency_value2 = array_get($input, 'currency_value2');

        $row->currency_id = array_get($input, 'currency_id');
        $row->currency_rate = array_get($input, 'currency_rate');
        $row->currency2_id = array_get($input, 'currency2_id');
        $row->currency2_rate = array_get($input, 'currency2_rate');

        $row->consent_marketing = array_get($input, 'consent_marketing');
        $row->consent_servicing = array_get($input, 'consent_servicing');
        $row->annual_currency_value2 = (array_get($input, 'currency_value2') * 12);
        $row->backdate = array_get($input, 'backdate_value');
        $row->conv_total = array_get($input, 'conv_total');
        $row->life_total_amount = array_get($input, 'life_total_amount');
        $row->policy_term = array_get($input, 'policy_term');
        $row->payment_frequency = array_get($input, 'payment_frequency');

        $row->mode_of_payment = array_get($input, 'mode_of_payment');

        if (array_get($input, 'mode_of_payment') == "Others") {
          $row->mode_others = array_get($input, 'mode_others');
        }

        $row->nationality_id = array_get($input, 'nationality_id');
        $row->residency_status = array_get($input, 'residency_status');

        if (array_get($input, 'residency_status') == "Others") {
          $row->residency_others = array_get($input, 'residency_others');
        }

        $row->ape = array_get($input, 'ape');
        $row->source = array_get($input, 'source');

        // save model
        $row->save();

        if (Request::hasFile('upload')) {
          $file = Request::file('upload');

          $destinationPath = storage_path('case');
          $filenaming = array_get($input, 'upload_name');
          $filename_orig = array_get($input, 'upload_name');

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-one.'.$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload   = $row->id . '-one.' . $extension;
            $row->upload_title = $filenaming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_2')) {
          $file = Request::file('upload_2');

          $destinationPath = storage_path('case');
          $file2naming = array_get($input, 'upload_name_2');
          $filename_orig = $file->getClientOriginalName();

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-two.'.$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_2   = $row->id . '-two.' . $extension;
            $row->upload_2_title = $file2naming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_3')) {
          $file = Request::file('upload_3');

          $destinationPath = storage_path('case');
          $file3naming = array_get($input, 'upload_name_3');
          $filename_orig = $file->getClientOriginalName();

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-three.'.$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_3   = $row->id . '-three.' . $extension;
            $row->upload_3_title = $file3naming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_4')) {
          $file = Request::file('upload_4');

          $destinationPath = storage_path('case');
          $file4naming = array_get($input, 'upload_name_4');
          $filename_orig = array_get($input, 'upload_name');

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-four.'.$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_4   = $row->id . '-four.' . $extension;
            $row->upload_4_title = $file4naming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_5')) {
          $file = Request::file('upload_5');

          $destinationPath = storage_path('case');
          $file5naming = array_get($input, 'upload_name_5');
          $filename_orig = $file->getClientOriginalName();

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-five.' .$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_5   = $row->id . '-five.' . $extension;
            $row->upload_5_title = $file5naming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_6')) {
          $file = Request::file('upload_6');

          $destinationPath = storage_path('case');
          $file6naming = array_get($input, 'upload_name_6');
          $filename_orig = $file->getClientOriginalName();

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-six.' .$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_6   = $row->id . '-six.' . $extension;
            $row->upload_6_title = $file6naming . '.' . $extension;
            $row->save();
          }
        }

        //trigger notification
        $row_noti = new Notification;
        $row_noti->user_id =  Auth::user()->id;
        $row_noti->activity= " added ".$row->firstname." ".$row->lastname."'s production case!";
        $row_noti->controller =  "PolicyController@notification";
           
        if (Auth::user()->usertype_id == '8') { //if sales user
          $row_noti->usertype_id = "2"; //sales user
        } else{
          $row_noti->usertype_id = "1"; //system user
        }

        $row_noti->save();

        //save to per user
        $row_noti  = Notification::orderBy('id', 'desc')->first();
        if (Auth::user()->usertype_id == '8') {
          $row_user = User::where(function($query) {
                                   $query->where('status', '=', '1')
                                   ->where('usertype_id', '<=', '7');
                                  })->get();
        } else {
          $row_user = User::where(function($query) {
                                   $query->where('status', '=', '1')
                                   ->where('usertype_id', '=', '8');
                                  })->get();
        }

        foreach ($row_user as $key => $field) {      
          $rows = new NotificationPerUser;
          $rows->user_id = $field->id;
          $rows->notification_id = $row_noti->id;
          $rows->save();
        }
      } else {
        $row->user_id = (Auth::user()->usertype_id == 8 ? Auth::user()->id : array_get($input, 'user_sales_id'));
        $row->firstname = array_get($input, 'firstname');
        $row->lastname = array_get($input, 'lastname');

        if (array_get($input, 'firstname') && array_get($input, 'lastname')) {
          $row->fullname = array_get($input, 'lastname') . ' ' . array_get($input, 'firstname');
          $row->fullname_first = array_get($input, 'firstname') . ' ' . array_get($input, 'lastname');
        } else if (array_get($input, 'firstname') && !array_get($input, 'lastname')) {
          $row->fullname = array_get($input, 'firstname');
          $row->fullname_first = array_get($input, 'firstname');
        } else if (!array_get($input, 'firstname') && array_get($input, 'lastname')) {
          $row->fullname = array_get($input, 'lastname');
          $row->fullname_first = array_get($input, 'lastname');
        } else {
          $row->fullname = null;
          $row->fullname_first = null;
        }

        $row->annual_income_range = array_get($input, 'annual_income_range');
        $row->life_firstname = array_get($input, 'life_firstname');
        $row->life_lastname = array_get($input, 'life_lastname');
        $row->owner_id = array_get($input, 'owner_id');
        $row->life_id = array_get($input, 'life_id');
        $row->owner_occupation = array_get($input, 'owner_occupation');
        $row->life_occupation = array_get($input, 'life_occupation');

        if (array_get($input, 'same')) {
          $row->life_gender = array_get($input, 'owner_gender');
          if (array_get($input, 'backdate')) {
            $row->life_age = array_get($input, 'owner_age');
          }
        } else {
          $row->life_gender = array_get($input, 'life_gender');
        }

        if (array_get($input, 'owner_dob')) {
          $row->owner_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_dob'));
        } else {
          $row->owner_dob = null;
        } 

        if (array_get($input, 'life_dob')) {
          $row->life_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'life_dob'));
        } else {
          $row->life_dob = null;
        }

        if (array_get($input, 'owner_age')) {
          $row->owner_age = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_age'));
        } else {
          $row->owner_age = null;
        }

        $row->owner_bday = array_get($input, 'owner_bday');
        $row->owner_gender = array_get($input, 'owner_gender');

        if (array_get($input, 'same')) {
          $row->life_gender = array_get($input, 'owner_gender');
          if (array_get($input, 'owner_age')) {
            $row->life_age = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_age'));
          } else {
            $row->life_age = null;
          }
          $row->life_bday = array_get($input, 'owner_bday');
        } else {
          $row->life_gender = array_get($input, 'life_gender');
          if (array_get($input, 'life_age')) {
            $row->life_age = Carbon::createFromFormat('d/m/Y', array_get($input, 'life_age'));
          } else {
            $row->life_age = null;
          }
          $row->life_bday = array_get($input, 'life_bday');
        }

        if (array_get($input, 'owner_dob')) {
          $row->owner_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_dob'));
        } else {
          $row->owner_dob = null;
        }  
        
        if (array_get($input, 'life_dob')) {
          $row->life_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'life_dob'));
        } else {
          $row->life_dob = null;
        }


        // $row->contact = array_get($input, 'contact');
        $row->address = array_get($input, 'address');
        $row->postal_code = array_get($input, 'postal_code');
        $row->email = array_get($input, 'production_email');
        $row->provider_list = array_get($input, 'provider_list');
        $row->main_product_id = array_get($input, 'main_product_id');

        if (array_get($input, 'main_sum_assured')) {
          $row->main_sum_assured = array_get($input, 'main_sum_assured');
        } else {
          $row->main_sum_assured = null;
        }

        $row->conversion_rate = array_get($input, 'conversion_rate');
        $row->conversion_year = array_get($input, 'conversion_year');
        $row->conversion_gst = array_get($input, 'conversion_gst');
        $row->currency = array_get($input, 'currency');
        $row->currency2 = array_get($input, 'currency2');
        $row->currency_value = array_get($input, 'currency_value');
        $row->currency_value2 = array_get($input, 'currency_value2');

        $row->currency_id = array_get($input, 'currency_id');
        $row->currency_rate = array_get($input, 'currency_rate');
        $row->currency2_id = array_get($input, 'currency2_id');
        $row->currency2_rate = array_get($input, 'currency2_rate');

        $row->backdate = array_get($input, 'backdate_value');
        $row->conv_total = array_get($input, 'conv_total');
        $row->life_total_amount = array_get($input, 'life_total_amount');
        $row->policy_term = array_get($input, 'policy_term');
        $row->consent_marketing = array_get($input, 'consent_marketing');
        $row->consent_servicing = array_get($input, 'consent_servicing');
        $row->payment_frequency = array_get($input, 'payment_frequency');
        $row->mode_of_payment = array_get($input, 'mode_of_payment');
        $row->annual_currency_value2 = (array_get($input, 'currency_value2') * 12);

        if (array_get($input, 'mode_of_payment') == "Others") {
          $row->mode_others = array_get($input, 'mode_others');
        } else {
          $row->mode_others = null;
        }

        $row->nationality_id = array_get($input, 'nationality_id');
        $row->residency_status = array_get($input, 'residency_status');

        if (array_get($input, 'residency_status') == "Others") {
          $row->residency_others = array_get($input, 'residency_others');
        } else {
          $row->residency_others = null;
        }

        $row->ape = array_get($input, 'ape');
        $row->source = array_get($input, 'source');
        // save model
        $row->save();

        if (!Request::input('upload_remove') || (Request::input('upload_remove') == "yes" && Request::hasFile('upload'))) {
          if (Request::hasFile('upload')) {
            $file = Request::file('upload');

            $destinationPath = storage_path('case');
            $filenaming = array_get($input, 'upload_name');
            $filename_orig = $file->getClientOriginalName();

            $temp = explode(".", $filename_orig);
            $extension = end($temp);

            $filename = $row->id . '-one.' . $extension;
            $upload_success = $file->move($destinationPath, $filename);

            if($upload_success) {
              $row->upload = $row->id . '-one.' . $extension;
              $row->upload_title = $filenaming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload = null;
          $row->upload_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_2') || (Request::input('upload_remove_2') == "yes" && Request::hasFile('upload_2'))) {
          if (Request::hasFile('upload_2')) {
            $file2 = Request::file('upload_2');

            $destinationPath = storage_path('case');
            $file2naming = array_get($input, 'upload_name_2');
            $file2name_orig = $file2->getClientOriginalName();

            $temp = explode(".", $file2name_orig);
            $extension = end($temp);

            $file2name = $row->id . '-two.' . $extension;
            $upload_success = $file2->move($destinationPath, $file2name);

            if( $upload_success)  {
              $row->upload_2   = $row->id . '-two.' . $extension;
              $row->upload_2_title = $file2naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_2 = null;
          $row->upload_2_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_3') || (Request::input('upload_remove_3') == "yes" && Request::hasFile('upload_3'))) {
          if (Request::hasFile('upload_3')) {
            $file3 = Request::file('upload_3');

            $destinationPath = storage_path('case');
            $file3naming = array_get($input, 'upload_name_3');
            $file3name_orig = $file3->getClientOriginalName();

            $temp = explode(".", $file3name_orig);
            $extension = end($temp);

            $file3name = $row->id . '-three.' . $extension;
            $upload_success = $file3->move($destinationPath, $file3name);

            if( $upload_success)  {
              $row->upload_3   = $row->id . '-three.' . $extension;
              $row->upload_3_title = $file3naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_3 = null;
          $row->upload_3_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_4') || (Request::input('upload_remove_4') == "yes" && Request::hasFile('upload_4'))) {
          if (Request::hasFile('upload_4')) {
            $file4 = Request::file('upload_4');

            $destinationPath = storage_path('case');
            $file4naming = array_get($input, 'upload_name_4');

            $file4name_orig = $file4->getClientOriginalName();

            $temp = explode(".", $file4name_orig);
            $extension = end($temp);

            $file4name = $row->id . '-four.' . $extension;
            $upload_success = $file4->move($destinationPath, $file4name);

            if( $upload_success)  {
              $row->upload_4   = $row->id . '-four.' . $extension;
              $row->upload_4_title = $file4naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_4 = null;
          $row->upload_4_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_5') || (Request::input('upload_remove_5') == "yes" && Request::hasFile('upload_5'))) {
          if (Request::hasFile('upload_5')) {
            $file5 = Request::file('upload_5');

            $destinationPath = storage_path('case');
            $file5naming = array_get($input, 'upload_name_5');
            $file5name_orig = $file5->getClientOriginalName();

            $temp = explode(".", $file5name_orig);
            $extension = end($temp);

            $file5name = $row->id . '-five.' . $extension;
            $upload_success = $file5->move($destinationPath, $file5name);

            if( $upload_success)  {
              $row->upload_5   = $row->id . '-five.' . $extension;
              $row->upload_5_title = $file5naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_5 = null;
          $row->upload_5_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_6') || (Request::input('upload_remove_6') == "yes" && Request::hasFile('upload_6'))) {
          if (Request::hasFile('upload_6')) {
            $file6 = Request::file('upload_6');

            $destinationPath = storage_path('case');
            $file6naming = array_get($input, 'upload_name_6');
            $file6name_orig = $file6->getClientOriginalName();

            $temp = explode(".", $file6name_orig);
            $extension = end($temp);

            $file6name = $row->id . '-five.' . $extension;
            $upload_success = $file6->move($destinationPath, $file6name);

            if( $upload_success)  {
              $row->upload_6   = $row->id . '-five.' . $extension;
              $row->upload_6_title = $file6naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_6 = null;
          $row->upload_6_title = null;
          $row->save();
        }

        //trigger notification
        $row_noti = new Notification;
        $row_noti->user_id =  Auth::user()->id;
        $row_noti->activity =" Edited ".$row->firstname." ".$row->lastname."'s production case!";
        $row_noti->controller =  "PolicyController@notification";

        if (Auth::user()->usertype_id == '8') { //if sales user
          $row_noti->usertype_id = "2"; //sales user
        } else {
          $row_noti->usertype_id = "1"; //system user
        }

        $row_noti->save();

        //save to per user
        $row_noti  = Notification::orderBy('id', 'desc')->first();

        if (Auth::user()->usertype_id == '8') {
          $row_user = User::where(function($query) {
                                  $query->where('status', '=', '1')
                                        ->where('usertype_id', '<=', '7');
                                })->get();
        } else {
          $row_user = User::where(function($query) {
                                  $query->where('status', '=', '1')
                                        ->where('usertype_id', '=', '8');
                                })->get();
        }

        foreach ($row_user as $key => $field) {      
          $rows = new NotificationPerUser;
          $rows->user_id = $field->id;
          $rows->notification_id = $row_noti->id;
          $rows->save();
        }            
      }

      // save rows
      if($contact_nos) {
          foreach($contact_nos as $key => $child) {
              if($contact_id[$key]) {
                  // this is an existing visa
                  $cinfo = ProductionCaseContact::find($contact_id[$key]);
              } else {
                  // this is a new visa
                  $cinfo = new ProductionCaseContact;
                  $cinfo->production_id = $row->id;
              }

              $cinfo->contact = $contact[$key];
              $cinfo->save();

              // add to visa ID array
              if(!$contact_id[$key]) {
                  $contact_id[] = $cinfo->id;
              }
          }
      }

      // save rows
      if($product_id_nos) {
          foreach($product_id_nos as $key => $child) {
              if($riders_id[$key]) {
                  // this is an existing visa
                  $pinfo = ProductionCaseRider::find($riders_id[$key]);
              } else {
                  // this is a new visa
                  $pinfo = new ProductionCaseRider;
                  $pinfo->production_id = $row->id;
              }

              $pinfo->product_id = $rider_product_id[$key];
              $pinfo->sum_assured = $rider_sum_assured[$key];
              $pinfo->sub_id = $riders_sub_id[$key];
              $pinfo->save();

              // add to visa ID array
              if(!$riders_id[$key]) {
                  $riders_id[] = $pinfo->id;
              }
          }
      }

      // delete excluded visa rows
      if(isset($row)) {
          $del = ProductionCaseRider::select('id')
                            ->where('production_id', $row->id);
                               
          if(!empty($riders_id)) {
              $del->whereNotIn('id', $riders_id);
          }

          $drows = $del->get();

          foreach($drows as $d) {
              ProductionCaseRider::destroy($d->id);
          }
      }

      // delete excluded visa rows
      if(isset($row)) {
          $del = ProductionCaseContact::select('id')
                            ->where('production_id', $row->id);
                               
          if(!empty($contact_id)) {
              $del->whereNotIn('id', $contact_id);
          }

          $drows = $del->get();

          foreach($drows as $d) {
              ProductionCaseContact::destroy($d->id);
          }
        }

      // return
        if(Request::input('selected_client'))
        {
          $sel_client = 1;
        }
        else
        {
          $sel_client = 0;
        }
        
        if($row->selected_client)
        {
          $row->selected_client = $sel_client;
          $row->save();
        }



      return Response::json(['body' => 'Changes have been saved.']);
    }

    public function removeCaseSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select cases first.']); 
        } else {
            // process
            foreach ($ids as $id) {
              $row = ProductionCases::find($id);
              if(!is_null($row)) {
                $status = $row->status;
                // you cannot remove user that already been remove
                if ($status != 2) {
                  $row->status = 2;
                  $row->save();
                }
              }
            }

          return Response::json(['body' => 'Selected cases has been Removed.']);
        }
    } 

    public function removeSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select cases first.']); 
        } else {
            // process
            foreach ($ids as $id) {
              $row = ProductionIncept::find($id);
              if(!is_null($row)) {
                $status = $row->status;
                // you cannot remove user that already been remove
                if ($status != 2) {
                  $row->status = 2;
                  $row->save();
                }
              }
            }

          return Response::json(['body' => 'Selected cases has been Removed.']);
        }
    } 

    public function caseInceptionAdvisorView() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = ProductionIncept::with('getCasePolicy')->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Save model
     *
     */
    public function caseInceptionAdvisorSave() {
    
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {


            // get the user info
            $row = ProductionCases::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // get the user info
            $case = ProductionIncept::where('case_policy_id', '=', array_get($input, 'id'))->first();

            if(is_null($case)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }
        
        $rules = [
            'info' => 'required',
            'code' => 'required',
            'name' => 'required',
            'policy_id' => 'required|min:2|max:100',
            'policy_no' => 'required|min:2|max:100',
            'policy_type' => 'required|min:2|max:1000',
            'comp_code' => 'required|min:2|max:1000',
            'policy_term' => 'required|min:2|max:1000',
            'contract_curr' => 'required|min:2|max:1000',
            'sum_insured' => 'required|min:2|max:1000',
            'incept_date' => 'required|min:2|max:1000',
            'install_form_date' => 'required|min:2|max:1000',
            'billing_freq' => 'required|min:2|max:1000',
            'policy_exp_date' => 'required|min:2|max:1000',
            'gross_prem_paid' => 'required|min:2|max:1000',
            'net_prem_paid' => 'required|min:2|max:1000',
            'gross_prem_inc_gst' => 'required|min:2|max:1000',
            'net_prem_inc_gst' => 'required|min:2|max:1000',
            'prem_wo_com' => 'required|min:2|max:1000',
            'prem_conv_rate' => 'required|min:2|max:1000',
            'payment_curr' => 'required|min:2|max:1000',
            'com_or' => 'required|min:2|max:1000',
            'com_run_date' => 'required|min:2|max:1000',
            'com_conv_rate' => 'required|min:2|max:1000',
            'trans_code' => 'required|min:2|max:1000',
            'prem_term' => 'required|min:2|max:1000',
            'date_issue' => 'required|min:2|max:1000',
        ];

        // field name overrides
        $names = [
            'info' => 'case info',
            'code' => 'case code',
            'name' => 'case name',
            'policy_id' => 'policy id',
            'policy_no' => 'policy number',
            'policy_type' => 'policy type',
            'comp_code' => 'component code',
            'policy_term' => 'policy term',
            'contract_curr' => 'contract currency',
            'sum_insured' => 'sum insured',
            'incept_date' => 'incept date',
            'install_form_date' => 'installment form date',
            'billing_freq' => 'billing frequency',
            'policy_exp_date' => 'policy expiry date',
            'gross_prem_paid' => 'gross premium paid',
            'net_prem_paid' => 'net premium paid',
            'gross_prem_inc_gst' => 'gross premium inc',
            'net_prem_inc_gst' => 'net premium inc',
            'prem_wo_com' => 'premium w/o com',
            'prem_conv_rate' => 'premium conv rate',
            'payment_curr' => 'payment currency',
            'com_or' => 'commission or',
            'com_run_date' => 'commission run date',
            'com_conv_rate' => 'commission conv rate',
            'trans_code' => 'transaction code',
            'prem_term' => 'premium term',
            'date_issue' => 'date of issue',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // create model if new
        if($new) {
            $row = new ProductionCases;
            $row->policy_id = array_get($input, 'policy_id');
            $row->policy_no = array_get($input, 'policy_no');
            $row->policy_type = array_get($input, 'policy_type');
            $row->comp_code = array_get($input, 'comp_code');
            $row->policy_term = array_get($input, 'policy_term');
            $row->contract_curr = array_get($input, 'contract_curr');
            $row->sum_insured = array_get($input, 'sum_insured');
            $row->incept_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'incept_date'). '00:00 AM');
            $row->install_form_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'install_form_date') . '00:00 AM');
            $row->billing_freq = array_get($input, 'billing_freq');
            $row->policy_exp_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'policy_exp_date'). '00:00 AM');
            $row->gross_prem_paid = array_get($input, 'gross_prem_paid');
            $row->net_prem_paid = array_get($input, 'net_prem_paid');
            $row->gross_prem_inc_gst = array_get($input, 'gross_prem_inc_gst');
            $row->net_prem_inc_gst = array_get($input, 'net_prem_inc_gst');
            $row->prem_wo_com = array_get($input, 'prem_wo_com');
            $row->prem_conv_rate = array_get($input, 'prem_conv_rate');
            $row->payment_curr = array_get($input, 'payment_curr');
            $row->com_or = array_get($input, 'com_or');
            $row->com_run_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'com_run_date'). '00:00 AM');
            $row->com_conv_rate = array_get($input, 'com_conv_rate');
            $row->trans_code = array_get($input, 'trans_code');
            $row->prem_term = array_get($input, 'prem_term');
            $row->date_issue =  Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'date_issue'). '00:00 AM');
        } else {
            $log_policy_id          = $row->policy_id;
            $log_policy_no          = $row->policy_no;
            $log_policy_type        = $row->policy_type;
            $log_comp_code          = $row->comp_code;
            $log_policy_term        = $row->policy_term;
            $log_contract_curr      = $row->contract_curr;
            $log_sum_insured        = $row->sum_insured;
            $log_incept_date        = $row->incept_date;
            $log_install_form_date  = $row->install_form_date;
            $log_billing_freq       = $row->billing_freq;
            $log_policy_exp_date    = $row->policy_exp_date;
            $log_gross_prem_paid    = $row->gross_prem_paid;
            $log_net_prem_paid      = $row->net_prem_paid;
            $log_gross_prem_inc_gst = $row->gross_prem_inc_gst;
            $log_net_prem_inc_gst   = $row->net_prem_inc_gst;
            $log_prem_wo_com        = $row->prem_wo_com;
            $log_prem_conv_rate     = $row->prem_conv_rate;
            $log_payment_curr       = $row->payment_curr;
            $log_com_or             = $row->com_or;
            $log_com_run_date       = $row->com_run_date;
            $log_com_conv_rate      = $row->com_conv_rate;
            $log_trans_code         = $row->trans_code;
            $log_prem_term          = $row->prem_term;
            $log_date_issuee        = $row->date_issue;

            $row->policy_id = array_get($input, 'policy_id');
            $row->policy_no = array_get($input, 'policy_no');
            $row->policy_type = array_get($input, 'policy_type');
            $row->comp_code = array_get($input, 'comp_code');
            $row->policy_term = array_get($input, 'policy_term');
            $row->contract_curr = array_get($input, 'contract_curr');
            $row->sum_insured = array_get($input, 'sum_insured');
            $row->incept_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'incept_date'). '00:00 AM');
            $row->install_form_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'install_form_date') . '00:00 AM');
            $row->billing_freq = array_get($input, 'billing_freq');
            $row->policy_exp_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'policy_exp_date'). '00:00 AM');
            $row->gross_prem_paid = array_get($input, 'gross_prem_paid');
            $row->net_prem_paid = array_get($input, 'net_prem_paid');
            $row->gross_prem_inc_gst = array_get($input, 'gross_prem_inc_gst');
            $row->net_prem_inc_gst = array_get($input, 'net_prem_inc_gst');
            $row->prem_wo_com = array_get($input, 'prem_wo_com');
            $row->prem_conv_rate = array_get($input, 'prem_conv_rate');
            $row->payment_curr = array_get($input, 'payment_curr');
            $row->com_or = array_get($input, 'com_or');
            $row->com_run_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'com_run_date'). '00:00 AM');
            $row->com_conv_rate = array_get($input, 'com_conv_rate');
            $row->trans_code = array_get($input, 'trans_code');
            $row->prem_term = array_get($input, 'prem_term');
            $row->date_issue =  Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'date_issue'). '00:00 AM');
        }

        // save model
        $row->save();

        if ($new) {

            $case = new ProductionIncept;
            $case->info = array_get($input, 'info');
            $case->code = array_get($input, 'code');
            $case->name = array_get($input, 'name');
            $case->case_policy_id = $row->id;
            $case->visor = 'advisor';
            $case->case = 'inception';
            $case->edit_id = Auth::user()->id;
            $case->save();

            $case->case_id = 'CASE-' . sprintf("%04d", $case->id);
            $case->save();
        } else {
            $case->info = array_get($input, 'info');
            $case->code = array_get($input, 'code');
            $case->name = array_get($input, 'name');
            $case->edit_id = Auth::user()->id;
            $case->save();
        }

         // return
        return Response::json(['body' => 'Case created successfully.']);
    }

    public function caseInceptionSupervisorView() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = ProductionIncept::with('getCasePolicy')->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Save model
     *
     */
    public function caseInceptionSupervisorSave() {
    
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {


            // get the user info
            $row = ProductionCases::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // get the user info
            $case = ProductionIncept::where('case_policy_id', '=', array_get($input, 'id'))->first();

            if(is_null($case)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }
        
        $rules = [
            'info' => 'required',
            'code' => 'required',
            'name' => 'required',
            'policy_id' => 'required|min:2|max:100',
            'policy_no' => 'required|min:2|max:100',
            'policy_type' => 'required|min:2|max:1000',
            'comp_code' => 'required|min:2|max:1000',
            'policy_term' => 'required|min:2|max:1000',
            'contract_curr' => 'required|min:2|max:1000',
            'sum_insured' => 'required|min:2|max:1000',
            'incept_date' => 'required|min:2|max:1000',
            'install_form_date' => 'required|min:2|max:1000',
            'billing_freq' => 'required|min:2|max:1000',
            'policy_exp_date' => 'required|min:2|max:1000',
            'gross_prem_paid' => 'required|min:2|max:1000',
            'net_prem_paid' => 'required|min:2|max:1000',
            'gross_prem_inc_gst' => 'required|min:2|max:1000',
            'net_prem_inc_gst' => 'required|min:2|max:1000',
            'prem_wo_com' => 'required|min:2|max:1000',
            'prem_conv_rate' => 'required|min:2|max:1000',
            'payment_curr' => 'required|min:2|max:1000',
            'com_or' => 'required|min:2|max:1000',
            'com_run_date' => 'required|min:2|max:1000',
            'com_conv_rate' => 'required|min:2|max:1000',
            'trans_code' => 'required|min:2|max:1000',
            'prem_term' => 'required|min:2|max:1000',
            'date_issue' => 'required|min:2|max:1000',
        ];

        // field name overrides
        $names = [
            'info' => 'case info',
            'code' => 'case code',
            'name' => 'case name',
            'policy_id' => 'policy id',
            'policy_no' => 'policy number',
            'policy_type' => 'policy type',
            'comp_code' => 'component code',
            'policy_term' => 'policy term',
            'contract_curr' => 'contract currency',
            'sum_insured' => 'sum insured',
            'incept_date' => 'incept date',
            'install_form_date' => 'installment form date',
            'billing_freq' => 'billing frequency',
            'policy_exp_date' => 'policy expiry date',
            'gross_prem_paid' => 'gross premium paid',
            'net_prem_paid' => 'net premium paid',
            'gross_prem_inc_gst' => 'gross premium inc',
            'net_prem_inc_gst' => 'net premium inc',
            'prem_wo_com' => 'premium w/o com',
            'prem_conv_rate' => 'premium conv rate',
            'payment_curr' => 'payment currency',
            'com_or' => 'commission or',
            'com_run_date' => 'commission run date',
            'com_conv_rate' => 'commission conv rate',
            'trans_code' => 'transaction code',
            'prem_term' => 'premium term',
            'date_issue' => 'date of issue',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // create model if new
        if($new) {
            $row = new ProductionCases;
            $row->policy_id = array_get($input, 'policy_id');
            $row->policy_no = array_get($input, 'policy_no');
            $row->policy_type = array_get($input, 'policy_type');
            $row->comp_code = array_get($input, 'comp_code');
            $row->policy_term = array_get($input, 'policy_term');
            $row->contract_curr = array_get($input, 'contract_curr');
            $row->sum_insured = array_get($input, 'sum_insured');
            $row->incept_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'incept_date'). '00:00 AM');
            $row->install_form_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'install_form_date') . '00:00 AM');
            $row->billing_freq = array_get($input, 'billing_freq');
            $row->policy_exp_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'policy_exp_date'). '00:00 AM');
            $row->gross_prem_paid = array_get($input, 'gross_prem_paid');
            $row->net_prem_paid = array_get($input, 'net_prem_paid');
            $row->gross_prem_inc_gst = array_get($input, 'gross_prem_inc_gst');
            $row->net_prem_inc_gst = array_get($input, 'net_prem_inc_gst');
            $row->prem_wo_com = array_get($input, 'prem_wo_com');
            $row->prem_conv_rate = array_get($input, 'prem_conv_rate');
            $row->payment_curr = array_get($input, 'payment_curr');
            $row->com_or = array_get($input, 'com_or');
            $row->com_run_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'com_run_date'). '00:00 AM');
            $row->com_conv_rate = array_get($input, 'com_conv_rate');
            $row->trans_code = array_get($input, 'trans_code');
            $row->prem_term = array_get($input, 'prem_term');
            $row->date_issue =  Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'date_issue'). '00:00 AM');
        } else {
            $log_policy_id          = $row->policy_id;
            $log_policy_no          = $row->policy_no;
            $log_policy_type        = $row->policy_type;
            $log_comp_code          = $row->comp_code;
            $log_policy_term        = $row->policy_term;
            $log_contract_curr      = $row->contract_curr;
            $log_sum_insured        = $row->sum_insured;
            $log_incept_date        = $row->incept_date;
            $log_install_form_date  = $row->install_form_date;
            $log_billing_freq       = $row->billing_freq;
            $log_policy_exp_date    = $row->policy_exp_date;
            $log_gross_prem_paid    = $row->gross_prem_paid;
            $log_net_prem_paid      = $row->net_prem_paid;
            $log_gross_prem_inc_gst = $row->gross_prem_inc_gst;
            $log_net_prem_inc_gst   = $row->net_prem_inc_gst;
            $log_prem_wo_com        = $row->prem_wo_com;
            $log_prem_conv_rate     = $row->prem_conv_rate;
            $log_payment_curr       = $row->payment_curr;
            $log_com_or             = $row->com_or;
            $log_com_run_date       = $row->com_run_date;
            $log_com_conv_rate      = $row->com_conv_rate;
            $log_trans_code         = $row->trans_code;
            $log_prem_term          = $row->prem_term;
            $log_date_issuee        = $row->date_issue;

            $row->policy_id = array_get($input, 'policy_id');
            $row->policy_no = array_get($input, 'policy_no');
            $row->policy_type = array_get($input, 'policy_type');
            $row->comp_code = array_get($input, 'comp_code');
            $row->policy_term = array_get($input, 'policy_term');
            $row->contract_curr = array_get($input, 'contract_curr');
            $row->sum_insured = array_get($input, 'sum_insured');
            $row->incept_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'incept_date'). '00:00 AM');
            $row->install_form_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'install_form_date') . '00:00 AM');
            $row->billing_freq = array_get($input, 'billing_freq');
            $row->policy_exp_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'policy_exp_date'). '00:00 AM');
            $row->gross_prem_paid = array_get($input, 'gross_prem_paid'); 
            $row->net_prem_paid = array_get($input, 'net_prem_paid');
            $row->gross_prem_inc_gst = array_get($input, 'gross_prem_inc_gst');
            $row->net_prem_inc_gst = array_get($input, 'net_prem_inc_gst');
            $row->prem_wo_com = array_get($input, 'prem_wo_com');
            $row->prem_conv_rate = array_get($input, 'prem_conv_rate');
            $row->payment_curr = array_get($input, 'payment_curr');
            $row->com_or = array_get($input, 'com_or');
            $row->com_run_date = Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'com_run_date'). '00:00 AM');
            $row->com_conv_rate = array_get($input, 'com_conv_rate');
            $row->trans_code = array_get($input, 'trans_code');
            $row->prem_term = array_get($input, 'prem_term');
            $row->date_issue =  Carbon::createFromFormat('d/m/Y h:i A', array_get($input, 'date_issue'). '00:00 AM');
        }

        // save model
        $row->save();

        if ($new) {

            $case = new ProductionIncept;
            $case->info = array_get($input, 'info');
            $case->code = array_get($input, 'code');
            $case->name = array_get($input, 'name');
            $case->case_policy_id = $row->id;
            $case->visor = 'supervisor';
            $case->case = 'inception';
            $case->edit_id = Auth::user()->id;
            $case->save();

            $case->case_id = 'CASE-' . sprintf("%04d", $case->id);
            $case->save();
        } else {
            $case->info = array_get($input, 'info');
            $case->code = array_get($input, 'code');
            $case->name = array_get($input, 'name');
            $case->edit_id = Auth::user()->id;
            $case->save();
        }

         // return
        return Response::json(['body' => 'Case created successfully.']);
    }


    public function introducerView() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Introducer::find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Save model
     *
     */
    public function introducerSave() {

      $new = true;

      $input = Input::all();

      // check if an ID is passed
      if(array_get($input, 'id')) {

        // get the user info
        $row = Introducer::find(array_get($input, 'id'));

        if(is_null($row)) {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
        // this is an existing row
        $new = false;
      }

      $rules = [
        'user_sales_id' => 'required',
        'code' => 'required|min:2|max:100',
        'name' => 'required|min:2|max:100',
        'email' => 'required|email',
        'zipcode' => 'required|min:2|max:1000',
      ];

      if ($new && (array_get($input, 'provider_id2') || array_get($input, 'policy_id2') || array_get($input, 'remarks2') || array_get($input, 'amount2' ))) {
        $rules['provider_id2'] = 'required';
        $rules['policy_id2'] = 'required';
        // $rules['remarks2'] = 'required|min:2|max:1000';
        $rules['amount2'] = 'required|numeric';
      }

      // field name overrides
      $names = [
        // 'user_sales_id' => 'agent',
        // 'name' => 'introducer name',
        // 'code' => 'user code',
        // 'email' => 'email',
        // 'zipcode' => 'zipcode',
        // 'provider_id2' => 'provider',
        // 'policy_id2' => 'policy',
        // 'remarks2' => 'remarks',
        // 'amount2' => 'percentage',
      ];

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names);

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      // create model if new
      if($new) {
        $row = new Introducer;
        $row->user_id      = array_get($input, 'user_sales_id');
        $row->code      = array_get($input, 'code');
        $row->name      = array_get($input, 'name');
        $row->email    = array_get($input, 'email');
        $row->zipcode    = array_get($input, 'zipcode');
        // save model
      } else {
        $row->code      = array_get($input, 'code');
        $row->name      = array_get($input, 'name');
        $row->email    = array_get($input, 'email');
        $row->zipcode    = array_get($input, 'zipcode');
      }

      $row->save();

      // create model if new
      if ($new && (array_get($input, 'provider_id2') || array_get($input, 'policy_id2') || array_get($input, 'remarks2') || array_get($input, 'amount2' ))) {
        $row2 = new AssignedPolicy;
        $row2->provider_id      = array_get($input, 'provider_id2');
        $row2->user_id      = array_get($input, 'user_sales_id');
        $row2->policy_no      = array_get($input, 'policy_id2');
        $row2->introducer_id      = $row->id;
        $row2->remarks    = array_get($input, 'remarks2');
        $row2->amount    = array_get($input, 'amount2');
        $row2->save();
      }

      // return
      return Response::json(['body' => 'Payroll created successfully.']);
    }

    /**
     * Save model
     *
     */
    public function introducerAssign() {

        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = AssignedPolicy::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
          'provider_id' => 'required',
          'policy_id' => 'required',
          'remarks' => 'required|min:2|max:1000',
          'amount' => 'required|numeric',
        ];

        // field name overrides
        $names = [
          'provider_id' => 'provider',
          'policy_id' => 'policy',
          'remarks' => 'remarks',
          'amount' => 'amount',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // create model if new
        if($new) {
          $row = new AssignedPolicy;
        }

        $get_user_id = Introducer::find(array_get($input, 'introducer_id'));

        $row->user_id      = $get_user_id->user_id;
        $row->provider_id      = array_get($input, 'provider_id');
        $row->policy_no      = array_get($input, 'policy_id');
        $row->introducer_id      = array_get($input, 'introducer_id');
        $row->remarks    = array_get($input, 'remarks');
        $row->amount    = array_get($input, 'amount');

        // save model
        $row->save();
        
        // return
        return Response::json(['body' => 'Assigning policy successfully.']);
    }

    /**
     * Get Product Rate
     *
     */
    public function introducerAssignView() {
        
      $get_assigned = AssignedPolicy::with('getAssignedPolicyCode')
                                    // ->with('getAssignedUser')
                                    ->with('getAssignedProvider')
                                    ->where('introducer_id', Request::input('id'))->get();

      $rows = '<table class="table table-striped">' .
                  '<thead class="tbheader">' .
                    '<tr>' .
                      '<th class="tbheader"><i class="fa fa-sort"></i> POLICY CODE</th>' .
                      '<th class="tbheader"><i class="fa fa-sort"></i> PROVIDER </th>' .
                      // '<th class="tbheader"><i class="fa fa-sort"></i> AGENT NAME </th>' .
                      '<th class="tbheader"><i class="fa fa-sort"></i> PERCENTAGE </th>' .
                      '<th class="tbheader rightalign"><i class="fa fa-sort"></i> TOOLS </th>' .
                    '</tr>' .
                '</thead>' .
                '<tbody>';

      foreach ($get_assigned as $key => $field) {
        $rows .= '<tr data-id="' . $field->id . '">' . 
                      '<td>' . $field->policy_no . '</td>' .
                  '<td>' . $field->getAssignedProvider->name . '</td>' .
                  // '<td>' . $field->getAssignedUser->name . '</td>' .
                  '<td>' . $field->amount . '%</td>' .
                  '<td class="rightalign">' .
                    '&nbsp;<button type="button" class="btn btn-xs btn-table btn-delete" data-toggle="modal" data-target="#myModaldelete"><i class="fa fa-trash"></i></button>' .
                  '</td>' .
                '</tr>';
      }

      $rows .= '</tbody>' .
                    '</table>' .
                    '<hr>' .
                    '<button type="button" class="btn btn-sm btn-warning borderzero btn-tool pull-right btn-assign" data-id="' . Request::input('id') . '"><i class="fa fa-plus"></i> Assign</button>';

      return Response::json($rows);
    }

    /**
     * Deactivate the user
     *
     */
    public function disableIntroducer() {
        $row = Introducer::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->status = 0;
                $row->save();

            // return
            return Response::json(['body' => 'Introducer has been deactivated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the user
     *
     */
    public function enableIntroducer() {
        $row = Introducer::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->status = 1;
                $row->save();

            // return
            return Response::json(['body' => 'Introducer has been activated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Disable multiple user
     *
     */
    public function introducerdisableSelected() {
        $ids = Request::input('ids');
       
        if (count($ids) == 0) {
            return Response::json(['error' => 'Select introducer first.']); 
        } else {
           

            // process
            foreach ($ids as $id) {
                $row = Introducer::find($id);
                if(!is_null($row)) {
                    
                    $status = $row->status;
                    // disable only the active users.
                    if ($status == 1){
                      $row->status = 0;
                      $row->save();
                     
                    }
                }
            }
            return Response::json(['body' => 'Selected introducer has been Deactivated.']);
        }
    }  

    /**
     * Remove multiple user
     *
     */
    public function introducerremoveSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select introducer first.']); 
        } else {

            // process
            foreach ($ids as $id) {
                $row = Introducer::find($id);
                if(!is_null($row)) {
                    $status = $row->status;
                    // you cannot remove user that already been remove
                    if ($status != 2){
                      $row->status = 2;
                      $row->save();
                   
                    }
                    
                }
            }

            return Response::json(['body' => 'Selected introducer has been Removed.']);
        }
    }

    /**
     * Deactivate the user
     *
     */
    public function disableAssign() {
        $row = AssignedPolicy::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->status = 0;
                $row->save();

            // return
            return Response::json(['body' => 'AssignedPolicy has been deactivated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the user
     *
     */
    public function duplicate() {

      if (Request::input('id')) {
        $row = ProductionCases::find(Request::input('id'));

        if (!$row) {
            // not found
            return Response::json(['errorlog' => "The requested item was not found in the database."]);
        }

        $new_row = new ProductionCases;
        $new_row->user_id = $row->user_id;
        $new_row->currency_id = $row->currency_id;
        $new_row->firstname = $row->firstname;
        $new_row->lastname = $row->lastname;
        $new_row->annual_income_range = $row->annual_income_range;
        $new_row->life_firstname = $row->life_firstname;
        $new_row->life_lastname = $row->life_lastname;
        $new_row->owner_id = $row->owner_id;
        $new_row->life_id = $row->life_id;
        $new_row->owner_occupation = $row->owner_occupation;
        $new_row->life_occupation = $row->life_occupation;
        $new_row->owner_gender = $row->owner_gender;
        $new_row->life_gender = $row->life_gender;
        $new_row->owner_dob = $row->owner_dob;
        $new_row->life_dob = $row->life_dob;
        $new_row->owner_age = $row->owner_age;
        $new_row->life_age = $row->life_age;
        $new_row->backdate = $row->backdate;
        $new_row->address = $row->address;
        $new_row->address = $row->address;
        $new_row->email = $row->email;
        $new_row->nationality_id = $row->nationality_id;
        $new_row->residency_status = $row->residency_status;
        $new_row->residency_others = $row->residency_others;
        $new_row->provider_list = $row->provider_list;
        $new_row->main_product_id = $row->main_product_id;
        $new_row->conversion_rate = $row->conversion_rate;
        $new_row->conversion_year = $row->conversion_year;
        $new_row->main_sum_assured = 0;
        $new_row->provider_list_rider = $row->provider_list_rider;
        $new_row->rider_product_id = $row->rider_product_id;
        $new_row->rider_sum_assured = $row->rider_sum_assured;
        $new_row->currency = $row->currency;
        $new_row->currency2 = $row->currency2;
        $new_row->currency_value = 0;
        $new_row->currency_value2 = 0;
        $new_row->annual_currency_value2 = 0;
        $new_row->conv_total = $row->conv_total;
        $new_row->currency_sd_rate = $row->currency_sd_rate;
        $new_row->currency_usd_rate = $row->currency_usd_rate;
        $new_row->life_total_amount = $row->life_total_amount;
        $new_row->policy_term = $row->policy_term;
        $new_row->payment_frequency = $row->payment_frequency;
        $new_row->mode_of_payment = $row->mode_of_payment;
        $new_row->mode_others = $row->mode_others;
        $new_row->ape = 0;
        $new_row->source = $row->source;
        $new_row->case = $row->case;
        $new_row->upload_title = $row->upload_title;
        $new_row->upload_2_title = $row->upload_2_title;
        $new_row->upload_3_title = $row->upload_3_title;
        $new_row->upload_4_title = $row->upload_4_title;
        $new_row->upload_5_title = $row->upload_5_title;
        $new_row->save();

        if ($row->upload) {
          $file = explode('.', $row->upload);
          $file_extenstion = end($file);
          copy(storage_path('case') . '\\' . $row->upload, storage_path('case') . '\\' . $new_row->id . '-one.' . $file_extenstion);
          $new_row->upload = $new_row->id . '-one.' . $file_extenstion;
        }

        if ($row->upload_2) {
          $file = explode('.', $row->upload_2);
          $file_extenstion = end($file);
          copy(storage_path('case') . '\\' . $row->upload_2, storage_path('case') . '\\' . $new_row->id . '-two.' . $file_extenstion);
          $new_row->upload_2 = $new_row->id . '-two.' . $file_extenstion;
        }
        
        if ($row->upload_3) {
          $file = explode('.', $row->upload_3);
          $file_extenstion = end($file);
          copy(storage_path('case') . '\\' . $row->upload_3, storage_path('case') . '\\' . $new_row->id . '-three.' . $file_extenstion);
          $new_row->upload_3 = $new_row->id . '-three.' . $file_extenstion;
        }

        if ($row->upload_4) {
          $file = explode('.', $row->upload_4);
          $file_extenstion = end($file);
          copy(storage_path('case') . '\\' . $row->upload_4, storage_path('case') . '\\' . $new_row->id . '-four.' . $file_extenstion);
          $new_row->upload_4 = $new_row->id . '-four.' . $file_extenstion;
        }

        if ($row->upload_5) {
          $file = explode('.', $row->upload_5);
          $file_extenstion = end($file);
          copy(storage_path('case') . '\\' . $row->upload_5, storage_path('case') . '\\' . $new_row->id . '-five.' . $file_extenstion);
          $new_row->upload_5 = $new_row->id . '-five.' . $file_extenstion;
        }

        $new_row->save();

        // return
        return Response::json(['body' => 'Case has been duplicated.']);

      } else {
        // not found
        return Response::json(['errorlog' => "The requested item was not found in the database."]);
      }

    }

    /**
     * Activate the user
     *
     */
    public function enableAssign() {
        $row = AssignedPolicy::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            //if users tries to disable itself
           
                $row->status = 1;
                $row->save();

            // return
            return Response::json(['body' => 'AssignedPolicy has been activated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

  /**
   * Delete File
   *
   * @return Response
   */
  public function delete() {
    $id = Request::input('id');

    AssignedPolicy::destroy($id);
    return Response::json(['type' => 'success', 'body' => 'File has been deleted.']);
  }

  public function notification($id){  
    $row = NotificationPerUser::where(function($query) use ($id) {
                                        $query->where('user_id', '=', Auth::user()->id)
                                               ->where('notification_id', '=', $id);
                                      })->update(['is_read' => 1]);
    
    return Redirect::to('/policy/production/case-submission');
  }

  public function notiOrphan($id) {  
    $row = NotificationPerUser::where(function($query) use ($id) {
                                        $query->where('user_id', '=', Auth::user()->id)
                                              ->where('notification_id', '=', $id);
                                      })->update(['is_read' => 1]);

    return Redirect::to('/policy/orphan-policies');
  }
  
  
  public function ARRAYTEST(){
    $array = array(
  array("life_lastname"=>"GWEE","life_firstname"=>"XIAOHUI GRACE","firstname"=>"XIAOHUI GRACE","lastname"=>"GWEE","owner_occupation"=>"NURSE","owner_dob"=>"2/22/1990","contact"=>86118550,"postal_code"=>"600339","address"=>"339 JURONG EAST AVENUE 1 #12-1538","email"=>"GRACEGWEEXIAOHUI@GMAIL.COM","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>493.2,"agent_code"=>"LN06-1152","submitted_date"=>"11/1/2016","owner_id"=>"S9070500H","owner_occupation"=>"NURSE","owner_dob"=>"2/22/1990","source"=>"Warm","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"LIU ZHIRUI","policy_term"=>"99","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"GWEE","life_firstname"=>"XIAOQI JEANNE","firstname"=>"XIAOQI JEANNE","lastname"=>"GWEE","owner_occupation"=>"AUDIT ASSOCIATE","owner_dob"=>"10/6/1991","contact"=>84889050,"postal_code"=>"600339","address"=>"339 JURONG EAST AVENUE 1 ##12-1538","email"=>"JEANNEGWEE9@HOTMAIL.COM","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>361.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF OA","annual_currency_value2"=>361.83,"agent_code"=>"LN06-1152","submitted_date"=>"11/1/2016","owner_id"=>"S9170204E","owner_occupation"=>"AUDIT ASSOCIATE","owner_dob"=>"10/6/1991","source"=>"Warm","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"LIU ZHIRUI","policy_term"=>"99","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"GWEE","life_firstname"=>"XIAOQI JEANNE","firstname"=>"XIAOQI JEANNE","lastname"=>"GWEE","owner_occupation"=>"AUDIT ASSOCIATE","owner_dob"=>"10/6/1991","contact"=>84889050,"postal_code"=>"600339","address"=>"339 JURONG EAST AVENUE 1 #12-1538","email"=>"JEANNEGWEE9@HOTMAIL.COM","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>493.2,"agent_code"=>"LN06-1152","submitted_date"=>"11/1/2016","owner_id"=>"S9170204E","owner_occupation"=>"AUDIT ASSOCIATE","owner_dob"=>"10/6/1991","source"=>"Warm","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"LIU ZHIRUI","policy_term"=>"99","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"CHEN","life_firstname"=>"YU XUAN ","firstname"=>"YU XUAN ","lastname"=>"CHEN","owner_occupation"=>"STUDENT","owner_dob"=>"5/1/1992","contact"=>98556698,"postal_code"=>"538582","address"=>"29B HOW SUN DRIVE","email"=>"ARCHMAGE012@GMAIL.COM","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>112,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>1344,"agent_code"=>"JC13-1050","submitted_date"=>"11/1/2016","owner_id"=>"S9215047Z","owner_occupation"=>"STUDENT","owner_dob"=>"5/1/1992","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"AVIS ONG MEI MAN","policy_term"=>"40","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"CHEN","life_firstname"=>"MING XUAN ","firstname"=>"MING XUAN ","lastname"=>"CHEN","owner_occupation"=>"BUSINESS ADMINISTRATOR","owner_dob"=>"4/9/1990","contact"=>90036625,"postal_code"=>"538582","address"=>"29B HOW SUN DRIVE","email"=>"OWENCHEN.MX@GMAIL.COM","main_product_id"=>88,"main_sum_assured"=>"3000","currency_value"=>51.5,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>618,"agent_code"=>"JC13-1050","submitted_date"=>"11/1/2016","owner_id"=>"S9011220A","owner_occupation"=>"BUSINESS ADMINISTRATOR","owner_dob"=>"4/9/1990","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"AVIS ONG MEI MAN","policy_term"=>"38","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"TAN","life_firstname"=>"YIH SHEE SAM","firstname"=>"YIH SHEE SAM","lastname"=>"TAN","owner_occupation"=>"REGIONAL MANAGER","owner_dob"=>"10/11/1975","contact"=>94502059,"postal_code"=>"572273","address"=>"273B BISHAN STREET 24 #25-108","email"=>"TANSAM2011@GMAIL.COM","main_product_id"=>92,"main_sum_assured"=>"1400","currency_value"=>1617,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>1617,"agent_code"=>"JC13-1050","submitted_date"=>"11/1/2016","owner_id"=>"S7530378E","owner_occupation"=>"REGIONAL MANAGER","owner_dob"=>"10/11/1975","source"=>"Existing Client","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"AVIS ONG MEI MAN","policy_term"=>"23","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Lim","life_firstname"=>"Kar Ho","firstname"=>"Kar Ho","lastname"=>"Lim","owner_occupation"=>"TECHNICIAN","owner_dob"=>"3/29/1971","contact"=>82055978,"postal_code"=>"460028","address"=>"BLK 28 NEW UPPER CHANGI ROAD #10-726","email"=>"","main_product_id"=>6,"main_sum_assured"=>"$700 MONTHLY BENEFIT","currency_value"=>524.5,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>524.5,"agent_code"=>"KT09-1103","submitted_date"=>"11/1/2016","owner_id"=>"S718205Z","owner_occupation"=>"TECHNICIAN","owner_dob"=>"3/29/1971","source"=>"Road Show","annual_income_range"=>"Below $30,000","fullname_first"=>"YUN HAN FUNG","policy_term"=>"YEARLY RENEWABLE","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Soh","life_firstname"=>"Siew Hoon Coreen","firstname"=>"Siew Hoon Coreen","lastname"=>"Soh","owner_occupation"=>"Deputy General Manager","owner_dob"=>"7/26/1975","contact"=>97869867,"postal_code"=>"510482","address"=>"Blk 482 Pasir Ris Drive 4 #05-391","email"=>"bearandco@gmail.com","main_product_id"=>8,"main_sum_assured"=>"1000","currency_value"=>582.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>582.83,"agent_code"=>"PT10-1070","submitted_date"=>"11/1/2016","owner_id"=>"S7522240H","owner_occupation"=>"Deputy General Manager","owner_dob"=>"7/26/1975","source"=>"Existing Client","annual_income_range"=>"$100,001-$150,000","fullname_first"=>"CHEN MINGLI","policy_term"=>"Life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"KOH","life_firstname"=>"WILLIAM KOH KARTONO","firstname"=>"WILLIAM KOH KARTONO","lastname"=>"KOH","owner_occupation"=>"RETIREE","owner_dob"=>"12/25/1955","contact"=>96306658,"postal_code"=>"650361","address"=>"BLK 361 BUKIT BATOK STREET 31 #11-455","email"=>"william.kohkartono@gmail.com","main_product_id"=>6,"main_sum_assured"=>"$700 MONTHLY BENEFIT","currency_value"=>518.42,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>518.42,"agent_code"=>"KT09-1103","submitted_date"=>"11/1/2016","owner_id"=>"S1219049B","owner_occupation"=>"RETIREE","owner_dob"=>"12/25/1955","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"YUN HAN FUNG","policy_term"=>"YEARLY RENEWABLE","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"BIN KAMIS","life_firstname"=>"MASBAITULLA","firstname"=>"MASBAITULLA","lastname"=>"BIN KAMIS","owner_occupation"=>"TECHNICIAN","owner_dob"=>"6/9/1974","contact"=>96744180,"postal_code"=>"6550361","address"=>"BLK 361 BUKIT BATOK STREET 31 #12-451","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1500","currency_value"=>595.88,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>595.88,"agent_code"=>"KT09-1103","submitted_date"=>"11/1/2016","owner_id"=>"S7417945B","owner_occupation"=>"TECHNICIAN","owner_dob"=>"6/9/1974","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"YUN HAN FUNG","policy_term"=>"YEARLY RENEWABLE","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Ng","life_firstname"=>"Amri","firstname"=>"Amri","lastname"=>"Ng","owner_occupation"=>"Moneychanger","owner_dob"=>"10/13/1975","contact"=>93459857,"postal_code"=>"29439","address"=>"Bloke N012 Batu Batam Permai","email"=>"","main_product_id"=>83,"main_sum_assured"=>"30000","currency_value"=>246.2,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>2954.4,"agent_code"=>"SL06-1095","submitted_date"=>"11/1/2016","owner_id"=>"B3836905","owner_occupation"=>"Moneychanger","owner_dob"=>"10/13/1975","source"=>"","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"ANG RUWEI CANDICE","policy_term"=>"13","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Than","life_firstname"=>"Moe Moe ","firstname"=>"Moe Moe ","lastname"=>"Than","owner_occupation"=>"Doctor","owner_dob"=>"8/10/1968","contact"=>87142684,"postal_code"=>"603287","address"=>"287C Jurong East St 21 #21-324","email"=>"moemoethan@yahoo.com","main_product_id"=>21,"main_sum_assured"=>"0","currency_value"=>10732,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>10732,"agent_code"=>"AB01-1039","submitted_date"=>"11/1/2016","owner_id"=>"MA443907","owner_occupation"=>"Doctor","owner_dob"=>"8/10/1968","source"=>"","annual_income_range"=>"$100,001-$150,000","fullname_first"=>"ESTELLE CHOW SIEW LE","policy_term"=>"27","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"LAI FONG","life_firstname"=>"CHEE","firstname"=>"CHEE","lastname"=>"LAI FONG","owner_occupation"=>"PRIVATE BANKING ASSISTANT","owner_dob"=>"5/25/1955","contact"=>98502330,"postal_code"=>"542299","address"=>"APT BLK 299B COMPASSVALE STREET 08-106 SINGAPORE","email"=>"","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>697.69,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>697.69,"agent_code"=>"JH12-1132","submitted_date"=>"11/1/2016","owner_id"=>"S1837748I","owner_occupation"=>"PRIVATE BANKING ASSISTANT","owner_dob"=>"5/25/1955","source"=>"Door Knocking ","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"PAN KE HSING","policy_term"=>"LIFETIME","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Asokan","life_firstname"=>"Sabrina Thewi","firstname"=>"Sabrina Thewi","lastname"=>"Asokan","owner_occupation"=>"Senior Program Consultant","owner_dob"=>"11/1/1910","contact"=>87001130,"postal_code"=>"730511","address"=>"block 511 woodlands drive 14 #11-61","email"=>"sabrinathewi@gmail.com","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>1890,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>1890,"agent_code"=>"JH12-1085","submitted_date"=>"11/1/2016","owner_id"=>"S8433634C","owner_occupation"=>"Senior Program Consultant","owner_dob"=>"11/1/1910","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"KER JIAN MIN DANIEL","policy_term"=>"33","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Wong","life_firstname"=>"Choong Meng Eric","firstname"=>"Choong Meng Eric","lastname"=>"Wong","owner_occupation"=>"Executive","owner_dob"=>"12/1/1910","contact"=>85889086,"postal_code"=>"760155","address"=>"Blk 155 Yishun Street 11 #08-104","email"=>"ewongcm12@singnet.com.sg","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>467.31,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>467.31,"agent_code"=>"KL05-1023","submitted_date"=>"11/1/2016","owner_id"=>"S7248469Z","owner_occupation"=>"Executive","owner_dob"=>"12/1/1910","source"=>"Referrals","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"KEN  LIM CHEE YONG","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Wong","life_firstname"=>"Choong Meng Eric","firstname"=>"Choong Meng Eric","lastname"=>"Wong","owner_occupation"=>"Executive","owner_dob"=>"12/26/1972","contact"=>85889086,"postal_code"=>"760155","address"=>"Blk 155 Yishun Street 11 #08-104","email"=>"ewongcm12@singnet.com.sg","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>537.6,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>537.6,"agent_code"=>"KL05-1023","submitted_date"=>"11/1/2016","owner_id"=>"S7248469Z","owner_occupation"=>"Executive","owner_dob"=>"12/26/1972","source"=>"Referrals","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"KEN  LIM CHEE YONG","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Lee","life_firstname"=>"Hwee Kheng","firstname"=>"Hwee Kheng","lastname"=>"Lee","owner_occupation"=>"Education Officer","owner_dob"=>"11/28/1968","contact"=>91770089,"postal_code"=>"540102","address"=>"Blk 102 Rivervale Walk #08-50","email"=>"decomrewa@gmail.com","main_product_id"=>88,"main_sum_assured"=>"7500","currency_value"=>2902.58,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>2902.58,"agent_code"=>"PT10-1070","submitted_date"=>"11/1/2016","owner_id"=>"S6845696G","owner_occupation"=>"Education Officer","owner_dob"=>"11/28/1968","source"=>"Existing Client","annual_income_range"=>"$100,001-$150,000","fullname_first"=>"CHEN MINGLI","policy_term"=>"17","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Lee","life_firstname"=>"Hwee Kheng","firstname"=>"Hwee Kheng","lastname"=>"Lee","owner_occupation"=>"Education Officer","owner_dob"=>"11/28/1968","contact"=>91770089,"postal_code"=>"540102","address"=>"Blk 102 Rivervale Walk #08-50","email"=>"decomrewa@gmail.com","main_product_id"=>100,"main_sum_assured"=>"62500","currency_value"=>7429.05,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>7429.05,"agent_code"=>"PT10-1070","submitted_date"=>"11/1/2016","owner_id"=>"S6845696G","owner_occupation"=>"Education Officer","owner_dob"=>"11/28/1968","source"=>"Existing Client","annual_income_range"=>"$100,001-$150,000","fullname_first"=>"CHEN MINGLI","policy_term"=>"Life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Yeoh","life_firstname"=>"Sin Yee","firstname"=>"Sin Yee","lastname"=>"Yeoh","owner_occupation"=>"Manager","owner_dob"=>"11/2/1980","contact"=>96713116,"postal_code"=>"544269","address"=>"Blk 269D Compassvale Link #13-89 ","email"=>"","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>933.4,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>933.4,"agent_code"=>"NO07-1017","submitted_date"=>"11/1/2016","owner_id"=>"S8083652Z","owner_occupation"=>"Manager","owner_dob"=>"11/2/1980","source"=>"Referrals","annual_income_range"=>"$100,001-$150,000","fullname_first"=>"CHEYENNE  SOH AI LAY","policy_term"=>"25","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"ZAINAL ABIDIN","life_firstname"=>"SHAHRUL NIZAM BIN ","firstname"=>"SHAHRUL NIZAM BIN ","lastname"=>"ZAINAL ABIDIN","owner_occupation"=>"","owner_dob"=>"10/1/1976","contact"=>null,"postal_code"=>"680211","address"=>"211 CHOA CHU KANG CENTRAL #06-122","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"TA09-1047","submitted_date"=>"11/2/2016","owner_id"=>"S7629669C","owner_occupation"=>"","owner_dob"=>"10/1/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"LIFE","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"FONG HAR","life_firstname"=>"LEE","firstname"=>"LEE","lastname"=>"FONG HAR","owner_occupation"=>"","owner_dob"=>"8/4/1966","contact"=>null,"postal_code"=>"570285","address"=>"285 BISHAN STREET 22 #12-217","email"=>"","main_product_id"=>8,"main_sum_assured"=>"800","currency_value"=>585.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>585.83,"agent_code"=>"TA09-1047","submitted_date"=>"11/2/2016","owner_id"=>"S1835481J","owner_occupation"=>"","owner_dob"=>"8/4/1966","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"LIFE","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"TAY","life_firstname"=>"PIEH SENG TONY","firstname"=>"PIEH SENG TONY","lastname"=>"TAY","owner_occupation"=>"","owner_dob"=>"10/1/1976","contact"=>null,"postal_code"=>"763336","address"=>"336C YISHUN STREET 31 #15-35","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"TA09-1047","submitted_date"=>"11/2/2016","owner_id"=>"S7629593Z","owner_occupation"=>"","owner_dob"=>"10/1/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"LIFE","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Bin Mohd Sani","life_firstname"=>"Suhaime","firstname"=>"Suhaime","lastname"=>"Bin Mohd Sani","owner_occupation"=>"Delivery Driver","owner_dob"=>"1/8/1982","contact"=>82628443,"postal_code"=>"640737","address"=>"Blk 737 Jurong West St 75 #02-57","email"=>"suhaimesani@gmail.com","main_product_id"=>24,"main_sum_assured"=>"400000","currency_value"=>166.21,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.21,"agent_code"=>"PT10-1070","submitted_date"=>"11/2/2016","owner_id"=>"S8200333I","owner_occupation"=>"Delivery Driver","owner_dob"=>"1/8/1982","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"CHEN MINGLI","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Bte Suhaime","life_firstname"=>"Allysa Yusyaira","firstname"=>"Suhaime","lastname"=>"Bin Mohd Sani","owner_occupation"=>"Delivery Driver","owner_dob"=>"1/8/1982","contact"=>82628443,"postal_code"=>"640737","address"=>"Blk 737 Jurong West St 75 #02-57","email"=>"suhaimesani@gmail.com","main_product_id"=>24,"main_sum_assured"=>"400000","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"PT10-1070","submitted_date"=>"11/2/2016","owner_id"=>"T0706193A","owner_occupation"=>"Student","owner_dob"=>"3/9/2007","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"CHEN MINGLI","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Bin Suhaime","life_firstname"=>"Khairul Zamani","firstname"=>"Suhaime","lastname"=>"Bin Mohd Sani","owner_occupation"=>"Delivery Driver","owner_dob"=>"1/8/1982","contact"=>82628443,"postal_code"=>"640737","address"=>"Blk 737 Jurong West St 75 #02-57","email"=>"suhaimesani@gmail.com","main_product_id"=>24,"main_sum_assured"=>"400000","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"PT10-1070","submitted_date"=>"11/2/2016","owner_id"=>"T0201165J","owner_occupation"=>"Student","owner_dob"=>"1/18/2002","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"CHEN MINGLI","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Bte Abdullah","life_firstname"=>"Mariani","firstname"=>"Mariani","lastname"=>"Bte Abdullah","owner_occupation"=>"Team Leader","owner_dob"=>"3/31/1982","contact"=>93625874,"postal_code"=>"640737","address"=>"Blk 737 Jurong West St 75 #02-57","email"=>"rynyaimee@gmail.com","main_product_id"=>24,"main_sum_assured"=>"400000","currency_value"=>162.21,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>162.21,"agent_code"=>"PT10-1070","submitted_date"=>"11/2/2016","owner_id"=>"S8271391C","owner_occupation"=>"Team Leader","owner_dob"=>"3/31/1982","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"CHEN MINGLI","policy_term"=>"Life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Bin Maskor","life_firstname"=>"Bahtiar","firstname"=>"Bahtiar","lastname"=>"Bin Maskor","owner_occupation"=>"Warehouse Asstistant","owner_dob"=>"7/28/1967","contact"=>96669804,"postal_code"=>"650329","address"=>"Blk 329 Bukit Batok Street 33 #02-89","email"=>"afiqdinniy@gmail.com","main_product_id"=>6,"main_sum_assured"=>"1100","currency_value"=>561.54,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>561.54,"agent_code"=>"KT09-1074","submitted_date"=>"11/2/2016","owner_id"=>"S1826959G","owner_occupation"=>"Warehouse Asstistant","owner_dob"=>"7/28/1967","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"Kelvin Kong","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"KIM PING","life_firstname"=>"LAU","firstname"=>"LAU","lastname"=>"KIM PING","owner_occupation"=>"PARTNER","owner_dob"=>"3/8/1973","contact"=>96696388,"postal_code"=>"259711","address"=>"317 BUKIT TIMAH ROAD #05-317","email"=>"kimping.lau83@gmail.com","main_product_id"=>24,"main_sum_assured"=>"N/A","currency_value"=>902.31,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>902.31,"agent_code"=>"GF03-1063","submitted_date"=>"11/2/2016","owner_id"=>"S7372472D","owner_occupation"=>"PARTNER","owner_dob"=>"3/8/1973","source"=>"Referrals","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"SIM YEN LENG","policy_term"=>"LIFE","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"KIM PING","life_firstname"=>"LAU","firstname"=>"LAU","lastname"=>"KIM PING","owner_occupation"=>"PARTNER","owner_dob"=>"3/8/1973","contact"=>96696388,"postal_code"=>"259711","address"=>"317 BUKIT TIMAH ROAD #05-317","email"=>"kimping.lau83@gmail.com","main_product_id"=>99,"main_sum_assured"=>"N/A","currency_value"=>537.6,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>537.6,"agent_code"=>"GF03-1063","submitted_date"=>"11/2/2016","owner_id"=>"S7372472D","owner_occupation"=>"PARTNER","owner_dob"=>"3/8/1973","source"=>"Referrals","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"SIM YEN LENG","policy_term"=>"LIFE","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"ramalingam","life_firstname"=>"Mohan","firstname"=>"Mohan","lastname"=>"ramalingam","owner_occupation"=>"Sales Team Leader","owner_dob"=>"1/16/1971","contact"=>86864636,"postal_code"=>"560521","address"=>"Blk 521 Ang Mo Kio Avenue 5 #20-4214","email"=>"r.mohanpillay@gmail.com","main_product_id"=>6,"main_sum_assured"=>"1300","currency_value"=>574.59,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>574.59,"agent_code"=>"KT09-1135","submitted_date"=>"11/2/2016","owner_id"=>"S7101241G","owner_occupation"=>"Sales Team Leader","owner_dob"=>"1/16/1971","source"=>"Door Knocking ","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Bin Ahmad Sinin","life_firstname"=>"Mohd Ismail","firstname"=>"Mohd Ismail","lastname"=>"Bin Ahmad Sinin","owner_occupation"=>"engineering manager","owner_dob"=>"2/28/1973","contact"=>90128278,"postal_code"=>"507535","address"=>"66 loyang rise","email"=>"mdismail_as@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.07,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.07,"agent_code"=>"KT09-1135","submitted_date"=>"11/2/2016","owner_id"=>"s7307085f","owner_occupation"=>"engineering manager","owner_dob"=>"2/28/1973","source"=>"Referrals","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"WASUTANACHAI","life_firstname"=>"NAWINYA","firstname"=>"NAWINYA","lastname"=>"WASUTANACHAI","owner_occupation"=>"a","owner_dob"=>"4/1/1910","contact"=>null,"postal_code"=>"1","address"=>"t","email"=>"","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>375.56,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>4506.72,"agent_code"=>"LY12-1084","submitted_date"=>"11/2/2016","owner_id"=>"a","owner_occupation"=>"a","owner_dob"=>"4/1/1910","source"=>"Existing Client","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LIM YISMIN","policy_term"=>"67","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Quek","life_firstname"=>"Yan Ting","firstname"=>"Yan Ting","lastname"=>"Quek","owner_occupation"=>"Nurse","owner_dob"=>"1/11/1989","contact"=>96611189,"postal_code"=>"540245","address"=>"Blk 245 Compassvale Road #09-658","email"=>"yanting89@hotmail.com","main_product_id"=>102,"main_sum_assured"=>"20000","currency_value"=>1757.3,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>1757.3,"agent_code"=>"MT06-1093","submitted_date"=>"11/2/2016","owner_id"=>"S8903622D","owner_occupation"=>"Nurse","owner_dob"=>"1/11/1989","source"=>"","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHEAH YAO DE","policy_term"=>"25","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Quek","life_firstname"=>"Yan Ting","firstname"=>"Yan Ting","lastname"=>"Quek","owner_occupation"=>"Nurse","owner_dob"=>"1/11/1989","contact"=>96611189,"postal_code"=>"540245","address"=>"Blk 245 Compassvale Road #09-658","email"=>"yanting89@hotmail.com","main_product_id"=>103,"main_sum_assured"=>"33000","currency_value"=>1229.15,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>1229.15,"agent_code"=>"MT06-1093","submitted_date"=>"11/2/2016","owner_id"=>"S8903622D","owner_occupation"=>"Nurse","owner_dob"=>"1/11/1989","source"=>"","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHEAH YAO DE","policy_term"=>"25","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Chua","life_firstname"=>"Gek Kwan","firstname"=>"Gek Kwan","lastname"=>"Chua","owner_occupation"=>"Homemaker","owner_dob"=>"5/3/1967","contact"=>98536797,"postal_code"=>"650317","address"=>"Blk 317 Bukit Batok Street 32 #04-141","email"=>"","main_product_id"=>6,"main_sum_assured"=>"900","currency_value"=>567.21,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>567.21,"agent_code"=>"KT09-1155","submitted_date"=>"11/3/2016","owner_id"=>"s1809422c","owner_occupation"=>"Homemaker","owner_dob"=>"5/3/1967","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"CHANG XIN ZHI, CLEMENCE","policy_term"=>"Yearly","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Ng","life_firstname"=>"Yu Zhe","firstname"=>"Kai Chern","lastname"=>"Ng","owner_occupation"=>"Manager","owner_dob"=>"5/25/1979","contact"=>null,"postal_code"=>"681688","address"=>"Blk 688A Choa Chu Kang Drive #09-340","email"=>"","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"NO07-1017","submitted_date"=>"11/3/2016","owner_id"=>"T1526364J","owner_occupation"=>"Baby","owner_dob"=>"8/26/2015","source"=>"Warm","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"CHEYENNE  SOH AI LAY","policy_term"=>"1","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Ng","life_firstname"=>"Yu Zhe","firstname"=>"Kai Chern","lastname"=>"Ng","owner_occupation"=>"Manager","owner_dob"=>"5/25/1979","contact"=>91519115,"postal_code"=>"681688","address"=>"Blk 688A Choa Chu Kang Drive #09-340","email"=>"","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>160,"agent_code"=>"NO07-1017","submitted_date"=>"11/3/2016","owner_id"=>"T1526364J","owner_occupation"=>"Baby","owner_dob"=>"8/26/2015","source"=>"Warm","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"CHEYENNE  SOH AI LAY","policy_term"=>"1","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Goh","life_firstname"=>"Lay Choo","firstname"=>"Lay Choo","lastname"=>"Goh","owner_occupation"=>"property development mana","owner_dob"=>"9/19/1970","contact"=>98469095,"postal_code"=>"271008","address"=>"Blk 8 Holland avenue #08-30","email"=>"lisa19_2003@hotmail.com","main_product_id"=>60,"main_sum_assured"=>"500000","currency_value"=>75.15,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>901.8,"agent_code"=>"AB01-1064","submitted_date"=>"11/3/2016","owner_id"=>"S7032813E","owner_occupation"=>"property development manager","owner_dob"=>"9/19/1970","source"=>"Referrals","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LI JINGJIN","policy_term"=>"15","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Kenyon","life_firstname"=>"Sin","firstname"=>"Sin","lastname"=>"Kenyon","owner_occupation"=>"delivery driver","owner_dob"=>"2/5/1993","contact"=>91991111,"postal_code"=>"120201","address"=>"APT BLK 201 Clementi Avenue 6 #01-21 ","email"=>"sinkenyon@hotmail.com","main_product_id"=>100,"main_sum_assured"=>"100000","currency_value"=>331.6,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>3979.2,"agent_code"=>"AB01-1064","submitted_date"=>"11/3/2016","owner_id"=>"S9303305A","owner_occupation"=>"delivery driver","owner_dob"=>"2/5/1993","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"LI JINGJIN","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"S/O NAGOOR MEERA","life_firstname"=>"SYED MASOOD","firstname"=>"SYED MASOOD","lastname"=>"S/O NAGOOR MEERA","owner_occupation"=>"SECURITY GUARD","owner_dob"=>"11/11/1954","contact"=>97844652,"postal_code"=>"650331","address"=>"BLK 331 BUKIT BATOK STREET 33 #09-233","email"=>"muhammadsm.2015@accountancy-smu.edu.sg","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>545.91,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>545.91,"agent_code"=>"TA09-1143","submitted_date"=>"11/3/2016","owner_id"=>"S1094040J","owner_occupation"=>"SECURITY GUARD","owner_dob"=>"11/11/1954","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"D/O ALLAPITCHAY","life_firstname"=>"ZEENAT BEEVI","firstname"=>"ZEENAT BEEVI","lastname"=>"D/O ALLAPITCHAY","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"4/26/1959","contact"=>97375934,"postal_code"=>"650331","address"=>"BLK 331 BUKIT BATOK STREET 33 #09-233","email"=>"muhammadsm.2015@accountancy-smu.edu.sg","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>578.01,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>578.01,"agent_code"=>"TA09-1143","submitted_date"=>"11/3/2016","owner_id"=>"S1343660F","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"4/26/1959","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"POO","life_firstname"=>"AH GEOK","firstname"=>"AH GEOK","lastname"=>"POO","owner_occupation"=>"STORE ASSISTANT","owner_dob"=>"1/12/1964","contact"=>97688281,"postal_code"=>"650323","address"=>"BLK 323 BUKIT BATOK STREET 33 #03-94","email"=>"ivytanmj@gmail.com","main_product_id"=>6,"main_sum_assured"=>"800","currency_value"=>554.26,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>554.26,"agent_code"=>"TA09-1143","submitted_date"=>"11/3/2016","owner_id"=>"S1627423B","owner_occupation"=>"STORE ASSISTANT","owner_dob"=>"1/12/1964","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Yan","life_firstname"=>"Ming","firstname"=>"Ming","lastname"=>"Yan","owner_occupation"=>"Engineer","owner_dob"=>"4/15/1970","contact"=>96783918,"postal_code"=>"650321","address"=>"Blk 321 Bukit Batok Street 33 #06-68","email"=>"haixia1938@gmail.com","main_product_id"=>6,"main_sum_assured"=>"1300","currency_value"=>599.47,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>599.47,"agent_code"=>"KT09-1135","submitted_date"=>"11/3/2016","owner_id"=>"S7060447G","owner_occupation"=>"Engineer","owner_dob"=>"4/15/1970","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"lim","life_firstname"=>"pei eng","firstname"=>"pei eng","lastname"=>"lim","owner_occupation"=>"na","owner_dob"=>"12/4/1955","contact"=>90774421,"postal_code"=>"542299","address"=>"299b compassvale street #05-102","email"=>"peter_ho_53@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>575.13,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.13,"agent_code"=>"NT12-1159","submitted_date"=>"11/3/2016","owner_id"=>"s11764550i","owner_occupation"=>"na","owner_dob"=>"12/4/1955","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"MOO ZHE AN","policy_term"=>"life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"bin musa","life_firstname"=>"zulkifli","firstname"=>"zulkifli","lastname"=>"bin musa","owner_occupation"=>"technician","owner_dob"=>"9/23/1971","contact"=>97818156,"postal_code"=>"541299","address"=>"299a compassvale street 03-142","email"=>"zulisfth@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"1300","currency_value"=>574.59,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>574.59,"agent_code"=>"JH12-1081","submitted_date"=>"11/3/2016","owner_id"=>"s7132264e","owner_occupation"=>"technician","owner_dob"=>"9/23/1971","source"=>"Door Knocking ","annual_income_range"=>" ","fullname_first"=>"LIM XIN YI","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"J S Tamizzuddin","life_firstname"=>"Syed Faizal S/O","firstname"=>"Syed Faizal S/O","lastname"=>"J S Tamizzuddin","owner_occupation"=>"Teacher","owner_dob"=>"9/19/1976","contact"=>98259575,"postal_code"=>"529863","address"=>"5 Simei Street 4 #09-07 ","email"=>"syedfaizal528@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"2500 monthly benefit","currency_value"=>951.02,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>951.02,"agent_code"=>"JH12-1083","submitted_date"=>"11/3/2016","owner_id"=>"S7629148I","owner_occupation"=>"Teacher","owner_dob"=>"9/19/1976","source"=>"Warm","annual_income_range"=>"$100,001-$150,000","fullname_first"=>"CHEN JIANHONG","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Lau","life_firstname"=>"Zer Hern Timothy","firstname"=>"Zer Hern Timothy","lastname"=>"Lau","owner_occupation"=>"Legal Associate","owner_dob"=>"2/1/1990","contact"=>82924693,"postal_code"=>"309309","address"=>"20 Evelyn Road #08-02 ","email"=>"lauzerhern@gmail.com","main_product_id"=>100,"main_sum_assured"=>"150000","currency_value"=>486.4,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>5836.8,"agent_code"=>"JC13-1073","submitted_date"=>"11/3/2016","owner_id"=>"S9077795E","owner_occupation"=>"Legal Associate","owner_dob"=>"2/1/1990","source"=>"Existing Client","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"ONG HUI MEI ENRY","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Lim","life_firstname"=>"Chai Siong ","firstname"=>"Chai Siong ","lastname"=>"Lim","owner_occupation"=>"Network Engineer","owner_dob"=>"11/21/1976","contact"=>92207206,"postal_code"=>"528609","address"=>"23 Tampines Central 7 #05-25 ","email"=>"chaisionglim@yahoo.com.sg","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"JC13-1073","submitted_date"=>"11/3/2016","owner_id"=>"S7681006J","owner_occupation"=>"Network Engineer","owner_dob"=>"11/21/1976","source"=>"Existing Client","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ONG HUI MEI ENRY","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Malvin Hernandez","life_firstname"=>"Malgapo Dennis","firstname"=>"Malgapo Dennis","lastname"=>"Malvin Hernandez","owner_occupation"=>"MEDICAL WRITER","owner_dob"=>"2/1/1977","contact"=>97879174,"postal_code"=>"118166","address"=>"42 South Buona Vista Road #02-06 ","email"=>"DENNIS.MALGAPO@MIMS.COM","main_product_id"=>102,"main_sum_assured"=>"15000","currency_value"=>142.9,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>1714.8,"agent_code"=>"JC13-1050","submitted_date"=>"11/3/2016","owner_id"=>"G5173565Q","owner_occupation"=>"MEDICAL WRITER","owner_dob"=>"2/1/1977","source"=>"Call Centre","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"AVIS ONG MEI MAN","policy_term"=>"18","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"KAI MIN","life_firstname"=>"CHAN ","firstname"=>"CHAN ","lastname"=>"KAI MIN","owner_occupation"=>"TECHNICIAN","owner_dob"=>"8/15/1964","contact"=>97631662,"postal_code"=>"730668","address"=>"BLK 668 WOODLANDS RING ROAD #09-345 ","email"=>"xingad@hotmail.com","main_product_id"=>102,"main_sum_assured"=>"20000","currency_value"=>248.95,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>2987.4,"agent_code"=>"TA09-1143","submitted_date"=>"11/3/2016","owner_id"=>"S2733159I","owner_occupation"=>"TECHNICIAN","owner_dob"=>"8/15/1964","source"=>"Road Show","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"12 years","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Quey","life_firstname"=>"Thiam Seng","firstname"=>"Thiam Seng","lastname"=>"Quey","owner_occupation"=>"Sales","owner_dob"=>"6/6/1967","contact"=>94472651,"postal_code"=>"600284","address"=>"block 284 Toh Guan Road #18-263 ","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1000","currency_value"=>558.49,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>558.49,"agent_code"=>"LN06-1160","submitted_date"=>"11/3/2016","owner_id"=>"S1818381A","owner_occupation"=>"Sales","owner_dob"=>"6/1/1910","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"TAN ZHEN FENG","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"KOH","life_firstname"=>"KAH AIK","firstname"=>"KAH AIK","lastname"=>"KOH","owner_occupation"=>"sales","owner_dob"=>"9/15/1967","contact"=>97691299,"postal_code"=>"680642","address"=>"blk 642 choa chu kang street 64 #12-79","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1000","currency_value"=>558.49,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>558.49,"agent_code"=>"LN06-1160","submitted_date"=>"11/3/2016","owner_id"=>"S1837669E","owner_occupation"=>"sales","owner_dob"=>"9/15/1967","source"=>"Referrals","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TAN ZHEN FENG","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"SEGARAN","life_firstname"=>"SAHANYA","firstname"=>"SINAYAH","lastname"=>"SEGARAN","owner_occupation"=>"","owner_dob"=>"5/2/1965","contact"=>93806502,"postal_code"=>"540232","address"=>"BLK 232 COMPASSVALE WALK #04-470","email"=>"","main_product_id"=>100,"main_sum_assured"=>"100000","currency_value"=>253.05,"payment_frequency"=>12,"mode_of_payment"=>"Cheque","annual_currency_value2"=>3036.6,"agent_code"=>"MT06-1016","submitted_date"=>"11/4/2016","owner_id"=>"T1492007I","owner_occupation"=>"","owner_dob"=>"3/28/2014","source"=>"","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"CLARIS  LIM SHIHUA","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Liu","life_firstname"=>"Fan Keong","firstname"=>"Fan Keong","lastname"=>"Liu","owner_occupation"=>"Driver","owner_dob"=>"8/14/1973","contact"=>97639086,"postal_code"=>"671635","address"=>"Blk 635A Senja Road #23-255","email"=>"shirlyn888@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.07,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.07,"agent_code"=>"KT09-1135","submitted_date"=>"11/4/2016","owner_id"=>"S7328713H","owner_occupation"=>"Driver","owner_dob"=>"8/14/1973","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"bin Abdul Rahman","life_firstname"=>"Zulkifli","firstname"=>"Zulkifli","lastname"=>"bin Abdul Rahman","owner_occupation"=>"Retired","owner_dob"=>"7/11/1962","contact"=>85460315,"postal_code"=>"650333","address"=>"Blk 333 Bukit Batok Street 32 #09-239","email"=>"","main_product_id"=>6,"main_sum_assured"=>"900","currency_value"=>549.02,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>549.02,"agent_code"=>"KT09-1135","submitted_date"=>"11/4/2016","owner_id"=>"S1548066A","owner_occupation"=>"Retired","owner_dob"=>"7/11/1962","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"binte Ali","life_firstname"=>"Khamisah","firstname"=>"Khamisah","lastname"=>"binte Ali","owner_occupation"=>"Retired","owner_dob"=>"9/9/1965","contact"=>85460315,"postal_code"=>"650333","address"=>"Blk 333 Bukit Batok Street 32 #09-239","email"=>"","main_product_id"=>6,"main_sum_assured"=>"800","currency_value"=>530.29,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>530.29,"agent_code"=>"KT09-1135","submitted_date"=>"11/4/2016","owner_id"=>"S1732221D","owner_occupation"=>"Retired","owner_dob"=>"9/9/1965","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"LIM","life_firstname"=>"SEOK KOON","firstname"=>"SEOK KOON","lastname"=>"LIM","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"4/11/1960","contact"=>82183538,"postal_code"=>"650323","address"=>"BLK 323 BUKIT BATOK STREET 33 #02-90","email"=>"","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>551.26,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>551.26,"agent_code"=>"TA09-1143","submitted_date"=>"11/4/2016","owner_id"=>"S1428131B","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"4/11/1960","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Weaver","life_firstname"=>"Ee-Jin Bronte","firstname"=>"Suan Cheng Joanne","lastname"=>"Chim","owner_occupation"=>"Homemaker","owner_dob"=>"10/2/1969","contact"=>96774917,"postal_code"=>"127321","address"=>"29C West Coast Road, Flora West","email"=>"jowea@singnet.com.sg","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"SL06-1092","submitted_date"=>"11/4/2016","owner_id"=>"T0327189C","owner_occupation"=>"Student","owner_dob"=>"10/1/2003","source"=>"Existing Client","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LOO WOON CHAN","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Weaver","life_firstname"=>"Ee-Jin Bronte","firstname"=>"Suan Cheng Joanne","lastname"=>"Chim","owner_occupation"=>"Homemaker","owner_dob"=>"10/2/1969","contact"=>96774917,"postal_code"=>"127321","address"=>"29C West Coast Road, Flora West","email"=>"jowea@singnet.com.sg","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>0,"agent_code"=>"SL06-1092","submitted_date"=>"11/4/2016","owner_id"=>"T0327189C","owner_occupation"=>"Student","owner_dob"=>"10/1/2003","source"=>"Existing Client","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LOO WOON CHAN","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Haryani Binte Hizin","life_firstname"=>"Liza","firstname"=>"Liza","lastname"=>"Haryani Binte Hizin","owner_occupation"=>"","owner_dob"=>"3/17/1976","contact"=>null,"postal_code"=>"541299","address"=>"299a compassvale street 09-130 singapore ","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1200/month","currency_value"=>554.85,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>554.85,"agent_code"=>"JH12-1132","submitted_date"=>"11/4/2016","owner_id"=>"s7607190h","owner_occupation"=>"","owner_dob"=>"3/17/1976","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"PAN KE HSING","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Leang Chua","life_firstname"=>"Kang","firstname"=>"Kang","lastname"=>"Leang Chua","owner_occupation"=>"","owner_dob"=>"12/15/1956","contact"=>93627700,"postal_code"=>"542292","address"=>"292b compassvale street 10-220 singapore ","email"=>"","main_product_id"=>6,"main_sum_assured"=>"700/month","currency_value"=>595.67,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>595.67,"agent_code"=>"JH12-1132","submitted_date"=>"11/4/2016","owner_id"=>"s1280146g","owner_occupation"=>"","owner_dob"=>"12/15/1956","source"=>"Door Knocking ","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"PAN KE HSING","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"MEI MEI","life_firstname"=>"TAN ","firstname"=>"TAN ","lastname"=>"MEI MEI","owner_occupation"=>"FINANCE & ADMIN EXECUTIVE","owner_dob"=>"7/2/1971","contact"=>97661905,"postal_code"=>"140083","address"=>"83 COMMONWEALTH CLOSE #02-01","email"=>"MMPIGLET71@GMAIL.COM","main_product_id"=>2,"main_sum_assured"=>"2000000","currency_value"=>1031.84,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>12382.08,"agent_code"=>"LN06-1152","submitted_date"=>"11/4/2016","owner_id"=>"S7121996H","owner_occupation"=>"FINANCE & ADMIN EXECUTIVE","owner_dob"=>"7/2/1971","source"=>"Warm","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"LIU ZHIRUI","policy_term"=>"53","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Foo ","life_firstname"=>"Hui Yu","firstname"=>"Hui Yu","lastname"=>"Foo ","owner_occupation"=>"PR Exectuive","owner_dob"=>"9/18/1991","contact"=>98803381,"postal_code"=>"S520896","address"=>"896 Tampines Street 81 #04-878 ","email"=>"gina.foo91@gmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF OA","annual_currency_value2"=>166.83,"agent_code"=>"JY08-1111","submitted_date"=>"11/7/2016","owner_id"=>"S9133190Z","owner_occupation"=>"PR Exectuive","owner_dob"=>"9/18/1991","source"=>"Referrals","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"LEE GOUHAO ERNEST","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Foo ","life_firstname"=>"Hui Yu","firstname"=>"Hui Yu","lastname"=>"Foo ","owner_occupation"=>"PR Exectuive","owner_dob"=>"9/18/1991","contact"=>98803381,"postal_code"=>"S520896","address"=>"896 Tampines Street 81 #04-878 ","email"=>"gina.foo91@gmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>493.2,"agent_code"=>"JY08-1111","submitted_date"=>"11/7/2016","owner_id"=>"S9133190Z","owner_occupation"=>"PR Exectuive","owner_dob"=>"9/18/1991","source"=>"Referrals","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"LEE GOUHAO ERNEST","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Chuang","life_firstname"=>"Eng Keng","firstname"=>"Wee Wee","lastname"=>"Goh","owner_occupation"=>"","owner_dob"=>"12/27/1949","contact"=>null,"postal_code"=>"537410","address"=>"39 Jalan Songket ","email"=>"","main_product_id"=>24,"main_sum_assured"=>"-","currency_value"=>1675.22,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>1675.22,"agent_code"=>"MW06-1048","submitted_date"=>"11/7/2016","owner_id"=>"S0048156D","owner_occupation"=>"","owner_dob"=>"6/4/1949","source"=>"Existing Client","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"CHRISTINA LIM LENG ENG","policy_term"=>"1","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"lee","life_firstname"=>"gerard ","firstname"=>"gerard ","lastname"=>"lee","owner_occupation"=>"engineer","owner_dob"=>"8/27/1965","contact"=>82833255,"postal_code"=>"641677","address"=>"677a jurong west 64 #01-269","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1000","currency_value"=>584.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>584.11,"agent_code"=>"NT12-1159","submitted_date"=>"11/7/2016","owner_id"=>"s1732395d","owner_occupation"=>"engineer","owner_dob"=>"8/27/1965","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"MOO ZHE AN","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"sim","life_firstname"=>"tse kong","firstname"=>"tse kong","lastname"=>"sim","owner_occupation"=>"engineer","owner_dob"=>"6/15/1962","contact"=>96333972,"postal_code"=>"543291","address"=>"291c compassvale st #11-258","email"=>"andy.sim@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"900","currency_value"=>596.1,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>596.1,"agent_code"=>"NT12-1159","submitted_date"=>"11/7/2016","owner_id"=>"s1567521G","owner_occupation"=>"engineer","owner_dob"=>"6/15/1962","source"=>"Door Knocking ","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"MOO ZHE AN","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"kong","life_firstname"=>"hui tuck","firstname"=>"hui tuck","lastname"=>"kong","owner_occupation"=>"security guard","owner_dob"=>"2/5/1973","contact"=>93355337,"postal_code"=>"542291","address"=>"291b compassvale st #06-240","email"=>"feb050273@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.05,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.05,"agent_code"=>"NT12-1159","submitted_date"=>"11/7/2016","owner_id"=>"s7303747f","owner_occupation"=>"security guard","owner_dob"=>"2/5/1973","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"MOO ZHE AN","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Yeo Yi Tong","life_firstname"=>"Daphne","firstname"=>"Shia ","lastname"=>"Ping Ngoh","owner_occupation"=>"Contracts Manager","owner_dob"=>"6/7/1961","contact"=>91868439,"postal_code"=>"507011","address"=>"5 Flora Drive #02-17","email"=>"shiapn@hotmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.83,"agent_code"=>"AB01-1039","submitted_date"=>"11/7/2016","owner_id"=>"S1506136G","owner_occupation"=>"Project Executive","owner_dob"=>"12/21/1991","source"=>"","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"ESTELLE CHOW SIEW LE","policy_term"=>"life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Yeo Yi Tong","life_firstname"=>"Daphne","firstname"=>"Shia ","lastname"=>"Ping Ngoh","owner_occupation"=>"Contracts Manager","owner_dob"=>"6/7/1961","contact"=>91868439,"postal_code"=>"507011","address"=>"5 Flora Drive #02-17","email"=>"shiapn@hotmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>493,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>493,"agent_code"=>"AB01-1039","submitted_date"=>"11/7/2016","owner_id"=>"S1506136G","owner_occupation"=>"Project Executive","owner_dob"=>"12/21/1991","source"=>"","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"ESTELLE CHOW SIEW LE","policy_term"=>"life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Bakar Bin Mohamed Kassim","life_firstname"=>"Abu","firstname"=>"Abu","lastname"=>"Bakar Bin Mohamed Kassim","owner_occupation"=>"Bank Clerk","owner_dob"=>"1/23/1958","contact"=>97772402,"postal_code"=>"541291","address"=>"291A Compassvale Street 02-292 Singapore ","email"=>"","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>565.76,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>565.76,"agent_code"=>"JH12-1132","submitted_date"=>"11/7/2016","owner_id"=>"S1335486C","owner_occupation"=>"Bank Clerk","owner_dob"=>"1/23/1958","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"PAN KE HSING","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Soon Yong","life_firstname"=>"Ng","firstname"=>"Ng","lastname"=>"Soon Yong","owner_occupation"=>"Deliveryman","owner_dob"=>"8/14/1973","contact"=>83684489,"postal_code"=>"542299","address"=>"299b Compassvale Street 02-108 Singapore","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.07,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.07,"agent_code"=>"JH12-1132","submitted_date"=>"11/7/2016","owner_id"=>"S7328299c","owner_occupation"=>"Deliveryman","owner_dob"=>"8/14/1973","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"PAN KE HSING","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"ONG","life_firstname"=>"KONG HUI","firstname"=>"KONG HUI","lastname"=>"ONG","owner_occupation"=>"","owner_dob"=>"6/17/1973","contact"=>91056424,"postal_code"=>"542292","address"=>"292B COMPASSVALE STREET #11-204","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1500","currency_value"=>595.88,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>595.88,"agent_code"=>"JH12-1120","submitted_date"=>"11/7/2016","owner_id"=>"S7321233B","owner_occupation"=>"","owner_dob"=>"6/17/1973","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"RAYNER GOH YONG EN","policy_term"=>"LIFETIME","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"HENG","life_firstname"=>"KHEOW SOON","firstname"=>"KHEOW SOON","lastname"=>"HENG","owner_occupation"=>"","owner_dob"=>"12/13/1963","contact"=>null,"postal_code"=>"541291","address"=>"291A COMPASSVALE STREET #13-288","email"=>"","main_product_id"=>6,"main_sum_assured"=>"900","currency_value"=>568.81,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>568.81,"agent_code"=>"JH12-1120","submitted_date"=>"11/7/2016","owner_id"=>"S1619115I","owner_occupation"=>"","owner_dob"=>"12/13/1963","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"RAYNER GOH YONG EN","policy_term"=>"LIFETIME","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Goh","life_firstname"=>"Yong Seng","firstname"=>"Yong Seng","lastname"=>"Goh","owner_occupation"=>"Mechanical Engineer","owner_dob"=>"8/26/1990","contact"=>81384113,"postal_code"=>"680507","address"=>"Blk 507 Choa Chu Kang St 51 #12-211 Singapore","email"=>"","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.83,"agent_code"=>"SL06-1115","submitted_date"=>"11/7/2016","owner_id"=>"S9031793H","owner_occupation"=>"Mechanical Engineer","owner_dob"=>"8/26/1990","source"=>"Warm","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"Ng Jun Feng","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Goh","life_firstname"=>"Yong Seng","firstname"=>"Yong Seng","lastname"=>"Goh","owner_occupation"=>"Mechanical Engineer","owner_dob"=>"8/26/1990","contact"=>81384113,"postal_code"=>"680507","address"=>"Blk 507 Choa Chu Kang St 51 #12-211 Singapore","email"=>"","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>493.2,"agent_code"=>"SL06-1115","submitted_date"=>"11/7/2016","owner_id"=>"S9031793H","owner_occupation"=>"Mechanical Engineer","owner_dob"=>"8/26/1990","source"=>"Warm","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"Ng Jun Feng","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Wang","life_firstname"=>"Chia Hui","firstname"=>"Chia Hui","lastname"=>"Wang","owner_occupation"=>"","owner_dob"=>"11/5/1975","contact"=>91909109,"postal_code"=>"426235","address"=>"52 Lorong G Telok Kurau #02-02 Singapore","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1000","currency_value"=>560.95,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>560.95,"agent_code"=>"SL06-1115","submitted_date"=>"11/7/2016","owner_id"=>"S7533841D","owner_occupation"=>"","owner_dob"=>"11/5/1975","source"=>"Referrals","annual_income_range"=>"$100,001-$150,000","fullname_first"=>"Ng Jun Feng","policy_term"=>"Lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Tan","life_firstname"=>"Woon Seng","firstname"=>"Woon Seng","lastname"=>"Tan","owner_occupation"=>"","owner_dob"=>"4/13/1952","contact"=>null,"postal_code"=>"321111","address"=>"111 Whampoa Road #02-37","email"=>"","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>750.14,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>750.14,"agent_code"=>"SL06-1088","submitted_date"=>"11/7/2016","owner_id"=>"S2510694F","owner_occupation"=>"","owner_dob"=>"4/13/1951","source"=>"Referrals","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"SAMMI LEE CHIA LING","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Tan","life_firstname"=>"Woon Seng","firstname"=>"Woon Seng","lastname"=>"Tan","owner_occupation"=>"","owner_dob"=>"4/13/1951","contact"=>null,"postal_code"=>"321111","address"=>"111 Whampoa Road #02-37","email"=>"","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>885,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>885,"agent_code"=>"SL06-1088","submitted_date"=>"11/7/2016","owner_id"=>"S2510694F","owner_occupation"=>"","owner_dob"=>"4/13/1951","source"=>"Referrals","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"SAMMI LEE CHIA LING","policy_term"=>"Life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Sawal","life_firstname"=>"Nur Hidayah Binte","firstname"=>"Nur Hidayah Binte","lastname"=>"Sawal","owner_occupation"=>"Student","owner_dob"=>"1/15/1988","contact"=>90256196,"postal_code"=>"610353","address"=>"353 Kang Ching Road #02-37 ","email"=>"","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.83,"agent_code"=>"SL06-1095","submitted_date"=>"11/7/2016","owner_id"=>"S8803219E","owner_occupation"=>"Student","owner_dob"=>"1/15/1988","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"ANG RUWEI CANDICE","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Sawal","life_firstname"=>"Nur Hidayah Binte","firstname"=>"Nur Hidayah Binte","lastname"=>"Sawal","owner_occupation"=>"Student","owner_dob"=>"1/15/1988","contact"=>90256196,"postal_code"=>"610353","address"=>"353 Kang Ching Road #02-37 ","email"=>"","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>493.2,"agent_code"=>"SL06-1095","submitted_date"=>"11/7/2016","owner_id"=>"S8803219E","owner_occupation"=>"Student","owner_dob"=>"1/15/1988","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"ANG RUWEI CANDICE","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Sawal","life_firstname"=>"Nur Hidayah Binte","firstname"=>"Nur Hidayah Binte","lastname"=>"Sawal","owner_occupation"=>"Student","owner_dob"=>"1/15/1988","contact"=>90256196,"postal_code"=>"610353","address"=>"353 Kang Ching Road #02-37 ","email"=>"","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>153.56,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>1842.72,"agent_code"=>"SL06-1095","submitted_date"=>"11/7/2016","owner_id"=>"S8803219E","owner_occupation"=>"Student","owner_dob"=>"1/15/1988","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"ANG RUWEI CANDICE","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Ng","life_firstname"=>"Jocelyn Li Ping","firstname"=>"Jocelyn Li Ping","lastname"=>"Ng","owner_occupation"=>"move coordinator","owner_dob"=>"11/21/1984","contact"=>null,"postal_code"=>"161019","address"=>"Blk 19 Lim Liak Street #02-06","email"=>"","main_product_id"=>27,"main_sum_assured"=>"25000","currency_value"=>25000,"payment_frequency"=>0,"mode_of_payment"=>"CPF OA","annual_currency_value2"=>2500,"agent_code"=>"MT06-1093","submitted_date"=>"11/7/2016","owner_id"=>"S8435191A","owner_occupation"=>"move coordinator","owner_dob"=>"11/21/1984","source"=>"Referrals","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"CHEAH YAO DE","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Tok","life_firstname"=>"Yu Feng","firstname"=>"Yu Feng","lastname"=>"Tok","owner_occupation"=>"Student","owner_dob"=>"1/1/1910","contact"=>93651243,"postal_code"=>"641666","address"=>"Blk 666AJurong West Street 65, #14-191","email"=>"tokyufeng@gmail.com","main_product_id"=>103,"main_sum_assured"=>"15000","currency_value"=>49.25,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>591,"agent_code"=>"MW06-1142","submitted_date"=>"11/7/2016","owner_id"=>"S9802189B","owner_occupation"=>"Student","owner_dob"=>"1/1/1910","source"=>"Canvassing","annual_income_range"=>"Below $30,000","fullname_first"=>"YEO HIOK WEE (YANG XUWEI)","policy_term"=>"25","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"NG","life_firstname"=>"KUI HUANG","firstname"=>"KUI HUANG","lastname"=>"NG","owner_occupation"=>"FOOD STALL OWNER","owner_dob"=>"9/20/1974","contact"=>96740550,"postal_code"=>"650319","address"=>"BLK 319 BUKIT BATOK STREET 33 #09-32 ","email"=>"theresankh@gmail.com","main_product_id"=>6,"main_sum_assured"=>"1100","currency_value"=>542.49,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>542.49,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S7477397D","owner_occupation"=>"FOOD STALL OWNER","owner_dob"=>"9/20/1974","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"LIM","life_firstname"=>"SWEE LIAN","firstname"=>"SWEE LIAN","lastname"=>"LIM","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"7/2/1955","contact"=>90544714,"postal_code"=>"650363","address"=>"BLK 363 BUKIT BATOK STREET 31 #03-31","email"=>"myip84@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>697.69,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>697.69,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S1128972H","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"7/2/1955","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"WOO","life_firstname"=>"HIN THYE KENNETH","firstname"=>"HIN THYE KENNETH","lastname"=>"WOO","owner_occupation"=>"CIVIL SERVANT","owner_dob"=>"8/16/1974","contact"=>90884084,"postal_code"=>"101111","address"=>"BLK 111A DEPOT ROAD #17-101 ","email"=>"woo_kenneth@yahoo.com","main_product_id"=>6,"main_sum_assured"=>"1500","currency_value"=>595.88,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>595.88,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S7497048F","owner_occupation"=>"CIVIL SERVANT","owner_dob"=>"8/16/1974","source"=>"Referrals","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"NG","life_firstname"=>"SEE LEH","firstname"=>"SEE LEH","lastname"=>"NG","owner_occupation"=>"PROJECT COORDINATOR","owner_dob"=>"12/8/1963","contact"=>92300622,"postal_code"=>"650331","address"=>"BLK 331 BUKIT BATOK STREET 33 #05-25 ","email"=>"ngseeleh@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"1000","currency_value"=>572.02,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>572.02,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S2580728F","owner_occupation"=>"PROJECT COORDINATOR","owner_dob"=>"12/8/1963","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"FOO","life_firstname"=>"MAY LING LINDA","firstname"=>"MAY LING LINDA","lastname"=>"FOO","owner_occupation"=>"MANAGER","owner_dob"=>"1/14/1970","contact"=>93868583,"postal_code"=>"659442","address"=>"50, BUKIT BATOK STREET 31 #23-01 ","email"=>"ifoo6977@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"1000","currency_value"=>569.35,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>569.35,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S7000840H","owner_occupation"=>"MANAGER","owner_dob"=>"1/14/1970","source"=>"Door Knocking ","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"LEE","life_firstname"=>"SOK HAR","firstname"=>"SOK HAR","lastname"=>"LEE","owner_occupation"=>"CLERK","owner_dob"=>"10/24/1957","contact"=>98766129,"postal_code"=>"650370","address"=>"BLK 370 BUKIT BATOK STREET 31 #07-205","email"=>"leenana2010@gmail.com","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>597.92,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>597.92,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S1225133E","owner_occupation"=>"CLERK","owner_dob"=>"10/24/1957","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"BTE HASSAN","life_firstname"=>"KAMISAH","firstname"=>"KAMISAH","lastname"=>"BTE HASSAN","owner_occupation"=>"ASST. COOK","owner_dob"=>"4/6/1955","contact"=>98511896,"postal_code"=>"650331","address"=>"BLK 331 BUKIT BATOK STREET 33 #05-229 ","email"=>"","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>552.49,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>552.49,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S1136491H","owner_occupation"=>"ASST. COOK","owner_dob"=>"4/6/1955","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"BIN ABU BAKAR","life_firstname"=>"ABDUL RASHID","firstname"=>"ABDUL RASHID","lastname"=>"BIN ABU BAKAR","owner_occupation"=>"DOCUMENTATION EXECUTIVE","owner_dob"=>"5/20/1957","contact"=>97394416,"postal_code"=>"650329","address"=>"BLK 329 BUKIT BATOK STREET 33 #10-97","email"=>"fer.ar@me.com","main_product_id"=>6,"main_sum_assured"=>"800","currency_value"=>599.2,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>599.2,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S1277286F","owner_occupation"=>"DOCUMENTATION EXECUTIVE","owner_dob"=>"5/20/1957","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"BIN ABDUL KADIR","life_firstname"=>"ZAINI ","firstname"=>"ZAINI ","lastname"=>"BIN ABDUL KADIR","owner_occupation"=>"CRANE OPERATOR","owner_dob"=>"6/12/1961","contact"=>83628149,"postal_code"=>"650333","address"=>"BLK 333 BUKIT BATOK STREET 32 #03-241","email"=>"","main_product_id"=>6,"main_sum_assured"=>"900","currency_value"=>575.82,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.82,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S1522949G","owner_occupation"=>"CRANE OPERATOR","owner_dob"=>"6/12/1961","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"SIA","life_firstname"=>"YOK CHEW ","firstname"=>"YOK CHEW ","lastname"=>"SIA","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"4/23/1956","contact"=>90837975,"postal_code"=>"650370","address"=>"BLK 370 BUKIT BATOK STREET 31 #03-201","email"=>"guofeng87@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>525.32,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>525.32,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S2573196D","owner_occupation"=>"HOUSEWIFE","owner_dob"=>"4/23/1956","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"CHONG","life_firstname"=>"TSU YAT","firstname"=>"TSU YAT","lastname"=>"CHONG","owner_occupation"=>"SELF EMPLOYED","owner_dob"=>"6/25/1952","contact"=>96288636,"postal_code"=>"650370","address"=>"BLK 370 BUKIT BATOK STREET 31 #03-201","email"=>"guofeng87@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>495.84,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>495.84,"agent_code"=>"TA09-1143","submitted_date"=>"11/7/2016","owner_id"=>"S2500367E","owner_occupation"=>"SELF EMPLOYED","owner_dob"=>"6/25/1952","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TOH TZE YI (ZHUO ZIYI)","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Muhammad Aliff Asraf","life_firstname"=>"Nur Ayesha Hawraa","firstname"=>"Suhailah","lastname"=>"Fauzi Abdat","owner_occupation"=>"Teacher","owner_dob"=>"9/15/1985","contact"=>82334142,"postal_code"=>"542274","address"=>"274B Compassvale Bow #06-519 ","email"=>"su_peaches@hotmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"AB01-1039","submitted_date"=>"11/8/2016","owner_id"=>"T16322029Z","owner_occupation"=>"Child","owner_dob"=>"10/14/2016","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ESTELLE CHOW SIEW LE","policy_term"=>"life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Muhammad Aliff Asraf","life_firstname"=>"Nur Ayesha Hawraa","firstname"=>"Suhailah","lastname"=>"Fauzi Abdat","owner_occupation"=>"Teacher","owner_dob"=>"9/15/1985","contact"=>82334142,"postal_code"=>"542274","address"=>"274B Compassvale Bow #06-519 ","email"=>"su_peaches@hotmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>160,"agent_code"=>"AB01-1039","submitted_date"=>"11/8/2016","owner_id"=>"T16322029Z","owner_occupation"=>"Child","owner_dob"=>"10/1/1910","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ESTELLE CHOW SIEW LE","policy_term"=>"life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"R","life_firstname"=>"Sandhia","firstname"=>"Sandhia","lastname"=>"R","owner_occupation"=>"","owner_dob"=>"1/27/1991","contact"=>null,"postal_code"=>"752592","address"=>"592B Montreal Link #14-18 ","email"=>"","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.83,"agent_code"=>"MT06-1042","submitted_date"=>"11/8/2016","owner_id"=>"S9103252Z","owner_occupation"=>"","owner_dob"=>"1/27/1991","source"=>"Existing Client","annual_income_range"=>" ","fullname_first"=>"KOH KAI LENG","policy_term"=>"WL","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Kok","life_firstname"=>"Chee Shin","firstname"=>"Chee Shin","lastname"=>"Kok","owner_occupation"=>"","owner_dob"=>"1/12/1982","contact"=>null,"postal_code"=>"642183","address"=>"183B Boon Lay Avenue #12-730 ","email"=>"","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>139.69,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>1676.28,"agent_code"=>"MT06-1042","submitted_date"=>"11/8/2016","owner_id"=>"S8200404A","owner_occupation"=>"","owner_dob"=>"1/12/1982","source"=>"Existing Client","annual_income_range"=>" ","fullname_first"=>"KOH KAI LENG","policy_term"=>"30","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Lee","life_firstname"=>"Jay Guat","firstname"=>"Jay Guat","lastname"=>"Lee","owner_occupation"=>"Production Operation","owner_dob"=>"12/9/1961","contact"=>98421647,"postal_code"=>"650324","address"=>"Blk 324 Bukit Batok Street 33 #07-31","email"=>"","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>502.9,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>502.9,"agent_code"=>"KT09-1036","submitted_date"=>"11/8/2016","owner_id"=>"S1505351H","owner_occupation"=>"Production Operation","owner_dob"=>"12/9/1961","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TAN CHUAN SHENG","policy_term"=>"yearly","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Loh","life_firstname"=>"Wai Seng","firstname"=>"Wai Seng","lastname"=>"Loh","owner_occupation"=>"Ship Fitter","owner_dob"=>"10/18/1955","contact"=>92998239,"postal_code"=>"650324","address"=>"Blk 324 Bukit Batok Street 33 #06-27","email"=>"","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>545.91,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>545.91,"agent_code"=>"KT09-1036","submitted_date"=>"11/8/2016","owner_id"=>"S1173122H","owner_occupation"=>"Ship Fitter","owner_dob"=>"10/18/1955","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TAN CHUAN SHENG","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Perumal","life_firstname"=>"Sundramutthy","firstname"=>"Sundramutthy","lastname"=>"Perumal","owner_occupation"=>"Supervisor","owner_dob"=>"2/2/1956","contact"=>94326056,"postal_code"=>"650334","address"=>"Blk 334 Bukit Batok Street 32 #03-285","email"=>"","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>318.42,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>318.42,"agent_code"=>"KT09-1036","submitted_date"=>"11/8/2016","owner_id"=>"S1256713H","owner_occupation"=>"Supervisor","owner_dob"=>"2/2/1956","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TAN CHUAN SHENG","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Mai","life_firstname"=>"Me Me Aye","firstname"=>"Me Me Aye","lastname"=>"Mai","owner_occupation"=>"nurse","owner_dob"=>"1/28/1970","contact"=>96443794,"postal_code"=>"650321","address"=>"blk 321 bukit batok street 33 #09-80","email"=>"maimemeaye@gmail.com","main_product_id"=>6,"main_sum_assured"=>"1000","currency_value"=>569.35,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>569.35,"agent_code"=>"KT09-1036","submitted_date"=>"11/8/2016","owner_id"=>"s7063853c","owner_occupation"=>"nurse","owner_dob"=>"1/28/1970","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TAN CHUAN SHENG","policy_term"=>"yearly","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Kamat","life_firstname"=>"Ibrahim","firstname"=>"Ibrahim","lastname"=>"Kamat","owner_occupation"=>"technician","owner_dob"=>"12/11/1968","contact"=>97874894,"postal_code"=>"650318","address"=>"blk 318 bukit batok street 32 #09-163","email"=>"ibni11@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"1200","currency_value"=>570.42,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>570.42,"agent_code"=>"KT09-1036","submitted_date"=>"11/8/2016","owner_id"=>"s6846594z","owner_occupation"=>"technician","owner_dob"=>"12/11/1968","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"TAN CHUAN SHENG","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"A Razak","life_firstname"=>"Hishamuddin","firstname"=>"Hishamuddin","lastname"=>"A Razak","owner_occupation"=>"security officer","owner_dob"=>"5/18/1965","contact"=>93847475,"postal_code"=>"650321","address"=>"blk 321 bukit batok street 33 #05-84","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1000","currency_value"=>546.72,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>546.72,"agent_code"=>"KT09-1036","submitted_date"=>"11/8/2016","owner_id"=>"s1727428g","owner_occupation"=>"security officer","owner_dob"=>"5/18/1965","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"TAN CHUAN SHENG","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Talib","life_firstname"=>"Rohayati","firstname"=>"Rohayati","lastname"=>"Talib","owner_occupation"=>"homemaker","owner_dob"=>"4/12/1967","contact"=>90236642,"postal_code"=>"650321","address"=>"blk 321 bukit batok street 33 #05-84","email"=>"","main_product_id"=>6,"main_sum_assured"=>"900","currency_value"=>567.21,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>567.21,"agent_code"=>"KT09-1036","submitted_date"=>"11/8/2016","owner_id"=>"s1818753a","owner_occupation"=>"homemaker","owner_dob"=>"4/12/1967","source"=>"Door Knocking ","annual_income_range"=>" ","fullname_first"=>"TAN CHUAN SHENG","policy_term"=>"yearly","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Ng Yong Rong","life_firstname"=>"Shaun ","firstname"=>"Shaun ","lastname"=>"Ng Yong Rong","owner_occupation"=>"Market Research Executive","owner_dob"=>"8/12/1991","contact"=>91176013,"postal_code"=>"529777","address"=>"63 Tampines Avenue 1 #08-04","email"=>"ngshaun91@gmail.com","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>94.88,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>1138.56,"agent_code"=>"GF03-1140","submitted_date"=>"11/8/2016","owner_id"=>"S9127815D","owner_occupation"=>"Market Research Executive","owner_dob"=>"8/12/1991","source"=>"Call Centre","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"JENNIFER UTAMA","policy_term"=>"35","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Ang","life_firstname"=>"Wee Meng","firstname"=>"Wee Meng","lastname"=>"Ang","owner_occupation"=>"Engineer","owner_dob"=>"3/13/1982","contact"=>96697792,"postal_code"=>"760223","address"=>"223 Yishun Street 21 #09-457","email"=>"weemeng1982@gmail.com","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>2072.9,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>2072.9,"agent_code"=>"TA09-1029","submitted_date"=>"11/8/2016","owner_id"=>"S8207904A","owner_occupation"=>"Engineer","owner_dob"=>"3/13/1982","source"=>"","annual_income_range"=>" ","fullname_first"=>"THOMAS ANG WEE PING","policy_term"=>"27","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Yitang","life_firstname"=>"Xu ","firstname"=>"Xu","lastname"=>"Mingming","owner_occupation"=>"Purchasing Manager","owner_dob"=>"4/30/1985","contact"=>85889165,"postal_code"=>"560248","address"=>"Blk 248 ang mo kio ave 2 #03-10","email"=>"singxmm@gmail.com","main_product_id"=>24,"main_sum_assured"=>"-","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"AB01-1064","submitted_date"=>"11/8/2016","owner_id"=>"T1632958J","owner_occupation"=>"child","owner_dob"=>"10/22/2016","source"=>"Existing Client","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"LI JINGJIN","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Yitang","life_firstname"=>"Xu ","firstname"=>"Xu","lastname"=>"Mingming","owner_occupation"=>"Purchasing Manager","owner_dob"=>"4/30/1985","contact"=>85889165,"postal_code"=>"560248","address"=>"Blk 248 ang mo kio ave 2 #03-10","email"=>"singxmm@gmail.com","main_product_id"=>99,"main_sum_assured"=>"-","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>160,"agent_code"=>"AB01-1064","submitted_date"=>"11/8/2016","owner_id"=>"T1632958J","owner_occupation"=>"child","owner_dob"=>"10/22/2016","source"=>"Existing Client","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"LI JINGJIN","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Yiran","life_firstname"=>"Xu ","firstname"=>"Xu","lastname"=>"Mingming","owner_occupation"=>"Purchasing Manager","owner_dob"=>"4/30/1985","contact"=>85889165,"postal_code"=>"560248","address"=>"Blk 248 ang mo kio ave 2 #03-10","email"=>"singxmm@gmail.com","main_product_id"=>99,"main_sum_assured"=>"-","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>160,"agent_code"=>"AB01-1064","submitted_date"=>"11/8/2016","owner_id"=>"T1400109Z","owner_occupation"=>"child","owner_dob"=>"1/2/2014","source"=>"Existing Client","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"LI JINGJIN","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Ng","life_firstname"=>"Guat Im","firstname"=>"Guat Im","lastname"=>"Ng","owner_occupation"=>"Admin Manager","owner_dob"=>"7/8/1970","contact"=>98290230,"postal_code"=>"310035","address"=>"Blk 35 Toa Payoh Lorong 5 #06-333","email"=>"jovi.creative@gmail.com","main_product_id"=>21,"main_sum_assured"=>"NA","currency_value"=>207.25,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>2487,"agent_code"=>"PT10-1068","submitted_date"=>"11/8/2016","owner_id"=>"S7076430Z","owner_occupation"=>"Admin Manager","owner_dob"=>"7/8/1970","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"Ng Yan Ying","policy_term"=>"28","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"lim","life_firstname"=>"lam choo","firstname"=>"lam choo","lastname"=>"lim","owner_occupation"=>"clerk","owner_dob"=>"2/22/1959","contact"=>90059432,"postal_code"=>"541296","address"=>"296a compassvale crescent #13-285","email"=>"yvonnelim9454@gmail.com","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>569.45,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>569.45,"agent_code"=>"NT12-1159","submitted_date"=>"11/8/2016","owner_id"=>"s1359454F","owner_occupation"=>"clerk","owner_dob"=>"2/22/1959","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"MOO ZHE AN","policy_term"=>"life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Chia ","life_firstname"=>"Yi Le Russell","firstname"=>"Kee Cheng Lawrence","lastname"=>"Chia","owner_occupation"=>"Director","owner_dob"=>"9/20/1980","contact"=>93283777,"postal_code"=>"S(600327)","address"=>"327 Jurong East Street 31 #03-160 ","email"=>"lawman.chia@gmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"T1626953G","owner_occupation"=>"Baby","owner_dob"=>"8/24/2016","source"=>"Existing Client","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Chia ","life_firstname"=>"Yi Le Russell","firstname"=>"Kee Cheng Lawrence","lastname"=>"Chia","owner_occupation"=>"Director","owner_dob"=>"9/20/1980","contact"=>93283777,"postal_code"=>"S(600327)","address"=>"327 Jurong East Street 31 #03-160 ","email"=>"lawman.chia@gmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>160,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"T1626953G","owner_occupation"=>"Baby","owner_dob"=>"8/24/2016","source"=>"Existing Client","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Wang","life_firstname"=>"Peiyu","firstname"=>"Yu","lastname"=>"Li","owner_occupation"=>"","owner_dob"=>"8/30/1977","contact"=>97885338,"postal_code"=>"S(259724)","address"=>"355 Bukit Timah Road #06-02 ","email"=>"joyeeli@gmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>1505.14,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>1505.14,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"G0489110X","owner_occupation"=>"retiree","owner_dob"=>"11/8/1955","source"=>"Others","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Wang","life_firstname"=>"Peiyu","firstname"=>"Yu","lastname"=>"Li","owner_occupation"=>"","owner_dob"=>"8/30/1977","contact"=>97885338,"postal_code"=>"S(259724)","address"=>"355 Bukit Timah Road #06-02 ","email"=>"joyeeli@gmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>885,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>885,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"G0489110X","owner_occupation"=>"retiree","owner_dob"=>"11/8/1955","source"=>"Others","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Ng","life_firstname"=>"Yat Fai","firstname"=>"Yat Fai","lastname"=>"Ng","owner_occupation"=>"Retiree","owner_dob"=>"3/6/1950","contact"=>96993755,"postal_code"=>"S(588374)","address"=>"4 Rifle Range Road #04-01 ","email"=>"fengli85@gmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>1675.22,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>1675.22,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S2549644B","owner_occupation"=>"Retiree","owner_dob"=>"3/6/1950","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Ng","life_firstname"=>"Yat Fai","firstname"=>"Yat Fai","lastname"=>"Ng","owner_occupation"=>"Retiree","owner_dob"=>"3/6/1950","contact"=>96993755,"postal_code"=>"S(588374)","address"=>"4 Rifle Range Road #04-01 ","email"=>"fengli85@gmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>2372.73,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>2372.73,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S2549644B","owner_occupation"=>"Retiree","owner_dob"=>"3/6/1950","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Salleh","life_firstname"=>"Muhamad Zaki Bin ","firstname"=>"Muhamad Zaki Bin ","lastname"=>"Salleh","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","contact"=>90066162,"postal_code"=>"S(510626)","address"=>"626 Pasir Ris Drive 3 #02-308 ","email"=>"zakkey18@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"1700","currency_value"=>683.73,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>683.73,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S7407569Z","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Salleh","life_firstname"=>"Muhamad Zaki Bin ","firstname"=>"Muhamad Zaki Bin ","lastname"=>"Salleh","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","contact"=>90066162,"postal_code"=>"S(510626)","address"=>"626 Pasir Ris Drive 3 #02-308 ","email"=>"zakkey18@hotmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>115,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>115,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S7407569Z","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Buang","life_firstname"=>"Noor Zabian Binte ","firstname"=>"Muhamad Zaki Bin ","lastname"=>"Salleh","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","contact"=>90066162,"postal_code"=>"S(510626)","address"=>"626 Pasir Ris Drive 3 #02-308 ","email"=>"zakkey18@hotmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>115,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>115,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S8213672Z","owner_occupation"=>"homemaker","owner_dob"=>"5/15/1982","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Buang","life_firstname"=>"Noor Zabian Binte ","firstname"=>"Muhamad Zaki Bin ","lastname"=>"Salleh","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","contact"=>90066162,"postal_code"=>"S(510626)","address"=>"626 Pasir Ris Drive 3 #02-308 ","email"=>"zakkey18@hotmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>278.17,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>278.17,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S8213672Z","owner_occupation"=>"homemaker","owner_dob"=>"5/15/1982","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Muhamad Zaki","life_firstname"=>"Nur Zuyyin Putri","firstname"=>"Muhamad Zaki Bin ","lastname"=>"Salleh","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","contact"=>90066162,"postal_code"=>"S(510626)","address"=>"626 Pasir Ris Drive 3 #02-308 ","email"=>"zakkey18@hotmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>148.37,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>148.37,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"T1331228H","owner_occupation"=>"baby","owner_dob"=>"10/21/2013","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Muhamad Zaki","life_firstname"=>"Nur Zuyyin Putri","firstname"=>"Muhamad Zaki Bin ","lastname"=>"Salleh","owner_occupation"=>"Supervisor","owner_dob"=>"3/18/1974","contact"=>90066162,"postal_code"=>"S(510626)","address"=>"626 Pasir Ris Drive 3 #02-308 ","email"=>"zakkey18@hotmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>66.24,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>66.24,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"T1331228H","owner_occupation"=>"baby","owner_dob"=>"10/21/2013","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Ong","life_firstname"=>"Yi Wen Geraldine","firstname"=>"Jia Ming","lastname"=>"Tan ","owner_occupation"=>"Pricing Exec","owner_dob"=>"5/10/1988","contact"=>91775491,"postal_code"=>"S(541286)","address"=>"286A Compassvale Crescent #11-77 ","email"=>"trey.tanjm@gmail.com","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>1623.825,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>1623.825,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S8822502C","owner_occupation"=>"Admin","owner_dob"=>"6/28/1988","source"=>"Aviva Lead","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"36","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Tan","life_firstname"=>"Jia Ming","firstname"=>"Yi Wen Geraldine","lastname"=>"Ong","owner_occupation"=>"Admin","owner_dob"=>"6/28/1988","contact"=>92975490,"postal_code"=>"S(541286)","address"=>"286A Compassvale Crescent #11-77 ","email"=>"geraldineoyw88@gmail.com","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>1715.25,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>1715.25,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S8816443A","owner_occupation"=>"Pricing Exec","owner_dob"=>"5/10/1988","source"=>"Aviva Lead","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"36","owner_gender"=>"Female","life_gender"=>"Male"),
  array("life_lastname"=>"Goh","life_firstname"=>"Soon Khoon","firstname"=>"Soon Khoon","lastname"=>"Goh","owner_occupation"=>"Army Officer","owner_dob"=>"9/12/1976","contact"=>96839266,"postal_code"=>"S(670177)","address"=>"177 Lompang Road #23-06 ","email"=>"goh_sk76@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"1600","currency_value"=>589.89,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>589.89,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S7629095D","owner_occupation"=>"Army Officer","owner_dob"=>"9/12/1976","source"=>"Existing Client","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Tan ","life_firstname"=>"Yin Pheng","firstname"=>"Yin Pheng","lastname"=>"Tan ","owner_occupation"=>"Admin","owner_dob"=>"9/16/1973","contact"=>90471199,"postal_code"=>"S(670544)","address"=>"544 Jelapang Road #06-544 ","email"=>"cheryltanyp@gmail.com","main_product_id"=>8,"main_sum_assured"=>"1000","currency_value"=>605.73,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>605.73,"agent_code"=>"JY08-1032","submitted_date"=>"11/8/2016","owner_id"=>"S7334134E","owner_occupation"=>"Admin","owner_dob"=>"9/16/1973","source"=>"Referrals","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHAN JUN YUAN","policy_term"=>"0","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Abg","life_firstname"=>"Shu Min ","firstname"=>"Shu Min ","lastname"=>"Abg","owner_occupation"=>"Teacher","owner_dob"=>"3/2/1991","contact"=>null,"postal_code"=>"640857","address"=>"Blk 857 Juring West St 81 #13-554","email"=>"","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.83,"agent_code"=>"NO07-1017","submitted_date"=>"11/9/2016","owner_id"=>"S9108246B","owner_occupation"=>"Teacher","owner_dob"=>"3/2/1991","source"=>"Referrals","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHEYENNE  SOH AI LAY","policy_term"=>"1","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Abg","life_firstname"=>"Shu Min ","firstname"=>"Shu Min ","lastname"=>"Abg","owner_occupation"=>"Teacher","owner_dob"=>"3/2/1991","contact"=>null,"postal_code"=>"640857","address"=>"Blk 857 Juring West St 81 #13-554","email"=>"","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>493.2,"agent_code"=>"NO07-1017","submitted_date"=>"11/9/2016","owner_id"=>"S9108246B","owner_occupation"=>"Teacher","owner_dob"=>"3/2/1991","source"=>"Referrals","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHEYENNE  SOH AI LAY","policy_term"=>"1","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"YISHAN","life_firstname"=>"LEE","firstname"=>"LEE","lastname"=>"YISHAN","owner_occupation"=>"STUDENT","owner_dob"=>"10/9/1995","contact"=>96412513,"postal_code"=>"760637","address"=>"BLK 637 YISHUN STREET 61 #12-120 ","email"=>"","main_product_id"=>24,"main_sum_assured"=>"NA","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.83,"agent_code"=>"KT09-1103","submitted_date"=>"11/9/2016","owner_id"=>"S9536886G","owner_occupation"=>"STUDENT","owner_dob"=>"10/9/1995","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"YUN HAN FUNG","policy_term"=>"YEARLY RENEWABLE","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"YISHAN","life_firstname"=>"LEE","firstname"=>"LEE","lastname"=>"YISHAN","owner_occupation"=>"STUDENT","owner_dob"=>"10/9/1995","contact"=>96412513,"postal_code"=>"760637","address"=>"BLK 637 YISHUN STREET 61 #12-120 ","email"=>"","main_product_id"=>99,"main_sum_assured"=>"NA","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>493.2,"agent_code"=>"KT09-1103","submitted_date"=>"11/9/2016","owner_id"=>"S9536886G","owner_occupation"=>"STUDENT","owner_dob"=>"10/9/1995","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"YUN HAN FUNG","policy_term"=>"YEARLY RENEWABLE","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Rosli","life_firstname"=>"Murnie Bte","firstname"=>"Murnie Bte","lastname"=>"Rosli","owner_occupation"=>"Management Support Office","owner_dob"=>"9/30/1976","contact"=>96510512,"postal_code"=>"460120","address"=>"Blk 120 Bedok North Street 2 #05-180","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1000","currency_value"=>560.95,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>560.95,"agent_code"=>"TA09-1047","submitted_date"=>"11/9/2016","owner_id"=>"S7629726F","owner_occupation"=>"Management Support Officer","owner_dob"=>"9/30/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"Yearly Renewable ","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Rosli","life_firstname"=>"Murnie Bte","firstname"=>"Murnie Bte","lastname"=>"Rosli","owner_occupation"=>"Management Support Office","owner_dob"=>"9/30/1976","contact"=>96510512,"postal_code"=>"460120","address"=>"Blk 120 Bedok North Street 2 #05-180","email"=>"","main_product_id"=>24,"main_sum_assured"=>"NA","currency_value"=>303.09,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>303.09,"agent_code"=>"TA09-1047","submitted_date"=>"11/9/2016","owner_id"=>"S7629726F","owner_occupation"=>"Management Support Officer","owner_dob"=>"9/30/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"Yearly Renewable ","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Lim","life_firstname"=>"Choon Siah","firstname"=>"Choon Siah","lastname"=>"Lim","owner_occupation"=>"Management Support Office","owner_dob"=>"9/15/1976","contact"=>92781026,"postal_code"=>"310163","address"=>"Blk 163 Lorong 1 Toa Payoh #10-1010","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"TA09-1047","submitted_date"=>"11/9/2016","owner_id"=>"S7629228J","owner_occupation"=>"Management Support Officer","owner_dob"=>"9/15/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"Yearly Renewable ","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Tan","life_firstname"=>"Yen Hau Desmond","firstname"=>"Yen Hau Desmond","lastname"=>"Tan","owner_occupation"=>"Lawyer","owner_dob"=>"9/19/1976","contact"=>98202440,"postal_code"=>"470115","address"=>"Blk 115 Bedok Reservoir Road #04-124","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"TA09-1047","submitted_date"=>"11/9/2016","owner_id"=>"S7629988I","owner_occupation"=>"Lawyer","owner_dob"=>"9/19/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"Yearly Renewable ","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Yip","life_firstname"=>"Wei Hsiung","firstname"=>"Wei Hsiung","lastname"=>"Yip","owner_occupation"=>"Engineer","owner_dob"=>"9/10/1976","contact"=>81860569,"postal_code"=>"120511","address"=>"Blk 511 West Coast Drive #10-323","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"TA09-1047","submitted_date"=>"11/9/2016","owner_id"=>"S7629753C","owner_occupation"=>"Engineer","owner_dob"=>"9/10/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"Yearly Renewable ","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Ong","life_firstname"=>"Swee Heng Michael","firstname"=>"Swee Heng Michael","lastname"=>"Ong","owner_occupation"=>"Director","owner_dob"=>"9/17/1976","contact"=>90993943,"postal_code"=>"439865","address"=>"15 Amber Road #17-02","email"=>"Mmikey76@gmail.com","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"TA09-1047","submitted_date"=>"11/9/2016","owner_id"=>"S7629890D","owner_occupation"=>"Director","owner_dob"=>"9/17/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"Yearly Renewable ","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Koh","life_firstname"=>"Boon Kwee","firstname"=>"Boon Kwee","lastname"=>"Koh","owner_occupation"=>"Banker","owner_dob"=>"8/16/1976","contact"=>94304305,"postal_code"=>"521156","address"=>"Blk 156 Tampines Street 12 #08-21","email"=>"","main_product_id"=>8,"main_sum_assured"=>"1300","currency_value"=>587.11,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>587.11,"agent_code"=>"TA09-1047","submitted_date"=>"11/9/2016","owner_id"=>"S7624852D","owner_occupation"=>"Banker","owner_dob"=>"8/16/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"JUN SIANG GOH","policy_term"=>"Yearly Renewable ","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Low","life_firstname"=>"Peck Khee","firstname"=>"Peck Khee","lastname"=>"Low","owner_occupation"=>"student","owner_dob"=>"7/14/1962","contact"=>90276273,"postal_code"=>"440062","address"=>"Blk 62 Marine Drive #14-106","email"=>"pksewlife@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"700","currency_value"=>502.9,"payment_frequency"=>0,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>502.9,"agent_code"=>"WK09-1162","submitted_date"=>"11/9/2016","owner_id"=>"s15947374C","owner_occupation"=>"student","owner_dob"=>"7/14/1962","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Bin Sukarjan","life_firstname"=>"Subari","firstname"=>"Subari","lastname"=>"Bin Sukarjan","owner_occupation"=>"Taxi Driver","owner_dob"=>"7/19/1960","contact"=>90236841,"postal_code"=>"650311","address"=>"Blk 311 Bukit Batok street 32 #01-31","email"=>"shahfiqueyusof@gmail.com","main_product_id"=>6,"main_sum_assured"=>"800","currency_value"=>515.79,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>515.79,"agent_code"=>"WK09-1162","submitted_date"=>"11/9/2016","owner_id"=>"S1430132A","owner_occupation"=>"Taxi Driver","owner_dob"=>"7/19/1960","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Lay","life_firstname"=>"Wai Leong","firstname"=>"Wai Leong","lastname"=>"Lay","owner_occupation"=>"Electrician","owner_dob"=>"7/20/1973","contact"=>86141293,"postal_code"=>"650313","address"=>"Blk 313 Bukit Batok St 32 #05-55","email"=>"wllay11@gmail.com","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.05,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.05,"agent_code"=>"WK09-1162","submitted_date"=>"11/9/2016","owner_id"=>"s7387093C","owner_occupation"=>"Electrician","owner_dob"=>"7/20/1973","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Wong","life_firstname"=>"Lian Sim Teresa","firstname"=>"Lian Sim Teresa","lastname"=>"Wong","owner_occupation"=>"Clerk","owner_dob"=>"1/5/1963","contact"=>96315530,"postal_code"=>"1544","address"=>"Blk 61 Marine Drive #08-86","email"=>"teresawo34@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"800","currency_value"=>579.67,"payment_frequency"=>0,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>579.67,"agent_code"=>"WK09-1162","submitted_date"=>"11/9/2016","owner_id"=>"S1578934D","owner_occupation"=>"Clerk","owner_dob"=>"1/5/1963","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"Lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Ang","life_firstname"=>"Eng Soon","firstname"=>"Eng Soon","lastname"=>"Ang","owner_occupation"=>"Sales Executive","owner_dob"=>"7/29/1973","contact"=>90085712,"postal_code"=>"520811","address"=>"blk 811 Tampines Avenue 4 #11-197","email"=>"paulang72@gmail.com","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.07,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.07,"agent_code"=>"KT09-1135","submitted_date"=>"11/9/2016","owner_id"=>"S7327486I","owner_occupation"=>"Sales Executive","owner_dob"=>"7/29/1973","source"=>"Referrals","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Bustami","life_firstname"=>"Maria","firstname"=>"Maria","lastname"=>"Bustami","owner_occupation"=>"Self employed","owner_dob"=>"2/7/1973","contact"=>94513299,"postal_code"=>"440064","address"=>"Blk 64 Marine Drive #04-140","email"=>"maria123ang@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"1100","currency_value"=>564.64,"payment_frequency"=>0,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>564.64,"agent_code"=>"WK09-1162","submitted_date"=>"11/9/2016","owner_id"=>"s7372327B","owner_occupation"=>"Self employed","owner_dob"=>"2/7/1973","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"Lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Lam","life_firstname"=>"Zong Hao Herman","firstname"=>"Zong Hao Herman","lastname"=>"Lam","owner_occupation"=>"Sales Executive","owner_dob"=>"2/11/1993","contact"=>93622520,"postal_code"=>"760610","address"=>"Block 610 yishun street 61 #09-227","email"=>"herman.lamzh@gmail.com","main_product_id"=>24,"main_sum_assured"=>"as charged","currency_value"=>166.83,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>166.83,"agent_code"=>"JH12-1085","submitted_date"=>"11/9/2016","owner_id"=>"S9308469A","owner_occupation"=>"Sales Executive","owner_dob"=>"2/11/1993","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"KER JIAN MIN DANIEL","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Lam","life_firstname"=>"Zong Hao Herman","firstname"=>"Zong Hao Herman","lastname"=>"Lam","owner_occupation"=>"Sales Executive","owner_dob"=>"2/11/1993","contact"=>93622520,"postal_code"=>"760610","address"=>"Block 610 yishun street 61 #09-227","email"=>"herman.lamzh@gmail.com","main_product_id"=>99,"main_sum_assured"=>"as charged","currency_value"=>493.2,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>493.2,"agent_code"=>"JH12-1085","submitted_date"=>"11/9/2016","owner_id"=>"S9308469A","owner_occupation"=>"Sales Executive","owner_dob"=>"2/11/1993","source"=>"Referrals","annual_income_range"=>"Below $30,000","fullname_first"=>"KER JIAN MIN DANIEL","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Sim","life_firstname"=>"Hui Yi","firstname"=>"Hui Yi","lastname"=>"Sim","owner_occupation"=>"Accountant","owner_dob"=>"8/12/1982","contact"=>97457680,"postal_code"=>"643652","address"=>"Blk 652C Jurong West Street 61 #15-416","email"=>"oceanlife12@gmail.com","main_product_id"=>24,"main_sum_assured"=>"As Charged","currency_value"=>859.78,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>859.78,"agent_code"=>"PT10-1078","submitted_date"=>"11/10/2016","owner_id"=>"G0882216L","owner_occupation"=>"Housewife","owner_dob"=>"7/19/1960","source"=>"Pod Duty","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"Wong Pei","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Sim","life_firstname"=>"Hui Yi","firstname"=>"Hui Yi","lastname"=>"Sim","owner_occupation"=>"Accountant","owner_dob"=>"8/12/1982","contact"=>97457680,"postal_code"=>"643652","address"=>"Blk 652C Jurong West Street 61 #15-416","email"=>"oceanlife12@gmail.com","main_product_id"=>99,"main_sum_assured"=>"As Charged","currency_value"=>915.2,"payment_frequency"=>1,"mode_of_payment"=>"Cash","annual_currency_value2"=>915.2,"agent_code"=>"PT10-1078","submitted_date"=>"11/10/2016","owner_id"=>"G0882216L","owner_occupation"=>"Housewife","owner_dob"=>"7/19/1960","source"=>"Pod Duty","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"Wong Pei","policy_term"=>"life","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"Bin Jais","life_firstname"=>"Mohammed Madeni ","firstname"=>"Mohammed Madeni ","lastname"=>"Bin Jais","owner_occupation"=>"Executive Architect","owner_dob"=>"11/3/1985","contact"=>97727676,"postal_code"=>"468080","address"=>"5 Jalan Remis ","email"=>"madeni85@gmail.com","main_product_id"=>87,"main_sum_assured"=>"N.A.","currency_value"=>211.1,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>2533.2,"agent_code"=>"KL05-1013","submitted_date"=>"11/10/2016","owner_id"=>"S8532964B","owner_occupation"=>"Executive Architect","owner_dob"=>"11/3/1985","source"=>"Warm","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"FARIDAH  BTE HASSAN","policy_term"=>"40","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Yong","life_firstname"=>"Poh Fong","firstname"=>"Poh Fong","lastname"=>"Yong","owner_occupation"=>"Electronics","owner_dob"=>"6/20/1976","contact"=>90566807,"postal_code"=>"50538","address"=>"538 Upper Cross Street #07-261","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1200","currency_value"=>554.85,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>554.85,"agent_code"=>"TA09-1029","submitted_date"=>"11/10/2016","owner_id"=>"S7657270D","owner_occupation"=>"Electronics","owner_dob"=>"6/20/1976","source"=>"","annual_income_range"=>" ","fullname_first"=>"THOMAS ANG WEE PING","policy_term"=>"Life","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"NA","life_firstname"=>"P Selvaraju","firstname"=>"P Selvaraju","lastname"=>"NA","owner_occupation"=>"HSE Operations Manager","owner_dob"=>"8/1/1910","contact"=>96398200,"postal_code"=>"750471","address"=>"Blk 471 Sembawang Drive #15-429","email"=>"raaj1721@hotmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>784.63,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>784.63,"agent_code"=>"GF03-1005","submitted_date"=>"11/10/2016","owner_id"=>"S1721999E","owner_occupation"=>"HSE Operations Manager","owner_dob"=>"8/1/1910","source"=>"","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"GABRIEL  FRANCIS JOSEPH","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"NA","life_firstname"=>"P Selvaraju","firstname"=>"P Selvaraju","lastname"=>"NA","owner_occupation"=>"HSE Operations Manager","owner_dob"=>"8/8/1965","contact"=>96398200,"postal_code"=>"750471","address"=>"Blk 471 Sembawang Drive #15-429","email"=>"raaj1721@hotmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>680,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>680,"agent_code"=>"GF03-1005","submitted_date"=>"11/10/2016","owner_id"=>"S1721999E","owner_occupation"=>"HSE Operations Manager","owner_dob"=>"8/8/1965","source"=>"","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"GABRIEL  FRANCIS JOSEPH","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"LAM THI","life_firstname"=>"KIM THI","firstname"=>"KIM THI","lastname"=>"LAM THI","owner_occupation"=>"","owner_dob"=>"4/20/1985","contact"=>null,"postal_code"=>"219067","address"=>"101 MERGUI ROAD #21-04 S(219067)","email"=>"","main_product_id"=>27,"main_sum_assured"=>"NA","currency_value"=>99000,"payment_frequency"=>0,"mode_of_payment"=>"Cash","annual_currency_value2"=>9900,"agent_code"=>"WK09-1101","submitted_date"=>"11/10/2016","owner_id"=>"S8560262D","owner_occupation"=>"","owner_dob"=>"4/20/1985","source"=>"Warm","annual_income_range"=>" ","fullname_first"=>"BINGCHEN LI","policy_term"=>"LIFE","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"BUSTAMI","life_firstname"=>"MARIA","firstname"=>"MARIA","lastname"=>"BUSTAMI","owner_occupation"=>"Freelance","owner_dob"=>"2/7/1973","contact"=>94513299,"postal_code"=>"440064","address"=>"Blk 64 Marine Drive #04-140","email"=>"maria123ang@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"1100","currency_value"=>564.64,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>564.64,"agent_code"=>"WK09-1162","submitted_date"=>"11/11/2016","owner_id"=>"S7372327B","owner_occupation"=>"Freelance","owner_dob"=>"2/7/1973","source"=>"Door Knocking ","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Lim","life_firstname"=>"Poh Kheng","firstname"=>"Poh Kheng","lastname"=>"Lim","owner_occupation"=>"Retired","owner_dob"=>"2/22/1951","contact"=>98522827,"postal_code"=>"1544","address"=>"Blk 60 Marine Drive #12-64","email"=>"limPKL@gmail.com","main_product_id"=>6,"main_sum_assured"=>"600","currency_value"=>827.81,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>827.81,"agent_code"=>"WK09-1162","submitted_date"=>"11/11/2016","owner_id"=>"S1795497J","owner_occupation"=>"Retired","owner_dob"=>"2/22/1951","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Bin Din","life_firstname"=>"Shahron","firstname"=>"Shahron","lastname"=>"Bin Din","owner_occupation"=>"Senior Store Keeper","owner_dob"=>"7/30/1961","contact"=>86483047,"postal_code"=>"440060","address"=>"Blk 60 Marine Drive #13-64","email"=>"","main_product_id"=>6,"main_sum_assured"=>"800","currency_value"=>572.4,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>572.4,"agent_code"=>"WK09-1162","submitted_date"=>"11/11/2016","owner_id"=>"S1469202I","owner_occupation"=>"Senior Store Keeper","owner_dob"=>"7/30/1961","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Wong","life_firstname"=>"Lian Sim Teresa","firstname"=>"Lian Sim Teresa","lastname"=>"Wong","owner_occupation"=>"Clerk","owner_dob"=>"1/5/1963","contact"=>96315530,"postal_code"=>"1544","address"=>"teresawo34@hotmail.com","email"=>"teresawo34@hotmail.com","main_product_id"=>6,"main_sum_assured"=>"800","currency_value"=>579.67,"payment_frequency"=>0,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>579.67,"agent_code"=>"WK09-1162","submitted_date"=>"11/11/2016","owner_id"=>"S1578934D","owner_occupation"=>"Clerk","owner_dob"=>"1/5/1963","source"=>"Door Knocking ","annual_income_range"=>"Below $30,000","fullname_first"=>"SENG CHONG HUNG","policy_term"=>"Lifetime","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Yap","life_firstname"=>"Ming Zhe","firstname"=>"Khar Koon","lastname"=>"Sim","owner_occupation"=>"Administration","owner_dob"=>"3/12/1978","contact"=>98368960,"postal_code"=>"644276","address"=>"276D Jurong West St 25 #09-13","email"=>"shenqiaojun@hotmail.com","main_product_id"=>24,"main_sum_assured"=>"0","currency_value"=>278.37,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>278.37,"agent_code"=>"MT06-1096","submitted_date"=>"11/11/2016","owner_id"=>"T1608797H","owner_occupation"=>"Son","owner_dob"=>"3/14/2016","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LAWRENCE LIM HUEY BENG","policy_term"=>"99","owner_gender"=>"Female","life_gender"=>"Male"),
  array("life_lastname"=>"Yap","life_firstname"=>"Ming Zhe","firstname"=>"Khar Koon","lastname"=>"Sim","owner_occupation"=>"Administration","owner_dob"=>"3/12/1978","contact"=>98368960,"postal_code"=>"644276","address"=>"276D Jurong West St 25 #09-13","email"=>"shenqiaojun@hotmail.com","main_product_id"=>99,"main_sum_assured"=>"0","currency_value"=>446.85,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>446.85,"agent_code"=>"MT06-1096","submitted_date"=>"11/11/2016","owner_id"=>"T1608797H","owner_occupation"=>"Son","owner_dob"=>"3/14/2016","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LAWRENCE LIM HUEY BENG","policy_term"=>"99","owner_gender"=>"Female","life_gender"=>"Male"),
  array("life_lastname"=>"Yap","life_firstname"=>"Ming Zhe","firstname"=>"Khar Koon","lastname"=>"Sim","owner_occupation"=>"Administration","owner_dob"=>"3/12/1978","contact"=>98368960,"postal_code"=>"644276","address"=>"276D Jurong West St 25 #09-13","email"=>"shenqiaojun@hotmail.com","main_product_id"=>12,"main_sum_assured"=>"150000","currency_value"=>1942.2,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>1942.2,"agent_code"=>"MT06-1096","submitted_date"=>"11/11/2016","owner_id"=>"T1608797H","owner_occupation"=>"Son","owner_dob"=>"3/14/2016","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LAWRENCE LIM HUEY BENG","policy_term"=>"99","owner_gender"=>"Female","life_gender"=>"Male"),
  array("life_lastname"=>"Sim","life_firstname"=>"Khar Koon","firstname"=>"Khar Koon","lastname"=>"Sim","owner_occupation"=>"Administration","owner_dob"=>"3/12/1978","contact"=>98368960,"postal_code"=>"644276","address"=>"276D Jurong West St 25 #09-13","email"=>"shenqiaojun@hotmail.com","main_product_id"=>12,"main_sum_assured"=>"150000","currency_value"=>142.2,"payment_frequency"=>1,"mode_of_payment"=>"Giro","annual_currency_value2"=>142.2,"agent_code"=>"MT06-1096","submitted_date"=>"11/11/2016","owner_id"=>"s7897148f","owner_occupation"=>"Administration","owner_dob"=>"3/12/1978","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"LAWRENCE LIM HUEY BENG","policy_term"=>"99","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"Tng","life_firstname"=>"Zhi Yong Daniel","firstname"=>"Zhi Yong Daniel","lastname"=>"Tng","owner_occupation"=>"","owner_dob"=>"1/24/1988","contact"=>null,"postal_code"=>"520724","address"=>"724 Tampines st 71 #02-147","email"=>"","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>173.17,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>2078.04,"agent_code"=>"MT06-1071","submitted_date"=>"11/11/2016","owner_id"=>"S8802186Z","owner_occupation"=>"","owner_dob"=>"1/29/1988","source"=>"Existing Client","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"CHEW CHEE CHIN","policy_term"=>"41","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Soh","life_firstname"=>"Cheng Chong","firstname"=>"Cheng Chong","lastname"=>"Soh","owner_occupation"=>"Police Coast Guard","owner_dob"=>"10/1/1910","contact"=>90045006,"postal_code"=>"530433","address"=>"433 Hougang Ave 8 #11-928","email"=>"kensohjz@yahoo.com.sg","main_product_id"=>87,"main_sum_assured"=>"2000000","currency_value"=>2652.02,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>2652.02,"agent_code"=>"KL05-1023","submitted_date"=>"11/11/2016","owner_id"=>"S8236462E","owner_occupation"=>"Police Coast Guard","owner_dob"=>"10/1/1910","source"=>"Warm","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"KEN  LIM CHEE YONG","policy_term"=>"31","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"-","life_firstname"=>"Sreenilayam Sreedhar Manju","firstname"=>"Sreenilayam Sreedhar Manju","lastname"=>"-","owner_occupation"=>"Housewife ","owner_dob"=>"5/29/1974","contact"=>94672893,"postal_code"=>"545119","address"=>"3 Rivervale Link #05-28 The Rivervale","email"=>"manu_sulu@hotmail.com","main_product_id"=>87,"main_sum_assured"=>"500000","currency_value"=>75.95,"payment_frequency"=>12,"mode_of_payment"=>"Giro","annual_currency_value2"=>911.4,"agent_code"=>"AJ03-1011","submitted_date"=>"11/11/2016","owner_id"=>"S7460973B","owner_occupation"=>"Housewife ","owner_dob"=>"5/29/1974","source"=>"Existing Client","annual_income_range"=>"Below $30,000","fullname_first"=>"ABDUL  JALIL BIN ABDUL RAZAK","policy_term"=>"22","owner_gender"=>"Female","life_gender"=>"Female"),
  array("life_lastname"=>"-","life_firstname"=>"Nabila Zahra Nizam ","firstname"=>"Nizam bin Abdul Rahman ","lastname"=>"-","owner_occupation"=>"IT Consultant ","owner_dob"=>"8/14/1983","contact"=>97514174,"postal_code"=>"641181","address"=>"Blk 181A Boon Lay Drive #03-616","email"=>"a.rahman.nizam@gmail.com","main_product_id"=>24,"main_sum_assured"=>"Plan 2","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"AJ03-1011","submitted_date"=>"11/11/2016","owner_id"=>"T1514464A","owner_occupation"=>"Child ","owner_dob"=>"5/13/2015","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ABDUL  JALIL BIN ABDUL RAZAK","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"-","life_firstname"=>"Nabila Zahra Nizam ","firstname"=>"Nizam bin Abdul Rahman ","lastname"=>"-","owner_occupation"=>"IT Consultant ","owner_dob"=>"8/14/1983","contact"=>97514174,"postal_code"=>"641181","address"=>"Blk 181A Boon Lay Drive #03-616","email"=>"a.rahman.nizam@gmail.com","main_product_id"=>99,"main_sum_assured"=>"Plan 2","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>160,"agent_code"=>"AJ03-1011","submitted_date"=>"11/11/2016","owner_id"=>"T1514464A","owner_occupation"=>"Child ","owner_dob"=>"5/13/2015","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ABDUL  JALIL BIN ABDUL RAZAK","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"-","life_firstname"=>"Naseerah Sarah Nizam ","firstname"=>"Nizam bin Abdul Rahman ","lastname"=>"-","owner_occupation"=>"IT Consultant ","owner_dob"=>"8/14/1983","contact"=>97514174,"postal_code"=>"641181","address"=>"Blk 181A Boon Lay Drive #03-616","email"=>"a.rahman.nizam@gmail.com","main_product_id"=>24,"main_sum_assured"=>"Plan 2","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"AJ03-1011","submitted_date"=>"11/11/2016","owner_id"=>"T1622588B","owner_occupation"=>"Child ","owner_dob"=>"7/25/2016","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ABDUL  JALIL BIN ABDUL RAZAK","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"-","life_firstname"=>"Naseerah Sarah Nizam ","firstname"=>"Nizam bin Abdul Rahman ","lastname"=>"-","owner_occupation"=>"IT Consultant ","owner_dob"=>"8/14/1983","contact"=>97514174,"postal_code"=>"641181","address"=>"Blk 181A Boon Lay Drive #03-616","email"=>"a.rahman.nizam@gmail.com","main_product_id"=>99,"main_sum_assured"=>"Plan 2","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>160,"agent_code"=>"AJ03-1011","submitted_date"=>"11/11/2016","owner_id"=>"T1622588B","owner_occupation"=>"Child ","owner_dob"=>"7/25/2016","source"=>"","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ABDUL  JALIL BIN ABDUL RAZAK","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"-","life_firstname"=>"Aoi Cho Li Xin ","firstname"=>"Cho Wai Pon","lastname"=>"-","owner_occupation"=>"Accounts","owner_dob"=>"4/2/1978","contact"=>94384746,"postal_code"=>"768102","address"=>"3 Canberra Drive #05-07 One Canberra","email"=>"bon_p@yahoo.com","main_product_id"=>24,"main_sum_assured"=>"Plan 2","currency_value"=>0,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>0,"agent_code"=>"AJ03-1011","submitted_date"=>"11/11/2016","owner_id"=>"T1631485J","owner_occupation"=>"Juvenile","owner_dob"=>"10/11/2016","source"=>"Existing Client","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ABDUL  JALIL BIN ABDUL RAZAK","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"-","life_firstname"=>"Aoi Cho Li Xin ","firstname"=>"Cho Wai Pon","lastname"=>"-","owner_occupation"=>"Accounts","owner_dob"=>"4/2/1978","contact"=>94384746,"postal_code"=>"768102","address"=>"3 Canberra Drive #05-07 One Canberra","email"=>"bon_p@yahoo.com","main_product_id"=>99,"main_sum_assured"=>"Plan 2","currency_value"=>160,"payment_frequency"=>1,"mode_of_payment"=>"Credit Card","annual_currency_value2"=>160,"agent_code"=>"AJ03-1011","submitted_date"=>"11/11/2016","owner_id"=>"T1631485J","owner_occupation"=>"Juvenile","owner_dob"=>"10/11/2016","source"=>"Existing Client","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"ABDUL  JALIL BIN ABDUL RAZAK","policy_term"=>"Lifetime","owner_gender"=>"Male","life_gender"=>"Female"),
  array("life_lastname"=>"KOH","life_firstname"=>"JIAN XIONG","firstname"=>"JIAN XIONG","lastname"=>"KOH","owner_occupation"=>"","owner_dob"=>"12/11/1985","contact"=>84991592,"postal_code"=>"530463","address"=>"463 HOUGANG AVENUE 10 #11-960","email"=>"","main_product_id"=>87,"main_sum_assured"=>"1000000","currency_value"=>151.6875,"payment_frequency"=>12,"mode_of_payment"=>"Cash","annual_currency_value2"=>1820.25,"agent_code"=>"JH12-1120","submitted_date"=>"11/11/2016","owner_id"=>"S8540716C","owner_occupation"=>"","owner_dob"=>"12/11/1985","source"=>"Others","annual_income_range"=>"$30,001-$50,000","fullname_first"=>"RAYNER GOH YONG EN","policy_term"=>"34","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"ONG","life_firstname"=>"SZE KID","firstname"=>"SZE KID","lastname"=>"ONG","owner_occupation"=>"SERVICE ENGINEER","owner_dob"=>"7/29/1968","contact"=>82188223,"postal_code"=>"542296","address"=>"296B COMPASSVALE CRESCENT #14-269","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1200","currency_value"=>570.42,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>570.42,"agent_code"=>"JH12-1120","submitted_date"=>"11/11/2016","owner_id"=>"S6882186Z","owner_occupation"=>"SERVICE ENGINEER","owner_dob"=>"7/29/1968","source"=>"Door Knocking ","annual_income_range"=>"$50,001-$75,000","fullname_first"=>"RAYNER GOH YONG EN","policy_term"=>"LIFETIME","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Chin","life_firstname"=>"Chee Kian","firstname"=>"Chee Kian","lastname"=>"Chin","owner_occupation"=>"Engineer","owner_dob"=>"7/30/1973","contact"=>91271516,"postal_code"=>"768122","address"=>"556 Miltonia Close #05-82","email"=>"wilson_cck@yahoo.com.sg","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.07,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.07,"agent_code"=>"KT09-1135","submitted_date"=>"11/14/2016","owner_id"=>"S7327685c","owner_occupation"=>"Engineer","owner_dob"=>"7/30/1973","source"=>"Referrals","annual_income_range"=>"$150,001-$250,000","fullname_first"=>"CHUA KOK KHANG (CAI GUOJIAN)","policy_term"=>"yearly","owner_gender"=>"Male","life_gender"=>"Male"),
  array("life_lastname"=>"Puthenpurackel Manuel","life_firstname"=>"Jacob ","firstname"=>"Jacob ","lastname"=>"Puthenpurackel Manuel","owner_occupation"=>"IT Project Manager","owner_dob"=>"12/21/1972","contact"=>null,"postal_code"=>"543290","address"=>"Apt 290c, Compassvale Crescent,04-32, Singapore","email"=>"","main_product_id"=>6,"main_sum_assured"=>"1400","currency_value"=>575.05,"payment_frequency"=>1,"mode_of_payment"=>"CPF MA","annual_currency_value2"=>575.05,"agent_code"=>"JH12-1132","submitted_date"=>"11/14/2016","owner_id"=>"s7262634f","owner_occupation"=>"IT Project Manager","owner_dob"=>"12/21/1972","source"=>"Door Knocking ","annual_income_range"=>"$75,001-$100,000","fullname_first"=>"PAN KE HSING","policy_term"=>"lifetime","owner_gender"=>"Male","life_gender"=>"Male")
);
try
{
  foreach($array as $put)
    { 
      $user_id = User::where('code', trim($put['agent_code']))->pluck('id');
      $prov_id = ProductRider::where('id', trim($put['main_product_id']))->pluck('provider_id');
      $main_id = ProductRider::where('id', trim($put['main_product_id']))->pluck('main_id');
      $row = new ProductionCases;
      $row->user_id = $user_id;
      $row->currency_id = 127;
      $row->currency_rate = 1;
      $row->currency2_id = 127;
      $row->currency2_rate = 1;
      $row->firstname = trim($put['firstname']);
      $row->lastname = trim($put['lastname']);
      $row->fullname = trim($put['lastname'].' '.$put['firstname']);
      $row->fullname_first = trim($put['firstname'].' '.$put['lastname']);
      $row->annual_income_range = trim($put['annual_income_range']);
      $row->life_firstname = trim($put['life_firstname']);
      $row->life_lastname = trim($put['life_lastname']);
      $row->owner_id = $put['owner_id'];
      $row->life_id = $put['owner_id'];
      $row->owner_occupation = trim($put['owner_occupation']);
      $row->life_occupation = trim($put['owner_occupation']);
      $row->owner_gender = trim($put['owner_gender']);
      $row->life_gender = trim($put['life_gender']);
      $row->owner_dob = Carbon::createFromFormat('d/m/Y', $put['owner_dob'])->format('Y-m-d');
      $row->life_dob = Carbon::createFromFormat('d/m/Y', $put['owner_dob'])->format('Y-m-d');
      $row->address = trim($put['address']);
      $row->postal_code = trim($put['postal_code']);
      $row->email = trim($put['email']);
      $row->provider_list = $prov_id;
      $row->main_product_id = $main_id;
      $row->conversion_rate = 1;
      $row->main_sum_assured = trim($put['main_sum_assured']);
      $row->currency = '1 SGD';
      $row->currency2 = '1 SGD';
      $row->currency_value = $put['currency_value'];
      $row->currency_value2 = $put['currency_value'];
      $row->conv_total = $put['currency_value'];
      $row->currency_value = $put['currency_value'];
      $row->life_total_amount = $put['currency_value'];
      $row->annual_currency_value2 = $put['annual_currency_value2'];
      $row->policy_term = $put['policy_term'];
      if($put['payment_frequency'] == 1)
      {
        $pf = 'Yearly';
      }
      elseif($put['payment_frequency'] == 12)
      {
        $pf = 'Monthly';
      }
      else
      {
        $pf = NULL;
      }
      $row->payment_frequency = $pf;
      $row->mode_of_payment = trim($put['mode_of_payment']);
      $row->ape = $put['annual_currency_value2'];
      $row->ape_wo_gst = $put['annual_currency_value2'];
      $row->source = $put['source'];
      $row->case = 'approved';
      $row->submitted_ho = 'Yes';
      $row->submitted_auth_id = 149;
      $row->submitted_date = Carbon::createFromFormat('d/m/Y', $put['submitted_date'])->format('Y-m-d H:i:s');
      $row->submission_date = Carbon::createFromFormat('d/m/Y', $put['submitted_date'])->format('Y-m-d H:i:s');
      $row->status = 1;
      $row->save();
    }
}
catch(\Exception $e)
{
  return dd($e->getMessage());
}
    

    return 'DONE';
  }
  

}
