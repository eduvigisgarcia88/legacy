<?php

namespace App\Http\Controllers;

use Response;
use \App\User;
use \App\LogUser;
use \App\AssignedSales;
use Request;
use Validator;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SalesAssistantController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    
	/**
     * Get Available Agents.
     *
     * @return Response
     */
    public function getAvailableAgents()
    {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        
        $ids = AssignedSales::all()->lists('sales_id');

        $rows = User::select('users.*')
                        	->where('users.usertype_id', '=', '8')
                        	->where('users.status', '!=', '2')
                        	->whereNotIn('users.id', $ids)
                            ->orderBy($result['sort'], $result['order'])
                            ->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Get User Agents.
     *
     * @return Response
     */
    public function getAgents()
    {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';
        $id = Request::input('id');
        
        $rows = AssignedSales::select('users.*', 'assigned_sales.id as assigned_sales_id','designations.designation', 'sales.id as sales_id')
                            ->join('users', 'users.id', '=', 'assigned_sales.sales_id')
                            ->join('sales', 'sales.user_id', '=', 'assigned_sales.sales_id')
                            ->join('designations', 'designations.id', '=', 'sales.designation_id')
                            ->where('assigned_sales.assistant_id', '=', $id)
                            ->where('users.usertype_id', '=', '8')
                            ->where('users.status', '!=', '2')
                            ->orderBy($result['sort'], $result['order'])
                            ->get();

        // return response (format accordingly)
        if(Request::ajax()) {
            //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Save available agents.
     *
     * @return Response
     */
    public function saveAgents()
    {

    	$validator = Validator::make(Request::all(), [
            'ids' => 'required|array',
            'row' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return Response::json(['type' => 'error', 'body' => 'failed']);
        }


    	$ids = Request::input('ids');

    	foreach ($ids as $id) {
    		AssignedSales::create(['sales_id' => $id, 'assistant_id' =>  Request::input('row')]);
            LogUser::create(['user_id' => $id, 'edit_id' => Auth::user()->id, 'activity' => 'Agent Assigned']);
    	}

    	return Response::json(['type' => 'success', 'body' => 'success']);
    }

    /**
     * Remove Multiple agents.
     *
     * @return Response
     */
    public function removeAgents()
    {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['type' => 'error', 'body' => 'Select Agent/s first.']);
        }

        foreach ($ids as $id) {
            AssignedSales::destroy($id);
            LogUser::create(['user_id' => $id, 'edit_id' => Auth::user()->id, 'activity' => 'Agent Remove']);
        }
        
        return Response::json(['type' => 'success', 'body' => 'Success']);
    }

    /**
     * Remove agent.
     *
     * @return Response
     */
    public function removeAgent()
    {
        $id = Request::input('id');

        AssignedSales::destroy($id);

        LogUser::create(['user_id' => $id, 'edit_id' => Auth::user()->id, 'activity' => 'Agent Remove']);
        
        return Response::json(['type' => 'success', 'body' => 'Success']);

    }
}
