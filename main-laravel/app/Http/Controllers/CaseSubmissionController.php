<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Input;
use View;
use Excel;
use Validator;
use Carbon;
use Auth;
use Paginator;
use Storage;
use DB;

use App\Permission;
use App\User;
use App\Sales;
use App\Group;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\BatchMonthUser;
use App\GroupComputation;
use App\SalesAdvisorComputation;
use App\SalesSupervisorComputation;
use App\Designation;
use App\Nationality;
use App\Policy;
use App\Batch;
use App\PayrollComputation;
use App\Product;
use App\ProductGross;
use App\ProductRider;
use App\ProductMain;
use App\ProductionCaseContact;
use App\ProductionCaseRider;
use App\PolicyContract;
use App\Provider;
use App\ProviderClassification;
use App\ProviderCategory;
use App\AssignedPolicy;
use App\Introducer;
use App\LogPolicy;
use App\ProductionCases;
use App\GIProductionCases;
use App\UploadFeed;
use App\SettingsRate;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;
use App\Settings;
use Redirect;


class CaseSubmissionController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }

    /**
     * Display a listing of the case submision.
     *
     * @return Response
     */
    public function indexCaseSubmission()
    {
      $result = $this->doListCaseSubmission();
      $this->data['rows'] = $result['rows'];
      $this->data['now'] = strval(Carbon::now()->format('Y-m-d'));

      if(Auth::user()->usertype_id == 8) {
        $this->data['sales_id'] = Auth::user()->id;

        $check_supervisor =SalesSupervisor::where('user_id', Auth::user()->id)->first();
        $this->data['supervisor'] = false;
        
        if ($check_supervisor) {
          $this->data['supervisor'] = true;
        }

      } else {
         $this->data['sales'] = $result['sales'];
      }
      
      $this->data['nations'] = Nationality::orderBy('country', 'asc')->get();

      $get_gis = ProviderClassification::where('is_gi', 1)->lists('provider_id');
      $this->data['providers'] = Provider::where('status', 1)->whereIn('id', $get_gis)->get();

      $this->data['sgd_value'] = SettingsRate::where('code','=','SGD')->first();
      $this->data['pages'] = $result['pages'];
      $this->data['case'] = 'pending';
      $this->data['currencies'] = SettingsRate::where('status','=','2')->get();

      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();
      $this->data['title'] = "GI Submission";

      if ( Auth::user()->usertype_id == '8') { //if sales user
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                          ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                          ->select('users.*','notifications_per_users.*','notifications.*')
                                          ->where(function($query) {
                                                $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                      ->where('notifications.usertype_id', '=', '1')
                                                      ->where('notifications_per_users.is_read', '=', '0');
                                            })->get();
      } else {
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                  ->select('users.*','notifications_per_users.*','notifications.*')
                                ->where(function($query) {
                                      $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                            ->where('notifications.usertype_id', '=', '2')
                                            ->where('notifications_per_users.is_read', '=', '0');
                                            
                                  })->get();
      }

      $this->data['settings'] = Settings::first();
      $this->data['noti_count'] = count($this->data['notification']);
      return view('policy-management.production.gi-case-submission.list', $this->data);
    }

    public function FixNames(){
      $row = ProductionCases::get();
      $row_two = GIProductionCases::get();
      
      foreach($row as $trimmed)
      {
        $trimmed->firstname = trim(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $trimmed->firstname));
        $trimmed->lastname = trim(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $trimmed->lastname));
        $trimmed->fullname = trim(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $trimmed->fullname));
        $trimmed->save();
      }

      foreach($row_two as $trimmed_two)
      {
        $trimmed_two->company_name = trim(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $trimmed_two->company_name));
        $trimmed_two->save();
      }

      return 'DONE';
    }

    public function doListCaseSubmission() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'gi_production_cases.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $start = null;
      $end = null;

      if ($status) {
        $start = Carbon::createFromFormat('M Y', $status)->startOfMonth();
        $end = Carbon::createFromFormat('M Y', $status)->endOfMonth();
      }
      // dd($status);
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        if (Auth::user()->usertype_id == 8) {

          $check_group = Group::where('user_id', Auth::user()->id)->first();
          if ($check_group) {
            $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

            $ids = [];
            // $ids[0] = Auth::user()->id;

            $get_supervisors_id = SalesSupervisor::where('group_id', $check_group->id)->lists('id');
            $get_supervisors = SalesSupervisor::where('group_id', $check_group->id)->where('user_id', '!=', Auth::user()->id)->lists('user_id');

            $get_advisors = SalesAdvisor::whereIn('supervisor_id', $get_supervisors_id)->lists('user_id');

            $ids = array_merge($ids, $get_supervisors->toArray());
            $ids = array_merge($ids, $get_advisors->toArray());
            // dd($ids);
          } else {
            $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

            $ids = [];
            $ctr = 0;
            // $ids[0] = Auth::user()->id;

            if ($check_supervisor) {
              $get_advisors = SalesAdvisor::where('supervisor_id', $check_supervisor->id)->lists('user_id');

              foreach ($get_advisors as $key => $value) {
                $ids[$ctr] = $value;
                $ctr += 1;
              }
            }
          }
        }

        if (Auth::user()->usertype_id == 8) {
          if ($check_supervisor) {
            $rows = GIProductionCases::select('gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id',
                    'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname',
                    'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date',
                    'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date',
                    'review_id.name as review_by', 'nationalities.nationality', 'gi_production_cases.policy_term',
                    'gi_production_cases.mode_of_payment', 'gi_production_cases.consent_telephone','gi_production_cases.case',
                    'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 
                    'gi_production_cases.source','gi_production_cases.annual_currency_value2', 'gi_production_cases.owner_dob', 
                    'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date',
                    'users.code as agent_code','gi_production_cases.submitted_ho', 'gi_production_cases.submitted_date', 
                    'submitted_id.name as submitted_by', 'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing',
                    'gi_production_cases.consent_servicing', 'auth_id.name as screened_by', 'gi_production_cases.currency', 'gi_production_cases.currency_id',
                    'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name',
                    'designations.designation', 'gi_production_cases.consent_mail', 'gi_production_cases.consent_sms', 'users.id as user_group', 'gi_production_cases.currency2',
                    'gi_production_cases.residency_status', 'gi_production_cases.residency_others', 'gi_production_cases.payment_frequency',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')
                                      ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                      ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                      ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                      ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                      ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                      ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                      ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                      ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                      ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                      ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->with('getSettingsRate')
                                      ->where(function($query) {
                                          $query->where('gi_production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search, $check_supervisor) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%');
                                        })
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                  ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                          }
                                        })
                                      ->where('gi_production_cases.user_id', Auth::user()->id)
                                      ->orderBy('users.name', 'asc')
                                      ->orderBy($result['sort'], $result['order'])
                                      ->get();

            $body = '';
            $curr_user = "NA";

            foreach ($rows as $key => $row) {
              $body .= '<tr ' . ($row->user_group != $curr_user ? 'style="border-top: 2px solid #60be54;"' : '') . ' class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                          '<td>' . ($row->user_group != $curr_user ? $row->name : '') . '</td>' .
                          '<td>' . ucwords(strtolower($row->lastname)) . '</td>';

              if ($row->type_of_insurance === "Others") {
                $body .= '<td>' . $row->type_of_insurance_others . '</td>';
              } else {
                $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
              }

              $body .= '<td>' . $row->main_sum_assured . '</td>' .
                          '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                          '<td>' . $row->payment_frequency . '</td>' .
                          '<td class="rightalign">' . number_format($row->ape, 2) . '</td>' .
                          '<td>' . $row->case . '</td>' .
                          '<td>' . ($row->selected_client == 1 ? 'YES' : 'NO') . '</td>' .
                          '<td class="rightalign">' .
                            '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                            (Auth::user()->id == $row->user_id ? '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' : '') .
                          '</td>' .
                        '</tr>';

              $curr_user = $row->user_group;
            }


            $rows = GIProductionCases::select('gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id',
                    'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname',
                    'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date',
                    'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date',
                    'review_id.name as review_by', 'nationalities.nationality', 'gi_production_cases.policy_term',
                    'gi_production_cases.mode_of_payment', 'gi_production_cases.consent_telephone','gi_production_cases.case',
                    'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 
                    'gi_production_cases.source','gi_production_cases.annual_currency_value2', 'gi_production_cases.owner_dob', 
                    'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date',
                    'users.code as agent_code','gi_production_cases.submitted_ho', 'gi_production_cases.submitted_date', 
                    'submitted_id.name as submitted_by', 'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing',
                    'gi_production_cases.consent_servicing', 'auth_id.name as screened_by', 'gi_production_cases.currency', 'gi_production_cases.currency_id',
                    'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name',
                    'designations.designation', 'gi_production_cases.consent_mail', 'gi_production_cases.consent_sms', 'users.id as user_group', 'gi_production_cases.currency2',
                    'gi_production_cases.residency_status', 'gi_production_cases.residency_others', 'gi_production_cases.payment_frequency',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')
                                      ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                      ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                      ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                      ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                      ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                      ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                      ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                      ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                      ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                      ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->with('getSettingsRate')
                                      ->where(function($query) {
                                          $query->where('gi_production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search, $check_supervisor) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%');
                                        })
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                  ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                          }
                                        })
                                      ->whereIn('gi_production_cases.user_id', $ids)
                                      ->orderBy('users.name', 'asc')
                                      ->orderBy($result['sort'], $result['order'])
                                      ->get();

            // $body = '';
            // $curr_user = "NA";

            foreach ($rows as $key => $row) {
              $body .= '<tr ' . ($row->user_group != $curr_user ? 'style="border-top: 2px solid #60be54;"' : '') . ' class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                          '<td>' . ($row->user_group != $curr_user ? $row->name : '') . '</td>' .
                          '<td>' . ucwords(strtolower($row->lastname)) . '</td>';

              if ($row->type_of_insurance === "Others") {
                $body .= '<td>' . $row->type_of_insurance_others . '</td>';
              } else {
                $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
              }

              $body .= '<td>' . $row->main_sum_assured . '</td>' .
                          '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                          '<td>' . $row->payment_frequency . '</td>' .
                          '<td class="rightalign">' . number_format($row->ape, 2) . '</td>' .
                          '<td>' . $row->case . '</td>' .
                          '<td>' . ($row->selected_client == 1 ? 'YES' : 'NO') . '</td>' .
                          '<td class="rightalign">' .
                            '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                            (Auth::user()->id == $row->user_id ? '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' : '') .
                          '</td>' .
                        '</tr>';

              $curr_user = $row->user_group;
            }

            $result['pages'] = '';
            $result['rows'] = $body;
            $result['sales'] = null;

            if(Request::ajax()) {
              return Response::json($result);
            } else {
              return $result;
            }

          } else {
            $rows = GIProductionCases::select('gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id',
                    'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname',
                    'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date',
                    'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date',
                    'review_id.name as review_by', 'nationalities.nationality', 'gi_production_cases.policy_term',
                    'gi_production_cases.mode_of_payment', 'gi_production_cases.consent_telephone','gi_production_cases.case',
                    'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 
                    'gi_production_cases.source','gi_production_cases.annual_currency_value2', 'gi_production_cases.owner_dob', 
                    'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date',
                    'users.code as agent_code','gi_production_cases.submitted_ho', 'gi_production_cases.submitted_date', 
                    'submitted_id.name as submitted_by', 'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing',
                    'gi_production_cases.consent_servicing', 'auth_id.name as screened_by', 'gi_production_cases.currency','gi_production_cases.currency_id',
                    'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name',
                    'designations.designation', 'gi_production_cases.consent_mail', 'gi_production_cases.consent_sms',
                    'gi_production_cases.residency_status', 'gi_production_cases.residency_others','gi_production_cases.currency2',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')
                                      ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                      ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                      ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                      ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                      ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                      ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                      ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                      ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                      ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                      ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                      ->with('getSettingsRate')
                                      ->where(function($query) {
                                          $query->where('gi_production_cases.status', '!=', 2);
                                        })
                                      ->where(function($query) use ($search, $check_supervisor) {
                                          if ($check_supervisor) {
                                            $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%');

                                          } else {
                                            $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                  ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                  ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                  ->orWhere('case', 'LIKE', '%' . $search . '%');
                                          }
                                        })
                                      ->where(function($query) use ($status, $start, $end) {
                                          if ($status) {
                                            $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                  ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                          }
                                        })
                                      ->where('gi_production_cases.user_id', Auth::user()->id)
                                      ->orderBy($result['sort'], $result['order'])
                                      ->paginate($per);

            $body = '';
            if(Request::ajax()) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">';

                if($check_supervisor) {
                  $body .= '<td>' . $row->name . '</td>';
                }

                $body .= '<td>' . $row->company_name . '</td>' .
                        '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status === "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . $row->case . '</td>' .
                        '<td>' . ($row->selected_client == 1 ? 'YES' : 'NO') . '</td>';

                $body .= '<td class="rightalign">' .
                        '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                      '</td>' .
                    '</tr>';
              }
            }
          }

        } else {
          $rows = GIProductionCases::select('nationalities.nationality', 'gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id', 'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date', 'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date', 'review_id.name as review_by',
                                            'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 'gi_production_cases.source','gi_production_cases.annual_currency_value2',
                                            'gi_production_cases.owner_dob', 'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date','users.code as agent_code','gi_production_cases.submitted_ho','gi_production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                            'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing', 'gi_production_cases.consent_servicing', 'auth_id.name as screened_by','gi_production_cases.currency','gi_production_cases.currency2','gi_production_cases.currency_id',
                                            'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name', 'designations.designation',
                                            'gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','nationalities.nationality', 'gi_production_cases.case','gi_production_cases.screened_status',
                  'gi_production_cases.residency_status', 'gi_production_cases.residency_others',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')
                                    ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->with('getSettingsRate')
                                    ->where(function($query) {
                                        $query->where('gi_production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhere('currency', 'LIKE', '%' . $search . '%')
                                                ->orWhere('mode_of_payment', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                ->orWhere('source', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

          $body = '';
          if(Request::ajax()) {
            if (Auth::user()->usertype_id == 1) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td class="hide">' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td class="hide">' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td class="hide">' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td class="hide">' . $row->review_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td class="hide">' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td class="hide">' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->selected_client == 1) {
                  $body .= '<td>YES</td>';
                } else {
                  $body .= '<td>NO</td>';
                }

                $body .= '<td class="rightalign">';
                if($row->assess_outcome && $row->review_outcome) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                }
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No"  || $row->case == "verified") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }

                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                      '</td>' .
                    '</tr>';
              }
            } else if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td class="hide">' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td class="hide">' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td class="hide">' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td class="hide">' . $row->review_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }

                $body .= '<td class="rightalign">';
                if (Auth::user()->usertype_id == 9) {
                  if($row->assess_outcome && !$row->review_outcome) {
                    $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                  }
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '</td>' .
                    '</tr>';
              }
            } else if(Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td class="hide">' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td class="hide">' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                $body .= '<td class="rightalign">';
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No" || $row->case == "verified") {
                  $body .= '<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                        '</td>' .
                    '</tr>';
              }
            } else {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td class="hide">' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td class="hide">' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td class="hide">' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td class="hide">' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if (Auth::user()->usertype_id == 2) {
                  if ($row->screened_status) {
                    $body .= '<td>' . $row->screened_status . '</td>';
                  } else {
                    $body .= '<td>N/A</td>';
                  }
                }

                $body .= '<td class="rightalign">';
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>';

                if (Auth::user()->usertype_id == 2) {
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No" || $row->case == "verified") {
                  $body .= '<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                }
                if (Auth::user()->usertype_id == 2) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                          '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">';
                }
                $body .= '</td>' .
                    '</tr>';
              }
            }
          }

        }
      } else {
        if (Auth::user()->usertype_id == 8) {
          $count = GIProductionCases::select('gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id', 'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date', 'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date', 'review_id.name as review_by','gi_production_cases.currency2',
                                    'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 'gi_production_cases.source','gi_production_cases.annual_currency_value2','gi_production_cases.currency_id',
                                    'gi_production_cases.owner_dob', 'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date','users.code as agent_code','gi_production_cases.submitted_ho','gi_production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                    'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing', 'gi_production_cases.consent_servicing', 'auth_id.name as screened_by','gi_production_cases.currency',
                                    'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name', 'designations.designation',
                                    'gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','nationalities.nationality', 'gi_production_cases.case',
                                    'gi_production_cases.residency_status', 'gi_production_cases.residency_others',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')   
                                    ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')      
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->with('getSettingsRate')
                                    ->where(function($query) {
                                        $query->where('gi_production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search, $check_supervisor) {
                                        if ($check_supervisor) {
                                          $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                ->orWhere('case', 'LIKE', '%' . $search . '%');

                                        } else {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                ->orWhere('case', 'LIKE', '%' . $search . '%');
                                        }
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->whereIn('gi_production_cases.user_id', $ids)
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
        } else {
          $count = GIProductionCases::select('gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id', 'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date', 'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date', 'review_id.name as review_by','gi_production_cases.currency2',
                                'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 'gi_production_cases.source','gi_production_cases.annual_currency_value2','gi_production_cases.currency_id',
                                'gi_production_cases.owner_dob', 'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date','users.code as agent_code','gi_production_cases.submitted_ho','gi_production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing', 'gi_production_cases.consent_servicing', 'auth_id.name as screened_by','gi_production_cases.currency',
                                'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name', 'designations.designation',
                                'gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','nationalities.nationality', 'gi_production_cases.case',
                                  'gi_production_cases.residency_status', 'gi_production_cases.residency_others',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client') 
                                    ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')           
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->with('getSettingsRate')
                                    ->where(function($query) {
                                        $query->where('gi_production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhere('currency', 'LIKE', '%' . $search . '%')
                                                ->orWhere('mode_of_payment', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                ->orWhere('source', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
        }
                                      
        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        if (Auth::user()->usertype_id == 8) {

          $rows = GIProductionCases::select('gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id', 'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date', 'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date', 'review_id.name as review_by','gi_production_cases.currency2',
                                          'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 'gi_production_cases.source','gi_production_cases.annual_currency_value2',
                                          'gi_production_cases.owner_dob', 'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date','users.code as agent_code','gi_production_cases.submitted_ho','gi_production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                          'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing', 'gi_production_cases.consent_servicing', 'auth_id.name as screened_by','gi_production_cases.currency','gi_production_cases.currency_id',
                                          'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name', 'designations.designation',
                                          'gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','nationalities.nationality', 'gi_production_cases.case',
                                  'gi_production_cases.residency_status', 'gi_production_cases.residency_others',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')
                                          ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')      
                                          ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                          ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                          ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                          ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                          ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                          ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                          ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                          ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                          ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                          ->with('getSettingsRate')
                                          ->where(function($query) {
                                              $query->where('gi_production_cases.status', '!=', 2);
                                            })
                                          ->where(function($query) use ($search, $check_supervisor) {
                                              if ($check_supervisor) {
                                                $query->where('users.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('lastname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                      ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                      ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                      ->orWhere('case', 'LIKE', '%' . $search . '%');

                                              } else {
                                                $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                      ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                      ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                      ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                      ->orWhere('case', 'LIKE', '%' . $search . '%');
                                              }
                                            })
                                          ->where(function($query) use ($status, $start, $end) {
                                              if ($status) {
                                                $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                      ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                              }
                                            })
                                          ->whereIn('gi_production_cases.user_id', $ids)
                                          ->orderBy($result['sort'], $result['order'])
                                          ->paginate($per);
                                          
          $body = '';
          if(Request::ajax()) {
            foreach ($rows as $key => $row) {
              $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">';

              if($check_supervisor) {
                $body .= '<td>' . $row->name . '</td>';
              }

              $body .= '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

              if ($row->residency_status === "Others") {
                $body .= '<td>' . $row->residency_others . '</td>';
              } else {
                $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
              }

              $body .= '<td>' . ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

              if ($row->type_of_insurance === "Others") {
                $body .= '<td>' . $row->type_of_insurance_others . '</td>';
              } else {
                $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
              }

              $body .= '<td class="rightalign">' . 
                      $row->main_sum_assured .
                      '</td>' .
                      '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                      '<td>' . $row->policy_term . '</td>' . 
                      '<td>' . $row->case . '</td>' .
                      '<td>' . ($row->selected_client == 1 ? 'YES' : 'NO') . '</td>';

              $body .= '<td class="rightalign">' .
                      '<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                      '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                    '</td>' .
                  '</tr>';
            }
          }

        } else {

          $rows = GIProductionCases::select('gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id', 'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date', 'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date', 'review_id.name as review_by','gi_production_cases.currency2',
                                            'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 'gi_production_cases.source','gi_production_cases.annual_currency_value2',
                                            'gi_production_cases.owner_dob', 'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date','users.code as agent_code','gi_production_cases.submitted_ho','gi_production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                            'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing', 'gi_production_cases.consent_servicing', 'auth_id.name as screened_by','gi_production_cases.currency','gi_production_cases.currency_id',
                                            'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name', 'designations.designation',
                                            'gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','nationalities.nationality', 'gi_production_cases.case','gi_production_cases.screened_status',
                                  'gi_production_cases.residency_status', 'gi_production_cases.residency_others',
                    'gi_production_cases.currency_value', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')
                                    ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')       
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->where(function($query) {
                                        $query->where('gi_production_cases.status', '!=', 2);
                                      })
                                    ->where(function($query) use ($search) {
                                          $query->where('lastname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('firstname', 'LIKE', '%' . $search . '%')
                                                ->orWhere('company_name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('nationality', 'LIKE', '%' . $search . '%')
                                                ->orWhere('residency_status', 'LIKE', '%' . $search . '%')
                                                ->orWhere('product_mains.name', 'LIKE', '%' . $search . '%')
                                                ->orWhere('main_sum_assured', 'LIKE', '%' . $search . '%')
                                                ->orWhere('case', 'LIKE', '%' . $search . '%')
                                                ->orWhere('currency', 'LIKE', '%' . $search . '%')
                                                ->orWhere('mode_of_payment', 'LIKE', '%' . $search . '%')
                                                ->orWhereRaw("concat(firstname, ' ', lastname) like '%$search%' ")
                                                ->orWhereRaw("concat(lastname, ' ', firstname) like '%$search%' ")
                                                ->orWhere('source', 'LIKE', '%' . $search . '%');
                                      })
                                    ->where(function($query) use ($status, $start, $end) {
                                        if ($status) {
                                          $query->where('gi_production_cases.created_at', '>=', $start->format('Y-m-d') . " 00:00:00")
                                                ->where('gi_production_cases.created_at', '<=', $end->format('Y-m-d') . " 00:00:00");
                                        }
                                      })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);

          $body = '';
          if(Request::ajax()) {
            if (Auth::user()->usertype_id == 1) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td class="hide">' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td class="hide">' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td class="hide">' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td class="hide">' . $row->review_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td class="hide">' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td class="hide">' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->selected_client == 1) {
                  $body .= '<td>YES</td>';
                } else {
                  $body .= '<td>NO</td>';
                }

                $body .= '<td class="rightalign">';
                if($row->assess_outcome && $row->review_outcome) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                }
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }

                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-duplicate" title="Duplicate Case"><i class="fa fa-files-o"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                      '</td>' .
                    '</tr>';
              }
            } else if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td class="hide">' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td class="hide">' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_outcome) {
                  $body .= '<td class="hide">' . $row->review_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->review_by) {
                  $body .= '<td class="hide">' . $row->review_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }

                $body .= '<td class="rightalign">';
                if (Auth::user()->usertype_id == 9) {
                  if($row->assess_outcome && !$row->review_outcome) {
                    $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-review" data-toggle="tooltip" title="ReviewOutcome"><i class="fa fa-book"></i></button>';
                  }
                }
                if($row->assess_outcome === null && $row->case === "rejected") {
                  $body .= '<button type="button" class="btn btn-xs btn-assessment btn-table" data-toggle="tooltip" title="AssessedOutcome"><i class="fa fa-check-square-o"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '</td>' .
                    '</tr>';
              }
            } else if(Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td class="hide">' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td class="hide">' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->screened_status) {
                  $body .= '<td>' . $row->screened_status . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                $body .= '<td class="rightalign">';
                if($row->assess_outcome === "False Positive" && $row->submitted_ho === "No") {
                  $body .= '<button type="button" class="btn btn-xs btn-table btn-submitted_ho" data-toggle="tooltip" title="Submit to HO"><i class="fa fa-check"></i></button>';
                }
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>' .
                        '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                        '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">' .
                        '</td>' .
                    '</tr>';
              }
            } else {
              foreach ($rows as $key => $row) {
                $body .= '<tr class="' . (($row->status == 1) ? '' : 'tr-disabled') . '" data-id="' . $row->id .'">' .
                      '<td>' . strtoupper($row->name) . '</td>' .
                      '<td>' . $row->company_name . '</td>' .
                      '<td>' . ($row->nationality ? $row->nationality : '') . '</td>';

                if ($row->residency_status == "Others") {
                  $body .= '<td>' . $row->residency_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->residency_status ? $row->residency_status : '') . '</td>';
                }

                $body .= '<td>' . ($row->owner_dob ?  date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : '') . '</td>';

                if ($row->type_of_insurance === "Others") {
                  $body .= '<td>' . $row->type_of_insurance_others . '</td>';
                } else {
                  $body .= '<td>' . ($row->type_of_insurance ? $row->type_of_insurance : '') . '</td>';
                }

                $cur = $row->getSettingsRate->code;
                $body .= '<td>' . $cur . '</td>';
                $body .= '<td class="rightalign">' . 
                        $row->main_sum_assured .
                        '</td>' .
                        '<td class="rightalign">' . number_format($row->currency_value2, 2) . '</td>' .
                        '<td>' . $row->policy_term . '</td>' . 
                        '<td>' . ($row->mode_of_payment ? $row->mode_of_payment : '') . '</td>' . 
                        '<td class="rightalign">' .  number_format($row->ape, 2) . '</td>' .
                        '<td>' . $row->agent_code . '</td>' .
                        '<td>' . date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') . '</td>' .
                        '<td>' . ($row->source ? $row->source : '') . '</td>' .
                        '<td>' . $row->case . '</td>' .
                        '<td>' . ($row->selected_client == 1 ? 'YES' : 'NO') . '</td>';

                if ($row->screen_date) {
                  $body .= '<td>' . date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->screened_by) {
                  $body .= '<td>' . $row->screened_by . '</td>';
                } else {
                  $body .= '<td>N/A</td>';
                }
                if ($row->assess_outcome) {
                  $body .= '<td class="hide">' . $row->assess_outcome . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->assess_by) {
                  $body .= '<td class="hide">' . $row->assess_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_ho) {
                  $body .= '<td class="hide">' . $row->submitted_ho . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_date) {
                  $body .= '<td class="hide">' . date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if ($row->submitted_by) {
                  $body .= '<td class="hide">' . $row->submitted_by . '</td>';
                } else {
                  $body .= '<td class="hide">N/A</td>';
                }
                if (Auth::user()->usertype_id == 2) {
                  if ($row->screened_status) {
                    $body .= '<td>' . $row->screened_status . '</td>';
                  } else {
                    $body .= '<td>N/A</td>';
                  }
                }

                $body .= '<td class="rightalign">';
                $body .= '&nbsp;<button type="button" class="btn btn-xs btn-view btn-table btn-view" title="View"><i class="fa fa-eye"></i></button>';

                if (Auth::user()->usertype_id == 2) {
                  $body .= '&nbsp;<button type="button" class="btn btn-xs btn-table btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>' .
                          '&nbsp;<input id="checkbox" type="checkbox" value="' . $row->id . '">';
                }
                $body .= '</td>' .
                    '</tr>';
              }
            }
          }

        }
      }

      $sales = User::where(function($query) {
                          $query->where('usertype_id', '=', '8')
                                ->where('status', '=', 1);
                      })->with('salesInfo')->get();

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $body;
          $result['sales'] = $sales->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['sales'] = $sales;
          return $result;
      }
    
}

    public function getMainPlan() {

      $get_plan = ProductMain::with('getCaseRiders')
                              ->where('provider_id', '=', Request::input('id'))
                              ->orderBy('name', 'asc')
                              ->get();

      $rows = '';

      if ($get_plan->count() > 0) {
        $ctr = 0;
        $rows = '<option class="hide" value="">Select Plan:</option>';
        foreach ($get_plan as $key => $field) {
          if ($field->getCaseRiders->count() > 0) {
            $ctr += 1;
            $rows .= '<option value="' . $field->id . '">' . $field->name . '</option>';
          }
        }
        if ($ctr == 0) {
          $rows = '<option class="hide" value="">0 Plan/s Found.</option>';
        }

      } else {
        $rows = '<option class="hide" value="">0 Plan/s Found.</option>';
      }

      return Response::json($rows);
    }

    public function getRate() {

      $rows = ProductMain::find(Request::input('id'));

      $aviva = ProductMain::where(function($query) {
                                  $query->where('name', 'LIKE', '%MyShield Plan 1%')
                                          ->orWhere('name', 'LIKE', '%MyShield Plan 2%')
                                          ->orWhere('name', 'LIKE', '%MyShield Plan 3%')
                                          ->orWhere('name', 'LIKE', '%MyShield Standard Plan%')
                                          ->orWhere('name', 'LIKE', '%MyHealth Plus Option A%')
                                          ->orWhere('name', 'LIKE', '%MyHealth Plus Option C%');
                                  })
                          ->where('provider_id', 1)
                          ->lists('id');

      $axa = ProductMain::where(function($query) {
                                  $query->where('name', 'LIKE', '%AXA Shield Plan A%')
                                          ->orWhere('name', 'LIKE', '%AXA Shield Plan B%')
                                          ->orWhere('name', 'LIKE', '%AXA Shield Standard Plan%');
                                  })
                          ->where('provider_id', 2)
                          ->lists('id');

      $ntuc = ProductMain::where(function($query) {
                                  $query->where('name', 'LIKE', '%PrimeShield%');
                                  })
                          ->where('provider_id', 8)
                          ->lists('id');

      $ids = array_merge($aviva->toArray(), $axa->toArray());
      $ids = array_merge($ids, $ntuc->toArray());

      $check = ProductMain::whereIn('id', $ids)->where('id', $rows->id)->first();

      $gst = 0;

      if ($check) {
        $gst = .07;
      }
      
      $result['list'] = $rows->toArray();
      $result['gst'] = $gst;

      return Response::json($result);
    }

    public function getRider() {

      $rows = ProductRider::where('main_id', '=', Request::input('id'))->orderBy('name', 'asc')->get();
      

      $row = '<option value="" class="hide">Select Plan</option>';

      foreach ($rows as $key => $field) {
        $row .= '<option value="' . $field->id . '">' . $field->name . '</option>';
      }

      return Response::json($row);
    }

    public function getRiderPlan() {

      $rows = Product::where('provider_id', Request::input('id'))
                      ->where('classification', '=', 'Rider')
                      ->get();


      $result['lists'] = $rows->toArray();

      return Response::json($result);

    }

    public function exportCase() {


      $selected_clients = Request::input('selected_client_on');
      $name = 'GI-CASE-' . Carbon::now()->format('Y-m-d') . ($selected_clients == 1 ? '-SC' : '');
      Excel::create($name, function($excel) use ($name, $selected_clients) {
          $rows = GIProductionCases::select('nationalities.nationality', 'gi_production_cases.id','gi_production_cases.user_id', 'gi_production_cases.life_id', 'gi_production_cases.owner_id', 'gi_production_cases.firstname', 'gi_production_cases.lastname', 'gi_production_cases.annual_income_range','gi_production_cases.assess_outcome','gi_production_cases.assess_date', 'assess_id.name as assess_by','gi_production_cases.review_outcome','gi_production_cases.review_date', 'review_id.name as review_by','gi_production_cases.currency2',
                                            'gi_production_cases.policy_term', 'gi_production_cases.mode_of_payment', 'product_mains.name as product_name', 'gi_production_cases.ape', 'gi_production_cases.created_at', 'gi_production_cases.source','gi_production_cases.annual_currency_value2',
                                            'gi_production_cases.owner_dob', 'gi_production_cases.life_dob', 'gi_production_cases.main_sum_assured', 'gi_production_cases.screen_date','users.code as agent_code','gi_production_cases.submitted_ho','gi_production_cases.submitted_date', 'submitted_id.name as submitted_by',
                                            'gi_production_cases.currency_value2', 'gi_production_cases.consent_marketing', 'gi_production_cases.consent_servicing', 'auth_id.name as screened_by','gi_production_cases.currency','gi_production_cases.currency_id',
                                            'gi_production_cases.life_firstname', 'gi_production_cases.life_lastname', 'gi_production_cases.status', 'users.name', 'designations.designation',
                                            'gi_production_cases.consent_mail','gi_production_cases.consent_sms','gi_production_cases.consent_telephone','nationalities.nationality', 'gi_production_cases.case','gi_production_cases.screened_status',
                  'gi_production_cases.residency_status', 'gi_production_cases.residency_others', 'gi_production_cases.type_of_insurance', 'gi_production_cases.type_of_insurance_others', 'providers.name as provider_name', 'gi_production_cases.company_name', 'gi_production_cases.selected_client')                                 
                                    ->leftJoin('users as auth_id', 'auth_id.id', '=', 'gi_production_cases.screen_auth_id')
                                    ->leftJoin('users as review_id', 'review_id.id', '=', 'gi_production_cases.review_auth_id')
                                    ->leftJoin('users as assess_id', 'assess_id.id', '=', 'gi_production_cases.assess_auth_id')
                                    ->leftJoin('providers', 'providers.id', '=', 'gi_production_cases.provider_list')
                                    ->leftJoin('nationalities', 'nationalities.id', '=', 'gi_production_cases.nationality_id')
                                    ->leftJoin('users as submitted_id', 'submitted_id.id', '=', 'gi_production_cases.submitted_auth_id')
                                    ->leftJoin('product_mains', 'product_mains.id', '=', 'gi_production_cases.main_product_id')
                                    ->leftJoin('users', 'users.id', '=', 'gi_production_cases.user_id')
                                    ->leftJoin('sales', 'sales.user_id', '=', 'users.id')
                                    ->leftJoin('designations', 'designations.id', '=', 'sales.designation_id')
                                    ->with('getSettingsRate')
                                    ->where(function($query) {
                                        $query->where('gi_production_cases.case', 'LIKE', '%%');
                                      })
                                    ->where(function($query) use ($selected_clients){
                                      if($selected_clients)
                                      {
                                        $query->where('gi_production_cases.selected_client', 1);
                                      }
                                    })
                                    ->where('gi_production_cases.status', '=', 1)
                                    ->orderBy('firstname', 'asc')
                                    ->get();

        $excel->sheet($name, function($sheet) use ($rows) {

            $row_cell = 1;
            if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
              $sheet->appendRow(array(
                'POLICY OWNER NAME',
                'POLICY OWNER ID',
                'POLICY OWNER NATIONALITY',
                'POLICY OWNER RESIDENCY STATUS',
                'POLICY OWNER DATE OF BIRTH',
                'MAIN PLAN',
                'CURRENCY CODE',
                'SUM ASSURED',
                'PREMIUM AMOUNT',
                'POLICY TERM',
                'MODE OF PAYMENT',
                'ANNUALIZED PREMIUM',
                'AGENT CODE',
                'DATE SUBMITTED',
                'SOURCE',
                'SUBMISSION STATUS',
                'SCREENED DATE',
                'SCREENED STATUS',
                'SCREENED BY',
                'ASSESSED OUTCOME',
                'ASSESSED DATE',
                'ASSESSED BY',
                'REVIEWED OUTCOME',
                'REVIEWED DATE',
                'REVIEWED BY',
                'SELECTED CLIENT'
              ));


              $sheet->cell('A' . $row_cell . ':Y' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });

            } else if (Auth::user()->usertype_id == 1 || Auth::user()->usertype_id == 2 || Auth::user()->usertype_id == 3 || Auth::user()->usertype_id == 4) {
              $sheet->appendRow(array(
                'POLICY OWNER NAME',
                'POLICY OWNER ID',
                'POLICY OWNER NATIONALITY',
                'POLICY OWNER RESIDENCY STATUS',
                'POLICY OWNER DATE OF BIRTH',
                'MAIN PLAN',
                'CURRENCY CODE',
                'SUM ASSURED',
                'PREMIUM AMOUNT',
                'POLICY TERM',
                'MODE OF PAYMENT',
                'APE',
                'SOURCE',
                'AGENT CODE',
                'DATE SUBMITTED',
                'SUBMISSION STATUS',
                'SCREENED DATE',
                'SCREENED STATUS',
                'SCREENED BY',
                'ASSESSED OUTCOME',
                'ASSESSED DATE',
                'ASSESSED BY',
                'REVIEWED OUTCOME',
                'REVIEWED DATE',
                'REVIEWED BY',
                'SUBMITTED TO HO',
                'SUBMITTED TO HO DATE',
                'SUBMITTED TO HO BY',
                'SELECTED CLIENT'
              ));

              $sheet->cell('A' . $row_cell . ':AC' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('center');
                $cells->setValignment('middle');
              });
            }

            foreach ($rows as $key => $row) {

              $curr = explode(" ", $row->currency2);
              $currency = "N/A";
              // if (count($curr) > 0) {
              //   $currency = $curr[1];
              // }

              $row_cell += 1;
              $residency = '';
              if($row->residency_status == "Others") {
                $residency = $row->residency_others;
              }
              else {
                $residency = $row->residency_status;
              }

              $insurance = '';
              if($row->type_of_insurance == "Others") {
                $residency = $row->type_of_insurance_others;
              }
              else {
                $residency = $row->type_of_insurance;
              }


              if (Auth::user()->usertype_id == 9 || Auth::user()->usertype_id == 10) {
                $sheet->appendRow(array(
                    $row->company_name,
                    $row->owner_id,
                    $row->nationality,
                    $residency,
                    ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : ''),
                    $insurance,
                    $row->getSettingsRate->code,
                    $row->main_sum_assured,
                    $row->currency_value2,
                    $row->policy_term,
                    $row->mode_of_payment,
                    $row->ape,
                    $row->agent_code,
                    ($row->created_at ? date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') : ''),
                    $row->source,
                    $row->case,
                    ($row->screen_date ? date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') : ''),
                    $row->screened_status,
                    $row->screened_by,
                    $row->assess_outcome,
                    ($row->assess_date ? date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') : ''),
                    $row->assess_by,
                    $row->review_outcome,
                    ($row->review_date ? date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') : ''),
                    $row->review_by,
                    ($row->selected_client == 1 ? 'YES' : 'NO')
                ));

                $sheet->cell('A' . $row_cell . ':Y' . $row_cell, function($cells) {
                  $cells->setAlignment('center');
                  $cells->setValignment('middle');
                });

              } else {
                // dd($row);
                $sheet->appendRow(array(
                    $row->company_name,
                    $row->owner_id,
                    $row->nationality,
                    $residency,
                    ($row->owner_dob ? date_format(date_create(substr($row->owner_dob, 0,10)),'d/m/Y') : ''),
                    $insurance,
                    $row->getSettingsRate->code,
                    $row->main_sum_assured,
                    $row->currency_value2,
                    $row->policy_term,
                    $row->mode_of_payment,
                    $row->ape,
                    $row->source,
                    $row->agent_code,
                    ($row->created_at ? date_format(date_create(substr($row->created_at, 0,10)),'d/m/Y') : ''),
                    $row->case,
                    ($row->screen_date ? date_format(date_create(substr($row->screen_date, 0,10)),'d/m/Y') : ''),
                    $row->screened_status,
                    $row->screened_by,
                    $row->assess_outcome,
                    ($row->assess_date ? date_format(date_create(substr($row->assess_date, 0,10)),'d/m/Y') : ''),
                    $row->assess_by,
                    $row->review_outcome,
                    ($row->review_date ? date_format(date_create(substr($row->review_date, 0,10)),'d/m/Y') : ''),
                    $row->review_by,
                    $row->submitted_ho,
                    ($row->submitted_date ? date_format(date_create(substr($row->submitted_date, 0,10)),'d/m/Y') : ''),
                    $row->submitted_by,
                    ($row->selected_client == 1 ? 'YES' : 'NO')
                ));

                // $sheet->setColumnFormat(array(
                //     'I' => '#,##0.00',
                //     'L' => '#,##0.00',
                // ));

                $sheet->cell('A' . $row_cell . ':AC' . $row_cell, function($cells) {
                  $cells->setAlignment('center');
                  $cells->setValignment('middle');
                });
              }
            }

      });

      })->download('xls');
    }

    public function caseView() {
        $id = Request::input('id');
        $result['same'] = false;
        $result['backdate'] = false;
        $result['owner_backdate'] = '';

        // prevent non-admin from viewing deactivated row
        $result['row'] = GIProductionCases::with('getContact')
                              ->with('getSum')
                              ->with('getProvider')
                              ->with('getProvider.getProducts')
                              ->with('getProvider.getProducts.getCaseRiders')
                              ->with('getMainRiders')
                              ->with('getSum.getCaseRider')->find($id);

        if ($result['row']) {
          $row = $result['row'];

          $result['owner_backdate'] = $row->owner_age;
          if ($row->firstname === $row->life_firstname && $row->lastname === $row->life_lastname
            && $row->owner_id === $row->life_id && $row->owner_occupation === $row->life_occupation
            && $row->owner_gender === $row->life_gender && $row->owner_dob === $row->life_dob 
            && $row->owner_age === $row->life_age && $row->owner_bday === $row->life_bday) {

            $result['same'] = true;
          }

          if ($row->owner_age && $row->life_age) {
            $result['backdate'] = true;
          }

          $getSum = ProductionCaseRider::where('production_id', $row->id)->get();
          // dd($getSum);

        }

        if($result['row']) {
            return Response::json($result);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function caseViewAll() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $result['row'] = GIProductionCases::with('getContact')
                              ->with('getSum')
                              ->with('getProvider')
                              ->with('getProvider.getProducts')
                              ->with('getProvider.getProducts.getCaseRiders')
                              ->with('getMainRiders')
                              ->with('getSum.getCaseRider')->find($id);
                              // dd($result);
        if($result['row']) {
            return Response::json($result);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function caseSave() {
      $new = true;

      $input = Input::all();
      // dd($input);
      // check if an ID is passed
    
      if(array_get($input, 'id')) {

        // get the user info
        $row = GIProductionCases::find(array_get($input, 'id'));

        if(is_null($row)) {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
        // this is an existing row
        $new = false;
      }
      
      $rules = [
          'user_sales_id' => (Auth::user()->usertype_id == 8 ? '' : 'required'),
          'currency_value2' => 'required|numeric',
          'currency_id' => 'required',
          'mode_others' => (array_get($input, 'mode_of_payment') == "Others" ? 'required' : ''),
          'payment_frequency' => 'required',
          'mode_of_payment' => 'required',
          // 'lastname' => 'required',
          // 'firstname' => 'required',
          'nationality_id' => 'required',
          'residency_status' => 'required',
          'owner_dob' => 'required|date_format:"d/m/Y"',
          'source' => 'required',
          'provider_list' => 'required',
          'nric' => 'required',
          'contact_no' => 'required',
          'ape' => 'required|numeric',
          'company_name' => 'required',
          'submission_date' => 'required|date_format:"d/m/Y"',
          'type_of_insurance' => 'required',
      ];

      if (array_get($input, 'residency_status')) {
        if (array_get($input, 'residency_status') == "Others") {
          $rules['residency_others'] = 'required';
        }
      }

      if (array_get($input, 'upload_2')) {
        $rules['upload_name_2'] = 'required';
      }

      if (array_get($input, 'upload_3')) {
        $rules['upload_name_3'] = 'required';
      }

      if (array_get($input, 'upload_4')) {
        $rules['upload_name_4'] = 'required';
      }

      if (array_get($input, 'upload_5')) {
        $rules['upload_name_5'] = 'required';
      }

      // if ($new) {
      //   $rules['upload_3'] = 'required';
      //   $rules['upload_name_3'] = 'required';
      // } else {
      //   if (!$row->upload_3) {
      //     $rules['upload_3'] = 'required';
      //     $rules['upload_name_3'] = 'required';
      //   } else if (array_get($input, 'upload_remove_3')) {
      //     $rules['upload_3'] = 'required';
      //     $rules['upload_name_3'] = 'required';
      //   }
      // }

      // field name overrides
      $names = [

      ];

      $messages = [

      ];
        
      // do validation
      if((Auth::user()->usertype_id == 1) || (Auth::user()->usertype_id == 2) || (Auth::user()->usertype_id == 3))
      {
       
      }
      else
      {
        $validator = Validator::make(Input::all(), $rules, $messages);
        $validator->setAttributeNames($names);
      
      // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
      }
      

      // create model if new
      if($new) {
        $row = new GIProductionCases;
        $row->user_id = (Auth::user()->usertype_id == 8 ? Auth::user()->id : array_get($input, 'user_sales_id'));

        $row->firstname = array_get($input, 'firstname');
        $row->lastname = array_get($input, 'lastname');

        $row->payment_frequency = array_get($input, 'payment_frequency');
        $row->currency_value2 = array_get($input, 'currency_value2');
        $row->currency2 = array_get($input, 'currency2');
        $row->currency_id = array_get($input, 'currency_id');
        $row->annual_currency_value2 = (array_get($input, 'currency_value2') * 12);
        $row->mode_of_payment = array_get($input, 'mode_of_payment');

        if (array_get($input, 'mode_of_payment') == "Others") {
          $row->mode_others = array_get($input, 'mode_others');
        }

        // if (array_get($input, 'firstname') && array_get($input, 'lastname')) {
        //   $row->fullname = array_get($input, 'lastname') . ' ' . array_get($input, 'firstname');
        //   $row->fullname_first = array_get($input, 'firstname') . ' ' . array_get($input, 'lastname');
        // } else if (array_get($input, 'firstname') && !array_get($input, 'lastname')) {
        //   $row->fullname = array_get($input, 'firstname');
        //   $row->fullname_first = array_get($input, 'firstname');
        // } else if (!array_get($input, 'firstname') && array_get($input, 'lastname')) {
        //   $row->fullname = array_get($input, 'lastname');
        //   $row->fullname_first = array_get($input, 'lastname');
        // } else {
        //   $row->fullname = null;
        //   $row->fullname_first = null;
        // }

        $row->nationality_id = array_get($input, 'nationality_id');
        $row->residency_status = array_get($input, 'residency_status');

        if (array_get($input, 'residency_status') == "Others") {
          $row->residency_others = array_get($input, 'residency_others');
        } else {
          $row->residency_others = null;
        }

        if (array_get($input, 'owner_dob')) {
          $row->owner_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_dob'));
        } else {
          $row->owner_dob = null;
        }  
        

        $row->ape = array_get($input, 'ape');
        $row->source = array_get($input, 'source');
        $row->company_name = array_get($input, 'company_name');
        $row->provider_list = array_get($input, 'provider_list');
        $row->nric = array_get($input, 'nric');
        $row->contact_no = array_get($input, 'contact_no');
        $row->insurance_company = array_get($input, 'insurance_company');
        $row->type_of_insurance = array_get($input, 'type_of_insurance');

        if (array_get($input, 'type_of_insurance') == "Others") {
          $row->type_of_insurance_others = array_get($input, 'type_of_insurance_others');
        }

        if (array_get($input, 'submission_date')) {
          $row->submission_date = Carbon::createFromFormat('d/m/Y', array_get($input, 'submission_date'));
        } else {
          $row->submission_date = null;
        }

        // save model
        $row->save();

        if (Request::hasFile('upload_2')) {
          $file = Request::file('upload_2');

          $destinationPath = storage_path('gi-case');
          $file2naming = array_get($input, 'upload_name_2');
          $filename_orig = $file->getClientOriginalName();

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-two.'.$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_2   = $row->id . '-two.' . $extension;
            $row->upload_2_title = $file2naming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_3')) {
          $file = Request::file('upload_3');

          $destinationPath = storage_path('gi-case');
          $file3naming = array_get($input, 'upload_name_3');
          $filename_orig = $file->getClientOriginalName();

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-three.'.$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_3   = $row->id . '-three.' . $extension;
            $row->upload_3_title = $file3naming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_4')) {
          $file = Request::file('upload_4');

          $destinationPath = storage_path('gi-case');
          $file4naming = array_get($input, 'upload_name_4');
          $filename_orig = array_get($input, 'upload_name');

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-four.'.$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_4   = $row->id . '-four.' . $extension;
            $row->upload_4_title = $file4naming . '.' . $extension;
            $row->save();
          }
        }

        if (Request::hasFile('upload_5')) {
          $file = Request::file('upload_5');

          $destinationPath = storage_path('gi-case');
          $file5naming = array_get($input, 'upload_name_5');
          $filename_orig = $file->getClientOriginalName();

          $temp = explode(".", $filename_orig);
          $extension = end($temp);

          $filename = $row->id. '-five.' .$extension;
          $upload_success = $file->move($destinationPath, $filename);

          if( $upload_success)  {
            $row->upload_5   = $row->id . '-five.' . $extension;
            $row->upload_5_title = $file5naming . '.' . $extension;
            $row->save();
          }
        }

        //trigger notification
        $row_noti = new Notification;
        $row_noti->user_id =  Auth::user()->id;
        $row_noti->activity= " added ".$row->firstname." ".$row->lastname."'s production case!";
        $row_noti->controller =  "CaseSubmissionController@notification";
           
        if (Auth::user()->usertype_id == '8') { //if sales user
          $row_noti->usertype_id = "2"; //sales user
        } else{
          $row_noti->usertype_id = "1"; //system user
        }

        $row_noti->save();

        //save to per user
        $row_noti  = Notification::orderBy('id', 'desc')->first();
        if (Auth::user()->usertype_id == '8') {
          $row_user = User::where(function($query) {
                                   $query->where('status', '=', '1')
                                   ->where('usertype_id', '<=', '7');
                                  })->get();
        } else {
          $row_user = User::where(function($query) {
                                   $query->where('status', '=', '1')
                                   ->where('usertype_id', '=', '8');
                                  })->get();
        }

        foreach ($row_user as $key => $field) {      
          $rows = new NotificationPerUser;
          $rows->user_id = $field->id;
          $rows->notification_id = $row_noti->id;
          $rows->save();
        }
      } else {
        $row->user_id = (Auth::user()->usertype_id == 8 ? Auth::user()->id : array_get($input, 'user_sales_id'));

        $row->firstname = array_get($input, 'firstname');
        $row->lastname = array_get($input, 'lastname');

        $row->provider_list = array_get($input, 'provider_list');
        $row->payment_frequency = array_get($input, 'payment_frequency');
        $row->currency_value2 = array_get($input, 'currency_value2');
        $row->currency2 = array_get($input, 'currency2');
        $row->currency_id = array_get($input, 'currency_id');
        $row->annual_currency_value2 = (array_get($input, 'currency_value2') * 12);
        $row->mode_of_payment = array_get($input, 'mode_of_payment');

        // if (array_get($input, 'firstname') && array_get($input, 'lastname')) {
        //   $row->fullname = array_get($input, 'lastname') . ' ' . array_get($input, 'firstname');
        //   $row->fullname_first = array_get($input, 'firstname') . ' ' . array_get($input, 'lastname');
        // } else if (array_get($input, 'firstname') && !array_get($input, 'lastname')) {
        //   $row->fullname = array_get($input, 'firstname');
        //   $row->fullname_first = array_get($input, 'firstname');
        // } else if (!array_get($input, 'firstname') && array_get($input, 'lastname')) {
        //   $row->fullname = array_get($input, 'lastname');
        //   $row->fullname_first = array_get($input, 'lastname');
        // } else {
        //   $row->fullname = null;
        //   $row->fullname_first = null;
        // }

        $row->nationality_id = array_get($input, 'nationality_id');
        $row->residency_status = array_get($input, 'residency_status');

        if (array_get($input, 'residency_status') == "Others") {
          $row->residency_others = array_get($input, 'residency_others');
        } else {
          $row->residency_others = null;
        }


        if (array_get($input, 'mode_of_payment') == "Others") {
          $row->mode_others = array_get($input, 'mode_others');
        }

        if (array_get($input, 'owner_dob')) {
          $row->owner_dob = Carbon::createFromFormat('d/m/Y', array_get($input, 'owner_dob'));
        } else {
          $row->owner_dob = null;
        }
        
        $row->ape = array_get($input, 'ape');
        $row->source = array_get($input, 'source');
        $row->company_name = array_get($input, 'company_name');
        $row->nric = array_get($input, 'nric');
        $row->contact_no = array_get($input, 'contact_no');
        $row->insurance_company = array_get($input, 'insurance_company');
        $row->type_of_insurance = array_get($input, 'type_of_insurance');

        if (array_get($input, 'type_of_insurance') == "Others") {
          $row->type_of_insurance_others = array_get($input, 'type_of_insurance_others');
        }

        if (array_get($input, 'submission_date')) {
          $row->submission_date = Carbon::createFromFormat('d/m/Y', array_get($input, 'submission_date'));
        } else {
          $row->submission_date = null;
        }
        // save model
        $row->save();

        if (!Request::input('upload_remove_2') || (Request::input('upload_remove_2') == "yes" && Request::hasFile('upload_2'))) {
          if (Request::hasFile('upload_2')) {
            $file2 = Request::file('upload_2');

            $destinationPath = storage_path('gi-case');
            $file2naming = array_get($input, 'upload_name_2');
            $file2name_orig = $file2->getClientOriginalName();

            $temp = explode(".", $file2name_orig);
            $extension = end($temp);

            $file2name = $row->id . '-two.' . $extension;
            $upload_success = $file2->move($destinationPath, $file2name);

            if( $upload_success)  {
              $row->upload_2   = $row->id . '-two.' . $extension;
              $row->upload_2_title = $file2naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_2 = null;
          $row->upload_2_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_3') || (Request::input('upload_remove_3') == "yes" && Request::hasFile('upload_3'))) {
          if (Request::hasFile('upload_3')) {
            $file3 = Request::file('upload_3');

            $destinationPath = storage_path('gi-case');
            $file3naming = array_get($input, 'upload_name_3');
            $file3name_orig = $file3->getClientOriginalName();

            $temp = explode(".", $file3name_orig);
            $extension = end($temp);

            $file3name = $row->id . '-three.' . $extension;
            $upload_success = $file3->move($destinationPath, $file3name);

            if( $upload_success)  {
              $row->upload_3   = $row->id . '-three.' . $extension;
              $row->upload_3_title = $file3naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_3 = null;
          $row->upload_3_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_4') || (Request::input('upload_remove_4') == "yes" && Request::hasFile('upload_4'))) {
          if (Request::hasFile('upload_4')) {
            $file4 = Request::file('upload_4');

            $destinationPath = storage_path('gi-case');
            $file4naming = array_get($input, 'upload_name_4');

            $file4name_orig = $file4->getClientOriginalName();

            $temp = explode(".", $file4name_orig);
            $extension = end($temp);

            $file4name = $row->id . '-four.' . $extension;
            $upload_success = $file4->move($destinationPath, $file4name);

            if( $upload_success)  {
              $row->upload_4   = $row->id . '-four.' . $extension;
              $row->upload_4_title = $file4naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_4 = null;
          $row->upload_4_title = null;
          $row->save();
        }

        if (!Request::input('upload_remove_5') || (Request::input('upload_remove_5') == "yes" && Request::hasFile('upload_5'))) {
          if (Request::hasFile('upload_5')) {
            $file5 = Request::file('upload_5');

            $destinationPath = storage_path('gi-case');
            $file5naming = array_get($input, 'upload_name_5');
            $file5name_orig = $file5->getClientOriginalName();

            $temp = explode(".", $file5name_orig);
            $extension = end($temp);

            $file5name = $row->id . '-five.' . $extension;
            $upload_success = $file5->move($destinationPath, $file5name);

            if( $upload_success)  {
              $row->upload_5   = $row->id . '-five.' . $extension;
              $row->upload_5_title = $file5naming . '.' . $extension;
              $row->save();
            }
          }
        } else {
          $row->upload_5 = null;
          $row->upload_5_title = null;
          $row->save();
        }


        //trigger notification
        $row_noti = new Notification;
        $row_noti->user_id =  Auth::user()->id;
        $row_noti->activity =" Edited ".$row->firstname." ".$row->lastname."'s production case!";
        $row_noti->controller =  "PolicyController@notification";

        if (Auth::user()->usertype_id == '8') { //if sales user
          $row_noti->usertype_id = "2"; //sales user
        } else {
          $row_noti->usertype_id = "1"; //system user
        }

        $row_noti->save();

        //save to per user
        $row_noti  = Notification::orderBy('id', 'desc')->first();

        if (Auth::user()->usertype_id == '8') {
          $row_user = User::where(function($query) {
                                  $query->where('status', '=', '1')
                                        ->where('usertype_id', '<=', '7');
                                })->get();
        } else {
          $row_user = User::where(function($query) {
                                  $query->where('status', '=', '1')
                                        ->where('usertype_id', '=', '8');
                                })->get();
        }

        foreach ($row_user as $key => $field) {      
          $rows = new NotificationPerUser;
          $rows->user_id = $field->id;
          $rows->notification_id = $row_noti->id;
          $rows->save();
        }            
      }

      $sel_client = Request::input('selected_client') ? Request::input('selected_client') : 0;
      if($sel_client == 1)
      {
        $row->selected_client = 1;
        $row->save();
      }
      else
      {
        $row->selected_client = 0;
        $row->save();
      }

      // return
      return Response::json(['body' => 'Changes have been saved.']);
    }

    public function getCaseCurrency() {

      $row = SettingsRate::where('status', '=', '1')
                          ->get();
      $col = Request::input('col');
      return Response::json($row->$col);
    }

    public function removeCaseSelected() {
        $ids = Request::input('ids');

        if (count($ids) == 0) {
            return Response::json(['error' => 'Select cases first.']); 
        } else {
            // process
            foreach ($ids as $id) {
              $row = GIProductionCases::find($id);
              if(!is_null($row)) {
                $status = $row->status;
                // you cannot remove user that already been remove
                if ($status != 2) {
                  $row->status = 2;
                  $row->save();
                }
              }
            }

          return Response::json(['body' => 'Selected cases has been Removed.']);
        }
    } 

    public function deletePending() {
      $row = GIProductionCases::find(Request::input('id'));

      if(!is_null($row)) {
        $row->status = 2;
        $row->save();

        return Response::json(['body' => 'Case has been deleted.']);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function approved() {
      $row = GIProductionCases::find(Request::input('id'));

      if(!is_null($row)) {
        $row->reject_reason = Request::input('reason');
        $row->case = "approved";
        $row->save();
        
        return Response::json(['body' => 'Case has been approved.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function verified() {
      $row = GIProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->reject_reason = Request::input('reason');
        $row->case = "verified";
        $row->screen_date = Carbon::now();
        $row->screen_auth_id = Auth::user()->id;
        $row->save();

        return Response::json(['body' => 'Case has been verfied.']);
      } else {
          // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }

    public function reject() {
      // get the user info
      $row = GIProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->reject_reason = Request::input('reason');
        $row->case = "rejected";
        $row->screen_date = Carbon::now();
        $row->screen_auth_id = Auth::user()->id;
        $row->save();
        
        return Response::json(['body' => 'Case has been rejected.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function assessment() {
      // get the user info
      $row = GIProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        if (!$row->assess_outcome) {
          $row->assess_outcome = Request::input('assessment_outcome');
          $row->assess_date = Carbon::now();
          $row->assess_auth_id = Auth::user()->id;
          $row->save();
          
          return Response::json(['body' => 'Case has been assessed.']);
        } else {
          return Response::json(['error' => "Unauthorized Access."]);
        }
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }

    public function review() {
      // get the user info
      $row = GIProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->review_outcome = Request::input('review_outcome');
        $row->review_date = Carbon::now();
        $row->review_auth_id = Auth::user()->id;
        $row->save();
        
        return Response::json(['body' => 'Case has been reviewed.']);
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
   }


    public function submitted() {
      // get the user info
      $row = GIProductionCases::find(Request::input('id'));
      if(!is_null($row)) {
        $row->submitted_ho = Request::input('submitted_outcome');
        $row->submitted_date = Carbon::now();
        $row->submitted_auth_id = Auth::user()->id;
        $row->save();
        
        if (Request::input('submitted_outcome') == "Yes") {
          return Response::json(['body' => 'Case has been submitted to HO.']);
        } else {
          // not found
          return Response::json(['error' => "The requested item was not found in the database."]);
        }
      } else {
        // not found
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function duplicate() {

      if (Request::input('id')) {
        $row = GIProductionCases::find(Request::input('id'));

        if (!$row) {
          // not found
          return Response::json(['errorlog' => "The requested item was not found in the database."]);
        }

        $new_row = new ProductionCases;
        $new_row->user_id = $row->user_id;
        $new_row->currency_id = $row->currency_id;
        $new_row->firstname = $row->firstname;
        $new_row->lastname = $row->lastname;
        $new_row->annual_income_range = $row->annual_income_range;
        $new_row->life_firstname = $row->life_firstname;
        $new_row->life_lastname = $row->life_lastname;
        $new_row->owner_id = $row->owner_id;
        $new_row->life_id = $row->life_id;
        $new_row->owner_occupation = $row->owner_occupation;
        $new_row->life_occupation = $row->life_occupation;
        $new_row->owner_gender = $row->owner_gender;
        $new_row->life_gender = $row->life_gender;
        $new_row->owner_dob = $row->owner_dob;
        $new_row->life_dob = $row->life_dob;
        $new_row->owner_age = $row->owner_age;
        $new_row->life_age = $row->life_age;
        $new_row->backdate = $row->backdate;
        $new_row->address = $row->address;
        $new_row->address = $row->address;
        $new_row->email = $row->email;
        $new_row->nationality_id = $row->nationality_id;
        $new_row->residency_status = $row->residency_status;
        $new_row->residency_others = $row->residency_others;
        $new_row->provider_list = $row->provider_list;
        $new_row->main_product_id = $row->main_product_id;
        $new_row->conversion_rate = $row->conversion_rate;
        $new_row->conversion_year = $row->conversion_year;
        $new_row->main_sum_assured = 0;
        $new_row->provider_list_rider = $row->provider_list_rider;
        $new_row->rider_product_id = $row->rider_product_id;
        $new_row->rider_sum_assured = $row->rider_sum_assured;
        $new_row->currency = $row->currency;
        $new_row->currency2 = $row->currency2;
        $new_row->currency_value = 0;
        $new_row->currency_value2 = 0;
        $new_row->annual_currency_value2 = 0;
        $new_row->conv_total = $row->conv_total;
        $new_row->currency_sd_rate = $row->currency_sd_rate;
        $new_row->currency_usd_rate = $row->currency_usd_rate;
        $new_row->life_total_amount = $row->life_total_amount;
        $new_row->policy_term = $row->policy_term;
        $new_row->payment_frequency = $row->payment_frequency;
        $new_row->mode_of_payment = $row->mode_of_payment;
        $new_row->mode_others = $row->mode_others;
        $new_row->ape = 0;
        $new_row->source = $row->source;
        $new_row->case = $row->case;
        $new_row->upload_title = $row->upload_title;
        $new_row->upload_2_title = $row->upload_2_title;
        $new_row->upload_3_title = $row->upload_3_title;
        $new_row->upload_4_title = $row->upload_4_title;
        $new_row->upload_5_title = $row->upload_5_title;
        $new_row->save();

        if ($row->upload) {
          $file = explode('.', $row->upload);
          $file_extenstion = end($file);
          copy(storage_path('gi-case') . '\\' . $row->upload, storage_path('gi-case') . '\\' . $new_row->id . '-one.' . $file_extenstion);
          $new_row->upload = $new_row->id . '-one.' . $file_extenstion;
        }

        if ($row->upload_2) {
          $file = explode('.', $row->upload_2);
          $file_extenstion = end($file);
          copy(storage_path('gi-case') . '\\' . $row->upload_2, storage_path('gi-case') . '\\' . $new_row->id . '-two.' . $file_extenstion);
          $new_row->upload_2 = $new_row->id . '-two.' . $file_extenstion;
        }
        
        if ($row->upload_3) {
          $file = explode('.', $row->upload_3);
          $file_extenstion = end($file);
          copy(storage_path('gi-case') . '\\' . $row->upload_3, storage_path('gi-case') . '\\' . $new_row->id . '-three.' . $file_extenstion);
          $new_row->upload_3 = $new_row->id . '-three.' . $file_extenstion;
        }

        if ($row->upload_4) {
          $file = explode('.', $row->upload_4);
          $file_extenstion = end($file);
          copy(storage_path('gi-case') . '\\' . $row->upload_4, storage_path('gi-case') . '\\' . $new_row->id . '-four.' . $file_extenstion);
          $new_row->upload_4 = $new_row->id . '-four.' . $file_extenstion;
        }

        if ($row->upload_5) {
          $file = explode('.', $row->upload_5);
          $file_extenstion = end($file);
          copy(storage_path('gi-case') . '\\' . $row->upload_5, storage_path('gi-case') . '\\' . $new_row->id . '-five.' . $file_extenstion);
          $new_row->upload_5 = $new_row->id . '-five.' . $file_extenstion;
        }

        $new_row->save();

        // return
        return Response::json(['body' => 'Case has been duplicated.']);

      } else {
        // not found
        return Response::json(['errorlog' => "The requested item was not found in the database."]);
      }
    }

    public function notification($id){  
      $row = NotificationPerUser::where(function($query) use ($id) {
                                          $query->where('user_id', '=', Auth::user()->id)
                                                ->where('notification_id', '=', $id);
                                        })->update(['is_read' => 1]);

      return Redirect::to('/policy/production/gi-case-submission');
    }


}