<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Displays the login form
     *
     */
    public function login() {   
// 			Auth::loginUsingId(1);
//       $ips = [
//                 '180.191.150.240',
//                 '180.191.70.197',
//                 '180.191.61.1',
//                 '180.191.61.2',
//                 '180.191.61.3',
//                 '180.191.61.4',
//                 '180.191.61.5',
//                 '180.191.61.6',
//                 '180.191.61.7',
//                 '180.191.61.8',
//                 '180.191.61.9',
//                 '180.191.61.10',
//                 '180.190.103.111',
//                 '203.111.67.17',
//                 '203.177.67.129',
//                 '175.11.107.178',
//                 '175.111.106.218',
//                 '175.111.107.177',
//                 '219.74.41.226',
//                 '118.189.129.135',
//                 '183.90.36.12',
//                 '183.90.37.145',
//                 '118.200.165.67',
//                 '180.191.146.29',
//                 '203.117.67.17',
//                 '203.117.67.129',
//                 '180.190.102.229',
//                 '219.74.182.116',
//                 '183.90.37.213',
// 		            '118.200.158.153'
//                ];
      
//       if(!in_array(request()->ip(), $ips))
//       {
//         return '<h1 style="margin:0">This domain is blocked.</h1><br><p style="margin:0">Site blocked. '.url().' is not allowed on this network</p>';
//       }

        if (!Auth::check()) {
            $this->data['title'] = "Login";
            return view('auth.login', $this->data);
        }
        else {
            // if user is already logged in
            return redirect()->intended();
        }

    }

    /**
     * Attempt to do login
     *
     */
    public function doLogin(Request $request) {
      
//         $ips = [
//                 '180.191.150.240',
//                 '180.191.70.197',
//                 '180.191.61.1',
//                 '180.191.61.2',
//                 '180.191.61.3',
//                 '180.191.61.4',
//                 '180.191.61.5',
//                 '180.191.61.6',
//                 '180.191.61.7',
//                 '180.191.61.8',
//                 '180.191.61.9',
//                 '180.191.61.10',
//                 '180.190.103.111',
//                 '203.111.67.17',
//                 '203.177.67.129',
//                 '175.11.107.178',
//                 '175.111.106.218',
//                 '175.111.107.177',
//                 '219.74.41.226',
//                 '118.189.129.135',
//                 '183.90.36.12',
//                 '183.90.37.145',
//                 '118.200.165.67',
//                 '180.191.146.29',
//                 '203.117.67.17',
//                 '203.117.67.129',
//                 '180.190.102.229',
//                 '219.74.182.116',
//                 '183.90.37.213',
// 		            '118.200.158.153'
//                ];
      
//             $curr_ip = $request->ip();
//             dd($curr_ip);
        $username = $request->input('email');
        $password = $request->input('password');
        $remember = (bool) $request->input('remember');
        $honeypot = $request->input('honeypot');
        $captcha = $request->input('g-recaptcha-response');

        if($honeypot) {
            // robot detected
            $error = trans('validation.blank');
        } 
//       else if (!$captcha) {
//             $error = "Captcha is required.";
// //         } else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
// //             // login successful
// //             return redirect()->intended();
// //         }
// //         else if(Auth::validate(['username' => $username, 'password' => $password])) {
// //             $user = User::where('username', $username)->first();

// //             if($user->status == 0) {
// //                 // user is not active
// //                 $error = "This account is currently inactive.";
// //             }
// //         }
//         } 
      else if(Auth::validate(['username' => $username, 'password' => $password])) {
            $user = User::where('username', $username)->first();
            $curr_ip = $request->ip();
//             dd($curr_ip);
            if($user->status == 0) {
                // user is not active
                $error = "This account is currently inactive.";
            } else if ($user->status == 1 && $user->usertype_id != 1) {
                // dd($user);
//                 if (!in_array($curr_ip, $ips)) {
//                     // $error = $curr_ip . " is not white listed you wont be able to login on this location.";
//                     $error = "This site can be accessed within office network only.";
//                 } else 
									if (Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
                    // login successful
                    return redirect()->intended();
                }
            } else if ($user->status == 1 && ($user->usertype_id == 1)) {
                if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
                    // login successful
                    return redirect()->intended();
                } else {
                    // invalid login
                    $error = 'Invalid email or password.';
                }
            }
        }
        else {
            // invalid login
            $error = 'Invalid email or password.';
        }
        // return error
        return redirect()->action('AuthController@login')
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
    }
  
  
    public function CEOlogin() {

        if (!Auth::check()) {
            $this->data['title'] = "Login";
            return view('auth.ceo-login', $this->data);
        }
        else {
            // if user is already logged in
            return redirect()->intended();
        }

    }

    /**
     * Attempt to do login
     *
     */
    public function CEOdoLogin(Request $request) {
      
//             $curr_ip = $request->ip();
//             dd($curr_ip);
        $username = $request->input('email');
        $password = $request->input('password');
        $remember = (bool) $request->input('remember');
        $honeypot = $request->input('honeypot');
        $captcha = $request->input('g-recaptcha-response');

        if($honeypot) {
            // robot detected
            $error = trans('validation.blank');
//         } else if (!$captcha) {
//             $error = "Captcha is required.";
        } else if(Auth::validate(['username' => $username, 'password' => $password])) {
            $user = User::where('username', $username)->first();
            if ($user->status == 1 && ($user->usertype_id == 1)) {
                if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1, 'usertype_id' => 1], $remember)) {
                    // login successful
                    return redirect()->intended();
                } else {
                    // invalid login
                    $error = 'Invalid email or password.';
                }
            }
              else {
                $error = 'Invalid email or password.';
            }
        }
        else {
            $error = 'Invalid email or password.';
        }
        // return error
        return redirect()->action('AuthController@CEOlogin')
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
    }

    /**
     * Displays the forgot password form
     *
     */
    public function forgotPassword() {
        $this->data['title'] = "Password Recovery";
        return View::make('users.forgotpass', $this->data);
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function doForgotPassword() {
        $user = User::where('email', Input::get('email'))
                    ->first();

        View::composer('emails.reminder', function($view) use ($user) {
            $view->with([
                'firstname' => $user->firstname
            ]);
        });

        $response = Password::remind(Input::only('email'), function($message) {
            $message->subject("Reset Password Request");
        });

        // (test) always say success
        $response = Password::REMINDER_SENT;

        switch ($response) {
            case Password::INVALID_USER:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'danger'
                ));

            case Password::REMINDER_SENT:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'success'
                ));
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function resetPassword($token = null) {
        if(is_null($token)) App::abort(404);

        if(Auth::guest()) {
            $this->data['token'] = $token;
            $this->data['title'] = "Reset Password";

            return View::make('users.resetpass', $this->data);
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Attempt change password of the user
     *
     */
    public function doResetPassword() {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        Password::validator(function($credentials) {
            return strlen($credentials['password']) >= 6 && $credentials['password'] != $credentials['email'];
        });

        $response = Password::reset($credentials, function($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()
                               ->withInput(Input::except('password'))
                               ->with('notice', array(
                                    'msg' => Lang::get($response),
                                    'type' => 'danger'
                ));
            case Password::PASSWORD_RESET:
                return Redirect::to('login')
                               ->with('notice', array(
                                    'msg' => Lang::get('reminders.complete'),
                                    'type' => 'success'
                ));
        }
    }

    public function logout() {
        Auth::logout();
        return redirect()->action('AuthController@login');
    }
}
