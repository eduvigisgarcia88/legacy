<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Input;
use View;
use Validator;
use Carbon;
use Auth;
use Paginator;
use DB;

use App\Permission;
use App\User;
use App\Sales;
use App\Group;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\GroupComputation;
use App\Designation;
use App\Policy;
use App\Product;
use App\ProductionCaseContact;
use App\ProductionCaseRider;
use App\PolicyContract;
use App\Provider;
use App\AssignedPolicy;
use App\Introducer;
use App\LogPolicy;
use App\UploadFeed;
use App\ProductionCases;
use App\SettingsRate;
use App\Notification;
use App\NotificationPerUser;
use App\Http\Controllers\Controller;
use Redirect;


class UserController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '!=', '7')->get();
        $this->data['href'] = url("users/refresh");
        $this->data['url'] = url("users");

        $this->data['title'] = "System Users";
        $this->data['subtitle'] = "SYSTEM USERS > USERS LIST";
        return view('users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '!=', '7');
                                })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the policy.
     *
     * @return Response
     */
    public function indexPolicy()
    {   
        $result = $this->doListPolicy();
        $this->data['title'] = "Policy Management";
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.list', $this->data);
    }
    
    /**
     * Build the list
     *
     */
    public function doListPolicy() {
        // sort and order
        $result['sort'] = Request::input('sort') ?: 'policies.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $per = Request::input('per') ?: 10;

        $import = UploadFeed::orderBy('created_at', 'desc')->first();

      $import = UploadFeed::orderBy('created_at', 'desc')->first();

      if (!$import) {
        $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->paginate($per);

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
        }
      }

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          if (Auth::user()->usertype_id == 8) {

            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->paginate($per);

          } else {

            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.upload_id', '=', $import->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->paginate($per);

                                    // dd($rows);
            // dd($rows);
          }
        } else {
          if (Auth::user()->usertype_id == 8) {
            $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->paginate($per);
          } else {
            $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.upload_id', '=', $import->id)
                                            // ->where('policies.duplicate', '=', 1)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->paginate($per);
          }

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          if (Auth::user()->usertype_id == 8) {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->paginate($per);
          } else {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('policies.upload_id', '=', $import->id)
                                            // ->where('policies.duplicate', '=', 1)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->paginate($per);
          }
        }

                                    // dd($rows);
        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
        }
    }

    /**
     * Display a listing of the policy.
     *
     * @return Response
     */
    public function indexDuplicate()
    {   
        $result = $this->doListDuplicate();
        $this->data['title'] = "Policy Management";
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.duplicates.list', $this->data);
    }
    
    /**
     * Display a listing of the policy.
     *
     * @return Response
     */
    public function indexRider()
    {   
        $result = $this->doListRider();

        $this->data['title'] = "Policy Management";
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['permission'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '1');
                              })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '2');
                              })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '3');
                              })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                                ->select('sales.*','designations.*')
                                                ->where('sales.user_id','=',Auth::user()->id)
                                                ->first();
  
        $this->data['permission_provider'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '4');
                              })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
                                    $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                          ->where('module_id', '=', '5');
                              })->first();
        $this->data['providers'] = Provider::all();

         if ( Auth::user()->usertype_id == '8') { //if sales user
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');                             
                                        })->get();
     
        }
        else
         {
             $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '2')
                                                  ->where('notifications_per_users.is_read', '=', '0');       
                                        })->get();
         }   
         $this->data['settings'] = Settings::first();
         $this->data['noti_count'] = count($this->data['notification']);
        return view('policy-management.riders.list', $this->data);
    }
    
    /**
     * Build the list
     *
     */
    public function doListRider() {
        $result['sort'] = Request::input('sort') ?: 'policies.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $per = Request::input('per') ?: 10;
        
        $import = UploadFeed::orderBy('created_at', 'desc')->first();

        if (!$import) {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) {
                                      $query->where('policies.status', '=', 1);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);


          // return response (format accordingly)
          if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
          } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
          }
        }

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          if (Auth::user()->usertype_id == 8) {

            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);

          } else {

            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);

                                    // $rows = Policy::groupBy('policies.contract_no')->havingRaw('sum(count) > 1')->selectRaw('sum(count) as rider')->take(10)->get();
                                    // dd($rows);
            // dd($rows);
          }
        } else {
          if (Auth::user()->usertype_id == 8) {
            $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          } else {
            $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          }

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

          if (Auth::user()->usertype_id == 8) {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '=', Auth::user()->id);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          } else {
            $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no', 'provider_categories.name as cat_name')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->join('provider_categories', 'provider_categories.id', '=', 'policies.category_id')
                                    ->where(function($query) use ($import) {
                                      $query->where('policies.status', '=', 1)
                                            ->where('upload_id', '=', $import->id)
                                            ->where('policies.user_id', '>', 0);
                                      })
                                    ->where(function($query) use ($search) {
                                      $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                            ->orWhere('users.name', 'LIKE', '%' . $search . '%');
                                      })
                                    ->selectRaw('sum(count) as rider')
                                    ->selectRaw('sum(duplicate) as duplication')
                                    ->orderBy($result['sort'], $result['order'])
                                    ->groupBy('policies.contract_no')
                                    ->havingRaw('sum(count) - sum(duplicate) > 1')
                                    ->paginate($per);
          }
        }

                                    // dd($rows);
        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
        }
    }


    /**
     * Build the list
     *
     */
    public function doListDuplicate() {
        // sort and order
        $result['sort'] = Request::input('sort') ?: 'policies.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $per = Request::input('per') ?: 10;

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          // $rows = Policy::select('policies.id', 'users.name', 'policies.contract_no as policy_no')
          //                         ->join('users', 'users.id', '=', 'policies.user_id')
          //                         ->where('contract_no', 'LIKE', '%' . $search . '%')
          //                         ->orWhere('name', 'LIKE', '%' . $search . '%')
          //                         ->orderBy($result['sort'], $result['order'])
          //                         ->groupBy('contract_no')
          //                         ->having('duplicate', '<=', '1')
          //                         ->paginate($per);

              $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->groupBy('contract_no')
                                    ->where('duplicate', '=', '1')
                                    ->where('policies.status', '=', '1')
                                    ->where(function($query) use ($search) {
                                        $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('name', 'LIKE', '%' . $search . '%');
                                    })
                                  ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
                                  // dd($rows);
        } else {
              $count = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->groupBy('contract_no')
                                    ->where('duplicate', '=', '1')
                                    ->where('policies.status', '=', '1')
                                    ->where(function($query) use ($search) {
                                        $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('name', 'LIKE', '%' . $search . '%');
                                    })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows = Policy::select('policies.*', 'users.name', 'policies.contract_no as policy_no')
                                    ->join('users', 'users.id', '=', 'policies.user_id')
                                    ->groupBy('contract_no')
                                    ->where('duplicate', '=', '1')
                                    ->where('policies.status', '=', '1')
                                    ->where(function($query) use ($search) {
                                        $query->where('contract_no', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('name', 'LIKE', '%' . $search . '%');
                                    })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
          
        }

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexCEO()
    {
        // get row set
        $result = $this->doListCEO();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
         $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '!=', '7')->get();
        $this->data['href'] = url("users/ceo/refresh");
        $this->data['url'] = url("users/ceo");

        $this->data['title'] = "System Users";
        $this->data['subtitle'] = "SYSTEM USERS > USERS LIST > CEO";
        return view('users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListCEO() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '!=', '7')
                                          ->where('users.usertype_id', '=', '1');
                                })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexIT()
    {
        // get row set
        $result = $this->doListIT();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
         $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '=', '2')->get();
        $this->data['href'] = url("users/it/refresh");
        $this->data['url'] = url("users/it");

        $this->data['title'] = "System Users";
        $this->data['subtitle'] = "SYSTEM USERS > USERS LIST > IT ACCOUNTS";
        return view('users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListIT() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '!=', '7')
                                          ->where('users.usertype_id', '=', '2');
                                })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexAdmin()
    {
        // get row set
        $result = $this->doListAdmin();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
         $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '=', '3')
                                        ->orWhere('id', '=', '4')->get();
        $this->data['href'] = url("users/admin/refresh");
        $this->data['url'] = url("users/admin");

        $this->data['title'] = "System Users";
        $this->data['subtitle'] = "SYSTEM USERS > USERS LIST > ADMIN ACCOUNTS";
        return view('users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListAdmin() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '!=', '7')
                                          ->where('users.usertype_id', '=', '3')
                                          ->orWhere('users.usertype_id', '=', '4');
                                })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexAccounting()
    {
        // get row set
        $result = $this->doListAccounting();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['settings'] = Settings::first();
        $this->data['types'] = UserType::where('id', '=', '5')
                                        ->orWhere('id', '=', '6')->get();
        $this->data['href'] = url("users/accounting/refresh");
        $this->data['url'] = url("users/accounting");

        $this->data['title'] = "System Users";
        $this->data['subtitle'] = "SYSTEM USERS > USERS LIST";
        $this->data['subtitle'] = "SYSTEM USERS > USERS LIST > ACCOUNTING";
        return view('users.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doListAccounting() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '!=', '7')
                                          ->where('users.usertype_id', '=', '5')
                                          ->orWhere('users.usertype_id', '=', '6');
                                })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Save model
     *
     */
    public function save() {
      // assume this is a new row
      $new = true;

      // check if an ID is passed
      if(Request::input('id')) {

        // get the user info
        $row = User::where('status', 1)->find(Input::get('id'));

        if(!$row) {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }

        // this is an existing row
        $new = false;
      }

      $rules = array(
        'username' => 'required|min:2|max:15|alpha_dash|unique:users,username' . (!$new ? ',' . $row->id : ''),
        'email' => 'required|email|unique:users,email' . (!$new ? ',' . $row->id : ''),
        'name' => 'required|min:2|max:100',
        'password' => $new ? 'required|min:6|confirmed' : 'min:6|confirmed',
      );

      // do not require type if user is editing self
      if($new || (!$new && $row->id != Auth::user()->id)) {
          $rules['type'] = 'required|exists:usertypes,id';
      }

      // field name overrides
      $names = array(
          'name' => 'full name'
      );

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      // create model if new
      if($new) {
          $row = new User;
      }

      $row->username  = Request::input('username');
      $row->email     = Request::input('email');
      $row->name      = strip_tags(Request::input('name'));

      // set type only if this isn't me
      if($new || (!$new && $row->id != Auth::user()->id)) {
        $row->usertype_id = Request::input('type');
      }

      // do not change password if old user and field is empty
      if($new || (!$new && Request::input('password'))) {
        $row->password = Hash::make(Request::input('password'));
      }

      // save model
      $row->save();

      // return
      return Response::json(['body' => 'Changes have been saved.']);
    }

    /**
     * Delete the user
     *
     */
    public function delete() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            User::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'User account has been deleted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Deactivate the user
     *
     */
    public function deactivate() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = 0;
            $row->save();

            // return
            return Response::json(['body' => 'User account has been deactivated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the user
     *
     */
    public function activate() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = 1;
            $row->save();

            // return
            return Response::json(['body' => 'User account has been activated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Retrieve user information
     *
     */
    public function info() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = User::with('userType')->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
