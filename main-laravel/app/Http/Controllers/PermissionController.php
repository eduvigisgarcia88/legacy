<?php

namespace App\Http\Controllers;

use Request;
use App\Permission;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    
 	/**
     * Save Permissions.
     *
     * @return Json
     */
    public function savePermissions()
    {
    
        $rows = Request::input('rows');

        foreach ($rows as $row) {
            $permission = Permission::find([$row['id']])->first();

            //
            if ($permission->usertype_id == 3 || $permission->usertype_id == 5 || $permission->usertype_id == 7 || $permission->usertype_id == 9) {
                $pe = Permission::where('usertype_id', $permission->usertype_id + 1)
                                ->where('module_id', $permission->module_id)
                                ->update(['view' => ($row['view'] == 'true') ? 1 : 0, 'edit' => ($row['edit'] == 'true') ? 1 : 0, 'delete' => ($row['delete'] == 'true') ? 1 : 0]);
                                
            }

    		$permission->view = ($row['view'] == 'true') ? 1 : 0;
            $permission->edit = ($row['edit'] == 'true') ? 1 : 0;
            $permission->delete = ($row['delete'] == 'true') ? 1 : 0;
            $permission->save();            
 
        }    

        return response()->json(['success' => 'Permissions has been updated.']);
    }
    
}