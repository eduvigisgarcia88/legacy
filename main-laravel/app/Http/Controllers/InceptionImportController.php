<?php

namespace App\Http\Controllers;

use Excel;
use App\Inception;
use Carbon\Carbon;
use App\User;
use App\Provider;
use Validator;
class InceptionImportController extends Controller
{

	public function importXLS() {
		// dd(request()->xlsfile->getClientMimeType());
		$messages = [
                        'xlsfile.required' => 'Import file is required.',
                    ];

        $validator = Validator::make(request()->all(), [
            'xlsfile' => 'required',
        ], $messages);

        if($validator->fails())
        {
            $response['code'] = 0;
            $response['message'] = $validator->errors()->first();
            return $response;
        }
        else
        {
        	$xls = request()->xlsfile;
			try
			{
				$array = Excel::load($xls)->toArray();
			}
			catch(\Exception $e)
			{
					$response['code'] = 0;
					$response['message'] = $e->getMessage();
					return $response;	
			}
			
			$err = '';
			try
			{
				$looped = 0;
				$saved = 0;
				foreach($array as $put)
				{
				    $looped++;
				    $user_id = User::where('code', trim($put['agent_code']))->pluck('id');
				    $prov_id = Provider::where('code', trim($put['provider']))->pluck('id') ? Provider::where('code', trim($put['provider']))->pluck('id') : 0;

				    $ape_mtd = $put['ape_mtd'] ? $put['ape_mtd'] : 0;
				    $cat_1_mtd = $put['cat_1_mtd'] ? $put['cat_1_mtd'] : 0;
				    $ape_ytd = $put['ape_ytd'] ? $put['ape_ytd'] : 0;
				    $cat_1_ytd = $put['cat_1_ytd'] ? $put['cat_1_ytd'] : 0;
				    $ape_count_mtd = $put['ape_count_mtd'] ? $put['ape_count_mtd'] : 0;
				    $cat_1_count_mtd = $put['cat_1_count_mtd'] ? $put['cat_1_count_mtd'] : 0;
				    $ape_count_ytd = $put['ape_count_ytd'] ? $put['ape_count_ytd'] : 0;
				    $cat_1_count_ytd = $put['cat_1_count_ytd'] ? $put['cat_1_count_ytd'] : 0;
						$cat_1_health_mtd = $put['cat_1_health_mtd'] ? $put['cat_1_health_mtd'] : 0;
						$cat_1_health_count_mtd = $put['cat_1_health_count_mtd'] ? $put['cat_1_health_count_mtd'] : 0;
						$cat_1_health_ytd = $put['cat_1_health_ytd'] ? $put['cat_1_health_ytd'] : 0;
						$cat_1_health_count_ytd = $put['cat_1_health_count_ytd'] ? $put['cat_1_health_count_ytd'] : 0;
				    $excel_incept_date = Carbon::createFromFormat('M Y', $put['incept_date'])->startOfMonth()->format('Y-m-d H:i:s');
				    $exist = Inception::whereNull('batch_id')
				                      ->where('user_id', $user_id)
				                      ->where('provider_id', $prov_id)
				                      ->where('agent_name', trim(strtoupper($put['agent_name'])))
				                      ->where('agent_code', trim($put['agent_code']))
				                      ->where('provider', trim($put['provider']))
				                      ->where('ape_mtd', $ape_mtd)
				                      ->where('ape_count_mtd', $ape_count_mtd)
				                      ->where('cat_1_mtd', $cat_1_mtd)
				                      ->where('cat_1_count_mtd', $cat_1_count_mtd)
				                      ->where('ape_ytd', $ape_ytd)
				                      ->where('ape_count_ytd', $ape_count_ytd)
				                      ->where('cat_1_ytd', $cat_1_ytd)
				                      ->where('cat_1_count_ytd', $cat_1_count_ytd)
															->where('cat_1_health_mtd', $cat_1_health_mtd)
															->where('cat_1_health_count_mtd', $cat_1_health_count_mtd)
															->where('cat_1_health_ytd', $cat_1_health_ytd)
															->where('cat_1_health_count_ytd', $cat_1_health_count_ytd)
				                      ->where('incept_date', $excel_incept_date)
				                      ->first();
				    // dd($exist);
				    if(!$exist)
				    {
				      $insert = new Inception;
				      $insert->batch_id = NULL;
				      $insert->user_id = $user_id;
				      $insert->provider_id = $prov_id;
				      $insert->agent_name = trim(strtoupper($put['agent_name']));
				      $insert->agent_code = trim($put['agent_code']);
				      $insert->provider = trim($put['provider']);
				      $insert->ape_mtd = $ape_mtd;
				      $insert->ape_count_mtd = $ape_count_mtd;
				      $insert->cat_1_mtd = $cat_1_mtd;
				      $insert->cat_1_count_mtd = $cat_1_count_mtd;
				      $insert->ape_ytd = $ape_ytd;
				      $insert->ape_count_ytd = $ape_count_ytd;
				      $insert->cat_1_ytd = $cat_1_ytd;
				      $insert->cat_1_count_ytd = $cat_1_count_ytd;
				      $insert->incept_date = $excel_incept_date;
							$insert->cat_1_health_mtd = $cat_1_health_mtd;
							$insert->cat_1_health_count_mtd = $cat_1_health_count_mtd;
							$insert->cat_1_health_ytd = $cat_1_health_ytd;
							$insert->cat_1_health_count_ytd = $cat_1_health_count_ytd;
				      $insert->save();
				      $saved++;
				    }
				}
			}
			catch(\Exception $e)
			{
				$err .= $e->getMessage().' Check excel file line number '.$looped.'<br>';
			}

			if($err)
			{
				$response['saved'] = $saved;
				$response['looped'] = $looped;
				$response['code'] = 0;
				$response['message'] = $err;
				return $response;
			}
			else
			{
				$response['saved'] = $saved;
				$response['looped'] = $looped;
				$response['code'] = 1;
				$response['message'] = 'Done';
				return $response;
			}
        }
		
	}

}
