<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Provider;
use App\ProviderClassification;
use App\ProductionCases;
use App\ProductionCasesSamp;
use App\ProductionCaseContact;
use App\ProductRider;
use App\Policy;
use App\PolicyContract;
use App\PayrollComputation;
use App\PayrollInception;
use App\InceptionReplicate;
use App\User;
use App\DataFeed;
use App\BSCGrade;
use App\Sales;
use App\SalesUser;
use App\SalesDesignation;
use App\BatchMonthUser;
use App\CPDHours;
use App\Settings;
use App\SettingsCPD;
use App\Product;
use App\ProductGross;
use App\ProductMain;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\Group;
use App\SalesSupervisorComputation;
use App\SalesAdvisorComputation;
use App\GroupComputation;
use App\Nationality;
use App\PolicyDuplicates;
use App\UploadFeed;
use App\Batch;
use App\BatchMonthGroup;
use App\Permission;
use App\ProviderCodes;
use App\Notification;
use Excel;
use App\NotificationPerUser;
use App\GIProductionCases;

use App\GroupInception;
use App\SalesSupervisorInception;
use App\SalesAdvisorInception;
use App\BatchInception;
use App\SalesInception;
use App\GI;
use App\BatchMonthSupervisor;
use App\PrePayrollComputation;


use Auth;
use Carbon;
use Input;
use Request;
use Response;
use DB;


class UpdatingController extends Controller
{
    public function index() {
    }
  
  
    public function updation()
    {

    }

}
