<?php

namespace App\Http\Controllers;

use Excel;
use Auth;
use Crypt;
use DB;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use App\Agent;
use App\Designation;
use App\User;
use App\UserType;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['designations'] = Designation::all();
        $this->data['types'] = UserType::where('id', '=', '7')->get();
        $this->data['href'] = url("agents/refresh");
        $this->data['url'] = url("agents");
        $this->data['settings'] = Settings::first();
        $this->data['title'] = "Agent Management";
        $this->data['subtitle'] = "AGENT MANAGEMENT > AGENT LIST";
        return view('agents.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doList() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = Agent::select('agents.*', 'users.*', 'agents.id as agent_id', 'designations.designation')
                                ->join('users', 'users.id', '=', 'agents.user_id')
                                ->join('designations', 'designations.id', '=', 'agents.designation_id')
                                ->where('users.usertype_id', '=', '7')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexSales()
    {
        // get row set
        $result = $this->doListSales();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['designations'] = Designation::where('id', '=', '1')->get();
        $this->data['types'] = UserType::where('id', '=', '7')->get();
        $this->data['href'] = url("agents/sales/refresh");
        $this->data['url'] = url("agents/sales");
        $this->data['settings'] = Settings::first();
        $this->data['title'] = "Agent Management";
        $this->data['subtitle'] = "AGENT MANAGEMENT > AGENT LIST > SALES AGENT";
        return view('agents.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doListSales() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = Agent::select('agents.*', 'users.*', 'agents.id as agent_id', 'designations.designation')
                                ->join('users', 'users.id', '=', 'agents.user_id')
                                ->join('designations', 'designations.id', '=', 'agents.designation_id')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('agents.designation_id', '=', '1');
                                })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexManager()
    {
        // get row set
        $result = $this->doListManager();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['designations'] = Designation::where('id', '=', '3')->get();
        $this->data['types'] = UserType::where('id', '=', '7')->get();
        $this->data['href'] = url("agents/manager/refresh");
        $this->data['url'] = url("agents/manager");
         $this->data['settings'] = Settings::first();
        $this->data['title'] = "Agent Management";
        $this->data['subtitle'] = "AGENT MANAGEMENT > AGENT LIST > MANAGER";
        return view('agents.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doListManager() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = Agent::select('agents.*', 'users.*', 'agents.id as agent_id', 'designations.designation')
                                ->join('users', 'users.id', '=', 'agents.user_id')
                                ->join('designations', 'designations.id', '=', 'agents.designation_id')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('agents.designation_id', '=', '3');
                                })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexSubManager()
    {
        // get row set
        $result = $this->doListSubManager();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['designations'] = Designation::where('id', '=', '2')->get();
        $this->data['types'] = UserType::where('id', '=', '7')->get();
        $this->data['href'] = url("agents/submanager/refresh");
        $this->data['url'] = url("agents/submanager");
         $this->data['settings'] = Settings::first();
        $this->data['title'] = "Agent Management";
        $this->data['subtitle'] = "AGENT MANAGEMENT > AGENT LIST > ASSISTANT MANAGER";
        return view('agents.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doListSubManager() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = Agent::select('agents.*', 'users.*', 'agents.id as agent_id', 'designations.designation')
                                ->join('users', 'users.id', '=', 'agents.user_id')
                                ->join('designations', 'designations.id', '=', 'agents.designation_id')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('agents.designation_id', '=', '2');
                                })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexDirector()
    {
        // get row set
        $result = $this->doListDirector();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['designations'] = Designation::where('id', '=', '5')->get();
        $this->data['types'] = UserType::where('id', '=', '7')->get();
        $this->data['href'] = url("agents/director/refresh");
        $this->data['url'] = url("agents/director");
         $this->data['settings'] = Settings::first();
        $this->data['title'] = "Agent Management";
        $this->data['subtitle'] = "AGENT MANAGEMENT > AGENT LIST > DIRECTOR";
        return view('agents.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doListDirector() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = Agent::select('agents.*', 'users.*', 'agents.id as agent_id', 'designations.designation')
                                ->join('users', 'users.id', '=', 'agents.user_id')
                                ->join('designations', 'designations.id', '=', 'agents.designation_id')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('agents.designation_id', '=', '5');
                                })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexSubDirector()
    {
        // get row set
        $result = $this->doListSubDirector();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['settings'] = Settings::first();
        $this->data['designations'] = Designation::where('id', '=', '4')->get();
        $this->data['types'] = UserType::where('id', '=', '7')->get();
        $this->data['href'] = url("agents/subdirector/refresh");
        $this->data['url'] = url("agents/subdirector");

        $this->data['title'] = "Agent Management";
        $this->data['subtitle'] = "AGENT MANAGEMENT > AGENT LIST > ASSISTANT DIRECTOR";
        return view('agents.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doListSubDirector() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = Agent::select('agents.*', 'users.*', 'agents.id as agent_id', 'designations.designation')
                                ->join('users', 'users.id', '=', 'agents.user_id')
                                ->join('designations', 'designations.id', '=', 'agents.designation_id')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('agents.designation_id', '=', '4');
                                })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexPartner()
    {
        // get row set
        $result = $this->doListPartner();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['designations'] = Designation::where('id', '=', '6')->get();
        $this->data['types'] = UserType::where('id', '=', '7')->get();
        $this->data['href'] = url("agents/partner/refresh");
        $this->data['url'] = url("agents/partner");
         $this->data['settings'] = Settings::first();
        $this->data['title'] = "Agent Management";
        $this->data['subtitle'] = "AGENT MANAGEMENT > AGENT LIST > PARTNER";
        return view('agents.list', $this->data);
    }


    /**
     * Build the list
     *
     */
    public function doListPartner() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'name';
        $result['order'] = Request::input('o') ?: 'asc';

        $rows = Agent::select('agents.*', 'users.*', 'agents.id as agent_id', 'designations.designation')
                                ->join('users', 'users.id', '=', 'agents.user_id')
                                ->join('designations', 'designations.id', '=', 'agents.designation_id')
                                ->where(function($query)
                                {
                                    $query->where('users.usertype_id', '=', '7')
                                          ->where('agents.designation_id', '=', '6');
                                })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Retrieve agent information
     *
     */
    public function info() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Agent::where('user_id', $id)->with('agents')->with('designations')->first();

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Save model
     *
     */
    public function save() {   
        // assume this is a new row
        $new = true;
        $id = Request::input('id');
        
        // check if an ID is passed
        if(Request::input('id')) {

            // get the user info
            $row = User::where('status', 1)->find(Request::input('id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
            'username' => 'required|min:2|max:15|alpha_dash|unique:users,username' . (!$new ? ',' . $row->id : ''),
            'email' => 'required|email|unique:users,email' . (!$new ? ',' . $row->id : ''),
            'name' => 'required|min:2|max:100',
            'password' => $new ? 'required|min:6|confirmed' : 'min:6|confirmed',
            'photo' => 'required|min:2|max:100',
            'mobile_no' => 'required|min:2|max:100',
            'code' => 'required|min:2|max:100',
            'designation_id' => 'required|min:1|max:100',
        );

        // do not require type if user is editing self
        if($new || (!$new && $row->id != Auth::user()->id)) {
            $rules['type'] = 'required|exists:usertypes,id';
        }

        // field name overrides
        $names = array(
            'name' => 'agent real name',
            'mobile_no' => 'mobile no.',
            'designation_id' => 'designation',
            'code' => 'agent code',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // create model if new
        if($new) {
            $row = new User;
        }

            $row->username  = Request::input('username');
            $row->email     = Request::input('email');
            $row->name      = strip_tags(Request::input('name'));

            // set type only if this isn't me
            if($new || (!$new && $row->id != Auth::user()->id)) {
                $row->usertype_id = Request::input('type');
            }

        // do not change password if old user and field is empty
        if($new || (!$new && Request::input('password'))) {
            $row->password = Hash::make(Request::input('password'));
        }

        // save model
        $row->save();

        // create agent info if new
        if($new) {
            $agent = new Agent;
            $agent->mobile_no = Request::input('mobile_no');
            $agent->designation_id = Request::input('designation_id');
            $agent->photo = Request::input('photo');
            $agent->code = Request::input('code');
            $agent->user_id = $row->id;
        } else {
            $agent = Agent::where('user_id', $id)->first();
            $agent->mobile_no = Request::input('mobile_no');
            $agent->designation_id = Request::input('designation_id');
            $agent->photo = Request::input('photo');
            $agent->code = Request::input('code');
            $agent->user_id = $id;
        }

        // save agent
        $agent->save();


        // return
        return Response::json(['body' => 'Changes have been saved.']);
    }

    /**
     * Delete the user
     *
     */
    public function delete() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            User::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'User account has been deleted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Deactivate the user
     *
     */
    public function deactivate() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = 0;
            $row->save();

            // return
            return Response::json(['body' => 'User account has been deactivated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Activate the user
     *
     */
    public function activate() {
        $row = User::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = 1;
            $row->save();

            // return
            return Response::json(['body' => 'User account has been activated.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    // Get all the values of CSV file 
    public function policy()
    {

        Excel::load('file.csv', function($reader) {

            // Getting all results
            $results = $reader->get();

            // ->all() is a wrapper for ->get() and will work the same
            $results = $reader->all();

            $reader->dd();
        });

    }
}
