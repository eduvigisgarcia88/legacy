<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Provider;
use App\ProviderClassification;
use App\ProductionCases;
use App\GIProductionCases;
use App\Policy;
use App\PayrollInception;
use App\PolicyContract;
use App\PayrollComputation;
use App\PayrollComm;
use App\User;
use App\DataFeed;
use App\BSCGrade;
use App\Sales;
use App\SalesDesignation;
use App\BatchMonthUser;
use App\CPDHours;
use App\Settings;
use App\SettingsCPD;
use App\Product;
use App\DashboardInception;
use App\DashboardSubmission;
use App\ProductMain;
use App\SalesSupervisor;
use App\SalesAdvisor;
use App\PolicyDuplicates;
use App\UploadFeed;
use App\Group;
use App\BatchMonthGroup;
use App\Permission;
use App\ProviderCodes;
use App\Notification;
use App\PayrollMTDBatch;
use App\PayrollMTDDashboard;
use App\PayrollMTDCategory;
use App\PayrollYTDBatch;
use App\PayrollYTDDashboard;
use App\PayrollYTDCategory;
use App\Inception;
use App\InceptionBatch;
use Excel;
use App\NotificationPerUser;
use Auth;
use Carbon;
use Input;
use Request;
use Response;
use DB;
use Paginator;
use Validator;


class DashboardController extends Controller
{

    public function __construct() {

        $this->middleware('auth');

    }
    
    /*
    |--------------------------------------------------------------------------
    | Dashboard Controller
    |--------------------------------------------------------------------------
    |
    | Main landing page
    |
    */
    public function index() {
      $this_year = Carbon::now()->format('Y');
      $this->data['settings'] = Settings::first();
      $this->data['ranking'] = "system";

      if (Auth::user()->usertype_id == 8) {

        $get_users = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 1)->lists('id');

        $get_agents = User::with('salesInfo')
                          ->with('salesInfo.designations')
                          ->where('usertype_id', 8)
                          ->where('id', '!=', 8)
                          ->where('status', 1)
                          ->orderBy('name', 'asc')
                          ->get();
        
        $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();
        $check_advisor = SalesAdvisor::where('user_id', Auth::user()->id)->first();

        $bsc_q1 = '-';
        $bsc_q2 = '-';
        $bsc_q3 = '-';
        $bsc_q4 = '-';
        $bsc_supervisor_q1 = '-';
        $bsc_supervisor_q2 = '-';
        $bsc_supervisor_q3 = '-';
        $bsc_supervisor_q4 = '-';
        $this->data['bsc_rank'] = null;
        if ($check_supervisor) {

          $users_cpd[0] = Auth::user()->id;
          $get_advisors_cpd = SalesAdvisor::whereIn('user_id', $get_users)->where('supervisor_id', $check_supervisor->id)->lists('user_id');
          $ctr = 1;
          foreach ($get_advisors_cpd as $gackey => $gacvalue) {
            $users_cpd[$ctr] = $gacvalue;
            $ctr++;
          }

          $this->data['total_cpd_hours_skills'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_skills');
          $this->data['total_cpd_hours_knowledge'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_knowledge');
          $this->data['total_cpd_hours_rules_and_regulations'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_rules_and_regulations');
          $this->data['total_cpd_hours_gi'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_gi');
          // $this->data['total_cpd_hours_final'] = CPDHours::where('user_id', Auth::user()->id)->sum('total_cpd_hours_final');
          $this->data['total_cpd_hours_final'] = $this->data['total_cpd_hours_skills'] + $this->data['total_cpd_hours_knowledge'] + $this->data['total_cpd_hours_rules_and_regulations'];
          $this->data['total_shortfall'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('shortfall');

          $this->data['total_cpd_hours_skills_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_skills');
          // dd($this->data['total_cpd_hours_skills_2016']);
          $this->data['total_cpd_hours_knowledge_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_knowledge');
          $this->data['total_cpd_hours_rules_and_regulations_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_rules_and_regulations');
          // $this->data['total_cpd_hours_final'] = CPDHours::where('user_id', Auth::user()->id)->sum('total_cpd_hours_final');
          $this->data['total_cpd_hours_final_2016'] = $this->data['total_cpd_hours_skills_2016'] + $this->data['total_cpd_hours_knowledge_2016'] + $this->data['total_cpd_hours_rules_and_regulations_2016'];
          $this->data['total_cpd_hours_gi_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_gi');
          $this->data['total_shortfall_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('shortfall');

          $this->data['bsc_rank'] = 'supervisor';

          $get_bsc_grade = BSCGrade::where('user_id', Auth::user()->id)->first();
          if ($get_bsc_grade) {
            $bsc_q1 = ($get_bsc_grade->q1_grade == "N/A" ? 'None Graded' : $get_bsc_grade->q1_grade);
            $bsc_q2 = ($get_bsc_grade->q2_grade == "N/A" ? '-' : $get_bsc_grade->q2_grade);
            $bsc_q3 = ($get_bsc_grade->q3_grade == "N/A" ? '-' : $get_bsc_grade->q3_grade);
            $bsc_q4 = ($get_bsc_grade->q4_grade == "N/A" ? '-' : $get_bsc_grade->q4_grade);

            $bsc_supervisor_q1 = ($get_bsc_grade->q1_supervisor_grade == "N/A" ? 'None Graded' : $get_bsc_grade->q1_supervisor_grade);
            $bsc_supervisor_q2 = ($get_bsc_grade->q2_supervisor_grade == "N/A" ? '-' : $get_bsc_grade->q2_supervisor_grade);
            $bsc_supervisor_q3 = ($get_bsc_grade->q3_supervisor_grade == "N/A" ? '-' : $get_bsc_grade->q3_supervisor_grade);
            $bsc_supervisor_q4 = ($get_bsc_grade->q4_supervisor_grade == "N/A" ? '-' : $get_bsc_grade->q4_supervisor_grade);
          }
        } else if ($check_advisor) {

          $this->data['total_cpd_hours_skills'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_skills');
          $this->data['total_cpd_hours_knowledge'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_knowledge');
          $this->data['total_cpd_hours_rules_and_regulations'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_rules_and_regulations');
          // $this->data['total_cpd_hours_final'] = CPDHours::where('user_id', Auth::user()->id)->sum('total_cpd_hours_final');
          $this->data['total_cpd_hours_final'] = $this->data['total_cpd_hours_skills'] + $this->data['total_cpd_hours_knowledge'] + $this->data['total_cpd_hours_rules_and_regulations'];
          $this->data['total_cpd_hours_gi'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_gi');
          $this->data['total_shortfall'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('shortfall');

          $this->data['total_cpd_hours_skills_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_skills');
          // dd($this->data['total_cpd_hours_skills_2016']);
          $this->data['total_cpd_hours_knowledge_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_knowledge');
          $this->data['total_cpd_hours_rules_and_regulations_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_rules_and_regulations');
          // $this->data['total_cpd_hours_final'] = CPDHours::where('user_id', Auth::user()->id)->sum('total_cpd_hours_final');
          $this->data['total_cpd_hours_final_2016'] = $this->data['total_cpd_hours_skills'] + $this->data['total_cpd_hours_knowledge'] + $this->data['total_cpd_hours_rules_and_regulations'];
          $this->data['total_cpd_hours_gi_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_gi');
          $this->data['total_shortfall_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('shortfall');

          $this->data['bsc_rank'] = 'advisor';
          $get_bsc_grade = BSCGrade::where('user_id', Auth::user()->id)->first();
          if ($get_bsc_grade) {
            $bsc_q1 = ($get_bsc_grade->q1_grade == "N/A" ? 'None Graded' : $get_bsc_grade->q1_grade);
            $bsc_q2 = ($get_bsc_grade->q2_grade == "N/A" ? '-' : $get_bsc_grade->q2_grade);
            $bsc_q3 = ($get_bsc_grade->q3_grade == "N/A" ? '-' : $get_bsc_grade->q3_grade);
            $bsc_q4 = ($get_bsc_grade->q4_grade == "N/A" ? '-' : $get_bsc_grade->q4_grade);
          }
          $get_sup = SalesSupervisor::find($check_advisor->supervisor_id);

          if ($get_sup) {
            $get_bsc_grade_sup = BSCGrade::where('user_id', $get_sup->user_id)->first();

            if ($get_bsc_grade_sup) {
              $bsc_supervisor_q1 = ($get_bsc_grade_sup->q1_supervisor_grade == "N/A" ? 'None Graded' : $get_bsc_grade_sup->q1_supervisor_grade);
              $bsc_supervisor_q2 = ($get_bsc_grade_sup->q2_supervisor_grade == "N/A" ? '-' : $get_bsc_grade_sup->q2_supervisor_grade);
              $bsc_supervisor_q3 = ($get_bsc_grade_sup->q3_supervisor_grade == "N/A" ? '-' : $get_bsc_grade_sup->q3_supervisor_grade);
              $bsc_supervisor_q4 = ($get_bsc_grade_sup->q4_supervisor_grade == "N/A" ? '-' : $get_bsc_grade_sup->q4_supervisor_grade);
            }
          }
        } else {

          $this->data['total_cpd_hours_skills'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_skills');
          $this->data['total_cpd_hours_knowledge'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_knowledge');
          $this->data['total_cpd_hours_rules_and_regulations'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_rules_and_regulations');
          // $this->data['total_cpd_hours_final'] = CPDHours::where('user_id', Auth::user()->id)->sum('total_cpd_hours_final');
          $this->data['total_cpd_hours_final'] = $this->data['total_cpd_hours_skills'] + $this->data['total_cpd_hours_knowledge'] + $this->data['total_cpd_hours_rules_and_regulations'];
          $this->data['total_shortfall'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('shortfall');
          $this->data['total_cpd_hours_gi'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=',$this_year)->sum('total_cpd_hours_gi');

          $this->data['total_cpd_hours_skills_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_skills');
          $this->data['total_cpd_hours_knowledge_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_knowledge');
          $this->data['total_cpd_hours_rules_and_regulations_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_rules_and_regulations');
          // $this->data['total_cpd_hours_final'] = CPDHours::where('user_id', Auth::user()->id)->sum('total_cpd_hours_final');
          $this->data['total_cpd_hours_final_2016'] = $this->data['total_cpd_hours_skills_2016'] + $this->data['total_cpd_hours_knowledge_2016'] + $this->data['total_cpd_hours_rules_and_regulations_2016'];
          $this->data['total_shortfall_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('shortfall');
          $this->data['total_cpd_hours_gi_2016'] = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date','=','2016')->sum('total_cpd_hours_gi');
        }

        $this->data['bsc_q1'] = $bsc_q1;
        $this->data['bsc_q2'] = $bsc_q2;
        $this->data['bsc_q3'] = $bsc_q3;
        $this->data['bsc_q4'] = $bsc_q4;
        $this->data['bsc_supervisor_q1'] = $bsc_supervisor_q1;
        $this->data['bsc_supervisor_q2'] = $bsc_supervisor_q2;
        $this->data['bsc_supervisor_q3'] = $bsc_supervisor_q3;
        $this->data['bsc_supervisor_q4'] = $bsc_supervisor_q4;


        $this->data['default_structured'] = SettingsCPD::where('id', 1)->sum('no_of_hours');
        $this->data['default_ethics'] = SettingsCPD::where('id', 2)->sum('no_of_hours');
        $this->data['default_rules'] = SettingsCPD::where('id', 3)->sum('no_of_hours');
        $this->data['default_total'] = $this->data['default_structured'] + $this->data['default_ethics'] + $this->data['default_rules'];
        $this->data['default_gi'] = SettingsCPD::where('id', 4)->sum('no_of_hours');

        $this->data['today_month'] = Carbon::now()->addMonth(-1)->format('F');
        $this->data['today_month_case'] = Carbon::now()->addMonth(0)->format('F');
        $this->data['today_year'] = Carbon::now()->format('Y');

        $get_gi_ids = ProviderClassification::where('is_gi', 1)->lists('id');

        $get_aviva_cat1 = array_unique(ProductMain::where('provider_id', 1)
                                ->where(function($query) {
                                    $query->where('name', 'LIKE', 'Ideal Income')
                                          ->orWhere('name', 'LIKE', 'MyCare')
                                          ->orWhere('name', 'LIKE', 'MyCarePlus')
                                          ->orWhere('name', 'LIKE', 'MyEarly Critical Illness Plan')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 1)')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 2)')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 3)')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 4)')
                                          ->orWhere('name', 'LIKE', 'MyProtector - MoneyBack')
                                          ->orWhere('name', 'LIKE', 'MyProtector - Decreasing')
                                          ->orWhere('name', 'LIKE', 'MyProtector - Level Plus')
                                          ->orWhere('name', 'LIKE', 'MyHealth Plus Option A')
                                          ->orWhere('name', 'LIKE', 'MyHealth Plus Option C')
                                          ->orWhere('name', 'LIKE', 'MyShield Plan 1')
                                          ->orWhere('name', 'LIKE', 'MyShield Plan 2')
                                          ->orWhere('name', 'LIKE', 'MyShield Plan 3')
                                          ->orWhere('name', 'LIKE', 'MyShield Standard Plan');
                                  })
                                ->lists('id')->toArray());
        // dd($get_aviva_cat1);
        // $avs = '';
        // foreach ($get_aviva_cat1 as $key => $value) {
        // $avs .= $value . ',';  
        // }
        // dd($avs);

        $this->data['groups'] = Group::with('groupinfo')->where('status', 1)->where('user_id', '!=', 8)->orderBy('code', 'asc')->lists('id');

        $get_gis = array_unique(Product::whereIn('classification_id', $get_gi_ids)
                                ->lists('main_id')->toArray());

        $count_sales = User::where('usertype_id', 8)->where('status', 1)->count();
        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->addMonth(0)->format('m');
        $month_year = Carbon::now()->addMonth(0)->format('Y');
        $month_f = Carbon::now()->format('F');
        $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
        $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
        $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_month_now = Carbon::now()->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        $end_month_now = Carbon::now()->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
        $start_year = $start_request_year->format('Y-m-d') . ' 23:59:59';
        // $end_year = $end_month;
        $end_year = Carbon::now()->format('Y-m-d') . ' 23:59:59';

        // $get_rank = PayrollComputation::select('user_id', 'users.name')
        //                               ->leftjoin('users', 'payroll_computations.user_id', '=', 'users.id')
        //                               ->selectRaw('sum(premium_freq_mtd) as total_premium')
        //                               ->where('comp_code', '!=', 'TF')
        //                               ->where('comp_code', '!=', 'OF')
        //                               ->where('payroll_computations.status', 1)
        //                               ->groupBy('user_id')
        //                               ->orderBy('total_premium', 'desc')
        //                               ->orderBy('users.name', 'asc')
        //                               ->whereIn('user_id', $get_users)
        //                               ->get()
        //                               ->toArray();

        $get_rank = Inception::select('user_id', 'users.name')
                              ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                              ->selectRaw('ape_ytd as total_premium')
                              ->groupBy('user_id')
                              ->orderBy('total_premium', 'desc')
                              ->whereIn('user_id', $get_users)
                              ->get()
                              ->toArray();

        // dd($get_rank);
        // $get_rank = ProductionCases::select('user_id', 'users.name')
        //                               ->leftjoin('users', 'production_cases.user_id', '=', 'users.id')
        //                               ->selectRaw('sum(ape) as total_ape')
        //                               ->where('production_cases.status', 1)
        //                               ->groupBy('user_id')
        //                               ->orderBy('total_ape', 'desc')
        //                               ->orderBy('users.name', 'asc')
        //                               ->whereIn('user_id', $get_users)
        //                               ->get()
        //                               ->toArray();

        $rank = '-'; 
        foreach ($get_rank as $grkey => $grfield) {
          if ($grfield['user_id'] == Auth::user()->id) {
            $rank = $grkey + 1;
          }
        }

        // dd($rank);

        if ($rank == '-') {
          // $get_ranked_users = PayrollComputation::select('user_id', 'users.name')
          //                               ->leftjoin('users', 'payroll_computations.user_id', '=', 'users.id')
          //                               ->selectRaw('sum(premium_freq_mtd) as total_premium')
          //                               ->where('comp_code', '!=', 'TF')
          //                               ->where('comp_code', '!=', 'OF')
          //                               ->where('payroll_computations.status', 1)
          //                               ->groupBy('user_id')
          //                               ->orderBy('total_premium', 'desc')
          //                               ->orderBy('users.name', 'asc')
          //                               ->whereIn('user_id', $get_users)
          //                               ->lists('user_id');

          $get_ranked_users = Inception::select('user_id')
                                        ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                                        ->selectRaw('sum(ape_ytd) as total_premium')
                                        ->groupBy('user_id')
                                        ->orderBy('total_premium', 'desc')
                                        ->whereIn('user_id', $get_users)
                                        ->lists('user_id');

          // $get_ranked_users = ProductionCases::select('user_id', 'users.name')
          //                               ->leftjoin('users', 'production_cases.user_id', '=', 'users.id')
          //                               ->selectRaw('sum(ape) as total_ape')
          //                               ->where('production_cases.status', 1)
          //                               ->groupBy('user_id')
          //                               ->orderBy('total_ape', 'desc')
          //                               ->orderBy('users.name', 'asc')
          //                               ->whereIn('user_id', $get_users)
          //                               ->lists('user_id');

          $get_not_users = User::where('usertype_id', 8)
                            ->where('id', '!=', 8)
                            ->whereNotIn('id', $get_ranked_users)
                            ->where('status', 1)
                            ->orderBy('name', 'asc')
                            ->lists('id')->toArray();

          $rank = '-';
          foreach ($get_not_users as $gnukey => $gnuvalue) {
            if ($gnuvalue == Auth::user()->id) {
              $rank = $gnukey + count($get_ranked_users) + 1;
            }
          }
        }

        $get_case_users = User::where('usertype_id', 8)
                              ->where('id', '!=', 8)
                              ->where(function($query) {
                                  $query->where('status', 1)
                                        ->orWhere('status', 0);
                                })
                              ->lists('id');

        $this->data['rank'] = $rank;

        // $this->data['total_mtd_ape_case'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                               $query->where('submission_date', '>=', $start_month)
        //                                     ->where('submission_date', '<=', $end_month)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                               $query->where('submission_date', '>=', $start_month)
        //                                     ->where('submission_date', '<=', $end_month)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_mtd_ape_gi_case'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_gi_case'] += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_gi_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_mtd_ape_gi_case_count'] += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_mtd_ape_aviva_case'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_aviva_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_case'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                               $query->where('submission_date', '>=', $start_year)
        //                                     ->where('submission_date', '<=', $end_year)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                               $query->where('submission_date', '>=', $start_year)
        //                                     ->where('submission_date', '<=', $end_year)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_gi_case'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_gi_case'] += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_gi_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_gi_case_count'] += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_aviva_case'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_aviva_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->count();

        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->addMonth(-1)->format('m');
        $month_year = Carbon::now()->addMonth(-1)->format('Y');
        $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
        $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
        $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_month_now = Carbon::now()->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        $end_month_now = Carbon::now()->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
        $start_year = $start_request_year->format('Y-m-d') . ' 23:59:59';
        $end_year = $end_month;

        // $this->data['total_mtd_ape_gross_inception'] = PayrollComputation::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('batch_date', '>=', $start_month)
        //                                       ->where('batch_date', '<=', $end_month)
        //                                       ->where('comp_code', '!=', 'TF')
        //                                       ->where('comp_code', '!=', 'OF')
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_users)->sum('gross_revenue_mtd');

        // $this->data['total_mtd_ape_inception'] = PayrollInception::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('incept_date', '>=', $start_month)
        //                                       ->where('incept_date', '<=', $end_month);
        //                           })->whereNotIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->sum('ape');

        // $this->data['total_mtd_ape_aviva_inception'] = PayrollInception::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('provider_id', 1)
        //                                       ->where('incept_date', '>=', $start_month)
        //                                       ->where('incept_date', '<=', $end_month);
        //                           })->where(function($query) {
        //                                 $query->where('category_id', 1)
        //                                       ->orWhere('category_id', 2);
        //                           })->whereIn('user_id', $get_users)->sum('ape');
        
        // $this->data['total_mtd_ape_gi_inception'] = PayrollInception::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('provider_id', 1)
        //                                       ->where('incept_date', '>=', $start_month)
        //                                       ->where('incept_date', '<=', $end_month);
        //                           })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->sum('ape');

        // $this->data['total_mtd_ape_inception_count'] = PayrollInception::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('incept_date', '>=', $start_month)
        //                                       ->where('incept_date', '<=', $end_month)
        //                                       ->where('ape', '>', 0);
        //                           })->whereNotIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->count();
        
        // $this->data['total_mtd_ape_aviva_inception_count'] = PayrollInception::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('provider_id', 1)
        //                                       ->where('incept_date', '>=', $start_month)
        //                                       ->where('incept_date', '<=', $end_month)
        //                                       ->where('ape', '>', 0);
        //                           })->where(function($query) {
        //                                 $query->where('category_id', 1)
        //                                       ->orWhere('category_id', 2);
        //                           })->whereNotIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->count();

        // $this->data['total_mtd_ape_gi_inception_count'] = PayrollInception::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('incept_date', '>=', $start_month)
        //                                       ->where('incept_date', '<=', $end_month)
        //                                       ->where('ape', '>', 0);
        //                           })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->count();


        // $this->data['total_ytd_ape_gross_inception'] = PayrollComputation::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('batch_date', '>=', $start_year)
        //                                       ->where('batch_date', '<=', $end_year)
        //                                       ->where('comp_code', '!=', 'TF')
        //                                       ->where('comp_code', '!=', 'OF')
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_users)->sum('gross_revenue_mtd');

        // $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('incept_date', '>=', $start_year)
        //                                       ->where('incept_date', '<=', $end_year);
        //                           })->whereIn('user_id', $get_users)->lists('upload_id')->toArray());

        // $total_ytd = 0;
        // $total_ytd_aviva = 0;
        // $total_ytd_gi = 0;

        // $policies_not = [];
        // $policies_not_aviva = [];
        // $policies_not_gi = [];

        // foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

        //   $total_ytd += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
        //                                   $query->where('incept_date', '>=', $start_year)
        //                                         ->where('incept_date', '<=', $end_year)
        //                                         ->where('upload_id', $pbvalue);
        //                           })->whereNotIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->whereNotIn('policy_no', $policies_not)->sum('ape');

        //   $policies_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
        //                                   $query->where('incept_date', '>=', $start_year)
        //                                         ->where('incept_date', '<=', $end_year)
        //                                         ->where('upload_id', $pbvalue);
        //                           })->whereNotIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->whereNotIn('policy_no', $policies_not)->lists('policy_no')->toArray());

        //   $policies_not = array_merge($policies_not, $policies_ytd);

        //   $total_ytd_aviva += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
        //                                   $query->where('provider_id', 1)
        //                                         ->where('incept_date', '>=', $start_year)
        //                                         ->where('incept_date', '<=', $end_year)
        //                                         ->where('upload_id', $pbvalue);
        //                             })->where(function($query) {
        //                                   $query->where('category_id', 1)
        //                                         ->orWhere('category_id', 2);
        //                             })->whereIn('user_id', $get_users)->whereNotIn('policy_no', $policies_not_aviva)->sum('ape');

        //   $policies_ytd_aviva = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
        //                                   $query->where('provider_id', 1)
        //                                         ->where('incept_date', '>=', $start_year)
        //                                         ->where('incept_date', '<=', $end_year)
        //                                         ->where('upload_id', $pbvalue);
        //                             })->where(function($query) {
        //                                   $query->where('category_id', 1)
        //                                         ->orWhere('category_id', 2);
        //                             })->whereIn('user_id', $get_users)->whereNotIn('policy_no', $policies_not_aviva)->lists('policy_no')->toArray());
          
        //   $policies_not_aviva = array_merge($policies_not_aviva, $policies_ytd_aviva);

        //   $total_ytd_gi +=  PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
        //                                 $query->where('incept_date', '>=', $start_year)
        //                                       ->where('incept_date', '<=', $end_year)
        //                                       ->where('upload_id', $pbvalue);
        //                           })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->whereNotIn('policy_no', $policies_not_gi)->sum('ape');

        //   $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
        //                                 $query->where('incept_date', '>=', $start_year)
        //                                       ->where('incept_date', '<=', $end_year)
        //                                       ->where('upload_id', $pbvalue);
        //                           })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());
        // }

        // $this->data['total_ytd_ape_inception'] = $total_ytd;
        // $this->data['total_ytd_ape_aviva_inception'] = $total_ytd_aviva;
        // $this->data['total_ytd_ape_gi_inception']= $total_ytd_gi;

        // $this->data['total_ytd_ape_inception_count'] = PayrollInception::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('incept_date', '>=', $start_year)
        //                                       ->where('incept_date', '<=', $end_year)
        //                                       ->where('ape', '>', 0);
        //                           })->whereNotIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->groupBy('policy_no')->lists('policy_no');
        
        // $this->data['total_ytd_ape_inception_count'] = count($this->data['total_ytd_ape_inception_count']);

        // $this->data['total_ytd_ape_aviva_inception_count'] = PayrollInception::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('provider_id', 1)
        //                                       ->where('incept_date', '>=', $start_year)
        //                                       ->where('incept_date', '<=', $end_year)
        //                                       ->where('ape', '>', 0);
        //                           })->where(function($query) {
        //                                 $query->where('category_id', 1)
        //                                       ->orWhere('category_id', 2);
        //                           })->whereNotIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->groupBy('policy_no')->lists('policy_no');

        // $this->data['total_ytd_ape_aviva_inception_count'] = count($this->data['total_ytd_ape_aviva_inception_count']);

        // $this->data['total_ytd_ape_gi_inception_count'] = PayrollInception::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('incept_date', '>=', $start_year)
        //                                       ->where('incept_date', '<=', $end_year)
        //                                       ->where('ape', '>', 0);
        //                           })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->groupBy('policy_no')->lists('policy_no');

        // $this->data['total_ytd_ape_gi_inception_count'] = count($this->data['total_ytd_ape_gi_inception_count']);

        $this->data['count_sales'] = $count_sales;

        $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

        $check_advisor = SalesAdvisor::where('user_id', Auth::user()->id)->first();
        
        $this->data['ranking'] = '-';
        
        if ($check_supervisor) {
          $this->data['ranking'] = "SUPERVISOR";
        } else if ($check_supervisor) {
          $this->data['ranking'] = "ADVISOR";
        }

        $dashboard_case = '';
        $dashboard_inception = '';
        $dashboard_replicate = '';

        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->addMonth(0)->format('m');
        $month_year = Carbon::now()->addMonth(0)->format('Y');
        $month_f = Carbon::now()->format('F');
        $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
        $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
        $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_month_now = Carbon::now()->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        $end_month_now = Carbon::now()->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
        $start_year = $start_request_year->format('Y-m-d') . ' 00:00:00';
        // $end_year = $end_month;
        $end_year = Carbon::now()->format('Y-m-d') . ' 23:59:59';

        $this->data['mtd_ape_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->count();

        $this->data['mtd_ape_case_total'] = number_format(ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->sum('ape'), 2);

        $this->data['mtd_ape_gi_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->count();

        $this->data['mtd_ape_gi_case_count'] += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->count();

        $this->data['mtd_ape_gi_case_total'] = number_format(ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->sum('ape'), 2);

        $this->data['mtd_ape_gi_case_total'] += number_format(GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->sum('ape'), 2);

        $this->data['mtd_ape_aviva_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->count();

        $this->data['mtd_ape_aviva_case_total'] = number_format(ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->sum('ape'), 2);
      
        $this->data['ytd_ape_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->count();

        $this->data['ytd_ape_case_total'] = number_format(ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->sum('ape'), 2);

        $this->data['ytd_ape_gi_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->count();

        $this->data['ytd_ape_gi_case_count'] += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->count();

        $this->data['ytd_ape_gi_case_total'] = number_format(ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->sum('ape'), 2);

        $this->data['ytd_ape_gi_case_total'] += number_format(GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->sum('ape'), 2);

        $this->data['ytd_ape_aviva_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->count();

        $this->data['ytd_ape_aviva_case_total'] = number_format(ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('user_id', Auth::user()->id)
                                              ->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->sum('ape'), 2);


        // $get_groups = Group::with('groupinfo')->where('status', 1)->where('user_id', '!=', 8)->orderBy('code', 'asc')->get();

        // foreach ($get_groups as $ggkey => $ggfield) {

        //   $get_owner_users = Group::whereIn('user_id', $get_case_users)->where('id', $ggfield->id)->where('status', 1)->lists('user_id');
        //   $get_supervisor_users = SalesSupervisor::whereIn('user_id', $get_case_users)->where('group_id', $ggfield->id)->lists('user_id');

        //   $get_group_users = array_merge($get_owner_users->toArray(), $get_supervisor_users->toArray());

        //   $get_supervisors_users_id = SalesSupervisor::where('group_id', $ggfield->id)->lists('id');

        //   foreach ($get_supervisors_users_id as $gsuikey => $gsuivalue) {
        //     $get_advisor_users = SalesAdvisor::where('supervisor_id', $gsuivalue)->whereIn('user_id', $get_case_users)->lists('user_id');
        //     $get_group_users = array_merge($get_group_users, $get_advisor_users->toArray());
        //   }

        //   $group_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->count();

        //   $group_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->count();

        //   $group_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->count();

        //   $group_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->count();

        //   $dashboard_case .= '<tr class="group-tr expand-table">
        //                         <td class="first-td td-hover" data-toggle="collapse" onclick="dashboard_case();"><b><u>' .  $ggfield->groupinfo->preferred_name . '</u></b></td>
        //                         <td class="group-td">' . number_format($group_mtd_ape_case, 2) . ' <b>(' . $group_mtd_ape_case_count . ')</b></td>
        //                         <td class="group-td hide">N/A</td>
        //                         <td class="group-td">' . number_format($group_mtd_ape_aviva_case, 2) . ' <b>(' . $group_mtd_ape_aviva_case_count . ')</b></td>
        //                         <td class="group-td">' . number_format($group_mtd_ape_gi_case, 2) . ' <b>(' . $group_mtd_ape_gi_case_count . ')</b></td>
        //                         <td class="group-td">' . number_format($group_ytd_ape_case, 2) . ' <b>(' . $group_ytd_ape_case_count . ')</b></td>
        //                         <td class="group-td hide">N/A</td>
        //                         <td class="group-td">' . number_format($group_ytd_ape_aviva_case, 2) . ' <b>(' . $group_ytd_ape_aviva_case_count . ')</b></td>
        //                         <td class="group-td">' . number_format($group_ytd_ape_gi_case, 2) . ' <b>(' . $group_ytd_ape_gi_case_count . ')</b></td>
        //                       </tr>';
        // }

        // $dashboard_case .= '<tr class="footer-tr">
        //                       <td class="footer-td" colspan="7">Updated as of <b>' . Carbon::now()->format('F j, Y') . '</b></td>
        //                     </tr>';

        // $year = Carbon::now()->format('Y');
        // $month = Carbon::now()->addMonth(-1)->format('m');
        // $month_year = Carbon::now()->addMonth(-1)->format('Y');
        // $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
        // $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
        // $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        // $start_month_now = Carbon::now()->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        // $end_month_now = Carbon::now()->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        // $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
        // $start_year = $start_request_year->format('Y-m-d') . ' 00:00:00';
        // $end_year = $end_month;
        
        // $get_batch = InceptionBatch::where('incept_date', '>=', $start_month)
        //                                 ->where('incept_date', '<=', $end_month)
        //                                 ->orderBy('created_at', 'desc')
        //                                 ->first();

        //MTD
        // $mtd_ape_inception_total = Inception::where(function($query) use ($start_month, $end_month) {
        //                                           $query->where('incept_date', '>=', $start_month)
        //                                                 ->where('incept_date', '<=', $end_month);
        //                                       })
        //                                     ->where('user_id', Auth::user()->id)
        //                                     ->sum('ape_mtd');

        // $mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
        //                                                 $query->where('incept_date', '>=', $start_month)
        //                                                       ->where('incept_date', '<=', $end_month);
        //                                             })
        //                                           ->where('user_id', Auth::user()->id)
        //                                           ->sum('ape_count_mtd');
        // $this->data['mtd_ape_inception_count'] = $mtd_ape_inception_count;

        $mtd_ape_inception_total = Inception::where('user_id', Auth::user()->id)
                                            ->orderBy('incept_date', 'desc')
                                            ->first();

                                            // dd($mtd_ape_inception_total);

        $this->data['mtd_ape_inception_total'] = number_format($mtd_ape_inception_total ? $mtd_ape_inception_total->ape_mtd : 0, 2);

        $mtd_ape_inception_count = Inception::where('user_id', Auth::user()->id)
                                            ->orderBy('incept_date', 'desc')
                                            ->first();

        $this->data['mtd_ape_inception_count'] = $mtd_ape_inception_count ? $mtd_ape_inception_count->ape_count_mtd : 0;
        $mtd_ape_gross_inception_total_alkane = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('user_id', Auth::user()->id)
                                                            ->where('batch_date', '>=', $start_month)
                                                            ->where('batch_date', '<=', $end_month)
                                                            ->where('comp_code', '!=', 'TF')
                                                            ->where('comp_code', '!=', 'OF')
                                                            ->where('status', 1);
                                                })->sum('gross_revenue_mtd');
        $this->data['mtd_ape_gross_inception_total'] = number_format($mtd_ape_gross_inception_total_alkane, 2);

        // $mtd_ape_gross_inception_total_alkane = PayrollComputation::where(function($query){
        //                                               $query->where('user_id', Auth::user()->id)
        //                                                     ->where('comp_code', '!=', 'TF')
        //                                                     ->where('comp_code', '!=', 'OF')
        //                                                     ->where('status', 1);
        //                                         })->orderBy('incept_date','desc')->first();

        // $this->data['mtd_ape_gross_inception_total'] = number_format($mtd_ape_gross_inception_total_alkane ? $mtd_ape_gross_inception_total_alkane->gross_revenue_mtd : 0, 2);

        // $mtd_ape_aviva_inception_total = Inception::where(function($query) use ($start_month, $end_month) {
        //                                                 $query->where('incept_date', '>=', $start_month)
        //                                                       ->where('incept_date', '<=', $end_month);
        //                                             })
        //                                           ->where('user_id', Auth::user()->id)
        //                                           ->sum('cat_1_mtd');
        // $this->data['mtd_ape_aviva_inception_total'] = number_format($mtd_ape_aviva_inception_total, 2);

        $mtd_ape_aviva_inception_total = Inception::where('user_id', Auth::user()->id)
                                                  ->orderBy('incept_date','desc')
                                                  ->first();
      
        $this->data['mtd_ape_aviva_inception_total'] = number_format($mtd_ape_aviva_inception_total ? $mtd_ape_aviva_inception_total->cat_1_mtd : 0, 2);

        // $mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
        //                                                       $query->where('incept_date', '>=', $start_month)
        //                                                             ->where('incept_date', '<=', $end_month);
        //                                                   })
        //                                                 ->where('user_id', Auth::user()->id)
        //                                                 ->sum('cat_1_count_mtd');
      
        // $this->data['mtd_ape_aviva_inception_count'] = $mtd_ape_aviva_inception_count;

        $mtd_ape_aviva_inception_count = Inception::where('user_id', Auth::user()->id)
                                                        ->orderBy('incept_date','desc')
                                                        ->first();
      
        $this->data['mtd_ape_aviva_inception_count'] = $mtd_ape_aviva_inception_count ? $mtd_ape_aviva_inception_count->cat_1_count_mtd : 0;

        $mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->where('user_id', Auth::user()->id)->whereIn('category_id', $get_gi_ids)->groupBy('policy_no')->selectRaw('SUM(premium_freq_mtd) as total_premium')->lists('total_premium');

        $count = 0;
        foreach ($mtd_ape_gi_inception_count as $key => $value) {
          if (floatval($value) > 0) {
            $count++;
          }
        }

        $this->data['mtd_ape_gi_inception_count'] = $count;

        // $this->data['mtd_ape_gi_inception_total'] = PayrollComputation::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('batch_date', '>=', $start_month)
        //                                       ->where('batch_date', '<=', $end_month)
        //                                       ->where('comp_code', '!=', 'TF')
        //                                       ->where('comp_code', '!=', 'OF')
        //                                       ->where('status', 1);
        //                           })->where('user_id', Auth::user()->id)->whereIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');
        $this->data['mtd_ape_gi_inception_total'] = number_format(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->where('user_id', Auth::user()->id)->whereIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd'),2);

        // $mtd_ape_gi_inception_total_alkane = PayrollComputation::where(function($query){
        //                                 $query->where('comp_code', '!=', 'TF')
        //                                       ->where('comp_code', '!=', 'OF')
        //                                       ->where('status', 1);
        //                           })->where('user_id', Auth::user()->id)->whereIn('category_id', $get_gi_ids)->orderBy('incept_date','desc')->first();
        // // dd($mtd_ape_gi_inception_total_alkane);
        // // dd($get_gi_ids);
        //  $this->data['mtd_ape_gi_inception_total'] = $mtd_ape_gi_inception_total_alkane ? $mtd_ape_gi_inception_total_alkane->gross_revenue_mtd : 0;

        //YTD - ALKANE COMMENT - start
        $ytd_ape_inception_total = Inception::where(function($query) use ($start_year, $end_year) {
                                                  $query->where('incept_date', '>=', $start_year)
                                                        ->where('incept_date', '<=', $end_year);
                                              })
                                            ->where('user_id', Auth::user()->id)
                                            ->sum('ape_ytd');
        
        $this->data['ytd_ape_inception_total'] = number_format($ytd_ape_inception_total, 2);

        $ytd_ape_inception_count = Inception::where(function($query) use ($start_year, $end_year) {
                                                        $query->where('incept_date', '>=', $start_year)
                                                              ->where('incept_date', '<=', $end_year);
                                                    })
                                                  ->where('user_id', Auth::user()->id)
                                                  ->sum('ape_count_ytd');
        //YTD - ALKANE COMMENT - end

        // $ytd_ape_inception_total = Inception::where(function($query) use ($start_year, $end_year) {
        //                                           $query->where('incept_date', '>=', $start_year)
        //                                                 ->where('incept_date', '<=', $end_year);
        //                                       })
        //                                     ->where('user_id', Auth::user()->id)
        //                                     ->orderBy('incept_date','desc')
        //                                     ->first();
        
        // $this->data['ytd_ape_inception_total'] = number_format($ytd_ape_inception_total ? $ytd_ape_inception_total->ape_ytd : 0, 2);

        // $ytd_ape_inception_count = Inception::where(function($query) use ($start_year, $end_year) {
        //                                                 $query->where('incept_date', '>=', $start_year)
        //                                                       ->where('incept_date', '<=', $end_year);
        //                                             })
        //                                           ->where('user_id', Auth::user()->id)
        //                                           ->orderBy('incept_date','desc')
        //                                           ->first();
        
        $this->data['ytd_ape_inception_count'] = $ytd_ape_inception_count;
        // $get_gi_ids = ProviderClassification::where('is_gi', 1)->lists('id');
        // $this->data['ytd_ape_gross_inception_total'] = number_format(PayrollComputation::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('user_id', Auth::user()->id)
        //                                       ->where('batch_date', '>=', $start_year)
        //                                       ->where('batch_date', '<=', $end_year)
        //                                       ->where('comp_code', '!=', 'TF')
        //                                       ->where('comp_code', '!=', 'OF')
        //                                       ->where('status', 1);
        //                           })->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd'), 2);
        // dd($this->data['ytd_ape_gross_inception_total']);

        //ALKANEPOGI
        
        // $ytd_ape_gross_inception_total_alk = PayrollComputation::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('batch_date', '>=', '2016-01-01 00:00:00')
        //                                         ->where('batch_date', '<=', '2016-12-31 23:59:59')
        //                                         ->where('comp_code', '!=', 'TF')
        //                                         ->where('comp_code', '!=', 'OF')
        //                                         ->where('status', 1);
        //                             })->where('user_id', Auth::user()->id)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');


        $ytd_ape_gross_inception_total_alk = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->where('user_id', Auth::user()->id)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

        $this->data['ytd_ape_gross_inception_total'] = number_format($ytd_ape_gross_inception_total_alk, 2);
        // dd($end_month);
        // $ytd_ape_aviva_inception_total = Inception::where(function($query) use ($start_year, $end_year) {
        //                                                 $query->where('incept_date', '>=', $start_year)
        //                                                       ->where('incept_date', '<=', $end_year);
        //                                             })
        //                                           ->where('user_id', Auth::user()->id)
        //                                           ->sum('cat_1_ytd');
        
        // $this->data['ytd_ape_aviva_inception_total'] = number_format($ytd_ape_aviva_inception_total, 2);

        $ytd_ape_aviva_inception_total = Inception::where('user_id', Auth::user()->id)
                                                  ->orderBy('incept_date','desc')
                                                  ->first();
        
        $this->data['ytd_ape_aviva_inception_total'] = number_format($ytd_ape_aviva_inception_total ? $ytd_ape_aviva_inception_total->cat_1_ytd : 0, 2);

        // $ytd_ape_aviva_inception_total = Inception::where('user_id', Auth::user()->id)
        //                                           ->orderBy('incept_date','desc')
        //                                           ->first();
        //                                           // dd($ytd_ape_aviva_inception_total);
        
        // $this->data['ytd_ape_aviva_inception_total'] = number_format($ytd_ape_aviva_inception_total ? $ytd_ape_aviva_inception_total->cat_1_ytd : 0, 2);

        // $ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_year, $end_year) {
        //                                                       $query->where('incept_date', '>=', $start_year)
        //                                                             ->where('incept_date', '<=', $end_year);
        //                                                   })
        //                                                 ->where('user_id', Auth::user()->id)
        //                                                 ->sum('cat_1_count_ytd');
      
        // $this->data['ytd_ape_aviva_inception_count'] = $ytd_ape_aviva_inception_count;

        $ytd_ape_aviva_inception_count = Inception::where('user_id', Auth::user()->id)
                                                        ->orderBy('incept_date','desc')
                                                        ->first();
      
        $this->data['ytd_ape_aviva_inception_count'] = $ytd_ape_aviva_inception_count ? $ytd_ape_aviva_inception_count->cat_1_count_ytd : 0;

        $policies_batch_ytd = array_unique(PayrollInception::select('upload_id')->where(function($query) use ($start_year, $end_year) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('batch_date', '>=', $start_year)
                                          ->where('batch_date', '<=', $end_year);
                              })->orderBy('upload_id', 'asc')->lists('upload_id')->toArray());

        $total_ytd_gi = 0;
        $policies_not_gi = [];



        foreach ($policies_batch_ytd as $pbkey => $pbvalue) {
          $total_ytd_gi +=  PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('upload_id', $pbvalue);
                                })->where('user_id', Auth::user()->id)->whereIn('category_id', $get_gi_ids)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

          $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                      $query->where('user_id', Auth::user()->id)
                                            ->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('upload_id', $pbvalue);
                                })->whereIn('category_id', $get_gi_ids)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

          $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
        }

        $this->data['ytd_ape_gi_inception_total'] = number_format($total_ytd_gi, 2);

        // $ytd_ape_aviva_inception_total = Inception::where(function($query) use ($start_year, $end_year) {
        //                                                 $query->where('incept_date', '>=', $start_year)
        //                                                       ->where('incept_date', '<=', $end_year);
        //                                             })
        //                                           ->where('user_id', Auth::user()->id)
        //                                           ->sum('cat_1_ytd');
      
        // $this->data['ytd_ape_aviva_inception_total'] = number_format($ytd_ape_aviva_inception_total, 2);

        // $ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_year, $end_year) {
        //                                                       $query->where('incept_date', '>=', $start_year)
        //                                                             ->where('incept_date', '<=', $end_year);
        //                                                   })
        //                                                 ->where('user_id', Auth::user()->id)
        //                                                 ->sum('cat_1_count_ytd');
      
        // $this->data['ytd_ape_aviva_inception_count'] = $ytd_ape_aviva_inception_count;

        $ytd_ape_aviva_inception_count = Inception::where('user_id', Auth::user()->id)
                                                        ->orderBy('incept_date','desc')
                                                        ->first();
        
        $this->data['ytd_ape_aviva_inception_count'] = $ytd_ape_aviva_inception_count ? $ytd_ape_aviva_inception_count->cat_1_count_ytd : 0;

        $ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->where('user_id', Auth::user()->id)->whereIn('category_id', $get_gi_ids)->groupBy('policy_no')->selectRaw('SUM(premium_freq_mtd) as total_premium')->lists('total_premium');

        $count = 0;
        foreach ($ytd_ape_gi_inception_count as $key => $value) {
          if (floatval($value) > 0) {
            $count++;
          }
        }
        $this->data['ytd_ape_gi_inception_count'] = $ytd_ape_gi_inception_count = $count;

        // $dashboard_inception = DashboardInception::orderBy('created_at', 'desc')->first();

        // if ($dashboard_inception) {
        //   $dashboard_inception = $dashboard_inception->dashboard;
        // }

        $batch_date = Carbon::now()->addMonth(-1)->startOfMonth()->format('Y-m-d');

        $dashboard_inception = DashboardInception::where('incept_date', '=', $batch_date . ' 00:00:00')->orderBy('created_at', 'desc')->first();
        $dashboard_date = Carbon::now()->addMonth(-1)->format('M Y');

        if ($dashboard_inception) {
          $dashboard_date = date_format(date_create(substr($dashboard_inception->incept_date, 0,10)),'M Y');
          $dashboard_inception = $dashboard_inception->dashboard;
        } else if (!$dashboard_inception) {
          $dashboard_inception = DashboardInception::orderBy('incept_date', 'desc')->orderBy('created_at', 'desc')->first();
          if ($dashboard_inception) {
            $dashboard_date = date_format(date_create(substr($dashboard_inception->incept_date, 0,10)),'M Y');
            $dashboard_inception = $dashboard_inception->dashboard;
          }
        }

        // $dashboard_case = DashboardSubmission::orderBy('created_at', 'desc')->first();

        // if ($dashboard_case) {
        //   $dashboard_case = $dashboard_case->dashboard;
        // }

        $case_date = Carbon::now()->addMonth(0)->startOfMonth()->format('Y-m-d');

        $dashboard_case = DashboardSubmission::where('incept_date', '=', $case_date . ' 00:00:00')->orderBy('created_at', 'desc')->first();

        $dashboard_case_date = Carbon::now()->addMonth(-1)->format('M Y');

        if ($dashboard_case) {
          $dashboard_case_date = date_format(date_create(substr($dashboard_case->incept_date, 0,10)),'M Y');
          $dashboard_case = $dashboard_case->dashboard;
        } else if (!$dashboard_case) {
          $dashboard_case = DashboardSubmission::orderBy('incept_date', 'desc')->orderBy('created_at', 'desc')->first();
          if ($dashboard_case) {
            $dashboard_case_date = date_format(date_create(substr($dashboard_case->incept_date, 0,10)),'M Y');
            $dashboard_case = $dashboard_case->dashboard;
          }
        }

        $get_batch = InceptionBatch::where('incept_date', '>=', $start_month)
                                        ->where('incept_date', '<=', $end_month)
                                        ->orderBy('created_at', 'desc')
                                        ->first();
        $batch_id = null;
        if ($get_batch) {
          $batch_id = $get_batch->id;
        }

        $this->data['batch'] = $batch_id;
        $this->data['dashboard_case'] = $dashboard_case;
        $this->data['dashboard_case_date'] = $dashboard_case_date;
        $this->data['dashboard_inception'] = $dashboard_inception;
        $this->data['dashboard_replicate'] = $dashboard_replicate;
        $this->data['dashboard_date'] = $dashboard_date;

       } else {

        $get_users = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 1)->lists('id');

        $total_ape_ytd = Inception::sum('ape_ytd');

//         $get_rank = Inception::select('user_id', 'users.name', 'users.preferred_name')
//                               ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
//                               ->selectRaw('sum(ape_ytd) as total_premium')
//                               ->groupBy('user_id')
//                               ->orderBy('total_premium', 'desc')
//                               ->whereIn('user_id', $get_users)
//                               ->get();

//         $get_ranked_users = Inception::select('user_id')
//                                       ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
//                                       ->selectRaw('sum(ape_ytd) as total_premium')
//                                       ->groupBy('user_id')
//                                       ->orderBy('total_premium', 'desc')
//                                       ->whereIn('user_id', $get_users)
//                                       ->lists('user_id');
        
        $get_rank = Inception::select('user_id', 'users.name', 'users.preferred_name')
                              ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                              ->selectRaw('ape_ytd as total_premium')
                              ->groupBy('user_id')
                              ->orderBy('total_premium', 'desc')
                              ->whereIn('user_id', $get_users)
                              ->get();

        $get_ranked_users = Inception::select('user_id')
                                      ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                                      ->selectRaw('ape_ytd as total_premium')
                                      ->groupBy('user_id')
                                      ->orderBy('total_premium', 'desc')
                                      ->whereIn('user_id', $get_users)
                                      ->lists('user_id');

        $get_not_users = User::where('usertype_id', 8)
                          ->where('id', '!=', 8)
                          ->whereNotIn('id', $get_ranked_users)
                          ->where('status', 1)
                          ->orderBy('name', 'asc')
                          ->get();

        $ranking = '<div class="col-xs-12 no-padding" style="display: block;">
                    <div class="table-responsive dashboard-responsive">
                      <table class="table dashboard-table">
                      <thead><tr><td class="first-td-ranking first-td-ranking-header">Rank</td><td class="ranking-header">Agent Name</td><td class="ranking-header">APE YTD</td></tr></thead>';

        $ctr_rank = 1;
        foreach ($get_rank as $grkey => $grfield) {

          if ($grfield->total_premium == 0) {
            $ranking .= '<tr class="ranking-span ranking-not">';
          } else {
            $ranking .= '<tr class="ranking-span">';
          }
          
          $ranking .= '<td class="first-td-ranking">' . $ctr_rank . '</td><td class="ranking-name">' . $grfield->name . '</td>' .
                  '<td class="ranking-ape">' . number_format($grfield->total_premium, 2) . '</td></tr>';  
          $ctr_rank++;
        }

        foreach ($get_not_users as $gnrkey => $gnrfield) {
          $ranking .= '<tr class="ranking-span ranking-not">' .
                  '<td class="first-td-ranking">' . $ctr_rank . '</td><td class="ranking-name">' . $gnrfield->name . '</td>' .
                  '<td class="ranking-ape">' . number_format(0, 2) . '</td></tr>';  
          $ctr_rank++;
        }

        $ranking .= '</table></div></div>';

        $this->data['dashboard_rank'] = $ranking;

        $this->data['batch_date'] = Carbon::now()->addMonth(-1)->format('M Y');
        $this->data['case_date'] = Carbon::now()->addMonth(0)->format('M Y');
        $this->data['today_month'] = Carbon::now()->addMonth(-1)->format('F');
        $this->data['today_month_case'] = Carbon::now()->format('F');
        $this->data['today_year'] = Carbon::now()->format('Y');

        $get_gi_ids = ProviderClassification::where('is_gi', 1)->lists('id');

        $get_aviva_cat1 = array_unique(ProductMain::where('provider_id', 1)
                                ->where(function($query) {
                                    $query->where('name', 'LIKE', 'Ideal Income')
                                          ->orWhere('name', 'LIKE', 'MyCare')
                                          ->orWhere('name', 'LIKE', 'MyCarePlus')
                                          ->orWhere('name', 'LIKE', 'MyEarly Critical Illness Plan')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 1)')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 2)')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 3)')
                                          ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 4)')
                                          ->orWhere('name', 'LIKE', 'MyProtector - MoneyBack')
                                          ->orWhere('name', 'LIKE', 'MyProtector - Decreasing')
                                          ->orWhere('name', 'LIKE', 'MyProtector - Level Plus')
                                          ->orWhere('name', 'LIKE', 'MyHealth Plus Option A')
                                          ->orWhere('name', 'LIKE', 'MyHealth Plus Option C')
                                          ->orWhere('name', 'LIKE', 'MyShield Plan 1')
                                          ->orWhere('name', 'LIKE', 'MyShield Plan 2')
                                          ->orWhere('name', 'LIKE', 'MyShield Plan 3')
                                          ->orWhere('name', 'LIKE', 'MyShield Standard Plan');
                                  })
                                ->lists('id')->toArray());

        $this->data['groups_dashboard'] = Group::with('groupinfo')->where('status', 1)->where('user_id', '!=', 8)->orderBy('code', 'asc')->lists('id');

        $get_gis = array_unique(Product::whereIn('classification_id', $get_gi_ids)
                                ->lists('main_id')->toArray());


        $dashboard_case = '';
        $dashboard_replicate = '';

        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->addMonth(0)->format('m');
        $month_year = Carbon::now()->addMonth(0)->format('Y');
        $month_f = Carbon::now()->format('F');
        $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
        $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
        $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_month_now = Carbon::now()->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        $end_month_now = Carbon::now()->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
        $start_year = $start_request_year->format('Y-m-d') . ' 00:00:00';
        // $end_year = $end_month;
        $end_year = Carbon::now()->format('Y-m-d') . ' 23:59:59';

        $get_groups = Group::with('groupinfo')->where('status', 1)->where('user_id', '!=', 8)->orderBy('code', 'asc')->get();

        $get_case_users = User::where('usertype_id', 8)
                              ->where('id', '!=', 8)
                              ->where(function($query) {
                                  $query->where('status', 1)
                                        ->orWhere('status', 0);
                                })
                              ->lists('id');

        // $this->data['total_mtd_ape_case'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                               $query->where('submission_date', '>=', $start_month)
        //                                     ->where('submission_date', '<=', $end_month)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                               $query->where('submission_date', '>=', $start_month)
        //                                     ->where('submission_date', '<=', $end_month)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_mtd_ape_gi_case'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_gi_case'] += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_gi_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_mtd_ape_gi_case_count'] += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_mtd_ape_aviva_case'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_mtd_ape_aviva_case_count'] = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_case'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                               $query->where('submission_date', '>=', $start_year)
        //                                     ->where('submission_date', '<=', $end_year)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                               $query->where('submission_date', '>=', $start_year)
        //                                     ->where('submission_date', '<=', $end_year)
        //                                     ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_gi_case'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_gi_case'] += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_gi_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_gi_case_count'] += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('user_id', $get_case_users)->count();

        // $this->data['total_ytd_ape_aviva_case'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->sum('ape');

        // $this->data['total_ytd_ape_aviva_case_count'] = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                           })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_case_users)->count();

        // foreach ($get_groups as $ggkey => $ggfield) {

        //   $get_owner_users = Group::whereIn('user_id', $get_case_users)->where('id', $ggfield->id)->where('status', 1)->lists('user_id');
        //   $get_supervisor_users = SalesSupervisor::whereIn('user_id', $get_case_users)->where('group_id', $ggfield->id)->lists('user_id');

        //   $get_group_users = array_merge($get_owner_users->toArray(), $get_supervisor_users->toArray());

        //   $get_supervisors_users_id = SalesSupervisor::where('group_id', $ggfield->id)->lists('id');

        //   foreach ($get_supervisors_users_id as $gsuikey => $gsuivalue) {
        //     $get_advisor_users = SalesAdvisor::where('supervisor_id', $gsuivalue)->whereIn('user_id', $get_case_users)->lists('user_id');
        //     $get_group_users = array_merge($get_group_users, $get_advisor_users->toArray());
        //   }

        //   $group_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                 $query->where('submission_date', '>=', $start_month)
        //                                       ->where('submission_date', '<=', $end_month)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->count();

        //   $group_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
        //                                   $query->where('submission_date', '>=', $start_month)
        //                                         ->where('submission_date', '<=', $end_month)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->count();


        //   $group_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                 $query->where('submission_date', '>=', $start_year)
        //                                       ->where('submission_date', '<=', $end_year)
        //                                       ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->count();

        //   $group_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('user_id', $get_group_users)->count();

        //   $group_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->sum('ape');

        //   $group_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
        //                                   $query->where('submission_date', '>=', $start_year)
        //                                         ->where('submission_date', '<=', $end_year)
        //                                         ->where('status', 1);
        //                             })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->count();

        //   $dashboard_case .= '<tr class="group-tr expand-table">
        //                         <td class="first-td td-hover" data-toggle="collapse" onclick="dashboard_case();"><b><u>' .  $ggfield->groupinfo->preferred_name . '</u></b></td>
        //                         <td class="group-td">' . number_format($group_mtd_ape_case, 2) . ' <b>(' . $group_mtd_ape_case_count . ')</b></td>
        //                         <td class="group-td hide">N/A</td>
        //                         <td class="group-td">' . number_format($group_mtd_ape_aviva_case, 2) . ' <b>(' . $group_mtd_ape_aviva_case_count . ')</b></td>
        //                         <td class="group-td">' . number_format($group_mtd_ape_gi_case, 2) . ' <b>(' . $group_mtd_ape_gi_case_count . ')</b></td>
        //                         <td class="group-td">' . number_format($group_ytd_ape_case, 2) . ' <b>(' . $group_ytd_ape_case_count . ')</b></td>
        //                         <td class="group-td hide">N/A</td>
        //                         <td class="group-td">' . number_format($group_ytd_ape_aviva_case, 2) . ' <b>(' . $group_ytd_ape_aviva_case_count . ')</b></td>
        //                         <td class="group-td">' . number_format($group_ytd_ape_gi_case, 2) . ' <b>(' . $group_ytd_ape_gi_case_count . ')</b></td>
        //                       </tr>';     
        // }

        // $dashboard_case .= '<tr class="footer-tr">
        //                       <td class="footer-td" colspan="7">Updated as of <b>' . Carbon::now()->format('F j, Y') . '</b></td>
        //                     </tr>';
        

        $get_batch = InceptionBatch::where('incept_date', '>=', $start_month)
                                        ->where('incept_date', '<=', $end_month)
                                        ->orderBy('created_at', 'desc')
                                        ->first();

        $batch_id = null;
        if ($get_batch) {
          $batch_id = $get_batch->id;
        }

        $this->data['batch'] = $batch_id;

        $batch_date = Carbon::now()->addMonth(-1)->startOfMonth()->format('Y-m-d');

        $dashboard_inception = DashboardInception::where('incept_date', '=', $batch_date . ' 00:00:00')->orderBy('created_at', 'desc')->first();

        $dashboard_date = Carbon::now()->addMonth(-1)->format('M Y');

        if ($dashboard_inception) {
          $dashboard_date = date_format(date_create(substr($dashboard_inception->incept_date, 0,10)),'M Y');
          $dashboard_inception = $dashboard_inception->dashboard;
        } else if (!$dashboard_inception) {
          $dashboard_inception = DashboardInception::orderBy('incept_date', 'desc')->orderBy('created_at', 'desc')->first();
          if ($dashboard_inception) {
            $dashboard_date = date_format(date_create(substr($dashboard_inception->incept_date, 0,10)),'M Y');
            $dashboard_inception = $dashboard_inception->dashboard;
          }
        }

        $case_date = Carbon::now()->addMonth(0)->startOfMonth()->format('Y-m-d');

        $dashboard_case = DashboardSubmission::where('incept_date', '=', $case_date . ' 00:00:00')->orderBy('created_at', 'desc')->first();

        $dashboard_case_date = Carbon::now()->addMonth(-1)->format('M Y');

        if ($dashboard_case) {
          $dashboard_case_date = date_format(date_create(substr($dashboard_case->incept_date, 0,10)),'M Y');
          $dashboard_case = $dashboard_case->dashboard;
        } else if (!$dashboard_case) {
          $dashboard_case = DashboardSubmission::orderBy('incept_date', 'desc')->orderBy('created_at', 'desc')->first();
          if ($dashboard_case) {
            $dashboard_case_date = date_format(date_create(substr($dashboard_case->incept_date, 0,10)),'M Y');
            $dashboard_case = $dashboard_case->dashboard;
          }
        }

        // $dashboard_case = DashboardSubmission::orderBy('created_at', 'desc')->first();

        // if ($dashboard_case) {
        //   $dashboard_case = $dashboard_case->dashboard;
        // }

        $this->data['dashboard_case'] = $dashboard_case;
        $this->data['dashboard_inception'] = $dashboard_inception;
        $this->data['dashboard_date'] = $dashboard_date;
        $this->data['dashboard_case_date'] = $dashboard_case_date;

        $today = Carbon::now()->format('Y-m-d');
        $today_year = Carbon::now()->format('Y');

        $jan = Carbon::createFromFormat('Y-m-d', substr($today_year . '-01-01', 0, 10));
        $feb = Carbon::createFromFormat('Y-m-d', substr($today_year . '-02-01', 0, 10));
        $mar = Carbon::createFromFormat('Y-m-d', substr($today_year . '-03-01', 0, 10));
        $apr = Carbon::createFromFormat('Y-m-d', substr($today_year . '-04-01', 0, 10));
        $may = Carbon::createFromFormat('Y-m-d', substr($today_year . '-05-01', 0, 10));
        $jun = Carbon::createFromFormat('Y-m-d', substr($today_year . '-06-01', 0, 10));
        $jul = Carbon::createFromFormat('Y-m-d', substr($today_year . '-07-01', 0, 10));
        $aug = Carbon::createFromFormat('Y-m-d', substr($today_year . '-08-01', 0, 10));
        $sep = Carbon::createFromFormat('Y-m-d', substr($today_year . '-09-01', 0, 10));
        $oct = Carbon::createFromFormat('Y-m-d', substr($today_year . '-10-01', 0, 10));
        $nov = Carbon::createFromFormat('Y-m-d', substr($today_year . '-11-01', 0, 10));
        $dec = Carbon::createFromFormat('Y-m-d', substr($today_year . '-12-01', 0, 10));

        $start_jan = $jan->format('Y-m-d') . ' 00:00:00';
        $end_jan = $jan->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_feb = $feb->format('Y-m-d') . ' 00:00:00';
        $end_feb = $feb->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_mar = $mar->format('Y-m-d') . ' 00:00:00';
        $end_mar = $mar->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_apr = $apr->format('Y-m-d') . ' 00:00:00';
        $end_apr = $apr->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_may = $may->format('Y-m-d') . ' 00:00:00';
        $end_may = $may->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_jun = $jun->format('Y-m-d') . ' 00:00:00';
        $end_jun = $jun->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_jul = $jul->format('Y-m-d') . ' 00:00:00';
        $end_jul = $jul->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_aug = $aug->format('Y-m-d') . ' 00:00:00';
        $end_aug = $aug->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_sep = $sep->format('Y-m-d') . ' 00:00:00';
        $end_sep = $sep->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_oct = $oct->format('Y-m-d') . ' 00:00:00';
        $end_oct = $oct->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_nov = $nov->format('Y-m-d') . ' 00:00:00';
        $end_nov = $nov->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $start_dec = $dec->format('Y-m-d') . ' 00:00:00';
        $end_dec = $dec->endOfMonth()->format('Y-m-d') . ' 23:59:59';

        $groups = Group::where('status', 1)->get();

        $g_id = 1;
        $xkeys = "[";
        $labels = "[";
        $jan_data = "{ y: '" . $today_year . "-01',";
        $feb_data = "{ y: '" . $today_year . "-02',";
        $mar_data = "{ y: '" . $today_year . "-03',";
        $apr_data = "{ y: '" . $today_year . "-04',";
        $may_data = "{ y: '" . $today_year . "-05',";
        $jun_data = "{ y: '" . $today_year . "-06',";
        $jul_data = "{ y: '" . $today_year . "-07',";
        $aug_data = "{ y: '" . $today_year . "-08',";
        $sep_data = "{ y: '" . $today_year . "-09',";
        $oct_data = "{ y: '" . $today_year . "-10',";
        $nov_data = "{ y: '" . $today_year . "-11',";
        $dec_data = "{ y: '" . $today_year . "-12',";

        foreach ($groups as $gkey => $gfield) {
            
          $get_group_jan = BatchMonthGroup::where(function($query) use ($start_jan, $end_jan, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_jan . " 00:00:00")
                                            ->where('created_at', '<=', $end_jan . " 23:59:59");
                                  })->lists('gross_revenue');
          $jan_val = 0;
          foreach ($get_group_jan as $jankey => $janvalue) {
            $jan_val += $janvalue;
          }

          $get_group_feb = BatchMonthGroup::where(function($query) use ($start_feb, $end_feb, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_feb . " 00:00:00")
                                            ->where('created_at', '<=', $end_feb . " 23:59:59");
                                  })->lists('gross_revenue');
          $feb_val = 0;
          foreach ($get_group_feb as $febkey => $febvalue) {
            $feb_val += $febvalue;
          }

          $get_group_mar = BatchMonthGroup::where(function($query) use ($start_mar, $end_mar, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_mar . " 00:00:00")
                                            ->where('created_at', '<=', $end_mar . " 23:59:59");
                                  })->lists('gross_revenue');
          $mar_val = 0;
          foreach ($get_group_mar as $markey => $marvalue) {
            $mar_val += $marvalue;
          }

          $get_group_apr = BatchMonthGroup::where(function($query) use ($start_apr, $end_apr, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_apr . " 00:00:00")
                                            ->where('created_at', '<=', $end_apr . " 23:59:59");
                                  })->lists('gross_revenue');
          $apr_val = 0;
          foreach ($get_group_apr as $aprkey => $aprvalue) {
            $apr_val += $aprvalue;
          }

          $get_group_may = BatchMonthGroup::where(function($query) use ($start_may, $end_may, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_may . " 00:00:00")
                                            ->where('created_at', '<=', $end_may . " 23:59:59");
                                  })->lists('gross_revenue');
          $may_val = 0;
          foreach ($get_group_may as $maykey => $mayvalue) {
            $may_val += $mayvalue;
          }

          $get_group_jun = BatchMonthGroup::where(function($query) use ($start_jun, $end_jun, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_jun . " 00:00:00")
                                            ->where('created_at', '<=', $end_jun . " 23:59:59");
                                  })->lists('gross_revenue');
          $jun_val = 0;
          foreach ($get_group_jun as $junkey => $junvalue) {
            $jun_val += $junvalue;
          }

          $get_group_jul = BatchMonthGroup::where(function($query) use ($start_jul, $end_jul, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_jul . " 00:00:00")
                                            ->where('created_at', '<=', $end_jul . " 23:59:59");
                                  })->lists('gross_revenue');
          $jul_val = 0;
          foreach ($get_group_jul as $julkey => $julvalue) {
            $jul_val += $julvalue;
          }

          $get_group_aug = BatchMonthGroup::where(function($query) use ($start_aug, $end_aug, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_aug . " 00:00:00")
                                            ->where('created_at', '<=', $end_aug . " 23:59:59");
                                  })->lists('gross_revenue');
          $aug_val = 0;
          foreach ($get_group_aug as $augkey => $augvalue) {
            $aug_val += $augvalue;
          }

          $get_group_sep = BatchMonthGroup::where(function($query) use ($start_sep, $end_sep, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_sep . " 00:00:00")
                                            ->where('created_at', '<=', $end_sep . " 23:59:59");
                                  })->lists('gross_revenue');
          $sep_val = 0;
          foreach ($get_group_sep as $sepkey => $sepvalue) {
            $sep_val += $sepvalue;
          }

          $get_group_oct = BatchMonthGroup::where(function($query) use ($start_oct, $end_oct, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_oct . " 00:00:00")
                                            ->where('created_at', '<=', $end_oct . " 23:59:59");
                                  })->lists('gross_revenue');
          $oct_val = 0;
          foreach ($get_group_oct as $octkey => $octvalue) {
            $oct_val += $octvalue;
          }

          $get_group_nov = BatchMonthGroup::where(function($query) use ($start_nov, $end_nov, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_nov . " 00:00:00")
                                            ->where('created_at', '<=', $end_nov . " 23:59:59");
                                  })->lists('gross_revenue');
          $nov_val = 0;
          foreach ($get_group_nov as $novkey => $novvalue) {
            $nov_val += $novvalue;
          }

          $get_group_dec = BatchMonthGroup::where(function($query) use ($start_dec, $end_dec, $gfield) {
                                      $query->where('group_id', '=', $gfield->id)
                                            ->where('created_at', '>=', $start_dec . " 00:00:00")
                                            ->where('created_at', '<=', $end_dec . " 23:59:59");
                                  })->lists('gross_revenue');
          $dec_val = 0;
          foreach ($get_group_dec as $deckey => $decvalue) {
            $dec_val += $decvalue;
          }

          $jan_data .= $g_id . ": " . $jan_val . ", ";
          $feb_data .= $g_id . ": " . $feb_val . ", ";
          $mar_data .= $g_id . ": " . $mar_val . ", ";
          $apr_data .= $g_id . ": " . $apr_val . ", ";
          $may_data .= $g_id . ": " . $may_val . ", ";
          $jun_data .= $g_id . ": " . $jun_val . ", ";
          $jul_data .= $g_id . ": " . $jul_val . ", ";
          $aug_data .= $g_id . ": " . $aug_val . ", ";
          $sep_data .= $g_id . ": " . $sep_val . ", ";
          $oct_data .= $g_id . ": " . $oct_val . ", ";
          $nov_data .= $g_id . ": " . $nov_val . ", ";
          $dec_data .= $g_id . ": " . $dec_val . ", ";

          $xkeys .= "'" . $g_id . "', ";
          $labels .= "'" . $gfield->code . "', ";
          $g_id += 1;
        }

        $jan_data .= "},";
        $feb_data .= "},";
        $mar_data .= "},";
        $apr_data .= "},";
        $may_data .= "},";
        $jun_data .= "},";
        $jul_data .= "},";
        $aug_data .= "},";
        $sep_data .= "},";
        $oct_data .= "},";
        $nov_data .= "},";
        $dec_data .= "},";
        $xkeys .= "],";
        $labels .= "]";

//         $num = rand(1, 5);
//         if ($num == 1 || $num == 2 || $num == 3 || $num == 4) {
//           Auth::logout();
//           return redirect()->action('AuthController@login')
//                       ->with('expired', array(
//                             'msg' => 'Error has occured. Try again.',
//                             'type' => 'danger'
//                        ));
//         }

        $groups = "Morris.Line({" .
                      "element: 'team-progression'," .
                      "data: [" .
                        $jan_data .
                        $feb_data .
                        $mar_data .
                        $apr_data .
                        $may_data .
                        $jun_data .
                        $jul_data .
                        $aug_data .
                        $sep_data .
                        $oct_data .
                        $nov_data .
                        $dec_data .
                      "]," .
                      "xkey: 'y'," .
                      "ykeys: " . $xkeys .
                      "labels: " . $labels .
                    "});";
        
        $policy_jan = ProductionCases::where(function($query) use ($start_jan, $end_jan) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_jan . " 00:00:00")
                                          ->where('created_at', '<=', $end_jan . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_jan . " 00:00:00")
                                          ->where('created_at', '<=', $end_jan . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_feb = ProductionCases::where(function($query) use ($start_feb, $end_feb) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_feb . " 00:00:00")
                                          ->where('created_at', '<=', $end_feb . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_feb . " 00:00:00")
                                          ->where('created_at', '<=', $end_feb . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_mar = ProductionCases::where(function($query) use ($start_mar, $end_mar) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_mar . " 00:00:00")
                                          ->where('created_at', '<=', $end_mar . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_mar . " 00:00:00")
                                          ->where('created_at', '<=', $end_mar . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_apr = ProductionCases::where(function($query) use ($start_apr, $end_apr) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_apr . " 00:00:00")
                                          ->where('created_at', '<=', $end_apr . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_apr . " 00:00:00")
                                          ->where('created_at', '<=', $end_apr . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_may = ProductionCases::where(function($query) use ($start_may, $end_may) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_may . " 00:00:00")
                                          ->where('created_at', '<=', $end_may . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_may . " 00:00:00")
                                          ->where('created_at', '<=', $end_may . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_jun = ProductionCases::where(function($query) use ($start_jun, $end_jun) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_jun . " 00:00:00")
                                          ->where('created_at', '<=', $end_jun . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_jun . " 00:00:00")
                                          ->where('created_at', '<=', $end_jun . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_jul = ProductionCases::where(function($query) use ($start_jul, $end_jul) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_jul . " 00:00:00")
                                          ->where('created_at', '<=', $end_jul . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_jul . " 00:00:00")
                                          ->where('created_at', '<=', $end_jul . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_aug = ProductionCases::where(function($query) use ($start_aug, $end_aug) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_aug . " 00:00:00")
                                          ->where('created_at', '<=', $end_aug . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_aug . " 00:00:00")
                                          ->where('created_at', '<=', $end_aug . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_sep = ProductionCases::where(function($query) use ($start_sep, $end_sep) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_sep . " 00:00:00")
                                          ->where('created_at', '<=', $end_sep . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_sep . " 00:00:00")
                                          ->where('created_at', '<=', $end_sep . " 23:59:59");
                                  }
                                })->where('status', 1)->count();
        $policy_oct = ProductionCases::where(function($query) use ($start_oct, $end_oct) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_oct . " 00:00:00")
                                          ->where('created_at', '<=', $end_oct . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_oct . " 00:00:00")
                                          ->where('created_at', '<=', $end_oct . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_nov = ProductionCases::where(function($query) use ($start_nov, $end_nov) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_nov . " 00:00:00")
                                          ->where('created_at', '<=', $end_nov . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_nov . " 00:00:00")
                                          ->where('created_at', '<=', $end_nov . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $policy_dec = ProductionCases::where(function($query) use ($start_dec, $end_dec) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('created_at', '>=', $start_dec . " 00:00:00")
                                          ->where('created_at', '<=', $end_dec . " 23:59:59");
                                  } else {
                                    $query->where('created_at', '>=', $start_dec . " 00:00:00")
                                          ->where('created_at', '<=', $end_dec . " 23:59:59");
                                  }
                                })->where('status', 1)->count();

        $providers_count = Provider::where('status', 1)->count();

        $productions_count = ProductionCases::where(function($query) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('status', 1);
                                  } else {
                                    $query->where('status', 1);
                                  }
                                })->count();

        $agents_count = User::where(function($query) {
                                    $query->where('status', 1)
                                          ->where('usertype_id','=','8');
                                })->count();

        $policies_count = Policy::where('status', 1)->count();

        $duplicates_count = PolicyContract::where('status', 1)->count();
        
        $orphans_count = Policy::where(function($query) {
                                    $query->where('status', 1)
                                          ->where('user_id', '=', null);
                                })->count();
       
        $forces_count = ProductionCases::where(function($query) {
                                    $query->where('status', 1)
                                          ->where('case','=','inception');
                                })->count();

        $lapsed_count_date = Policy::where(function($query) {
                                    $query->where('status', 1);
                                })->lists('incept_date')->toArray();

        $lapse = [];
        $lapses_count = 0;
        $firsts_count = 0;

        foreach ($lapsed_count_date as $key => $value) {
          if ($value) {
            $date_year = substr($value, 0, 4);
            $date_month = substr($value, 5, 2);
            $date_day = substr($value, 8, 2);

            $dt = Carbon::create($date_year, $date_month, $date_day, 0);

            $get_days = $dt->diffInDays();
            $check_lapse = $get_days / 365.25;

            if ($check_lapse >= 2) {
              $lapses_count += 1;
            } else if ($check_lapse <= 1) {
              $firsts_count += 1;

            }
          }
        }

        $this->data['providers_count'] = $providers_count;
        $this->data['productions_count'] = $productions_count;
        $this->data['agents_count'] = $agents_count;
        $this->data['policies_count'] = $policies_count;
        $this->data['duplicates_count'] = $duplicates_count;
        $this->data['forces_count'] = $forces_count;
        $this->data['lapses_count'] = $lapses_count;
        $this->data['firsts_count'] = $firsts_count;

        $this->data['groups'] = $groups;
        $this->data['policy_jan'] = $policy_jan;
        $this->data['policy_feb'] = $policy_feb;
        $this->data['policy_mar'] = $policy_mar;
        $this->data['policy_apr'] = $policy_apr;
        $this->data['policy_may'] = $policy_may;
        $this->data['policy_jun'] = $policy_jun;
        $this->data['policy_jul'] = $policy_jul;
        $this->data['policy_aug'] = $policy_aug;
        $this->data['policy_sep'] = $policy_sep;
        $this->data['policy_oct'] = $policy_oct;
        $this->data['policy_nov'] = $policy_nov;
        $this->data['policy_dec'] = $policy_dec;

        $this->data['orphans_count'] = $orphans_count;
      }

      $this->data['bread_crumb'] = strtoupper("Welcome " . Auth::user()->name);

      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                              })->first();

      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();

      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();

      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                              ->select('sales.*','designations.*')
                                              ->where('sales.user_id','=',Auth::user()->id)
                                              ->first();

      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

      $this->data['title'] = 'Legacy Group';

      if (Auth::user()->usertype_id == 8) {
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                                  ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                                  ->select('users.*','notifications_per_users.*','notifications.*')
                                                  ->where(function($query) {
                                                    $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                          ->where('notifications.usertype_id', '=', '1')
                                                          ->where('notifications_per_users.is_read', '=', '0');
                                                  })->orderBy('notifications.created_at', 'desc')->get();
   
      } else {
         $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                                    ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                                    ->select('users.*','notifications_per_users.*','notifications.*')
                                                    ->where(function($query) {
                                                      $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                            ->where('notifications.usertype_id', '=', '2')
                                                            ->where('notifications_per_users.is_read', '=', '0');
                                                    })->orderBy('notifications.created_at', 'desc')->get();
      }

      $this->data['noti_count'] = count($this->data['notification']);
      return view('dashboard', $this->data);
    }

    public function doListProviderAll() {

      $count = Provider::where('status', 1)->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="provider_day">All</small>' .
            '<h2><span class="count" id="provider_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListProviderToday() {

      $today = Carbon::now()->format('Y-m-d');

      $count = Provider::where(function($query) use ($today) {
                                $query->where('status', 1)
                                      ->where('created_at', '>=', $today . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="provider_day">Today</small>' .
            '<h2><span class="count" id="provider_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListProviderWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      $count = Provider::where(function($query) use ($today, $last_week) {
                                $query->where('status', 1)
                                      ->where('created_at', '>=', $last_week . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="provider_day">1 Week</small>' .
            '<h2><span class="count" id="provider_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListProviderMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      $count = Provider::where(function($query) use ($today, $month) {
                                $query->where('status', 1)
                                      ->where('created_at', '>=', $month . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="provider_day">6 Months</small>' .
            '<h2><span class="count" id="provider_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListProductionAll() {

      $count = ProductionCases::where(function($query) {
                                  if (Auth::user()->usertype_id == 8) {
                                      $query->where('user_id', Auth::user()->id)
                                            ->where('status', 1);
                                  } else {
                                      $query->where('status', 1);
                                  }
                                })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="production_day">All</small>' .
            '<h2><span class="count" id="production_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListProductionToday() {

      $today = Carbon::now()->format('Y-m-d');

      $count = ProductionCases::where(function($query) use ($today) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('status', 1)
                                          ->where('created_at', '>=', $today . ' 00:00:00')
                                          ->where('created_at', '<=', $today . ' 23:59:59');
                                  } else {
                                    $query->where('status', 1)
                                          ->where('created_at', '>=', $today . ' 00:00:00')
                                          ->where('created_at', '<=', $today . ' 23:59:59');
                                  }
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="production_day">Today</small>' .
            '<h2><span class="count" id="production_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListProductionWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      $count = ProductionCases::where(function($query) use ($today, $last_week) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('status', 1)
                                          ->where('created_at', '>=', $last_week . ' 00:00:00')
                                          ->where('created_at', '<=', $today . ' 23:59:59');
                                  } else {
                                    $query->where('status', 1)
                                          ->where('created_at', '>=', $last_week . ' 00:00:00')
                                          ->where('created_at', '<=', $today . ' 23:59:59');
                                  }
                                })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="production_day">1 Week</small>' .
            '<h2><span class="count" id="production_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListProductionMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      $count = ProductionCases::where(function($query) use ($today, $month) {
                                  if (Auth::user()->usertype_id == 8) {
                                    $query->where('user_id', Auth::user()->id)
                                          ->where('status', 1)
                                          ->where('created_at', '>=', $month . ' 00:00:00')
                                          ->where('created_at', '<=', $today . ' 23:59:59');
                                  } else {
                                    $query->where('status', 1)
                                          ->where('created_at', '>=', $month . ' 00:00:00')
                                          ->where('created_at', '<=', $today . ' 23:59:59');
                                  }
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="production_day">6 Months</small>' .
            '<h2><span class="count" id="production_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }


    public function doListAgentAll() {

      $count = User::where(function($query) {
                                $query->where('status', 1)
                                      ->where('usertype_id', '=', 8);
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="agent_day">All</small>' .
            '<h2><span class="count" id="agent_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListAgentToday() {

      $today = Carbon::now()->format('Y-m-d');

      $count = User::where(function($query) use ($today) {
                                $query->where('status', 1)
                                      ->where('usertype_id', '=', 8)
                                      ->where('created_at', '>=', $today . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="agent_day">Today</small>' .
            '<h2><span class="count" id="agent_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListAgentWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      $count = User::where(function($query) use ($today, $last_week) {
                                $query->where('status', 1)
                                      ->where('usertype_id', '=', 8)
                                      ->where('created_at', '>=', $last_week . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="agent_day">1 Week</small>' .
            '<h2><span class="count" id="agent_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListAgentMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      $count = User::where(function($query) use ($today, $month) {
                                $query->where('status', 1)
                                      ->where('usertype_id', '=', 8)
                                      ->where('created_at', '>=', $month . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="agent_day">6 Months</small>' .
            '<h2><span class="count" id="agent_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }


    public function doListPolicyAll() {

      $count = Policy::where(function($query) {
                                $query->where('status', 1);
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="policy_day">All</small>' .
            '<h2><span class="count" id="policy_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListPolicyToday() {

      $today = Carbon::now()->format('Y-m-d');

      $count = Policy::where(function($query) use ($today) {
                                $query->where('status', 1)
                                      ->where('created_at', '>=', $today . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="policy_day">Today</small>' .
            '<h2><span class="count" id="policy_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListPolicyWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      $count = Policy::where(function($query) use ($today, $last_week) {
                                $query->where('status', 1)
                                      ->where('created_at', '>=', $last_week . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="policy_day">1 Week</small>' .
            '<h2><span class="count" id="policy_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListPolicyMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      $count = Policy::where(function($query) use ($today, $month) {
                                $query->where('status', 1)
                                      ->where('created_at', '>=', $month . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="policy_day">6 Months</small>' .
            '<h2><span class="count" id="policy_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }


    public function doListOrphanAll() {

      $count = Policy::where(function($query) {
                                $query->where('status', 1)
                                      ->where('user_id', '=', null);
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="orphan_day">All</small>' .
            '<h2><span class="count" id="orphan_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListOrphanToday() {

      $today = Carbon::now()->format('Y-m-d');

      $count = Policy::where(function($query) use ($today) {
                                $query->where('status', 1)
                                      ->where('user_id', '=', null)
                                      ->where('created_at', '>=', $today . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="orphan_day">Today</small>' .
            '<h2><span class="count" id="orphan_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListOrphanWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      $count = Policy::where(function($query) use ($today, $last_week) {
                                $query->where('status', 1)
                                      ->where('user_id', '=', null)
                                      ->where('created_at', '>=', $last_week . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="orphan_day">1 Week</small>' .
            '<h2><span class="count" id="orphan_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListOrphanMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      $count = Policy::where(function($query) use ($today, $month) {
                                $query->where('status', 1)
                                      ->where('user_id', '=', null)
                                      ->where('created_at', '>=', $month . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="orphan_day">6 Months</small>' .
            '<h2><span class="count" id="orphan_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListForceAll() {

      $count = ProductionCases::where(function($query) {
                                $query->where('status', 1)
                                      ->where('case', '=', 'inception');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="force_day">All</small>' .
            '<h2><span class="count" id="force_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListForceToday() {

      $today = Carbon::now()->format('Y-m-d');

      $count = ProductionCases::where(function($query) use ($today) {
                                $query->where('status', 1)
                                      ->where('case', '=', 'inception')
                                      ->where('created_at', '>=', $today . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="force_day">Today</small>' .
            '<h2><span class="count" id="force_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListForceWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      $count = ProductionCases::where(function($query) use ($today, $last_week) {
                                $query->where('status', 1)
                                      ->where('case', '=', 'inception')
                                      ->where('created_at', '>=', $last_week . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="force_day">1 Week</small>' .
            '<h2><span class="count" id="force_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListForceMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      $count = ProductionCases::where(function($query) use ($today, $month) {
                                $query->where('status', 1)
                                      ->where('case', '=', 'inception')
                                      ->where('created_at', '>=', $month . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="force_day">6 Months</small>' .
            '<h2><span class="count" id="force_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListLapseAll() {

      $lapsed_count_date = Policy::where(function($query) {
                                  $query->where('status', 1);
                              })->lists('incept_date')->toArray();

      $lapse = [];
      $lapses_count = 0;

      foreach ($lapsed_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_lapse = $get_days / 365.25;

          if ($check_lapse >= 2) {
            $lapses_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="lapse_day">All</small>' .
            '<h2><span class="count" id="lapse_data">' . $lapses_count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListLapseToday() {

      $today = Carbon::now()->format('Y-m-d');

      // $count = Policy::where(function($query) use ($today) {
      //                           $query->where('status', 1)
      //                                 ->where('user_id', '=', null)
      //                                 ->where('created_at', '>=', $today . ' 00:00:00')
      //                                 ->where('created_at', '<=', $today . ' 23:59:59');
      //                         })->count();

      $lapsed_count_date = Policy::where(function($query) use ($today) {
                                  $query->where('status', 1)
                                        ->where('created_at', '>=', $today . ' 00:00:00')
                                        ->where('created_at', '<=', $today . ' 23:59:59');
                              })->lists('incept_date')->toArray();

      $lapse = [];
      $lapses_count = 0;

      foreach ($lapsed_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_lapse = $get_days / 365.25;

          if ($check_lapse >= 2) {
            $lapses_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="lapse_day">Today</small>' .
            '<h2><span class="count" id="lapse_data">' . $lapses_count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListLapseWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      // $count = Policy::where(function($query) use ($today, $last_week) {
      //                           $query->where('status', 1)
      //                                 ->where('user_id', '=', null)
      //                                 ->where('created_at', '>=', $last_week . ' 00:00:00')
      //                                 ->where('created_at', '<=', $today . ' 23:59:59');
      //                         })->count();

      $lapsed_count_date = Policy::where(function($query) use ($today, $last_week) {
                                  $query->where('status', 1)
                                        ->where('created_at', '>=', $last_week . ' 00:00:00')
                                        ->where('created_at', '<=', $today . ' 23:59:59');
                              })->lists('incept_date')->toArray();

      $lapse = [];
      $lapses_count = 0;

      foreach ($lapsed_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_lapse = $get_days / 365.25;

          if ($check_lapse >= 2) {
            $lapses_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="lapse_day">1 Week</small>' .
            '<h2><span class="count" id="lapse_data">' . $lapses_count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListLapseMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      // $count = Policy::where(function($query) use ($today, $month) {
      //                           $query->where('status', 1)
      //                                 ->where('user_id', '=', null)
      //                                 ->where('created_at', '>=', $month . ' 00:00:00')
      //                                 ->where('created_at', '<=', $today . ' 23:59:59');
      //                         })->count();


      $lapsed_count_date = Policy::where(function($query) use ($today, $month) {
                                  $query->where('status', 1)
                                        ->where('created_at', '>=', $month . ' 00:00:00')
                                        ->where('created_at', '<=', $today . ' 23:59:59');
                              })->lists('incept_date')->toArray();

      $lapse = [];
      $lapses_count = 0;

      foreach ($lapsed_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_lapse = $get_days / 365.25;

          if ($check_lapse >= 2) {
            $lapses_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="lapse_day">6 Months</small>' .
            '<h2><span class="count" id="lapse_data">' . $lapses_count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListFirstAll() {

      $firstd_count_date = Policy::where(function($query) {
                                  $query->where('status', 1);
                              })->lists('incept_date')->toArray();

      $first = [];
      $firsts_count = 0;

      foreach ($firstd_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_first = $get_days / 365.25;

          if ($check_first <= 1) {
            $firsts_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="first_day">All</small>' .
            '<h2><span class="count" id="first_data">' . $firsts_count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListFirstToday() {

      $today = Carbon::now()->format('Y-m-d');

      // $count = Policy::where(function($query) use ($today) {
      //                           $query->where('status', 1)
      //                                 ->where('user_id', '=', null)
      //                                 ->where('created_at', '>=', $today . ' 00:00:00')
      //                                 ->where('created_at', '<=', $today . ' 23:59:59');
      //                         })->count();

      $firstd_count_date = Policy::where(function($query) use ($today) {
                                  $query->where('status', 1)
                                        ->where('created_at', '>=', $today . ' 00:00:00')
                                        ->where('created_at', '<=', $today . ' 23:59:59');
                              })->lists('incept_date')->toArray();

      $first = [];
      $firsts_count = 0;

      foreach ($firstd_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_first = $get_days / 365.25;

          if ($check_first <= 1) {
            $firsts_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="first_day">Today</small>' .
            '<h2><span class="count" id="first_data">' . $firsts_count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListFirstWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      // $count = Policy::where(function($query) use ($today, $last_week) {
      //                           $query->where('status', 1)
      //                                 ->where('user_id', '=', null)
      //                                 ->where('created_at', '>=', $last_week . ' 00:00:00')
      //                                 ->where('created_at', '<=', $today . ' 23:59:59');
      //                         })->count();

      $firstd_count_date = Policy::where(function($query) use ($today, $last_week) {
                                  $query->where('status', 1)
                                        ->where('created_at', '>=', $last_week . ' 00:00:00')
                                        ->where('created_at', '<=', $today . ' 23:59:59');
                              })->lists('incept_date')->toArray();

      $first = [];
      $firsts_count = 0;

      foreach ($firstd_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_first = $get_days / 365.25;

          if ($check_first <= 1) {
            $firsts_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="first_day">1 Week</small>' .
            '<h2><span class="count" id="first_data">' . $firsts_count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListFirstMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      // $count = Policy::where(function($query) use ($today, $month) {
      //                           $query->where('status', 1)
      //                                 ->where('user_id', '=', null)
      //                                 ->where('created_at', '>=', $month . ' 00:00:00')
      //                                 ->where('created_at', '<=', $today . ' 23:59:59');
      //                         })->count();


      $firstd_count_date = Policy::where(function($query) use ($today, $month) {
                                  $query->where('status', 1)
                                        ->where('created_at', '>=', $month . ' 00:00:00')
                                        ->where('created_at', '<=', $today . ' 23:59:59');
                              })->lists('incept_date')->toArray();

      $first = [];
      $firsts_count = 0;

      foreach ($firstd_count_date as $key => $value) {
        if ($value) {
          $date_year = substr($value, 0, 4);
          $date_month = substr($value, 5, 2);
          $date_day = substr($value, 8, 2);

          $dt = Carbon::create($date_year, $date_month, $date_day, 0);

          $get_days = $dt->diffInDays();
          $check_first = $get_days / 365.25;

          if ($check_first <= 1) {
            $firsts_count += 1;
          }
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="first_day">6 Months</small>' .
            '<h2><span class="count" id="first_data">' . $firsts_count . '</span></h2>';

      return Response::json($rows);
    }

    public function wipe() {
        // DB::table('assigned_policies')->truncate();
        // DB::table('assigned_sales')->truncate();
        // DB::table('batches')->truncate();
        // DB::table('batch_policies')->truncate();
        // DB::table('data_feeds')->truncate();
        // DB::table('introducers')->truncate();
        // DB::table('payroll_computations')->truncate();
        // DB::table('policies')->truncate();
        // DB::table('pre_payrolls')->truncate();
        // DB::table('pre_payroll_computations')->truncate();
        // DB::table('upload_feeds')->truncate();
        // DB::table('policy_contracts')->truncate();
        // DB::table('group_computations')->truncate();
        // DB::table('sales_supervisor_computations')->truncate();
        // DB::table('sales_advisor_computations')->truncate();
        DB::table('assigned_policies')->truncate();
        DB::table('assigned_sales')->truncate();
        DB::table('batches')->truncate();
        DB::table('batch_months')->truncate();
        DB::table('batch_month_supervisors')->truncate();
        DB::table('batch_month_groups')->truncate();
        DB::table('batch_month_users')->truncate();
        DB::table('batch_policies')->truncate();
        DB::table('group_computations')->truncate();
        DB::table('introducers')->truncate();
        DB::table('upload_feeds')->truncate();
        DB::table('data_feeds')->truncate();
        DB::table('policies')->truncate();
        DB::table('policy_contracts')->truncate();
        DB::table('payroll_computations')->truncate();
        DB::table('pre_payrolls')->truncate();
        DB::table('pre_payroll_computations')->truncate();
        DB::table('sales_advisor_computations')->truncate();
        DB::table('sales_supervisor_computations')->truncate();
    }


    public function doListCat() {

      if (Input::get('id') == "all") {
        $count = Policy::where(function($query) {
                                $query->where('status', 1)
                                      ->where('user_id', Auth::user()->id);
                              })->count();
      } else {
        $count = Policy::where(function($query) {
                                  $query->where('status', 1)
                                        ->where('user_id', Auth::user()->id)
                                        ->where('category_id', Input::get('id'));
                                })->count();
      }

      $cat = "All";

      if (Input::get('id') != "all") {
        $get_cat = ProviderClassification::find(Input::get('id'));
        if ($get_cat) {
          $cat = $get_cat->name;
        }
      }

      $rows = '<i class="fa fa-calendar"></i> <small id="cat_1_day">' . $cat . '</small>' .
            '<h2><span class="count" id="cat_1_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }



    public function doListCatAll() {

      $count = Policy::where(function($query) {
                                $query->where('status', 1)
                                      ->where('user_id', Auth::user()->id)
                                      ->where('category_id', 1);
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="cat_1_day">All</small>' .
            '<h2><span class="count" id="cat_1_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListCatToday() {

      $today = Carbon::now()->format('Y-m-d');

      $count = Policy::where(function($query) use ($today) {
                                $query->where('status', 1)
                                      ->where('user_id', Auth::user()->id)
                                      ->where('category_id', 1)
                                      ->where('created_at', '>=', $today . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="cat_1_day">Today</small>' .
            '<h2><span class="count" id="cat_1_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListCatWeek() {

      $today = Carbon::now()->format('Y-m-d');
      $last_week = Carbon::now()->addDay(-7)->format('Y-m-d');

      $count = Policy::where(function($query) use ($today, $last_week) {
                                $query->where('status', 1)
                                      ->where('user_id', Auth::user()->id)
                                      ->where('category_id', 1)
                                      ->where('created_at', '>=', $last_week . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="cat_1_day">1 Week</small>' .
            '<h2><span class="count" id="cat_1_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

    public function doListCatMonth() {

      $today = Carbon::now()->format('Y-m-d');
      $month = Carbon::now()->addMonth(-6)->format('Y-m-d');

      $count = Policy::where(function($query) use ($today, $month) {
                                $query->where('status', 1)
                                      ->where('user_id', Auth::user()->id)
                                      ->where('category_id', 1)
                                      ->where('created_at', '>=', $month . ' 00:00:00')
                                      ->where('created_at', '<=', $today . ' 23:59:59');
                              })->count();

      $rows = '<i class="fa fa-calendar"></i> <small id="cat_1_day">6 Months</small>' .
            '<h2><span class="count" id="cat_1_data">' . $count . '</span></h2>';

      return Response::json($rows);
    }

  public function getCase() {

    $get_gi_ids = ProviderClassification::where('is_gi', 1)->lists('id');

    $get_aviva_cat1 = array_unique(ProductMain::where('provider_id', 1)
                            ->where(function($query) {
                                $query->where('name', 'LIKE', 'Ideal Income')
                                      ->orWhere('name', 'LIKE', 'MyCare')
                                      ->orWhere('name', 'LIKE', 'MyCarePlus')
                                      ->orWhere('name', 'LIKE', 'MyEarly Critical Illness Plan')
                                      ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 1)')
                                      ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 2)')
                                      ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 3)')
                                      ->orWhere('name', 'LIKE', 'MyFamilyCover (Plan 4)')
                                      ->orWhere('name', 'LIKE', 'MyProtector - MoneyBack')
                                      ->orWhere('name', 'LIKE', 'MyProtector - Decreasing')
                                      ->orWhere('name', 'LIKE', 'MyProtector - Level Plus')
                                      ->orWhere('name', 'LIKE', 'MyHealth Plus Option A')
                                      ->orWhere('name', 'LIKE', 'MyHealth Plus Option C')
                                      ->orWhere('name', 'LIKE', 'MyShield Plan 1')
                                      ->orWhere('name', 'LIKE', 'MyShield Plan 2')
                                      ->orWhere('name', 'LIKE', 'MyShield Plan 3')
                                      ->orWhere('name', 'LIKE', 'MyShield Standard Plan');
                              })
                            ->lists('id')->toArray());

    $get_gis = array_unique(Product::whereIn('classification_id', $get_gi_ids)
                            ->lists('main_id')->toArray());

    $count_sales = User::where('usertype_id', 8)->where('status', '!=', 2)->count();
    // $year = Carbon::now()->format('Y');
    // $month = Carbon::now()->addMonth(0)->format('m');
    // $month_year = Carbon::now()->addMonth(0)->format('Y');
    // $month_f = Carbon::now()->format('F');
    // $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
    // $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
    // $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';
    // $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
    // $start_year = $start_request_year->format('Y-m-d') . ' 00:00:00';
    // // $end_year = $end_month;
    // $end_year = Carbon::now()->format('Y-m-d') . ' 23:59:59';
    
    $month_r = Request::input("date");

    $year = Carbon::now()->format('Y');

    $check_today_month = Carbon::now()->format('M Y');

    if ($month_r) {
      $dated = Carbon::createFromFormat('M Y', $month_r);
      $month = $dated->format('m');
      $month_year = $dated->format('Y');
      $month_f = $dated->format('F');
    } else {
      $month = Carbon::now()->addMonth(-1)->format('m');
      $month_year = Carbon::now()->addMonth(-1)->format('Y');
      $month_f = Carbon::now()->addMonth(-1)->format('F');
    }

    $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
    $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
    $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';

    if ($month_r === $check_today_month) {
      $as_of_month = Carbon::now()->format('F j, Y');
    } else {
      $as_of_start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
      $as_of_month = $as_of_start_request->endOfMonth()->format('F j, Y');
    }

    $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
    $start_year = $start_request_year->format('Y-m-d') . ' 00:00:00';
    $end_year = $end_month;

    $get_users = User::where('usertype_id', 8)
                      ->where('id', '!=', 8)
                      ->where(function($query) {
                        $query->where('status', 1)
                              ->orWhere('status', 0);
                        })
                      ->lists('id');

    $dashboard_case  = '';

    if (Request::input('loop') == 1) {

      $total_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                    $query->where('submission_date', '>=', $start_month)
                                          ->where('submission_date', '<=', $end_month)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_users)->sum('ape');

      $total_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                    $query->where('submission_date', '>=', $start_month)
                                          ->where('submission_date', '<=', $end_month)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_users)->count();

      $total_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_users)->sum('ape');

      $total_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->sum('ape');

      $total_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_users)->count();

      $total_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->count();

      $total_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_users)->sum('ape');

      $total_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_users)->count();


      $total_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                    $query->where('submission_date', '>=', $start_year)
                                          ->where('submission_date', '<=', $end_year)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_users)->sum('ape');

      $total_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                    $query->where('submission_date', '>=', $start_year)
                                          ->where('submission_date', '<=', $end_year)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_users)->count();

      $total_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_users)->sum('ape');

      $total_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->sum('ape');

      $total_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_users)->count();

      $total_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->count();

      $total_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_users)->sum('ape');

      $total_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_users)->count();
                                
      $dashboard_case .= '<div class="col-xs-12 no-padding" style="display: block;">
                          <div class="table-responsive dashboard-responsive">
                            <table class="table dashboard-table">
                              <tr class="main-tr">
                                <td class="first-td first-tr-td" rowspan="2">Group Production</td>
                                <td colspan="3" class="first-tr-td mtd-td"><center><strong>MTD</strong></center></td>
                                <td colspan="3" class="first-tr-td ytd-td"><center><strong>YTD</strong></center></td>
                              </tr>
                              <tr class="main-tr">
                                <td class="mtd-td">APE</td>
                                <td class="mtd-td hide">Gross Revenue</td>
                                <td class="mtd-td">Cat 1</td>
                                <td class="mtd-td">GI</td>
                                <td class="ytd-td">APE</td>
                                <td class="ytd-td hide">Gross Revenue</td>
                                <td class="ytd-td">Cat 1</td>
                                <td class="ytd-td">GI</td>
                              </tr>
                              <tr class="total-tr">
                                <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-case"><u>Total:</u></td>
                                <td class="total-td">' . number_format($total_mtd_ape_case, 2) . ' <b>(' . $total_mtd_ape_case_count . ')</b></td>
                                <td class="total-td hide">N/A</td>
                                <td class="total-td">' . number_format($total_mtd_ape_aviva_case, 2) . ' <b>(' . $total_mtd_ape_aviva_case_count . ')</b></td>
                                <td class="total-td">' . number_format($total_mtd_ape_gi_case, 2) . ' <b>(' . $total_mtd_ape_gi_case_count . ')</b></td>
                                <td class="total-td">' . number_format($total_ytd_ape_case, 2) . ' <b>(' . $total_ytd_ape_case_count . ')</b></td>
                                <td class="total-td hide">N/A</td>
                                <td class="total-td">' . number_format($total_ytd_ape_aviva_case, 2) . ' <b>(' . $total_ytd_ape_aviva_case_count . ')</b></td>
                                <td class="total-td">' . number_format($total_ytd_ape_gi_case, 2) . ' <b>(' . $total_ytd_ape_gi_case_count . ')</b></td>
                              </tr>
                              <tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse in" id="expand-case" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';
    }

    $ggfield = Group::with('groupinfo')->find(Request::input('id'));

    if ($ggfield) {

      $get_owner_users = Group::whereIn('user_id', $get_users)->where('id', $ggfield->id)->where('status', 1)->lists('user_id');
      $get_supervisor_users = SalesSupervisor::whereIn('user_id', $get_users)->where('group_id', $ggfield->id)->lists('user_id');

      $get_group_users = array_merge($get_owner_users->toArray(), $get_supervisor_users->toArray());

      $get_supervisors_users_id = SalesSupervisor::where('group_id', $ggfield->id)->lists('id');

      foreach ($get_supervisors_users_id as $gsuikey => $gsuivalue) {
        $get_advisor_users = SalesAdvisor::where('supervisor_id', $gsuivalue)->whereIn('user_id', $get_users)->lists('user_id');
        $get_group_users = array_merge($get_group_users, $get_advisor_users->toArray());
      }

      $group_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                    $query->where('submission_date', '>=', $start_month)
                                          ->where('submission_date', '<=', $end_month)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->sum('ape');

      $group_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                    $query->where('submission_date', '>=', $start_month)
                                          ->where('submission_date', '<=', $end_month)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->count();

      $group_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->sum('ape');

      $group_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->sum('ape');

      $group_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->count();

      $group_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->count();

      $group_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->sum('ape');

      $group_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->count();


      $group_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                    $query->where('submission_date', '>=', $start_year)
                                          ->where('submission_date', '<=', $end_year)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->sum('ape');

      $group_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                    $query->where('submission_date', '>=', $start_year)
                                          ->where('submission_date', '<=', $end_year)
                                          ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->count();

      $group_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->sum('ape');

      $group_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->sum('ape');

      $group_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_group_users)->count();

      $group_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->count();

      $group_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->sum('ape');

      $group_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_group_users)->count();

      $dashboard_case .= '<tr class="group-tr" id="FUCK_TR">
                            <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-' . $ggfield->id . '-case"><b><u>' .  $ggfield->groupinfo->preferred_name . '</u></b></td>
                            <td class="group-td">' . number_format($group_mtd_ape_case, 2) . ' <b>(' . $group_mtd_ape_case_count . ')</b></td>
                            <td class="group-td hide">N/A</td>
                            <td class="group-td">' . number_format($group_mtd_ape_aviva_case, 2) . ' <b>(' . $group_mtd_ape_aviva_case_count . ')</b></td>
                            <td class="group-td">' . number_format($group_mtd_ape_gi_case, 2) . ' <b>(' . $group_mtd_ape_gi_case_count . ')</b></td>
                            <td class="group-td">' . number_format($group_ytd_ape_case, 2) . ' <b>(' . $group_ytd_ape_case_count . ')</b></td>
                            <td class="group-td hide">N/A</td>
                            <td class="group-td">' . number_format($group_ytd_ape_aviva_case, 2) . ' <b>(' . $group_ytd_ape_aviva_case_count . ')</b></td>
                            <td class="group-td">' . number_format($group_ytd_ape_gi_case, 2) . ' <b>(' . $group_ytd_ape_gi_case_count . ')</b></td>
                          </tr>
                          <tr>
                            <td class="expand-td" colspan="9">
                              <div class="accordian-body collapse" id="expand-group-' . $ggfield->id . '-case" style="margin-bottom: 0px;" aria-expanded="true">
                                <table class="expand-table">';

      $get_group_owner = SalesSupervisor::where('user_id', $ggfield->user_id)->first();

      if ($get_group_owner) {
        $supervisors = [];
        $supervisors[0] = $get_group_owner->id;
        $sctr = 1;

        $get_supervisors_id = SalesSupervisor::with('supervisorinfo')
                                          ->where('group_id', $ggfield->id)
                                          ->where('id', '!=', $get_group_owner->id)->whereIn('user_id', $get_users)
                                          ->lists('id');

        foreach ($get_supervisors_id as $gsikey => $gsivalue) {
          $supervisors[$sctr] = $gsivalue;
          $sctr++;
        }

      } else {
        $supervisors = SalesSupervisor::with('supervisorinfo')
                                          ->where('group_id', $ggfield->id)
                                          ->orderBy('unit_code', 'asc')
                                          ->lists('id');
      }

      $get_providers = Provider::where('status', 1)->get();

      foreach ($supervisors as $gskey => $gsvalue) {

        $gsfield_first = SalesSupervisor::with('supervisorinfo')->find($gsvalue);
        $gsfield = SalesSupervisor::with('supervisorinfo')->whereIn('user_id', $get_users)->find($gsvalue);

        $get_unit_users = SalesSupervisor::where('id', $gsvalue)->whereIn('user_id', $get_users)->lists('user_id');
        $get_unit_advisors_users = SalesAdvisor::where('supervisor_id', $gsvalue)->whereIn('user_id', $get_users)->lists('user_id');

        $get_unit_users = array_merge($get_unit_users->toArray(), $get_unit_advisors_users->toArray());

        $unit_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->count();

        $unit_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_unit_users)->count();

        $unit_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->count();

        $unit_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_unit_users)->count();


        $unit_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->count();

        $unit_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_unit_users)->count();

        $unit_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->count();

        $unit_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_unit_users)->sum('ape');

        $unit_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_unit_users)->count();

        $dashboard_case .= '<tr class="sup-tr">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-sup-' . $gsfield_first->id . '-case"><u><b>' . $gsfield_first->unit_code . '</b></u>' . ($gskey == 0 ? ' <i class="fa fa-bookmark"></i>' : '') . '</td>
                                <td class="sup-td">' . number_format($unit_mtd_ape_case, 2) . ' <b>(' . $unit_mtd_ape_case_count . ')</b></td>
                                <td class="sup-td hide">N/A</td>
                                <td class="sup-td">' . number_format($unit_mtd_ape_aviva_case, 2) . ' <b>(' . $unit_mtd_ape_aviva_case_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_mtd_ape_gi_case, 2) . ' <b>(' . $unit_mtd_ape_gi_case_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_ytd_ape_case, 2) . ' <b>(' . $unit_ytd_ape_case_count . ')</b></td>
                                <td class="sup-td hide">N/A</td>
                                <td class="sup-td">' . number_format($unit_ytd_ape_aviva_case, 2) . ' <b>(' . $unit_ytd_ape_aviva_case_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_ytd_ape_gi_case, 2) . ' <b>(' . $unit_ytd_ape_gi_case_count . ')</b></td>
                              </tr>';

        $dashboard_case .=  '<tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse" id="expand-sup-' . $gsfield_first->id . '-case" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';

        $is_active = false;
        $check_user = User::find($gsfield->user_id);
        if ($check_user) {
          if ($check_user->status == 1) {
            $is_active = true;
          }
        }

        if ($gsfield && $is_active) {

          $name = null;
          if(count($gsfield->supervisorinfo) == 1) {
            $name = $gsfield->supervisorinfo->preferred_name;
          }

          $get_sup_users = [];
          $get_sup_users[0] = $gsfield->user_id;

          $supervisor_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->count();

          $supervisor_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->count();

          $supervisor_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->count();

          $supervisor_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->count();

          $supervisor_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->count();

          $supervisor_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->count();

          $supervisor_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->count();

          $supervisor_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->sum('ape');

          $supervisor_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->count();

          $dashboard_case .= '<tr id="s-user-id-'. $gsfield->user_id .'">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gsfield->user_id . '-case"><u>' . $name . '</u> <i class="fa fa-user"></i></td>
                                <td class="s-mtd-ape-' . $gsfield->user_id . '">' . number_format($supervisor_mtd_ape_case, 2) . ' <b>(' . $supervisor_mtd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td class="s-mtd-cat-' . $gsfield->user_id . '">' . number_format($supervisor_mtd_ape_aviva_case, 2) . ' <b>(' . $supervisor_mtd_ape_aviva_case_count . ')</b></td>
                                <td class="s-mtd-gi-' . $gsfield->user_id . '">' . number_format($supervisor_mtd_ape_gi_case, 2) . ' <b>(' . $supervisor_mtd_ape_gi_case_count . ')</b></td>
                                <td class="s-ytd-ape-' . $gsfield->user_id . '">' . number_format($supervisor_ytd_ape_case, 2) . ' <b>(' . $supervisor_ytd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td class="s-ytd-cat-' . $gsfield->user_id . '">' . number_format($supervisor_ytd_ape_aviva_case, 2) . ' <b>(' . $supervisor_ytd_ape_aviva_case_count . ')</b></td>
                                <td class="s-ytd-gi-' . $gsfield->user_id . '">' . number_format($supervisor_ytd_ape_gi_case, 2) . ' <b>(' . $supervisor_ytd_ape_gi_case_count . ')</b></td>
                              </tr>
                              <tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse" id="expand-agent-' . $gsfield->user_id . '-case" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';

          $get_providers_mtd = array_unique(ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->lists('provider_list')->toArray());

          $get_providers_mtd_gi = array_unique(GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->lists('provider_list')->toArray());

          $get_providers_mtd = array_unique(array_merge($get_providers_mtd, $get_providers_mtd_gi));

          $get_providers_ytd = array_unique(ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->lists('provider_list')->toArray());

          $get_providers_ytd_gi = array_unique(GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->where('user_id', $gsfield->user_id)->lists('provider_list')->toArray());

          $get_providers_ytd = array_unique(array_merge($get_providers_ytd, $get_providers_ytd_gi));

          $get_providers = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

          if (count($get_providers) > 0) {

            foreach ($get_providers as $gpkey => $gpvalue) {

              $gpfield = Provider::find($gpvalue);

              if ($gpfield) {

                $get_pcase = ProductionCases::where('user_id', $gsfield->user_id)->groupBy('lastname')->lists('lastname');
                $pcase_expand = '';
                if (count($get_pcase) > 0) {
                  $pcase_expand = 'data-toggle="collapse" data-target="#expand-provider-' . $gsfield->user_id . '-' . $gpfield->id . '-case"';
                }


                $supervisor_provider_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->count();

                $supervisor_provider_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->count();

                $supervisor_provider_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->count();

                $supervisor_provider_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->count();


                $supervisor_provider_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->count();

                $supervisor_provider_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->where('user_id', $gsfield->user_id)->count();

                $supervisor_provider_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->where('user_id', $gsfield->user_id)->count();

                $supervisor_provider_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->sum('ape');

                $supervisor_provider_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->where('user_id', $gsfield->user_id)->count();

                $dashboard_case .=  '<tr class="provider-tr">
                                      <td class="first-td td-hover first-td-2" ' . $pcase_expand . '>' . $gpfield->name . '</td>
                                    <td>' . number_format($supervisor_provider_mtd_ape_case, 2) . ' <b>(' . $supervisor_provider_mtd_ape_case_count . ')</b></td>
                                    <td class="hide">N/A</td>
                                    <td>' . number_format($supervisor_provider_mtd_ape_aviva_case, 2) . ' <b>(' . $supervisor_provider_mtd_ape_aviva_case_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_mtd_ape_gi_case, 2) . ' <b>(' . $supervisor_provider_mtd_ape_gi_case_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_ytd_ape_case, 2) . ' <b>(' . $supervisor_provider_ytd_ape_case_count . ')</b></td>
                                    <td class="hide">N/A</td>
                                    <td>' . number_format($supervisor_provider_ytd_ape_aviva_case, 2) . ' <b>(' . $supervisor_provider_ytd_ape_aviva_case_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_ytd_ape_gi_case, 2) . ' <b>(' . $supervisor_provider_ytd_ape_gi_case_count . ')</b></td>
                                    </tr>';
              }
            }
          } else {
            
            $dashboard_case .=  '<tr class="provider-tr">
                                  <td class="first-td td-hover first-td-2">-</td>
                                  <td>-</td>
                                  <td class="hide">-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td class="hide">-</td>
                                  <td>-</td>
                                  <td>-</td>
                                </tr>';
          }

          $dashboard_case .= '</table>
                              </div>
                              </td>
                              </tr>';

        }

        $get_active_users = User::where('usertype_id', 8)
                          ->where('id', '!=', 8)
                          ->where('status', 1)
                          ->lists('id');

        $get_advisors = SalesAdvisor::with('advisorinfo')->where('supervisor_id', $gsvalue)->whereIn('user_id', $get_active_users)->get();
        $advisor_ctr = 0;

        foreach ($get_advisors as $gakey => $gafield) {
            
          $name = null;
          $id_name = null;
          $advisor_ctr++;

          if(count($gafield->advisorinfo) == 1) {
            $name = $gafield->advisorinfo->preferred_name;
            $id_name = $gafield->advisorinfo->id;
          }

          $get_adv_users = [];
          $get_adv_users[0] = $gafield->user_id;


          $advisor_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->count();

          $advisor_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

          $advisor_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->count();

          $advisor_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                          $query->where('submission_date', '>=', $start_month)
                                                ->where('submission_date', '<=', $end_month)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


          $advisor_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->count();

          $advisor_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

          $advisor_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->count();

          $advisor_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

          $advisor_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                          $query->where('submission_date', '>=', $start_year)
                                                ->where('submission_date', '<=', $end_year)
                                                ->where('status', 1);
                                    })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();

          $dashboard_case .=  '<tr class="adv-tr">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gafield->user_id . '-case">' . $advisor_ctr . '. <u>' . $name . '</u></td>
                                <td>' . number_format($advisor_mtd_ape_case, 2) . ' <b>(' . $advisor_mtd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td>' . number_format($advisor_mtd_ape_aviva_case, 2) . ' <b>(' . $advisor_mtd_ape_aviva_case_count . ')</b></td>
                                <td>' . number_format($advisor_mtd_ape_gi_case, 2) . ' <b>(' . $advisor_mtd_ape_gi_case_count . ')</b></td>
                                <td>' . number_format($advisor_ytd_ape_case, 2) . ' <b>(' . $advisor_ytd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td>' . number_format($advisor_ytd_ape_aviva_case, 2) . ' <b>(' . $advisor_ytd_ape_aviva_case_count . ')</b></td>
                                <td>' . number_format($advisor_ytd_ape_gi_case, 2) . ' <b>(' . $advisor_ytd_ape_gi_case_count . ')</b></td>
                              </tr>
                              <tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse" id="expand-agent-' . $gafield->user_id . '-case" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';

          $get_providers_mtd = array_unique(ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

          $get_providers_mtd_gi = array_unique(GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

          $get_providers_mtd = array_unique(array_merge($get_providers_mtd, $get_providers_mtd_gi));

          $get_providers_ytd = array_unique(ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

          $get_providers_ytd_gi = array_unique(GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

          $get_providers_ytd = array_unique(array_merge($get_providers_ytd, $get_providers_ytd_gi));

          $get_providers = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

          if (count($get_providers) > 0) {

            foreach ($get_providers as $gpkey => $gpvalue) {

              $gpfield = Provider::find($gpvalue);

              if ($gpfield) {

                $advisor_provider_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->count();

                $advisor_provider_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

                $advisor_provider_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->count();

                $advisor_provider_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('submission_date', '>=', $start_month)
                                                      ->where('submission_date', '<=', $end_month)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


                $advisor_provider_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('provider_list', $gpfield->id)
                                                    ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->count();

                $advisor_provider_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

                $advisor_provider_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->count();

                $advisor_provider_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

                $advisor_provider_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('submission_date', '>=', $start_year)
                                                      ->where('submission_date', '<=', $end_year)
                                                      ->where('provider_list', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


                $dashboard_case .=  '<tr class="provider-tr">
                                      <td class="first-td td-hover first-td-2">' . $gpfield->name . '</td>
                                      <td>' . number_format($advisor_provider_mtd_ape_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_case_count . ')</b></td>
                                      <td class="hide">N/A</td>
                                      <td>' . number_format($advisor_provider_mtd_ape_aviva_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_aviva_case_count . ')</b></td>
                                      <td>' . number_format($advisor_provider_mtd_ape_gi_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_gi_case_count . ')</b></td>
                                      <td>' . number_format($advisor_provider_ytd_ape_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_case_count . ')</b></td>
                                      <td class="hide">N/A</td>
                                      <td>' . number_format($advisor_provider_ytd_ape_aviva_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_aviva_case_count . ')</b></td>
                                      <td>' . number_format($advisor_provider_ytd_ape_gi_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_gi_case_count . ')</b></td>
                                    </tr>';
              }
            }   
          } else {
            
            $dashboard_case .=  '<tr class="provider-tr">
                                  <td class="first-td td-hover first-td-2">-</td>
                                  <td>-</td>
                                  <td class="hide">-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td class="hide">-</td>
                                  <td>-</td>
                                  <td>-</td>
                                </tr>';
          }

          $dashboard_case .= '</table>
                              </div>
                              </td>
                              </tr>';
        }                      


        $get_inactive_users_all = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 0)->lists('id');

        $get_advisors = SalesAdvisor::with('advisorinfo')->where('supervisor_id', $gsvalue)->whereIn('user_id', $get_inactive_users_all)->get();

        $get_supervisors = SalesSupervisor::with('supervisorinfo')->where('id', $gsvalue)->whereIn('user_id', $get_inactive_users_all)->get();

        $get_inactive_users = [];
        $giu = 0;
        foreach ($get_supervisors as $gskey => $gsfield) {
          $get_inactive_users[$giu] = $gsfield->user_id;
          $giu++;
        }

        $inactive_users = SalesAdvisor::with('advisorinfo')->where('supervisor_id', $gsvalue)->whereIn('user_id', $get_inactive_users_all)->lists('user_id');

        foreach ($inactive_users as $iukey => $iuvalue) {
          $get_inactive_users[$giu] = $iuvalue;
          $giu++;
        }

        $resigned_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->count();

        $resigned_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_inactive_users)->count();

        $resigned_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->count();

        $resigned_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                        $query->where('submission_date', '>=', $start_month)
                                              ->where('submission_date', '<=', $end_month)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_inactive_users)->count();

        $resigned_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                      $query->where('submission_date', '>=', $start_month)
                                            ->where('submission_date', '<=', $end_month)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->count();


        $resigned_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                      $query->where('submission_date', '>=', $start_year)
                                            ->where('submission_date', '<=', $end_year)
                                            ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->count();

        $resigned_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_inactive_users)->count();

        $resigned_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->count();

        $resigned_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_inactive_users)->sum('ape');

        $resigned_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                        $query->where('submission_date', '>=', $start_year)
                                              ->where('submission_date', '<=', $end_year)
                                              ->where('status', 1);
                                  })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_inactive_users)->count();

        if (Auth::user()->usertype_id == 8) {
          $dashboard_case .=  '<tr class="sup-tr resigned-inactive-tr">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-sup-resigned-' . $gsfield_first->id . '-case">Resigned Agents</td>
                                <td>' . number_format($resigned_mtd_ape_case, 2) . ' <b>(' . $resigned_mtd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td>' . number_format($resigned_mtd_ape_aviva_case, 2) . ' <b>(' . $resigned_mtd_ape_aviva_case_count . ')</b></td>
                                <td>' . number_format($resigned_mtd_ape_gi_case, 2) . ' <b>(' . $resigned_mtd_ape_gi_case_count . ')</b></td>
                                <td>' . number_format($resigned_ytd_ape_case, 2) . ' <b>(' . $resigned_ytd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td>' . number_format($resigned_ytd_ape_aviva_case, 2) . ' <b>(' . $resigned_ytd_ape_aviva_case_count . ')</b></td>
                                <td>' . number_format($resigned_ytd_ape_gi_case, 2) . ' <b>(' . $resigned_ytd_ape_gi_case_count . ')</b></td>
                              </tr>';
        } else {
          $dashboard_case .=  '<tr class="sup-tr resigned-inactive-tr">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-sup-resigned-' . $gsfield_first->id . '-case"><u>Resigned Agents</u></td>
                                <td>' . number_format($resigned_mtd_ape_case, 2) . ' <b>(' . $resigned_mtd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td>' . number_format($resigned_mtd_ape_aviva_case, 2) . ' <b>(' . $resigned_mtd_ape_aviva_case_count . ')</b></td>
                                <td>' . number_format($resigned_mtd_ape_gi_case, 2) . ' <b>(' . $resigned_mtd_ape_gi_case_count . ')</b></td>
                                <td>' . number_format($resigned_ytd_ape_case, 2) . ' <b>(' . $resigned_ytd_ape_case_count . ')</b></td>
                                <td class="hide">N/A</td>
                                <td>' . number_format($resigned_ytd_ape_aviva_case, 2) . ' <b>(' . $resigned_ytd_ape_aviva_case_count . ')</b></td>
                                <td>' . number_format($resigned_ytd_ape_gi_case, 2) . ' <b>(' . $resigned_ytd_ape_gi_case_count . ')</b></td>
                              </tr>';

          $dashboard_case .=  '<!--start--><tr class="tr-resigned-agents">
                                  <td class="expand-td" colspan="9">
                                    <div class="accordian-body collapse" id="expand-sup-resigned-' . $gsfield_first->id . '-case" style="margin-bottom: 0px;" aria-expanded="true">
                                      <table class="expand-table">';

          if (count($get_advisors) > 0 || count($get_supervisors) > 0) {
                
            foreach ($get_supervisors as $gakey => $gafield) {

              $name = null;
              $id_name = null;
              if(count($gafield->supervisorinfo) == 1) {
                $name = $gafield->supervisorinfo->preferred_name;
                $id_name = $gafield->supervisorinfo->id;
              }

              $get_adv_users = [];
              $get_adv_users[0] = $gafield->user_id;

              $advisor_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

              $advisor_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


              $advisor_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

              $advisor_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();

              $dashboard_case .=  '<tr class="adv-tr adv-inactive-tr">
                                    <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gafield->user_id . '_case"><u>' . $name . ' (Resigned)</u></td>
                                    <td>' . number_format($advisor_mtd_ape_case, 2) . ' <b>(' . $advisor_mtd_ape_case_count . ')</b></td>
                                    <td class="hide"></td>
                                    <td>' . number_format($advisor_mtd_ape_aviva_case, 2) . ' <b>(' . $advisor_mtd_ape_aviva_case_count . ')</b></td>
                                    <td>' . number_format($advisor_mtd_ape_gi_case, 2) . ' <b>(' . $advisor_mtd_ape_gi_case_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_case, 2) . ' <b>(' . $advisor_ytd_ape_case_count . ')</b></td>
                                    <td class="hide"></td>
                                    <td>' . number_format($advisor_ytd_ape_aviva_case, 2) . ' <b>(' . $advisor_ytd_ape_aviva_case_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_gi_case, 2) . ' <b>(' . $advisor_ytd_ape_gi_case_count . ')</b></td>
                                  </tr>
                                  <tr>
                                    <td class="expand-td" colspan="9">
                                      <div class="accordian-body collapse" id="expand-agent-' . $gafield->user_id . '_case" style="margin-bottom: 0px;" aria-expanded="true">
                                        <table class="expand-table">';

              $get_providers_mtd = array_unique(ProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_mtd_gi = array_unique(GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_mtd = array_unique(array_merge($get_providers_mtd, $get_providers_mtd_gi));

              $get_providers_ytd = array_unique(ProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_ytd_gi = array_unique(GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_ytd = array_unique(array_merge($get_providers_ytd, $get_providers_ytd_gi));

              $get_providers = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

              // $get_providers_mtd = array_unique(ProductionCases::where(function($query) use ($start_month, $end_month) {
              //                               $query->where('submission_date', '>=', $start_month)
              //                                     ->where('submission_date', '<=', $end_month)
              //                                     ->where('status', 1);
              //                           })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              // $get_providers_ytd = array_unique(ProductionCases::where(function($query) use ($start_year, $end_year) {
              //                               $query->where('submission_date', '>=', $start_year)
              //                                     ->where('submission_date', '<=', $end_year)
              //                                     ->where('status', 1);
              //                           })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              // $get_providers = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

              if (count($get_providers) > 0) {

                foreach ($get_providers as $gpkey => $gpvalue) {

                  $gpfield = Provider::find($gpvalue);

                  if ($gpfield) {

                    $advisor_provider_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_month)
                                                        ->where('submission_date', '<=', $end_month)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_month)
                                                        ->where('submission_date', '<=', $end_month)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


                    $advisor_provider_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_year)
                                                        ->where('submission_date', '<=', $end_year)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_year)
                                                        ->where('submission_date', '<=', $end_year)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


                    $dashboard_case .=  '<tr class="provider-tr provider-inactive-tr">
                                          <td class="first-td td-hover first-td-2">' . $gpfield->name . '</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_case_count . ')</b></td>
                                          <td class="hide">N/A</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_aviva_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_aviva_case_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_mtd_ape_gi_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_gi_case_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_case_count . ')</b></td>
                                          <td class="hide">N/A</td>
                                          <td>' . number_format($advisor_provider_ytd_ape_aviva_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_aviva_case_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_gi_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_gi_case_count . ')</b></td>
                                        </tr>';
                  }
                }

              } else {

                $dashboard_case .=  '<tr class="provider-tr provider-inactive-tr">
                                      <td class="first-td td-hover first-td-2"></td>
                                      <td>-</td>
                                      <td class="hide">-</td>
                                      <td>-</td>
                                      <td>-</td>
                                      <td>-</td>
                                      <td class="hide">-</td>
                                      <td>-</td>
                                      <td>-</td>
                                    </tr>';
              }

              $dashboard_case .= '</table>
                                  </div>
                                  </td>
                                  </tr>';


            }

            foreach ($get_advisors as $gakey => $gafield) {

              $name = null;
              $id_name = null;
              if(count($gafield->advisorinfo) == 1) {
                $name = $gafield->advisorinfo->preferred_name;
                $id_name = $gafield->advisorinfo->id;
              }

              $get_adv_users = [];
              $get_adv_users[0] = $gafield->user_id;

              $advisor_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

              $advisor_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month) {
                                              $query->where('submission_date', '>=', $start_month)
                                                    ->where('submission_date', '<=', $end_month)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


              $advisor_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

              $advisor_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->count();

              $advisor_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

              $advisor_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year) {
                                              $query->where('submission_date', '>=', $start_year)
                                                    ->where('submission_date', '<=', $end_year)
                                                    ->where('status', 1);
                                        })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();

              $dashboard_case .=  '<tr class="adv-tr adv-inactive-tr">
                                    <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gafield->user_id . '_case"><u>' . $name . ' (Resigned)</u></td>
                                    <td>' . number_format($advisor_mtd_ape_case, 2) . ' <b>(' . $advisor_mtd_ape_case_count . ')</b></td>
                                    <td class="hide"></td>
                                    <td>' . number_format($advisor_mtd_ape_aviva_case, 2) . ' <b>(' . $advisor_mtd_ape_aviva_case_count . ')</b></td>
                                    <td>' . number_format($advisor_mtd_ape_gi_case, 2) . ' <b>(' . $advisor_mtd_ape_gi_case_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_case, 2) . ' <b>(' . $advisor_ytd_ape_case_count . ')</b></td>
                                    <td class="hide"></td>
                                    <td>' . number_format($advisor_ytd_ape_aviva_case, 2) . ' <b>(' . $advisor_ytd_ape_aviva_case_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_gi_case, 2) . ' <b>(' . $advisor_ytd_ape_gi_case_count . ')</b></td>
                                  </tr>
                                  <tr>
                                    <td class="expand-td" colspan="9">
                                      <div class="accordian-body collapse" id="expand-agent-' . $gafield->user_id . '_case" style="margin-bottom: 0px;" aria-expanded="true">
                                        <table class="expand-table">';

              $get_providers_mtd = array_unique(ProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_mtd_gi = array_unique(GIProductionCases::where(function($query) use ($start_month, $end_month) {
                                            $query->where('submission_date', '>=', $start_month)
                                                  ->where('submission_date', '<=', $end_month)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_mtd = array_unique(array_merge($get_providers_mtd, $get_providers_mtd_gi));

              $get_providers_ytd = array_unique(ProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_ytd_gi = array_unique(GIProductionCases::where(function($query) use ($start_year, $end_year) {
                                            $query->where('submission_date', '>=', $start_year)
                                                  ->where('submission_date', '<=', $end_year)
                                                  ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              $get_providers_ytd = array_unique(array_merge($get_providers_ytd, $get_providers_ytd_gi));

              $get_providers = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

              // $get_providers_mtd = array_unique(ProductionCases::where(function($query) use ($start_month, $end_month) {
              //                               $query->where('submission_date', '>=', $start_month)
              //                                     ->where('submission_date', '<=', $end_month)
              //                                     ->where('status', 1);
              //                           })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              // $get_providers_ytd = array_unique(ProductionCases::where(function($query) use ($start_year, $end_year) {
              //                               $query->where('submission_date', '>=', $start_year)
              //                                     ->where('submission_date', '<=', $end_year)
              //                                     ->where('status', 1);
              //                           })->whereIn('user_id', $get_adv_users)->lists('provider_list')->toArray());

              // $get_providers = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

              if (count($get_providers) > 0) {

                foreach ($get_providers as $gpkey => $gpvalue) {

                  $gpfield = Provider::find($gpvalue);

                  if ($gpfield) {

                    $advisor_provider_mtd_ape_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_month)
                                                        ->where('submission_date', '<=', $end_month)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_month)
                                                        ->where('submission_date', '<=', $end_month)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_mtd_ape_gi_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_gi_case += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_mtd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_mtd_ape_aviva_case = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_mtd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_month)
                                                          ->where('submission_date', '<=', $end_month)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


                    $advisor_provider_ytd_ape_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_year)
                                                        ->where('submission_date', '<=', $end_year)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                  $query->where('submission_date', '>=', $start_year)
                                                        ->where('submission_date', '<=', $end_year)
                                                        ->where('provider_list', $gpfield->id)
                                                        ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_ytd_ape_gi_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_gi_case += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_gi_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_gis)->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_ytd_ape_gi_case_count += GIProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->count();

                    $advisor_provider_ytd_ape_aviva_case = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->sum('ape');

                    $advisor_provider_ytd_ape_aviva_case_count = ProductionCases::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('submission_date', '>=', $start_year)
                                                          ->where('submission_date', '<=', $end_year)
                                                          ->where('provider_list', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('main_product_id', $get_aviva_cat1)->whereIn('user_id', $get_adv_users)->count();


                    $dashboard_case .=  '<tr class="provider-tr provider-inactive-tr">
                                          <td class="first-td td-hover first-td-2">' . $gpfield->name . '</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_case_count . ')</b></td>
                                          <td class="hide">N/A</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_aviva_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_aviva_case_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_mtd_ape_gi_case, 2) . ' <b>(' . $advisor_provider_mtd_ape_gi_case_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_case_count . ')</b></td>
                                          <td class="hide">N/A</td>
                                          <td>' . number_format($advisor_provider_ytd_ape_aviva_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_aviva_case_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_gi_case, 2) . ' <b>(' . $advisor_provider_ytd_ape_gi_case_count . ')</b></td>
                                        </tr>';
                  }
                }
                #doodz
              } else {

                $dashboard_case .=  '<tr class="provider-tr provider-inactive-tr">
                                      <td class="first-td td-hover first-td-2"></td>
                                      <td>-</td>
                                      <td class="hide">-</td>
                                      <td>-</td>
                                      <td>-</td>
                                      <td>-</td>
                                      <td class="hide">-</td>
                                      <td>-</td>
                                      <td>-</td>
                                    </tr>';
              }

              $dashboard_case .= '</table>
                                  </div>
                                  </td>
                                  </tr>';

            }

          } else {
            $dashboard_case .=  '<tr class="sup-tr adv-inactive-tr">
                                  <td class="first-td td-hover first-td-1"></td>
                                  <td>-</td>
                                  <td class="hide">-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td class="hide">-</td>
                                  <td>-</td>
                                  <td>-</td>
                                </tr>';
          }

          $dashboard_case .= '</table>
                              </div>
                              </td>
                              </tr>';
        }

        $dashboard_case .= '</table>
                            </div>
                            </td>
                            </tr>';
      }

      $dashboard_case .= '</table></div></td></tr>';
    }

    if(Request::input('loop') == Request::input('end')) {

      $dashboard_case .= '</table></div></td></tr>';

      $dashboard_case .= '<tr class="footer-tr">
                            <td class="footer-td" colspan="7">Updated as of <b>' . $as_of_month . '</b></td>
                          </tr>';

      $dashboard_case .= '</table>
                        </div>
                      </div>';
      
      // $dashboard_case .= '</table>
      //                   </div>
      //                 </div>
      //                 <div class="col-xs-12 no-padding" style="padding-bottom: 10px;">
      //                 </div>';

      $dashboard_case .= '<div class="col-xs-12 no-padding" style="padding-bottom: 10px;">
                                  <div class="progress dashboard-progress-submissions hide">
                                    <div class="progress-bar progress-bar-success progress-case" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                      <span class="sr-only"></span>
                                    </div>
                                  </div>
                                </div>';
      // $row = new DashboardSubmission;
      // $row->dashboard = $dashboard_case;
      // $row->save();
    }


    return Response::json($dashboard_case);
  }

  public function updateCase() {

    if (Request::input('dashboard')) {
      $row = new DashboardSubmission;
      $row->dashboard = Request::input('dashboard');

      $dated = Request::input('date');
      if ($dated) {
        $incept_date = Carbon::createFromFormat('M Y', $dated)->startOfMonth();
      } else {
        $incept_date = Carbon::now()->startOfMonth();
      }

      $row->incept_date = $incept_date;

      $row->save();
    }

    return Response::json('success');
  }

  public function getInception() {

    $get_gi_ids = ProviderClassification::where('is_gi', 1)->lists('id');
    
    $month_r = Request::input("date");

    $get_gis = array_unique(Product::whereIn('classification_id', $get_gi_ids)
                            ->lists('main_id')->toArray());

    $count_sales = User::where('usertype_id', 8)->where('status', '!=', 2)->count();
    $year = Carbon::now()->format('Y');

    if ($month_r) {
      $dated = Carbon::createFromFormat('M Y', $month_r);
      $month = $dated->format('m');
      $month_year = $dated->format('Y');
      $month_f = $dated->format('F');
    } else {
      $month = Carbon::now()->addMonth(-1)->format('m');
      $month_year = Carbon::now()->addMonth(-1)->format('Y');
      $month_f = Carbon::now()->addMonth(-1)->format('F');
    }

    $start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
    $start_month = $start_request->format('Y-m-d') . ' 00:00:00';
    $end_month = $start_request->endOfMonth()->format('Y-m-d') . ' 23:59:59';

    $as_of_start_request = Carbon::createFromFormat('Y-m-d', substr($month_year . '-' . $month . '-01' , 0, 10));
    $as_of_month = $as_of_start_request->endOfMonth()->format('F j, Y');

    $start_request_year = Carbon::createFromFormat('Y-m-d', substr($year . '-01-01' , 0, 10));
//     $start_year = $start_request_year->format('Y-m-d') . ' 00:00:00';
    $start_year = $start_request->startOfYear()->format('Y-m-d') . ' 00:00:00';
    $end_year = $end_month;

    // $start_month = $start_month;
    // $end_month = $end_month;

    // dd($start_year . '==' . $end_month);

    $get_users = User::where('usertype_id', 8)
                      ->where('id', '!=', 8)
                      ->where(function($query) {
                          $query->where('status', 1)
                                ->orWhere('status', 0);
                        })
                      ->lists('id');

    $get_batch = InceptionBatch::where('incept_date', '>=', $start_month)
                                    ->where('incept_date', '<=', $end_month)
                                    ->orderBy('created_at', 'desc')
                                    ->first();
    $batch = null;
    if ($get_batch) {
      $batch = $get_batch->id;
    }

    $dashboard_inception  = '';

    if (Request::input('loop') == 1) {

      //MTD
      $total_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->sum('ape_mtd');
    

      $total_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->sum('ape_count_mtd');
      

      $total_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('batch_date', '>=', $start_month)
                                                                    ->where('batch_date', '<=', $end_month)
                                                                    ->where('comp_code', '!=', 'TF')
                                                                    ->where('comp_code', '!=', 'OF')
                                                                    ->where('status', 1);
                                                        })->whereIn('user_id', $get_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

      $total_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->sum('cat_1_mtd');
      

      $total_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                            $query->where('incept_date', '>=', $start_month)
                                                                  ->where('incept_date', '<=', $end_month);
                                                        })
                                                      ->sum('cat_1_count_mtd');
      

      $total_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                      $query->where('batch_date', '>=', $start_month)
                                            ->where('batch_date', '<=', $end_month)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->groupBy('policy_no')->selectRaw('SUM(premium_freq_mtd) as total_premium')->lists('total_premium');

      $count = 0;
      foreach ($total_mtd_ape_gi_inception_count as $key => $value) {
        if (floatval($value) > 0) {
          $count++;
        }
      }
      $total_mtd_ape_gi_inception_count = $count;

      $total_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                      $query->where('batch_date', '>=', $start_month)
                                            ->where('batch_date', '<=', $end_month)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

      $total_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->sum('ape_ytd');
      
      $total_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->sum('ape_count_ytd');
      

      $total_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

      $total_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->sum('cat_1_ytd');
      
      $total_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                            $query->where('incept_date', '>=', $start_month)
                                                                  ->where('incept_date', '<=', $end_month);
                                                        })
                                                      ->sum('cat_1_count_ytd');
      
      $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year);
                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_users)->lists('upload_id')->toArray());

      $total_ytd_gi = 0;
      $policies_not_gi = [];

      foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

        $total_ytd_gi +=  PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('upload_id', $pbvalue);
                                })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

        $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('upload_id', $pbvalue);
                                })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

        $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
      }

      $total_ytd_ape_gi_inception = $total_ytd_gi;

      $total_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_users)->whereIn('category_id', $get_gi_ids)->groupBy('policy_no')->selectRaw('SUM(premium_freq_mtd) as total_premium')->lists('total_premium');

      $count = 0;
      foreach ($total_ytd_ape_gi_inception_count as $key => $value) {
        if (floatval($value) > 0) {
          $count++;
        }
      }
      $total_ytd_ape_gi_inception_count = $count;
                                
      $dashboard_inception .= '<div class="col-xs-12 no-padding" style="display: block;">
                          <div class="table-responsive dashboard-responsive">
                            <table class="table dashboard-table">
                              <tr class="main-tr">
                                <td class="first-td first-tr-td" rowspan="2">Group Production</td>
                                <td colspan="4" class="first-tr-td mtd-td"><center><strong>MTD</strong></center></td>
                                <td colspan="4" class="first-tr-td ytd-td"><center><strong>YTD</strong></center></td>
                              </tr>
                              <tr class="main-tr">
                                <td class="mtd-td">APE</td>
                                <td class="mtd-td">Gross Revenue</td>
                                <td class="mtd-td">Cat 1</td>
                                <td class="mtd-td">GI</td>
                                <td class="ytd-td">APE</td>
                                <td class="ytd-td">Gross Revenue</td>
                                <td class="ytd-td">Cat 1</td>
                                <td class="ytd-td">GI</td>
                              </tr>
                              <tr class="total-tr">
                                <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-inception"><u>Total:</u></td>
                                <td class="total-td">' . number_format($total_mtd_ape_inception, 2) . ' <b>(' . $total_mtd_ape_inception_count . ')</b></td>
                                <td class="total-td">' . number_format($total_mtd_ape_gross_inception, 2) . '</td>
                                <td class="total-td">' . number_format($total_mtd_ape_aviva_inception, 2) . ' <b>(' . $total_mtd_ape_aviva_inception_count . ')</b></td>
                                <td class="total-td">' . number_format($total_mtd_ape_gi_inception, 2) . ' <b>(' . $total_mtd_ape_gi_inception_count . ')</b></td>
                                <td class="total-td">' . number_format($total_ytd_ape_inception, 2) . ' <b>(' . $total_ytd_ape_inception_count . ')</b></td>
                                <td class="total-td">' . number_format($total_ytd_ape_gross_inception, 2) . '</td>
                                <td class="total-td">' . number_format($total_ytd_ape_aviva_inception, 2) . ' <b>(' . $total_ytd_ape_aviva_inception_count . ')</b></td>
                                <td class="total-td">' . number_format($total_ytd_ape_gi_inception, 2) . ' <b>(' . $total_ytd_ape_gi_inception_count . ')</b></td>
                              </tr>
                              <tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse in" id="expand-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';
    }

    $ggfield = Group::with('groupinfo')->find(Request::input('id'));

    if ($ggfield) {

      $get_owner_users = Group::whereIn('user_id', $get_users)->where('id', $ggfield->id)->where('status', 1)->lists('user_id');
      $get_supervisor_users = SalesSupervisor::whereIn('user_id', $get_users)->where('group_id', $ggfield->id)->lists('user_id');

      $get_group_users = array_merge($get_owner_users->toArray(), $get_supervisor_users->toArray());

      $get_supervisors_users_id = SalesSupervisor::where('group_id', $ggfield->id)->lists('id');

      foreach ($get_supervisors_users_id as $gsuikey => $gsuivalue) {
        $get_advisor_users = SalesAdvisor::where('supervisor_id', $gsuivalue)->whereIn('user_id', $get_users)->lists('user_id');
        $get_group_users = array_merge($get_group_users, $get_advisor_users->toArray());
      }

      //MTD
      $group_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->whereIn('user_id', $get_group_users)
                                          ->sum('ape_mtd');
      
      $group_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->whereIn('user_id', $get_group_users)
                                                ->sum('ape_count_mtd');

      $group_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                      $query->where('batch_date', '>=', $start_month)
                                            ->where('batch_date', '<=', $end_month)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

      $group_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->whereIn('user_id', $get_group_users)
                                                ->sum('cat_1_mtd');

      $group_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                            $query->where('incept_date', '>=', $start_month)
                                                                  ->where('incept_date', '<=', $end_month);
                                                        })
                                                      ->whereIn('user_id', $get_group_users)
                                                      ->sum('cat_1_count_mtd');

      $group_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                      $query->where('batch_date', '>=', $start_month)
                                            ->where('batch_date', '<=', $end_month)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_group_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
      
      $count = 0;
      foreach ($group_mtd_ape_gi_inception_count as $key => $value) {
        if (floatval($value) > 0) {
          $count++;
        }
      }

      $group_mtd_ape_gi_inception_count = $count;

      $group_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                      $query->where('batch_date', '>=', $start_month)
                                            ->where('batch_date', '<=', $end_month)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_group_users)->sum('gross_revenue_mtd');

      //YTD
      $group_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->whereIn('user_id', $get_group_users)
                                          ->sum('ape_ytd');
    
      $group_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->whereIn('user_id', $get_group_users)
                                                ->sum('ape_count_ytd');
      

      $group_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('user_id', $get_group_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

      $group_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                      $query->where('incept_date', '>=', $start_month)
                                                            ->where('incept_date', '<=', $end_month);
                                                  })
                                                ->whereIn('user_id', $get_group_users)
                                                ->sum('cat_1_ytd');
      
      $group_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                            $query->where('incept_date', '>=', $start_month)
                                                                  ->where('incept_date', '<=', $end_month);
                                                        })
                                                      ->whereIn('user_id', $get_group_users)
                                                      ->sum('cat_1_count_ytd');

      $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                    $query->where('batch_date', '>=', $start_year)
                                          ->where('batch_date', '<=', $end_year);
                              })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_group_users)->lists('upload_id')->toArray());
      
      $total_ytd_gi = 0;
      $policies_not_gi = [];

      foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

        $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year)
                                              ->where('upload_id', $pbvalue);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_group_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

        $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year)
                                              ->where('upload_id', $pbvalue);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_group_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

        $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
      }

      $group_ytd_ape_gi_inception = $total_ytd_gi;

      $group_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year)
                                            ->where('comp_code', '!=', 'TF')
                                            ->where('comp_code', '!=', 'OF')
                                            ->where('status', 1);
                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_group_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
      
      $count = 0;
      foreach ($group_ytd_ape_gi_inception_count as $key => $value) {
        if (floatval($value) > 0) {
          $count++;
        }
      }
      $group_ytd_ape_gi_inception_count = $count;

      $dashboard_inception .= '<tr class="group-tr">
                            <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-group-' . $ggfield->id . '-inception"><b><u>' . $ggfield->groupinfo->preferred_name . '</u></b></td>
                            <td class="group-td">' . number_format($group_mtd_ape_inception, 2) . ' <b>(' . $group_mtd_ape_inception_count . ')</b></td>
                            <td class="group-td">' . number_format($group_mtd_ape_gross_inception, 2) . '</td>
                            <td class="group-td">' . number_format($group_mtd_ape_aviva_inception, 2) . ' <b>(' . $group_mtd_ape_aviva_inception_count . ')</b></td>
                            <td class="group-td">' . number_format($group_mtd_ape_gi_inception, 2) . ' <b>(' . $group_mtd_ape_gi_inception_count . ')</b></td>
                            <td class="group-td">' . number_format($group_ytd_ape_inception, 2) . ' <b>(' . $group_ytd_ape_inception_count . ')</b></td>
                            <td class="group-td">' . number_format($group_ytd_ape_gross_inception, 2) . '</td>
                            <td class="group-td">' . number_format($group_ytd_ape_aviva_inception, 2) . ' <b>(' . $group_ytd_ape_aviva_inception_count . ')</b></td>
                            <td class="group-td">' . number_format($group_ytd_ape_gi_inception, 2) . ' <b>(' . $group_ytd_ape_gi_inception_count . ')</b></td>
                          </tr>
                          <tr>
                            <td class="expand-td" colspan="9">
                              <div class="accordian-body collapse" id="expand-group-' . $ggfield->id . '-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                <table class="expand-table">';

      $get_group_owner = SalesSupervisor::where('user_id', $ggfield->user_id)->first();

      if ($get_group_owner) {

        $supervisors = [];
        $supervisors[0] = $get_group_owner->id;
        $sctr = 1;

        $get_supervisors_id = SalesSupervisor::with('supervisorinfo')
                                          ->where('group_id', $ggfield->id)
                                          ->where('id', '!=', $get_group_owner->id)->whereIn('user_id', $get_users)
                                          ->lists('id');

        foreach ($get_supervisors_id as $gsikey => $gsivalue) {
          $supervisors[$sctr] = $gsivalue;
          $sctr++;
        }

      } else {

        $supervisors = SalesSupervisor::with('supervisorinfo')
                                          ->where('group_id', $ggfield->id)
                                          ->orderBy('unit_code', 'asc')
                                          ->lists('id');
      }

      $get_providers = Provider::where('status', 1)->get();

      foreach ($supervisors as $gskey => $gsvalue) {

        $gsfield_first = SalesSupervisor::with('supervisorinfo')->find($gsvalue);
        $gsfield = SalesSupervisor::with('supervisorinfo')->whereIn('user_id', $get_users)->find($gsvalue);

        $get_unit_users = SalesSupervisor::where('id', $gsvalue)->whereIn('user_id', $get_users)->lists('user_id');
        $get_unit_advisors_users = SalesAdvisor::where('supervisor_id', $gsvalue)->whereIn('user_id', $get_users)->lists('user_id');

        $get_unit_users = array_merge($get_unit_users->toArray(), $get_unit_advisors_users->toArray());

        //MTD
        $unit_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                  $query->where('incept_date', '>=', $start_month)
                                                        ->where('incept_date', '<=', $end_month);
                                              })
                                            ->whereIn('user_id', $get_unit_users)
                                            ->sum('ape_mtd');

        $unit_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_unit_users)
                                                  ->sum('ape_count_mtd');

        $unit_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

        $unit_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_unit_users)
                                                  ->sum('cat_1_mtd');

        $unit_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_unit_users)
                                                        ->sum('cat_1_count_mtd');
      
        $unit_mtd_ape_gi_inception_count =  PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_unit_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
        
        $count = 0;
        foreach ($unit_mtd_ape_gi_inception_count as $key => $value) {
          if (floatval($value) > 0) {
            $count++;
          }
        }
        $unit_mtd_ape_gi_inception_count = $count;

        $unit_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_unit_users)->sum('gross_revenue_mtd');

        //YTD
        $unit_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                  $query->where('incept_date', '>=', $start_month)
                                                        ->where('incept_date', '<=', $end_month);
                                              })
                                            ->whereIn('user_id', $get_unit_users)
                                            ->sum('ape_ytd');
        
        $unit_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_unit_users)
                                                  ->sum('ape_count_ytd');

        $unit_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_unit_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

        $unit_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_unit_users)
                                                  ->sum('cat_1_ytd');

        $unit_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_unit_users)
                                                        ->sum('cat_1_count_ytd');

        $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year);
                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_unit_users)->lists('upload_id')->toArray());
        
        $total_ytd_gi = 0;
        $policies_not_gi = [];

        foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

          $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('upload_id', $pbvalue);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_unit_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

          $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('upload_id', $pbvalue);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_unit_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

          $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
        }

        $unit_ytd_ape_gi_inception = $total_ytd_gi;

        $unit_ytd_ape_gi_inception_count =  PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_unit_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
        
        $count = 0;
        foreach ($unit_ytd_ape_gi_inception_count as $key => $value) {
          if (floatval($value) > 0) {
            $count++;
          }
        }
        $unit_ytd_ape_gi_inception_count = $count;

        $dashboard_inception .= '<tr class="sup-tr">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-sup-' . $gsfield_first->id . '-inception"><u><b>' . $gsfield_first->unit_code . '</b></u>' . ($gskey == 0 ? ' <i class="fa fa-bookmark"></i>' : '') . '</td>
                                <td class="sup-td">' . number_format($unit_mtd_ape_inception, 2) . ' <b>(' . $unit_mtd_ape_inception_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_mtd_ape_gross_inception, 2) . '</td>
                                <td class="sup-td">' . number_format($unit_mtd_ape_aviva_inception, 2) . ' <b>(' . $unit_mtd_ape_aviva_inception_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_mtd_ape_gi_inception, 2) . ' <b>(' . $unit_mtd_ape_gi_inception_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_ytd_ape_inception, 2) . ' <b>(' . $unit_ytd_ape_inception_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_ytd_ape_gross_inception, 2) . '</td>
                                <td class="sup-td">' . number_format($unit_ytd_ape_aviva_inception, 2) . ' <b>(' . $unit_ytd_ape_aviva_inception_count . ')</b></td>
                                <td class="sup-td">' . number_format($unit_ytd_ape_gi_inception, 2) . ' <b>(' . $unit_ytd_ape_gi_inception_count . ')</b></td>
                              </tr>';

        $dashboard_inception .=  '<tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse" id="expand-sup-' . $gsfield_first->id . '-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';
                                    
        $is_active = false;
        $check_user = User::find($gsfield->user_id);
        if ($check_user) {
          if ($check_user->status == 1) {
            $is_active = true;
          }
        }

        if ($gsfield && $is_active) {

          $name = null;
          if(count($gsfield->supervisorinfo) == 1) {
            $name = $gsfield->supervisorinfo->preferred_name;
          }

          $get_sup_users = [];
          $get_sup_users[0] = $gsfield->user_id;

          //MTD
          $supervisor_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_sup_users)
                                              ->sum('ape_mtd');

          $supervisor_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->whereIn('user_id', $get_sup_users)
                                                          ->sum('ape_count_mtd');

          $supervisor_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_sup_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

          $supervisor_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                            $query->where('incept_date', '>=', $start_month)
                                                                  ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->whereIn('user_id', $get_sup_users)
                                                          ->sum('cat_1_mtd');

          $supervisor_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                      $query->where('incept_date', '>=', $start_month)
                                                                            ->where('incept_date', '<=', $end_month);
                                                                  })
                                                                ->whereIn('user_id', $get_sup_users)
                                                                ->sum('cat_1_count_mtd');

          $supervisor_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
          
          $count = 0;
          foreach ($supervisor_mtd_ape_gi_inception_count as $key => $value) {
            if (floatval($value) > 0) {
              $count++;
            }
          }
          $supervisor_mtd_ape_gi_inception_count = $count;

          $supervisor_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->sum('gross_revenue_mtd');

          //YTD
          $supervisor_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_sup_users)
                                              ->sum('ape_ytd');
          
          $supervisor_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->whereIn('user_id', $get_sup_users)
                                                    ->sum('ape_count_ytd');

          $supervisor_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_sup_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

          $supervisor_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->whereIn('user_id', $get_sup_users)
                                                    ->sum('cat_1_ytd');

          $supervisor_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->whereIn('user_id', $get_sup_users)
                                                          ->sum('cat_1_count_ytd');

          $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->lists('upload_id')->toArray());
          
          $total_ytd_gi = 0;
          $policies_not_gi = [];

          foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

            $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                            $query->where('batch_date', '>=', $start_year)
                                                  ->where('batch_date', '<=', $end_year)
                                                  ->where('upload_id', $pbvalue);
                                      })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

            $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                            $query->where('batch_date', '>=', $start_year)
                                                  ->where('batch_date', '<=', $end_year)
                                                  ->where('upload_id', $pbvalue);
                                      })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

            $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
          }

          $supervisor_ytd_ape_gi_inception = $total_ytd_gi;

          $supervisor_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
          
          $count = 0;
          foreach ($supervisor_ytd_ape_gi_inception_count as $key => $value) {
            if (floatval($value) > 0) {
              $count++;
            }
          }
          $supervisor_ytd_ape_gi_inception_count = $count;

          $dashboard_inception .= '<tr id="i-user-id-' . $gsfield->user_id . '">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gsfield->user_id . '-inception"><u>' . $name . '</u> <i class="fa fa-user"></i></td>
                                <td class="i-mtd-ape-' . $gsfield->user_id . '">' . number_format($supervisor_mtd_ape_inception, 2) . ' <b>(' . $supervisor_mtd_ape_inception_count . ')</b></td>
                                <td class="i-mtd-gross-' . $gsfield->user_id . '">' . number_format($supervisor_mtd_ape_gross_inception, 2) . '</td>
                                <td class="i-mtd-cat-' . $gsfield->user_id . '">' . number_format($supervisor_mtd_ape_aviva_inception, 2) . ' <b>(' . $supervisor_mtd_ape_aviva_inception_count . ')</b></td>
                                <td class="i-mtd-gi-' . $gsfield->user_id . '">' . number_format($supervisor_mtd_ape_gi_inception, 2) . ' <b>(' . $supervisor_mtd_ape_gi_inception_count . ')</b></td>
                                <td class="i-ytd-ape-' . $gsfield->user_id . '">' . number_format($supervisor_ytd_ape_inception, 2) . ' <b>(' . $supervisor_ytd_ape_inception_count . ')</b></td>
                                <td class="i-ytd-gross-' . $gsfield->user_id . '">' . number_format($supervisor_ytd_ape_gross_inception, 2) . '</td>
                                <td class="i-ytd-cat-' . $gsfield->user_id . '">' . number_format($supervisor_ytd_ape_aviva_inception, 2) . ' <b>(' . $supervisor_ytd_ape_aviva_inception_count . ')</b></td>
                                <td class="i-ytd-gi-' . $gsfield->user_id . '">' . number_format($supervisor_ytd_ape_gi_inception, 2) . ' <b>(' . $supervisor_ytd_ape_gi_inception_count . ')</b></td>
                              </tr>
                              <tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse" id="expand-agent-' . $gsfield->user_id . '-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';

          //MTD
          $get_inception_providers_mtd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_sup_users)
                                              ->lists('provider_id')->toArray());

          $get_providers_mtd = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('incept_date', '>=', $start_month)
                                                ->where('incept_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_sup_users)->lists('provider_id')->toArray());

          $get_providers_mtd = array_merge($get_inception_providers_mtd, $get_providers_mtd);

          $get_providers_mtd_batch = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_sup_users)->lists('provider_id')->toArray());

          $get_providers_mtd = array_merge($get_providers_mtd, $get_providers_mtd_batch);
          $get_providers_mtd = array_unique($get_providers_mtd);

          //YTD
          $get_inception_providers_ytd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_sup_users)
                                              ->lists('provider_id')->toArray());

          $get_providers_ytd = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('incept_date', '>=', $start_year)
                                                ->where('incept_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_sup_users)->lists('provider_id')->toArray());

          $get_providers_ytd = array_merge($get_inception_providers_ytd, $get_providers_ytd);

          $get_providers_ytd_batch = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_sup_users)->lists('provider_id')->toArray());

          $get_providers_ytd = array_merge($get_providers_ytd, $get_providers_ytd_batch);
          $get_providers_ytd = array_unique($get_providers_ytd);

          $get_providers_unique = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

          sort($get_providers_unique, SORT_NATURAL | SORT_FLAG_CASE);

          $get_providers = [];
          $gpctr = 0;
          foreach ($get_providers_unique as $gpkey => $gpvalue) {
            $get_providers[$gpctr] = $gpvalue;
            $gpctr++;
          }

          $get_providers = array_filter($get_providers);

          if (count($get_providers) > 0) {

            foreach ($get_providers as $gpkey => $gpvalue) {
              $gpfield = Provider::find($gpvalue);

              if ($gpfield) {

                $get_pinception = ProductionCases::where('user_id', $gsfield->user_id)->groupBy('lastname')->lists('lastname');
                $pinception_expand = '';
                if (count($get_pinception) > 0) {
                  $pinception_expand = 'data-toggle="collapse" data-target="#expand-provider-' . $gsfield->user_id . '-' . $gpfield->id . '-inception"';
                }

                //MTD
                $supervisor_provider_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->where('provider_id', $gpfield->id)
                                                    ->whereIn('user_id', $get_sup_users)
                                                    ->sum('ape_mtd');
                
                $supervisor_provider_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_sup_users)
                                                          ->sum('ape_count_mtd');

                $supervisor_provider_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('batch_date', '>=', $start_month)
                                                      ->where('batch_date', '<=', $end_month)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('provider_id', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_sup_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                $supervisor_provider_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_sup_users)
                                                          ->sum('cat_1_mtd');

                $supervisor_provider_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                      $query->where('incept_date', '>=', $start_month)
                                                                            ->where('incept_date', '<=', $end_month);
                                                                  })
                                                                ->where('provider_id', $gpfield->id)
                                                                ->whereIn('user_id', $get_sup_users)
                                                                ->sum('cat_1_count_mtd');

                $supervisor_provider_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('provider_id', $gpfield->id)
                                                      ->where('batch_date', '>=', $start_month)
                                                      ->where('batch_date', '<=', $end_month)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('status', 1);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                
                $count = 0;
                foreach ($supervisor_provider_mtd_ape_gi_inception_count as $key => $value) {
                  if (floatval($value) > 0) {
                    $count++;
                  }
                }
                $supervisor_provider_mtd_ape_gi_inception_count = $count;

                $supervisor_provider_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('provider_id', $gpfield->id)
                                                      ->where('batch_date', '>=', $start_month)
                                                      ->where('batch_date', '<=', $end_month)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('status', 1);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->sum('gross_revenue_mtd');

                //YTD
                $supervisor_provider_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->where('provider_id', $gpfield->id)
                                                    ->whereIn('user_id', $get_sup_users)
                                                    ->sum('ape_ytd');

                $supervisor_provider_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_sup_users)
                                                          ->sum('ape_count_ytd');

                $supervisor_provider_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('provider_id', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_sup_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                $supervisor_provider_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_sup_users)
                                                          ->sum('cat_1_ytd');
                
                $supervisor_provider_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                      $query->where('incept_date', '>=', $start_month)
                                                                            ->where('incept_date', '<=', $end_month);
                                                                  })
                                                                ->where('provider_id', $gpfield->id)
                                                                ->whereIn('user_id', $get_sup_users)
                                                                ->sum('cat_1_count_ytd');

                $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $gpfield) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('provider_id', $gpfield->id);
                                        })->whereIn('user_id', $get_sup_users)->lists('upload_id')->toArray());
                
                $total_ytd_gi = 0;
                $policies_not_gi = [];

                foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

                  $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                  $query->where('batch_date', '>=', $start_year)
                                                        ->where('batch_date', '<=', $end_year)
                                                        ->where('upload_id', $pbvalue)
                                                        ->where('provider_id', $gpfield->id);
                                            })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

                  $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                  $query->where('batch_date', '>=', $start_year)
                                                        ->where('batch_date', '<=', $end_year)
                                                        ->where('upload_id', $pbvalue)
                                                        ->where('provider_id', $gpfield->id);
                                            })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

                  $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
                }

                $supervisor_provider_ytd_ape_gi_inception = $total_ytd_gi;

                $supervisor_provider_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('provider_id', $gpfield->id)
                                                      ->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('status', 1);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_sup_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                
                $count = 0;
                foreach ($supervisor_provider_ytd_ape_gi_inception_count as $key => $value) {
                  if (floatval($value) > 0) {
                    $count++;
                  }
                }
                $supervisor_provider_ytd_ape_gi_inception_count = $count;

                $dashboard_inception .=  '<tr class="provider-tr">
                                      <td class="first-td td-hover first-td-2" ' . $pinception_expand . '>' . $gpfield->name . '</td>
                                    <td>' . number_format($supervisor_provider_mtd_ape_inception, 2) . ' <b>(' . $supervisor_provider_mtd_ape_inception_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_mtd_ape_gross_inception, 2) . '</td>
                                    <td>' . number_format($supervisor_provider_mtd_ape_aviva_inception, 2) . ' <b>(' . $supervisor_provider_mtd_ape_aviva_inception_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_mtd_ape_gi_inception, 2) . ' <b>(' . $supervisor_provider_mtd_ape_gi_inception_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_ytd_ape_inception, 2) . ' <b>(' . $supervisor_provider_ytd_ape_inception_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_ytd_ape_gross_inception, 2) . '</td>
                                    <td>' . number_format($supervisor_provider_ytd_ape_aviva_inception, 2) . ' <b>(' . $supervisor_provider_ytd_ape_aviva_inception_count . ')</b></td>
                                    <td>' . number_format($supervisor_provider_ytd_ape_gi_inception, 2) . ' <b>(' . $supervisor_provider_ytd_ape_gi_inception_count . ')</b></td>
                                    </tr>';
              }
            }

            $supervisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_mtd');

            $supervisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_count_mtd');

            $supervisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_mtd');

            $supervisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_count_mtd');

            $supervisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_ytd');

            $supervisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_count_ytd');

            $supervisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_ytd');

            $supervisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_count_ytd');
            

            $dashboard_inception .=  '<tr class="provider-tr">
                                  <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                  <td>' . number_format($supervisor_aa_ape_mtd, 2) . ' <b>(' . $supervisor_aa_ape_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($supervisor_aa_cat_1_mtd, 2) . ' <b>(' . $supervisor_aa_cat_1_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($supervisor_aa_ape_ytd, 2) . ' <b>(' . $supervisor_aa_ape_count_ytd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($supervisor_aa_cat_1_ytd, 2) . ' <b>(' . $supervisor_aa_cat_1_count_ytd . ')</b></td>
                                  <td>-</td>
                                </tr>';

          } else {

            $supervisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_mtd');

            $supervisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_count_mtd');

            $supervisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_mtd');

            $supervisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_count_mtd');

            $supervisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_ytd');

            $supervisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('ape_count_ytd');

            $supervisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_ytd');

            $supervisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gsfield->user_id)
                                          ->sum('cat_1_count_ytd');
          

            $dashboard_inception .=  '<tr class="provider-tr">
                                  <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                  <td>' . number_format($supervisor_aa_ape_mtd, 2) . ' <b>(' . $supervisor_aa_ape_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($supervisor_aa_cat_1_mtd, 2) . ' <b>(' . $supervisor_aa_cat_1_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($supervisor_aa_ape_ytd, 2) . ' <b>(' . $supervisor_aa_ape_count_ytd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($supervisor_aa_cat_1_ytd, 2) . ' <b>(' . $supervisor_aa_cat_1_count_ytd . ')</b></td>
                                  <td>-</td>
                                </tr>';
          }

          $dashboard_inception .= '</table>
                              </div>
                              </td>
                              </tr>';

        }

        $get_active_users = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 1)->lists('id');
        $get_advisors = SalesAdvisor::with('advisorinfo')->where('supervisor_id', $gsvalue)->whereIn('user_id', $get_active_users)->get();
        $advisor_ctr = 0;

        foreach ($get_advisors as $gakey => $gafield) {
            
          $name = null;
          $id_name = null;
          if(count($gafield->advisorinfo) == 1) {
            $name = $gafield->advisorinfo->preferred_name;
            $id_name = $gafield->advisorinfo->id;
          }

          $get_adv_users = [];
          $get_adv_users[0] = $gafield->user_id;
          $advisor_ctr++;
          //MTD
          $advisor_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_adv_users)
                                              ->sum('ape_mtd');

          $advisor_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->whereIn('user_id', $get_adv_users)
                                                    ->sum('ape_count_mtd');

          $advisor_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

          $advisor_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->whereIn('user_id', $get_adv_users)
                                                    ->sum('cat_1_mtd');

          $advisor_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->whereIn('user_id', $get_adv_users)
                                                          ->sum('cat_1_count_mtd');

          $advisor_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
          
          $count = 0;
          foreach ($advisor_mtd_ape_gi_inception_count as $key => $value) {
            if (floatval($value) > 0) {
              $count++;
            }
          }
          $advisor_mtd_ape_gi_inception_count = $count;

          $advisor_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->sum('gross_revenue_mtd');

          //YTD
          $advisor_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_adv_users)
                                              ->sum('ape_ytd');

          $advisor_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->whereIn('user_id', $get_adv_users)
                                                    ->sum('ape_count_ytd');

          $advisor_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

          $advisor_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->whereIn('user_id', $get_adv_users)
                                                    ->sum('cat_1_ytd');

          $advisor_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->whereIn('user_id', $get_adv_users)
                                                          ->sum('cat_1_count_ytd');

          $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->lists('upload_id')->toArray());
          
          $total_ytd_gi = 0;
          $policies_not_gi = [];

          foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

            $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                            $query->where('batch_date', '>=', $start_year)
                                                  ->where('batch_date', '<=', $end_year)
                                                  ->where('upload_id', $pbvalue);
                                      })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

            $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                            $query->where('batch_date', '>=', $start_year)
                                                  ->where('batch_date', '<=', $end_year)
                                                  ->where('upload_id', $pbvalue);
                                      })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

            $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
          }

          $advisor_ytd_ape_gi_inception = $total_ytd_gi;

          $advisor_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
          
          $count = 0;
          foreach ($advisor_ytd_ape_gi_inception_count as $key => $value) {
            if (floatval($value) > 0) {
              $count++;
            }
          }
          $advisor_ytd_ape_gi_inception_count = $count;

          $dashboard_inception .=  '<tr class="adv-tr">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gafield->user_id . '-inception">' . $advisor_ctr . '. <u>' . $name . '</u></td>
                                <td>' . number_format($advisor_mtd_ape_inception, 2) . ' <b>(' . $advisor_mtd_ape_inception_count . ')</b></td>
                                <td>' . number_format($advisor_mtd_ape_gross_inception, 2) . '</td>
                                <td>' . number_format($advisor_mtd_ape_aviva_inception, 2) . ' <b>(' . $advisor_mtd_ape_aviva_inception_count . ')</b></td>
                                <td>' . number_format($advisor_mtd_ape_gi_inception, 2) . ' <b>(' . $advisor_mtd_ape_gi_inception_count . ')</b></td>
                                <td>' . number_format($advisor_ytd_ape_inception, 2) . ' <b>(' . $advisor_ytd_ape_inception_count . ')</b></td>
                                <td>' . number_format($advisor_ytd_ape_gross_inception, 2) . '</td>
                                <td>' . number_format($advisor_ytd_ape_aviva_inception, 2) . ' <b>(' . $advisor_ytd_ape_aviva_inception_count . ')</b></td>
                                <td>' . number_format($advisor_ytd_ape_gi_inception, 2) . ' <b>(' . $advisor_ytd_ape_gi_inception_count . ')</b></td>
                              </tr>
                              <tr>
                                <td class="expand-td" colspan="9">
                                  <div class="accordian-body collapse" id="expand-agent-' . $gafield->user_id . '-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                    <table class="expand-table">';

          //MTD
          $get_inception_providers_mtd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_adv_users)
                                              ->lists('provider_id')->toArray());

          $get_providers_mtd = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('incept_date', '>=', $start_month)
                                                ->where('incept_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

          $get_providers_mtd = array_merge($get_inception_providers_mtd, $get_providers_mtd);

          $get_providers_mtd_batch = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                          $query->where('batch_date', '>=', $start_month)
                                                ->where('batch_date', '<=', $end_month)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

          $get_providers_mtd = array_merge($get_providers_mtd, $get_providers_mtd_batch);
          $get_providers_mtd = array_unique($get_providers_mtd);

          //YTD
          $get_inception_providers_ytd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->whereIn('user_id', $get_adv_users)
                                              ->lists('provider_id')->toArray());

          $get_providers_ytd = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('incept_date', '>=', $start_year)
                                                ->where('incept_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

          $get_providers_ytd = array_merge($get_inception_providers_ytd, $get_providers_ytd);

          $get_providers_ytd_batch = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('comp_code', '!=', 'TF')
                                                ->where('comp_code', '!=', 'OF')
                                                ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

          $get_providers_ytd = array_merge($get_providers_ytd, $get_providers_ytd_batch);
          $get_providers_ytd = array_unique($get_providers_ytd);

          $get_providers_unique = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

          sort($get_providers_unique, SORT_NATURAL | SORT_FLAG_CASE);

          $get_providers = [];
          $gpctr = 0;
          foreach ($get_providers_unique as $gpkey => $gpvalue) {
            $get_providers[$gpctr] = $gpvalue;
            $gpctr++;
          }

          $get_providers = array_filter($get_providers);

          if (count($get_providers) > 0) {

            foreach ($get_providers as $gpkey => $gpvalue) {

              $gpfield = Provider::find($gpvalue);

              if ($gpfield) {

                //MTD
                $advisor_provider_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->where('provider_id', $gpfield->id)
                                                    ->whereIn('user_id', $get_adv_users)
                                                    ->sum('ape_mtd');

                $advisor_provider_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_adv_users)
                                                          ->sum('ape_count_mtd');

                $advisor_provider_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('batch_date', '>=', $start_month)
                                                      ->where('batch_date', '<=', $end_month)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('provider_id', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                $advisor_provider_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_adv_users)
                                                          ->sum('cat_1_mtd');

                $advisor_provider_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                      $query->where('incept_date', '>=', $start_month)
                                                                            ->where('incept_date', '<=', $end_month);
                                                                  })
                                                                ->where('provider_id', $gpfield->id)
                                                                ->whereIn('user_id', $get_adv_users)
                                                                ->sum('cat_1_count_mtd');

                $advisor_provider_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('provider_id', $gpfield->id)
                                                      ->where('batch_date', '>=', $start_month)
                                                      ->where('batch_date', '<=', $end_month)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('status', 1);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                
                $count = 0;
                foreach ($advisor_provider_mtd_ape_gi_inception_count as $key => $value) {
                  if (floatval($value) > 0) {
                    $count++;
                  }
                }
                $advisor_provider_mtd_ape_gi_inception_count = $count;

                $advisor_provider_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                $query->where('provider_id', $gpfield->id)
                                                      ->where('batch_date', '>=', $start_month)
                                                      ->where('batch_date', '<=', $end_month)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('status', 1);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->sum('gross_revenue_mtd');

                //YTD
                $advisor_provider_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                          $query->where('incept_date', '>=', $start_month)
                                                                ->where('incept_date', '<=', $end_month);
                                                      })
                                                    ->where('provider_id', $gpfield->id)
                                                    ->whereIn('user_id', $get_adv_users)
                                                    ->sum('ape_ytd');

                $advisor_provider_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_adv_users)
                                                          ->sum('ape_count_ytd');

                $advisor_provider_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('provider_id', $gpfield->id)
                                                      ->where('status', 1);
                                          })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                $advisor_provider_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                $query->where('incept_date', '>=', $start_month)
                                                                      ->where('incept_date', '<=', $end_month);
                                                            })
                                                          ->where('provider_id', $gpfield->id)
                                                          ->whereIn('user_id', $get_adv_users)
                                                          ->sum('cat_1_ytd');

                $advisor_provider_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                      $query->where('incept_date', '>=', $start_month)
                                                                            ->where('incept_date', '<=', $end_month);
                                                                  })
                                                                ->where('provider_id', $gpfield->id)
                                                                ->whereIn('user_id', $get_adv_users)
                                                                ->sum('cat_1_count_ytd');

                $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $gpfield) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('provider_id', $gpfield->id);
                                        })->whereIn('user_id', $get_adv_users)->lists('upload_id')->toArray());
                
                $total_ytd_gi = 0;
                $policies_not_gi = [];

                foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

                  $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                  $query->where('batch_date', '>=', $start_year)
                                                        ->where('batch_date', '<=', $end_year)
                                                        ->where('upload_id', $pbvalue)
                                                        ->where('provider_id', $gpfield->id);
                                            })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

                  $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                  $query->where('batch_date', '>=', $start_year)
                                                        ->where('batch_date', '<=', $end_year)
                                                        ->where('upload_id', $pbvalue)
                                                        ->where('provider_id', $gpfield->id);
                                            })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

                  $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
                }

                $advisor_provider_ytd_ape_gi_inception = $total_ytd_gi;

                $advisor_provider_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                $query->where('provider_id', $gpfield->id)
                                                      ->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('comp_code', '!=', 'TF')
                                                      ->where('comp_code', '!=', 'OF')
                                                      ->where('status', 1);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                
                $count = 0;
                foreach ($advisor_provider_ytd_ape_gi_inception_count as $key => $value) {
                  if (floatval($value) > 0) {
                    $count++;
                  }
                }
                $advisor_provider_ytd_ape_gi_inception_count = $count;

                $dashboard_inception .=  '<tr class="provider-tr">
                                      <td class="first-td td-hover first-td-2">' . $gpfield->name . '</td>
                                      <td>' . number_format($advisor_provider_mtd_ape_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_inception_count . ')</b></td>
                                      <td>' . number_format($advisor_provider_mtd_ape_gross_inception, 2) . '</td>
                                      <td>' . number_format($advisor_provider_mtd_ape_aviva_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_aviva_inception_count . ')</b></td>
                                      <td>' . number_format($advisor_provider_mtd_ape_gi_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_gi_inception_count . ')</b></td>
                                      <td>' . number_format($advisor_provider_ytd_ape_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_inception_count . ')</b></td>
                                      <td>'.  number_format($advisor_provider_ytd_ape_gross_inception, 2) . '</td>
                                      <td>' . number_format($advisor_provider_ytd_ape_aviva_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_aviva_inception_count . ')</b></td>
                                      <td>' . number_format($advisor_provider_ytd_ape_gi_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_gi_inception_count . ')</b></td>
                                    </tr>';
              }
            }

            $advisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_mtd');

            $advisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_count_mtd');

            $advisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_mtd');

            $advisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_count_mtd');

            $advisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_ytd');

            $advisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_count_ytd');

            $advisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_ytd');

            $advisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_count_ytd');
          

            $dashboard_inception .=  '<tr class="provider-tr">
                                  <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                  <td>' . number_format($advisor_aa_ape_mtd, 2) . ' <b>(' . $advisor_aa_ape_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($advisor_aa_cat_1_mtd, 2) . ' <b>(' . $advisor_aa_cat_1_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($advisor_aa_ape_ytd, 2) . ' <b>(' . $advisor_aa_ape_count_ytd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($advisor_aa_cat_1_ytd, 2) . ' <b>(' . $advisor_aa_cat_1_count_ytd . ')</b></td>
                                  <td>-</td>
                                </tr>';

          } else {

            $advisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_mtd');

            $advisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_count_mtd');

            $advisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_mtd');

            $advisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_count_mtd');

            $advisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_ytd');

            $advisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('ape_count_ytd');

            $advisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_ytd');

            $advisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                $query->where('incept_date', '>=', $start_month)
                                                      ->where('incept_date', '<=', $end_month);
                                            })
                                          ->where('provider_id', 0)
                                          ->where('user_id', $gafield->user_id)
                                          ->sum('cat_1_count_ytd');
            

            $dashboard_inception .=  '<tr class="provider-tr">
                                  <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                  <td>' . number_format($advisor_aa_ape_mtd, 2) . ' <b>(' . $advisor_aa_ape_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($advisor_aa_cat_1_mtd, 2) . ' <b>(' . $advisor_aa_cat_1_count_mtd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($advisor_aa_ape_ytd, 2) . ' <b>(' . $advisor_aa_ape_count_ytd . ')</b></td>
                                  <td>-</td>
                                  <td>' . number_format($advisor_aa_cat_1_ytd, 2) . ' <b>(' . $advisor_aa_cat_1_count_ytd . ')</b></td>
                                  <td>-</td>
                                </tr>';
          }

          $dashboard_inception .= '</table>
                              </div>
                              </td>
                              </tr>';
        }

        $get_inactive_users_all = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 0)->lists('id');

        $get_advisors = SalesAdvisor::with('advisorinfo')->where('supervisor_id', $gsvalue)->whereIn('user_id', $get_inactive_users_all)->get();

        $get_supervisors = SalesSupervisor::with('supervisorinfo')->where('id', $gsvalue)->whereIn('user_id', $get_inactive_users_all)->get();

        $get_inactive_users = [];
        $giu = 0;
        foreach ($get_supervisors as $gskey => $gsfield) {
          $get_inactive_users[$giu] = $gsfield->user_id;
          $giu++;
        }

        $inactive_users = SalesAdvisor::with('advisorinfo')->where('supervisor_id', $gsvalue)->whereIn('user_id', $get_inactive_users_all)->lists('user_id');

        foreach ($inactive_users as $iukey => $iuvalue) {
          $get_inactive_users[$giu] = $iuvalue;
          $giu++;
        }

        //MTD
        $resigned_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                  $query->where('incept_date', '>=', $start_month)
                                                        ->where('incept_date', '<=', $end_month);
                                              })
                                            ->whereIn('user_id', $get_inactive_users)
                                            ->sum('ape_mtd');

        $resigned_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_inactive_users)
                                                  ->sum('ape_count_mtd');

        $resigned_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

        $resigned_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_inactive_users)
                                                  ->sum('cat_1_mtd');

        $resigned_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_inactive_users)
                                                        ->sum('cat_1_count_mtd');

        $resigned_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_inactive_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
        
        $count = 0;
        foreach ($resigned_mtd_ape_gi_inception_count as $key => $value) {
          if (floatval($value) > 0) {
            $count++;
          }
        }
        $resigned_mtd_ape_gi_inception_count = $count;

        $resigned_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                        $query->where('batch_date', '>=', $start_month)
                                              ->where('batch_date', '<=', $end_month)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_inactive_users)->sum('gross_revenue_mtd');

        //YTD
        $resigned_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                  $query->where('incept_date', '>=', $start_month)
                                                        ->where('incept_date', '<=', $end_month);
                                              })
                                            ->whereIn('user_id', $get_inactive_users)
                                            ->sum('ape_ytd');
        
        $resigned_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_inactive_users)
                                                  ->sum('ape_count_ytd');

        $resigned_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('user_id', $get_inactive_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

        $resigned_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_inactive_users)
                                                  ->sum('cat_1_ytd');
        
        $resigned_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_inactive_users)
                                                        ->sum('cat_1_count_ytd');
        
        $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                      $query->where('batch_date', '>=', $start_year)
                                            ->where('batch_date', '<=', $end_year);
                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_inactive_users)->lists('upload_id')->toArray());
        
        $total_ytd_gi = 0;
        $policies_not_gi = [];

        foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

          $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('upload_id', $pbvalue);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_inactive_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

          $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                          $query->where('batch_date', '>=', $start_year)
                                                ->where('batch_date', '<=', $end_year)
                                                ->where('upload_id', $pbvalue);
                                    })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_inactive_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

          $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
        }

        $resigned_ytd_ape_gi_inception = $total_ytd_gi;

        $resigned_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                        $query->where('batch_date', '>=', $start_year)
                                              ->where('batch_date', '<=', $end_year)
                                              ->where('comp_code', '!=', 'TF')
                                              ->where('comp_code', '!=', 'OF')
                                              ->where('status', 1);
                                  })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_inactive_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
        
        $count = 0;
        foreach ($resigned_ytd_ape_gi_inception_count as $key => $value) {
          if (floatval($value) > 0) {
            $count++;
          }
        }
        $resigned_ytd_ape_gi_inception_count = $count;

        $dashboard_inception .= '<tr class="sup-tr resigned-inactive-tr">
                                <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-sup-resigned-' . $gsfield_first->id . '-inception"><u>Resigned Agents</u></td>
                                <td>' . number_format($resigned_mtd_ape_inception, 2) . ' <b>(' . $resigned_mtd_ape_inception_count . ')</b></td>
                                <td>' . number_format($resigned_mtd_ape_gross_inception, 2) . '</td>
                                <td>' . number_format($resigned_mtd_ape_aviva_inception, 2) . ' <b>(' . $resigned_mtd_ape_aviva_inception_count . ')</b></td>
                                <td>' . number_format($resigned_mtd_ape_gi_inception, 2) . ' <b>(' . $resigned_mtd_ape_gi_inception_count . ')</b></td>
                                <td>' . number_format($resigned_ytd_ape_inception, 2) . ' <b>(' . $resigned_ytd_ape_inception_count . ')</b></td>
                                <td>' . number_format($resigned_ytd_ape_gross_inception, 2) . '</td>
                                <td>' . number_format($resigned_ytd_ape_aviva_inception, 2) . ' <b>(' . $resigned_ytd_ape_aviva_inception_count . ')</b></td>
                                <td>' . number_format($resigned_ytd_ape_gi_inception, 2) . ' <b>(' . $resigned_ytd_ape_gi_inception_count . ')</b></td>
                              </tr>';

          $dashboard_inception .=  '<!--start--><tr class="tr-resigned-agents">
                                  <td class="expand-td" colspan="9">
                                    <div class="accordian-body collapse" id="expand-sup-resigned-' . $gsfield_first->id . '-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                      <table class="expand-table">';

          if (count($get_advisors) > 0 || count($get_supervisors) > 0) {

            foreach ($get_supervisors as $gakey => $gafield) {
                
              $name = null;
              $id_name = null;
              if(count($gafield->supervisorinfo) == 1) {
                $name = $gafield->supervisorinfo->preferred_name;
                $id_name = $gafield->supervisorinfo->id;
              }

              $get_adv_users = [];
              $get_adv_users[0] = $gafield->user_id;

              //MTD
              $advisor_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_adv_users)
                                                  ->sum('ape_mtd');
            
              $advisor_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_count_mtd');
            
              $advisor_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

              $advisor_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('cat_1_mtd');

              $advisor_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_count_mtd');

              $advisor_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
              
              $count = 0;
              foreach ($advisor_mtd_ape_gi_inception_count as $key => $value) {
                if (floatval($value) > 0) {
                  $count++;
                }
              }
              $advisor_mtd_ape_gi_inception_count = $count;

              $advisor_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->sum('gross_revenue_mtd');

              //YTD
              $advisor_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_adv_users)
                                                  ->sum('ape_ytd');

              $advisor_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_count_ytd');
              
              $advisor_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

              $advisor_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('cat_1_ytd');
              
              $advisor_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_count_ytd');

              $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                            $query->where('batch_date', '>=', $start_year)
                                                  ->where('batch_date', '<=', $end_year);
                                      })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->lists('upload_id')->toArray());
              
              $total_ytd_gi = 0;
              $policies_not_gi = [];

              foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

                $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                                $query->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('upload_id', $pbvalue);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

                $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                                $query->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('upload_id', $pbvalue);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

                $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
              }

              $advisor_ytd_ape_gi_inception = $total_ytd_gi;

              $advisor_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
              
              $count = 0;
              foreach ($advisor_ytd_ape_gi_inception_count as $key => $value) {
                if (floatval($value) > 0) {
                  $count++;
                }
              }
              $advisor_ytd_ape_gi_inception_count = $count;

              $dashboard_inception .=  '<tr class="adv-tr adv-inactive-tr">
                                    <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gafield->user_id . '-inception"><u>' . $name . ' (Resigned)</u></td>
                                    <td>' . number_format($advisor_mtd_ape_inception, 2) . ' <b>(' . $advisor_mtd_ape_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_mtd_ape_gross_inception, 2) . '</td>
                                    <td>' . number_format($advisor_mtd_ape_aviva_inception, 2) . ' <b>(' . $advisor_mtd_ape_aviva_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_mtd_ape_gi_inception, 2) . ' <b>(' . $advisor_mtd_ape_gi_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_inception, 2) . ' <b>(' . $advisor_ytd_ape_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_gross_inception, 2) . '</td>
                                    <td>' . number_format($advisor_ytd_ape_aviva_inception, 2) . ' <b>(' . $advisor_ytd_ape_aviva_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_gi_inception, 2) . ' <b>(' . $advisor_ytd_ape_gi_inception_count . ')</b></td>
                                  </tr>
                                  <tr>
                                    <td class="expand-td" colspan="9">
                                      <div class="accordian-body collapse" id="expand-agent-' . $gafield->user_id . '-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                        <table class="expand-table">';

              //MTD
              $get_inception_providers_mtd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_adv_users)
                                                  ->lists('provider_id')->toArray());
              
              $get_providers_mtd = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('incept_date', '>=', $start_month)
                                                    ->where('incept_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

              $get_providers_mtd = array_merge($get_inception_providers_mtd, $get_providers_mtd);

              $get_providers_mtd_batch = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

              $get_providers_mtd = array_merge($get_providers_mtd, $get_providers_mtd_batch);
              $get_providers_mtd = array_unique($get_providers_mtd);

              //YTD
              $get_inception_providers_ytd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_adv_users)
                                                  ->lists('provider_id')->toArray());

              $get_providers_ytd = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('incept_date', '>=', $start_year)
                                                    ->where('incept_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

              $get_providers_ytd = array_merge($get_inception_providers_ytd, $get_providers_ytd);

              $get_providers_ytd_batch = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->lists('provider_id')->toArray());

              $get_providers_ytd = array_merge($get_inception_providers_ytd, $get_providers_ytd);
              $get_providers_ytd = array_unique($get_providers_ytd);

              $get_providers_unique = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

              sort($get_providers_unique, SORT_NATURAL | SORT_FLAG_CASE);

              $get_providers = [];
              $gpctr = 0;
              foreach ($get_providers_unique as $gpkey => $gpvalue) {
                $get_providers[$gpctr] = $gpvalue;
                $gpctr++;
              }

              $get_providers = array_filter($get_providers);

              if (count($get_providers) > 0) {

                foreach ($get_providers as $gpkey => $gpvalue) {

                  $gpfield = Provider::find($gpvalue);

                  if ($gpfield) {

                    //MTD
                    $advisor_provider_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->where('provider_id', $gpfield->id)
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_mtd');
                    
                    $advisor_provider_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('ape_count_mtd');

                    $advisor_provider_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('batch_date', '>=', $start_month)
                                                          ->where('batch_date', '<=', $end_month)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('provider_id', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                    $advisor_provider_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_mtd');

                    $advisor_provider_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                          $query->where('incept_date', '>=', $start_month)
                                                                                ->where('incept_date', '<=', $end_month);
                                                                      })
                                                                    ->where('provider_id', $gpfield->id)
                                                                    ->whereIn('user_id', $get_adv_users)
                                                                    ->sum('cat_1_count_mtd');

                    $advisor_provider_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('provider_id', $gpfield->id)
                                                          ->where('batch_date', '>=', $start_month)
                                                          ->where('batch_date', '<=', $end_month)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('status', 1);
                                              })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                    
                    $count = 0;
                    foreach ($advisor_provider_mtd_ape_gi_inception_count as $key => $value) {
                      if (floatval($value) > 0) {
                        $count++;
                      }
                    }
                    $advisor_provider_mtd_ape_gi_inception_count = $count;

                    $advisor_provider_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('provider_id', $gpfield->id)
                                                          ->where('batch_date', '>=', $start_month)
                                                          ->where('batch_date', '<=', $end_month)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('status', 1);
                                              })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->sum('gross_revenue_mtd');

                    //YTD
                    $advisor_provider_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->where('provider_id', $gpfield->id)
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_ytd');
                    
                    $advisor_provider_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('ape_count_ytd');

                    $advisor_provider_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('batch_date', '>=', $start_year)
                                                          ->where('batch_date', '<=', $end_year)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('provider_id', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                    $advisor_provider_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_ytd');
                      
                    $advisor_provider_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                          $query->where('incept_date', '>=', $start_month)
                                                                                ->where('incept_date', '<=', $end_month);
                                                                      })
                                                                    ->where('provider_id', $gpfield->id)
                                                                    ->whereIn('user_id', $get_adv_users)
                                                                    ->sum('cat_1_count_ytd');
                    

                    $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                  $query->where('batch_date', '>=', $start_year)
                                                        ->where('batch_date', '<=', $end_year)
                                                        ->where('provider_id', $gpfield->id);
                                            })->whereIn('user_id', $get_adv_users)->lists('upload_id')->toArray());
                    
                    $total_ytd_gi = 0;
                    $policies_not_gi = [];

                    foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

                      $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                      $query->where('batch_date', '>=', $start_year)
                                                            ->where('batch_date', '<=', $end_year)
                                                            ->where('upload_id', $pbvalue)
                                                            ->where('provider_id', $gpfield->id);
                                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

                      $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                      $query->where('batch_date', '>=', $start_year)
                                                            ->where('batch_date', '<=', $end_year)
                                                            ->where('upload_id', $pbvalue)
                                                            ->where('provider_id', $gpfield->id);
                                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

                      $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
                    }

                    $advisor_provider_ytd_ape_gi_inception = $total_ytd_gi;

                    $advisor_provider_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('provider_id', $gpfield->id)
                                                          ->where('batch_date', '>=', $start_year)
                                                          ->where('batch_date', '<=', $end_year)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('status', 1);
                                              })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                    
                    $count = 0;
                    foreach ($advisor_provider_ytd_ape_gi_inception_count as $key => $value) {
                      if (floatval($value) > 0) {
                        $count++;
                      }
                    }
                    $advisor_provider_ytd_ape_gi_inception_count = $count;

                    $dashboard_inception .=  '<tr class="provider-tr provider-inactive-tr">
                                          <td class="first-td td-hover first-td-2">' . $gpfield->name . '</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_mtd_ape_gross_inception, 2) . '</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_aviva_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_aviva_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_mtd_ape_gi_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_gi_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_gross_inception, 2) . '</td>
                                          <td>' . number_format($advisor_provider_ytd_ape_aviva_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_aviva_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_gi_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_gi_inception_count . ')</b></td>
                                        </tr>';
                  }
                }

                $advisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_mtd');

                $advisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_mtd');

                $advisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_mtd');

                $advisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_mtd');

                $advisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_ytd');

                $advisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_ytd');

                $advisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_ytd');

                $advisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_ytd');
              
                $dashboard_inception .=  '<tr class="provider-tr provider-inactive-tr">
                                      <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                      <td>' . number_format($advisor_aa_ape_mtd, 2) . ' <b>(' . $advisor_aa_ape_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_mtd, 2) . ' <b>(' . $advisor_aa_cat_1_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_ape_ytd, 2) . ' <b>(' . $advisor_aa_ape_count_ytd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_ytd, 2) . ' <b>(' . $advisor_aa_cat_1_count_ytd . ')</b></td>
                                      <td>-</td>
                                    </tr>';

              } else {

                $advisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_mtd');

                $advisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_mtd');

                $advisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_mtd');

                $advisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_mtd');

                $advisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_ytd');

                $advisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_ytd');

                $advisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_ytd');

                $advisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_ytd');

                $dashboard_inception .=  '<tr class="provider-tr provider-inactive-tr">
                                      <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                      <td>' . number_format($advisor_aa_ape_mtd, 2) . ' <b>(' . $advisor_aa_ape_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_mtd, 2) . ' <b>(' . $advisor_aa_cat_1_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_ape_ytd, 2) . ' <b>(' . $advisor_aa_ape_count_ytd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_ytd, 2) . ' <b>(' . $advisor_aa_cat_1_count_ytd . ')</b></td>
                                      <td>-</td>
                                    </tr>';
              }

              $dashboard_inception .= '</table>
                                  </div>
                                  </td>
                                  </tr>';
            
            }

            foreach ($get_advisors as $gakey => $gafield) {
                
              $name = null;
              $id_name = null;
              if(count($gafield->advisorinfo) == 1) {
                $name = $gafield->advisorinfo->preferred_name;
                $id_name = $gafield->advisorinfo->id;
              }

              $get_adv_users = [];
              $get_adv_users[0] = $gafield->user_id;

              //MTD
              $advisor_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_adv_users)
                                                  ->sum('ape_mtd');
              
              $advisor_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_count_mtd');

              $advisor_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

              $advisor_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('cat_1_mtd');
              

              $advisor_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_count_mtd');

              $advisor_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
              
              $count = 0;
              foreach ($advisor_mtd_ape_gi_inception_count as $key => $value) {
                if (floatval($value) > 0) {
                  $count++;
                }
              }
              $advisor_mtd_ape_gi_inception_count = $count;

              $advisor_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->sum('gross_revenue_mtd');

              //YTD
              $advisor_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->whereIn('user_id', $get_adv_users)
                                                  ->sum('ape_ytd');
              
              $advisor_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_count_ytd');

              $advisor_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

              $advisor_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('cat_1_ytd');

              $advisor_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_count_ytd');

              $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year) {
                                            $query->where('batch_date', '>=', $start_year)
                                                  ->where('batch_date', '<=', $end_year);
                                      })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->lists('upload_id')->toArray());
              
              $total_ytd_gi = 0;
              $policies_not_gi = [];

              foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

                $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                                $query->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('upload_id', $pbvalue);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

                $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue) {
                                                $query->where('batch_date', '>=', $start_year)
                                                      ->where('batch_date', '<=', $end_year)
                                                      ->where('upload_id', $pbvalue);
                                          })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

                $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
              }

              $advisor_ytd_ape_gi_inception = $total_ytd_gi;

              $advisor_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                        })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
              
              $count = 0;
              foreach ($advisor_ytd_ape_gi_inception_count as $key => $value) {
                if (floatval($value) > 0) {
                  $count++;
                }
              }
              $advisor_ytd_ape_gi_inception_count = $count;

              $dashboard_inception .=  '<tr class="adv-tr adv-inactive-tr">
                                    <td class="first-td first-td-1 td-hover" data-toggle="collapse" data-target="#expand-agent-' . $gafield->user_id . '-inception"><u>' . $name . ' (Resigned)</u></td>
                                    <td>' . number_format($advisor_mtd_ape_inception, 2) . ' <b>(' . $advisor_mtd_ape_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_mtd_ape_gross_inception, 2) . '</td>
                                    <td>' . number_format($advisor_mtd_ape_aviva_inception, 2) . ' <b>(' . $advisor_mtd_ape_aviva_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_mtd_ape_gi_inception, 2) . ' <b>(' . $advisor_mtd_ape_gi_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_inception, 2) . ' <b>(' . $advisor_ytd_ape_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_gross_inception, 2) . '</td>
                                    <td>' . number_format($advisor_ytd_ape_aviva_inception, 2) . ' <b>(' . $advisor_ytd_ape_aviva_inception_count . ')</b></td>
                                    <td>' . number_format($advisor_ytd_ape_gi_inception, 2) . ' <b>(' . $advisor_ytd_ape_gi_inception_count . ')</b></td>
                                  </tr>
                                  <tr>
                                    <td class="expand-td" colspan="9">
                                      <div class="accordian-body collapse" id="expand-agent-' . $gafield->user_id . '-inception" style="margin-bottom: 0px;" aria-expanded="true">
                                        <table class="expand-table">';

              //MTD
              $get_inception_providers_mtd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->where('user_id', $gafield->user_id)
                                                  ->lists('provider_id')->toArray());

              $get_providers_mtd = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('incept_date', '>=', $start_month)
                                                    ->where('incept_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->where('user_id', $gafield->user_id)->lists('provider_id')->toArray());

              $get_providers_mtd = array_merge($get_inception_providers_mtd, $get_providers_mtd);

              $get_providers_mtd_batch = array_unique(PayrollComputation::where(function($query) use ($start_month, $end_month) {
                                              $query->where('batch_date', '>=', $start_month)
                                                    ->where('batch_date', '<=', $end_month)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->where('user_id', $gafield->user_id)->lists('provider_id')->toArray());

              $get_providers_mtd = array_merge($get_providers_mtd, $get_providers_mtd_batch);
              $get_providers_mtd = array_unique($get_providers_mtd);

              //YTD
              $get_inception_providers_ytd = array_unique(Inception::where(function($query) use ($start_month, $end_month) {
                                                        $query->where('incept_date', '>=', $start_month)
                                                              ->where('incept_date', '<=', $end_month);
                                                    })
                                                  ->where('user_id', $gafield->user_id)
                                                  ->lists('provider_id')->toArray());

              $get_providers_ytd = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('incept_date', '>=', $start_year)
                                                    ->where('incept_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->where('user_id', $gafield->user_id)->lists('provider_id')->toArray());

              $get_providers_ytd = array_merge($get_inception_providers_ytd, $get_providers_ytd);

              $get_providers_ytd_batch = array_unique(PayrollComputation::where(function($query) use ($start_year, $end_year) {
                                              $query->where('batch_date', '>=', $start_year)
                                                    ->where('batch_date', '<=', $end_year)
                                                    ->where('comp_code', '!=', 'TF')
                                                    ->where('comp_code', '!=', 'OF')
                                                    ->where('status', 1);
                                              })->where('user_id', $gafield->user_id)->lists('provider_id')->toArray());

              $get_providers_ytd = array_merge($get_providers_ytd, $get_providers_ytd_batch);
              $get_providers_ytd = array_unique($get_providers_ytd);

              $get_providers_unique = array_unique(array_merge($get_providers_mtd, $get_providers_ytd));

              sort($get_providers_unique, SORT_NATURAL | SORT_FLAG_CASE);

              $get_providers = [];
              $gpctr = 0;
              foreach ($get_providers_unique as $gpkey => $gpvalue) {
                $get_providers[$gpctr] = $gpvalue;
                $gpctr++;
              }

              $get_providers = array_filter($get_providers);

              if (count($get_providers) > 0) {

                foreach ($get_providers as $gpkey => $gpvalue) {

                  $gpfield = Provider::find($gpvalue);

                  if ($gpfield) {

                    //MTD
                    $advisor_provider_mtd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->where('provider_id', $gpfield->id)
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_mtd');

                    $advisor_provider_mtd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('ape_count_mtd');

                    $advisor_provider_mtd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('batch_date', '>=', $start_month)
                                                          ->where('batch_date', '<=', $end_month)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('provider_id', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                    $advisor_provider_mtd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_mtd');

                    $advisor_provider_mtd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                          $query->where('incept_date', '>=', $start_month)
                                                                                ->where('incept_date', '<=', $end_month);
                                                                      })
                                                                    ->where('provider_id', $gpfield->id)
                                                                    ->whereIn('user_id', $get_adv_users)
                                                                    ->sum('cat_1_count_mtd');

                    $advisor_provider_mtd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('provider_id', $gpfield->id)
                                                          ->where('batch_date', '>=', $start_month)
                                                          ->where('batch_date', '<=', $end_month)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('status', 1);
                                              })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                    
                    $count = 0;
                    foreach ($advisor_provider_mtd_ape_gi_inception_count as $key => $value) {
                      if (floatval($value) > 0) {
                        $count++;
                      }
                    }
                    $advisor_provider_mtd_ape_gi_inception_count = $count;

                    $advisor_provider_mtd_ape_gi_inception = PayrollComputation::where(function($query) use ($start_month, $end_month, $gpfield) {
                                                    $query->where('provider_id', $gpfield->id)
                                                          ->where('batch_date', '>=', $start_month)
                                                          ->where('batch_date', '<=', $end_month)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('status', 1);
                                              })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->sum('gross_revenue_mtd');

                    //YTD
                    $advisor_provider_ytd_ape_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                              $query->where('incept_date', '>=', $start_month)
                                                                    ->where('incept_date', '<=', $end_month);
                                                          })
                                                        ->where('provider_id', $gpfield->id)
                                                        ->whereIn('user_id', $get_adv_users)
                                                        ->sum('ape_ytd');
                    
                    $advisor_provider_ytd_ape_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('ape_count_ytd');

                    $advisor_provider_ytd_ape_gross_inception = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('batch_date', '>=', $start_year)
                                                          ->where('batch_date', '<=', $end_year)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('provider_id', $gpfield->id)
                                                          ->where('status', 1);
                                              })->whereIn('user_id', $get_adv_users)->whereNotIn('category_id', $get_gi_ids)->sum('gross_revenue_mtd');

                    $advisor_provider_ytd_ape_aviva_inception = Inception::where(function($query) use ($start_month, $end_month) {
                                                                    $query->where('incept_date', '>=', $start_month)
                                                                          ->where('incept_date', '<=', $end_month);
                                                                })
                                                              ->where('provider_id', $gpfield->id)
                                                              ->whereIn('user_id', $get_adv_users)
                                                              ->sum('cat_1_ytd');
                    
                    $advisor_provider_ytd_ape_aviva_inception_count = Inception::where(function($query) use ($start_month, $end_month) {
                                                                          $query->where('incept_date', '>=', $start_month)
                                                                                ->where('incept_date', '<=', $end_month);
                                                                      })
                                                                    ->where('provider_id', $gpfield->id)
                                                                    ->whereIn('user_id', $get_adv_users)
                                                                    ->sum('cat_1_count_ytd');
                    
                    $policies_batch_ytd = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                  $query->where('batch_date', '>=', $start_year)
                                                        ->where('batch_date', '<=', $end_year)
                                                        ->where('provider_id', $gpfield->id);
                                            })->whereIn('user_id', $get_adv_users)->lists('upload_id')->toArray());
                    
                    $total_ytd_gi = 0;
                    $policies_not_gi = [];

                    foreach ($policies_batch_ytd as $pbkey => $pbvalue) {

                      $total_ytd_gi += PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                      $query->where('batch_date', '>=', $start_year)
                                                            ->where('batch_date', '<=', $end_year)
                                                            ->where('upload_id', $pbvalue)
                                                            ->where('provider_id', $gpfield->id);
                                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->sum('gross');

                      $policies_month_gi = array_unique(PayrollInception::where(function($query) use ($start_year, $end_year, $pbvalue, $gpfield) {
                                                      $query->where('batch_date', '>=', $start_year)
                                                            ->where('batch_date', '<=', $end_year)
                                                            ->where('upload_id', $pbvalue)
                                                            ->where('provider_id', $gpfield->id);
                                                })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->whereNotIn('policy_no', $policies_not_gi)->lists('policy_no')->toArray());

                      $policies_not_gi = array_merge($policies_not_gi, $policies_month_gi);
                    }

                    $advisor_provider_ytd_ape_gi_inception = $total_ytd_gi;

                    $advisor_provider_ytd_ape_gi_inception_count = PayrollComputation::where(function($query) use ($start_year, $end_year, $gpfield) {
                                                    $query->where('provider_id', $gpfield->id)
                                                          ->where('batch_date', '>=', $start_year)
                                                          ->where('batch_date', '<=', $end_year)
                                                          ->where('comp_code', '!=', 'TF')
                                                          ->where('comp_code', '!=', 'OF')
                                                          ->where('status', 1);
                                              })->whereIn('category_id', $get_gi_ids)->whereIn('user_id', $get_adv_users)->groupBy('policy_no')->selectRaw('sum(premium_freq_mtd) as total_premium')->lists('total_premium');
                    
                    $count = 0;
                    foreach ($advisor_provider_ytd_ape_gi_inception_count as $key => $value) {
                      if (floatval($value) > 0) {
                        $count++;
                      }
                    }
                    $advisor_provider_ytd_ape_gi_inception_count = $count;

                    $dashboard_inception .=  '<tr class="provider-tr provider-inactive-tr">
                                          <td class="first-td td-hover first-td-2">' . $gpfield->name . '</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_mtd_ape_gross_inception, 2) . '</td>
                                          <td>' . number_format($advisor_provider_mtd_ape_aviva_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_aviva_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_mtd_ape_gi_inception, 2) . ' <b>(' . $advisor_provider_mtd_ape_gi_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_gross_inception, 2) . '</td>
                                          <td>' . number_format($advisor_provider_ytd_ape_aviva_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_aviva_inception_count . ')</b></td>
                                          <td>' . number_format($advisor_provider_ytd_ape_gi_inception, 2) . ' <b>(' . $advisor_provider_ytd_ape_gi_inception_count . ')</b></td>
                                        </tr>';
                  }
                }

                $advisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_mtd');

                $advisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_mtd');

                $advisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_mtd');

                $advisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_mtd');

                $advisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_ytd');

                $advisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_ytd');

                $advisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_ytd');

                $advisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_ytd');
                

                $dashboard_inception .=  '<tr class="provider-tr provider-inactive-tr">
                                      <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                      <td>' . number_format($advisor_aa_ape_mtd, 2) . ' <b>(' . $advisor_aa_ape_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_mtd, 2) . ' <b>(' . $advisor_aa_cat_1_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_ape_ytd, 2) . ' <b>(' . $advisor_aa_ape_count_ytd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_ytd, 2) . ' <b>(' . $advisor_aa_cat_1_count_ytd . ')</b></td>
                                      <td>-</td>
                                    </tr>';

              } else {

                $advisor_aa_ape_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_mtd');

                $advisor_aa_ape_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_mtd');

                $advisor_aa_cat_1_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_mtd');

                $advisor_aa_cat_1_count_mtd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_mtd');

                $advisor_aa_ape_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_ytd');

                $advisor_aa_ape_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('ape_count_ytd');

                $advisor_aa_cat_1_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_ytd');

                $advisor_aa_cat_1_count_ytd = Inception::where(function($query) use ($start_month, $end_month) {
                                                    $query->where('incept_date', '>=', $start_month)
                                                          ->where('incept_date', '<=', $end_month);
                                                })
                                              ->where('provider_id', 0)
                                              ->where('user_id', $gafield->user_id)
                                              ->sum('cat_1_count_ytd');

                $dashboard_inception .=  '<tr class="provider-tr provider-inactive-tr">
                                      <td class="first-td td-hover first-td-2">Production Tagged to AA</td>
                                      <td>' . number_format($advisor_aa_ape_mtd, 2) . ' <b>(' . $advisor_aa_ape_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_mtd, 2) . ' <b>(' . $advisor_aa_cat_1_count_mtd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_ape_ytd, 2) . ' <b>(' . $advisor_aa_ape_count_ytd . ')</b></td>
                                      <td>-</td>
                                      <td>' . number_format($advisor_aa_cat_1_ytd, 2) . ' <b>(' . $advisor_aa_cat_1_count_ytd . ')</b></td>
                                      <td>-</td>
                                    </tr>';
              }

              $dashboard_inception .= '</table>
                                  </div>
                                  </td>
                                  </tr>';
            }

            $dashboard_inception .= '</table>
                                </div>
                                </td>
                                </tr><!--end-->';

        } else {

            $dashboard_inception .=  '<tr class="sup-tr adv-inactive-tr">
                                  <td class="first-td td-hover first-td-1"></td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                  <td>-</td>
                                </tr>';

            $dashboard_inception .= '</table>
                                </div>
                                </td>
                                </tr><!--end-->';
        }

        $dashboard_inception .= '</table>
                            </div>
                            </td>
                            </tr>';
      }

      $dashboard_inception .= '</table></div></td></tr>';

    }

    if(Request::input('loop') == Request::input('end')) {
      $dashboard_inception .= '</table></div></td></tr>';

      $dashboard_inception .= '<tr class="footer-tr">
                                <td class="footer-td" colspan="9">Updated as of <b>' . $as_of_month . '</b></td>
                              </tr>';

      $dashboard_inception .= '</table>
                              </div>
                            </div>';

      $dashboard_inception .= '<div class="col-xs-12 no-padding" style="padding-bottom: 10px;">
                                  <div class="progress dashboard-progress-inceptions hide">
                                    <div class="progress-bar progress-bar-success progress-inception" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                      <span class="sr-only"></span>
                                    </div>
                                  </div>
                                </div>';
    }

    return Response::json($dashboard_inception);
  }

  public function updateInception() {

    if (Request::input('dashboard')) {
      $row = new DashboardInception;
      $row->dashboard = Request::input('dashboard');

      $dated = Request::input('date');
      if ($dated) {
        $incept_date = Carbon::createFromFormat('M Y', $dated)->startOfMonth();
      } else {
        $incept_date = Carbon::now()->startOfMonth();
      }

      $row->incept_date = $incept_date;

      $row->save();
    }

    return Response::json('success');
  }


  public function monthInception() {

    $dated = Request::input('date');

    $batch_date = Carbon::createFromFormat('M Y', $dated)->startOfMonth()->toDateString();

    $dashboard_inception = DashboardInception::where('incept_date', '=', $batch_date . ' 00:00:00')->orderBy('created_at', 'desc')->first();
    $dashboard_date = Carbon::now()->addMonth(-1)->format('M Y');

    if ($dashboard_inception) {
      $dashboard_date = date_format(date_create(substr($dashboard_inception->incept_date, 0,10)),'M Y');
      $dashboard_inception = $dashboard_inception->dashboard;
    } else if (!$dashboard_inception) {
      $dashboard_inception = DashboardInception::orderBy('incept_date', 'desc')->orderBy('created_at', 'desc')->first();
      if ($dashboard_inception) {
        $dashboard_date = date_format(date_create(substr($dashboard_inception->incept_date, 0,10)),'M Y');
        $dashboard_none = "<div class='col-xs-12' style='padding-right: 0; padding-left: 0;'><span style='color: red;'>No results found in <strong>" . $dated . "</strong>.</span><div>";
        $dashboard_inception = $dashboard_none . $dashboard_inception->dashboard;
      }
    }

    return Response::json($dashboard_inception);
  }

  public function monthCase() {

    $dated = Request::input('date');

    $batch_date = Carbon::createFromFormat('M Y', $dated)->startOfMonth()->toDateString();

    $dashboard_case = DashboardSubmission::where('incept_date', '=', $batch_date . ' 00:00:00')->orderBy('created_at', 'desc')->first();
    $dashboard_case_date = Carbon::now()->addMonth(-1)->format('M Y');

    if ($dashboard_case) {
      $dashboard_case_date = date_format(date_create(substr($dashboard_case->incept_date, 0,10)),'M Y');
      $dashboard_case = $dashboard_case->dashboard;
    } else if (!$dashboard_case) {
      $dashboard_case = DashboardSubmission::orderBy('incept_date', 'desc')->orderBy('created_at', 'desc')->first();
      if ($dashboard_case) {
        $dashboard_case_date = date_format(date_create(substr($dashboard_case->incept_date, 0,10)),'M Y');
        $dashboard_none = "<div class='col-xs-12' style='padding-right: 0; padding-left: 0;'><span style='color: red;'>No results found in <strong>" . $dated . "</strong>.</span><div>";
        $dashboard_case = $dashboard_none . $dashboard_case->dashboard;
      }
    }

    return Response::json($dashboard_case);
  }


  public function getCPD() {

    $default_structured = SettingsCPD::where('id', 1)->sum('no_of_hours');
    $default_ethics = SettingsCPD::where('id', 2)->sum('no_of_hours');
    $default_rules = SettingsCPD::where('id', 3)->sum('no_of_hours');
    $default_total = $default_structured + $default_ethics + $default_rules;
    $today_year = Carbon::now()->format('Y');
    $input_year = Request::input('year') ? Request::input('year') : $today_year;

    $get_users = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 1)->lists('id');

    $dashboard_cpd = '';

    $check_supervisor = SalesSupervisor::where('user_id', Auth::user()->id)->first();

    if ($check_supervisor) {

      $users[0] = '{|}' . Auth::user()->id . '{|} <i class="fa fa-bookmark"></i>';

      $total_cpd_hours_skills = CPDHours::whereIn('user_id', $users)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_skills');
      $total_cpd_hours_knowledge = CPDHours::whereIn('user_id', $users)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_knowledge');
      $total_cpd_hours_rules_and_regulations = CPDHours::whereIn('user_id', $users)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_rules_and_regulations');
      $total_cpd_hours_gi = CPDHours::whereIn('user_id', $users)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_gi');
      $total_cpd_hours_final = $total_cpd_hours_skills + $total_cpd_hours_knowledge + $total_cpd_hours_rules_and_regulations;

      $get_advisors = SalesAdvisor::whereIn('user_id', $get_users)->where('supervisor_id', $check_supervisor->id)->lists('user_id');
      $ctr = 1;
      foreach ($get_advisors as $gakey => $gavalue) {
        $users[$ctr] = '{|}' . $gavalue. '{|}';
        $ctr++;
      }

      $check_group = Group::whereIn('user_id', $get_users)->where('user_id', Auth::user()->id)->first();

      if ($check_group) {
        $get_supervisors = SalesSupervisor::whereIn('user_id', $get_users)->where('group_id', $check_group->id)->where('id', '!=', $check_supervisor->id)->get();

        foreach ($get_supervisors as $gskey => $gsfield) {
          $users[$ctr] = '{|}' . $gsfield->user_id. '{|} <i class="fa fa-user"></i>';
          $ctr++;
          $get_supervisor_advisors = SalesAdvisor::whereIn('user_id', $get_users)->where('supervisor_id', $gsfield->id)->lists('user_id');
          $gsactr = 1;

          foreach ($get_supervisor_advisors as $gsakey => $gsavalue) {
            $users[$ctr] = '{|}' . $gsavalue. '{|}';
            $ctr++;
            $gsactr++;
          }
        }
      }

      $dashboard_cpd .= '<div class="col-xs-12" style="display: block;">
            <div class="table-responsive dashboard-responsive">
              <table class="table dashboard-table">
                <tr class="total-tr">
                  <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-cpd-' . $input_year . '"><u>' . $input_year . '</u></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_skills, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_knowledge, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_rules_and_regulations, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_final, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_gi, 2) . '</big></center></td>
                </tr>';
        
      $dashboard_cpd .= '<tr>
                <td class="expand-td" colspan="9">
                  <div class="accordian-body collapse in" id="expand-cpd-' . $input_year . '" style="margin-bottom: 0px;" aria-expanded="true">
                    <table class="expand-table">';


      foreach ($users as $ukey => $uvalue) {
        
        $euvalue = explode("{|}", $uvalue);
        $uvalue = $euvalue[1];

        $user_cpd_hours_skills = CPDHours::where('user_id', $uvalue)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_skills');
        $user_cpd_hours_knowledge = CPDHours::where('user_id', $uvalue)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_knowledge');
        $user_cpd_hours_rules_and_regulations = CPDHours::where('user_id', $uvalue)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_rules_and_regulations');
        $user_cpd_hours_gi = CPDHours::where('user_id', $uvalue)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_gi');
        $user_cpd_hours_final = $user_cpd_hours_skills + $user_cpd_hours_knowledge + $user_cpd_hours_rules_and_regulations;
        $user = User::find($uvalue);

        if ($user) {
          $name = ($user->preferred_name ? $user->preferred_name : $user->name);
        }

        $dashboard_cpd .= '<tr class="group-cpd-tr">
                <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-user_' . $uvalue . '-' . $input_year . '"><b>' . $euvalue[0] . '<u>' . $name . '</u>' . $euvalue[2] . '</b></td>
                <td class="group-cpd-td"><center><big>' . number_format($user_cpd_hours_skills, 2) . '</big></center></td>
                <td class="group-cpd-td"><center><big>' . number_format($user_cpd_hours_knowledge, 2) . '</big></center></td>
                <td class="group-cpd-td"><center><big>' . number_format($user_cpd_hours_rules_and_regulations, 2) . '</big></center></td>
                <td class="group-cpd-td"><center><big>' . number_format($user_cpd_hours_final, 2) . '</big></center></td>
                <td class="group-cpd-td"><center><big>' . number_format($user_cpd_hours_gi, 2) . '</big></center></td>
              </tr>';

        $dashboard_cpd .= '<tr>
                  <td class="expand-td" colspan="9">
                    <div class="accordian-body collapse" id="expand-user_' . $uvalue . '-' . $input_year . '" style="margin-bottom: 0px;" aria-expanded="true">
                      <table class="expand-table">';

        $rows = CPDHours::where('user_id', $uvalue)->whereYear('cpd_starting_date', '=', $input_year)->get();
        
        if (count($rows) > 0) {
          foreach ($rows as $key => $field) {
            $dashboard_cpd .= '<tr>
                                <td class="first-td"><small>' . $field->course . ' - ' . $field->cpd_period . '</small></td>
                                <td><center><big>' . number_format($field->total_cpd_hours_skills, 2) . '</big></center></td>
                                <td><center><big>' . number_format($field->total_cpd_hours_knowledge, 2) . '</big></center></td>
                                <td><center><big>' . number_format($field->total_cpd_hours_rules_and_regulations, 2) . '</big></center></td>
                                <td><center><big>' . number_format($field->total_cpd_hours_skills + $field->total_cpd_hours_knowledge + $field->total_cpd_hours_rules_and_regulations, 2) . '</big></center></td>
                                <td><center><big>' . number_format($field->total_cpd_hours_gi, 2) . '</big></center></td>
                              </tr>';
          }
        } else {
          $dashboard_cpd .= '<tr>
                              <td class="first-td"></td>
                              <td><center><big>-</big></center></td>
                              <td><center><big>-</big></center></td>
                              <td><center><big>-</big></center></td>
                              <td><center><big>-</big></center></td>
                              <td><center><big>-</big></center></td>
                            </tr>';
        }

        $dashboard_cpd .= '</table>
                    </div>
                  </td>
                </tr>';
      }

    } else {

      $total_cpd_hours_skills = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_skills');
      $total_cpd_hours_knowledge = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_knowledge');
      $total_cpd_hours_rules_and_regulations = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_rules_and_regulations');
      $total_cpd_hours_final = $total_cpd_hours_skills + $total_cpd_hours_knowledge + $total_cpd_hours_rules_and_regulations;
      $total_cpd_hours_gi = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date', '=', $input_year)->sum('total_cpd_hours_gi');

      $dashboard_cpd .= '<div class="col-xs-12" style="display: block;">
            <div class="table-responsive dashboard-responsive">
              <table class="table dashboard-table">
                <tr class="total-tr">
                  <td class="first-td td-hover" data-toggle="collapse" data-target="#expand-cpd-' . $input_year . '"><u>' . $input_year . '</u></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_skills, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_knowledge, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_rules_and_regulations, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_final, 2) . '</big></center></td>
                  <td class="cpd-td"><center><big>' . number_format($total_cpd_hours_gi, 2) . '</big></center></td>
                </tr>';
        
      $dashboard_cpd .= '<tr>
                <td class="expand-td" colspan="9">
                  <div class="accordian-body collapse in" id="expand-cpd-' . $input_year . '" style="margin-bottom: 0px;" aria-expanded="true">
                    <table class="expand-table">';

      $rows = CPDHours::where('user_id', Auth::user()->id)->whereYear('cpd_starting_date', '=', $input_year)->get();
      
      if (count($rows) > 0) {
        foreach ($rows as $key => $field) {
          $dashboard_cpd .= '<tr>
                              <td class="first-td"><small>' . $field->course . ' - ' . $field->cpd_period . '</small></td>
                              <td><center><big>' . number_format($field->total_cpd_hours_skills, 2) . '</big></center></td>
                              <td><center><big>' . number_format($field->total_cpd_hours_knowledge, 2) . '</big></center></td>
                              <td><center><big>' . number_format($field->total_cpd_hours_rules_and_regulations, 2) . '</big></center></td>
                              <td><center><big>' . number_format($field->total_cpd_hours_skills + $field->total_cpd_hours_knowledge + $field->total_cpd_hours_rules_and_regulations, 2) . '</big></center></td>
                              <td><center><big>' . number_format($field->total_cpd_hours_gi, 2) . '</big></center></td>
                            </tr>';
        }
      } else {
        $dashboard_cpd .= '<tr>
                            <td class="first-td"></td>
                            <td><center><big>-</big></center></td>
                            <td><center><big>-</big></center></td>
                            <td><center><big>-</big></center></td>
                            <td><center><big>-</big></center></td>
                            <td><center><big>-</big></center></td>
                          </tr>';
      }
    }

    $dashboard_cpd .= '</table>
                </div>
              </td>
            </tr>';

    $dashboard_cpd .= '</table>
                      </div>
                    </div>';

    return Response::json($dashboard_cpd);
  }

  public function doListInception() {

    $result['sort'] = Request::input('sort') ?: 'agent_name';
    $result['order'] = Request::input('order') ?: 'asc';
    $search = Request::input('search') ?: '';

    $batch_id = null; 
    $get_batch = Inception::orderBy('id', 'desc')->whereNotNull('batch_id')->first();
    $batch_date = Carbon::now()->addMonth(-1)->format('M Y');
    if ($get_batch) {
      $batch_id = $get_batch->id;
      $batch_date = Carbon::createFromFormat('Y-m-d H:i:s', $get_batch->incept_date)->format('M Y');
    }
    
    $date = Request::input('status') ? : $batch_date;
    $start = Carbon::createFromFormat('M Y', $date)->startOfMonth()->format('Y-m-d');
    $end = Carbon::createFromFormat('M Y', $date)->endOfMonth()->format('Y-m-d');

    if (Request::input('page') != '»') {
      Paginator::currentPageResolver(function () {
          return Request::input('page'); 
      });

      $rows = Inception::where(function($query) use ($search) {
                          $query->where('agent_name', 'LIKE', '%' . $search . '%')
                                ->orWhere('agent_code', 'LIKE', '%' . $search . '%')
                                ->orWhere('provider', 'LIKE', '%' . $search . '%');
                        })
                      ->where(function ($query) use ($start, $end) {
                          $query->where('incept_date', '>=', $start . ' 00:00:00')
                                ->where('incept_date', '<=', $end . ' 00:00:00');
                        })
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate(15);
    } else {
      $count = Inception::where(function($query) use ($search) {
                          $query->where('agent_name', 'LIKE', '%' . $search . '%')
                                ->orWhere('agent_code', 'LIKE', '%' . $search . '%')
                                ->orWhere('provider', 'LIKE', '%' . $search . '%');
                        })
                      ->where(function ($query) use ($start, $end) {
                          $query->where('incept_date', '>=', $start . ' 00:00:00')
                                ->where('incept_date', '<=', $end . ' 00:00:00');
                        })
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate(15);

      Paginator::currentPageResolver(function () use ($count) {
        return ceil($count->total() / 15);
      });

      $rows = Inception::where(function($query) use ($search) {
                          $query->where('agent_name', 'LIKE', '%' . $search . '%')
                                ->orWhere('agent_code', 'LIKE', '%' . $search . '%')
                                ->orWhere('provider', 'LIKE', '%' . $search . '%');
                        })
                      ->where(function ($query) use ($start, $end) {
                          $query->where('incept_date', '>=', $start . ' 00:00:00')
                                ->where('incept_date', '<=', $end . ' 00:00:00');
                        })
                      ->orderBy($result['sort'], $result['order'])
                      ->paginate(15);
    }

    $table = '';
    foreach ($rows as $key => $field) {
      $table .= '<tr class="inception-tr" data-id="' . $field->id . '">
                  <td class="first-inception-td">' . $field->agent_name . '</td>
                  <td>' . $field->agent_code . '</td>
                  <td>' . $field->provider . '</td>
                  <td>' . number_format($field->ape_mtd, 2) . ' (' . $field->ape_count_mtd . ')' . '</td>
                  <td>' . number_format($field->ape_ytd, 2) . ' (' . $field->ape_count_ytd . ')' . '</td>
                </tr>';
    }

    $get_users = User::where('usertype_id', 8)
                      ->where('id', '!=', 8)
                      ->where(function($query) {
                        $query->where('status', 1)
                              ->orWhere('status', 0);
                      })
                      ->get();

    $users = '<option value="" class="hide">Select</option>';
    foreach ($get_users as $gukey => $gufield) {
      $users .= '<option value="' . $gufield->id . '">[' . $gufield->code . '] ' . strtoupper($gufield->name) . '</option>';
    }

    $get_providers = Provider::where('status', 1)->get();

    $providers = '<option value="" class="hide">Select</option>';
    foreach ($get_providers as $gpkey => $gpfield) {
      $providers .= '<option value="' . $gpfield->id . '">' . $gpfield->name . '</option>';
    }
    $providers .= '<option value="0">Production Tagged to AA</option>';

    $this->data['rows'] = $rows->toArray();
    $this->data['table'] = $table;
    $this->data['users'] = $users;
    $this->data['date'] = $batch_date;
    $this->data['providers'] = $providers;
    $this->data['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());

    return Response::json($this->data);
  }

  public function viewInception() {

    if (Request::input('id')) {
      $row = Inception::find(Request::input('id'));

      if ($row) {
        return Response::json($row);
      }

      return Response::json(['error' => "The requested item was not found in the database."]);
    } else {
      return Response::json(['error' => "The requested item was not found in the database."]);
    }
  }

  public function saveInception() {

    $new = true;
    $input = Input::all();

    if(array_get($input, 'id')) {
      $row = Inception::find(array_get($input, 'id'));
      if(is_null($row)) {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

      $new = false;
    }

    $rules = [
      'user_id' => 'required',
      'provider_id' => 'required',
      'ape_mtd' => 'required|numeric',
      'ape_count_mtd' => 'required|numeric',
      'cat_1_mtd' => 'required|numeric',
      'cat_1_count_mtd' => 'required|numeric',
      'ape_ytd' => 'required|numeric',
      'ape_count_ytd' => 'required|numeric',
      'cat_1_ytd' => 'required|numeric',
      'cat_1_count_ytd' => 'required|numeric',
    ];

    if($new) {
      $rules['incept_date'] = 'required';
    }

    // field name overrides
    $names = [

    ];

    // do validation
    $validator = Validator::make(Input::all(), $rules);
    $validator->setAttributeNames($names);

    // return errors
    if($validator->fails()) {
      return Response::json(['error' => array_unique($validator->errors()->all())]);
    }

    // create model if new
    if($new) {
      $row = new Inception;
    }

    $user = User::find(array_get($input, 'user_id'));

    if ($user) {
      $row->agent_name = $user->name;
      $row->agent_code = $user->code;
    }

    $row->user_id = array_get($input, 'user_id');
    $row->provider_id = array_get($input, 'provider_id');

    $provider = Provider::find(array_get($input, 'provider_id'));
    if ($provider) {
      $row->provider = $provider->name;
    }

    $row->ape_mtd = array_get($input, 'ape_mtd');
    $row->ape_count_mtd = array_get($input, 'ape_count_mtd');
    $row->cat_1_mtd = array_get($input, 'cat_1_mtd');
    $row->cat_1_count_mtd = array_get($input, 'cat_1_count_mtd');
    $row->ape_ytd = array_get($input, 'ape_ytd');
    $row->ape_count_ytd = array_get($input, 'ape_count_ytd');
    $row->cat_1_ytd = array_get($input, 'cat_1_ytd');
    $row->cat_1_count_ytd = array_get($input, 'cat_1_count_ytd');

    if ($new) {
      $incept_date = Carbon::createFromFormat('M Y', array_get($input, 'incept_date'))->startOfMonth()->format('Y-m-d');
      $row->incept_date = Carbon::createFromFormat('Y-m-d H:i:s', $incept_date . ' 00:00:00');
    }

    $row->save();
    
    // return
    if ($new) {
      return Response::json(['body' => 'Inception created successfully.']);
    } else {
      return Response::json(['body' => 'Inception updated successfully.']);
    }
  }

  public function getProvider() {
    $get_provider = Provider::find(Request::input('id'));

    $provider = '';
    if ($get_provider) {
      $provider = $get_provider->name;
    }

    return Response::json($provider);
  }

  public function getUser() {
    $get_user = User::find(Request::input('id'));

    $name = '';
    $code = '';
    if ($get_user) {
      $name = $get_user->name;
      $code = $get_user->code;
    }

    $result['name'] = $name;
    $result['code'] = $code;

    return Response::json($result);
  }

  public function deleteInception() {

    $input = Request::all();

    if(array_get($input, 'id')) {
      $row = Inception::find(array_get($input, 'id'));
      if(is_null($row)) {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

      $row->delete();
      return Response::json(['body' => "Inception successfully removed."]);
    } else {
      return Response::json(['error' => "The requested item was not found in the database."]);
    }

  }
  
  public function getRanking() {
        $month_r = Request::input("date");
        $start_month = Carbon::createFromFormat('M Y', $month_r)->startOfMonth()->toDateString();
        $end_month = Carbon::createFromFormat('M Y', $month_r)->endOfMonth()->toDateString();
        $get_users = User::where('usertype_id', 8)->where('id', '!=', 8)->where('status', 1)->lists('id');
        $get_rank = Inception::select('user_id', 'users.name', 'users.preferred_name')
                              ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                              ->selectRaw('ape_ytd as total_premium')
                              ->groupBy('user_id')
                              ->orderBy('total_premium', 'desc')
                              ->whereIn('user_id', $get_users)
                              ->where('inceptions.incept_date', '>=', $start_month)
                              ->where('inceptions.incept_date', '<=', $end_month) 
                              ->get();

        $get_ranked_users = Inception::select('user_id')
                                      ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                                      ->selectRaw('ape_ytd as total_premium')
                                      ->groupBy('user_id')
                                      ->orderBy('total_premium', 'desc')
                                      ->whereIn('user_id', $get_users)
                                      ->where('inceptions.incept_date', '>=', $start_month)
                                      ->where('inceptions.incept_date', '<=', $end_month)
                                      ->lists('user_id');

        $get_not_users = User::where('usertype_id', 8)
                          ->where('id', '!=', 8)
                          ->whereNotIn('id', $get_ranked_users)
                          ->where('status', 1)
                          ->orderBy('name', 'asc')
                          ->get();

        $ranking = '<div class="col-xs-12 no-padding" style="display: block;">
                    <div class="table-responsive dashboard-responsive">
                      <table class="table dashboard-table">
                      <thead><tr><td class="first-td-ranking first-td-ranking-header">Rank</td><td class="ranking-header">Agent Name</td><td class="ranking-header">APE YTD</td></tr></thead>';
        $ctr_rank = 1;
        foreach ($get_rank as $grkey => $grfield) {
          if ($grfield->total_premium == 0) {
            $ranking .= '<tr class="ranking-span ranking-not">';
          } else {
            $ranking .= '<tr class="ranking-span">';
          }
          $ranking .= '<td class="first-td-ranking">' . $ctr_rank . '</td><td class="ranking-name">' . $grfield->name . '</td>' .
                  '<td class="ranking-ape">' . number_format($grfield->total_premium, 2) . '</td></tr>';  
          $ctr_rank++;
        }
        foreach ($get_not_users as $gnrkey => $gnrfield) {
          $ranking .= '<tr class="ranking-span ranking-not">' .
                  '<td class="first-td-ranking">' . $ctr_rank . '</td><td class="ranking-name">' . $gnrfield->name . '</td>' .
                  '<td class="ranking-ape">' . number_format(0, 2) . '</td></tr>';  
          $ctr_rank++;
        }
        $ranking .= '</table></div></div>';    
        return $ranking;
  }
  
  public function inceptionsExport(){
    $name = 'INCEPTIONS-'.str_slug(Request::input('incept_export_date'), '-');
    Excel::create($name, function($excel) use ($name){
      $export_date = Request::input('incept_export_date');
      if(Request::input('date_indicator') == 1)
      {
        $start_month = Carbon::createFromFormat('Y', $export_date)->startOfYear()->format('Y-m-d') . ' 00:00:00';
        $end_month = Carbon::createFromFormat('Y', $export_date)->endOfYear()->format('Y-m-d') . ' 23:59:59';
        // $rows = Inception::leftjoin('users', 'inceptions.user_id', '=', 'users.id')->where(function($query) use ($start_month, $end_month){
        //                   $query->where('incept_date', '>=', $start_month)
        //                         ->where('incept_date', '<=', $end_month);
        //                 })->orderBy('agent_code','asc')->orderBy('incept_date','asc')->orderBy('provider','asc')->get();

        $rows = Inception::leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                          ->select('users.status','inceptions.*')
                          ->where(function($query) use ($start_month, $end_month){
                            $query->where('inceptions.incept_date', '>=', $start_month)
                                  ->where('inceptions.incept_date', '<=', $end_month);
                          })
                          ->orderBy('inceptions.agent_code','asc')
                          ->orderBy('inceptions.incept_date','asc')
                          ->orderBy('inceptions.provider','asc')
                          ->get();

        // dd($rows);

        // $get_ranked_users = Inception::select('user_id')
        //                                 ->leftjoin('users', 'inceptions.user_id', '=', 'users.id')
        //                                 ->selectRaw('sum(ape_ytd) as total_premium')
        //                                 ->groupBy('user_id')
        //                                 ->orderBy('total_premium', 'desc')
        //                                 ->whereIn('user_id', $get_users)
        //                                 ->lists('user_id');
      }
      else
      {
        $start_month = Carbon::createFromFormat('M Y', $export_date)->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        $end_month = Carbon::createFromFormat('M Y', $export_date)->endOfMonth()->format('Y-m-d') . ' 23:59:59';
        $rows = Inception::leftjoin('users', 'inceptions.user_id', '=', 'users.id')
                          ->select('users.status','inceptions.*')
                          ->where(function($query) use ($start_month, $end_month){
                            $query->where('inceptions.incept_date', '>=', $start_month)
                                  ->where('inceptions.incept_date', '<=', $end_month);
                          })
                          ->orderBy('inceptions.agent_code','asc')
                          ->orderBy('inceptions.provider','asc')
                          ->orderBy('inceptions.incept_date','asc')
                          ->get();
        // dd($rows);
      }
      
      $excel->sheet($name, function($sheet) use ($rows) {
              $row_cell = 1;
              $sheet->appendRow([
                'AGENT CODE',
                'AGENT NAME',
                'PROVIDER',
                'APE MTD',
                'APE MTD COUNT',
                'CAT 1 MTD',
                'CAT 1 MTD COUNT',
                'APE YTD',
                'APE YTD COUNT',
                'CAT 1 YTD',
                'CAT 1 YTD COUNT',
                'INCEPTION MONTH',
                'AGENT STATUS'
              ]);

              $sheet->cell('A' . $row_cell . ':M' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('left');
                $cells->setValignment('middle');
              });

          foreach($rows as $row){
            $row_cell += 1;
            $sheet->appendRow([
                $row->agent_code,
                $row->agent_name,
                $row->provider,
                number_format($row->ape_mtd, 2),
                $row->ape_count_mtd,
                number_format($row->cat_1_mtd, 2),
                $row->cat_1_count_mtd,
                number_format($row->ape_ytd, 2),
                $row->ape_count_ytd,
                number_format($row->cat_1_ytd, 2),
                $row->cat_1_count_ytd,
                Carbon::createFromFormat('Y-m-d H:i:s', $row->incept_date)->format('M Y'),
                ($row->status == 1 ? 'Active' : 'Resigned')
              ]);
            $sheet->cell('A' . $row_cell . ':M' . $row_cell, function($cells) {
              $cells->setAlignment('left');
              $cells->setValignment('middle');
            });
          }
      });
    })->download('xls');;
//UPDATED ALKANE

  }
  
  public function XLSBreakdown($date){

    $name = 'IF-PRODUCTS-'.strtoupper(str_slug($date, '-'));
    Excel::create($name, function($excel) use ($name, $date){
      // $export_date = Request::input('incept_export_date');
      $start_month = Carbon::createFromFormat('M-Y', $date)->startOfMonth()->format('Y-m-d') . ' 00:00:00';
      $end_month = Carbon::createFromFormat('M-Y', $date)->endOfMonth()->format('Y-m-d') . ' 23:59:59';
      // dd($end_month);
      $rows = PayrollComputation::leftjoin('users', 'payroll_computations.user_id', '=', 'users.id')
                        ->leftjoin('providers', 'payroll_computations.provider_id', '=', 'providers.id')
                        ->leftjoin('policies', 'payroll_computations.policy_id', '=', 'policies.id')
                        ->leftjoin('provider_categories', 'payroll_computations.category_id', '=', 'provider_categories.id')
                        // ->leftjoin('products', 'payroll_computations.product_id', '=', 'products.id')
                        ->select('users.preferred_name as agent_name', 'users.code as code','providers.name as provider_name', 'provider_categories.name as category_name', 'payroll_computations.*')
                        ->where(function($query) use ($start_month, $end_month){
                          $query->where('payroll_computations.comp_code','!=','OF')
                                ->where('payroll_computations.comp_code','!=','UP')
                                ->where('payroll_computations.batch_date', '>=', $start_month)
                                ->where('payroll_computations.batch_date', '<=', $end_month);
                        })
                        ->get();
      // dd($rows);

      $excel->sheet($name, function($sheet) use ($rows, $date) {
              $row_cell = 1;
              $sheet->appendRow([
                'AGENT CODE',
                'TIER',
                'AGENT NAME',
                'PROVIDER',
                'POLICY NUMBER',
                'POLICY CATEGORY',
                'PRODUCT CODE',
                'BILL FREQ',
                'CONT CURR',
                'SUM INSURED',
                'PREMIUM',
                'GROSS REV',
                'APE',
                'MONTH'
              ]);

              $sheet->cell('A' . $row_cell . ':N' . $row_cell, function($cells) {
                $cells->setFont(array(
                    'bold' => true,
                ));
                $cells->setAlignment('left');
                $cells->setValignment('middle');
              });

          foreach($rows as $row){
            $row_cell += 1;
            $sheet->appendRow([
                $row->code,
                ($row->tier_rank == null ? 'N/A' : $row->tier_rank{4} ),
                $row->agent_name,
                $row->provider_name,
                $row->policy_no,
                $row->category_name,
                $row->comp_code,
                ($row->bill_freq == null ? '-' : $row->bill_freq),
                ($row->contract_currency ? $row->contract_currency : 'SGD'),
                number_format($row->sum_insured, 2),
                number_format($row->premium, 2),
                number_format($row->gross_revenue, 2),
                number_format($row->premium * $row->billing_freq_mtd * $row->conversion_rate, 2),
                Carbon::createFromFormat('M-Y', $date)->endOfMonth()->format('Ymd')
              ]);
            $sheet->cell('A' . $row_cell . ':N' . $row_cell, function($cells) {
              $cells->setAlignment('left');
              $cells->setValignment('middle');
            });
          }
      });
    })->download('xls');


  }
  

}
