<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permission;
use Auth;
use App\Sales;
use App\Notification;
use App\UserType;
use App\Settings;
use App\IPManage;
use Carbon\Carbon;
use Validator;
use Paginator;
use Input;
use Request;
use Response;
use Hash;
use App\BypassIP;
class IPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'IP Manager';
        $this->data['permission'] = Permission::where(function($query) {
        $query->where('usertype_id', '=', Auth::user()->usertype_id)
        ->where('module_id', '=', '1');
        })->first();
        $this->data['permission_payroll'] = Permission::where(function($query) {
        $query->where('usertype_id', '=', Auth::user()->usertype_id)
        ->where('module_id', '=', '2');
        })->first();
        $this->data['permission_policy'] = Permission::where(function($query) {
        $query->where('usertype_id', '=', Auth::user()->usertype_id)
        ->where('module_id', '=', '3');
        })->first();
        $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
        ->select('sales.*','designations.*')
        ->where('sales.user_id','=',Auth::user()->id)
        ->first();
        $this->data['permission_provider'] = Permission::where(function($query) {
        $query->where('usertype_id', '=', Auth::user()->usertype_id)
        ->where('module_id', '=', '4');
        })->first();
        $this->data['permission_reports'] = Permission::where(function($query) {
        $query->where('usertype_id', '=', Auth::user()->usertype_id)
        ->where('module_id', '=', '5');
        })->first();
        if ( Auth::user()->usertype_id == '8') { //if sales user
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
        ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
        ->select('users.*','notifications_per_users.*','notifications.*')
        ->where(function($query) {
        $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
        ->where('notifications.usertype_id', '=', '1')
        ->where('notifications_per_users.is_read', '=', '0');                             
        })->get();

        }
        else
        {
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
        ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
        ->select('users.*','notifications_per_users.*','notifications.*')
        ->where(function($query) {
        $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
        ->where('notifications.usertype_id', '=', '2')
        ->where('notifications_per_users.is_read', '=', '0');       
        })->get();
        }  
        $this->data['noti_count'] = count($this->data['notification']);
        $this->data['settings'] = Settings::first();
        $this->data['ips'] = IPManage::whereIn('status', [1,2])->get();

        $result = $this->doList();      
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        return view('ipblock.list', $this->data);
    }

    public function IPAction($action, $ip, $expiration = null){
        if($ip)
        {
            $ip_bool = (filter_var($ip, FILTER_VALIDATE_IP) !== false);
            if($ip_bool)
            {
                $row = IPManage::where('ip_address', $ip)->first();
                if(!is_null($row) && $row->status == 1 && $action == 'whitelist')
                {
                    if($expiration)
                    {
                        $row->expiration_hours = $expiration;
                        $row->expiration_date = Carbon::now()->addHours($expiration);
                        $row->save();
                    }
                    return 'IP already whitelisted.'.($expiration ? ' Added expiration of '.$expiration.' hour/s.' : 'Expiration: NONE');
                }
                else if(!is_null($row) && $row->status == 2 && $action == 'whitelist')
                {
                    $row->status = 1;
                    if($expiration)
                    {
                        $row->expiration_hours = $expiration;
                        $row->expiration_date = Carbon::now()->addHours($expiration);
                    }
                    else
                    {
                        $row->expiration_hours = NULL;
                        $row->expiration_date = NULL;
                    }
                    
                    $row->description = 'Whitelisted using quick method.';
                    $row->save();
                    return $ip.' has been whitelisted. Expiration: '.($expiration ? $expiration.' hour/s' : 'NONE');
                }
                else if(is_null($row) && $action == 'whitelist')
                {
                    $new = new IPManage;
                    $new->ip_address = $ip;
                    $new->status = 1;
                    if($expiration)
                    {
                        $new->expiration_hours = NULL;
                        $new->expiration_date = NULL;
                    }
                    else
                    {
                        $new->expiration_hours = NULL;
                        $new->expiration_date = NULL;
                    }
                    
                    $new->description = 'Whitelisted using quick method.';
                    $new->save();
                    return $ip.' has been whitelisted. Expiration: '.($expiration ? $expiration.' hour/s' : 'NONE');
                }
                else if(!is_null($row) && $row->status == 2 && $action == 'blacklist')
                {
                    return 'IP already blacklisted.';
                }
                else if(!is_null($row) && $row->status == 1 && $action == 'blacklist')
                {
                    $row->status = 2;
                    $row->expiration_hours = NULL;
                    $row->expiration_date = NULL;
                    $row->description = 'Blacklisted using quick method.';
                    $row->save();
                    return $ip.' has been blacklisted.';
                }
                else if(is_null($row) && $action == 'blacklist')
                {
                    return 'IP: '.$ip.' is not in the whitelist, this is blacklisted automatically.';
                }
                else if(!is_null($row) && $action == 'delete')
                {
                    $row->delete();
                    return $ip.' has been deleted.';
                }
                else
                {
                    return 'I DONT KNOW YOU';
                }
            }
            else
            {
                return 'Invalid IP';
            }
        }
    }

    public function BypassIPRestriction($hash){
        $new_hash = BypassIP::pluck('hash');
        if(Hash::check($hash, $new_hash))
        {
            $ip = request()->ip();
            $row = IPManage::where('ip_address', $ip)->first();
            if($row)
            {
                if($row->status == 1)
                {
                    return redirect()->to('login');
                }
                else
                {
                    $row->status = 1;
                    $row->expiration_hours = 1;
                    $row->expiration_date = $row->expiration_date = Carbon::now()->addHour();
                    $row->save();
                    return redirect()->to('login');
                }
            }
            else
            {
                $new = new IPManage;
                $new->ip_address = $ip;
                $new->status = 1;
                $new->description = 'Added using dev-bypass. Expiration date: GMT +8 '.Carbon::now('Asia/Manila')->addHour();
                $new->expiration_hours = 1;
                $new->expiration_date = Carbon::now()->addHour();
                $new->save();
                return redirect()->to('login');
            }
        }
        else
        {
            return 'Key: '.$hash.' is incorrect.';
        }
    }

    public function getIPInfo(){
        $id = request()->id;
        $response = IPManage::where('id', $id)->first();
        if($response)
        {
            $response['code'] = 1;
        }
        else
        {
            $response['code'] = 0;
        }
        return $response;
    }

    public function save(){
        $messages = [
                        'ip_address.required' => 'ip-address|IP address is required.',
                        'ip_address.ip' => 'ip-address|Must be a valid IP address.',
                        'ip_address.unique' => 'ip-address|The IP address has already been taken.',
                        'expiration_date.numeric' => 'expiration-hours|Must be a number.'
                    ];
        $validator = Validator::make(request()->all(), [
            'ip_address' => 'required|ip|unique:ip_list,ip_address,except,'.request()->id,
            'expiration_date' => 'numeric'
        ], $messages);

        if($validator->fails())
        {
            $response['code'] = 0;
            $response['errors'] = $validator->errors()->all();
            return $response;
        }
        else
        {
            $id = request()->row_id;
            $indicator = request()->indicator;
            $ip_address = request()->ip_address;
            $description = request()->description;
            $expiration_date = Carbon::now()->addHours(request()->expiration_date);
            $expiration_hours = request()->expiration_date;
            if($indicator == 1)
            {
                $row = new IPManage;
                $row->status = 1;
                $response['code'] = 1;
                $response['message'] = 'IP successfully added.';
            }
            else if($indicator == 2)
            {
                $row = IPManage::where('id', $id)->first();
                $row->status = 1;
                $response['code'] = 1;
                $response['message'] = 'IP successfully updated.';
            }
            else
            {
                $response['code'] = 0;
                $response['message'] = 'Failed';
                return $response;
            }
            $row->ip_address = $ip_address;
            $row->description = $description;
            if($expiration_hours)
            {
                $row->expiration_date = $expiration_date;
                $row->expiration_hours = $expiration_hours;
            }
            else
            {
                $row->expiration_date = NULL;
                $row->expiration_hours = NULL;
            }
            $row->save();
            return $response;
        }
    }

    public function delete(){
        $id = request()->id;
        $row = IPManage::where('id', $id)->first();
        $row->delete();
        $response['body'] = 'IP successfully deleted.';
        return $response;
    }

    public function toggle(){
        $id = request()->id;
        $row = IPManage::where('id', $id)->first();
        // $row->status = $row->status == 1 ? 2 : 1;
        if($row->status == 1)
        {
            $row->status = 2;
            $response['body'] = 'IP successfully blacklisted.';
        }
        else
        {
            $row->status = 1;
            $response['body'] = 'IP successfully whitelisted.';
        }
        $row->save();
        return $response;
    }

    public function doList() {
        // dd(Request::all());
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'asc';
        $per = Request::input('per') ? : 10;
         if (Request::input('page') != '»') {
            Paginator::currentPageResolver(function () {
                return Request::input('page'); 
            });

            $rows = IPManage::whereIn('status', [1,2])
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);       
          } else {
            $count = IPManage::whereIn('status', [1,2])
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
            Paginator::currentPageResolver(function () use ($count, $per) {
                return ceil($count->total() / $per);
            });
            $rows = IPManage::whereIn('status', [1,2])
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

          }
        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            // dd($result);
            return $result;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
