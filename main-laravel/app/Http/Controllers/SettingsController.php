<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Settings;
use App\SettingsRate;
use App\SettingsCPD;
use App\Sales;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use DB;
use Response;
use Auth;
use App\Permission;
use App\GI;
use Redirect;
use App\Notification;
use App\NotificationPerUser;
use App\SettingsTiers;
use App\DesignationRates;
use App\PayrollRates;

class SettingsController extends Controller
{
    
    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }
    
    /**
     * Display a listing of the reports.
     *
     * @return Response
     */
    public function index() {

      $result = $this->doList();
      $result = $this->dolistTier();
      $this->data['default'] = Settings::first();
      $this->data['rate'] = SettingsRate::all();
      $this->data['tiers'] = SettingsTiers::all();
      $this->data['cpd'] = SettingsCPD::all();

      $this->data['gis'] = GI::all();

      $count_all = SettingsRate::count();
      $count = SettingsRate::where('status', 2)->count();

      $this->data['select_all'] = ($count_all == $count ? 'checked="checked' : '');

      //dd($this->data['rate']);
      $this->data['title'] = "Settings";
      $this->data['refresh_route'] = url("settings/refresh");
      $this->data['refresh_route_tier'] = url("settings/refresh/tier");
      $this->data['refresh_route_rate'] = url("settings/refreshRate");
      
      $this->data['permission'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '1');
                            })->first();
      $this->data['permission_payroll'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '2');
                            })->first();
      $this->data['permission_policy'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '3');
                            })->first();
      $this->data['permission_rank'] = Sales::join('designations','designations.id','=','sales.designation_id')
                                              ->select('sales.*','designations.*')
                                              ->where('sales.user_id','=',Auth::user()->id)
                                              ->first();
      $this->data['permission_provider'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '4');
                            })->first();
      $this->data['permission_reports'] = Permission::where(function($query) {
                                  $query->where('usertype_id', '=', Auth::user()->usertype_id)
                                        ->where('module_id', '=', '5');
                            })->first();

      // for notification
      if ( Auth::user()->usertype_id == '8') { //if sales user
        $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                      ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                        ->select('users.*','notifications_per_users.*','notifications.*')
                                      ->where(function($query) {
                                            $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                                  ->where('notifications.usertype_id', '=', '1')
                                                  ->where('notifications_per_users.is_read', '=', '0');      
                                        })->get();
      }
      else {
       $this->data['notification'] = Notification::join('users', 'notifications.user_id', '=', 'users.id')
                                ->join('notifications_per_users', 'notifications_per_users.notification_id', '=', 'notifications.id')
                                  ->select('users.*','notifications_per_users.*','notifications.*')
                                ->where(function($query) {
                                      $query->where('notifications_per_users.user_id', '=', Auth::user()->id)
                                            ->where('notifications.usertype_id', '=', '2')
                                            ->where('notifications_per_users.is_read', '=', '0'); 
                                  })->get();
      }   
      $this->data['settings'] = Settings::first();
      $this->data['designation_rates'] = DesignationRates::all();
      $this->data['payroll_rates'] = PayrollRates::all();
      $this->data['noti_count'] = count($this->data['notification']);
      return view('settings.list', $this->data);
    }

    public function doList() {
      $rows = Settings::first();

      $result['rows'] = $rows;
      return $result;
    }

    public function doListRate(){
      $rows = SettingsRate::first();

      $result['rows'] = $rows;
      return $result;
    }
    public function dolistTier(){
      $rows = SettingsTiers::all();

      $result['rows'] = $rows;
      return $result;

    }
    public function doListDesignationRates(){
      $rows = DesignationRates::all();

      $result['rows'] = $rows;
      return $result;

    }
    public function doListPayrollRates(){
      $rows = PayrollRates::all();

      $result['rows'] = $rows;
      return $result;

    }
   public function doListCPD(){
      $rows = SettingsCPD::all();

      $result['rows'] = $rows;
      return $result;

    }
    
    public function saveGI() {
      $input = Input::all();

        $ids = [];
        $legacy_fa = Input::get('legacy_fa');
        $mgr = Input::get('mgr');
        $fsd = Input::get('fsd');
        $agents = Input::get('agents');
        $gi_id = Input::get('gi_id');
        foreach ($gi_id as $key => $value) {
          $row = GI::find($key);
  
          if ($row) {
            $row->legacy_fa = $legacy_fa[$key];
            $row->mgr = $mgr[$key];
            $row->fsd = $fsd[$key];
            $row->agents = $agents[$key];
            $row->save();
          }
          $ids[$row->id] = $row->id;
        }

       return Response::json(['body' => 'Changes have been saved.']);
    }

    public function saveCPD(){
       $input = Input::all();


          $rules = [

          ];

          // field name overrides
          $names = [

          ];
        $cpd_array = array();
        $cpd_value = Input::get('cpd_value');
        $cpd_id = Input::get('cpd_id');


        if($cpd_id) {
            foreach($cpd_id as $key => $val) {
                if($cpd_id[$key]) {
                    $subrules = array(
                        "cpd_value.$key" => 'required|numeric',

                    );

                    $subfn = array(
                        
                    );

                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $cpd_array[] = $cpd_id[$key];
 
                } else {
                    unset($cpd_id[$key]);
                }
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

          if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        $ids = [];
        foreach ($cpd_id as $key => $value) {
          $row = SettingsCPD::find($key);
  
          if ($row) {
            $row->no_of_hours = $cpd_value[$key];
            $row->save();
          }
          $ids[$row->id] = $row->id;
        }

       return Response::json(['body' => 'Changes have been saved.']);
    }
    public function saveTier(){

        $input = Input::all();


          $rules = [

          ];

          // field name overrides
          $names = [

          ];
        $tier_array = array();
        $tier_value = Input::get('tier_value');
        $tier_id = Input::get('tier_id');
        $tier_code = Input::get('tier_code');


        if($tier_id) {
            foreach($tier_id as $key => $val) {
                if($tier_id[$key]) {
                    $subrules = array(
                        "tier_value.$key" => 'required|numeric',

                    );

                    $subfn = array(
                        "tier_value.$key" => $tier_code[$key],
                    );

                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $tier_array[] = $tier_id[$key];
 
                } else {
                    unset($tier_id[$key]);
                }
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

          if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        $ids = [];
        foreach ($tier_id as $key => $value) {
          $row = SettingsTiers::find($key);
  
          if ($row) {
            $row->rate = $tier_value[$key];
            $row->save();
          }
          $ids[$row->id] = $row->id;
        }

       return Response::json(['body' => 'Changes have been saved.']);
    }
    public function save(){

        $input = Input::all();
        // dd($input);
        $rules = [
            // 'partner' => 'required|numeric',
            // 'FSD' => 'required|numeric|max:40',
            // 'AFSD' => 'required|numeric',
            // 'FSM' => 'required|numeric',
            // 'FSC' => 'required|numeric',
            // 'EFSC' => 'required|numeric',
            // 'PFSC' => 'required|numeric',
            // 'SFSC' => 'required|numeric',
            // 'FSC_Elite' => 'required|numeric',
            'GST' => 'required|numeric',
        ];

        // field name overrides
        $names = [
            // 'partner' => 'Partner',
            // 'FSD' => 'Financial Services Director',
            // 'AFSD' => 'Assistant Financial Services Director',
            // 'FSM' => 'Financial Services Manager',
            // 'FSC' => 'Financial Services Consultant',
            // 'EFSC' => 'Executive Financial Services Consultant',
            // 'PFSC' => 'Premier Financial Services Consultant',
            // 'SFSC' => 'Senior Financial Services Consultant',
            // 'FSC_Elite' => 'Financial Services Consultant-Elite',
            'GST' => 'GST',

        ];

        $designation_rate_array = array();
        $designation_value = Input::get('designation_value');
        $designation_id = Input::get('designation_id');

        if($designation_id) {
            foreach($designation_id as $key => $val) {
                if($designation_id[$key]) {
                    $subrules = array(
                        "designation_value.$key" => 'required|numeric',
                    );
                    $subfn = array(
                        "designation_value.$key" => 'Designation Rate',
                    );
                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $designation_rate_array[] = $designation_id[$key];
 
                } else {
                    unset($designation_id[$key]);
                }
            }
        }
        $payroll_rate_array = array();
        $payroll_rate_value = Input::get('payroll_rate_value');
        $payroll_rate_id = Input::get('payroll_rate_id');

        if($payroll_rate_id) {
            foreach($payroll_rate_id as $key => $val) {
                if($payroll_rate_id[$key]) {
                    $subrules = array(
                        "payroll_rate_value.$key" => 'required|numeric',
                    );
                    $subfn = array(
                        "payroll_rate_value.$key" => 'Payroll Rate',
                    );
                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $payroll_rate_array[] = $payroll_rate_id[$key];
 
                } else {
                    unset($payroll_rate_id[$key]);
                }
            }
        }
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        
       
          $row = Settings::all()->first();
          // $row->partner      = array_get($input, 'partner');
          // $row->FSD      =  array_get($input, 'FSD');
          // $row->AFSD     = array_get($input, 'AFSD');
          // $row->FSM    = array_get($input, 'FSM');
          // $row->FSC  = array_get($input, 'FSC');
          // $row->EFSC  = array_get($input, 'EFSC');
          // $row->PFSC  = array_get($input, 'PFSC');
          // $row->SFSC  = array_get($input, 'SFSC');
          // $row->FSC_Elite  = array_get($input, 'FSC_Elite');
          $row->GST  = array_get($input, 'GST');
      
          $row->save();


          $ids = [];
        foreach ($designation_id as $key => $value) {
          $row = DesignationRates::find($key);
  
          if ($row) {
            $row->rate = $designation_value[$key];
            $row->save();
          }
          $ids[$row->id] = $row->id;
        }

        foreach ($payroll_rate_id as $key => $value) {
          $row = PayrollRates::find($key);
  
          if ($row) {
            $row->rate = $payroll_rate_value[$key];
            $row->save();
          }
          $ids[$row->id] = $row->id;
        }
         return Response::json(['body' => 'Changes have been saved.']);
        }

        public function currencysave(){

        $input = Input::all();

        // foreach (array_get($input, 'rate_value') as $key => $value) {

        //   $rules = [
        //       'rate_value.$key' => 'required|numeric',
        //   ];

        //   // field name overrides
        //   $names = [
        //       'rate_value.$key' => 'rate value',
        //   ];
        // }

        // // dd(array_get($input, 'rate_value'));
        

          $rules = [

          ];

          // field name overrides
          $names = [

          ];

        $rate_array = array();
        $rate_value = Input::get('rate_value');
        $rate_id = Input::get('rate_id');
        $rate_status = Input::get('status');
        $rate_code = Input::get('rate_code');
      // dd($rate_status);
        //dd($rate_id);
        // validate child rows
        if($rate_id) {
            foreach($rate_id as $key => $val) {
                if($rate_id[$key]) {
                    $subrules = array(
                        "rate_value.$key" => 'required|numeric',

                    );

                    $subfn = array(
                        "rate_value.$key" => $rate_code[$key],
                    );

                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $rate_array[] = $rate_id[$key];
 
                } else {
                    unset($rate_id[$key]);
                    unset($rate_status[$key]);
                }
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);
        
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $ids = [];
        foreach ($rate_id as $key => $value) {
          $row = SettingsRate::find($key);
          if ($row) {
            $row->rate = $rate_value[$key];
            $row->save();
          }
          $ids[$row->id] = $row->id;
        }

        if ($rate_status) {
          foreach ($rate_status as $key => $value) {
            $row = SettingsRate::find($key);
            if ($row) {
              $row->status = $rate_status[$key];
              $row->save();
            }
          }
        }

      foreach ($ids as $key => $value) {
        $check = false;
        if ($rate_status) {
          foreach ($rate_status as $nkey => $nvalue) {
            if ($value == $nkey) {
              $check = true;
            }
          }
        }
        if (!$check) {
          $row = SettingsRate::find($value);
          $row->status = 1;
          $row->save();
        }
      }

        //  // save visa rows
        // if($rate_array) {
         
        //     foreach($rate_array as $key => $child) {
        //         if($rate_id[$key]) {
        //             // this is an existing visa
        //             $cinfo = SettingsRate::find($rate_id[$key]);
        //             if($rate_status[$key] != null){
        //                $cinfo->status = $rate_status[$key]; 
        //             }
        //             $cinfo->rate = $rate_value[$key];
                   
        //             $cinfo->save();
        //       }
                

        //     }
        // }

         return Response::json(['body' => 'Changes have been saved.']);
        }

    public function notification($id) {  
     $row = NotificationPerUser::where(function($query) use ($id) {
                                  $query->where('user_id', '=', Auth::user()->id)
                                        ->where('notification_id', '=', $id);
                                })->update(['is_read' => 1]);

    return Redirect::to('/policy/production/case-submission');
   }
  
    public function addGIrank($rank, $lfa, $mgr, $fsd, $agent) {
      $check = GI::where('rank', $rank)->first();
      if(!$check)
      {
        if(Auth::check() && Auth::user()->usertype_id != 8)
        {
          $new = new GI;
          $new->rank = $rank;
          $new->legacy_fa = $lfa;
          $new->mgr = $mgr;
          $new->fsd = $fsd;
          $new->agents = $agent;
          $saved = $new->save();
          if($saved === true)
          {
            return 'SAVED';
          }
          else
          {
            return 'FAILED';
          }
        }
        else
        {
          return 'FAILED';
        }
      }
      else
      {
        return 'RANK ALREADY EXISTS';
      }
    }
          
}
