<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'DashboardController@index');

Route::get('login',	'AuthController@login');
Route::post('login', 'AuthController@doLogin');
Route::get('logout', 'AuthController@logout');

Route::get('ceo/login',	'AuthController@CEOlogin');
Route::post('ceo/login', 'AuthController@CEOdoLogin');

Route::get('updation', 'UpdatingController@updation');
Route::post('inception/export', 'DashboardController@inceptionsExport');
Route::post('wipe', 'DashboardController@wipe');
Route::post('get/ranking', 'DashboardController@getRanking');
Route::group(array('before' => 'ajax'), function() {

	Route::post('case/refresh', 'DashboardController@getCase');
	Route::post('inception/get-inception', 'DashboardController@getInception');
	Route::post('inception/update', 'DashboardController@updateInception');
	
	Route::post('inception/month', 'DashboardController@monthInception');
	Route::post('case/month', 'DashboardController@monthCase');

	Route::post('submission/update', 'DashboardController@updateCase');
	Route::post('replicate/refresh', 'DashboardController@getReplicate');
	Route::post('cpd/refresh', 'DashboardController@getCPD');

	Route::post('inception/refresh', 'DashboardController@doListInception');
	Route::post('inception/view', 'DashboardController@viewInception');
	Route::post('inception/save', 'DashboardController@saveInception');
	Route::post('inception/delete', 'DashboardController@deleteInception');
	Route::post('inception/get-provider', 'DashboardController@getProvider');
	Route::post('inception/get-user', 'DashboardController@getUser');

	Route::post('mtd-ape/refresh', 'DashboardController@getMTDAPE');
	Route::post('ytd-ape/refresh', 'DashboardController@getYTDAPE');
	Route::post('mtd-ape-aviva/refresh', 'DashboardController@getMTDAvivaAPE');
	Route::post('ytd-ape-aviva/refresh', 'DashboardController@getYTDAvivaAPE');

	Route::post('mtd-ape-case/refresh', 'DashboardController@getMTDAPECase');
	Route::post('ytd-ape-case/refresh', 'DashboardController@getYTDAPECase');
	Route::post('mtd-ape-aviva-case/refresh', 'DashboardController@getMTDAPEAvivaCase');
	Route::post('ytd-ape-aviva-case/refresh', 'DashboardController@getYTDAPEAvivaCase');

	Route::post('mtd-ape-inception/refresh', 'DashboardController@getMTDAPEInception');
	Route::post('ytd-ape-inception/refresh', 'DashboardController@getYTDAPEInception');
	Route::post('mtd-ape-aviva-inception/refresh', 'DashboardController@getMTDAPEAvivaInception');
	Route::post('ytd-ape-aviva-inception/refresh', 'DashboardController@getYTDAPEAvivaInception');

	Route::post('mtd-ape-gross/refresh', 'DashboardController@getMTDAPEGross');
	Route::post('ytd-ape-gross/refresh', 'DashboardController@getYTDAPEGross');
	Route::post('mtd-ape-aviva-gross/refresh', 'DashboardController@getMTDAPEAvivaGross');
	Route::post('ytd-ape-aviva-gross/refresh', 'DashboardController@getYTDAPEAvivaGross');


	Route::post('sup-mtd-ape-case/refresh', 'DashboardController@getSupMTDAPECase');
	Route::post('sup-ytd-ape-case/refresh', 'DashboardController@getSupYTDAPECase');
	Route::post('sup-mtd-ape-aviva-case/refresh', 'DashboardController@getSupMTDAPEAvivaCase');
	Route::post('sup-ytd-ape-aviva-case/refresh', 'DashboardController@getSupYTDAPEAvivaCase');

	Route::post('sup-mtd-ape-inception/refresh', 'DashboardController@getSupMTDAPEInception');
	Route::post('sup-ytd-ape-inception/refresh', 'DashboardController@getSupYTDAPEInception');
	Route::post('sup-mtd-ape-aviva-inception/refresh', 'DashboardController@getSupMTDAPEAvivaInception');
	Route::post('sup-ytd-ape-aviva-inception/refresh', 'DashboardController@getSupYTDAPEAvivaInception');

	Route::post('sup-mtd-ape-gross/refresh', 'DashboardController@getSupMTDAPEGross');
	Route::post('sup-ytd-ape-gross/refresh', 'DashboardController@getSupYTDAPEGross');
	Route::post('sup-mtd-ape-aviva-gross/refresh', 'DashboardController@getSupMTDAPEAvivaGross');
	Route::post('sup-ytd-ape-aviva-gross/refresh', 'DashboardController@getSupYTDAPEAvivaGross');


	Route::post('provider-count/all/refresh', 'DashboardController@doListProviderAll');
	Route::post('provider-count/today/refresh', 'DashboardController@doListProviderToday');
	Route::post('provider-count/week/refresh', 'DashboardController@doListProviderWeek');
	Route::post('provider-count/month/refresh', 'DashboardController@doListProviderMonth');
	Route::post('production-count/all/refresh', 'DashboardController@doListProductionAll');
	Route::post('production-count/today/refresh', 'DashboardController@doListProductionToday');
	Route::post('production-count/week/refresh', 'DashboardController@doListProductionWeek');
	Route::post('production-count/month/refresh', 'DashboardController@doListProductionMonth');
	Route::post('agent-count/all/refresh', 'DashboardController@doListAgentAll');
	Route::post('agent-count/today/refresh', 'DashboardController@doListAgentToday');
	Route::post('agent-count/week/refresh', 'DashboardController@doListAgentWeek');
	Route::post('agent-count/month/refresh', 'DashboardController@doListAgentMonth');
	Route::post('policy-count/all/refresh', 'DashboardController@doListPolicyAll');
	Route::post('policy-count/today/refresh', 'DashboardController@doListPolicyToday');
	Route::post('policy-count/week/refresh', 'DashboardController@doListPolicyWeek');
	Route::post('policy-count/month/refresh', 'DashboardController@doListPolicyMonth');
	Route::post('orphan-count/all/refresh', 'DashboardController@doListOrphanAll');
	Route::post('orphan-count/today/refresh', 'DashboardController@doListOrphanToday');
	Route::post('orphan-count/week/refresh', 'DashboardController@doListOrphanWeek');
	Route::post('orphan-count/month/refresh', 'DashboardController@doListOrphanMonth');
	Route::post('first-count/all/refresh', 'DashboardController@doListFirstAll');
	Route::post('first-count/today/refresh', 'DashboardController@doListFirstToday');
	Route::post('first-count/week/refresh', 'DashboardController@doListFirstWeek');
	Route::post('first-count/month/refresh', 'DashboardController@doListFirstMonth');
	Route::post('force-count/all/refresh', 'DashboardController@doListForceAll');
	Route::post('force-count/today/refresh', 'DashboardController@doListForceToday');
	Route::post('force-count/week/refresh', 'DashboardController@doListForceWeek');
	Route::post('force-count/month/refresh', 'DashboardController@doListForceMonth');
	Route::post('lapse-count/all/refresh', 'DashboardController@doListLapseAll');
	Route::post('lapse-count/today/refresh', 'DashboardController@doListLapseToday');
	Route::post('lapse-count/week/refresh', 'DashboardController@doListLapseWeek');
	Route::post('lapse-count/month/refresh', 'DashboardController@doListLapseMonth');
	Route::post('cat-count/category', 'DashboardController@doListCat');
	Route::post('cat-count/all/refresh', 'DashboardController@doListCatAll');
	Route::post('cat-count/today/refresh', 'DashboardController@doListCatToday');
	Route::post('cat-count/week/refresh', 'DashboardController@doListCatWeek');
	Route::post('cat-count/month/refresh', 'DashboardController@doListCatMonth');
});

// Profile
Route::group(array('before' => 'ajax'), function() {
	Route::post('profile/info', 'ProfileController@info');
	Route::post('profile/save', 'ProfileController@save');
	Route::post('profile/refresh', 'ProfileController@doInfo');
});

//System Users
Route::get('user-management/system-users', 'SystemController@index');
Route::get('users/ceo', 'SystemController@indexCEO');
Route::get('users/it-users', 'SystemController@indexIT');
Route::get('users/admin-accounts', 'SystemController@indexAdmin');
Route::get('users/accounting', 'SystemController@indexAccounting');
Route::get('users/compliance', 'SystemController@indexCompliance');
Route::get('users/sales-assistant', 'SystemController@indexSales');
Route::get('user-management/permissions', 'SystemController@indexPermission');
Route::get('user-management/system-user-log', 'SystemController@indexLog');
Route::post('users/logs', 'SystemController@indexLog');
Route::post('users/logs/export-check', 'SystemController@exportCheck');
Route::post('users/logs/export', 'SystemController@export');
Route::post('users/logs/export-all', 'SystemController@exportAll');

Route::group(array('before' => 'ajax'), function() {
	Route::post('users/save', 'SystemController@save');
	Route::post('users/view', 'SystemController@view');
	Route::post('users/enable', 'SystemController@enable');
	Route::post('users/disable', 'SystemController@disable');
	Route::post('users/disable-select', 'SystemController@disableSelected');
	Route::post('users/remove-select', 'SystemController@removeSelected');
	Route::post('users/refresh', 'SystemController@doList');
	Route::post('users/refresh/ceo', 'SystemController@doListCEO');
	Route::post('users/refresh/it', 'SystemController@doListIT');
	Route::post('users/refresh/admin', 'SystemController@doListAdmin');
	Route::post('users/refresh/accounting', 'SystemController@doListAccounting');
	Route::post('users/refresh/compliance', 'SystemController@doListCompliance');
	Route::post('users/refresh/sales-assistant', 'SystemController@doListSales');
	Route::post('users/refresh/permissions', 'SystemController@doListPermission');
	Route::post('users/permissions/save', 'SystemController@savePermission');
	Route::post('sales-assistant/get-available-agents', 'SystemController@getAvailableAgents');
	Route::post('sales-assistant/save-agents', 'SystemController@saveAgents');
	Route::post('sales-assistant/get-agents', 'SystemController@getAgents');
	Route::post('sales-assistant/remove-agents', 'SystemController@removeAgents');
	Route::post('sales-assistant/remove-agent', 'SystemController@removeAgent');
	Route::post('users/logs/refresh', 'SystemController@doListLog');
});

//Sales Users
Route::get('user-management/sales-users', 'SalesController@index');

// Route::get('sales/view', 'SalesController@view');
// Route::get('sales/sales-agent', 'SalesController@indexAgent');
Route::get('user-management/sales-users/fsc-elite', 'SalesController@indexFSCElite');
Route::get('user-management/sales-users/efsc', 'SalesController@indexEFSC');
Route::get('user-management/sales-users/pfsc', 'SalesController@indexPFSC');
Route::get('user-management/sales-users/sfsc', 'SalesController@indexSFSC');
Route::get('user-management/sales-users/fsc', 'SalesController@indexFSC');
Route::get('user-management/sales-users/fsm', 'SalesController@indexFSM');
Route::get('user-management/sales-users/afsd', 'SalesController@indexAFSD');
Route::get('user-management/sales-users/fsd', 'SalesController@indexFSD');
Route::get('user-management/sales-users/partner', 'SalesController@indexPartner');
// Route::get('sales/assistant-manager', 'SalesController@indexSubManager');
// Route::get('sales/manager', 'SalesController@indexManager');
// Route::get('sales/assistant-director', 'SalesController@indexSubDirector');
// Route::get('sales/director', 'SalesController@indexDirector');
Route::get('user-management/team-management', 'SalesController@indexTeamManagement');
Route::get('summary-and-reports/cpd-management', 'SalesController@indexCPDManagement');
Route::get('sales/team-management/incomplete', 'SalesController@indexIncomplete');
Route::get('sales/team-management/duplicate', 'SalesController@indexDuplicate');
Route::get('user-management/sales-user-log', 'SalesController@indexLog');
Route::get('user-management/team-management-log', 'SalesController@indexTeamLog');
Route::get('user-management/sales-users/{id}/edit', 'SalesController@edit');

Route::post('sales/logs/export-all', 'SalesController@exportAll');

Route::post('sales/export', 'SalesController@export');

Route::group(array('before' => 'ajax'), function() {
	Route::post('sales/get-cpds', 'SalesController@getCPDs');
	Route::post('sales/cpd/save', 'SalesController@saveCPD');
	Route::post('sales/cpd/refresh', 'SalesController@doListCPDManagement');
	Route::post('sales/cpd/view', 'SalesController@viewCPD');
	Route::post('sales/cpd/delete', 'SalesController@deleteCPD');
	Route::post('sales/cpd/remove', 'SalesController@deleteMultipleCPD');
	Route::post('sales/get-supervisor', 'SalesController@getSupervisor');
	Route::post('sales/get-sales-supervisor', 'SalesController@getSalesSupervisor');
	Route::post('sales/get-tiers', 'SalesController@getTierRate');
	Route::post('sales/get-designation_rate', 'SalesController@getDesignationRate');
	Route::post('sales/save', 'SalesController@save');
	Route::post('sales/designate', 'SalesController@designate');
	Route::post('sales/bond', 'SalesController@bond');
	Route::post('sales/view', 'SalesController@view');
	Route::post('sales/team','SalesController@team');//save team
	Route::post('sales/edit/banding','SalesController@editBanding'); //edit banding
	Route::post('sales/edit-banding/save','SalesController@saveBanding'); //edit banding
	Route::post('sales/save-sales', 'SalesController@saveSales');
	Route::post('sales/{id}/get-info', 'SalesController@doEdit');
	Route::post('sales/get-info', 'SalesController@doEditUser');
	Route::post('sales/get-groups', 'SalesController@getGroup');
	Route::post('sales/get-units', 'SalesController@getUnit');
	Route::post('sales/enable', 'SalesController@enable');
	Route::post('sales/remove', 'SalesController@remove');
	Route::post('sales/disable', 'SalesController@disabled');
	Route::post('sales/disable-select', 'SalesController@disableSelected');
	Route::post('sales/remove-select', 'SalesController@removeSelected');
	Route::post('sales/refresh', 'SalesController@doList');
	Route::post('sales/policiesView', 'SalesController@doListView');
	// Route::post('sales/refresh/sales-agent', 'SalesController@doListAgent');
	// Route::post('sales/refresh/assistant-manager', 'SalesController@doListSubManager');
	// Route::post('sales/refresh/manager', 'SalesController@doListManager');
	// Route::post('sales/refresh/assistant-director', 'SalesController@doListSubDirector');
	// Route::post('sales/refresh/director', 'SalesController@doListDirector');
	Route::post('sales/refresh/fsc-elite', 'SalesController@doListFSCElite');
	Route::post('sales/refresh/efsc', 'SalesController@doListEFSC');
	Route::post('sales/refresh/pfsc', 'SalesController@doListPFSC');
	Route::post('sales/refresh/sfsc', 'SalesController@doListSFSC');
	Route::post('sales/refresh/fsc', 'SalesController@doListFSC');
	Route::post('sales/refresh/fsm', 'SalesController@doListFSM');
	Route::post('sales/refresh/afsd', 'SalesController@doListAFSD');
	Route::post('sales/refresh/fsd', 'SalesController@doListFSD');
	Route::post('sales/refresh/partner', 'SalesController@doListPartner');
	Route::post('sales/team-management/get-supervisors', 'SalesController@getGroupSupervisor');
	Route::post('sales/team-management/get-advisors', 'SalesController@getGroupAdvisor');
	Route::post('sales/team-management/view', 'SalesController@groupView');
	Route::post('sales/team-management/get-owner', 'SalesController@getOwner');
	Route::post('sales/team-management/get-owner-supervisor', 'SalesController@getOwnerSupervisor');
	Route::post('sales/team-management/supervisor/check-if-supervisor', 'SalesController@getSupervisorAdvisor');
	Route::post('sales/team-management/supervisor/get-code', 'SalesController@getSupervisorCode');
	Route::post('sales/team-management/get-group', 'SalesController@getGroupDelete');
	Route::post('sales/team-management/supervisor/get-relocate', 'SalesController@getRelocateCode');
	Route::post('sales/team-management/advisor/get-relocate', 'SalesController@getRelocateCodeAdvisor');
	Route::post('sales/team-management/advisor/get-relocate-code', 'SalesController@getRelocationCodeAdvisor');
	Route::post('sales/team-management/delete', 'SalesController@groupDelete');


	Route::post('sales/team-management/supervisor/get-demote', 'SalesController@getDemoteCode');
	Route::post('sales/team-management/supervisor/get-demote-code', 'SalesController@getDemoteManagerCode');
	
	Route::post('sales/team-management/supervisor/save-demote', 'SalesController@demoteSave');
	Route::post('sales/team-management/advisor/save-promote', 'SalesController@promoteSave');
	Route::post('sales/team-management/advisor/save-relocation', 'SalesController@relocationSave');
	Route::post('sales/team-management/save', 'SalesController@groupSave');
	Route::post('sales/team-management/supervisor/save-code', 'SalesController@supervisorSave');
	Route::post('sales/team-management/supervisor/save-relocate', 'SalesController@relocateSave');

	Route::post('sales/team-management/get-manager-code', 'SalesController@getManagerCode');
	Route::post('sales/team-management/refresh', 'SalesController@doListTeamManagement');
	Route::post('sales/team-management/manipulate', 'SalesController@editDirectorCode');
	Route::post('sales/team-management/supervisor/manipulate', 'SalesController@editSupervisor');
	Route::post('sales/team-management/advisor/manipulate', 'SalesController@editAdvisor');
	Route::post('sales/team-management/get-users', 'SystemController@getUser');
	Route::post('sales/groups/all', 'SalesController@getGroup');

	Route::post('sales/refresh/log', 'SalesController@doListLog');
	Route::post('sales/team-management/refresh/log', 'SalesController@doListTeamLog');
});

//Payroll Management
Route::get('payroll-management/upload-data-feeds', 'PayrollController@index');
Route::get('payroll-management/generate-payroll', 'PayrollController@indexGenerate');
Route::get('payroll-management/incentives-or-deductions', 'PayrollController@indexPrePayroll');
Route::get('payroll-management/payroll-computation', 'PayrollController@indexPayrollComputation');
Route::get('payroll-management/payroll-summary', 'PayrollController@indexSummaryAdvisor');
// Route::post('payroll/payroll-summary-advisor/view-batch-expand', 'PrintPreviewController@viewBatchExpand');
// Route::post('payroll/payroll-summary-advisor/view-advisor-item-expand', 'PrintPreviewController@viewAdvisorItemExpand');
// Route::post('payroll/payroll-summary-advisor/view-advisor-item-sub-expand-revenue', 'PrintPreviewController@viewAdvisorItemSubExpandRevenue');
Route::post('payroll/payroll-summary-advisor/view-batch-expand', 'PayrollPDFController@viewBatchExpand');
Route::post('payroll/payroll-summary-advisor/xls-batch-expand', 'PayrollPDFController@xlsBatchExpand');
Route::post('payroll/payroll-summary-advisor/pdf-firm-revenue', 'PayrollPDFController@pdfFirmRevenue');
Route::post('payroll/payroll-summary-advisor/pdf-batch-expand', 'PayrollPDFController@batchExpand');
Route::post('payroll/payroll-summary-advisor/pdf-advisor-expand', 'PayrollPDFController@advisorExpand');
Route::post('payroll/payroll-summary-advisor/pdf-payroll-breakdown-revenue', 'PayrollPDFController@payrollBreakdownRevenue');
Route::post('payroll/payroll-summary-advisor/pdf-payroll-breakdown-override', 'PayrollPDFController@payrollBreakdownOverride');
// Route::post('payroll/payroll-summary-advisor/pdf-advisor-item-expand', 'PayrollPDFController@advisorItemExpand');
Route::post('payroll/payroll-summary-advisor/pdf-summary-revenue', 'PayrollPDFController@advisorSummaryRevenue');
Route::post('payroll/payroll-summary-advisor/pdf-summary-override', 'PayrollPDFController@advisorSummaryOverride');
Route::post('payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue', 'PayrollPDFController@advisorItemSubExpandRevenue');
Route::post('payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-revenue-or', 'PayrollPDFController@advisorItemSubExpandRevenueOR');
Route::post('payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-override', 'PayrollPDFController@advisorItemSubExpandOverride');
Route::post('payroll/payroll-summary-advisor/pdf-advisor-item-sub-expand-override-or', 'PayrollPDFController@advisorItemSubExpandOverrideOR');
// Route::get('payroll/payroll-summary-supervisor', 'PayrollController@indexSummarySupervisor');

Route::group(array('before' => 'ajax'), function() {
	Route::post('payroll/refresh/upload', 'PayrollController@doList');
	Route::post('payroll/paginate', 'PayrollController@paginateList');
	Route::post('payroll/upload', 'PayrollController@upload');
	Route::post('payroll/show-map', 'PayrollController@showMap');
	Route::post('payroll/get-feed', 'PayrollController@getFeed');
	Route::post('payroll/get-parser', 'PayrollController@getParser');
	Route::post('payroll/get-error', 'PayrollController@getError');
	Route::post('payroll/map', 'PayrollController@map');
	Route::post('payroll/parse', 'PayrollController@parse');

	Route::post('payroll/allocate', 'PayrollController@allocate');
	Route::post('payroll/allocate-check', 'PayrollController@allocateCheck');
	Route::post('payroll/allocate-total', 'PayrollController@allocateTotal');

	Route::post('payroll/delete', 'PayrollController@delete');

	Route::post('payroll/batch/check', 'PayrollController@batchCheck');
	Route::post('payroll/batch/save', 'PayrollController@batchSave');
	Route::post('payroll/batch/save-total', 'PayrollController@saveTotal');
	
	Route::post('payroll/admin-pre-payroll-entries/clawback', 'PayrollController@getClawback');
	Route::post('payroll/admin-pre-payroll-entries/clawback/save', 'PayrollController@saveClawback');
	
	Route::post('payroll/generate-payroll/remove-select', 'PayrollController@generateRemove');
	Route::post('payroll/generate-payroll/rollback-select', 'PayrollController@generateRollback');

	Route::post('payroll/generate-payroll/recompute-check', 'PayrollController@recomputeCheck');
	Route::post('payroll/generate-payroll/recompute', 'PayrollController@recompute');
	Route::post('payroll/generate-payroll/recompute-save-total', 'PayrollController@recomputeSaveTotal');

	Route::post('payroll/generate-payroll/release-check', 'PayrollController@releaseCheck');
	Route::post('payroll/generate-payroll/release', 'PayrollController@release');
	Route::post('payroll/generate-payroll/release-save-total', 'PayrollController@releaseSaveTotal');
	Route::post('payroll/generate-payroll/release-save-inception', 'PayrollController@releaseInception');

	Route::post('payroll/generate-payroll/refresh', 'PayrollController@doListGenerate');
	Route::post('payroll/generate-payroll/get-batch', 'PayrollController@getBatch');
	Route::post('payroll/generate-payroll/update-batch', 'PayrollController@updateBatch');
	Route::post('payroll/admin-pre-payroll-entries/get-batch', 'PayrollController@getPrePayrollBatch');
	Route::post('payroll/admin-pre-payroll-entries/get-policies', 'PayrollController@getBatchPolicies');
	Route::post('payroll/admin-pre-payroll-entries/get-type', 'PayrollController@getPrePayrollType');
	Route::post('payroll/admin-pre-payroll-entries/get-adjustments', 'PayrollController@getAdjustments');
	Route::post('payroll/admin-pre-payroll-entries/save', 'PayrollController@prePayrollSave');
	Route::post('payroll/admin-pre-payroll-entries/view', 'PayrollController@prePayrollView');
	Route::post('payroll/admin-pre-payroll-entries/refresh', 'PayrollController@doListPrePayroll');
	Route::post('payroll/admin-pre-payroll-entries/enable', 'PayrollController@enableprePayroll');
	Route::post('payroll/admin-pre-payroll-entries/disable', 'PayrollController@disableprePayroll');
	Route::post('payroll/admin-pre-payroll-entries/remove-select', 'PayrollController@prePayrollremoveSelected');
	Route::post('payroll/admin-pre-payroll-entries/disable-select', 'PayrollController@prePayrolldisableSelected');
	Route::post('payroll/payroll-computation/refresh', 'PayrollController@doListPayrollComputation');
	Route::post('payroll/payroll-computation/get-agents', 'PayrollController@getComputationAgents');
	Route::post('payroll/payroll-computation/get-payroll', 'PayrollController@getComputationAgentPayroll');
	Route::post('payroll/payroll-computation/get-pre-payroll', 'PayrollController@getComputationAgentPrePayroll');
	Route::post('payroll/payroll-computation/get-incentives', 'PayrollController@getComputationAgentIncentives');
	Route::post('payroll/payroll-computation/get-deductions', 'PayrollController@getComputationAgentDeductions');
	Route::post('payroll/payroll-computation/get-policies', 'PayrollController@getComputationAgentPolicies');
	Route::post('payroll/payroll-computation/get-computation', 'PayrollController@getComputation');
	Route::post('payroll/payroll-summary-advisor/refresh', 'PayrollController@doListSummaryAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-month', 'PayrollController@getMonthsAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-item', 'PayrollController@getItemsAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-introducer', 'PayrollController@getIntroducerAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-introducer-policies', 'PayrollController@getIntroducerSubPoliciesAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-assigned-policies', 'PayrollController@getIntroducerPoliciesAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission', 'PayrollController@getCommissionAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override', 'PayrollController@getOverrideAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-incentives', 'PayrollController@getIncentivesAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-deductions', 'PayrollController@getDeductionsAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-provider', 'PayrollController@getCommissionProviderAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-provider', 'PayrollController@getCommissionProviderGroupAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-unit-provider', 'PayrollController@getCommissionProviderGroupUnitAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-unit-team-provider', 'PayrollController@getCommissionProviderGroupUnitTeamAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-personal-provider', 'PayrollController@getCommissionProviderGroupPersonalAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-unit-advisor-provider', 'PayrollController@getCommissionProviderGroupUnitUserAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-unit-personal-advisor-provider', 'PayrollController@getCommissionProviderGroupUnitPersonalAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-unit-supervisor-advisor-provider', 'PayrollController@getCommissionProviderGroupUnitSupervisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-unit-advisor-advisor-provider', 'PayrollController@getCommissionProviderGroupUnitAdvisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-unit-user-supervisor-advisor-provider', 'PayrollController@getCommissionProviderGroupUnitUserSupervisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-group-supervisor-provider', 'PayrollController@getCommissionProviderGroupSupervisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-commission-nogroup-provider', 'PayrollController@getCommissionProviderNoGroupAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-provider', 'PayrollController@getOverrideProviderAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-provider', 'PayrollController@getOverrideProviderGroupAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-personal-provider', 'PayrollController@getOverrideProviderGroupPersonalAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-unit-team-provider', 'PayrollController@getOverrideProviderGroupUnitTeamAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-unit-personal-advisor-provider', 'PayrollController@getOverrideProviderGroupUnitPersonalAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-unit-supervisor-advisor-provider', 'PayrollController@getOverrideProviderGroupUnitSupervisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-unit-advisor-advisor-provider', 'PayrollController@getOverrideProviderGroupUnitAdvisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-unit-user-supervisor-advisor-provider', 'PayrollController@getOverrideProviderGroupUnitUserSupervisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-unit-provider', 'PayrollController@getOverrideProviderGroupUnitAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-unit-advisor-provider', 'PayrollController@getOverrideProviderGroupUnitUserAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-group-supervisor-provider', 'PayrollController@getOverrideProviderGroupSupervisorAdvisor');
	Route::post('payroll/payroll-summary-advisor/get-override-nogroup-provider', 'PayrollController@getOverrideProviderNoGroupAdvisor');
});

//Policy Management
Route::get('production-and-policy-mgt/policies', 'PolicyController@index');
Route::get('policy/{id}/view', 'PolicyController@view');
Route::get('policy/edit', 'PolicyController@edit');
Route::post('policy/export', 'PolicyController@export');
Route::post('policy/bsc/export', 'PolicyController@exportBSC');
Route::get('production-and-policy-mgt/policies/riders', 'PolicyController@indexRider');
Route::get('summary-and-reports/bsc-report', 'PolicyController@indexBSC');
Route::get('policy/riders/view', 'PolicyController@viewRider');
Route::get('policy/riders/edit', 'PolicyController@editRider');
Route::get('production-and-policy-mgt/introducers', 'PolicyController@indexIntroducer');
Route::get('policy/introducers/view', 'PolicyController@viewIntroducer');
Route::get('policy/introducers/edit', 'PolicyController@editIntroducer');
Route::get('production-and-policy-mgt/policy-log', 'PolicyController@indexHistory');
Route::get('production-and-policy-mgt/orphan-policy-service', 'PolicyController@indexOrphan');
Route::get('policy/orphan-policies/view', 'PolicyController@viewOrphan');
Route::get('policy/orphan-policies/edit', 'PolicyController@editOrphan');
Route::get('policy/orphan-policies/move', 'PolicyController@moveOrphan');
Route::get('policy/orphan-policies/{id}/notification', 'PolicyController@notiOrphan');
Route::get('production-and-policy-mgt/duplicates', 'PolicyController@indexDuplicate');
Route::get('policy/duplicates/view', 'PolicyController@viewDuplicate');
Route::get('policy/duplicates/edit', 'PolicyController@editDuplicate');

Route::get('production-and-policy-mgt/life-submission', 'PolicyController@indexCaseSubmission');
Route::get('production-and-policy-mgt/pending-cases', 'PolicyController@indexPendingCases');
Route::post('policy/production/case-submission/export', 'PolicyController@exportCase');
Route::get('policy/production/case-inception', 'PolicyController@indexCaseInception');	
Route::get('policy/production/case-submission/{id}/notification', 'PolicyController@notification');

Route::get('policy/production/download/{filename}', function($filename) {

// 	if (Auth::check()) {
		$get_id = explode('-', $filename);
		$filename_display = "download";

		if (count($get_id) > 1) {
			if (is_numeric($get_id[0])) {
				$get_file = App\ProductionCases::find($get_id[0]);
				if ($get_file) {
					$get_name = explode('.', $get_id[1]);
					if ($get_name[0] == "one") {
						$filename_display = $get_file->upload_title;
					} else if ($get_name[0] == "two") {
						$filename_display = $get_file->upload_2_title;
					} else if ($get_name[0] == "three") {
						$filename_display = $get_file->upload_3_title;
					} else if ($get_name[0] == "four") {
						$filename_display = $get_file->upload_4_title;
					} else if ($get_name[0] == "five") {
						$filename_display = $get_file->upload_5_title;
					}
				}
			}
		}

	    $file = storage_path('case') . '/' . $filename;
			return Response::make(file_get_contents($file), 200, [
					'Content-Type' => 'application/pdf',
					'Content-Disposition' => 'inline; filename="'.$filename_display.'"'
			]);		
		
// 	} else {
// 		echo("Service unavailable");
// 	}

});

Route::group(array('before' => 'ajax'), function() {
	Route::post('policy/refresh', 'PolicyController@doList');
	Route::post('policy/paginate', 'PolicyController@paginateList');
	Route::post('policy/get-policies', 'PolicyController@getUserPolicies');
	Route::post('policy/duplicates/refresh', 'PolicyController@doListDuplicate');
	Route::post('policy/riders/refresh', 'PolicyController@doListRider');
	Route::post('policy/bsc/refresh', 'PolicyController@doListBSC');
	Route::post('policy/bsc/view', 'PolicyController@viewBSC');
	Route::post('policy/bsc/save-bsc', 'PolicyController@saveBSCReport');
	
	Route::post('policy/production/get-currency-rate', 'PolicyController@getCaseCurrencyRate');

	Route::post('policy/production/case-submission-advisor/save', 'PolicyController@caseSubmissionAdvisorSave');
	Route::post('policy/production/case-submission-advisor/get-currency', 'PolicyController@getCaseCurrency');
	Route::post('policy/production/case-submission-advisor/view', 'PolicyController@caseSubmissionAdvisorView');
	Route::post('policy/production/case-submission-advisor/refresh', 'PolicyController@doListCaseSubAdvisor');
	Route::post('policy/production/case-submission/pending-cases/refresh', 'PolicyController@doListPendingCases');
	Route::post('policy/production/case-submission-advisor/approved', 'PolicyController@approved');
	Route::post('policy/production/case-submission-advisor/verified', 'PolicyController@verified');
	Route::post('policy/production/case-submission-advisor/reject', 'PolicyController@reject');
	Route::post('policy/production/case-submission-advisor/decept', 'PolicyController@decept');
	Route::post('policy/production/case-submission-advisor/enable', 'PolicyController@enable');
	
	Route::post('policy/production/assessment', 'PolicyController@assessment');
	Route::post('policy/production/undetermined-life', 'PolicyController@undeterminedLife');
	Route::post('policy/production/negative-life', 'PolicyController@negativeLife');
	Route::post('policy/production/undetermined-gi', 'PolicyController@undeterminedGI');
	Route::post('policy/production/negative-gi', 'PolicyController@negativeGI');

	Route::post('policy/production/assessment', 'PolicyController@assessment');
	Route::post('policy/production/review', 'PolicyController@review');
	Route::post('policy/production/submitted', 'PolicyController@submitted');
	Route::post('policy/production/duplicate', 'PolicyController@duplicate');
	Route::post('policy/production/case-submission-advisor/disable', 'PolicyController@disable');
	Route::post('policy/production/case-submission-advisor/incept-select', 'PolicyController@inceptSelected');
	Route::post('policy/production/case-submission-advisor/decept-select', 'PolicyController@deceptSelected');
	Route::post('policy/production/case-submission/remove-select', 'PolicyController@removeCaseSelected');
	Route::post('policy/production/save', 'PolicyController@caseSave');
	Route::post('policy/production/view', 'PolicyController@caseView');
	Route::post('policy/production/view-all', 'PolicyController@caseViewAll');
	Route::post('policy/production/case-submission/refresh', 'PolicyController@doListCaseSubmission');
	Route::post('policy/production/case-inception/refresh', 'PolicyController@doListCaseInception');
	Route::post('policy/production/case-submission/delete', 'PolicyController@deletePending');
	Route::post('policy/production/case-submission/get-plan', 'PolicyController@getMainPlan');
	Route::post('policy/production/case-submission/get-rate', 'PolicyController@getRate');
	Route::post('policy/production/case-submission/get-riders', 'PolicyController@getRider');
	Route::post('policy/production/case-submission/get-rider-plan', 'PolicyController@getRiderPlan');

	Route::post('policy/bsc/check', 'PolicyController@checkBSC');
	Route::post('policy/bsc/save', 'PolicyController@saveBSC');
	Route::post('policy/bsc/done', 'PolicyController@doneBSC');


	// Route::post('policy/production/case-submission-advisor/incept', 'PolicyController@incept');
	// Route::post('policy/production/case-submission-supervisor/save', 'PolicyController@caseSubmissionSupervisorSave');
	// Route::post('policy/production/case-submission-supervisor/view', 'PolicyController@caseSubmissionSupervisorView');
	// Route::post('policy/production/case-submission-supervisor/refresh', 'PolicyController@doListCaseSubSupervisor');
	// Route::post('policy/production/case-inception-advisor/save', 'PolicyController@caseInceptionAdvisorSave');
	// Route::post('policy/production/case-inception-advisor/view', 'PolicyController@caseInceptionAdvisorView');
	// Route::post('policy/production/case-inception-advisor/refresh', 'PolicyController@doListCaseIncAdvisor');
	// Route::post('policy/production/case-inception-supervisor/save', 'PolicyController@caseInceptionSupervisorSave');
	// Route::post('policy/production/case-inception-supervisor/view', 'PolicyController@caseInceptionSupervisorView');
	// Route::post('policy/production/case-inception-supervisor/refresh', 'PolicyController@doListCaseIncSupervisor');

	Route::post('policy/orphan-policies/refresh', 'PolicyController@doListOrphan');
	Route::post('policy/introducers/save', 'PolicyController@introducerSave');
	Route::post('policy/introducers/assign', 'PolicyController@introducerAssign');
	Route::post('policy/introducers/view', 'PolicyController@introducerView');
	Route::post('policy/introducers/assign-view', 'PolicyController@introducerAssignView');
	Route::post('policy/introducers/get-providers', 'PolicyController@getProviders');
	Route::post('policy/introducers/get-policies', 'PolicyController@getPolicies');
	Route::post('policy/introducers/refresh', 'PolicyController@doListIntroducer');
	Route::post('policy/introducers/enable', 'PolicyController@enableIntroducer');
	Route::post('policy/introducers/disable', 'PolicyController@disableIntroducer');
	Route::post('policy/introducers/enableAssign', 'PolicyController@enableAssign');
	Route::post('policy/introducers/disableAssign', 'PolicyController@disableAssign');
	Route::post('policy/introducers/delete', 'PolicyController@delete');
	Route::post('policy/production/remove-select', 'PolicyController@introducerremoveSelected');
	Route::post('policy/production/disable-select', 'PolicyController@introducerdisableSelected');
});

//Provider Management
Route::get('provider-and-products-mgt/provider-list', 'ProviderController@index');

Route::group(array('before' => 'ajax'), function() {
	Route::post('provider/save', 'ProviderController@save');
	Route::post('provider/view', 'ProviderController@view');
	Route::post('provider/refresh', 'ProviderController@doList');
	Route::post('provider/enable', 'ProviderController@enable');
	Route::post('provider/disable', 'ProviderController@disable');
	Route::post('provider/disable-select', 'ProviderController@disableSelected');
	Route::post('provider/remove', 'ProviderController@remove');
	Route::post('provider/get-provider-property', 'ProviderController@getProviderProperty');
	Route::post('provider/get-provider-product', 'ProviderController@getProviderProduct');
	Route::post('provider/property/save', 'ProviderController@saveProperty');
	Route::post('provider/property/view', 'ProviderController@viewProperty');
	Route::post('provider/classification/delete', 'ProviderController@deleteClassification');
	Route::post('provider/category/delete', 'ProviderController@deleteCategory');
});

//Product Management
Route::get('provider-and-products-mgt/product-list', 'ProductController@index');
Route::get('provider-and-products-mgt/product-log', 'ProductController@indexLog');
Route::post('product/logs/export', 'ProductController@export');
Route::post('product/export', 'ProductController@exportProduct');

Route::group(array('before' => 'ajax'), function() {
	Route::post('product/save', 'ProductController@save');
	Route::post('product/delete', 'ProductController@delete');
	Route::post('product/delete-case', 'ProductController@deleteCase');
	Route::post('product/save-main', 'ProductController@mainSave');
	Route::post('product/save-case', 'ProductController@caseSave');
	Route::post('product/view', 'ProductController@view');
	Route::post('product/view-case', 'ProductController@caseView');
	Route::post('product/view-main', 'ProductController@mainView');
	Route::post('product/refresh', 'ProductController@doList');
	Route::post('product/enable', 'ProductController@enable');
	Route::post('product/disable', 'ProductController@disable');
	Route::post('product/disable-select', 'ProductController@disableSelected');
	Route::post('product/remove-select', 'ProductController@removeSelected');
	Route::post('product/product-rate', 'ProductController@productRate');
	Route::post('product/product-rider', 'ProductController@productRider');
	Route::post('product/product-case', 'ProductController@productCase');
	Route::post('product/rate', 'ProductController@rate');
	Route::post('product/get-property', 'ProductController@getProperty');
	Route::post('product/log/refresh', 'ProductController@doListLog');
});

Route::get('summary', 'SummaryController@index');

//Reports
Route::get('reports', 'ReportController@index');
Route::get('reports/export', 'ReportController@indexExport');
Route::get('summary-and-reports/ongoing-fees-report', 'PolicyController@indexOngoing');
Route::post('reports/ongoing-fees/export', 'PolicyController@exportOngoing');
Route::get('summary-and-reports/initial-fees-report', 'PolicyController@indexOngoing');
Route::post('reports/initial-fees/export', 'PolicyController@exportOngoing');

Route::get('summary-and-reports/cpd-report','ReportController@indexCPDReport');
Route::get('reports/{id}/get-cpd-info', 'ReportController@getReport');
Route::get('reports/{id}/exportexcel', 'ReportController@exportExcel');
Route::get('reports/{id}/exportpdf', 'ReportController@exportPdf');
Route::group(array('before' => 'ajax'), function() {
	Route::post('reports/ongoing-fees/refresh','PolicyController@doListOngoing');
	Route::post('reports/initial-fees/refresh','PolicyController@doListOngoing');
	Route::post('reports/cpd/generate','ReportController@generateCPD');
	Route::post('reports/cpd/refresh','ReportController@doListCPDReport');
	Route::post('reports/cpd/view','ReportController@viewCPDReport');
	Route::post('reports/cpd/remove','ReportController@deleteCPDReport');
	Route::post('reports/get/group','ReportController@getPerGroup');
	Route::post('reports/get/user','ReportController@getPerUser');
});


//Settings
Route::get('settings', 'SettingsController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('settings/save', 'SettingsController@save');
	Route::post('settings/refresh', 'SettingsController@doList');
	Route::post('settings/refreshRate', 'SettingsController@doListRate');
	Route::post('settings/refresh/tier', 'SettingsController@doListTier');
	Route::post('settings/refresh/designation-rates', 'SettingsController@doListDesignationRates');
	Route::post('settings/refresh/payroll-rates', 'SettingsController@doListPayrollRates');
	Route::post('settings/refresh/cpd', 'SettingsController@doListCPD');
	Route::post('settings/currencysave', 'SettingsController@currencysave');
	Route::post('settings/tierSave','SettingsController@saveTier');
	Route::post('settings/cpdSave','SettingsController@saveCPD');
	Route::post('settings/giSave','SettingsController@saveGI');
});

Route::get('production-and-policy-mgt/gi-submission', 'CaseSubmissionController@indexCaseSubmission');
Route::get('policy/production/gi-case-submission/{id}/notification', 'CaseSubmissionController@notification');
Route::get('policy/production/gi-case-submission/download/{filename}', function($filename) {

	$get_id = explode('-', $filename);
	$filename_display = "download";

	if (count($get_id) > 1) {
		if (is_numeric($get_id[0])) {
			$get_file = App\GIProductionCases::find($get_id[0]);
			if ($get_file) {
				$get_name = explode('.', $get_id[1]);
				if ($get_name[0] == "one") {
					$filename_display = $get_file->upload_title;
				} else if ($get_name[0] == "two") {
					$filename_display = $get_file->upload_2_title;
				} else if ($get_name[0] == "three") {
					$filename_display = $get_file->upload_3_title;
				} else if ($get_name[0] == "four") {
					$filename_display = $get_file->upload_4_title;
				} else if ($get_name[0] == "five") {
					$filename_display = $get_file->upload_5_title;
				}
			}
		}
	}

    $file = storage_path('gi-case') . '/' . $filename;
    return Response::make(file_get_contents($file), 200, [
				'Content-Type' => 'application/pdf',
				'Content-Disposition' => 'inline; filename="'.$filename.'"'
		]);
});

Route::post('policy/production/gi-case-submission/export', 'CaseSubmissionController@exportCase');

Route::group(array('before' => 'ajax'), function() {
	Route::post('policy/production/gi-case-submission/get-currency', 'CaseSubmissionController@getCaseCurrency');
	Route::post('policy/production/gi-case-submission/approved', 'CaseSubmissionController@approved');
	Route::post('policy/production/gi-case-submission/verified', 'CaseSubmissionController@verified');
	Route::post('policy/production/gi-case-submission/reject', 'CaseSubmissionController@reject');
	Route::post('policy/production/gi-case-submission/assessment', 'CaseSubmissionController@assessment');
	Route::post('policy/production/gi-case-submission/review', 'CaseSubmissionController@review');
	Route::post('policy/production/gi-case-submission/submitted', 'CaseSubmissionController@submitted');
	Route::post('policy/production/gi-case-submission/duplicate', 'CaseSubmissionController@duplicate');
	Route::post('policy/production/gi-case-submission/remove-select', 'CaseSubmissionController@removeCaseSelected');
	Route::post('policy/production/gi-case-submission/save', 'CaseSubmissionController@caseSave');
	Route::post('policy/production/gi-case-submission/view', 'CaseSubmissionController@caseView');
	Route::post('policy/production/gi-case-submission/view-all', 'CaseSubmissionController@caseViewAll');
	Route::post('policy/production/gi-case-submission/refresh', 'CaseSubmissionController@doListCaseSubmission');
	Route::post('policy/production/gi-case-submission/get-plan', 'CaseSubmissionController@getMainPlan');
	Route::post('policy/production/gi-case-submission/get-rate', 'CaseSubmissionController@getRate');
	Route::post('policy/production/gi-case-submission/get-riders', 'CaseSubmissionController@getRider');
	Route::post('policy/production/gi-case-submission/get-rider-plan', 'CaseSubmissionController@getRiderPlan');
	Route::post('policy/production/gi-case-submission/delete', 'CaseSubmissionController@deletePending');
});


Route::get('manage/list/ips', 'IPController@index');
Route::post('manage/ips/save', 'IPController@save');
Route::post('manage/ips/get', 'IPController@getIPInfo');
Route::post('manage/ips/delete', 'IPController@delete');
Route::post('manage/ips/toggle', 'IPController@toggle');
Route::post('manage/ips/refresh', 'IPController@doList');
Route::get('quick-ip/{action}/{ip}/{expiration?}', 'IPController@IPAction');
Route::get('dev-bypass/login/{hash}', 'IPController@BypassIPRestriction');
Route::get('if/export/{date}', 'DashboardController@XLSBreakdown');
Route::post('riders/product/export', 'ProductController@exportRiders');

Route::get('fix_cpd_total/now', 'ReportController@fix_cpd_total');

Route::post('forgot/passwd', 'ForgotController@SendMail');

Route::get('fix/all-names/now', 'CaseSubmissionController@FixNames');

Route::post('inceptions/import', 'InceptionImportController@importXLS');

Route::get('array/test', 'PolicyController@ARRAYTEST');

//GI Banding "GET" add
Route::get('gi-rank/{rank}/{lfa}/{mgr}/{fsd}/{agent}', 'SettingsController@addGIrank');