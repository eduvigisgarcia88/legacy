<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teams';

    public function getTeams() {
        return $this->hasMany('App\SalesTeam', 'supervisor_id');
    }

}