<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignedPolicy extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'assigned_policies';

    public function getAssignedPolicyCode() {
        return $this->belongsTo('App\Policy', 'policy_id');
    }

    public function getAssignedUser() {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function getAssignedProvider() {
        return $this->belongsTo('App\Provider', 'provider_id');
    }

    public function getAssignedIntroducer() {
        return $this->belongsTo('App\Introducer', 'introducer_id');
    }
}