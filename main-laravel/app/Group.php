<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    public function salesSupervisor() {
        return $this->hasMany('App\SalesSupervisor', 'group_id');
    }

    public function getGroupSupervisor() {
        return $this->belongsTo('App\SalesSupervisor', 'group_id');
    }

    public function groupinfo(){
        return $this->belongsTo('App\User','user_id');
    }
    public function getGroupComputation() {
        return $this->hasMany('App\BatchMonthGroup', 'group_id');
    }
  
}