<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesDesignation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_designations';


    public function newDesignation() {
        return $this->belongsTo('App\Designation', 'designation_id');
    }

    public function prevDesignation() {
        return $this->belongsTo('App\Designation', 'prev_designation_id');
    }

    public function modify() {
        return $this->belongsTo('App\User', 'edit_id');
    }


}