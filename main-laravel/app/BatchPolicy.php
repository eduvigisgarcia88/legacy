<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchPolicy extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'batch_policies';

  	public function getBatchPolicyInfo() {
        return $this->belongsTo('App\Policy', 'policy_id');
    }

    public function getBatchPolicyUser() {
        return $this->belongsTo('App\User', 'user_id');
    }
    
  	public function getBatch() {
        return $this->belongsTo('App\Batch', 'batch_id');
    }

    public function getBatchPayrollComputation() {
        return $this->hasOne('App\PayrollComputation', 'batch_policy_id');
    }
}