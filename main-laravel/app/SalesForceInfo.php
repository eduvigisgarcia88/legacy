<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesForceInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_force_info';

     public function salesforceinfo(){

    	return $this->belongsTo('App\User','user_id');
    }


}
