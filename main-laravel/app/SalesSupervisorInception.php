<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesSupervisorInception extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_supervisor_inceptions';

    public function salesCompAdvisor() {
        return $this->hasMany('App\SalesAdvisorInception', 'supervisor_id');
    }

}