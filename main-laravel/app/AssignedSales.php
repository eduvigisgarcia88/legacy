<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignedSales extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'assigned_sales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sales_id', 'assistant_id'];
    
    public function assistant() {
        return $this->belongsTo('App\User', 'assistant_id');
    }

    public function sales() {
        return $this->belongsTo('App\User', 'sales_id');
    }
}
