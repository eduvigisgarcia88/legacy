<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardInception extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dashboard_inceptions';
}
