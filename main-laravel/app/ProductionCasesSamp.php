<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionCasesSamp extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'production_cases_samp';

}