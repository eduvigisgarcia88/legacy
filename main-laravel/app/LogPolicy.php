<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogPolicy extends Model
{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'log_policies';

  	public function policyLog() {//added
        return $this->belongsTo('App\User', 'edit_id');
    }
   	public function policyinfo(){//added
   		return $this->belongsTo('App\Policy','policy_id');
   	}
}