<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesGI extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_gis';
    
}
