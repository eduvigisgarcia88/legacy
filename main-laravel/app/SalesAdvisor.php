<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesAdvisor extends Model
{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_advisors';

 	public function getAdvisors(){
 		return $this->belongsTo('App\Group','group_id');
 	}


 	public function getSupervisorInfo(){

 		return $this->belongsTo('App\SalesSupervisor','supervisor_id');
 	}


 	public function advisorInfo(){
        return $this->belongsTo('App\User','user_id');
    }
}