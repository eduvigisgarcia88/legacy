<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'batches';

    public function getBatchPolicies() {
        return $this->hasMany('App\BatchPolicy', 'batch_id');
    }
  
}