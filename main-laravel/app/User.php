<?php

namespace App;
use Crypt;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['system_id', 'code', 'usertype_id', 'email', 'mobile', 'name', 'username', 'password', 'status'];

    // public function getNameAttribute($value)
    // {
    //     return Crypt::decrypt($value);
    // }
    
    // public function setNameAttribute($value)
    // {
    //     $this->attributes['name'] = Crypt::encrypt($value);
    // }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function userType() {
        return $this->belongsTo('App\UserType', 'usertype_id');
    }

    public function salesInfo() {
        return $this->hasOne('App\Sales', 'user_id');
    }

    public function salesLogs() {
        return $this->hasOne('App\LogSales', 'user_id');
    }

    public function editSalesLogs() {
        return $this->hasOne('App\LogSales', 'edit_id');
    }

    public function userLogs() {
        return $this->hasOne('App\LogUser', 'user_id');
    }

    public function editUserLogs() {
        return $this->hasOne('App\LogUser', 'edit_id');
    }

    public function modifyDesignation() {
        return $this->hasOne('App\SalesDesignation', 'edit_id');
    }

    public function modifyBonding() {
        return $this->hasOne('App\SalesBonding', 'edit_id');
    }

    public function getUserPolicies() {
        return $this->hasMany('App\Policy', 'user_id');
    }

    public function policyLogs() {
        return $this->hasOne('App\LogPolicy','edit_id');
    }

    public function getAddInfo() {
        return $this->hasOne('App\Additionalinfo','user_id');
    }

    public function getTeamInfo() {
        return $this->hasOne('App\TeamInfo','user_id');
    }

    public function getMiscInfo() {
        return $this->hasOne('App\MiscInfo','user_id');
    }

    public function getSecurityInfo() {
        return $this->hasOne('App\SecurityInfo','user_id');
    }

    public function getDialerInfo() {
        return $this->hasOne('App\DialerInfo','user_id');
    }

    public function getCompanyInfo() {
        return $this->hasOne('App\CompanyInfo','user_id');
    }

     public function getServerInfo() {
        return $this->hasOne('App\iServerInfo','user_id');
    }

    public function getJumpsiteInfo() {
        return $this->hasOne('App\JumpsiteInfo','user_id');
    }
    
    public function getSalesForceInfo() {
        return $this->hasOne('App\SalesForceInfo','user_id');  
    }
  
    public function getAdvisorInfo() {
        return $this->hasOne('App\SalesAdvisor','user_id');
    }
  
    public function getSupervisorInfo() {
        return $this->hasOne('App\SalesSupervisor','user_id');
    }
  
    public function notifications() {
        return $this->hasMany('Notification');
    }
  
    public function getSalesUserInfo() {
        return $this->hasOne('App\SalesUser','user_id');
    }
  
    public function getGroupInfo() {
        return $this->hasMany('App\SalesUser','user_id');
    }
  
    public function getProviderCodes() {
        return $this->hasMany('App\ProviderCodes','user_id');
    }

    public function getSalesGroup() {
        return $this->hasOne('App\SalesGroup','user_id');
    }
}