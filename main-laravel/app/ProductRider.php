<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRider extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_riders';

    public function getProvider() {
        return $this->belongsTo('App\Provider', 'provider_id');
    }

    public function getCategory() {
        return $this->belongsTo('App\ProviderCategory', 'category_id');
    }
    
}