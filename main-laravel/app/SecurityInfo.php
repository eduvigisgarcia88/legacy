<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'security_info';


     public function securityinfo(){

    	return $this->belongsTo('App\User','user_id');
    }

}
