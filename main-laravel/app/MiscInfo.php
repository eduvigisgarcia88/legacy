<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiscInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'misc_info';

     public function miscinfo(){

    	return $this->belongsTo('App\User','user_id');
    }


}
