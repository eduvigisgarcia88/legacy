<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';


    public function getProvider() {
        return $this->belongsTo('App\Provider', 'provider_id');
    }

    public function getCategory() {
        return $this->belongsTo('App\ProviderCategory', 'category_id');
    }

    public function getClassification() {
        return $this->belongsTo('App\ProviderClassification', 'classification_id');
    }

    public function getGross() {
        return $this->hasMany('App\ProductGross', 'product_id');
    }

    public function getGross1() {
        return $this->hasOne('App\ProductGross', 'product_id')->where('year_id', 1);
    }

    public function getGross2() {
        return $this->hasOne('App\ProductGross', 'product_id')->where('year_id', 2);
    }

    public function getGross3() {
        return $this->hasOne('App\ProductGross', 'product_id')->where('year_id', 3);
    }

    public function getGross4() {
        return $this->hasOne('App\ProductGross', 'product_id')->where('year_id', 4);
    }
    
    public function getGross5() {
        return $this->hasOne('App\ProductGross', 'product_id')->where('year_id', 5);
    }
	
    public function getGross6() {
        return $this->hasOne('App\ProductGross', 'product_id')->where('year_id', 6);
    }
}