<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchMonth extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'batch_months';

    public function getBatchMonth() {
        return $this->belongsTo('App\Batch', 'batch_id');
    }

    public function getBatchMonthComputations() {
        return $this->hasMany('App\PayrollComputation', 'batch_id', 'batch_id');
    }

}