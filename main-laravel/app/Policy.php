<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'policies';
    
    public function getPolicyProvider() {
        return $this->belongsTo('App\Provider', 'provider_id');
    }

    public function getPolicySales() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function policyInfo() {
        return $this->hasOne('App\LogPolicy','policy_id');
    }

    public function getPolicyCategoryInfo(){
        return $this->belongsTo('App\ProviderClassification', 'category_id');
    }

    public function getRate(){
        return $this->belongsTo('App\SettingsRate', 'currency_id');
    }

}