<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class iServerInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'iserver_info';

     public function iserverinfo(){

    	return $this->belongsTo('App\User','user_id');
    }


}
