<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchMonthSupervisor extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'batch_month_supervisors';

}