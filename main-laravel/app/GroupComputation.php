<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupComputation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_computations';

    public function salesCompSupervisor() {
        return $this->hasMany('App\SalesSupervisorComputation', 'group_id');
    }
    
}