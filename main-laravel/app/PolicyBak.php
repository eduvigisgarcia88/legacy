<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyBak extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    // protected $table = 'policies';
    protected $table = 'policies_bak';
   
}