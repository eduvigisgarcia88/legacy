<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Additionalinfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'additional_info';

   	public function addInfo(){

   		return $this->belongsTo('App\User','user_id');
   	}
}
