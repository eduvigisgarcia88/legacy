<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Introducer extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'introducers';

    public function getUser() {
        return $this->belongsTo('App\User', 'user_id');
    }
   
}