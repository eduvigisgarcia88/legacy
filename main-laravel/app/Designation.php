<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'designations';

    public function salesDesignations() {
		return $this->hasMany('App\Sales', 'designation_id');
	}

    /*
    public function salesDesignations() {
        return $this->hasMany('App\Sales');
    }*/

    public function designationNew() {
        return $this->belongsTo('App\SalesDesignation', 'designation_id');
    }

    public function designationPrev() {
        return $this->belongsTo('App\SalesDesignation', 'prev_designation_id');
    }

    public function getRate() {
        return $this->belongsTo('App\Settings', 'id');
    }

    public function designation_rate(){
        return $this->belongsTo('App\Sales', 'sales_id');
    }
}
