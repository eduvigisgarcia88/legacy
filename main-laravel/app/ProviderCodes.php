<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderCodes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'provider_codes';


	 public function getproviderinfo(){
        return $this->belongsTo('App\Provider', 'provider_id');
    }    

  	 public function getuserproviderinfo(){
        return $this->belongsToMany('App\User', 'user_id');
    }    
}
