<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyContract extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'policy_contracts';
  
    public function getPolicyContract(){
        return $this->hasOne('App\Policy','contract_no', 'policy_no');
    }

    public function getPolicyContractUser(){
        return $this->belongsTo('App\User', 'user_id');
    }

}