<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Carbon;

use App\Group;
use App\GroupInception;
use App\SalesSupervisorInception;
use App\SalesAdvisorInception;
use App\BatchInception;
use App\SalesInception;
use App\GI;
use App\Sales;
use App\User;
use App\IPManage;
use Hash;
use App\BypassIP;
use Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {

          $row = new BatchInception;
          $row->incept_date = Carbon::createFromFormat('d/m/Y H:i:s',  Carbon::now()->format('d/m/Y') . ' 00:00:00');
          $row->save();

          $row->name = strval(Carbon::now()->format('d-m-Y')) . '-' . $row->id;
          $row->save();

          $get_group = Group::with('salesSupervisor.salesAdvisor')->where('status', '=', 1)->get();

          foreach ($get_group as $key => $field) {
            $group = new GroupInception;
            $group->batch_id = $row->id;
            $group->group_id = $field->id;
            $group->code = $field->code;
            $group->user_id = $field->user_id;
            $group->save();

            $supervisor = $field->salesSupervisor;
            foreach ($supervisor as $ikey => $ifield) {

              $sup = new SalesSupervisorInception;
              $sup->batch_id = $row->id;
              $sup->group_id = $group->id;
              $sup->user_id = $ifield->user_id;
              $sup->unit_code = $ifield->unit_code;
              $sup->save();

              $advisor = $ifield->salesAdvisor;
              foreach ($advisor as $skey => $sfield) {
                $adv = new SalesAdvisorInception;
                $adv->batch_id = $row->id;
                $adv->group_id = $group->id;
                $adv->supervisor_id = $sup->id;
                $adv->user_id = $sfield->user_id;
                $adv->save();
              }
            }
          }

          $get_users = User::where('usertype_id', 8)->where('status', '!=', 2)->get();
          foreach ($get_users as $gukey => $gufield) {
            $sales_user = Sales::where('user_id', $gufield->id)->first();

            if ($sales_user) {
              $row_user = new SalesInception;

              $row_user->batch_id = $row->id;
              $row_user->user_id = $gufield->id;
              $row_user->sales_id = $sales_user->id;
              $row_user->designation_id = $sales_user->designation_id;
              $row_user->designation_rate = $sales_user->designation_rate;
              $row_user->banding_rate = $sales_user->bonding_rate;
              $row_user->gi_banding_rank = $sales_user->gi_banding_rank;

              $gi = GI::where('rank', $sales_user->gi_banding_rank)->first();

              if ($gi) {
                $row_user->gi_banding_rate = $gi->rank . ',' . $gi->legacy_fa . ',' . $gi->mgr . ',' . $gi->fsd . ',' . $gi->agents;
              }

              $row_user->save();
            }
          }

        })->dailyAt('00:00');
      
      
        $schedule->call(function(){
          $ips = IPManage::where('status', 1)->whereNotNull('expiration_hours')->whereDate('expiration_date', '<=', Carbon::now()->toDateString())->get();
          foreach($ips as $ip)
          {
            $ip->status = 2;
            $ip->save();
          }
        })->everyMinute();

        $schedule->call(function(){
          $random_str = strtoupper(str_random(5));
          $ip_hash = BypassIP::first();
          $ip_hash->hash = Hash::make($random_str);
          $ip_hash->save();
          $body = 'New password as of GMT +8 Asia/Manila time '.Carbon::now('Asia/Manila').' is: '.$random_str;
          Mail::raw($body, function ($message) {
              $message->subject('Bypas IP Restriction - LegacyFA');
              $message->to('mallarikiyen@gmail.com', 'Alkane Mallari');
              $message->from('noreply@egacyfa-asia.com', 'Legacy FA');
          });
        })->everyMinute();

    }
}
