<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignationRates extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'designation_rates';

    public function getDesignation() {
        return $this->belongsTo('App\Designation', 'designation_id');
    }

    public function getDesignationOld() {
        return $this->belongsTo('App\Designation', 'prev_designation_id');
    }
}
