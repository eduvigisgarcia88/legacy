<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    //
    protected $table = 'usertypes';


	public function users() {
		return $this->hasMany('App\User');
	}
	
	public function modules() {
		return $this->hasMany('App\Permission', 'usertype_id');
	}

}