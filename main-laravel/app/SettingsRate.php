<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingsRate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'currencies';

}