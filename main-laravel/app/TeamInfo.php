<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'team_info';

    public function teaminfo(){

    	return $this->belongsTo('App\User','user_id');
    }
   

}
