<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feed_types';

}
