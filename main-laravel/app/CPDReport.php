<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CPDReport extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cpd_reports';

   	public function cpdInfo(){

   		return $this->belongsTo('App\User','user_id');
   	}
}
