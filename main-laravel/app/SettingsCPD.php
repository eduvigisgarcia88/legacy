<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingsCPD extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cpd_settings';

}