<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollYTDDashboard extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payroll_ytd_dashboards';

}
