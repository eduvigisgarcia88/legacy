<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionCaseContact extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'production_case_contacts';

}
