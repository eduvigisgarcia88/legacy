<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesSupervisor extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_supervisors';

    public function salesAdvisor() {
        return $this->hasMany('App\SalesAdvisor', 'supervisor_id');
    }
    public function supervisorinfo(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function groupInfo(){
        return $this->belongsTo('App\Group', 'group_id');
    }
  
    public function getGroupSupervisors(){
        return $this->hasMany('App\Group', 'group_id');
    }

    // public function supervisor() {
    //     return $this->belongsTo('App\Sales', 'supervisor_id');
    // }

    // public function teamID() {
    //     return $this->belongsTo('App\Team', 'team_id');
    // }

    // public function teamSales() {
    //     return $this->belongsTo('App\Sales', 'sales_id');
    // }

}