<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesPartner extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_partners';

    public function partnerInfo() {
        return $this->belongsTo('App\Sales', 'partner_id');
    }

}