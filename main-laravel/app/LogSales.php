<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSales extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'log_sales';

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'edit_id', 'activity'];

    public function salesLog() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function editSalesLog() {
        return $this->belongsTo('App\User', 'edit_id');
    }
}
