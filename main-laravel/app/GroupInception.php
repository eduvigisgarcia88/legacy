<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupInception extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_inceptions';

    public function salesCompSupervisor() {
        return $this->hasMany('App\SalesSupervisorInception', 'group_id');
    }
    
}