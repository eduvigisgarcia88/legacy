<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesBonding extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_bondings';

    public function bondingModify() {
        return $this->belongsTo('App\User', 'edit_id');
    }

}