<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrePayroll extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pre_payrolls';


    public function getUser() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getPrePayrollBatch() {
        return $this->belongsTo('App\Batch', 'batch_id');
    }

    public function getPrePayrollBatchPolicy() {
        return $this->belongsTo('App\BatchPolicy', 'batch_policy_id');
    }

    public function getPrePayrollPolicy() {
        return $this->belongsTo('App\Policy', 'policy_id');
    }

}