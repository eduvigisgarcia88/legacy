<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function notificationsPerUser()
    {
        return $this->hasMany('notifications_per_user');
    }
}