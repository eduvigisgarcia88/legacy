<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_info';

    public function companyinfo(){
    	return $this->belongsTo('App\User','user_id');
    }

}
