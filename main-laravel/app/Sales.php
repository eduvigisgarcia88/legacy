<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales';

    public function users() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function salesSupervisor() {
        return $this->hasOne('App\SalesSupervisor', 'sales_id');
    }
    
    public function getAdvisor() {
        return $this->hasOne('App\SalesTeam', 'sales_id');
    }

    public function visor() {
        return $this->hasOne('App\SalesPartner', 'sales_id');
    }

    public function team() {
        return $this->hasOne('App\Team', 'sales_id');
    }

    public function designations() {
        return $this->belongsTo('App\Designation', 'designation_id');
    }
  
    public function getDesignationRate() {
        return $this->hasOne('App\SalesDesignation','sales_id')->orderBy('created_at','desc');
    }
  
    public function getBandinginfo() {
        return $this->hasOne('App\SalesBonding','sales_id')->orderBy('created_at','desc');
    }
}
