<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesTeam extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_teams';

    public function teamCode() {
        return $this->belongsTo('App\SalesSupervisor', 'supervisor_id');
    }

    public function getSupervisor() {
        return $this->belongsTo('App\Sales', 'supervisor_id');
    }

    public function salesTeamInfo() {
        return $this->belongsTo('App\Sales', 'sales_id');
    }

    public function teamModified() {
        return $this->belongsTo('App\User', 'edit_id');
    }
    
    public function getSalesTeamCode() {
        return $this->belongsTo('App\Team', 'team_id');
    }
}