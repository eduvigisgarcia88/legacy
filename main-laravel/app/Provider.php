<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'providers';
    
    public function getProviderCategory() {
        return $this->hasMany('App\ProviderCategory', 'provider_id');
    }
    public function getProviderClassification() {
        return $this->hasMany('App\ProviderClassification', 'provider_id');
    }
    public function getprovidercodes() {
        return $this->hasMany('App\Provider', 'provider_id');
    }
    public function getProviderClassifications() {
        return $this->hasMany('App\ProviderClassification', 'provider_id');
    }
    public function getProducts() {
        return $this->hasMany('App\ProductMain', 'provider_id')->orderBy('name', 'asc')->where('status', '=', 1);
    }
    public function getProductsAll() {
        return $this->hasMany('App\ProductMain', 'provider_id');
    }
}