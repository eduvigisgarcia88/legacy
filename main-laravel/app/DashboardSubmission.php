<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardSubmission extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dashboard_submissions';
}
