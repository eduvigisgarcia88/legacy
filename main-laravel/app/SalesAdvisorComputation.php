<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesAdvisorComputation extends Model
{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_advisor_computations';

}