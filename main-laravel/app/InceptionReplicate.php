<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InceptionReplicate extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inception_replicates';
  
}