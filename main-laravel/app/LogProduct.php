<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogProduct extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'log_products';

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'edit_id', 'activity'];

    public function productLog() {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function editProductLog() {
        return $this->belongsTo('App\User', 'edit_id');
    }

}
