<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesGroup extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sales_groups';

    public function getSupervisorInfo() {
        return $this->belongsTo('App\SalesSupervisor','supervisor_id');
    }

    public function getGroupInfo() {
        return $this->belongsTo('App\SalesSupervisor','group_id');
    }
}