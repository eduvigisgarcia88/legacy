<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DialerInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dialer_info';

     public function dialerinfo(){

    	return $this->belongsTo('App\User','user_id');
    }


}
