<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionCaseRider extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'production_case_riders';

    public function getCaseRider() {
        return $this->belongsTo('App\Product', 'product_id');
    }

}
