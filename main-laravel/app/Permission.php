<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['view', 'edit', 'delete'];

	public function permissionUsers() {
        return $this->hasOne('App\UserType');
	}

    public function permissionModules() {
        return $this->belongsTo('App\PermissionModule', 'module_id');
    }
}