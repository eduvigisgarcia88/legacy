<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionCases extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'production_cases';

    public function getUserSubmission() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getContact() {
        return $this->hasMany('App\ProductionCaseContact', 'production_id');
    }

    public function getMainRiders() {
        return $this->hasMany('App\ProductRider', 'main_id', 'main_product_id');
    }

    public function getSum() {
        return $this->hasMany('App\ProductionCaseRider', 'production_id');
    }

    public function getProvider() {
        return $this->belongsTo('App\Provider', 'provider_list');
    }

    public function getSettingsRate() {
        return $this->belongsTo('App\SettingsRate', 'currency_id');
    }

}