 $("#btn-custom-photo").click(function() {

        $(".uploader").removeClass("hide");
        $(".default-photo-pane").addClass("hide");
        $(".photo_view").addClass("hide");
        $("#image_pane").addClass("hide");
        $("#image_pane_edit_photo").addClass("hide");

});


//tab navigation
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });

	$('.div-panel-dashboard .dropdown').on('show.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	});

	$('.div-panel-dashboard .dropdown').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	});
 //   $(".table-dashboard .fa-folder-o").parent().attr('title', 'Detailed Breakdown');
 //   $(".table-dashboard .fa-plus").parent().attr('title', 'Team Details');

 //   $(".table-dashboard .btn-table").click(function() {
 //   		if ($(this).find('i').hasClass('fa-plus')) {
 //   			$(this).find('i').removeClass('fa-plus').addClass('fa-minus');
 //   		} else if ($(this).find('i').hasClass('fa-minus')) {
 //   			$(this).find('i').removeClass('fa-minus').addClass('fa-plus');
 //   		} else if ($(this).find('i').hasClass('fa-folder-o')) {
 //   			$(this).find('i').removeClass('fa-folder-o').addClass('fa-folder-open-o');
 //   		} else if ($(this).find('i').hasClass('fa-folder-open-o')) {
 //   			$(this).find('i').removeClass('fa-folder-open-o').addClass('fa-folder-o');
 //   		}
 //   });

	// $('.table-responsive.table-responsive-dashboard .dropdown').on('show.bs.dropdown', function () {

	// 	var check_month = $(this).hasClass('month-fa');
	// 	if (check_month) {
	// 		console.log('asdasd');
	// 		$('.table-responsive.table-responsive-dashboard').removeAttr('style').attr('style', "min-height: 400px;");
	// 	} else {
	// 		$('.table-responsive.table-responsive-dashboard').removeAttr('style').attr('style', "min-height: 129px;");
	// 	}
	// });

	// $('.table-responsive.table-responsive-dashboard .dropdown').on('hide.bs.dropdown', function () {
	//     $('.table-responsive.table-responsive-dashboard').removeAttr('style');
	// })

});

//animate number
$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1200,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$("input.file_photo").fileinput({
    maxFileCount: 1,
    maxFileSize: 1024,
    allowedFileTypes: ['image'],
    allowedFileExtensions: ['jpg', 'bmp', 'png', 'jpeg'],
  
});

$("input.file_csv").fileinput({
    maxFileCount: 1,
    maxFileSize: 204800,
    allowedFileExtensions: ['xls', 'csv', 'xlsx'],
});

$("input.file_case").fileinput({
    maxFileCount: 1,
    maxFileSize: 204800,
    allowedFileExtensions: ['pdf', 'jpg', 'jpeg', 'tiff'],
});

$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
    options.async = true;
});

//$(".btn-file").html("<i class='fa fa-folder'></i>");
// tooltip
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
// show dialog modal
function dialog(title, body, action, id) {
	$("#dialog-title").html(title);
	$("#dialog-body").html(body);
	$("#dialog-confirm").data('url', action)
						.data('id', id);

	// show confirmation
    $("#modal-dialog").modal('show');
}

// display status message
function status(title, body, type, box) {
	if(box) {
		var html = '<' + 'div class="alert alert-dismissible ' + type + '" role="alert">' +
				   '<' + 'button type="button" class="close" data-dismiss="alert"><' + 'span aria-hidden="true">&times;</' + 'span><' + 'span class="sr-only">Close</' + 'span></' + 'button>' +
				   (title ? '<strong>' + title + '</strong> ' : '') + body +
				   '</div>';

		$(box).removeClass('hide').html(html);
	} else {
		switch(type) {
			case 'alert-success':
				toastr.success(body);
				break;
			case 'alert-danger':
				toastr.error(body);
				break;
		}
	}
}

// get url parameter
function urlData(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for(var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if(sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(document).ready(function() {

	$(".table").addClass("tablesorter");

	// do action after confirmation
	$("#dialog-confirm").click(function() {
		var url = $(this).data('url');
		var id = $(this).data('id');
      	token = $("#token").val();
  		var loading = $("#load-dialog");
  		loading.removeClass('hide');

		$.post(url, { id: id, _token: token }, function(response) {
			$("#modal-dialog").modal('hide');

	        if (response.unauthorized) {
	          window.location.href = response.unauthorized;
	        }

			else if(response.error) {
				status("Error", response.error, 'alert-danger');
			} else {
				status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);
				refresh();
			}
			loading.addClass('hide');
		}, 'json');
	});

	// reset form notice
	$(".modal").on('show.bs.modal', function(e) {
		$(".modal .sup-errors").html("");
		$(".modal .form-control").removeClass("required");
	});

	// reset form notice
	$("#modal-form").on('show.bs.modal', function(e) {
		$("#form-notice").html("");
	});

	// submit form
    $("#modal-form").find("form").submit(function(e) {
   
  		var form = $("#modal-form").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
      		processData: false,
      		contentType: false,
			success: function(response) {

		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        } else if (response.error_prompt)  {
					status(response.title, response.error_prompt, 'alert-danger');
		        } else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-form").modal('hide');
					status(response.title, response.body, 'alert-success');
					
					// refresh the list
					
					refresh();
          $(".facebook-photo-pane").addClass("hide");
          $(".default-photo-pane").addClass("hide");
				}

				loading.addClass('hide');

			}
  		});
    });
   

	// submit form
    $("#modal-profile").find("form").submit(function(e) {
  		var form = $("#modal-profile").find("form");
  		var loading = $("#load-profile");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_profile")[0]),
			dataType: "json",
			async:true,
      		processData: false,
      		contentType: false,
			success: function(response) {

		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-profile").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list 
					refresh_profile();

				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-designate").find("form").submit(function(e) {
   
  		var form = $("#modal-designate").find("form");
  		var loading = $("#load-designate");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');

  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {

			    if (response.unauthorized) {
			      window.location.href = response.unauthorized;
			    }

				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#designate-notice');
				} else {
					$("#modal-designate").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-team").find("form").submit(function(e) {
   
  		var form = $("#modal-team").find("form");
  		var loading = $("#load-team");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');

  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }

				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#team-notice');
				} else {
					$("#modal-team").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-bonding").find("form").submit(function(e) {
   
  		var form = $("#modal-bonding").find("form");
  		var loading = $("#load-bonding");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');

  		console.log(form.serialize());

  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }

				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#bonding-notice');
				} else {
					$("#modal-bonding").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-provider").find("form").submit(function(e) {
   
  		var form = $("#modal-provider").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-provider").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				
				loading.addClass('hide');

			}
  		});
    });

    // submit form
    $("#modal-product").find("form").submit(function(e) {
   
  		var form = $("#modal-product").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					// var errors = '<ul>';
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});

					// errors += '</ul>';

					// status('Please correct the following:', errors, 'alert-danger', '#product-notice');
				} else {
					$("#modal-product").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				
				loading.addClass('hide');

			}
  		});

    });

    // submit form
    $("#modal-rate").find("form").submit(function(e) {
   
  		var form = $("#modal-rate").find("form");
  		var loading = $("#load-rate");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#rate-notice');
				} else {
					$("#modal-rate").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });
     // submit form
   //  $("#modal-export").find("form").submit(function(e) {
   
  	// 	var $_token = $("input[name=_token]").val();
  	// 	var form = $("#modal-export").find("form");
  	// 	var loading = $("#load-form");
  	// 	var desigation_id = $("row-designation_id").val();
  	// 	var from = $("row-from").val();
  	// 	var to = $("row-to").val();
  	// 	// stop form from submitting
  	// 	e.preventDefault();

  	// 	loading.removeClass('hide');
  	// 	console.log(form.serialize());
  	// 	// push form data
  	// 	$.ajax({
			// type: "post",
			// url: form.attr('action'),
			// data: form.serialize(),
			// dataType: "json",
			// success: function(response) {
			// 	else if(response.error) {
			// 		 var errors = '<ul>';

			// 		 $.each(response.error, function(index, value) {
			// 			errors += '<li>' + value + '</li>';
			// 		});

			// 		 errors += '</ul>';

			// 		status('Please correct the following:',error, 'alert-danger', '#export-notice');
			// 	} else {
			// 		$("#modal-export").modal('hide');
			// 		status(response.title, response.body, 'alert-success');

					
			// 	}

			// 	loading.addClass('hide');

			// }
  	// 	});
   //  });
    // $("#modal-export").find("form").submit(function(e) {
    // 	var $_token = $("input[name=_token]").val();
  		// var form = $("#modal-export").find("form");
  		// var loading = $("#load-form");
  		// var desigation_id = $("row-designation_id").val();
  		// var from = $("row-from").val();
  		// var to = $("row-to").val();

    //       console.log($_token);


    //       $.post(form.attr('action'), { _token: $_token }, function(response) {
		  //       if (response.unauthorized) {
		  //         window.location.href = response.unauthorized;
		  //       }
    //        // var blob=new Blob([response]);
    //        // var link=document.createElement('a');
    //         //link.href=window.URL.createObjectURL(blob);
    //         //link.download="myfile.xls";
    //         //link.click();
    //       });

    //       $("#modal-export").modal('hide');

    //   });

  //   // submit form
  //   $("#modal-export").find("form").submit(function(e) {

  // 		var form = $("#modal-export").find("form");
  // 		var loading = $("#load-form");
  //    	var $_token = $("input[name=_token]").val();
     	
  //       $.post(form.attr('action'), { _token: $_token }, function(response) {
		// 	var blob=new Blob([response]);
		//     var link=document.createElement('a');
		//     link.href=window.URL.createObjectURL(blob);
		//     link.download="myfile.csv";
		//     link.click();
		// });

  // 		// stop form from submitting
  // 		e.preventDefault();

  // 		loading.removeClass('hide');

  // 		// push form data
  // 		$.ajax({
		// 	type: "post",
		// 	url: form.attr('action'),
		// 	data: form.serialize(),
		// 	dataType: "json",
		// 	success: function(response) {
		// 		else if(response.error) {
		// 			var errors = '<ul>';

		// 			$.each(response.error, function(index, value) {
		// 				errors += '<li>' + value + '</li>';
		// 			});

		// 			errors += '</ul>';

		// 			status('Please correct the following:', errors, 'alert-danger', '#export-notice');
		// 		} else {


		// 			$("#modal-export").modal('hide');
		// 			status(response.title, response.body, 'alert-success');

		// 			// // refresh the list
		// 			// refresh();
		// 		}

		// 		loading.addClass('hide');

		// 	}
  // 		});
  //   });
   $("#modal-reject").find("form").submit(function(e) {
      // console.log("sdsds");
      var form = $("#modal-reject").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if (response.unauthorized) {
          window.location.href = response.unauthorized;
        }
        else if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#reject-notice');
        } else {
          $("#modal-reject").modal('hide');
          status(response.title, response.body, 'alert-success');

          // // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

    // submit form
    $("#modal-production").find("form").submit(function(e) {
   
  		var form = $("#modal-production").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();
  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_production")[0]),
			dataType: "json",	
			async: true,
      		processData: false,
      		contentType: false,
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-production").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

    // submit form
    $("#modal-bsc").find("form").submit(function(e) {
   
  		var form = $("#modal-bsc").find("form");
  		var loading = $("#load-bsc");
  		// stop form from submitting
  		e.preventDefault();
  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        } else if (response.error_prompt)  {
					status(response.title, response.error_prompt, 'alert-danger');
		        } else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-bsc").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					after_save();
				}

				loading.addClass('hide');

			}
  		});
    });

    // submit form
    $("#modal-main").find("form").submit(function(e) {
   
  		var form = $("#modal-main").find("form");
  		var loading = $("#load-main");
  		// stop form from submitting
  		e.preventDefault();
  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-main").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

    // submit form
    $("#modal-case").find("form").submit(function(e) {
   
  		var form = $("#modal-case").find("form");
  		var loading = $("#load-case");
  		// stop form from submitting
  		e.preventDefault();
  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-case").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#form-parse").find("form").submit(function(e) {

    	var form = $("#form-parse").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#production-notice');
				} else {
					//$("#modal-production").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


    // submit form
   //  $("#modal-banding-edit").find("form").submit(function(e) {
   
  	// 	var form = $("#modal-banding-edit").find("form");
  	// 	var loading = $("#load-form");
  	// 	// stop form from submitting
  	// 	e.preventDefault();

  	// 	loading.removeClass('hide');
  	// 	console.log(form.serialize());
  	// 	// push form data
  	// 	$.ajax({
			// type: "post",
			// url: form.attr('action'),
			// data: form.serialize(),
			// dataType: "json",
			// success: function(response) {
			// 	else if(response.error) {
			// 		var errors = '<ul>';

			// 		$.each(response.error, function(index, value) {
			// 			errors += '<li>' + value + '</li>';
			// 		});

			// 		errors += '</ul>';

			// 		status('Please correct the following:', errors, 'alert-danger', '#banding-notice');
			// 	} else {
			// 		$("#modal-banding-edit").modal('hide');
			// 		status(response.title, response.body, 'alert-success');

			// 		// refresh the list
			// 		refresh();
			// 	}

			// 	loading.addClass('hide');

			// }
  	// 	});
   //  });

    // submit form
    $("#modal-assign").find("form").submit(function(e) {
   
  		var form = $("#modal-assign").find("form");
  		var loading = $("#load-form");
      var loading_assign = $("#load-assign");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
      loading_assign.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#assign-notice');
				} else {
					$("#modal-assign").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');
        loading_assign.addClass('hide');

			}
  		});
    });


    // submit form
    $("#modal-assessment").on('click', '.btn-false_positive', function (e) {
   
  		var form = $("#modal-assessment").find("form");
  		var loading = $("#load-assessment");

      $("#assessment-outcome").val("False Positive");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {

					$("#modal-assessment").modal('hide');
					status('Error', response.error, 'alert-danger');

				} else {
					$("#modal-assessment").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


    // submit form
    $("#modal-review").on('click', '.btn-false_positive', function (e) {
   
  		var form = $("#modal-review").find("form");
  		var loading = $("#load-review");

      $("#review-outcome").val("False Positive");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#review-notice');
				} else {
					$("#modal-review").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


    // submit form
    $("#modal-submitted").on('click', '.btn-yes_submit', function (e) {
   
  		var form = $("#modal-submitted").find("form");
  		var loading = $("#load-submitted");
      var loading_submitted = $("#load-submitted");

      $("#submitted-outcome").val("Yes");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
      loading_submitted.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#submitted-notice');
				} else {
					$("#modal-submitted").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');
        loading_assign.addClass('hide');

			}
  		});
    });

    // submit form
    $("#modal-assessment").on('click', '.btn-positive_hit', function (e) {
   
  		var form = $("#modal-assessment").find("form");
  		var loading = $("#load-assessment");

      $("#assessment-outcome").val("Positive Hit");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {

					$("#modal-assessment").modal('hide');
					status('Error', response.error, 'alert-danger');

				} else {
					$("#modal-assessment").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


    // submit form
    $("#modal-review").on('click', '.btn-positive_hit', function (e) {
   
  		var form = $("#modal-review").find("form");
  		var loading = $("#load-review");

      $("#review-outcome").val("Positive Hit");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#review-notice');
				} else {
					$("#modal-review").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-code").find("form").submit(function(e) {
   
  		var form = $("#modal-code").find("form");
  		var loading = $("#load-code");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-code").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-relocate").find("form").submit(function(e) {
   
  		var form = $("#modal-relocate").find("form");
  		var loading = $("#load-relocate");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-relocate").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-demote").find("form").submit(function(e) {
   
  		var form = $("#modal-demote").find("form");
  		var loading = $("#load-demote");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-demote").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-promote").find("form").submit(function(e) {
   
  		var form = $("#modal-promote").find("form");
  		var loading = $("#load-promote");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-promote").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-relocation").find("form").submit(function(e) {
   
  		var form = $("#modal-relocation").find("form");
  		var loading = $("#load-relocation");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-relocation").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });
    
	// submit form
    $("#modal-delete").find("form").submit(function(e) {
   
  		var form = $("#modal-delete").find("form");
  		var loading = $("#load-delete");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-delete").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


	// submit form
    $("#modal-duplicate").find("form").submit(function(e) {
   
  		var form = $("#modal-duplicate").find("form");
  		var loading = $("#load-duplicate");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.errorlog) {
					$("#modal-duplicate").modal('hide');
					status('', response.errorlog, 'alert-danger');

					// refresh the list
					refresh();
				} else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-duplicate").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-clawback").find("form").submit(function(e) {
   
  		var form = $("#modal-clawback").find("form");
  		var loading = $("#load-clawback");
  		
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
		        if (response.unauthorized) {
		          window.location.href = response.unauthorized;
		        }
				else if(response.errorlog) {
					$("#modal-clawback").modal('hide');
					status('', response.errorlog, 'alert-danger');

					// refresh the list
					refresh();
				} else if(response.error) {
					$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});
				} else {
					$("#modal-clawback").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });
   //  // submit form
   //  $("#modal-release").find("form").submit(function(e) {
   
  	// 	var form = $("#modal-release").find("form");
  	// 	var loading = $("#load-release");
  	// 	// stop form from submitting
  	// 	e.preventDefault();

  	// 	loading.removeClass('hide');
  	// 	console.log(form.serialize());
  	// 	// push form data
  	// 	$.ajax({
			// type: "post",
			// url: form.attr('action'),
			// data: form.serialize(),
			// dataType: "json",
			// success: function(response) {
			// 	console.log("uy");
			// 	else if(response.error) {
			// 		var errors = '<ul>';

			// 		$.each(response.error, function(index, value) {
			// 			errors += '<li>' + value + '</li>';
			// 		});

			// 		errors += '</ul>';

			// 		status('Batch Released Failed:', errors, 'alert-danger', '#release-notice');
			// 	} else {
			// 		$("#modal-release").modal('hide');
			// 		status(response.title, response.body, 'alert-success');

			// 		// refresh the list
			// 		refresh();
			// 	}

			// 	loading.addClass('hide');

			// }
  	// 	});
   //  });


   $("#modal-remove-feed").find("form").submit(function(e) {
      // console.log("sdsds");
      var form = $("#modal-remove-feed").find("form");
      var loading = $("#load-remove-feed");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if (response.unauthorized) {
          window.location.href = response.unauthorized;
        }
        else if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#remove-feed-notice');
        } else {
          $("#modal-remove-feed").modal('hide');
          status(response.title, response.body, 'alert-success');

          // // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

   $("#modal-provider-property").find("form").submit(function(e) {
      // console.log("sdsds");
      var form = $("#modal-provider-property").find("form");
      var loading = $("#load-provider-property");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if (response.unauthorized) {
          window.location.href = response.unauthorized;
        }
        else if(response.error) {

        	$(".sup-errors").html("");
					$(".form-control").removeClass("required");
					$.each(response.error, function(index, value) {
						var errors = value.split("|");
						$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
						$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
					});

          // status('Please correct the following:', errors, 'alert-danger', '#provider-property-notice');
        } else {
          $("#modal-provider-property").modal('hide');
          status(response.title, response.body, 'alert-success');

          // // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
  
	// // submit form
 //    $("#modal-export").find("form").submit(function(e) {
   
 //  		var form = $("#modal-export").find("form");
 //  		var loading = $("#load-export");
  		
 //  		// stop form from submitting
 //  		e.preventDefault();

 //  		loading.removeClass('hide');
 //  		// console.log(form.serialize());
 //  		// push form data
 //  		$.ajax({
	// 		type: "post",
	// 		url: form.attr('action'),
	// 		data: form.serialize(),
	// 		dataType: "json",
	// 		success: function(response) {
	// 	        if (response.unauthorized) {
	// 	          window.location.href = response.unauthorized;
	// 	        }
	// 			else if(response.error) {
	// 				$(".sup-errors").html("");
	// 				$(".form-control").removeClass("required");
	// 				$.each(response.error, function(index, value) {
	// 					var errors = value.split("|");
	// 					$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
	// 					$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
	// 				});
	// 			} else {
	// 				$("#modal-export").modal('hide');
	// 				status(response.title, response.body, 'alert-success');

	// 				// refresh the list
	// 				refresh();
	// 			}

	// 			loading.addClass('hide');
	// 		}
 //  		});
 //    });

});
